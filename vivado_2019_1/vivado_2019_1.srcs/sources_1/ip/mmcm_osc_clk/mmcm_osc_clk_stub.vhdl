-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Wed Jul 17 20:02:13 2019
-- Host        : siplx16.fysik.su.se running 64-bit Fedora release 27 (Twenty Seven)
-- Command     : write_vhdl -force -mode synth_stub
--               /scratch/edval/PhD/TileCal/Daughterboard_rev5/dbv5_production_fw/src/ip/mmcm_osc_clk/mmcm_osc_clk_stub.vhdl
-- Design      : mmcm_osc_clk
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku3p-ffvb676-2LV-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mmcm_osc_clk is
  Port ( 
    p_clk_40_out : out STD_LOGIC;
    p_clk_400_out : out STD_LOGIC;
    clk_in1_p : in STD_LOGIC;
    clk_in1_n : in STD_LOGIC
  );

end mmcm_osc_clk;

architecture stub of mmcm_osc_clk is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "p_clk_40_out,p_clk_400_out,clk_in1_p,clk_in1_n";
begin
end;
