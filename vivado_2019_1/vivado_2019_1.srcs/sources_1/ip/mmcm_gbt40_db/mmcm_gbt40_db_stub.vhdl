-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Aug  6 19:36:52 2019
-- Host        : siplx16.fysik.su.se running 64-bit Fedora release 27 (Twenty Seven)
-- Command     : write_vhdl -force -mode synth_stub
--               /scratch/edval/PhD/TileCal/Daughterboard_rev5/dbv5_production_fw/vivado_2019_1/vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db/mmcm_gbt40_db_stub.vhdl
-- Design      : mmcm_gbt40_db
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku3p-ffvb676-2LV-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mmcm_gbt40_db is
  Port ( 
    p_clk40_out : out STD_LOGIC;
    p_clk80_out : out STD_LOGIC;
    reset : in STD_LOGIC;
    p_input_clk_stopped_out : out STD_LOGIC;
    p_clkfb_stopped_out : out STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_clkin1_in : in STD_LOGIC
  );

end mmcm_gbt40_db;

architecture stub of mmcm_gbt40_db is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "p_clk40_out,p_clk80_out,reset,p_input_clk_stopped_out,p_clkfb_stopped_out,p_locked_out,p_clkin1_in";
begin
end;
