# file: xadc_system_managment.xdc
# (c) Copyright 2013 - 2013 Xilinx, Inc. All rights reserved.
# 
# This file contains confidential and proprietary information
# of Xilinx, Inc. and is protected under U.S. and
# international copyright and other intellectual property
# laws.
# 
# DISCLAIMER
# This disclaimer is not a license and does not grant any
# rights to the materials distributed herewith. Except as
# otherwise provided in a valid license issued to you by
# Xilinx, and to the maximum extent permitted by applicable
# law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
# WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
# AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
# BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
# INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
# (2) Xilinx shall not be liable (whether in contract or tort,
# including negligence, or under any other theory of
# liability) for any loss or damage of any kind or nature
# related to, arising under or in connection with these
# materials, including for any direct, or any indirect,
# special, incidental, or consequential loss or damage
# (including loss of data, profits, goodwill, or any type of
# loss or damage suffered as a result of any action brought
# by a third party) even if such damage or loss was
# reasonably foreseeable or Xilinx had been advised of the
# possibility of the same.
# 
# CRITICAL APPLICATIONS
# Xilinx products are not designed or intended to be fail-
# safe, or for use in any application requiring fail-safe
# performance, such as life-support or safety devices or
# systems, Class III medical devices, nuclear facilities,
# applications related to the deployment of airbags, or any
# other applications that could lead to death, personal
# injury, or severe property or environmental damage
# (individually and collectively, "Critical
# Applications"). Customer assumes the sole risk and
# liability of any use of Xilinx products in Critical
# Applications, subject only to applicable laws and
# regulations governing limitations on product liability.
# 
# THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
# PART OF THIS FILE AT ALL TIMES.


# Input clock periods. These duplicate the values entered for the
#  input clocks. You can use these to time your system
#----------------------------------------------------------------
#create_clock -name dclk_in -period 25 [get_ports dclk_in]
set_property IOSTANDARD ANALOG [get_ports  vp]
set_property IOSTANDARD ANALOG [get_ports  vn]
set_property PACKAGE_PIN AD26 [get_ports vauxp0]
set_property IOSTANDARD ANALOG [get_ports  vauxp0]
set_property PACKAGE_PIN AE26 [get_ports vauxn0]
set_property IOSTANDARD ANALOG [get_ports  vauxn0]
set_property PACKAGE_PIN AE25 [get_ports vauxp1]
set_property IOSTANDARD ANALOG [get_ports  vauxp1]
set_property PACKAGE_PIN AF25 [get_ports vauxn1]
set_property IOSTANDARD ANALOG [get_ports  vauxn1]

set_property PACKAGE_PIN AD20 [get_ports vauxp2]
set_property IOSTANDARD ANALOG [get_ports  vauxp2]
set_property PACKAGE_PIN AD21 [get_ports vauxn2]
set_property IOSTANDARD ANALOG [get_ports  vauxn2]

set_property PACKAGE_PIN AE20 [get_ports vauxp3]
set_property IOSTANDARD ANALOG [get_ports  vauxp3]
set_property PACKAGE_PIN AE21 [get_ports vauxn3]
set_property IOSTANDARD ANALOG [get_ports  vauxn3]

set_property PACKAGE_PIN V18 [get_ports vauxp4]
set_property IOSTANDARD ANALOG [get_ports  vauxp4]
set_property PACKAGE_PIN V19 [get_ports vauxn4]
set_property IOSTANDARD ANALOG [get_ports  vauxn4]

set_property PACKAGE_PIN W21 [get_ports vauxp5]
set_property IOSTANDARD ANALOG [get_ports  vauxp5]
set_property PACKAGE_PIN Y21 [get_ports vauxn5]
set_property IOSTANDARD ANALOG [get_ports  vauxn5]

set_property PACKAGE_PIN AF17 [get_ports vauxp6]
set_property IOSTANDARD ANALOG [get_ports  vauxp6]
set_property PACKAGE_PIN AF18 [get_ports vauxn6]
set_property IOSTANDARD ANALOG [get_ports  vauxn6]

set_property PACKAGE_PIN AF19 [get_ports vauxp7]
set_property IOSTANDARD ANALOG [get_ports  vauxp7]
set_property PACKAGE_PIN AF20 [get_ports vauxn7]
set_property IOSTANDARD ANALOG [get_ports  vauxn7]

set_property PACKAGE_PIN AF23 [get_ports vauxp8]
set_property IOSTANDARD ANALOG [get_ports  vauxp8]
set_property PACKAGE_PIN AF24 [get_ports vauxn8]
set_property IOSTANDARD ANALOG [get_ports  vauxn8]

set_property PACKAGE_PIN AE22 [get_ports vauxp9]
set_property IOSTANDARD ANALOG [get_ports  vauxp9]
set_property PACKAGE_PIN AF22 [get_ports vauxn9]
set_property IOSTANDARD ANALOG [get_ports  vauxn9]

set_property PACKAGE_PIN AB21 [get_ports vauxp10]
set_property IOSTANDARD ANALOG [get_ports  vauxp10]
set_property PACKAGE_PIN AC21 [get_ports vauxn10]
set_property IOSTANDARD ANALOG [get_ports  vauxn10]

set_property PACKAGE_PIN AC19 [get_ports vauxp11]
set_property IOSTANDARD ANALOG [get_ports  vauxp11]
set_property PACKAGE_PIN AD19 [get_ports vauxn11]
set_property IOSTANDARD ANALOG [get_ports  vauxn11]

set_property PACKAGE_PIN W18 [get_ports vauxp12]
set_property IOSTANDARD ANALOG [get_ports  vauxp12]
set_property PACKAGE_PIN W19 [get_ports vauxn12]
set_property IOSTANDARD ANALOG [get_ports  vauxn12]

set_property PACKAGE_PIN W16 [get_ports vauxp13]
set_property IOSTANDARD ANALOG [get_ports  vauxp13]
set_property PACKAGE_PIN Y16 [get_ports vauxn13]
set_property IOSTANDARD ANALOG [get_ports  vauxn13]

set_property PACKAGE_PIN AC18 [get_ports vauxp14]
set_property IOSTANDARD ANALOG [get_ports  vauxp14]
set_property PACKAGE_PIN AD18 [get_ports vauxn14]
set_property IOSTANDARD ANALOG [get_ports  vauxn14]

set_property PACKAGE_PIN AE17 [get_ports vauxp15]
set_property IOSTANDARD ANALOG [get_ports  vauxp15]
set_property PACKAGE_PIN AE18 [get_ports vauxn15]
set_property IOSTANDARD ANALOG [get_ports  vauxn15]


set_property LOC SYSMONE1_X0Y0 [get_cells -hier {*inst_sysmon} -filter {NAME =~ *inst_sysmon}]
#set_false_path -to [get_pins -hierarchical -filter {NAME =~ *inst_sysmon*/RESET}]
set_false_path -to [get_pins -of [get_cells -hier -filter {NAME =~ *inst_sysmon*}] -filter {NAME =~*RESET}]
set_property DONT_TOUCH true [get_cells -hierarchical -filter {NAME =~*/inst_sysmon*}]


