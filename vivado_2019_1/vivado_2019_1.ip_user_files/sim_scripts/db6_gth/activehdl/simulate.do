onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+db6_gth -L xil_defaultlib -L xpm -L gtwizard_ultrascale_v1_7_6 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.db6_gth xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {db6_gth.udo}

run -all

endsim

quit -force
