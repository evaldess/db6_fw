vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../ipstatic" \
"/scratch/edval/local/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -93 \
"/scratch/edval/local/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../ipstatic" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_osc_clk_1/mmcm_osc_clk_clk_wiz.v" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_osc_clk_1/mmcm_osc_clk.v" \

vlog -work xil_defaultlib \
"glbl.v"

