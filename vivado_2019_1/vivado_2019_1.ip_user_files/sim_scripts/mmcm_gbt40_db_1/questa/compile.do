vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm

vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm

vlog -work xil_defaultlib -64 -sv "+incdir+../../../ipstatic" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -64 -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../ipstatic" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db_1/mmcm_gbt40_db_clk_wiz.v" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db_1/mmcm_gbt40_db.v" \

vlog -work xil_defaultlib \
"glbl.v"

