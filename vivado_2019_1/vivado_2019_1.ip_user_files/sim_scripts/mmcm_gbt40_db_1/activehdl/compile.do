vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../ipstatic" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../ipstatic" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db_1/mmcm_gbt40_db_clk_wiz.v" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db_1/mmcm_gbt40_db.v" \

vlog -work xil_defaultlib \
"glbl.v"

