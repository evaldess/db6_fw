vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xil_defaultlib  -sv2k12 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -93 \
"../../../../vivado_2019_1.srcs/sources_1/ip/xadc_system_managment/xadc_system_managment_sysmon.vhd" \
"../../../../vivado_2019_1.srcs/sources_1/ip/xadc_system_managment/xadc_system_managment.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

