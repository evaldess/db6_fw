onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib xadc_system_managment_opt

do {wave.do}

view wave
view structure
view signals

do {xadc_system_managment.udo}

run -all

quit -force
