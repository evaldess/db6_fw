// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Nov 12 13:11:01 2019
// Host        : PiroFysikumOfficeAthlon-PC running 64-bit Ubuntu 18.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ xadc_system_managment_sim_netlist.v
// Design      : xadc_system_managment
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (dclk_in,
    reset_in,
    vauxp0,
    vauxn0,
    vauxp1,
    vauxn1,
    vauxp2,
    vauxn2,
    vauxp3,
    vauxn3,
    vauxp4,
    vauxn4,
    vauxp5,
    vauxn5,
    vauxp6,
    vauxn6,
    vauxp7,
    vauxn7,
    vauxp8,
    vauxn8,
    vauxp9,
    vauxn9,
    vauxp10,
    vauxn10,
    vauxp11,
    vauxn11,
    vauxp12,
    vauxn12,
    vauxp13,
    vauxn13,
    vauxp14,
    vauxn14,
    vauxp15,
    vauxn15,
    vp,
    vn,
    busy_out,
    channel_out,
    eoc_out,
    eos_out,
    ot_out,
    vccaux_alarm_out,
    vccint_alarm_out,
    user_temp_alarm_out,
    alarm_out);
  input dclk_in;
  input reset_in;
  input vauxp0;
  input vauxn0;
  input vauxp1;
  input vauxn1;
  input vauxp2;
  input vauxn2;
  input vauxp3;
  input vauxn3;
  input vauxp4;
  input vauxn4;
  input vauxp5;
  input vauxn5;
  input vauxp6;
  input vauxn6;
  input vauxp7;
  input vauxn7;
  input vauxp8;
  input vauxn8;
  input vauxp9;
  input vauxn9;
  input vauxp10;
  input vauxn10;
  input vauxp11;
  input vauxn11;
  input vauxp12;
  input vauxn12;
  input vauxp13;
  input vauxn13;
  input vauxp14;
  input vauxn14;
  input vauxp15;
  input vauxn15;
  input vp;
  input vn;
  output busy_out;
  output [5:0]channel_out;
  output eoc_out;
  output eos_out;
  output ot_out;
  output vccaux_alarm_out;
  output vccint_alarm_out;
  output user_temp_alarm_out;
  output alarm_out;

  wire alarm_out;
  wire busy_out;
  wire [5:0]channel_out;
  wire dclk_in;
  wire eoc_out;
  wire eos_out;
  wire ot_out;
  wire reset_in;
  wire user_temp_alarm_out;
  wire vauxn0;
  wire vauxn1;
  wire vauxn10;
  wire vauxn11;
  wire vauxn12;
  wire vauxn13;
  wire vauxn14;
  wire vauxn15;
  wire vauxn2;
  wire vauxn3;
  wire vauxn4;
  wire vauxn5;
  wire vauxn6;
  wire vauxn7;
  wire vauxn8;
  wire vauxn9;
  wire vauxp0;
  wire vauxp1;
  wire vauxp10;
  wire vauxp11;
  wire vauxp12;
  wire vauxp13;
  wire vauxp14;
  wire vauxp15;
  wire vauxp2;
  wire vauxp3;
  wire vauxp4;
  wire vauxp5;
  wire vauxp6;
  wire vauxp7;
  wire vauxp8;
  wire vauxp9;
  wire vccaux_alarm_out;
  wire vccint_alarm_out;
  wire vn;
  wire vp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xadc_system_managment_sysmon U0
       (.alarm_out(alarm_out),
        .busy_out(busy_out),
        .channel_out(channel_out),
        .dclk_in(dclk_in),
        .eoc_out(eoc_out),
        .eos_out(eos_out),
        .ot_out(ot_out),
        .reset_in(reset_in),
        .user_temp_alarm_out(user_temp_alarm_out),
        .vauxn0(vauxn0),
        .vauxn1(vauxn1),
        .vauxn10(vauxn10),
        .vauxn11(vauxn11),
        .vauxn12(vauxn12),
        .vauxn13(vauxn13),
        .vauxn14(vauxn14),
        .vauxn15(vauxn15),
        .vauxn2(vauxn2),
        .vauxn3(vauxn3),
        .vauxn4(vauxn4),
        .vauxn5(vauxn5),
        .vauxn6(vauxn6),
        .vauxn7(vauxn7),
        .vauxn8(vauxn8),
        .vauxn9(vauxn9),
        .vauxp0(vauxp0),
        .vauxp1(vauxp1),
        .vauxp10(vauxp10),
        .vauxp11(vauxp11),
        .vauxp12(vauxp12),
        .vauxp13(vauxp13),
        .vauxp14(vauxp14),
        .vauxp15(vauxp15),
        .vauxp2(vauxp2),
        .vauxp3(vauxp3),
        .vauxp4(vauxp4),
        .vauxp5(vauxp5),
        .vauxp6(vauxp6),
        .vauxp7(vauxp7),
        .vauxp8(vauxp8),
        .vauxp9(vauxp9),
        .vccaux_alarm_out(vccaux_alarm_out),
        .vccint_alarm_out(vccint_alarm_out),
        .vn(vn),
        .vp(vp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_xadc_system_managment_sysmon
   (dclk_in,
    reset_in,
    vauxp0,
    vauxn0,
    vauxp1,
    vauxn1,
    vauxp2,
    vauxn2,
    vauxp3,
    vauxn3,
    vauxp4,
    vauxn4,
    vauxp5,
    vauxn5,
    vauxp6,
    vauxn6,
    vauxp7,
    vauxn7,
    vauxp8,
    vauxn8,
    vauxp9,
    vauxn9,
    vauxp10,
    vauxn10,
    vauxp11,
    vauxn11,
    vauxp12,
    vauxn12,
    vauxp13,
    vauxn13,
    vauxp14,
    vauxn14,
    vauxp15,
    vauxn15,
    vp,
    vn,
    busy_out,
    channel_out,
    eoc_out,
    eos_out,
    ot_out,
    vccaux_alarm_out,
    vccint_alarm_out,
    user_temp_alarm_out,
    alarm_out);
  input dclk_in;
  input reset_in;
  input vauxp0;
  input vauxn0;
  input vauxp1;
  input vauxn1;
  input vauxp2;
  input vauxn2;
  input vauxp3;
  input vauxn3;
  input vauxp4;
  input vauxn4;
  input vauxp5;
  input vauxn5;
  input vauxp6;
  input vauxn6;
  input vauxp7;
  input vauxn7;
  input vauxp8;
  input vauxn8;
  input vauxp9;
  input vauxn9;
  input vauxp10;
  input vauxn10;
  input vauxp11;
  input vauxn11;
  input vauxp12;
  input vauxn12;
  input vauxp13;
  input vauxn13;
  input vauxp14;
  input vauxn14;
  input vauxp15;
  input vauxn15;
  input vp;
  input vn;
  output busy_out;
  output [5:0]channel_out;
  output eoc_out;
  output eos_out;
  output ot_out;
  output vccaux_alarm_out;
  output vccint_alarm_out;
  output user_temp_alarm_out;
  output alarm_out;

  wire O;
  wire [15:0]VAUXN;
  wire alarm_out;
  wire busy_out;
  wire [5:0]channel_out;
  wire dclk_in;
  wire eoc_out;
  wire eos_out;
  wire ibuf_an_vauxp10_n_0;
  wire ibuf_an_vauxp11_n_0;
  wire ibuf_an_vauxp12_n_0;
  wire ibuf_an_vauxp13_n_0;
  wire ibuf_an_vauxp14_n_0;
  wire ibuf_an_vauxp15_n_0;
  wire ibuf_an_vauxp1_n_0;
  wire ibuf_an_vauxp2_n_0;
  wire ibuf_an_vauxp3_n_0;
  wire ibuf_an_vauxp4_n_0;
  wire ibuf_an_vauxp5_n_0;
  wire ibuf_an_vauxp6_n_0;
  wire ibuf_an_vauxp7_n_0;
  wire ibuf_an_vauxp8_n_0;
  wire ibuf_an_vauxp9_n_0;
  wire ot_out;
  wire reset_in;
  wire user_temp_alarm_out;
  wire vauxn0;
  wire vauxn1;
  wire vauxn10;
  wire vauxn11;
  wire vauxn12;
  wire vauxn13;
  wire vauxn14;
  wire vauxn15;
  wire vauxn2;
  wire vauxn3;
  wire vauxn4;
  wire vauxn5;
  wire vauxn6;
  wire vauxn7;
  wire vauxn8;
  wire vauxn9;
  wire vauxp0;
  wire vauxp1;
  wire vauxp10;
  wire vauxp11;
  wire vauxp12;
  wire vauxp13;
  wire vauxp14;
  wire vauxp15;
  wire vauxp2;
  wire vauxp3;
  wire vauxp4;
  wire vauxp5;
  wire vauxp6;
  wire vauxp7;
  wire vauxp8;
  wire vauxp9;
  wire vccaux_alarm_out;
  wire vccint_alarm_out;
  wire vn;
  wire vp;
  wire NLW_inst_sysmon_DRDY_UNCONNECTED;
  wire NLW_inst_sysmon_I2C_SCLK_TS_UNCONNECTED;
  wire NLW_inst_sysmon_I2C_SDA_TS_UNCONNECTED;
  wire NLW_inst_sysmon_JTAGBUSY_UNCONNECTED;
  wire NLW_inst_sysmon_JTAGLOCKED_UNCONNECTED;
  wire NLW_inst_sysmon_JTAGMODIFIED_UNCONNECTED;
  wire [15:3]NLW_inst_sysmon_ALM_UNCONNECTED;
  wire [15:0]NLW_inst_sysmon_DO_UNCONNECTED;
  wire [4:0]NLW_inst_sysmon_MUXADDR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn0
       (.I(vauxn0),
        .O(VAUXN[0]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn1
       (.I(vauxn1),
        .O(VAUXN[1]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn10
       (.I(vauxn10),
        .O(VAUXN[10]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn11
       (.I(vauxn11),
        .O(VAUXN[11]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn12
       (.I(vauxn12),
        .O(VAUXN[12]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn13
       (.I(vauxn13),
        .O(VAUXN[13]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn14
       (.I(vauxn14),
        .O(VAUXN[14]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn15
       (.I(vauxn15),
        .O(VAUXN[15]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn2
       (.I(vauxn2),
        .O(VAUXN[2]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn3
       (.I(vauxn3),
        .O(VAUXN[3]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn4
       (.I(vauxn4),
        .O(VAUXN[4]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn5
       (.I(vauxn5),
        .O(VAUXN[5]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn6
       (.I(vauxn6),
        .O(VAUXN[6]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn7
       (.I(vauxn7),
        .O(VAUXN[7]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn8
       (.I(vauxn8),
        .O(VAUXN[8]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxn9
       (.I(vauxn9),
        .O(VAUXN[9]));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp0
       (.I(vauxp0),
        .O(O));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp1
       (.I(vauxp1),
        .O(ibuf_an_vauxp1_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp10
       (.I(vauxp10),
        .O(ibuf_an_vauxp10_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp11
       (.I(vauxp11),
        .O(ibuf_an_vauxp11_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp12
       (.I(vauxp12),
        .O(ibuf_an_vauxp12_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp13
       (.I(vauxp13),
        .O(ibuf_an_vauxp13_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp14
       (.I(vauxp14),
        .O(ibuf_an_vauxp14_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp15
       (.I(vauxp15),
        .O(ibuf_an_vauxp15_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp2
       (.I(vauxp2),
        .O(ibuf_an_vauxp2_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp3
       (.I(vauxp3),
        .O(ibuf_an_vauxp3_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp4
       (.I(vauxp4),
        .O(ibuf_an_vauxp4_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp5
       (.I(vauxp5),
        .O(ibuf_an_vauxp5_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp6
       (.I(vauxp6),
        .O(ibuf_an_vauxp6_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp7
       (.I(vauxp7),
        .O(ibuf_an_vauxp7_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp8
       (.I(vauxp8),
        .O(ibuf_an_vauxp8_n_0));
  (* box_type = "PRIMITIVE" *) 
  IBUF_ANALOG ibuf_an_vauxp9
       (.I(vauxp9),
        .O(ibuf_an_vauxp9_n_0));
  (* box_type = "PRIMITIVE" *) 
  SYSMONE1 #(
    .INIT_40(16'h3000),
    .INIT_41(16'h2F90),
    .INIT_42(16'h0800),
    .INIT_43(16'h200F),
    .INIT_44(16'h0000),
    .INIT_45(16'hDEDC),
    .INIT_46(16'h0000),
    .INIT_47(16'h0000),
    .INIT_48(16'h4F01),
    .INIT_49(16'hFFFF),
    .INIT_4A(16'h4F00),
    .INIT_4B(16'hFFFF),
    .INIT_4C(16'h0000),
    .INIT_4D(16'h0000),
    .INIT_4E(16'h0000),
    .INIT_4F(16'h0000),
    .INIT_50(16'hB723),
    .INIT_51(16'h4E81),
    .INIT_52(16'hA147),
    .INIT_53(16'hCB93),
    .INIT_54(16'hAA5F),
    .INIT_55(16'h4963),
    .INIT_56(16'h9555),
    .INIT_57(16'hAF7B),
    .INIT_58(16'h4E81),
    .INIT_59(16'h4963),
    .INIT_5A(16'h4963),
    .INIT_5B(16'h9A74),
    .INIT_5C(16'h4963),
    .INIT_5D(16'h451E),
    .INIT_5E(16'h451E),
    .INIT_5F(16'h91EB),
    .INIT_60(16'h9A74),
    .INIT_61(16'h4DA7),
    .INIT_62(16'h9A74),
    .INIT_63(16'h4D39),
    .INIT_64(16'h0000),
    .INIT_65(16'h0000),
    .INIT_66(16'h0000),
    .INIT_67(16'h0000),
    .INIT_68(16'h98BF),
    .INIT_69(16'h4BF2),
    .INIT_6A(16'h98BF),
    .INIT_6B(16'h4C5E),
    .INIT_6C(16'h0000),
    .INIT_6D(16'h0000),
    .INIT_6E(16'h0000),
    .INIT_6F(16'h0000),
    .INIT_70(16'h0000),
    .INIT_71(16'h0000),
    .INIT_72(16'h0000),
    .INIT_73(16'h0000),
    .INIT_74(16'h0000),
    .INIT_75(16'h0000),
    .INIT_76(16'h0000),
    .INIT_77(16'h0000),
    .INIT_78(16'h0000),
    .INIT_79(16'h0000),
    .INIT_7A(16'h0000),
    .INIT_7B(16'h0000),
    .INIT_7C(16'h0000),
    .INIT_7D(16'h0000),
    .INIT_7E(16'h0000),
    .INIT_7F(16'h0000),
    .IS_CONVSTCLK_INVERTED(1'b0),
    .IS_DCLK_INVERTED(1'b0),
    .SIM_MONITOR_FILE("design.dat"),
    .SYSMON_VUSER0_BANK(0),
    .SYSMON_VUSER0_MONITOR("NONE"),
    .SYSMON_VUSER1_BANK(0),
    .SYSMON_VUSER1_MONITOR("NONE"),
    .SYSMON_VUSER2_BANK(0),
    .SYSMON_VUSER2_MONITOR("NONE"),
    .SYSMON_VUSER3_BANK(0),
    .SYSMON_VUSER3_MONITOR("NONE")) 
    inst_sysmon
       (.ALM({NLW_inst_sysmon_ALM_UNCONNECTED[15:8],alarm_out,NLW_inst_sysmon_ALM_UNCONNECTED[6:3],vccaux_alarm_out,vccint_alarm_out,user_temp_alarm_out}),
        .BUSY(busy_out),
        .CHANNEL(channel_out),
        .CONVST(1'b0),
        .CONVSTCLK(1'b0),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(dclk_in),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_inst_sysmon_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_inst_sysmon_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .EOC(eoc_out),
        .EOS(eos_out),
        .I2C_SCLK(1'b0),
        .I2C_SCLK_TS(NLW_inst_sysmon_I2C_SCLK_TS_UNCONNECTED),
        .I2C_SDA(1'b0),
        .I2C_SDA_TS(NLW_inst_sysmon_I2C_SDA_TS_UNCONNECTED),
        .JTAGBUSY(NLW_inst_sysmon_JTAGBUSY_UNCONNECTED),
        .JTAGLOCKED(NLW_inst_sysmon_JTAGLOCKED_UNCONNECTED),
        .JTAGMODIFIED(NLW_inst_sysmon_JTAGMODIFIED_UNCONNECTED),
        .MUXADDR(NLW_inst_sysmon_MUXADDR_UNCONNECTED[4:0]),
        .OT(ot_out),
        .RESET(reset_in),
        .VAUXN(VAUXN),
        .VAUXP({ibuf_an_vauxp15_n_0,ibuf_an_vauxp14_n_0,ibuf_an_vauxp13_n_0,ibuf_an_vauxp12_n_0,ibuf_an_vauxp11_n_0,ibuf_an_vauxp10_n_0,ibuf_an_vauxp9_n_0,ibuf_an_vauxp8_n_0,ibuf_an_vauxp7_n_0,ibuf_an_vauxp6_n_0,ibuf_an_vauxp5_n_0,ibuf_an_vauxp4_n_0,ibuf_an_vauxp3_n_0,ibuf_an_vauxp2_n_0,ibuf_an_vauxp1_n_0,O}),
        .VN(vn),
        .VP(vp));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
