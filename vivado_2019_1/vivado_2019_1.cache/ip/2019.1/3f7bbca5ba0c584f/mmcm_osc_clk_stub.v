// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Sep  7 20:12:49 2019
// Host        : siplx16.fysik.su.se running 64-bit Fedora release 27 (Twenty Seven)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_osc_clk_stub.v
// Design      : mmcm_osc_clk
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(p_clk_40_out, p_clk_400_out, clk_in1_p, 
  clk_in1_n)
/* synthesis syn_black_box black_box_pad_pin="p_clk_40_out,p_clk_400_out,clk_in1_p,clk_in1_n" */;
  output p_clk_40_out;
  output p_clk_400_out;
  input clk_in1_p;
  input clk_in1_n;
endmodule
