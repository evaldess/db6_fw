## osc_clocks

##40mhz_clocks

##commbus clock


## configbus clocks (80 mhz)
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {gen_configbus_clks[0].i_configbus_clk_loc/O}]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {gen_configbus_clks[1].i_configbus_clk_loc/O}]
#set_property CLOCK_DEDICATED_ROUTE ANY_CMT_COLUMN [get_nets {gen_configbus_clks[1].i_configbus_clk_loc/O}]

#db_clk40
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets i_mmcm_gbt40_db/inst/p_clkin2_in_mmcm_gbt40_db]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets i_mmcm_gbt40_db/inst/p_clkin1_in_mmcm_gbt40_db]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets i_gbtx_clk40_db_loc/O]


#mb_clk4
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets i_primary_clk_mb0/O]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {i_mmcm_gbt40_mb/gen_mmcms[0].i_mmcm_mb/inst/p_clkin2_in_mmcm_mb}]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {i_mmcm_gbt40_mb/gen_mmcms[1].i_mmcm_mb/inst/p_clkin1_in_mmcm_mb}]
#set_property CLKIN2_PERIOD {25 nS} [get_cells {i_mmcm_gbt40_mb/gen_mmcms[1].i_mmcm_mb/inst/mmcme4_adv_inst}]


#clocks
#create_clock -period 12.5 [get_ports i_db6_gbt_gth_wrapper/i_gth_tx_2/qpll1outrefclk_out[0]]
create_clock -period 12.500 -name {p_configbus_clk_p_in[0]} -waveform {0.000 6.250} [get_ports {p_configbus_clk_p_in[0]}]
create_clock -period 12.500 -name {i_db6_gbt_gth_wrapper/i_gth_tx_2/inst/gen_gtwizard_gthe4_top.gth_tx_2_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[2].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/txoutclkfabric_out[0]} -waveform {0.000 6.250} [get_pins {i_db6_gbt_gth_wrapper/i_gth_tx_2/inst/gen_gtwizard_gthe4_top.gth_tx_2_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[2].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].gthE4_CHANNEL_PRIM_INST/TXOUTCLKFABRIC}]

create_clock -period 25.000 -name {p_clk40_gbtx_loc_p_in[0]} -waveform {0.000 12.500} [get_ports {p_clk40_gbtx_loc_p_in[0]}]
create_clock -period 25.000 -name {p_clk40_gbtx_loc_p_in[1]} -waveform {0.000 12.500} [get_ports {p_clk40_gbtx_loc_p_in[1]}]
#create_clock -period 25.000 -name {p_clk40_gbtx_loc_p_in[2]} -waveform {0.000 12.500} [get_ports {p_clk40_gbtx_loc_p_in[2]}]

create_clock -period 25.000 -name {p_clk40_gbtx_rem_p_in[0]} -waveform {0.000 12.500} [get_ports {p_clk40_gbtx_rem_p_in[0]}]
create_clock -period 25.000 -name {p_clk40_gbtx_rem_p_in[1]} -waveform {0.000 12.500} [get_ports {p_clk40_gbtx_rem_p_in[1]}]
#create_clock -period 25.000 -name {p_clk40_gbtx_rem_p_in[2]} -waveform {0.000 12.500} [get_ports {p_clk40_gbtx_rem_p_in[2]}]
#
#
create_clock -period 25.000 -name {gbtx_loc_mb_p_in[0]} -waveform {0.000 12.500} [get_ports {gbtx_loc_mb_p_in[0]}]
create_clock -period 25.000 -name {gbtx_loc_mb_p_in[1]} -waveform {0.000 12.500} [get_ports {gbtx_loc_mb_p_in[1]}]

create_clock -period 25.000 -name {gbtx_rem_mb_p_in[0]} -waveform {0.000 12.500} [get_ports {gbtx_rem_mb_p_in[0]}]
create_clock -period 25.000 -name {gbtx_rem_mb_p_in[1]} -waveform {0.000 12.500} [get_ports {gbtx_rem_mb_p_in[1]}]
#
#
create_clock -period 3.571 -name {p_adc_bitclk_p_in[0]} [get_ports {p_adc_bitclk_p_in[0]}]
create_clock -period 3.571 -name {p_adc_bitclk_p_in[1]} [get_ports {p_adc_bitclk_p_in[1]}]
create_clock -period 3.571 -name {p_adc_bitclk_p_in[2]} [get_ports {p_adc_bitclk_p_in[2]}]
create_clock -period 3.571 -name {p_adc_bitclk_p_in[3]} [get_ports {p_adc_bitclk_p_in[3]}]
create_clock -period 3.571 -name {p_adc_bitclk_p_in[4]} [get_ports {p_adc_bitclk_p_in[4]}]
create_clock -period 3.571 -name {p_adc_bitclk_p_in[5]} [get_ports {p_adc_bitclk_p_in[5]}]
create_clock -period 25.000 -name p_fpga_commbus_clk_in -waveform {0.000 12.500} [get_ports p_fpga_commbus_clk_in]
create_clock -period 6.250 -name p_gth_refclk_gbtx_loc_p_in -waveform {0.000 3.125} [get_ports p_gth_refclk_gbtx_loc_p_in]

create_generated_clock -name i_mmcm_osc/s_clk100_osc_reg_0 -source [get_pins i_mmcm_osc/i_mmcm_osc_clk/inst/mmcme4_adv_inst/CLKOUT1] -divide_by 4 [get_pins i_mmcm_osc/s_clk100_osc_reg/Q]
create_generated_clock -name i_mmcm_osc/s_clk1_osc_reg_0 -source [get_pins i_mmcm_osc/s_clk10_osc_reg/Q] -divide_by 10 [get_pins i_mmcm_osc/s_clk1_osc_reg/Q]
create_generated_clock -name {i_mmcm_osc/s_clknet[clk10_osc]} -source [get_pins i_mmcm_osc/i_mmcm_osc_clk/inst/mmcme4_adv_inst/CLKOUT0] -divide_by 4 [get_pins i_mmcm_osc/s_clk10_osc_reg/Q]
create_generated_clock -name i_mmcm_osc/s_clk100khz_osc_reg_0 -source [get_pins i_mmcm_osc/s_clk1_osc_reg/Q] -divide_by 10 [get_pins i_mmcm_osc/s_clk100khz_osc_reg/Q]
create_generated_clock -name i_mmcm_osc/p_clk5hz_out -source [get_pins i_mmcm_osc/s_clk10hz_osc_reg/Q] -divide_by 2 [get_pins i_mmcm_osc/s_clk5hz_osc_reg/Q]
create_generated_clock -name i_mmcm_osc/s_clk100hz_osc_reg_0 -source [get_pins i_mmcm_osc/s_clk100khz_osc_reg/Q] -divide_by 1000 [get_pins i_mmcm_osc/s_clk100hz_osc_reg/Q]
create_generated_clock -name i_mmcm_osc/s_clk10hz_osc_reg_0 -source [get_pins i_mmcm_osc/s_clk100hz_osc_reg/Q] -divide_by 10 [get_pins i_mmcm_osc/s_clk10hz_osc_reg/Q]



#bitclock
set_clock_groups -physically_exclusive -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_adc_bitclk_p_in[0]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_adc_bitclk_p_in[1]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_adc_bitclk_p_in[2]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_adc_bitclk_p_in[3]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_adc_bitclk_p_in[4]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_adc_bitclk_p_in[5]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_adc_bitclk_p_in[0]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_adc_bitclk_p_in[1]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_adc_bitclk_p_in[2]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_adc_bitclk_p_in[3]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_adc_bitclk_p_in[4]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_adc_bitclk_p_in[5]}]



set_clock_groups -physically_exclusive -group [get_clocks p_fpga_commbus_clk_in] -group [get_clocks p_clk_40_out_mmcm_osc_clk]






set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {p_adc_bitclk_p_in[0]}]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {p_adc_bitclk_p_in[1]}]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {p_adc_bitclk_p_in[2]}]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {p_adc_bitclk_p_in[3]}]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {p_adc_bitclk_p_in[4]}]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {p_adc_bitclk_p_in[5]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks p_clk_400_out_mmcm_osc_clk]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]

set_clock_groups -physically_exclusive -group [get_clocks p_fpga_commbus_clk_in] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]

#set_clock_groups -asynchronous -group [get_clocks p_clk560_out_pll_adc_bitclk] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk560_out_pll_adc_bitclk_1] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk560_out_pll_adc_bitclk_2] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk560_out_pll_adc_bitclk_3] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk560_out_pll_adc_bitclk_4] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk560_out_pll_adc_bitclk_5] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk560_out_pll_adc_bitclk]
#set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk560_out_pll_adc_bitclk]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk560_out_pll_adc_bitclk_1]
#set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk560_out_pll_adc_bitclk_1]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk560_out_pll_adc_bitclk_2]
#set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk560_out_pll_adc_bitclk_2]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk560_out_pll_adc_bitclk_3]
#set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk560_out_pll_adc_bitclk_3]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk560_out_pll_adc_bitclk_4]
#set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk560_out_pll_adc_bitclk_4]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk560_out_pll_adc_bitclk_5]
#set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk560_out_pll_adc_bitclk_5]

set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks p_fpga_commbus_clk_in]


#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[0]}]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[1]}]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[2]}]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[3]}]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[4]}]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[5]}]
#set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[0]}] -group [get_clocks p_clk40_out_pll_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[1]}] -group [get_clocks p_clk40_out_pll_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[2]}] -group [get_clocks p_clk40_out_pll_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[3]}] -group [get_clocks p_clk40_out_pll_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[4]}] -group [get_clocks p_clk40_out_pll_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[5]}] -group [get_clocks p_clk40_out_pll_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_gbt40_db] -group [get_clocks p_clk_40_out_mmcm_osc_clk]

#gth refclkfabric test
#create_generated_clock -name i_db6_main/i_db6_cs_interface/s_cs_sclk_d -source [get_pins {i_db6_gbt_gth_wrapper/gen_gthrefout[0].i_gthrefout_fabric_bufgce_div/O}] -divide_by 1 [get_pins {i_db6_main/i_db6_cs_interface/s_cs_sclk_fifo_reg[3]/Q}]
##set_false_path -from [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -to [get_clocks {p_adc_bitclk_p_in[0]}]
##set_false_path -from [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -to [get_clocks {p_adc_bitclk_p_in[1]}]
##set_false_path -from [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -to [get_clocks {p_adc_bitclk_p_in[2]}]
##set_false_path -from [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -to [get_clocks {p_adc_bitclk_p_in[3]}]
##set_false_path -from [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -to [get_clocks {p_adc_bitclk_p_in[4]}]
##set_false_path -from [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -to [get_clocks {p_adc_bitclk_p_in[5]}]
#set_clock_groups -physically_exclusive -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]
#set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[0]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[1]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[2]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[3]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[4]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[5]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -group [get_clocks {i_db6_gbt_gth_wrapper/i_gth_tx_2/inst/gen_gtwizard_gthe4_top.gth_tx_2_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[2].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/txoutclkfabric_out[0]}]
#set_clock_groups -physically_exclusive -group [get_clocks {i_db6_gbt_gth_wrapper/i_gth_tx_2/inst/gen_gtwizard_gthe4_top.gth_tx_2_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[2].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/txoutclkfabric_out[0]}] -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}]
#set_clock_groups -physically_exclusive -group [get_clocks {s_gthclkoutputs[gth_gtgrefclk_fabric_div]}] -group [get_clocks {txoutclk_out[1]}]

#no gthtxfabricclk
set_property ASYNC_REG true [get_cells {i_db6_xadc_interface/p_leds_out_reg[1]}]
set_property ASYNC_REG true [get_cells {i_db6_xadc_interface/p_leds_out_reg[3]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[8]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_data_aligned_reg[0]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[14]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_data_aligned_reg[8]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[9]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_data_aligned_reg[1]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_data_address_buffer_reg[1]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_data_address_buffer_reg[2]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_data_address_buffer_reg[3]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[10]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[11]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[12]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[13]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[15]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[18]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[19]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[20]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[21]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[23]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[24]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[25]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[26]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[27]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[28]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[29]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[30]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[31]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_register_data_buffer_reg[7]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[0]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[11]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[12]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[13]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[1]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[27]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[28]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[29]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[2]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[3]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[43]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[44]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[45]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[4]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[5]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[6]}]
set_property ASYNC_REG true [get_cells {i_db6_configbus_interface/s_db_configbus_shift_register_reg[7]}]
set_property ASYNC_REG true [get_cells {i_db6_xadc_interface/s_db_xadc_voltages_reg[0][10]}]
set_property ASYNC_REG true [get_cells {i_db6_xadc_interface/s_db_xadc_voltages_reg[0][11]}]
set_property ASYNC_REG true [get_cells {i_db6_xadc_interface/s_db_xadc_voltages_reg[0][15]}]
set_property ASYNC_REG true [get_cells {i_db6_xadc_interface/p_leds_out_reg[0]}]

create_generated_clock -name i_db6_main/i_db6_cs_interface/s_cs_sclk_d -source [get_pins i_mmcm_gbt40_db/inst/mmcme4_adv_inst/CLKOUT0] -divide_by 1 [get_pins {i_db6_main/i_db6_cs_interface/s_cs_sclk_fifo_reg[3]/Q}]

#set_clock_groups -asynchronous -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
#set_clock_groups -asynchronous -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks p_clk80_out_mmcm_gbt40_db]



set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks {p_clk40_gbtx_rem_p_in[0]}] -group [get_clocks -include_generated_clocks {p_clk40_gbtx_loc_p_in[0]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[0]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[1]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[2]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[3]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[4]}]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {p_adc_bitclk_p_in[5]}]
set_clock_groups -physically_exclusive -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[0]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[1]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[2]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[3]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[4]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_adc_bitclk_p_in[5]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {txoutclk_out[1]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks p_clk80_out_mmcm_gbt40_db]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -physically_exclusive -group [get_clocks {txoutclk_out[1]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks s_bitclk_div_0]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks s_bitclk_div_1]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks s_bitclk_div_2]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks s_bitclk_div_3]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks s_bitclk_div_4]
set_clock_groups -physically_exclusive -group [get_clocks {p_clk40_gbtx_loc_p_in[0]}] -group [get_clocks s_bitclk_div_5]
set_clock_groups -physically_exclusive -group [get_clocks p_clk40_out_mmcm_gbt40_db] -group [get_clocks {txoutclk_out[1]}]

set_clock_groups -asynchronous -group [get_clocks i_mmcm_osc/s_clk100_osc_reg_0] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]
set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0]
set_clock_groups -asynchronous -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks i_mmcm_osc/s_clk100khz_osc_reg_0]
set_clock_groups -asynchronous -group [get_clocks i_mmcm_osc/p_clk5hz_out] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks i_mmcm_osc/s_clk100hz_osc_reg_0] -group [get_clocks p_clk_40_out_mmcm_osc_clk]




