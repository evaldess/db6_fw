#set_property PACKAGE_PIN K22 [get_ports {p_adc_frameclk_p_in[4]}]
#set_property PACKAGE_PIN AA22 [get_ports {p_adc_frameclk_p_in[3]}]
#set_property PACKAGE_PIN AA19 [get_ports {p_adc_frameclk_p_in[2]}]
#set_property PACKAGE_PIN J23 [get_ports {p_adc_frameclk_p_in[1]}]
#set_property PACKAGE_PIN H23 [get_ports {p_adc_frameclk_p_in[0]}]
#set_property PACKAGE_PIN AB24 [get_ports {p_adc_frameclk_p_in[5]}]
#################################################################################--
###                                                                            ##--
### db6 constraints                                                            ##--
### Version: 1.0                                                               ##--
### Creation date: 2013-11-05                                                  ##--
### Created by: Sam Silverstein                                                ##--
###           : Eduardo Valdes                                                 ##--
### Modification date:                                                         ##--
### Modified by:                         		                              ##--
###                                                                            ##--
#################################################################################--

## bitstream setting constraints

set_property BITSTREAM.GENERAL.COMPRESS true [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 63.8 [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS gnd [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR no [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE yes [current_design]


#set_property IOSTANDARD LVDS [get_ports p_osc_n_in]
#set_property IOSTANDARD LVDS [get_ports p_osc_p_in]
#set_property PACKAGE_PIN E18 [get_ports p_osc_p_in]
#set_property PACKAGE_PIN D18 [get_ports p_osc_n_in]


#set_property PACKAGE_PIN V23 [get_ports {p_clk40_gbtx_loc_p_in[0]}]
#set_property PACKAGE_PIN W23 [get_ports {p_clk40_gbtx_loc_n_in[0]}]
#set_property PACKAGE_PIN AC19 [get_ports {p_clk40_gbtx_loc_p_in[1]}]
#set_property PACKAGE_PIN AD19 [get_ports {p_clk40_gbtx_loc_n_in[1]}]
#set_property PACKAGE_PIN AD20 [get_ports {p_clk40_gbtx_loc_p_in[2]}]
#set_property PACKAGE_PIN AE20 [get_ports {p_clk40_gbtx_loc_n_in[2]}]

#set_property PACKAGE_PIN D19 [get_ports {p_clk40_gbtx_rem_p_in[0]}]
#set_property PACKAGE_PIN D20 [get_ports {p_clk40_gbtx_rem_n_in[0]}]
#set_property PACKAGE_PIN C18 [get_ports {p_clk40_gbtx_rem_p_in[1]}]
#set_property PACKAGE_PIN C19 [get_ports {p_clk40_gbtx_rem_n_in[1]}]
#set_property PACKAGE_PIN T24 [get_ports {p_clk40_gbtx_rem_p_in[2]}]
#set_property PACKAGE_PIN U24 [get_ports {p_clk40_gbtx_rem_n_in[2]}]


#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_n_in[0]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_p_in[0]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_p_in[1]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_n_in[1]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_n_in[2]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_p_in[2]}]

#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_n_in[0]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_p_in[0]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_n_in[1]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_p_in[1]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_n_in[2]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_p_in[2]}]

## gth reference clocks (160 mhz)
#set_property PACKAGE_PIN M6 [get_ports p_gth_refclk_gbtx_loc_n_in]
#set_property PACKAGE_PIN M7 [get_ports p_gth_refclk_gbtx_loc_p_in]

##set_property PACKAGE_PIN P6 [get_ports p_gth_refclk_gbtx_rem_n_in]
##set_property PACKAGE_PIN P7 [get_ports p_gth_refclk_gbtx_rem_p_in]




#set_property PACKAGE_PIN M20 [get_ports {p_configbus_clk_p_in[0]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_configbus_clk_p_in[0]}]

#set_property PACKAGE_PIN M19 [get_ports {p_configbus_clk_p_in[1]}]
#set_property IOSTANDARD SUB_LVDS [get_ports {p_configbus_clk_p_in[1]}]



##this are for the phase shifting module if inserted
##set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {i_mmcm_gbt40_mb/gen_mmcms[0].i_mmcm_mb/inst/p_clkin1_in_mmcm_mb}]
##set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {i_mmcm_gbt40_mb/gen_mmcms[0].i_mmcm_mb/inst/p_clkin2_in_mmcm_mb}]
##set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {i_mmcm_gbt40_mb/gen_mmcms[1].i_mmcm_mb/inst/p_clkin1_in_mmcm_mb}]
##set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {i_mmcm_gbt40_mb/gen_mmcms[1].i_mmcm_mb/inst/p_clkin2_in_mmcm_mb}]


##
## io signal standards/placement
##
##leds
#set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[3]}]
#set_property PACKAGE_PIN AA15 [get_ports {p_leds_out[0]}]
#set_property PACKAGE_PIN AB15 [get_ports {p_leds_out[1]}]
#set_property PACKAGE_PIN AB16 [get_ports {p_leds_out[2]}]
#set_property PACKAGE_PIN AC14 [get_ports {p_leds_out[3]}]

##gbtx i2c / extra pins
#set_property IOSTANDARD LVCMOS18 [get_ports p_gbtx_i2c_scl_inout]
#set_property IOSTANDARD LVCMOS18 [get_ports p_gbtx_i2c_sda_inout]
#set_property PACKAGE_PIN F17 [get_ports p_gbtx_i2c_scl_inout]
#set_property PACKAGE_PIN F22 [get_ports p_gbtx_i2c_sda_inout]

#set_property IOSTANDARD LVCMOS18 [get_ports p_gbtx_loc_reset_inout]
#set_property IOSTANDARD LVCMOS18 [get_ports p_gbtx_rem_reset_inout]
#set_property PACKAGE_PIN AE18 [get_ports p_gbtx_loc_reset_inout]
#set_property PACKAGE_PIN M22 [get_ports p_gbtx_rem_reset_inout]

#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_rxready_in[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_rxready_in[1]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_datavalid_in[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_datavalid_in[1]}]
#set_property PACKAGE_PIN T19 [get_ports {p_gbtx_rxready_in[0]}]
#set_property PACKAGE_PIN B16 [get_ports {p_gbtx_rxready_in[1]}]
#set_property PACKAGE_PIN G19 [get_ports {p_gbtx_datavalid_in[0]}]
#set_property PACKAGE_PIN G22 [get_ports {p_gbtx_datavalid_in[1]}]


##jtag remote tdo
#set_property IOSTANDARD LVCMOS18 [get_ports p_tdo_remote_in]
#set_property PACKAGE_PIN E22 [get_ports p_tdo_remote_in]

##serial id i2c interface
#set_property IOSTANDARD LVCMOS18 [get_ports p_serial_id_sda_inout]
#set_property IOSTANDARD LVCMOS18 [get_ports p_serial_id_scl_inout]
#set_property PACKAGE_PIN M24 [get_ports p_serial_id_scl_inout]
#set_property PACKAGE_PIN N26 [get_ports p_serial_id_sda_inout]

##daughterboard side
#set_property IOSTANDARD LVCMOS18 [get_ports {p_db_side_in[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_db_side_in[1]}]
#set_property PACKAGE_PIN W21 [get_ports {p_db_side_in[1]}]
#set_property PACKAGE_PIN AA23 [get_ports {p_db_side_in[0]}]
#set_property PACKAGE_PIN P20 [get_ports {p_configbus_loc_p_in[0]}]
#set_property PACKAGE_PIN AB17 [get_ports {p_configbus_loc_p_in[3]}]
#set_property PACKAGE_PIN Y17 [get_ports {p_configbus_loc_p_in[4]}]
#set_property PACKAGE_PIN U19 [get_ports {p_configbus_loc_p_in[5]}]
#set_property PACKAGE_PIN R20 [get_ports {p_configbus_loc_p_in[6]}]
#set_property PACKAGE_PIN Y18 [get_ports {p_configbus_loc_p_in[7]}]
#set_property PACKAGE_PIN L18 [get_ports {p_configbus_rem_p_in[0]}]
#set_property PACKAGE_PIN K21 [get_ports {p_configbus_rem_p_in[1]}]
#set_property PACKAGE_PIN G15 [get_ports {p_configbus_rem_p_in[2]}]
#set_property PACKAGE_PIN D23 [get_ports {p_configbus_rem_p_in[3]}]
#set_property PACKAGE_PIN H21 [get_ports {p_configbus_rem_p_in[4]}]
#set_property PACKAGE_PIN F23 [get_ports {p_configbus_rem_p_in[5]}]
#set_property PACKAGE_PIN J19 [get_ports {p_configbus_rem_p_in[6]}]
#set_property PACKAGE_PIN D24 [get_ports {p_configbus_rem_p_in[7]}]
#set_property PACKAGE_PIN N19 [get_ports {p_configbus_loc_p_in[1]}]
#set_property PACKAGE_PIN N21 [get_ports {p_configbus_loc_p_in[2]}]

##power good monitoring
#set_property IOSTANDARD LVCMOS33 [get_ports {p_pgood_in[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_pgood_in[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_pgood_in[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_pgood_in[0]}]

#set_property PACKAGE_PIN AF14 [get_ports {p_pgood_in[3]}]
#set_property PACKAGE_PIN Y16 [get_ports {p_pgood_in[2]}]
#set_property PACKAGE_PIN W16 [get_ports {p_pgood_in[1]}]
#set_property PACKAGE_PIN W15 [get_ports {p_pgood_in[0]}]


##mainboard jtag chain
#set_property IOSTANDARD LVCMOS25 [get_ports p_mb_tms_out]
#set_property IOSTANDARD LVCMOS25 [get_ports p_mb_tck_out]
#set_property IOSTANDARD LVCMOS25 [get_ports p_mb_tdi_out]
#set_property IOSTANDARD LVCMOS25 [get_ports p_mb_tdo_in]

#set_property PACKAGE_PIN A13 [get_ports p_mb_tms_out]
#set_property PACKAGE_PIN A12 [get_ports p_mb_tck_out]
#set_property PACKAGE_PIN B14 [get_ports p_mb_tdi_out]
#set_property PACKAGE_PIN A14 [get_ports p_mb_tdo_in]

#set_property PACKAGE_PIN C12 [get_ports {p_mb_fpga_reset_low[0]}]
#set_property PACKAGE_PIN B12 [get_ports {p_mb_fpga_reset_low[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {p_mb_fpga_reset_low[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {p_mb_fpga_reset_low[1]}]


##sfp/gth
#set_property PACKAGE_PIN J5 [get_ports {p_tx_sfp_p_out[0]}]
#set_property PACKAGE_PIN J4 [get_ports {p_tx_sfp_n_out[0]}]
#set_property PACKAGE_PIN G5 [get_ports {p_tx_sfp_p_out[1]}]
#set_property PACKAGE_PIN G4 [get_ports {p_tx_sfp_n_out[1]}]

#set_property PACKAGE_PIN AB14 [get_ports p_sfp_loc_tx_dis_out]
#set_property PACKAGE_PIN W13 [get_ports p_sfp_rem_tx_dis_out]
##set_property PACKAGE_PIN AE13 [get_ports p_sfp_1_mod_abs_in]
##set_property PACKAGE_PIN Y13 [get_ports p_sfp_2_mod_abs_in]
##set_property PACKAGE_PIN AF13 [get_ports p_sfp_1_los_in]
#set_property IOSTANDARD LVCMOS33 [get_ports p_sfp_loc_tx_dis_out]
#set_property IOSTANDARD LVCMOS33 [get_ports p_sfp_rem_tx_dis_out]
##set_property IOSTANDARD LVCMOS33 [get_ports p_sfp_1_mod_abs_in]
##set_property IOSTANDARD LVCMOS33 [get_ports p_sfp_2_mod_abs_in]
##set_property IOSTANDARD LVCMOS33 [get_ports p_sfp_1_los_in]

#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfpabs_in[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfpabs_in[1]}]
#set_property PACKAGE_PIN AE13 [get_ports {p_sfpabs_in[0]}]
#set_property PACKAGE_PIN Y13 [get_ports {p_sfpabs_in[1]}]




##
## adc pin standards
##
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_n_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_p_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_n_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_p_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_n_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_p_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_n_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_p_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_n_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_p_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_n_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data0_p_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_n_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_p_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_n_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_p_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_n_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_p_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_n_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_p_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_n_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_p_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_n_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_p_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_n_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_p_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_n_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_p_in[2]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_n_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_p_in[3]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_n_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_p_in[4]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_n_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_frameclk_p_in[5]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_n_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_adc_data1_p_in[0]}]
##
## adc pin locations
##
#set_property PACKAGE_PIN T25 [get_ports {p_adc_bitclk_p_in[5]}]
##bank 65
#set_property PACKAGE_PIN AD21 [get_ports {p_adc_bitclk_p_in[4]}]
##bank 64
#set_property PACKAGE_PIN G24 [get_ports {p_adc_bitclk_p_in[3]}]
##bank 66
#set_property PACKAGE_PIN AB21 [get_ports {p_adc_bitclk_p_in[2]}]
##bank 64
#set_property PACKAGE_PIN B19 [get_ports {p_adc_bitclk_p_in[1]}]
##bank 64
#set_property PACKAGE_PIN V24 [get_ports {p_adc_bitclk_p_in[0]}]
##bank 65
#set_property PACKAGE_PIN L20 [get_ports {p_adc_data0_p_in[5]}]
##bank 66
#set_property PACKAGE_PIN J25 [get_ports {p_adc_data0_p_in[4]}]
##bank 66
#set_property PACKAGE_PIN L24 [get_ports {p_adc_data0_p_in[3]}]
##bank 66
#set_property PACKAGE_PIN B25 [get_ports {p_adc_data0_p_in[2]}]
##bank 66
#set_property PACKAGE_PIN AA24 [get_ports {p_adc_data0_p_in[1]}]
##bank 65
#set_property PACKAGE_PIN K25 [get_ports {p_adc_data0_p_in[0]}]
##bank 66
#set_property PACKAGE_PIN AC22 [get_ports {p_adc_data1_p_in[5]}]
##bank 64
#set_property PACKAGE_PIN AF24 [get_ports {p_adc_data1_p_in[4]}]
##bank 64
#set_property PACKAGE_PIN AD23 [get_ports {p_adc_data1_p_in[3]}]
##bank 64
#set_property PACKAGE_PIN AE17 [get_ports {p_adc_data1_p_in[2]}]
##bank 64
#set_property PACKAGE_PIN AD16 [get_ports {p_adc_data1_p_in[1]}]
##bank 64
#set_property PACKAGE_PIN M25 [get_ports {p_adc_data1_p_in[0]}]
##bank 66
##bank 64
##bank 66
##bank 64
##bank 64
##bank 66
##bank 66

##set_property iostandard lvds [get_ports i2c_busy_loc_n]
##set_property iostandard lvds [get_ports i2c_busy_loc_p]
##set_property iostandard lvds [get_ports i2c_busy_rem_n]
##set_property iostandard lvds [get_ports i2c_busy_rem_p]

##set_property iostandard lvcmos25 [get_ports spi_miso_n]
##set_property iostandard lvcmos25 [get_ports spi_miso_p]
##set_property iostandard lvcmos25 [get_ports spi_mosi_n]
##set_property iostandard lvcmos25 [get_ports spi_mosi_p]
##set_property iostandard lvcmos25 [get_ports spi_sclk_p]
##set_property iostandard lvcmos25 [get_ports spi_sclk_n]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_n[0]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_n[1]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_n[2]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_n[3]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_n[4]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_n[5]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_p[0]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_p[1]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_p[2]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_p[3]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_p[4]}]
##set_property iostandard lvcmos25 [get_ports {spi_ssn_p[5]}]

##set_property iostandard lvcmos33 [get_ports {fmc_ebus_i2c_scl[0]}]
##set_property iostandard lvcmos33 [get_ports {fmc_ebus_i2c_scl[1]}]
##set_property iostandard lvcmos33 [get_ports {fmc_ebus_i2c_sda[0]}]
##set_property iostandard lvcmos33 [get_ports {fmc_ebus_i2c_sda[1]}]

##bypiro
#set_property IOSTANDARD LVCMOS25 [get_ports {hv_enable[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {hv_enable[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {hv_enable[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {hv_enable[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {hv_enable[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {hv_enable[5]}]


##set_property package_pin ad15 [get_ports {fmc_ebus_i2c_scl[0]}]
##set_property package_pin af15 [get_ports {fmc_ebus_i2c_scl[1]}]
##set_property package_pin ad14 [get_ports {fmc_ebus_i2c_sda[0]}]
##set_property package_pin ae15 [get_ports {fmc_ebus_i2c_sda[1]}]

#set_property PACKAGE_PIN B9 [get_ports {hv_enable[0]}]
#set_property PACKAGE_PIN A9 [get_ports {hv_enable[1]}]
#set_property PACKAGE_PIN B10 [get_ports {hv_enable[2]}]
#set_property PACKAGE_PIN A10 [get_ports {hv_enable[3]}]
#set_property PACKAGE_PIN C11 [get_ports {hv_enable[4]}]
#set_property PACKAGE_PIN B11 [get_ports {hv_enable[5]}]


#set_property PACKAGE_PIN AD24 [get_ports {p_tph_p_out[0]}]
#set_property PACKAGE_PIN N23 [get_ports {p_tph_p_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_n_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_p_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_n_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_p_out[1]}]

##set_property package_pin k9 [get_ports {spi_ssn_n[0]}]
##set_property package_pin j10 [get_ports {spi_ssn_n[1]}]
##set_property package_pin h9 [get_ports {spi_ssn_n[2]}]
##set_property package_pin g11 [get_ports {spi_ssn_n[3]}]
##set_property package_pin g9 [get_ports {spi_ssn_n[4]}]
##set_property package_pin f9 [get_ports {spi_ssn_n[5]}]
##set_property package_pin k10 [get_ports {spi_ssn_p[0]}]
##set_property package_pin j11 [get_ports {spi_ssn_p[1]}]
##set_property package_pin j9 [get_ports {spi_ssn_p[2]}]
##set_property package_pin h11 [get_ports {spi_ssn_p[3]}]
##set_property package_pin g10 [get_ports {spi_ssn_p[4]}]
##set_property package_pin f10 [get_ports {spi_ssn_p[5]}]
## set_property package_pin d10 [get_ports spi_miso_n]
##set_property package_pin d11 [get_ports spi_miso_p]
##set_property package_pin c9 [get_ports spi_mosi_n]
##set_property package_pin d9 [get_ports spi_mosi_p]
##set_property package_pin e10 [get_ports spi_sclk_n]
##set_property package_pin e11 [get_ports spi_sclk_p]
##set_property package_pin g17 [get_ports i2c_busy_loc_n]
##set_property package_pin h17 [get_ports i2c_busy_loc_p]
##set_property package_pin p24 [get_ports i2c_busy_rem_n]
##set_property package_pin n24 [get_ports i2c_busy_rem_p]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_n_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_p_out[0]}]
#set_property PACKAGE_PIN T20 [get_ports {p_ssel_p_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_n_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_p_out[1]}]
#set_property PACKAGE_PIN W19 [get_ports {p_ssel_p_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_sclk_n_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_sclk_p_out[0]}]
#set_property PACKAGE_PIN Y22 [get_ports {p_sclk_p_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_sclk_n_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_sclk_p_out[1]}]
#set_property PACKAGE_PIN T22 [get_ports {p_sclk_p_out[1]}]

##
##sdata
##
#set_property PACKAGE_PIN H26 [get_ports {p_sdata_p_out[0]}]
#set_property PACKAGE_PIN A24 [get_ports {p_sdata_p_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_n_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_p_in[0]}]
#set_property PACKAGE_PIN AC26 [get_ports {p_sdata_p_in[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_n_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_p_in[1]}]
#set_property PACKAGE_PIN Y25 [get_ports {p_sdata_p_in[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_n_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_p_out[0]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_n_out[1]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_p_out[1]}]
##
##ttc clocks
##
#set_property PACKAGE_PIN G12 [get_ports {p_ttc_clk_p_out[0]}]
#set_property PACKAGE_PIN F12 [get_ports {p_ttc_clk_n_out[0]}]
#set_property PACKAGE_PIN F14 [get_ports {p_ttc_clk_p_out[1]}]
#set_property PACKAGE_PIN F13 [get_ports {p_ttc_clk_n_out[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {p_ttc_clk_n_out[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {p_ttc_clk_p_out[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {p_ttc_clk_n_out[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {p_ttc_clk_p_out[1]}]
##set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports {p_ttc_clk_n_out[0]}]
##set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports {p_ttc_clk_p_out[0]}]
##set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports {p_ttc_clk_n_out[1]}]
##set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports {p_ttc_clk_p_out[1]}]
##set_property SLEW FAST [get_ports {p_ttc_clk_p_out[1]}]
##set_property SLEW FAST [get_ports {p_ttc_clk_p_out[0]}]

##
##cs interface
##
#set_property IOSTANDARD LVDS_25 [get_ports p_cs_mosi_p_in]
#set_property IOSTANDARD LVDS_25 [get_ports p_cs_sclk_p_in]
#set_property IOSTANDARD LVCMOS25 [get_ports p_cs_nreq_n_out]
#set_property IOSTANDARD LVCMOS25 [get_ports p_cs_nreq_p_out]
#set_property IOSTANDARD LVCMOS25 [get_ports p_cs_miso_n_out]
#set_property IOSTANDARD LVCMOS25 [get_ports p_cs_miso_p_out]
#set_property PACKAGE_PIN H12 [get_ports p_cs_nreq_n_out]
#set_property PACKAGE_PIN J12 [get_ports p_cs_nreq_p_out]
#set_property PACKAGE_PIN H13 [get_ports p_cs_miso_n_out]
#set_property PACKAGE_PIN J13 [get_ports p_cs_miso_p_out]
#set_property PACKAGE_PIN H14 [get_ports p_cs_mosi_p_in]
#set_property PACKAGE_PIN J15 [get_ports p_cs_sclk_p_in]





## adc bit clock inputs (280 mhz)

## decouple adc bit clock domain from local clocks
##set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[0]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db*]
##set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[1]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db*]
##set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[2]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db*]
##set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[3]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db*]
##set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_p_in[5]}] -group [get_clocks p_clk40_out_mmcm_gbt40_db*]

## set input delays for inputs associate with each bit clock


## Fix timing violations between bit clocks and 40 MHz DB clock

## power-up reset signals from gbt block

##set_false_path -rise -from [get_clocks {clk_40_mmcm_gbt40_loc_5}] -to [get_clocks {clk_100_mmcm_osc}]

### arbitrary phase shift from gearbox input to 120 mhz gth user clock

##set_false_path -from [get_pins {*/*/tx_frame_240_reg[*]/c}]

### asynchronous gth reference clock driven outputs (for monitoring)

##set_multicycle_path -hold -from [get_pins {*/*/tx_gearbox_0/phase_aligned_reg/c}] 3
##set_multicycle_path -hold -from [get_pins i_db_system/*/*/*/*/*/*/reset_synchronizer_tx_done_inst/rst_in_out_reg/c] 3


## debug core

##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[0]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[1]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[2]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[3]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[4]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[5]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[6]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_remote_out[7]}]

##set_property PACKAGE_PIN E10 [get_ports {p_gbtx_config_bus_remote_out[0]}]
##set_property PACKAGE_PIN D10 [get_ports {p_gbtx_config_bus_remote_out[1]}]
##set_property PACKAGE_PIN C9 [get_ports {p_gbtx_config_bus_remote_out[2]}]
##set_property PACKAGE_PIN K9 [get_ports {p_gbtx_config_bus_remote_out[3]}]
##set_property PACKAGE_PIN J10 [get_ports {p_gbtx_config_bus_remote_out[4]}]
##set_property PACKAGE_PIN H9 [get_ports {p_gbtx_config_bus_remote_out[5]}]
##set_property PACKAGE_PIN G11 [get_ports {p_gbtx_config_bus_remote_out[6]}]
##set_property PACKAGE_PIN G9 [get_ports {p_gbtx_config_bus_remote_out[7]}]

##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_remote_out[0]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_remote_out[1]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_remote_out[2]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_remote_out[3]}]

##set_property PACKAGE_PIN F9 [get_ports {p_gbtx_clks_remote_out[0]}]
##set_property PACKAGE_PIN B9 [get_ports {p_gbtx_clks_remote_out[1]}]
##set_property PACKAGE_PIN B10 [get_ports {p_gbtx_clks_remote_out[2]}]
##set_property PACKAGE_PIN C11 [get_ports {p_gbtx_clks_remote_out[3]}]


##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[0]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[1]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[2]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[3]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[4]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[5]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[6]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_config_bus_local_out[7]}]

##set_property PACKAGE_PIN E11 [get_ports {p_gbtx_config_bus_local_out[0]}]
##set_property PACKAGE_PIN D11 [get_ports {p_gbtx_config_bus_local_out[1]}]
##set_property PACKAGE_PIN D9 [get_ports {p_gbtx_config_bus_local_out[2]}]
##set_property PACKAGE_PIN K10 [get_ports {p_gbtx_config_bus_local_out[3]}]
##set_property PACKAGE_PIN J11 [get_ports {p_gbtx_config_bus_local_out[4]}]
##set_property PACKAGE_PIN J9 [get_ports {p_gbtx_config_bus_local_out[5]}]
##set_property PACKAGE_PIN H11 [get_ports {p_gbtx_config_bus_local_out[6]}]
##set_property PACKAGE_PIN G10 [get_ports {p_gbtx_config_bus_local_out[7]}]

##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_local_out[0]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_local_out[1]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_local_out[2]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {p_gbtx_clks_local_out[3]}]

##set_property PACKAGE_PIN F10 [get_ports {p_gbtx_clks_local_out[0]}]
##set_property PACKAGE_PIN A9 [get_ports {p_gbtx_clks_local_out[1]}]
##set_property PACKAGE_PIN A10 [get_ports {p_gbtx_clks_local_out[2]}]
##set_property PACKAGE_PIN B11 [get_ports {p_gbtx_clks_local_out[3]}]

##set_property IOSTANDARD lvds [get_ports p_fpga_commbus_txn_out]
##set_property IOSTANDARD lvds [get_ports p_fpga_commbus_txp_out]
##set_property IOSTANDARD lvds [get_ports p_fpga_commbus_rxn_in]
##set_property IOSTANDARD lvds [get_ports p_fpga_commbus_rxp_in]

##set_property PACKAGE_PIN G17 [get_ports p_fpga_commbus_txn_out]
##set_property PACKAGE_PIN H17 [get_ports p_fpga_commbus_txp_out]
##set_property PACKAGE_PIN P24 [get_ports p_fpga_commbus_rxn_in]
##set_property PACKAGE_PIN N24 [get_ports p_fpga_commbus_rxp_in]

#set_property IOSTANDARD LVCMOS18 [get_ports p_fpga_commbus_clk_out]
#set_property IOSTANDARD LVCMOS18 [get_ports p_fpga_commbus_tx_out]
#set_property IOSTANDARD LVCMOS18 [get_ports p_fpga_commbus_clk_in]
#set_property IOSTANDARD LVCMOS18 [get_ports p_fpga_commbus_rx_in]

#set_property PACKAGE_PIN G17 [get_ports p_fpga_commbus_tx_out]
#set_property PACKAGE_PIN H17 [get_ports p_fpga_commbus_clk_out]
#set_property PACKAGE_PIN P24 [get_ports p_fpga_commbus_rx_in]
#set_property PACKAGE_PIN N24 [get_ports p_fpga_commbus_clk_in]



#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfplos_in[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfplos_in[1]}]
#set_property PACKAGE_PIN AF13 [get_ports {p_sfplos_in[0]}]
#set_property PACKAGE_PIN AA14 [get_ports {p_sfplos_in[1]}]

#set_property PACKAGE_PIN C22 [get_ports {p_tpl_p_out[1]}]
#set_property PACKAGE_PIN E25 [get_ports {p_tpl_p_out[0]}]



###############################################################################################################################
###############################################################################################################################
###############################################################################################################################


## gth reference clocks (160 mhz)
#set_property PACKAGE_PIN M6 [get_ports p_gth_refclk_gbtx_loc_n_in]
set_property PACKAGE_PIN M7 [get_ports p_gth_refclk_gbtx_loc_p_in]

#set_property PACKAGE_PIN P6 [get_ports p_gth_refclk_gbtx_rem_n_in]
set_property PACKAGE_PIN P7 [get_ports p_gth_refclk_gbtx_rem_p_in]

## 
set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_n_in[0]}]
set_property IOSTANDARD LVDS [get_ports {p_adc_bitclk_p_in[0]}]
set_property PACKAGE_PIN D20 [get_ports {p_adc_bitclk_p_in[0]}]
set_property PACKAGE_PIN B20 [get_ports {p_adc_data0_p_in[0]}]
set_property PACKAGE_PIN B21 [get_ports {p_adc_data1_p_in[0]}]
set_property PACKAGE_PIN C21 [get_ports {p_adc_frameclk_p_in[0]}]
set_property PACKAGE_PIN D19 [get_ports {p_adc_bitclk_p_in[1]}]
set_property PACKAGE_PIN B19 [get_ports {p_adc_frameclk_p_in[1]}]
set_property PACKAGE_PIN C16 [get_ports {p_adc_data1_p_in[1]}]
set_property PACKAGE_PIN A17 [get_ports {p_adc_data0_p_in[1]}]
set_property PACKAGE_PIN E20 [get_ports {p_clk40_gbtx_loc_p_in[1]}]

set_property PACKAGE_PIN H24 [get_ports {p_adc_bitclk_p_in[2]}]
set_property PACKAGE_PIN H26 [get_ports {p_adc_data0_p_in[2]}]
set_property PACKAGE_PIN G25 [get_ports {p_adc_data1_p_in[2]}]
set_property PACKAGE_PIN F22 [get_ports {p_adc_frameclk_p_in[2]}]
set_property PACKAGE_PIN H22 [get_ports {p_adc_bitclk_p_in[3]}]
set_property PACKAGE_PIN G21 [get_ports {p_adc_data0_p_in[3]}]
set_property PACKAGE_PIN J21 [get_ports {p_adc_data1_p_in[3]}]
set_property PACKAGE_PIN V22 [get_ports {p_adc_bitclk_p_in[4]}]
set_property PACKAGE_PIN W25 [get_ports {p_adc_data0_p_in[4]}]
set_property PACKAGE_PIN Y25 [get_ports {p_adc_data1_p_in[4]}]
set_property PACKAGE_PIN U26 [get_ports {p_adc_frameclk_p_in[4]}]
set_property PACKAGE_PIN T23 [get_ports {p_adc_bitclk_p_in[5]}]
set_property PACKAGE_PIN P23 [get_ports {p_adc_data0_p_in[5]}]
set_property PACKAGE_PIN P25 [get_ports {p_adc_data1_p_in[5]}]

set_property PACKAGE_PIN J24 [get_ports {p_clk40_gbtx_rem_p_in[0]}]
set_property PACKAGE_PIN J25 [get_ports {p_clk40_gbtx_rem_n_in[0]}]

set_property PACKAGE_PIN T22 [get_ports {p_clk40_gbtx_loc_mb_p_in[1]}]
set_property PACKAGE_PIN R22 [get_ports {p_adc_frameclk_p_in[5]}]
set_property PACKAGE_PIN L22 [get_ports {p_adc_frameclk_p_in[3]}]

set_property PACKAGE_PIN K23 [get_ports {p_clk40_gbtx_loc_p_in[0]}]

set_property PACKAGE_PIN D18 [get_ports {p_clk40_gbtx_loc_mb_p_in[0]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_mb_n_in[0]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_rem_mb_p_in[0]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_clk40_gbtx_loc_mb_n_in[0]}]

set_property PACKAGE_PIN E20 [get_ports {p_clk40_gbtx_rem_mb_p_in[0]}]
set_property PACKAGE_PIN Y17 [get_ports p_osc_p_in]
set_property IOSTANDARD LVDS_25 [get_ports p_osc_p_in]

set_property PACKAGE_PIN E11 [get_ports {p_configbus_clk_rem_p_in[0]}]
set_property PACKAGE_PIN C9 [get_ports {p_configbus_rem_p_in[0]}]
set_property PACKAGE_PIN C11 [get_ports {p_configbus_rem_p_in[1]}]
set_property PACKAGE_PIN E8 [get_ports {p_configbus_rem_p_in[2]}]
set_property PACKAGE_PIN B10 [get_ports {p_configbus_rem_p_in[3]}]
set_property PACKAGE_PIN F12 [get_ports {p_configbus_clk_loc_n_in[0]}]
set_property PACKAGE_PIN H14 [get_ports {p_configbus_loc_p_in[0]}]
set_property PACKAGE_PIN G15 [get_ports {p_configbus_loc_p_in[1]}]
set_property PACKAGE_PIN J13 [get_ports {p_configbus_loc_p_in[2]}]
set_property PACKAGE_PIN F14 [get_ports {p_configbus_loc_p_in[3]}]
set_property PACKAGE_PIN E10 [get_ports {p_configbus_rem_p_in[4]}]
set_property PACKAGE_PIN D10 [get_ports {p_configbus_rem_n_in[4]}]
set_property PACKAGE_PIN E13 [get_ports {p_configbus_loc_p_in[4]}]
set_property PACKAGE_PIN D14 [get_ports {p_configbus_loc_p_in[5]}]
set_property PACKAGE_PIN B15 [get_ports {p_configbus_loc_p_in[6]}]
set_property PACKAGE_PIN D13 [get_ports {p_configbus_loc_p_in[7]}]
set_property PACKAGE_PIN B14 [get_ports {p_configbus_rem_p_in[5]}]
set_property PACKAGE_PIN C12 [get_ports {p_configbus_rem_p_in[6]}]
set_property PACKAGE_PIN A13 [get_ports {p_configbus_rem_p_in[7]}]

set_property PACKAGE_PIN AA4 [get_ports {p_tx_sfp_p_out[0]}]
set_property PACKAGE_PIN W4 [get_ports {p_tx_sfp_p_out[1]}]
