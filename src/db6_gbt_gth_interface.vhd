----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2020 11:33:15 AM
-- Design Name: 
-- Module Name: db6_gbt_gth_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library gbt;
use gbt.all;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity db6_gbt_gth_interface is 
   generic (   
        g_num_gth_links                 : integer := 1                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
   );
  Port (
        p_clknet_in : in t_db_clknet;
        p_master_reset_in : in std_logic;
        p_db_reg_rx_in : in t_db_reg_rx;
        
        --ref_clks
        --p_gth_refclk_gbtx_local_in    : in   t_diff_pair;
        --p_gth_refclk_gbtx_remote_in    : in   t_diff_pair;
        p_gth_txwordclk_out : out  std_logic_vector(g_num_gth_links-1 downto 0);
        p_gth_rxwordclk_out : out  std_logic_vector(g_num_gth_links-1 downto 0);
        p_gth_txoutclkfabric_out         : out std_logic_vector(g_num_gth_links-1 downto 0);
        p_gth_rxoutclkfabric_out         : out std_logic_vector(g_num_gth_links-1 downto 0);
      
        --sfp/gth
--        p_tx_sfp_out         : out  t_mgt_diff_pair;
--        p_rx_sfp_in         : in  t_mgt_diff_pair;
        p_tx_sfp_out  : out t_diff_pair_vector(1 downto 0);
        p_rx_sfp_in  : in t_diff_pair_vector(0 downto 0);
        
        p_tx_gbtx_to_fpga_out : out t_diff_pair_vector(0 downto 0);
        p_rx_gbtx_from_fpga_in  : in t_diff_pair_vector(0 downto 0);
        p_rx_gbtx_tx_in  : in t_diff_pair_vector(0 downto 0);
            
        --tdo from other fpga
        p_tdo_remote_in	            : in	std_logic;
        
        --interfaces
        p_gbt_encoder_interface_out         : out t_gbt_encoder_interface;        
        p_mb_interface_in : in t_mb_interface;
        p_sem_interface_in : in t_sem_interface;
        p_system_management_interface_in : in t_system_management_interface;
        p_gbtx_interface_in : in t_gbtx_interface;
        p_serial_id_interface_in : in t_serial_id_interface
  );
end db6_gbt_gth_interface;

architecture Behavioral of db6_gbt_gth_interface is
attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;

--gbt_bank
signal s_mgt_diff_pair_rx, s_mgt_diff_pair_tx : t_mgt_diff_pair;
signal s_db6_gbt_bank : t_db6_gbt_bank;
--signal s_gbt_tx_data_out : std_logic_vector(115 downto 0);
signal s_gbt_encoder_interface : t_gbt_encoder_interface;
attribute keep of s_db6_gbt_bank, s_gbt_encoder_interface : signal is "true"; 
attribute dont_touch of s_db6_gbt_bank, s_gbt_encoder_interface : signal is "true";

begin

p_gbt_encoder_interface_out<=s_gbt_encoder_interface;

i_db6_gbt_encoder : entity tilecal.db6_gbt_encoder
  port map (
        p_master_reset_in => p_master_reset_in,
		p_clknet_in => p_clknet_in,
        p_db_reg_rx_in => p_db_reg_rx_in,
        --p_gbt_tx_data_out => s_gbt_tx_data_out,
		p_gbt_encoder_interface_out => s_gbt_encoder_interface,
		
		--interfaces
		p_mb_interface_in => p_mb_interface_in,
		p_sem_interface_in => p_sem_interface_in,
		p_tdo_remote_in => p_tdo_remote_in,
		p_system_management_interface_in => p_system_management_interface_in,
		p_gbtx_interface_in => p_gbtx_interface_in,
		p_serial_id_interface_in => p_serial_id_interface_in

		);


s_mgt_diff_pair_rx(0) <= p_rx_sfp_in(0);
s_mgt_diff_pair_rx(1) <= p_rx_gbtx_from_fpga_in(0);
s_mgt_diff_pair_rx(2) <= p_rx_gbtx_tx_in(0);

p_tx_sfp_out(0) <= s_mgt_diff_pair_tx(0);
p_tx_sfp_out(1) <= s_mgt_diff_pair_tx(1);
p_tx_gbtx_to_fpga_out(0) <= s_mgt_diff_pair_tx(2);

--s_db6_gbt_bank.mgt_clk_i <= p_clknet_in.gth_refclk_remote & p_clknet_in.gth_refclk_local;
gen_link_connections: for i in 0 to g_num_gth_links-1 generate

    s_db6_gbt_bank.mgt_devspecific_i.rx_p(i+1) <= s_mgt_diff_pair_rx(i).p;
    s_db6_gbt_bank.mgt_devspecific_i.rx_n(i+1) <= s_mgt_diff_pair_rx(i).n;
    s_mgt_diff_pair_tx(i).p <= s_db6_gbt_bank.mgt_devspecific_o.tx_p(i+1);
    s_mgt_diff_pair_tx(i).n <= s_db6_gbt_bank.mgt_devspecific_o.tx_n(i+1);
--    s_db6_gbt_bank.mgt_devspecific_i.rx_p(2) <= p_rx_sfp_in(1).p;
--    s_db6_gbt_bank.mgt_devspecific_i.rx_n(2) <= p_rx_sfp_in(1).n;
--    p_tx_sfp_out(1).p <= s_db6_gbt_bank.mgt_devspecific_o.tx_p(2);
--    p_tx_sfp_out(1).n <= s_db6_gbt_bank.mgt_devspecific_o.tx_n(2);
    s_db6_gbt_bank.gbt_txframeclk_i(i)<=p_clknet_in.clk40;
    s_db6_gbt_bank.gbt_rxframeclk_i(i)<=p_clknet_in.clk40;

    --formatted data
    s_db6_gbt_bank.gbt_txdata_i(i+1)<= s_gbt_encoder_interface.gbt_tx_data_out(83 downto 0);
    s_db6_gbt_bank.wb_txdata_i(i+1)<=s_gbt_encoder_interface.gbt_tx_data_out(115 downto 84);
    
end generate;

i_db6_gbt_bank : entity gbt.gbt_bank
   generic map (   
        num_links                => g_num_gth_links,          --! num_links: number of links instantiated by the core (altera: up to 6, xilinx: up to 4)
        tx_optimization          => latency_optimized,          --! tx_optimization: latency mode for the tx path (standard or latency_optimized)
        rx_optimization          => latency_optimized,         --! rx_optimization: latency mode for the rx path (standard or latency_optimized)
        tx_encoding              => wide_bus,         --! tx_encoding: encoding scheme for the tx datapath (gbt_frame or wide_bus)
        rx_encoding              => gbt_frame          --! rx_encoding: encoding scheme for the rx datapath (gbt_frame or wide_bus)
   )
   port map(   
        --========--
        -- resets --
        --========--
        mgt_txreset_i => s_db6_gbt_bank.mgt_txreset_i, --! reset the tx path of the transceiver (tx pll is reset with the first link) [clock domain: mgt_clk_i]
        mgt_rxreset_i => s_db6_gbt_bank.mgt_rxreset_i,    --! reset the rx path of the transceiver [clock domain: mgt_clk_i]
        gbt_txreset_i => s_db6_gbt_bank.gbt_txreset_i, --! reset the tx scrambler/encoder and the tx gearbox [clock domain: gbt_txframeclk_i]
        gbt_rxreset_i => s_db6_gbt_bank.gbt_rxreset_i,  --! reset the rx decoder/descrambler and the rx gearbox [clock domain: gbt_rxframeclk_i]
        
        --========--
        -- clocks --     
        --========--
        p_clknet_in                  => p_clknet_in,
        p_txoutclkfabric_out     => p_gth_txoutclkfabric_out,
        p_rxoutclkfabric_out     => p_gth_txoutclkfabric_out,
        --mgt_clk_i => s_db6_gbt_bank.mgt_clk_i,  --! transceiver reference clock
        gbt_txframeclk_i  => s_db6_gbt_bank.gbt_txframeclk_i,  --! tx datapath's clock (40mhz with gbt_txclken_i = '1' or mgt_txwordclk_o with gbt_txclken_i pulsed every 3/6 clock cycles)
        gbt_txclken_i => s_db6_gbt_bank.gbt_txclken_i,  --! rx clock enable signal used when the rx frameclock is different from 40mhz
		gbt_rxframeclk_i  => s_db6_gbt_bank.gbt_rxframeclk_i, --! rx datapath's clock (40mhz syncrhonous with ngt_rxwordclk_o and gbt_rxclken_i = '1' or mgt_rxwordclk_o with gbt_rxclken_i connected to the header flag signal)
        gbt_rxclken_i  => s_db6_gbt_bank.gbt_rxclken_i,  --! rx clock enable signal used when the rx frameclock is different from 40mhz
		mgt_txwordclk_o => s_db6_gbt_bank.mgt_txwordclk_o,  --! tx wordclock from the transceiver (could be used to clock the core with clocking enable)
        mgt_rxwordclk_o  => s_db6_gbt_bank.mgt_rxwordclk_o, --! rx wordclock from the transceiver (could be used to clock the core with clocking enable)
        
        --================--
        -- gbt tx control --
        --================--
        tx_encoding_sel_i => s_db6_gbt_bank.tx_encoding_sel_i, --! select the tx encoding in dynamic mode ('1': gbt / '0': widebus)
        gbt_isdataflag_i => s_db6_gbt_bank.gbt_isdataflag_i, --! enable dataflag (header) [clock domain: gbt_txframeclk_i]
        
        --=================--
        -- gbt tx status   --
        --=================--
        tx_phcomputed_o  => s_db6_gbt_bank.tx_phcomputed_o,--! tx frameclock and tx wordclock alignement is computed (flag)
		tx_phaligned_o  => s_db6_gbt_bank.tx_phaligned_o, --! tx frameclock and tx wordclock are aligned (gearbox is working correctly)
		  
        --================--
        -- gbt rx control --
        --================--
        rx_encoding_sel_i => s_db6_gbt_bank.rx_encoding_sel_i,  --! select the rx encoding in dynamic mode ('1': gbt / '0': widebus)
        
        --=================--
        -- gbt rx status   --
        --=================--
        gbt_rxready_o  => s_db6_gbt_bank.gbt_rxready_o,  --! gbt encoding/scrambler is ready[clock domain: gbt_rxframeclk_i]
        gbt_isdataflag_o  => s_db6_gbt_bank.gbt_isdataflag_o, --! header is data flag [clock domain: gbt_rxframeclk_i]
        gbt_errordetected_o  => s_db6_gbt_bank.gbt_errordetected_o, --! pulsed when error has been corrected by the decoder [clock domain: gbt_rxframeclk_i]
        gbt_errorflag_o  => s_db6_gbt_bank.gbt_errorflag_o,   --! position of high level bits indicate the position of the bits flipped by the decoder [clock domain: gbt_rxframeclk_i]
        
        --================--
        -- mgt control    --
        --================--
        mgt_devspecific_i  => s_db6_gbt_bank.mgt_devspecific_i,  --! device specific record connected to the transceiver, defined into the device specific package
        mgt_rstonbitslipen_i => s_db6_gbt_bank.mgt_rstonbitslipen_i, --! enable of the "reset on even or odd bitslip" state machine. it ensures fix latency with a ui precision [clock domain: mgt_clk_i]
        mgt_rstoneven_i => s_db6_gbt_bank.mgt_rstoneven_i, --! configure the "reset on even or odd bitslip": '1' reset when bitslip is even and '0' when is odd [clock domain: mgt_clk_i]
        
        --=================--
        -- mgt status      --
        --=================--
        mgt_txready_o  => s_db6_gbt_bank.mgt_txready_o, --! transceiver tx's path ready signal [clock domain: mgt_clk_i]
        mgt_rxready_o  => s_db6_gbt_bank.mgt_rxready_o,  --! transceiver rx's path ready signal [clock domain: mgt_clk_i]
        mgt_devspecific_o => s_db6_gbt_bank.mgt_devspecific_o,  --! device specific record connected to the transceiver, defined into the device specific package
        mgt_headerflag_o => s_db6_gbt_bank.mgt_headerflag_o, --! pulsed when the mgt word contains the header [clock domain: mgt_rxwordclk_o]
        mgt_headerlocked_o => s_db6_gbt_bank.mgt_headerlocked_o,   --! asserted when the header is locked
        mgt_rstcnt_o  => s_db6_gbt_bank.mgt_rstcnt_o,   --! number of resets because of a wrong bitslip parity
		  
        --========--
        -- data   --
        --========--
        gbt_txdata_i => s_db6_gbt_bank.gbt_txdata_i,   --! gbt data to be encoded and transmit
        gbt_rxdata_o  => s_db6_gbt_bank.gbt_rxdata_o,  --! gbt data received and decoded
        
        wb_txdata_i => s_db6_gbt_bank.wb_txdata_i,  --! (tx) extra data (32bit) to replace the fec when the widebus encoding scheme is selected
        wb_rxdata_o  => s_db6_gbt_bank.wb_rxdata_o  --! (rx) extra data (32bit) replacing the fec when the widebus encoding scheme is selected
      
   );



end Behavioral;
