----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
--           Fernando Carrio
--           Samuel Silverstein
--           Alberto Valero 
--
-- Create Date: 09/14/2020 02:22:15 AM
-- Design Name: 
-- Module Name: db6_gbt_encoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: this module assembles the gbt words to send to the gbt_wrapper
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

library tilecal;
use tilecal.db6_design_package.all;

--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

-- uncomment the following library declaration if instantiating
-- any xilinx primitives in this code.
library unisim;
use unisim.vcomponents.all;

library gbt;
use gbt.vendor_specific_gbt_bank_package.all;

entity db6_gbt_encoder is
  port (
        p_master_reset_in : std_logic;
		p_clknet_in : in t_db_clknet;
        p_db_reg_rx_in : in t_db_reg_rx;
        --p_gbt_tx_data_out       : out std_logic_vector(115 downto 0);
        p_gbt_encoder_interface_out         : out t_gbt_encoder_interface;
		
		--interfaces
		p_mb_interface_in : in t_mb_interface;
		p_sem_interface_in : in t_sem_interface;
		p_tdo_remote_in	            : in	std_logic;
		p_system_management_interface_in : in t_system_management_interface;
		p_gbtx_interface_in : in t_gbtx_interface;
		p_serial_id_interface_in : in t_serial_id_interface
		);
		

end db6_gbt_encoder;

architecture behavioral of db6_gbt_encoder is

-- slow control data readout
    
--    signal s_sc_data              	: std_logic_vector(31 downto 0):=(others=>'0');
--    signal s_sc_address          	    : std_logic_vector(15 downto 0):=(others=>'0');
--    signal s_sc_tx0, s_sc_tx1         : std_logic_vector(15 downto 0):=(others=>'0');
--    signal s_sc_switch0, s_sc_switch1 : std_logic_vector(1 downto 0) := "00";
--    signal s_gbt_sc_address           : std_logic_vector(15 downto 0);
    
    signal s_adc_data_o_lg 	: std_logic_vector(71 downto 0) := (others => '0');
    signal s_adc_data_o_hg 	: std_logic_vector(71 downto 0) := (others => '0');  
    
    signal s_gbt_encoder_interface : t_gbt_encoder_interface;
      -- fifo
    --signal s_sending_flag			: std_logic:='0';
    --signal s_datavalid				: std_logic:='1';
    --signal s_tdo_from_other_fpga	: std_logic:='0';
    --signal s_bcidlocal 				: std_logic_vector(11 downto 0):=(others=>'0');
    
    attribute keep : string;
    attribute dont_touch : string;
    attribute keep of s_adc_data_o_hg, s_adc_data_o_lg, s_gbt_encoder_interface  : signal is "true";
    attribute dont_touch of s_adc_data_o_hg, s_adc_data_o_lg, s_gbt_encoder_interface  : signal is "true";    

    signal s_db_reg_tx_in      	: t_db_reg_tx;
    
    signal s_gbt_tx_data : t_gbt_tx_data;

    signal s_encoder_fifo : t_fifo_gbt_encoder;

COMPONENT fifo_gbt_encoder
  PORT (
    clk : IN STD_LOGIC;
    srst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(115 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    injectdbiterr : IN STD_LOGIC;
    injectsbiterr : IN STD_LOGIC;
    sleep : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(115 DOWNTO 0);
    full : OUT STD_LOGIC;
    wr_ack : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC;
    sbiterr : OUT STD_LOGIC;
    dbiterr : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT sr_ram_gbt_encoder
  PORT (
    D : IN STD_LOGIC_VECTOR(115 DOWNTO 0);
    CLK : IN STD_LOGIC;
    CE : IN STD_LOGIC;
    SCLR : IN STD_LOGIC;
    SSET : IN STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(115 DOWNTO 0)
  );
END COMPONENT;

    
begin

-- register connections
s_db_reg_tx_in(stb_mb) <= p_mb_interface_in.mb_driver.rxword_out.q1(15 downto 0) & p_mb_interface_in.mb_driver.rxword_out.q0(15 downto 0);
s_db_reg_tx_in(stb_mb_q0) <= "00" & p_mb_interface_in.mb_driver.txword_in(23 downto 12) & p_mb_interface_in.mb_driver.rxword_out.q0;
s_db_reg_tx_in(stb_mb_q1) <= "00" & p_mb_interface_in.mb_driver.txword_in(23 downto 12) & p_mb_interface_in.mb_driver.rxword_out.q1;
s_db_reg_tx_in(stb_db_cfbstrobe) <= p_db_reg_rx_in(cfb_strobe_reg);

--on the sm
--s_db_reg_tx_in(stb_db_xadc)    		<= (others=> '0'); --s_temp_data;--5   -- 32-bit xadc sensor data

s_db_reg_tx_in(stb_sem)(31) <= p_sem_interface_in.status_heartbeat;
s_db_reg_tx_in(stb_sem)(30) <= p_sem_interface_in.status_initialization;
s_db_reg_tx_in(stb_sem)(29) <= p_sem_interface_in.status_observation;
s_db_reg_tx_in(stb_sem)(28) <= p_sem_interface_in.status_correction;
s_db_reg_tx_in(stb_sem)(27) <= p_sem_interface_in.status_classification;
s_db_reg_tx_in(stb_sem)(26) <= p_sem_interface_in.status_injection;
s_db_reg_tx_in(stb_sem)(25) <= p_sem_interface_in.status_diagnostic_scan;
s_db_reg_tx_in(stb_sem)(24) <= p_sem_interface_in.status_detect_only;
s_db_reg_tx_in(stb_sem)(23) <= p_sem_interface_in.status_essential;
s_db_reg_tx_in(stb_sem)(22) <= p_sem_interface_in.status_uncorrectable;
s_db_reg_tx_in(stb_sem)(21) <= p_sem_interface_in.uart_tx; --(others=> '0'); --s_sem_data_out;--6
s_db_reg_tx_in(stb_sem)(20 downto 13) <= p_sem_interface_in.monitor_txdata;
s_db_reg_tx_in(stb_sem)(12) <= p_sem_interface_in.monitor_txwrite;
s_db_reg_tx_in(stb_sem)(11) <= p_sem_interface_in.monitor_txfull;

s_db_reg_tx_in(stb_db_sem_icap)    		<= p_sem_interface_in.icap_out;--(others=> '0');

s_db_reg_tx_in(stb_db_xadc_control)(31)         		<= p_system_management_interface_in.xadc_control.reset_in;--(others=> '0'); --s_hv_status;--8
s_db_reg_tx_in(stb_db_xadc_control)(30)         		<= p_system_management_interface_in.xadc_control.user_temp_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(29)         		<= p_system_management_interface_in.xadc_control.vccint_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(28)         		<= p_system_management_interface_in.xadc_control.vccaux_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(27)         		<= p_system_management_interface_in.xadc_control.user_supply0_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(26)         		<= p_system_management_interface_in.xadc_control.user_supply1_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(25)         		<= p_system_management_interface_in.xadc_control.user_supply2_alarm_out;
--s_db_reg_tx_in(stb_db_xadc_control)(24)         		<= p_system_management_interface_in.xadc_control.user_supply3_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(23)         		<= p_system_management_interface_in.xadc_control.jtaglocked_out;
s_db_reg_tx_in(stb_db_xadc_control)(22)         		<= p_system_management_interface_in.xadc_control.jtagmodified_out;
s_db_reg_tx_in(stb_db_xadc_control)(21)         		<= p_system_management_interface_in.xadc_control.jtagbusy_out;

s_db_reg_tx_in(stb_gbtxa_reg)(31 downto 30)                   <=p_gbtx_interface_in.gbtx_side;
s_db_reg_tx_in(stb_gbtxa_reg)(29)                   <=p_gbtx_interface_in.busy;
s_db_reg_tx_in(stb_gbtxa_reg)(28 downto 27)                   <=p_clknet_in.gbtx_rxready;
s_db_reg_tx_in(stb_gbtxa_reg)(26 downto 25)                   <=p_clknet_in.gbtx_datavalid;
s_db_reg_tx_in(stb_gbtxa_reg)(7 downto 0)                   <=p_gbtx_interface_in.gbtxa_dpram.doutb;
s_db_reg_tx_in(stb_gbtxa_reg)(16 downto 8)                   <=p_gbtx_interface_in.gbtxa_dpram.addrb;

s_db_reg_tx_in(stb_gbtxb_reg)(31 downto 30)                   <=p_gbtx_interface_in.gbtx_side;
s_db_reg_tx_in(stb_gbtxb_reg)(29)                   <=p_gbtx_interface_in.busy;
s_db_reg_tx_in(stb_gbtxb_reg)(28 downto 27)                   <=p_clknet_in.gbtx_rxready;
s_db_reg_tx_in(stb_gbtxb_reg)(26 downto 25)                   <=p_clknet_in.gbtx_datavalid;
s_db_reg_tx_in(stb_gbtxb_reg)(7 downto 0)                   <=p_gbtx_interface_in.gbtxb_dpram.doutb;
s_db_reg_tx_in(stb_gbtxb_reg)(16 downto 8)                   <=p_gbtx_interface_in.gbtxb_dpram.addrb;

s_db_reg_tx_in(stb_cis_config)    	<= p_db_reg_rx_in(cfb_cis_config); --(others=> '0'); --s_db_cis_config_reg;									--9
-- cs
s_db_reg_tx_in(stb_cs_t)       <= (others=> '0'); --s_cs_timestamp;
s_db_reg_tx_in(stb_cs_s)       <= (others=> '0'); --s_cs_status;

s_db_reg_tx_in(stb_db_serial_id_msb) <= p_serial_id_interface_in.crc_read(7 downto 0) & p_serial_id_interface_in.family_code(7 downto 0) & p_serial_id_interface_in.serial_number(47 downto 32);
s_db_reg_tx_in(stb_db_serial_id_lsb) <= p_serial_id_interface_in.serial_number(31 downto 0);
s_db_reg_tx_in(stb_db_serial_id_status)(15 downto 0) <= p_serial_id_interface_in.crc_calculated(7 downto 0) & p_serial_id_interface_in.crc_read(7 downto 0);
s_db_reg_tx_in(stb_db_serial_id_status)(31) <= p_serial_id_interface_in.busy;
s_db_reg_tx_in(stb_db_serial_id_status)(30) <= p_serial_id_interface_in.crc_ok;

s_gbt_encoder_interface.sc_datavalid 				<= '1';

s_db_reg_tx_in(stb_gbt_encoder)(0) <=s_encoder_fifo.rst;
s_db_reg_tx_in(stb_gbt_encoder)(1) <= s_encoder_fifo.wr_en;
s_db_reg_tx_in(stb_gbt_encoder)(2) <= s_encoder_fifo.injectdbiterr;
s_db_reg_tx_in(stb_gbt_encoder)(3) <= s_encoder_fifo.injectsbiterr;
s_db_reg_tx_in(stb_gbt_encoder)(4) <= s_encoder_fifo.sleep;
s_db_reg_tx_in(stb_gbt_encoder)(5) <= s_encoder_fifo.full;
s_db_reg_tx_in(stb_gbt_encoder)(6) <= s_encoder_fifo.wr_ack;
s_db_reg_tx_in(stb_gbt_encoder)(7) <= s_encoder_fifo.overflow;
s_db_reg_tx_in(stb_gbt_encoder)(8) <= s_encoder_fifo.valid;
s_db_reg_tx_in(stb_gbt_encoder)(9) <= s_encoder_fifo.underflow;
s_db_reg_tx_in(stb_gbt_encoder)(10) <= s_encoder_fifo.sbiterr;
s_db_reg_tx_in(stb_gbt_encoder)(11) <= s_encoder_fifo.wr_rst_busy;
s_db_reg_tx_in(stb_gbt_encoder)(12) <= s_encoder_fifo.rd_rst_busy;

-- reformat low- and high-gain data
gen_adc_reformat : for adc in 0 to 5 generate
    s_adc_data_o_hg(adc*12+11 downto adc*12)  <=  p_mb_interface_in.adc_readout.hg_data(adc)(13 downto 2);
    s_adc_data_o_lg(adc*12+11 downto adc*12)  <=  p_mb_interface_in.adc_readout.lg_data(adc)(13 downto 2);        
end generate; -- gen_adc_reformat


-- assembling data words
s_encoder_fifo.clk <= p_clknet_in.clk80;
s_encoder_fifo.rst <= p_master_reset_in;
s_encoder_fifo.wr_en <= '1';
s_encoder_fifo.rd_en <= '1';
s_encoder_fifo.injectdbiterr <= '0';
s_encoder_fifo.injectsbiterr <= '0';
s_encoder_fifo.sleep <= '0';

p_gbt_encoder_interface_out<=s_gbt_encoder_interface;

s_gbt_encoder_interface.gbt_tx_data_out <= s_encoder_fifo.dout;

--i_fifo_gbt_encoder : fifo_gbt_encoder
--  PORT MAP (
--    clk => s_encoder_fifo.clk,
--    srst => s_encoder_fifo.rst,
--    din => s_encoder_fifo.din,
--    wr_en => s_encoder_fifo.wr_en,
--    rd_en => s_encoder_fifo.rd_en,
--    injectdbiterr => s_encoder_fifo.injectdbiterr,
--    injectsbiterr => s_encoder_fifo.injectsbiterr,
--    sleep => s_encoder_fifo.sleep,
--    dout => s_encoder_fifo.dout,
--    full => s_encoder_fifo.full,
--    wr_ack => s_encoder_fifo.wr_ack,
--    overflow => s_encoder_fifo.overflow,
--    empty => s_encoder_fifo.empty,
--    valid => s_encoder_fifo.valid,
--    underflow => s_encoder_fifo.underflow,
--    sbiterr => s_encoder_fifo.sbiterr,
--    dbiterr => s_encoder_fifo.dbiterr,
--    wr_rst_busy => s_encoder_fifo.wr_rst_busy,
--    rd_rst_busy => s_encoder_fifo.rd_rst_busy
--  );

i_sr_ram_gbt_encoder : sr_ram_gbt_encoder
  PORT MAP (
    D => s_encoder_fifo.din,
    CLK => s_encoder_fifo.clk,
    CE => '1',
    SCLR => s_encoder_fifo.rst ,
    SSET => '0',
    Q => s_encoder_fifo.dout
  );


    proc_db_data_assembler : process(p_clknet_in.clk80)
    variable v_tdo, v_bcr : std_logic :='1';
    variable v_bcr_count : std_logic_vector(31 downto 0);
    variable v_adc_data_o_lg, v_adc_data_o_hg 	: std_logic_vector(71 downto 0); 
    begin
        
        if rising_edge(p_clknet_in.clk80) then

            v_tdo := p_tdo_remote_in;
            v_bcr := p_clknet_in.bcr.bcr;
            v_bcr_count := p_clknet_in.bcr.count;
            v_adc_data_o_lg := s_adc_data_o_lg;
            v_adc_data_o_hg := s_adc_data_o_hg;

            if (p_clknet_in.clk40 = '1') then
                -- assemble lg data
                s_encoder_fifo.din(10 downto 0)    <= tilecal.db6_design_package.tile_link_crc_compute("00" & s_gbt_encoder_interface.sc_switch(0) & s_gbt_encoder_interface.sc_tx(0) & p_mb_interface_in.mb_integrator.gbt & v_tdo & "0" & v_bcr & v_adc_data_o_lg)(10 downto 0);
                s_encoder_fifo.din(15 downto 11)   <= v_bcr_count(4 downto 0);
                s_encoder_fifo.din(87 downto 16)   <= v_adc_data_o_lg;
                s_encoder_fifo.din(88)             <= v_bcr; 
                s_encoder_fifo.din(89)             <= '0';
                s_encoder_fifo.din(90)             <= v_tdo;
                s_encoder_fifo.din(95 downto 91)   <= p_mb_interface_in.mb_integrator.gbt;
                s_encoder_fifo.din(111 downto 96)  <= s_gbt_encoder_interface.sc_tx(0);
                s_encoder_fifo.din(113 downto 112) <= s_gbt_encoder_interface.sc_switch(0);
                s_encoder_fifo.din(115 downto 114) <= "00";--p_adc_mon_in;
                
            else
                --assemble hg data
                s_encoder_fifo.din(10 downto 0)    <= tilecal.db6_design_package.tile_link_crc_compute("00" & s_gbt_encoder_interface.sc_switch(1) & s_gbt_encoder_interface.sc_tx(1) & p_mb_interface_in.mb_integrator.gbt & v_tdo & "1" & v_bcr & v_adc_data_o_hg)(10 downto 0);
                s_encoder_fifo.din(15 downto 11)   <= v_bcr_count(4 downto 0);
                s_encoder_fifo.din(87 downto 16)   <= v_adc_data_o_hg;
                s_encoder_fifo.din(88)             <= v_bcr;  -- bcr
                s_encoder_fifo.din(89)             <= '1';  -- gain
                s_encoder_fifo.din(90)             <= v_tdo;
                s_encoder_fifo.din(95 downto 91)   <= p_mb_interface_in.mb_integrator.gbt;
                s_encoder_fifo.din(111 downto 96)  <= s_gbt_encoder_interface.sc_tx(1);
                s_encoder_fifo.din(113 downto 112) <= s_gbt_encoder_interface.sc_switch(1);
                s_encoder_fifo.din(115 downto 114) <= "00"; --p_adc_mon_in;
            end if;    
        end if;
    end process;
    
    
    
-- mb slow control (sc) logic

 proc_control_logic : process (p_clknet_in.clk40)
    
	variable v_mb0_tube          : std_logic_vector(1 downto 0);
	variable v_mb0_command       : std_logic_vector(3 downto 0);
	variable v_mb1_tube          : std_logic_vector(1 downto 0);
	variable v_mb1_command       : std_logic_vector(3 downto 0);
	constant v_mb0_fpga          : std_logic   := '0';
	constant v_mb1_fpga          : std_logic   := '1';
	variable v_pmtaddr           : std_logic_vector(3 downto 0);
	variable v_cis_t_cmd	     : std_logic;
	variable v_pmtaddr_to_integer : std_logic_vector(3 downto 0) := x"0";	
	
	--xadc
	variable v_xadc_channel : integer range 0 to c_n_db_xadc_channels -1 := 0;
 
  begin
	if rising_edge(p_clknet_in.clk40) then
	    
		s_gbt_encoder_interface.sc_sending 	 <= not s_gbt_encoder_interface.sc_sending;
		if (s_gbt_encoder_interface.sc_datavalid = '1' and s_gbt_encoder_interface.sc_sending = '1') then
			if p_mb_interface_in.mb_driver.done_out.q0 = '1' then
				v_cis_t_cmd			:= (s_db_reg_tx_in(stb_mb_q0)(29));
				v_mb0_tube          	:= (s_db_reg_tx_in(stb_mb_q0)(17 downto 16)); -- this register is shifted
				v_mb0_command       	:= (s_db_reg_tx_in(stb_mb_q0)(15 downto 12));
				v_pmtaddr_to_integer := (p_clknet_in.db_side(1) & v_mb0_fpga & v_mb0_tube);
				v_pmtaddr            	:= c_mb_to_pmt_addr(to_integer(unsigned(v_pmtaddr_to_integer)));
				s_gbt_encoder_interface.sc_data             	<= x"000" & p_clknet_in.db_side(1) & v_mb0_fpga & (s_db_reg_tx_in(stb_mb_q0)(17 downto 0));
				s_gbt_encoder_interface.sc_address          	<= x"0" & "000" & v_cis_t_cmd & v_pmtaddr & c_mb_to_ppr(to_integer(unsigned(v_mb0_command)));
			elsif p_mb_interface_in.mb_driver.done_out.q1 = '1' then
				v_cis_t_cmd			:= (s_db_reg_tx_in(stb_mb_q1)(29));
				v_mb1_tube          	:= (s_db_reg_tx_in(stb_mb_q1)(17 downto 16));
				v_mb1_command       	:= (s_db_reg_tx_in(stb_mb_q1)(15 downto 12));
				v_pmtaddr_to_integer    := (p_clknet_in.db_side(1) & v_mb1_fpga & v_mb1_tube);
				v_pmtaddr           	:= c_mb_to_pmt_addr(to_integer(unsigned(v_pmtaddr_to_integer)));
				s_gbt_encoder_interface.sc_data            	<= x"000" & p_clknet_in.db_side(1) & v_mb1_fpga & (s_db_reg_tx_in(stb_mb_q1)(17 downto 0));
				s_gbt_encoder_interface.sc_address         	<= x"0" & "000" & v_cis_t_cmd & v_pmtaddr & c_mb_to_ppr(to_integer(unsigned(v_mb1_command)));				

			else
		 --###########################################--
		 --## continous write of register contents  ##--
		 --###########################################--
				s_gbt_encoder_interface.sc_address          <= c_db_reg_tx_lut(to_integer(unsigned(s_gbt_encoder_interface.sc_address)));
				
				if to_integer(unsigned(s_gbt_encoder_interface.sc_address)) < c_number_of_gbttx_regs then
				    s_gbt_encoder_interface.sc_address <= std_logic_vector(to_unsigned(to_integer(unsigned(s_gbt_encoder_interface.sc_address)) + 1,16));
				else
				    s_gbt_encoder_interface.sc_address <= x"0000";
				end if;
						 
                case s_gbt_encoder_interface.sc_address is
                    when c_db_reg_tx_lut(stb_mb) =>  							--x"001"
                        if (s_db_reg_tx_in(stb_mb) /= x"00000000") then
                            s_gbt_encoder_interface.sc_data <= s_db_reg_tx_in(stb_mb);
                        end if;
                    when c_db_reg_tx_lut(stb_db_xadc) =>
                        if v_xadc_channel < c_n_db_xadc_channels then
                            v_xadc_channel := v_xadc_channel+1;
                        else
                            v_xadc_channel:=0;
                        end if;
                        s_gbt_encoder_interface.sc_data <= "00000000" & c_db_drp_xadc_addresses(v_xadc_channel) & p_system_management_interface_in.xadc_voltages(v_xadc_channel);
                        s_db_reg_tx_in(stb_db_xadc) <= "00000000" & c_db_drp_xadc_addresses(v_xadc_channel) & p_system_management_interface_in.xadc_voltages(v_xadc_channel);
                    
                    when c_db_reg_tx_lut(stb_mb_q0) =>							--x"00c"
                        if ((s_db_reg_tx_in(stb_mb_q0)(11 downto 0)) /= x"000") then
                            s_gbt_encoder_interface.sc_data <= s_db_reg_tx_in(stb_mb_q0);						
                        end if;
                    when c_db_reg_tx_lut(stb_mb_q1) =>							--x"00d"
                        if ((s_db_reg_tx_in(stb_mb_q1)(11 downto 0)) /= x"000") then
                            s_gbt_encoder_interface.sc_data <= s_db_reg_tx_in(stb_mb_q1);						
                        end if;
                    when c_db_reg_tx_lut(stb_db_cfbstrobe) =>  							--x"001"
                        if (s_db_reg_tx_in(stb_db_cfbstrobe) /= x"00000000") then
                            s_gbt_encoder_interface.sc_data <= s_db_reg_tx_in(stb_mb);
                        end if;
                    when others =>
                        s_gbt_encoder_interface.sc_data 			<= s_db_reg_tx_in(to_integer(unsigned(s_gbt_encoder_interface.sc_address)));
                end case;
			end if;
		end if;
	end if;
end process proc_control_logic;


proc_sc_manager: process(p_clknet_in.clk40)
type t_sm_write_state is (st_writing,st_idle);
variable v_sm_write_state : t_sm_write_state := st_idle;
begin
	if rising_edge(p_clknet_in.clk40) then
		case v_sm_write_state is
		 when st_idle =>
			if s_gbt_encoder_interface.sc_sending = '0' then
				s_gbt_encoder_interface.sc_switch(0)   <= "01";
				s_gbt_encoder_interface.sc_switch(1)   <= "10";
				s_gbt_encoder_interface.sc_tx(0)       <= s_gbt_encoder_interface.sc_address;
				s_gbt_encoder_interface.sc_tx(1)       <= s_gbt_encoder_interface.sc_data(31 downto 16);
				--writestate <= "01";
				v_sm_write_state := st_writing;
			else
				s_gbt_encoder_interface.sc_switch(0)   <= "00";
				s_gbt_encoder_interface.sc_switch(1)   <= "00";
				s_gbt_encoder_interface.sc_tx(0)       <= (others=>'0');
				s_gbt_encoder_interface.sc_tx(1)       <= (others=>'0');				
				--writestate <= "00";			
				v_sm_write_state := st_idle;
			end if;
		 when others => -- nominally st_writing
			s_gbt_encoder_interface.sc_switch(0)   <= "11";
			s_gbt_encoder_interface.sc_switch(1)   <= "00";
			s_gbt_encoder_interface.sc_tx(0)       <= s_gbt_encoder_interface.sc_data(15 downto 0);
			s_gbt_encoder_interface.sc_tx(1)       <= (others=>'0');
			--writestate <= "00";
			v_sm_write_state := st_idle;
	   end case;
	end if; -- clock edge
end process;




  
end behavioral;

