----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/02/2020 12:55:38 PM
-- Design Name: 
-- Module Name: db6_sfp_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library gbt;
use gbt.all;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
library tilecal;
use tilecal.db6_design_package.all;


entity db6_sfp_interface is

   generic (   
        g_num_gth_links                 : integer := 1                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
   );

  Port (
        p_clknet_in : in t_db_clknet;
        p_master_reset_in : in std_logic;
        p_db_reg_rx_in : in t_db_reg_rx;
        
        --ref_clks
        --p_gth_refclk_gbtx_local_in    : in   t_diff_pair;
        --p_gth_refclk_gbtx_remote_in    : in   t_diff_pair;
        p_gth_txwordclk_out : out  std_logic_vector(g_num_gth_links-1 downto 0);
        p_gth_rxwordclk_out : out  std_logic_vector(g_num_gth_links-1 downto 0);
        p_gth_txoutclkfabric_out         : out std_logic_vector(g_num_gth_links-1 downto 0);
        p_gth_rxoutclkfabric_out         : out std_logic_vector(g_num_gth_links-1 downto 0);
      
        --sfp/gth
--        p_tx_sfp_out         : out  t_mgt_diff_pair;
--        p_rx_sfp_in         : in  t_mgt_diff_pair;
        p_tx_sfp_out  : out t_diff_pair_vector(1 downto 0);
        p_rx_sfp_in  : in t_diff_pair_vector(0 downto 0);

        p_sfp_abs_in                : in std_logic_vector(1 downto 0);
        p_sfp_los_in                : in std_logic_vector(1 downto 0);
        p_sfp_tx_fault_in                : in std_logic_vector(1 downto 0);
        
        p_tx_gbtx_to_fpga_out : out t_diff_pair_vector(0 downto 0);
        p_rx_gbtx_from_fpga_in  : in t_diff_pair_vector(0 downto 0);
        p_rx_gbtx_tx_in  : in t_diff_pair_vector(0 downto 0);

        p_sfp_i2c_scl_inout 				: inout std_logic_vector(1 downto 0);
        p_sfp_i2c_sda_inout                 : inout std_logic_vector(1 downto 0);
        p_sfp_control_in                       : in t_sfp_control;
        p_sfp_interface_out             : out t_sfp_interface;
            
        --tdo from other fpga
        p_tdo_remote_in	            : in	std_logic;
        
        --interfaces
        p_gbt_encoder_interface_out         : out t_gbt_encoder_interface;        
        p_mb_interface_in : in t_mb_interface;
        p_sem_interface_in : in t_sem_interface;
        p_system_management_interface_in : in t_system_management_interface;
        p_gbtx_interface_in : in t_gbtx_interface;
        p_serial_id_interface_in : in t_serial_id_interface
  );
end db6_sfp_interface;


architecture Behavioral of db6_sfp_interface is

signal s_gbt_encoder_interface : t_gbt_encoder_interface;

begin


i_db6_gbt_gth_interface : entity tilecal.db6_gbt_gth_interface
   generic map (   
        g_num_gth_links                => g_num_gth_links          --! num_links: number of links instantiated by the core (altera: up to 6, xilinx: up to 4)
        )
  port map(
        p_clknet_in => p_clknet_in,
        p_master_reset_in => p_master_reset_in,
        p_db_reg_rx_in => p_db_reg_rx_in,
        p_gbt_encoder_interface_out => s_gbt_encoder_interface,
        
        --refclk inputs
        --p_gth_refclk_gbtx_local_in    => p_gth_refclk_gbtx_local_in,
        --p_gth_refclk_gbtx_remote_in    => p_gth_refclk_gbtx_remote_in,
        p_gth_txwordclk_out => p_gth_txwordclk_out,
        p_gth_rxwordclk_out => p_gth_rxwordclk_out,
        p_gth_txoutclkfabric_out => p_gth_txoutclkfabric_out,
        p_gth_rxoutclkfabric_out => p_gth_rxoutclkfabric_out,
        
        --sfp/gth
        --p_tx_sfp_out         => p_tx_sfp_out,
        --p_rx_sfp_in         => p_rx_sfp_in,
        p_tx_sfp_out  => p_tx_sfp_out,
        p_rx_sfp_in  => p_rx_sfp_in,
        
        p_tx_gbtx_to_fpga_out => p_tx_gbtx_to_fpga_out,
        p_rx_gbtx_from_fpga_in => p_rx_gbtx_from_fpga_in,
        p_rx_gbtx_tx_in => p_rx_gbtx_tx_in,
                
        --tdo from remote fpga
        p_tdo_remote_in => p_tdo_remote_in,
        
        --interfaces
        p_mb_interface_in => p_mb_interface_in,
        p_sem_interface_in => p_sem_interface_in,
        p_system_management_interface_in => p_system_management_interface_in,
        p_gbtx_interface_in => p_gbtx_interface_in,
        p_serial_id_interface_in => p_serial_id_interface_in
  );


i_db6_sfp_i2c_interface : entity tilecal.db6_sfp_i2c_interface
  port map ( 
    p_clknet_in 	=> p_clknet_in,
    p_master_reset_in  => p_master_reset_in,
    p_db_reg_rx_in => p_db_reg_rx_in,
    p_gbt_encoder_interface_in => s_gbt_encoder_interface,
    
    p_sfp_control_in => p_sfp_control_in,
    
    p_sfp_abs_in => p_sfp_abs_in,
    p_sfp_los_in => p_sfp_los_in,
    p_sfp_tx_fault_in => p_sfp_tx_fault_in,
            
    p_sfp_i2c_interface_out => p_sfp_interface_out,
    p_scl_inout => p_sfp_i2c_scl_inout,
    p_sda_inout => p_sfp_i2c_sda_inout,
        
    p_leds_out => open
  
  );


end Behavioral;
