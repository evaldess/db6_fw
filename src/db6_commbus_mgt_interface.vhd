----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/17/2020 11:55:41 PM
-- Design Name: 
-- Module Name: db6_commbus_mgt_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_commbus_mgt_interface is
   generic (   
        g_num_gth_links                 : integer := 1                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
   );
  Port ( 
        p_clknet_in : in t_db_clknet;
        p_master_reset_in : in std_logic;
        p_db_reg_rx_in : in t_db_reg_rx;
        
        p_commbus_gth_tx_out  : out t_diff_pair_vector(1 downto 0);
        p_commbus_gth_rx_in  : in t_diff_pair_vector(1 downto 0);
        p_commbus_gth_loopback_tx_out : out t_diff_pair_vector(0 downto 0);
        p_commbus_gth_loopback_rx_in : in t_diff_pair_vector(0 downto 0);
        
        --tdo from other fpga
        p_tdo_remote_in	            : in	std_logic;
        
        --interfaces
        p_gbt_encoder_interface_out         : out t_gbt_encoder_interface;        
        p_mb_interface_in : in t_mb_interface;
        p_sem_interface_in : in t_sem_interface;
        p_system_management_interface_in : in t_system_management_interface;
        p_gbtx_interface_in : in t_gbtx_interface;
        p_serial_id_interface_in : in t_serial_id_interface
  );
end db6_commbus_mgt_interface;

architecture Behavioral of db6_commbus_mgt_interface is

attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;

COMPONENT xlx_ku_mgt_commbus
  PORT (
    gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
    drpaddr_common_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    drpclk_common_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpdi_common_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drpen_common_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpwe_common_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtgrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtgrefclk1_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk11_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpdo_common_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    drprdy_common_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    cpllrefclksel_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    drpaddr_in : IN STD_LOGIC_VECTOR(26 DOWNTO 0);
    drpclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpdi_in : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    drpen_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpwe_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtgrefclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtrefclk1_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxoutclksel_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    rxsysclksel_in : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    rxusrclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxusrclk2_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    txoutclksel_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    txsysclksel_in : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    txusrclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    txusrclk2_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpdo_out : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    drprdy_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxoutclk_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txoutclk_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
  );
END COMPONENT;

signal s_gbt_encoder_interface : t_gbt_encoder_interface;

signal s_ku_mgt : t_ku_mgt;
signal s_mgt_diff_pair_rx, s_mgt_diff_pair_tx : t_mgt_diff_pair;

--attribute keep of s_ku_mgt : signal is "TRUE";
--attribute dont_touch of s_ku_mgt : signal is "TRUE";

begin


s_ku_mgt.gtwiz_reset_rx_pll_and_datapath_in <= "0";
s_ku_mgt.gtwiz_reset_tx_datapath_in <= "0";
s_ku_mgt.gtwiz_reset_all_in <= "0";
s_ku_mgt.gtwiz_buffbypass_tx_start_user_in <= "0";
s_ku_mgt.gtwiz_buffbypass_rx_start_user_in <= "0";
s_ku_mgt.gtwiz_userclk_tx_active_in(0) <= '1';
s_ku_mgt.gtwiz_userclk_rx_active_in(0) <= '1';
s_ku_mgt.gtwiz_buffbypass_tx_reset_in(0) <= '0';
--s_ku_mgt.gtwiz_buffbypass_tx_error_out
s_ku_mgt.gtwiz_buffbypass_rx_reset_in(0) <= '0';
--s_ku_mgt.gtwiz_buffbypass_rx_error_out(0)
s_ku_mgt.gtwiz_reset_clk_freerun_in(0) <= p_clknet_in.clk40;
s_ku_mgt.gtwiz_reset_tx_pll_and_datapath_in(0) <= '0';
            
s_ku_mgt.gtwiz_reset_rx_datapath_in(0) <= '0';
--s_ku_mgt.gtrefclk01_in(0)                             <= p_clknet_in.gth_refclk_remote(1);
--s_ku_mgt.gtrefclk11_in(0)                             <= p_clknet_in.gth_refclk_local(1);

s_ku_mgt.qpll1refclksel_in                            <= p_clknet_in.qpllclksel;


s_mgt_diff_pair_rx(0) <= p_commbus_gth_rx_in(1);
s_mgt_diff_pair_rx(1) <= p_commbus_gth_loopback_rx_in(0);
s_mgt_diff_pair_rx(2) <= p_commbus_gth_rx_in(0);

p_commbus_gth_tx_out(1) <= s_mgt_diff_pair_tx(0);
p_commbus_gth_loopback_tx_out(0) <= s_mgt_diff_pair_tx(1);
p_commbus_gth_tx_out(0)  <= s_mgt_diff_pair_tx(2);

s_ku_mgt.gtgrefclk0_in(0) <= p_clknet_in.clk160;
s_ku_mgt.gtgrefclk1_in(0) <= p_clknet_in.clk160;

gen_mgt_signals : for c in 0 to g_num_gth_links-1 generate  
--    s_ku_mgt.gtgrefclk_in(c) <= p_clknet_in.clk160;
--    s_ku_mgt.gtgrefclk_in(1) <= p_clknet_in.clk160;
--    s_ku_mgt.gtgrefclk_in(2) <= p_clknet_in.clk160;
    
    --s_ku_mgt.gtrefclk0_in(c)                             <= p_clknet_in.gth_refclk_remote(1);
    --s_ku_mgt.gtrefclk1_in(c)                             <= p_clknet_in.gth_refclk_local(1);
    s_ku_mgt.cpllrefclksel_in(3*(c+1)-1 downto 3*c)                            <= p_clknet_in.cpllclksel;
    s_ku_mgt.txsysclksel_in(2*(c+1)-1 downto 2*c)                              <= p_clknet_in.txsysclksel;
    s_ku_mgt.rxsysclksel_in(2*(c+1)-1 downto 2*c)                              <= p_clknet_in.rxsysclksel;
    s_ku_mgt.txoutclksel_in(3*(c+1)-1 downto 3*c)                              <= p_clknet_in.txoutclksel;
    s_ku_mgt.rxoutclksel_in(3*(c+1)-1 downto 3*c)                              <= p_clknet_in.rxoutclksel;

    s_ku_mgt.gthrxn_in(c) <= s_mgt_diff_pair_rx(c).n;
    s_ku_mgt.gthrxp_in(c) <= s_mgt_diff_pair_rx(c).p;
    s_mgt_diff_pair_tx(c).n <= s_ku_mgt.gthtxn_out(c); 
    s_mgt_diff_pair_tx(c).p <= s_ku_mgt.gthtxp_out(c);

end generate;


i_xlx_ku_mgt_commbus : xlx_ku_mgt_commbus
  PORT MAP (
    gtwiz_userclk_tx_active_in => s_ku_mgt.gtwiz_userclk_tx_active_in,
    gtwiz_userclk_rx_active_in => s_ku_mgt.gtwiz_userclk_rx_active_in,
    gtwiz_buffbypass_tx_reset_in => s_ku_mgt.gtwiz_buffbypass_tx_reset_in,
    gtwiz_buffbypass_tx_start_user_in => s_ku_mgt.gtwiz_buffbypass_tx_start_user_in,
    gtwiz_buffbypass_tx_done_out => s_ku_mgt.gtwiz_buffbypass_tx_done_out,
    gtwiz_buffbypass_tx_error_out => s_ku_mgt.gtwiz_buffbypass_tx_error_out,
    gtwiz_buffbypass_rx_reset_in => s_ku_mgt.gtwiz_buffbypass_rx_reset_in,
    gtwiz_buffbypass_rx_start_user_in => s_ku_mgt.gtwiz_buffbypass_rx_start_user_in,
    gtwiz_buffbypass_rx_done_out => s_ku_mgt.gtwiz_buffbypass_rx_done_out,
    gtwiz_buffbypass_rx_error_out => s_ku_mgt.gtwiz_buffbypass_rx_error_out,
    gtwiz_reset_clk_freerun_in => s_ku_mgt.gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in => s_ku_mgt.gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in => s_ku_mgt.gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in => s_ku_mgt.gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in => s_ku_mgt.gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in => s_ku_mgt.gtwiz_reset_rx_datapath_in,
    gtwiz_reset_rx_cdr_stable_out => s_ku_mgt.gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out => s_ku_mgt.gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out => s_ku_mgt.gtwiz_reset_rx_done_out,
    gtwiz_userdata_tx_in => s_ku_mgt.gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out => s_ku_mgt.gtwiz_userdata_rx_out,
    drpaddr_common_in => s_ku_mgt.drpaddr_common_in,
    drpclk_common_in => s_ku_mgt.drpclk_common_in,
    drpdi_common_in => s_ku_mgt.drpdi_common_in,
    drpen_common_in => s_ku_mgt.drpen_common_in,
    drpwe_common_in => s_ku_mgt.drpwe_common_in,
    gtrefclk01_in(0) => p_clknet_in.gth_refclk_remote(1),--s_ku_mgt.gtrefclk01_in,
    gtrefclk11_in(0) => p_clknet_in.gth_refclk_local(1),--s_ku_mgt.gtrefclk11_in,
    qpll1refclksel_in => s_ku_mgt.qpll1refclksel_in,
    drpdo_common_out => s_ku_mgt.drpdo_common_out,
    drprdy_common_out => s_ku_mgt.drprdy_common_out,
    qpll1fbclklost_out => s_ku_mgt.qpll1fbclklost_out,
    qpll1lock_out => s_ku_mgt.qpll1lock_out,
    qpll1outclk_out => s_ku_mgt.qpll1outclk_out,
    qpll1outrefclk_out => s_ku_mgt.qpll1outrefclk_out,
    qpll1refclklost_out => s_ku_mgt.qpll1refclklost_out,
    cpllrefclksel_in                   => s_ku_mgt.cpllrefclksel_in,
    --gtgrefclk_in => s_ku_mgt.gtgrefclk_in,
    gtgrefclk_in(0) => p_clknet_in.clk160,
    gtgrefclk_in(1) => p_clknet_in.clk160,
    gtgrefclk_in(2) => p_clknet_in.clk160,
    gthrxn_in => s_ku_mgt.gthrxn_in,
    gthrxp_in => s_ku_mgt.gthrxp_in,
    rxusrclk_in => s_ku_mgt.rxusrclk_in,
    rxusrclk2_in => s_ku_mgt.rxusrclk2_in,
    txusrclk_in => s_ku_mgt.txusrclk_in,
    txusrclk2_in => s_ku_mgt.txusrclk2_in,
    gthtxn_out => s_ku_mgt.gthtxn_out,
    gthtxp_out => s_ku_mgt.gthtxp_out,
    --gtrefclk0_in                        => s_ku_mgt.gtrefclk0_in,
    gtrefclk0_in(0) => p_clknet_in.gth_refclk_remote(1),
    gtrefclk0_in(1) => p_clknet_in.gth_refclk_remote(1),
    gtrefclk0_in(2) => p_clknet_in.gth_refclk_remote(1),
    --gtrefclk1_in                        => s_ku_mgt.gtrefclk1_in,
    gtrefclk1_in(0)                        => p_clknet_in.gth_refclk_local(1),
    gtrefclk1_in(1)                        => p_clknet_in.gth_refclk_local(1),
    gtrefclk1_in(2)                        => p_clknet_in.gth_refclk_local(1),
    rxoutclksel_in                      => s_ku_mgt.rxoutclksel_in,
    txoutclksel_in                      => s_ku_mgt.txoutclksel_in,
    rxsysclksel_in                      => s_ku_mgt.rxsysclksel_in,
    txsysclksel_in                      => s_ku_mgt.txsysclksel_in,
    gtpowergood_out => s_ku_mgt.gtpowergood_out,
    rxoutclk_out => s_ku_mgt.rxoutclk_out,
    rxpmaresetdone_out => s_ku_mgt.rxpmaresetdone_out,
    txoutclk_out => s_ku_mgt.txoutclk_out,
    txpmaresetdone_out => s_ku_mgt.txpmaresetdone_out,
    txprgdivresetdone_out => s_ku_mgt.txprgdivresetdone_out,
    
    drpaddr_in                             => s_ku_mgt.drpaddr_in,
    drpclk_in                           => s_ku_mgt.drpclk_in,
    drpdi_in                               => s_ku_mgt.drpdi_in,
    drpen_in                            => s_ku_mgt.drpen_in,
    drpwe_in                            => s_ku_mgt.drpwe_in,
    drpdo_out                              => s_ku_mgt.drpdo_out,
    drprdy_out                          => s_ku_mgt.drprdy_out,
    
    gtgrefclk0_in                       => s_ku_mgt.gtgrefclk0_in,
    gtgrefclk1_in                       => s_ku_mgt.gtgrefclk1_in
    
--    loopback_in                            => s_ku_mgt.loopback_in,
    
--    rxpolarity_in                       => s_ku_mgt.rxpolarity_in,
--    txpolarity_in                       => s_ku_mgt.txpolarity_in,
    
--    rxslide_in                          => s_ku_mgt.rxslide_in,
--    txdiffctrl_in                          => s_ku_mgt.txdiffctrl_in,
--    txmaincursor_in                        => s_ku_mgt.txmaincursor_in,
--    txpostcursor_in                        => s_ku_mgt.txpostcursor_in,
--    txprecursor_in                         => s_ku_mgt.txprecursor_in
    

    );


i_db6_gbt_encoder : entity tilecal.db6_gbt_encoder
  port map (
        p_master_reset_in => p_master_reset_in,
		p_clknet_in => p_clknet_in,
        p_db_reg_rx_in => p_db_reg_rx_in,
        --p_gbt_tx_data_out => s_gbt_tx_data_out,
		p_gbt_encoder_interface_out => s_gbt_encoder_interface,
		
		--interfaces
		p_mb_interface_in => p_mb_interface_in,
		p_sem_interface_in => p_sem_interface_in,
		p_tdo_remote_in => p_tdo_remote_in,
		p_system_management_interface_in => p_system_management_interface_in,
		p_gbtx_interface_in => p_gbtx_interface_in,
		p_serial_id_interface_in => p_serial_id_interface_in

		);



end Behavioral;
