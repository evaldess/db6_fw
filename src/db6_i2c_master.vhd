----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/03/2020 02:15:55 PM
-- Design Name: 
-- Module Name: db6_i2c_master - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
--   HDL CODE IS PROVIDED "AS IS."  DIGI-KEY EXPRESSLY DISCLAIMS ANY
--   WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
--   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
--   PARTICULAR PURPOSE, OR NON-INFRINGEMENT. IN NO EVENT SHALL DIGI-KEY
--   BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL
--   DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR EQUIPMENT, COST OF
--   PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
--   BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
--   ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER SIMILAR COSTS.
--
--   Version History
--   Version 1.0 11/01/2012 Scott Larson
--     Initial Public Release
--   Version 2.0 06/20/2014 Scott Larson
--     Added ability to interface with different slaves in the same transaction
--     Corrected ack_error bug where ack_error went 'Z' instead of '1' on error
--     Corrected timing of when ack_error signal clears
--   Version 2.1 10/21/2014 Scott Larson
--     Replaced gated clock with clock enable
--     Adjusted timing of SCL during start and stop conditions
--   Version 2.2 02/05/2015 Scott Larson
--     Corrected small SDA glitch introduced in version 2.1
-- 
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_i2c_master is
  generic(
    input_clk : integer := 40_000_000; --input clock speed from user logic in hz
    bus_clk   : integer := 400_000);   --speed the i2c bus (scl) will run at in hz
  port(
    p_clknet_in       : in     t_db_clknet;                    --system clock
    p_master_reset_in   : in     std_logic;                    --active low reset
    p_ena_in       : in     std_logic;                    --latch in command
    p_addr_in      : in     std_logic_vector(6 downto 0); --address of target slave
    p_rw_in        : in     std_logic;                    --'0' is write, '1' is read
    p_data_in   : in     std_logic_vector(7 downto 0); --data to write to slave
    p_busy_out      : out    std_logic;                    --indicates transaction in progress
    p_data_out   : out    std_logic_vector(7 downto 0); --data read from slave
    p_ack_error_buffer : buffer std_logic;                    --flag if improper acknowledge from slave
    p_sda_inout       : inout  std_logic;                    --serial data output of i2c bus
    p_scl_inout       : inout  std_logic);                   --serial clock output of i2c bus
end db6_i2c_master;

architecture logic of db6_i2c_master is
  constant c_divider  :  integer := (input_clk/bus_clk)/4; --number of clocks in 1/4 cycle of scl
  type t_machine is(st_ready, st_start, st_command, st_slv_ack1, st_wr, st_rd, st_slv_ack2, st_mstr_ack, st_stop); --needed states
  signal s_reset_n       : std_logic;
  signal s_state         : t_machine;                        --state machine
  signal s_data_clk      : std_logic;                      --data clock for sda
  signal s_data_clk_prev : std_logic;                      --data clock during previous system clock
  signal s_scl_clk       : std_logic;                      --constantly running internal scl
  signal s_scl_ena       : std_logic := '0';               --enables internal scl to output
  signal s_sda_int       : std_logic := '1';               --internal sda
  signal s_sda_ena_n     : std_logic;                      --enables internal sda to output
  signal s_addr_rw       : std_logic_vector(7 downto 0);   --latched in address and read/write
  signal s_data_tx       : std_logic_vector(7 downto 0);   --latched in data to write to slave
  signal s_data_rx       : std_logic_vector(7 downto 0);   --data received from slave
  signal s_bit_cnt       : integer range 0 to 7 := 7;      --tracks bit number in transaction
  signal s_stretch       : std_logic := '0';               --identifies if slave is stretching scl
begin
  s_reset_n<= not p_master_reset_in;
  --generate the timing for the bus clock (scl_clk) and the data clock (data_clk)
  process(p_clknet_in.clk80)
    variable v_count  :  integer range 0 to c_divider*4;  --timing for clock generation
  begin
    if rising_edge(p_clknet_in.clk80) then
        if(s_reset_n = '0') then                --reset asserted
          s_stretch <= '0';
          v_count := 0;
        else
          s_data_clk_prev <= s_data_clk;          --store previous value of data clock
          if(v_count = c_divider*4-1) then        --end of timing cycle
            v_count := 0;                       --reset timer
          elsif(s_stretch = '0') then           --clock stretching from slave not detected
            v_count := v_count + 1;               --continue clock generation timing
          end if;
          if (v_count < (1*c_divider)-1) then   --first 1/4 cycle of clocking
              s_scl_clk <= '0';
              s_data_clk <= '0';
          elsif (v_count > (1*c_divider)) and  (v_count < (2*c_divider)-1) then    --second 1/4 cycle of clocking
              s_scl_clk <= '0';
              s_data_clk <= '1';
          elsif (v_count > (2*c_divider)) and  (v_count < (3*c_divider)-1) then    --third 1/4 cycle of clocking
              s_scl_clk <= '1';                 --release scl
              if(p_scl_inout = '0') then              --detect if slave is stretching clock
                s_stretch <= '1';
              else
                s_stretch <= '0';
              end if;
              s_data_clk <= '1';
          elsif (v_count > (3*c_divider)) and  (v_count < (4*c_divider)-1) then                    --last 1/4 cycle of clocking
              s_scl_clk <= '1';
              s_data_clk <= '0';
          else
              
          end if;
        end if;
    end if;
  end process;

  --state machine and writing to sda during scl low (data_clk rising edge)
  process(p_clknet_in.clk80)
  begin
    if rising_edge(p_clknet_in.clk80) then
    
        if(s_reset_n = '0') then                 --reset asserted
          s_state <= st_ready;                      --return to initial state
          p_busy_out <= '1';                         --indicate not available
          s_scl_ena <= '0';                      --sets scl high impedance
          s_sda_int <= '1';                      --sets sda high impedance
          p_ack_error_buffer <= '0';                    --clear acknowledge error flag
          s_bit_cnt <= 7;                        --restarts data bit counter
          p_data_out <= "00000000";               --clear data read port
        else
          if(s_data_clk = '1' and s_data_clk_prev = '0') then
            case s_state is
              when st_ready =>                      --idle state
                if(p_ena_in = '1') then               --transaction requested
                  p_busy_out <= '1';                   --flag busy
                  s_addr_rw <= p_addr_in & p_rw_in;          --collect requested slave address and command
                  s_data_tx <= p_data_in;            --collect requested data to write
                  s_state <= st_start;                --go to start bit
                else                             --remain idle
                  p_busy_out <= '0';                   --unflag busy
                  s_state <= st_ready;                --remain idle
                end if;
              when st_start =>                      --start bit of transaction
                p_busy_out <= '1';                     --resume busy if continuous mode
                s_sda_int <= s_addr_rw(s_bit_cnt);     --set first address bit to bus
                s_state <= st_command;                --go to command
              when st_command =>                    --address and command byte of transaction
                if(s_bit_cnt = 0) then             --command transmit finished
                  s_sda_int <= '1';                --release sda for slave acknowledge
                  s_bit_cnt <= 7;                  --reset bit counter for "byte" states
                  s_state <= st_slv_ack1;             --go to slave acknowledge (command)
                else                             --next clock cycle of command state
                  s_bit_cnt <= s_bit_cnt - 1;        --keep track of transaction bits
                  s_sda_int <= s_addr_rw(s_bit_cnt-1); --write address/command bit to bus
                  s_state <= st_command;              --continue with command
                end if;
              when st_slv_ack1 =>                   --slave acknowledge bit (command)
                if(s_addr_rw(0) = '0') then        --write command
                  s_sda_int <= s_data_tx(s_bit_cnt);   --write first bit of data
                  s_state <= st_wr;                   --go to write byte
                else                             --read command
                  s_sda_int <= '1';                --release sda from incoming data
                  s_state <= st_rd;                   --go to read byte
                end if;
              when st_wr =>                         --write byte of transaction
                p_busy_out <= '1';                     --resume busy if continuous mode
                if(s_bit_cnt = 0) then             --write byte transmit finished
                  s_sda_int <= '1';                --release sda for slave acknowledge
                  s_bit_cnt <= 7;                  --reset bit counter for "byte" states
                  s_state <= st_slv_ack2;             --go to slave acknowledge (write)
                else                             --next clock cycle of write state
                  s_bit_cnt <= s_bit_cnt - 1;        --keep track of transaction bits
                  s_sda_int <= s_data_tx(s_bit_cnt-1); --write next bit to bus
                  s_state <= st_wr;                   --continue writing
                end if;
              when st_rd =>                         --read byte of transaction
                p_busy_out <= '1';                     --resume busy if continuous mode
                if(s_bit_cnt = 0) then             --read byte receive finished
                  if(p_ena_in = '1' and s_addr_rw = p_addr_in & p_rw_in) then  --continuing with another read at same address
                    s_sda_int <= '0';              --acknowledge the byte has been received
                  else                           --stopping or continuing with a write
                    s_sda_int <= '1';              --send a no-acknowledge (before stop or repeated start)
                  end if;
                  s_bit_cnt <= 7;                  --reset bit counter for "byte" states
                  p_data_out <= s_data_rx;            --output received data
                  s_state <= st_mstr_ack;             --go to master acknowledge
                else                             --next clock cycle of read state
                  s_bit_cnt <= s_bit_cnt - 1;        --keep track of transaction bits
                  s_state <= st_rd;                   --continue reading
                end if;
              when st_slv_ack2 =>                   --slave acknowledge bit (write)
                if(p_ena_in = '1') then               --continue transaction
                  p_busy_out <= '0';                   --continue is accepted
                  s_addr_rw <= p_addr_in & p_rw_in;          --collect requested slave address and command
                  s_data_tx <= p_data_in;            --collect requested data to write
                  if(s_addr_rw = p_addr_in & p_rw_in) then   --continue transaction with another write
                    s_sda_int <= p_data_in(s_bit_cnt); --write first bit of data
                    s_state <= st_wr;                 --go to write byte
                  else                           --continue transaction with a read or new slave
                    s_state <= st_start;              --go to repeated start
                  end if;
                else                             --complete transaction
                  s_state <= st_stop;                 --go to stop bit
                end if;
              when st_mstr_ack =>                   --master acknowledge bit after a read
                if(p_ena_in = '1') then               --continue transaction
                  p_busy_out <= '0';                   --continue is accepted and data received is available on bus
                  s_addr_rw <= p_addr_in & p_rw_in;          --collect requested slave address and command
                  s_data_tx <= p_data_in;            --collect requested data to write
                  if(s_addr_rw = p_addr_in & p_rw_in) then   --continue transaction with another read
                    s_sda_int <= '1';              --release sda from incoming data
                    s_state <= st_rd;                 --go to read byte
                  else                           --continue transaction with a write or new slave
                    s_state <= st_start;              --repeated start
                  end if;    
                else                             --complete transaction
                  s_state <= st_stop;                 --go to stop bit
                end if;
              when st_stop =>                       --stop bit of transaction
                p_busy_out <= '0';                     --unflag busy
                s_state <= st_ready;                  --go to idle state
            end case;    
          elsif(s_data_clk = '0' and s_data_clk_prev = '1') then  --data clock falling edge
            case s_state is
              when st_start =>                  
                if(s_scl_ena = '0') then                  --starting new transaction
                  s_scl_ena <= '1';                       --enable scl output
                  p_ack_error_buffer <= '0';                     --reset acknowledge error output
                end if;
              when st_slv_ack1 =>                          --receiving slave acknowledge (command)
                if(p_sda_inout /= '0' or p_ack_error_buffer = '1') then  --no-acknowledge or previous no-acknowledge
                  p_ack_error_buffer <= '1';                     --set error output if no-acknowledge
                end if;
              when st_rd =>                                --receiving slave data
                s_data_rx(s_bit_cnt) <= p_sda_inout;                --receive current slave data bit
              when st_slv_ack2 =>                          --receiving slave acknowledge (write)
                if(p_sda_inout /= '0' or p_ack_error_buffer = '1') then  --no-acknowledge or previous no-acknowledge
                  p_ack_error_buffer <= '1';                     --set error output if no-acknowledge
                end if;
              when st_stop =>
                s_scl_ena <= '0';                         --disable scl
              when others =>
                null;
            end case;
          end if;
        end if;
    end if;
  end process;  

  --set sda output
  with s_state select
    s_sda_ena_n <= s_data_clk_prev when st_start,     --generate start condition
                 not s_data_clk_prev when st_stop,  --generate stop condition
                 s_sda_int when others;          --set to internal sda signal    
      
  --set scl and sda outputs
  p_scl_inout <= '0' when (s_scl_ena = '1' and s_scl_clk = '0') else 'Z';
  p_sda_inout <= '0' when s_sda_ena_n = '0' else 'Z';
  
end logic;

