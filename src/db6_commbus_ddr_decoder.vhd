----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
-- 
-- Create Date: 06/24/2020 12:43:56 PM
-- Design Name: 
-- Module Name: db6_commbus_ddr_encoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

library tilecal;
use tilecal.db6_design_package.all;

--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

-- uncomment the following library declaration if instantiating
-- any xilinx primitives in this code.
library unisim;
use unisim.vcomponents.all;

--library gbt;
--use gbt.vendor_specific_gbt_bank_package.all;

entity db6_commbus_ddr_decoder is
  port (
        p_master_reset_in : std_logic;
		p_clknet_in : in t_db_clknet;
        p_db_reg_rx_in : in t_db_reg_rx;
        --p_gbt_tx_data_out       : out std_logic_vector(115 downto 0);

		--interfaces
		p_commbus_ddr_in : in t_commbus_ddr;
		p_commbus_decoder_interface_out : out t_commbus_decoder_interface;
		p_clknet_out : out t_db_clknet;
		p_db_reg_rx_out : out t_db_reg_rx;
		p_mb_interface_out : out t_mb_interface;
		p_sem_interface_out : out t_sem_interface;
		p_tdo_remote_out	            : out	std_logic;
		p_system_management_interface_out : out t_system_management_interface;
		p_gbtx_interface_out : out t_gbtx_interface;
		p_serial_id_interface_out : out t_serial_id_interface
		);
		

end db6_commbus_ddr_decoder;

architecture behavioral of db6_commbus_ddr_decoder is

 
    attribute keep : string;
    attribute dont_touch : string;

    signal s_db_reg_tx_in      	: t_db_reg_tx;
    signal s_db_reg_rx_out      	: t_db_reg_rx;
    signal s_encoder_fifo : t_fifo_commbus_encoder;

    attribute keep of s_db_reg_tx_in, s_db_reg_rx_out : signal is "true";
    attribute dont_touch of s_db_reg_tx_in, s_db_reg_rx_out : signal is "true";    



--COMPONENT fifo_commbus
--  PORT (
--    clk : IN STD_LOGIC;
--    srst : IN STD_LOGIC;
--    din : IN STD_LOGIC_VECTOR(115 DOWNTO 0);
--    wr_en : IN STD_LOGIC;
--    rd_en : IN STD_LOGIC;
--    injectdbiterr : IN STD_LOGIC;
--    injectsbiterr : IN STD_LOGIC;
--    sleep : IN STD_LOGIC;
--    dout : OUT STD_LOGIC_VECTOR(115 DOWNTO 0);
--    full : OUT STD_LOGIC;
--    wr_ack : OUT STD_LOGIC;
--    overflow : OUT STD_LOGIC;
--    empty : OUT STD_LOGIC;
--    valid : OUT STD_LOGIC;
--    underflow : OUT STD_LOGIC;
--    sbiterr : OUT STD_LOGIC;
--    dbiterr : OUT STD_LOGIC;
--    wr_rst_busy : OUT STD_LOGIC;
--    rd_rst_busy : OUT STD_LOGIC
--  );
--END COMPONENT;

--COMPONENT sr_ram_commbus
--  PORT (
--    D : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
--    CLK : IN STD_LOGIC;
--    CE : IN STD_LOGIC;
--    SCLR : IN STD_LOGIC;
--    SSET : IN STD_LOGIC;
--    Q : OUT STD_LOGIC_VECTOR(119 DOWNTO 0)
--  );
--END COMPONENT;

    
begin

-- register connections
p_db_reg_rx_out <= s_db_reg_rx_out;

p_mb_interface_out.mb_driver.rxword_out.q1(15 downto 0) <= s_db_reg_tx_in(stb_mb)(31 downto 16);
p_mb_interface_out.mb_driver.rxword_out.q1(17 downto 16) <= "00";
p_mb_interface_out.mb_driver.rxword_out.q0(15 downto 0) <= s_db_reg_tx_in(stb_mb)(15 downto 0);
p_mb_interface_out.mb_driver.rxword_out.q0(17 downto 16) <= "00";

p_mb_interface_out.mb_driver.txword_in(23 downto 12) <= s_db_reg_tx_in(stb_mb_q0)(29 downto 18);
p_mb_interface_out.mb_driver.txword_in(31 downto 24) <= (others=> '0');
p_mb_interface_out.mb_driver.txword_in(11 downto 0) <= (others=> '0'); 


--on the sm
--s_db_reg_tx_in(stb_db_xadc)    		<= (others=> '0'); --s_temp_data;--5   -- 32-bit xadc sensor data

p_sem_interface_out.status_heartbeat <= s_db_reg_tx_in(stb_sem)(31);
p_sem_interface_out.status_initialization <= s_db_reg_tx_in(stb_sem)(30); 
p_sem_interface_out.status_observation <= s_db_reg_tx_in(stb_sem)(29);
p_sem_interface_out.status_correction <= s_db_reg_tx_in(stb_sem)(28);
p_sem_interface_out.status_classification <= s_db_reg_tx_in(stb_sem)(27);
p_sem_interface_out.status_injection <= s_db_reg_tx_in(stb_sem)(26);
p_sem_interface_out.status_diagnostic_scan <= s_db_reg_tx_in(stb_sem)(25);
p_sem_interface_out.status_detect_only <= s_db_reg_tx_in(stb_sem)(24);
p_sem_interface_out.status_essential <= s_db_reg_tx_in(stb_sem)(23);
p_sem_interface_out.status_uncorrectable <= s_db_reg_tx_in(stb_sem)(22);
p_sem_interface_out.uart_tx <= s_db_reg_tx_in(stb_sem)(21);
p_sem_interface_out.monitor_txdata <= s_db_reg_tx_in(stb_sem)(20 downto 13);
p_sem_interface_out.monitor_txwrite <= s_db_reg_tx_in(stb_sem)(12);
p_sem_interface_out.monitor_txfull <= s_db_reg_tx_in(stb_sem)(11);


p_sem_interface_out.icap_out <= s_db_reg_tx_in(stb_db_sem_icap);--(others=> '0');

p_system_management_interface_out.xadc_control.reset_in <= s_db_reg_tx_in(stb_db_xadc_control)(31);
p_system_management_interface_out.xadc_control.user_temp_alarm_out <= s_db_reg_tx_in(stb_db_xadc_control)(30);
p_system_management_interface_out.xadc_control.vccint_alarm_out <= s_db_reg_tx_in(stb_db_xadc_control)(29);
p_system_management_interface_out.xadc_control.vccaux_alarm_out <= s_db_reg_tx_in(stb_db_xadc_control)(28);
p_system_management_interface_out.xadc_control.user_supply0_alarm_out <= s_db_reg_tx_in(stb_db_xadc_control)(27);
p_system_management_interface_out.xadc_control.user_supply1_alarm_out <= s_db_reg_tx_in(stb_db_xadc_control)(26);
p_system_management_interface_out.xadc_control.user_supply2_alarm_out <= s_db_reg_tx_in(stb_db_xadc_control)(25);

p_system_management_interface_out.xadc_control.jtaglocked_out <= s_db_reg_tx_in(stb_db_xadc_control)(23);
p_system_management_interface_out.xadc_control.jtagmodified_out <= s_db_reg_tx_in(stb_db_xadc_control)(22);
p_system_management_interface_out.xadc_control.jtagbusy_out <= s_db_reg_tx_in(stb_db_xadc_control)(21);

p_gbtx_interface_out.gbtx_side <= s_db_reg_tx_in(stb_gbtxa_reg)(31 downto 30);
p_gbtx_interface_out.busy <= s_db_reg_tx_in(stb_gbtxa_reg)(29);
p_clknet_out.gbtx_rxready <= s_db_reg_tx_in(stb_gbtxa_reg)(28 downto 27);
p_clknet_out.gbtx_datavalid <= s_db_reg_tx_in(stb_gbtxa_reg)(26 downto 25);
p_gbtx_interface_out.gbtxa_dpram.doutb <= s_db_reg_tx_in(stb_gbtxa_reg)(7 downto 0);
p_gbtx_interface_out.gbtxa_dpram.addrb <= s_db_reg_tx_in(stb_gbtxa_reg)(16 downto 8);
--p_gbtx_interface_out.gbtx_side <= s_db_reg_tx_in(stb_gbtxb_reg)(31 downto 30);
--p_gbtx_interface_out.busy <= s_db_reg_tx_in(stb_gbtxb_reg)(29);

p_gbtx_interface_out.gbtxb_dpram.doutb <= s_db_reg_tx_in(stb_gbtxb_reg)(7 downto 0);
p_gbtx_interface_out.gbtxb_dpram.addrb <= s_db_reg_tx_in(stb_gbtxb_reg)(16 downto 8);

--s_db_reg_tx_in(stb_cis_config)    	<= p_db_reg_rx_in(cfb_cis_config); --(others=> '0'); --s_db_cis_config_reg;									--9
-- cs
--s_db_reg_tx_in(stb_cs_t)       <= (others=> '0'); --s_cs_timestamp;
--s_db_reg_tx_in(stb_cs_s)       <= (others=> '0'); --s_cs_status;

p_serial_id_interface_out.family_code <= s_db_reg_tx_in(stb_db_serial_id_msb)(23 downto 16);
p_serial_id_interface_out.serial_number(47 downto 32) <= s_db_reg_tx_in(stb_db_serial_id_msb)(15 downto 0);
p_serial_id_interface_out.serial_number(31 downto 0) <= s_db_reg_tx_in(stb_db_serial_id_lsb);
p_serial_id_interface_out.crc_calculated <= s_db_reg_tx_in(stb_db_serial_id_status)(15 downto 8);
p_serial_id_interface_out.crc_read <= s_db_reg_tx_in(stb_db_serial_id_status)(7 downto 0);
p_serial_id_interface_out.busy <= s_db_reg_tx_in(stb_db_serial_id_status)(31);
p_serial_id_interface_out.crc_ok <= s_db_reg_tx_in(stb_db_serial_id_status)(30);

--s_db_reg_tx_in(stb_gbt_encoder)(0) <=s_encoder_fifo.rst;
--s_db_reg_tx_in(stb_gbt_encoder)(1) <= s_encoder_fifo.wr_en;
--s_db_reg_tx_in(stb_gbt_encoder)(2) <= s_encoder_fifo.injectdbiterr;
--s_db_reg_tx_in(stb_gbt_encoder)(3) <= s_encoder_fifo.injectsbiterr;
--s_db_reg_tx_in(stb_gbt_encoder)(4) <= s_encoder_fifo.sleep;
--s_db_reg_tx_in(stb_gbt_encoder)(5) <= s_encoder_fifo.full;
--s_db_reg_tx_in(stb_gbt_encoder)(6) <= s_encoder_fifo.wr_ack;
--s_db_reg_tx_in(stb_gbt_encoder)(7) <= s_encoder_fifo.overflow;
--s_db_reg_tx_in(stb_gbt_encoder)(8) <= s_encoder_fifo.valid;
--s_db_reg_tx_in(stb_gbt_encoder)(9) <= s_encoder_fifo.underflow;
--s_db_reg_tx_in(stb_gbt_encoder)(10) <= s_encoder_fifo.sbiterr;
--s_db_reg_tx_in(stb_gbt_encoder)(11) <= s_encoder_fifo.wr_rst_busy;
--s_db_reg_tx_in(stb_gbt_encoder)(12) <= s_encoder_fifo.rd_rst_busy;



-- assembling data words
s_encoder_fifo.clk <= p_clknet_in.clk80;
s_encoder_fifo.rst <= p_master_reset_in;
s_encoder_fifo.wr_en <= '1';
s_encoder_fifo.rd_en <= '1';
s_encoder_fifo.injectdbiterr <= '0';
s_encoder_fifo.injectsbiterr <= '0';
s_encoder_fifo.sleep <= '0';

--s_commbus_encoder_interface.commbus_tx_data_out <= s_encoder_fifo.dout;
s_encoder_fifo.din <= p_commbus_ddr_in.commbus_rx_data_in;

--i_fifo_gbt_encoder : fifo_gbt_encoder
--  PORT MAP (
--    clk => s_encoder_fifo.clk,
--    srst => s_encoder_fifo.rst,
--    din => s_encoder_fifo.din,
--    wr_en => s_encoder_fifo.wr_en,
--    rd_en => s_encoder_fifo.rd_en,
--    injectdbiterr => s_encoder_fifo.injectdbiterr,
--    injectsbiterr => s_encoder_fifo.injectsbiterr,
--    sleep => s_encoder_fifo.sleep,
--    dout => s_encoder_fifo.dout,
--    full => s_encoder_fifo.full,
--    wr_ack => s_encoder_fifo.wr_ack,
--    overflow => s_encoder_fifo.overflow,
--    empty => s_encoder_fifo.empty,
--    valid => s_encoder_fifo.valid,
--    underflow => s_encoder_fifo.underflow,
--    sbiterr => s_encoder_fifo.sbiterr,
--    dbiterr => s_encoder_fifo.dbiterr,
--    wr_rst_busy => s_encoder_fifo.wr_rst_busy,
--    rd_rst_busy => s_encoder_fifo.rd_rst_busy
--  );

--i_sr_ram_commbus : sr_ram_commbus
--  PORT MAP (
--    D => s_encoder_fifo.din,
--    CLK => s_encoder_fifo.clk,
--    CE => '1',
--    SCLR => s_encoder_fifo.rst ,
--    SSET => '0',
--    Q => s_encoder_fifo.dout
--  );


    proc_db_data_disassembler : process(p_commbus_ddr_in.bitclk_rx)
    --variable v_counter : integer range 0 to 120 := 0;
    variable v_crc_counter : integer := 0;
    variable v_loopback_counter, v_loopback_counter_buffer : integer := 0;
    variable v_loopback_locked_counter : integer range 0 to 15;
    begin
        
        if rising_edge(p_commbus_ddr_in.bitclk_rx) then

            p_commbus_decoder_interface_out.crc_counter<=std_logic_vector(to_unsigned(v_crc_counter,32));
            p_commbus_decoder_interface_out.loopback_counter<=std_logic_vector(to_unsigned(v_loopback_counter,32));
            if p_master_reset_in = '1' then
                v_crc_counter := 0;
                v_loopback_counter :=0;
                v_loopback_counter_buffer :=0;
            else
                if p_commbus_ddr_in.bitcnt_rx = 60 then
                    s_encoder_fifo.dout<= s_encoder_fifo.din;
                    if p_commbus_ddr_in.locked = '1' then
                        p_commbus_decoder_interface_out.reset_locked<='0';
                        if p_commbus_ddr_in.datavalid = '1' then
                            if tilecal.db6_design_package.tile_link_crc_compute(s_encoder_fifo.dout(115 downto 16))(15 downto 0) = s_encoder_fifo.dout(15 downto 0) then
                                --disassemble data
                                if to_integer(unsigned(s_encoder_fifo.dout(85 downto 80))) < c_number_of_cfgbus_regs then
                                    s_db_reg_rx_out(to_integer(unsigned(s_encoder_fifo.dout(85 downto 80)))) <= s_encoder_fifo.dout(15+32 downto 16);
                                end if;
                                if to_integer(unsigned(s_encoder_fifo.dout(85 downto 80))) < c_number_of_gbttx_regs then
                                    s_db_reg_tx_in(to_integer(unsigned(s_encoder_fifo.dout(85 downto 80)))) <= s_encoder_fifo.dout(15+32+32 downto 16+32);
                                end if;
                                p_tdo_remote_out <= s_encoder_fifo.dout(90);
                            else
                                v_crc_counter:=v_crc_counter+1;
                            end if;
                        end if;
                    

                    elsif p_commbus_ddr_in.dataloopback = '1' then
                        p_commbus_decoder_interface_out.reset_locked<='0';
                        
                        if tilecal.db6_design_package.tile_link_crc_compute(s_encoder_fifo.dout(115 downto 16))(15 downto 0) = s_encoder_fifo.dout(15 downto 0) then
                            v_loopback_counter := to_integer(unsigned(s_encoder_fifo.dout(16+31 downto 16)));
                            v_loopback_counter_buffer := v_loopback_counter;
                            if v_loopback_counter_buffer+1 = v_loopback_counter then
                                if v_loopback_locked_counter = 15 then
                                    p_commbus_decoder_interface_out.loopback_counter_locked <= '1';
                                else
                                    v_loopback_locked_counter := v_loopback_locked_counter +1;
                                    p_commbus_decoder_interface_out.loopback_counter_locked <= '0';
                                end if;
                            else
                                if v_loopback_locked_counter > 0 then
                                    v_loopback_locked_counter := v_loopback_locked_counter -1;
                                    p_commbus_decoder_interface_out.loopback_counter_locked <= '0';
                                end if;
                            end if;
                        else
                            v_crc_counter:=v_crc_counter+1;
                        end if;
                    
                    elsif p_commbus_ddr_in.datareset = '1' then
                        if s_encoder_fifo.dout = x"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" then
                            p_commbus_decoder_interface_out.reset_locked<='1';
                        else
                            p_commbus_decoder_interface_out.reset_locked<='0';
                        end if;
                        
                    end if;
                end if;
            end if;
        end if;
    end process;
 
end behavioral;


