--=================================================================================================--
--##################################   module information   #######################################--
--=================================================================================================--
--                                                                                         
-- company:               stockholm university                                                        
-- engineer:              samuel silverstein    silver@fysik.su.se
--                        eduardo valdes santurio
--                                                                                                 
-- project name:          adc deserializer for ltc2264-12                                                                
-- module name:           adc_top                                        
--                                                                                                 
-- language:              vhdl                                                                 
--                                                                                                   --
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_unsigned.all;

library unisim;
use unisim.vcomponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_adc_interface_fifo is

    port ( 	
        p_master_reset_in : in std_logic;
        --clock
        p_clknet_in                        : in t_db_clknet;
        --inputs
        p_adc_bitclk_in : in t_adc_clk_in;
        p_adc_frameclk_in : in t_adc_clk_in;
        p_adc_lg_data_in : in t_adc_data_in;
        p_adc_hg_data_in : in t_adc_data_in;
        --control
        p_adc_readout_control_in : in t_adc_readout_control;
        --output
        p_adc_readout_out       : out t_adc_readout;
        p_leds_out      : out std_logic_vector(3 downto 0)
				);
end db6_adc_interface_fifo;

architecture behavioral of db6_adc_interface_fifo is

    
    signal s_data_lg, s_data_hg, s_data_lg_delayed, s_data_hg_delayed , s_bitclk_in, s_bitclk_se, s_frameclk, s_frameclk_to_bufg ,s_frameclk_delayed: std_logic_vector (5 downto 0) := (others => '0');
    signal s_adc_data_lg_pipeline, s_adc_data_hg_pipeline : t_adc_data_pipeline;
    signal s_pipeline_flag : std_logic;
    attribute iob: string;
    attribute keep: string;
    attribute dont_touch: string;
    attribute iob of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "true";
    attribute keep of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "true";
    attribute dont_touch of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "true";
    --attribute keep of s_adc_readout          : signal is "true";    
    
    constant c_adc_test_pattern : std_logic_vector(13 downto 0) := "00" & x"cab";
    
    signal s_bufgce_div_ctrl_reset, s_bitclk_div : std_logic_vector(5 downto 0) := (others => '0'); 

    signal s_idelay_ctrl_rdy, s_idelay_ctrl_reset : std_logic := '0';

    
    signal s_lg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_sm, s_lg_idelay_count_in_from_sm : t_idelay_integer_array;
    signal s_lg_idelay_count_in, s_lg_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_hg_idelay_count_in, s_hg_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_fc_idelay_count_in, s_fc_idelay_count_out, s_fc_idelay_count_in_from_sm : t_idelay_count := (others => (others => '0'));
    signal s_lg_idelay_ctrl_reset, s_lg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_reset, s_hg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_reset, s_fc_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_idelay_ctrl_load, s_lg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_load, s_hg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_load, s_fc_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');  
    signal s_lg_idelay_ctrl_en_vtc, s_lg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_en_vtc, s_hg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_en_vtc, s_fc_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    
    
    signal s_channel_frame_missalignemt : std_logic_vector (5 downto 0) := (others => '1');
    type t_phase_offset is array (5 downto 0) of std_logic_vector(1 downto 0);
    signal s_phase_offset : t_phase_offset := (others=> "11");
    signal s_frame_strobe : std_logic_vector (5 downto 0);
    

    signal s_readout_lg_sr,s_readout_hg_sr : t_adc_sr;
    signal s_bitslice_lg_sr, s_bitslice_hg_sr, s_bitslice_fc_sr : t_bitslice_sr;
    signal s_adc_readout : t_adc_readout :=
    (
    lg_bitslip => (others => "0111"),
    hg_bitslip => (others => "0111"),
    lg_idelay_count => (others =>"000000000"),
    hg_idelay_count => (others =>"000000000"),
    fc_idelay_count => (others =>"000000000"),
    lg_data =>(others=>"00000000000000"),
    hg_data =>(others=>"00000000000000"),
    fc_data =>(others=>"00000000000000"),
    channel_missed_bit_count=>(others=>(others=>'0')),
    channel_frame_missalignemt => (others=>'0'),
    readout_initialized => '0',
    
    mb_adc_config_control => c_adc_register_init_config,
    
    leds => "0000"
    );
 
    signal s_lg_delay_control_array, s_hg_delay_control_array, s_fc_delay_control_array : t_adc_readout_delay_control_array := (others => c_delay_control);
    
    
    signal s_fc_bitslips_from_sm, s_hg_bitslips_from_sm, s_lg_bitslips_from_sm, s_hg_bitslips_from_hw, s_lg_bitslips_from_hw : t_bitslips_integer_array;
    signal s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word, s_fc_output_word : t_adc_data;
    signal s_adc_input_fc_temp, s_adc_input_lg_temp, s_adc_input_hg_temp:t_adc_data;
    
    
    attribute keep of s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word : signal is "true";
    attribute dont_touch of s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word : signal is "true";
    
    --signal s_adc_output_temp, s_adc_input_fc_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;
    
    signal s_adc_channel_fifo_fc, s_adc_channel_fifo_hg, s_adc_channel_fifo_lg, s_adc_channel_fifo_lg_debug, s_adc_channel_fifo_hg_debug, s_adc_channel_fifo_fc_debug : t_adc_channel_fifo;
    signal s_adc_channel_sr_fc, s_adc_channel_sr_lg, s_adc_channel_sr_hg : t_adc_channel_sr;
    
    ------------- begin cut here for component declaration ------ comp_tag
component fifo_adc_readout
  port (
    srst : in std_logic;
    wr_clk : in std_logic;
    rd_clk : in std_logic;
    din : in std_logic_vector(13 downto 0);
    wr_en : in std_logic;
    rd_en : in std_logic;
    injectdbiterr : in std_logic;
    injectsbiterr : in std_logic;
    dout : out std_logic_vector(13 downto 0);
    full : out std_logic;
    wr_ack : out std_logic;
    overflow : out std_logic;
    empty : out std_logic;
    valid : out std_logic;
    underflow : out std_logic;
    sbiterr : out std_logic;
    dbiterr : out std_logic;
    wr_rst_busy : out std_logic;
    rd_rst_busy : out std_logic
  );
end component;

component sr_ram_adc_iddr
  port (
    d : in std_logic_vector(1 downto 0);
    clk : in std_logic;
    ce : in std_logic;
    sclr : in std_logic;
    sset : in std_logic;
    q : out std_logic_vector(1 downto 0)
  );
end component;

--component ila_adc_fifo

--port (
--	clk : in std_logic;



--	probe0 : in std_logic_vector(1 downto 0); 
--	probe1 : in std_logic_vector(1 downto 0); 
--	probe2 : in std_logic_vector(13 downto 0); 
--	probe3 : in std_logic_vector(13 downto 0); 
--	probe4 : in std_logic_vector(3 downto 0); 
--	probe5 : in std_logic_vector(3 downto 0); 
--	probe6 : in std_logic_vector(0 downto 0); 
--	probe7 : in std_logic_vector(0 downto 0); 
--	probe8 : in std_logic_vector(0 downto 0); 
--	probe9 : in std_logic_vector(0 downto 0); 
--	probe10 : in std_logic_vector(0 downto 0); 
--	probe11 : in std_logic_vector(0 downto 0); 
--	probe12 : in std_logic_vector(0 downto 0); 
--	probe13 : in std_logic_vector(0 downto 0); 
--	probe14 : in std_logic_vector(0 downto 0);
--	probe15 : in std_logic_vector(0 downto 0);
--	probe16 : in std_logic_vector(0 downto 0);
--	probe17 : in std_logic_vector(0 downto 0);
--	probe18 : in std_logic_vector(0 downto 0)
	
	
--);
--end component  ;


--component vio_adc_fifo
--  port (
--    clk : in std_logic;
--    probe_in0 : in std_logic_vector(3 downto 0);
--    probe_in1 : in std_logic_vector(3 downto 0);
--    probe_in2 : in std_logic_vector(0 downto 0);
--    probe_in3 : in std_logic_vector(0 downto 0);
--    probe_in4 : in std_logic_vector(0 downto 0);
--    probe_in5 : in std_logic_vector(0 downto 0);
--    probe_in6 : in std_logic_vector(0 downto 0);
--    probe_in7 : in std_logic_vector(0 downto 0);
--    probe_in8 : in std_logic_vector(0 downto 0);
--    probe_in9 : in std_logic_vector(0 downto 0);
--    probe_in10 : in std_logic_vector(0 downto 0);
--    probe_in11 : in std_logic_vector(0 downto 0);
--    probe_in12 : in std_logic_vector(0 downto 0);
--    probe_in13 : in std_logic_vector(0 downto 0);
--    probe_in14 : in std_logic_vector(0 downto 0);
--    probe_out0 : out std_logic_vector(0 downto 0);
--    probe_out1 : out std_logic_vector(0 downto 0);
--    probe_out2 : out std_logic_vector(0 downto 0);
--    probe_out3 : out std_logic_vector(0 downto 0);
--    probe_out4 : out std_logic_vector(0 downto 0)
--  );
--end component;
    

------------- begin cut here for component declaration ------ comp_tag


begin

p_adc_readout_out <= s_adc_readout;

-- differential to single-ended conversion of adc inputs from fmc
gen_adc_data_diff_to_se : for i in 0 to 5 generate

    i_IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_lg_delay_control_array(i).idatain,             -- Buffer diff_p output
        I  => p_adc_lg_data_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_lg_data_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_IBUFDS_DATA1 : IBUFDS -- ADC output High gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_hg_delay_control_array(i).idatain,             -- Buffer diff_p output
        I  => p_adc_hg_data_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_hg_data_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_fc_delay_control_array(i).idatain,             -- Buffer diff_p output
        I  => p_adc_frameclk_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_frameclk_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_bitclk_se(i),             -- Buffer diff_p output
        I  => p_adc_bitclk_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_bitclk_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );


    bufgce_div_inst : bufgce_div
    generic map (
        bufgce_divide => 7, -- 1-8
        -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
        is_ce_inverted => '0', -- optional inversion for ce
        is_clr_inverted => '0', -- optional inversion for clr
        is_i_inverted => '0' -- optional inversion for i
        )
        port map (
        o => s_bitclk_div(i), -- 1-bit output: buffer
        ce => '1', -- 1-bit input: buffer enable
        clr => s_bufgce_div_ctrl_reset(i), -- 1-bit input: asynchronous clear
        i => s_bitclk_se(i) -- 1-bit input: buffer
    );

--    i_pll_bitslice : pll_bitslice
--   port map ( 
--      -- Clock out ports  
--       clk_out1 => s_bitclk(i),
--       clk_out2 => s_frameclk_pll(i),
--      -- Dynamic reconfiguration ports             
--       p_daddr_in => s_pll_bitslice_control_array(i).daddr_in,
--       p_dclk_in => p_clknet_in.osc_clk40,
--       p_den_in => s_pll_bitslice_control_array(i).den_in,
--       p_din_in => s_pll_bitslice_control_array(i).din_in,
--       p_dout_out => s_pll_bitslice_control_array(i).dout_out,
--       p_drdy_out => s_pll_bitslice_control_array(i).drdy_out,
--       p_dwe_in => s_pll_bitslice_control_array(i).dwe_in,
--      -- Status and control signals                
--       p_reset_in => s_pll_bitslice_control_array(i).reset_in,
--       p_locked_out => s_pll_bitslice_control_array(i).locked_out,
--       -- Clock in ports
--       p_clk280_in => s_bitclk_se(i)
-- );
    
    
--    s_pll_bitslice_control_array(i).daddr_in <= (others =>'0');
--    s_pll_bitslice_control_array(i).den_in <= '0';
--    s_pll_bitslice_control_array(i).din_in <= (others =>'0');
--    s_pll_bitslice_control_array(i).dwe_in <= '0';
--    s_pll_bitslice_control_array(i).psclk_in <= '0';
--    s_pll_bitslice_control_array(i).psen_in <= '0';
--    s_pll_bitslice_control_array(i).psincdec_in <= '0';
--    s_pll_bitslice_control_array(i).reset_in <= '0';
--    s_pll_bitslice_control_array(i).cddcreq_in <= '0';

    


end generate;
-- generate adc data input registers and output word mapping,
-- the output bits from each adc is stored in four shift registers (two even and two odd).


s_idelay_ctrl_reset <= p_master_reset_in;

proc_load_hw_bitslips: process(p_adc_readout_control_in.db_side)
begin
    if p_adc_readout_control_in.db_side = "01" then

        s_lg_bitslips_from_hw <= (14,14,14,14,14,14);
        s_hg_bitslips_from_hw <= (14,14,14,14,14,14);

        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);--(60,0,0,0,0,0);
        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,0);--(60,0,0,0,0,0);
    
    elsif p_adc_readout_control_in.db_side = "10" then
        
        s_lg_bitslips_from_hw <= (14,14,14,14,14,14);--(14-1,14,14,14,14,14);
        s_hg_bitslips_from_hw <= (14,14,14,14,14,14);--(14,14,14,14,14,14-1);
        
        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);--(60,0,0,0,0,60);
        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,0);--(0,0,0,0,0,200);

    else

    end if;

end process;

gen_adc_channels: for v_adc in 0 to 5 generate

--    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);
    s_adc_readout.hg_bitslip(v_adc) <= p_adc_readout_control_in.hg_bitslip(v_adc);
    s_adc_readout.lg_bitslip(v_adc) <= p_adc_readout_control_in.lg_bitslip(v_adc);
--p_channel_frame_missalignemt_out(v_adc) <= s_channel_frame_missalignemt(v_adc);
    


i_idelaye3_data_lg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_lg_delay_control_array(v_adc).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_lg_delay_control_array(v_adc).cntvalueout,--s_lg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_lg_delay_control_array(v_adc).dataout,--s_adc_lg_data_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => s_lg_delay_control_array(v_adc).casc_in,--'0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_lg_delay_control_array(v_adc).casc_return,--'0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_lg_delay_control_array(v_adc).ce, --'0', -- 1-bit input: active high enable increment/decrement input
        clk => s_lg_delay_control_array(v_adc).clk,--s_bitclk(v_adc), -- 1-bit input: clock input
        cntvaluein => s_lg_delay_control_array(v_adc).cntvaluein, --s_lg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => s_lg_delay_control_array(v_adc).datain, --'0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_lg_delay_control_array(v_adc).en_vtc, --s_lg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_lg_delay_control_array(v_adc).idatain, --s_adc_lg_data_se(v_adc), -- 1-bit input: data input from the logic
        inc => s_lg_delay_control_array(v_adc).inc, --'0', -- 1-bit input: increment / decrement tap delay input
        load => s_lg_delay_control_array(v_adc).load, --s_lg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_lg_delay_control_array(v_adc).rst --s_lg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);
s_lg_delay_control_array(v_adc).clk<= s_bitclk_se(v_adc);
s_lg_delay_control_array(v_adc).rst <= s_lg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc); 
s_lg_delay_control_array(v_adc).load <= s_lg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_load(v_adc);
s_lg_delay_control_array(v_adc).en_vtc <= s_lg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
s_lg_delay_control_array(v_adc).cntvaluein <= std_logic_vector(to_unsigned(s_lg_idelay_count_in_from_sm(v_adc) + s_lg_idelay_count_in_from_hw(v_adc),9));-- p_adc_readout_control_in.lg_idelay_count(v_adc);
s_adc_readout.lg_idelay_count(v_adc) <= s_lg_delay_control_array(v_adc).cntvalueout;

i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_hg_delay_control_array(v_adc).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_hg_delay_control_array(v_adc).cntvalueout,--s_hg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_hg_delay_control_array(v_adc).dataout,--s_adc_hg_data_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => s_hg_delay_control_array(v_adc).casc_in,--'0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_hg_delay_control_array(v_adc).casc_return,--'0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_hg_delay_control_array(v_adc).ce, --'0', -- 1-bit input: active high enable increment/decrement input
        clk => s_hg_delay_control_array(v_adc).clk,--s_bitclk(v_adc), -- 1-bit input: clock input
        cntvaluein => s_hg_delay_control_array(v_adc).cntvaluein, --s_hg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => s_hg_delay_control_array(v_adc).datain, --'0',--s_data_hg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_hg_delay_control_array(v_adc).en_vtc, --s_hg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_hg_delay_control_array(v_adc).idatain, --s_adc_hg_data_se(v_adc), -- 1-bit input: data input from the logic
        inc => s_hg_delay_control_array(v_adc).inc, --'0', -- 1-bit input: increment / decrement tap delay input
        load => s_hg_delay_control_array(v_adc).load, --s_hg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_hg_delay_control_array(v_adc).rst --s_hg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_hg_delay_control_array(v_adc).clk<= s_bitclk_se(v_adc);
s_hg_delay_control_array(v_adc).rst <= s_hg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc); 
s_hg_delay_control_array(v_adc).load <= s_hg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_load(v_adc);
s_hg_delay_control_array(v_adc).en_vtc <= s_hg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
s_hg_delay_control_array(v_adc).cntvaluein <= std_logic_vector(to_unsigned(s_hg_idelay_count_in_from_sm(v_adc) + s_hg_idelay_count_in_from_hw(v_adc),9));-- p_adc_readout_control_in.hg_idelay_count(v_adc);
s_adc_readout.hg_idelay_count(v_adc) <= s_hg_delay_control_array(v_adc).cntvalueout;

i_idelaye3_data_fc : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_fc_delay_control_array(v_adc).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_fc_delay_control_array(v_adc).cntvalueout,--s_fc_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_fc_delay_control_array(v_adc).dataout,--s_adc_fc_data_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => s_fc_delay_control_array(v_adc).casc_in,--'0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_fc_delay_control_array(v_adc).casc_return,--'0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_fc_delay_control_array(v_adc).ce, --'0', -- 1-bit input: active high enable increment/decrement input
        clk => s_fc_delay_control_array(v_adc).clk,--s_bitclk(v_adc), -- 1-bit input: clock input
        cntvaluein => s_fc_delay_control_array(v_adc).cntvaluein, --s_fc_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => s_fc_delay_control_array(v_adc).datain, --'0',--s_data_fc(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_fc_delay_control_array(v_adc).en_vtc, --s_fc_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_fc_delay_control_array(v_adc).idatain, --s_adc_fc_data_se(v_adc), -- 1-bit input: data input from the logic
        inc => s_fc_delay_control_array(v_adc).inc, --'0', -- 1-bit input: increment / decrement tap delay input
        load => s_fc_delay_control_array(v_adc).load, --s_fc_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_fc_delay_control_array(v_adc).rst --s_fc_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_fc_delay_control_array(v_adc).clk<= s_bitclk_se(v_adc);
s_fc_delay_control_array(v_adc).rst <= p_adc_readout_control_in.fc_idelay_ctrl_reset(v_adc) or s_fc_idelay_ctrl_reset_from_sm(v_adc);
s_fc_delay_control_array(v_adc).load <= s_fc_idelay_ctrl_load_from_sm(v_adc);--p_adc_readout_control_in.fc_idelay_load(v_adc);
s_fc_delay_control_array(v_adc).en_vtc <= s_fc_idelay_ctrl_en_vtc_from_sm(v_adc);-- p_adc_readout_control_in.fc_idelay_en_vtc(v_adc);
s_fc_delay_control_array(v_adc).cntvaluein <= s_fc_idelay_count_in_from_sm(v_adc); --p_adc_readout_control_in.fc_idelay_count(v_adc);
s_adc_readout.fc_idelay_count(v_adc) <= s_fc_delay_control_array(v_adc).cntvalueout;

i_iddre1_lg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_lg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_lg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => s_bitclk_se(v_adc), -- 1-bit input: high-speed clock
cb => s_bitclk_se(v_adc), -- 1-bit input: inversion of high-speed clock c
d => s_lg_delay_control_array(v_adc).dataout, -- 1-bit input: serial data input
r => s_lg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);

i_iddre1_hg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_hg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_hg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => s_bitclk_se(v_adc), -- 1-bit input: high-speed clock
cb => s_bitclk_se(v_adc), -- 1-bit input: inversion of high-speed clock c
d => s_hg_delay_control_array(v_adc).dataout, -- 1-bit input: serial data input
r => s_hg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);

i_iddre1_fc : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_fc_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_fc_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => s_bitclk_se(v_adc), -- 1-bit input: high-speed clock
cb => s_bitclk_se(v_adc), -- 1-bit input: inversion of high-speed clock c
d => s_fc_delay_control_array(v_adc).dataout, -- 1-bit input: serial data input
r => s_fc_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);



i_sr_hg : sr_ram_adc_iddr
  port map (
    d => s_adc_channel_sr_hg(v_adc).d,
    clk => s_adc_channel_sr_hg(v_adc).clk,
    ce => s_adc_channel_sr_hg(v_adc).ce,
    sclr => s_adc_channel_sr_hg(v_adc).sclr,
    sset => s_adc_channel_sr_hg(v_adc).sset,
    q => s_adc_channel_sr_hg(v_adc).q
  );
s_adc_channel_sr_hg(v_adc).clk <= s_bitclk_se(v_adc);
s_adc_channel_sr_hg(v_adc).d <= s_bitslice_hg_sr(v_adc)(0) & s_bitslice_hg_sr(v_adc)(1);
s_adc_channel_sr_hg(v_adc).sclr <= '0';
s_adc_channel_sr_hg(v_adc).ce <= '1';
s_adc_channel_sr_hg(v_adc).sset <= '0';

i_adc_fifo_hg : fifo_adc_readout
  port map (
    srst => s_adc_channel_fifo_hg(v_adc).srst,
    wr_clk => s_adc_channel_fifo_hg(v_adc).wr_clk,
    rd_clk => s_adc_channel_fifo_hg(v_adc).rd_clk,
    din => s_adc_channel_fifo_hg(v_adc).din,
    wr_en => s_adc_channel_fifo_hg(v_adc).wr_en,
    rd_en => s_adc_channel_fifo_hg(v_adc).rd_en,
    injectdbiterr => s_adc_channel_fifo_hg(v_adc).injectdbiterr,
    injectsbiterr => s_adc_channel_fifo_hg(v_adc).injectsbiterr,
    --sleep => s_adc_channel_fifo_hg(v_adc).sleep,
    dout => s_adc_channel_fifo_hg(v_adc).dout,
    full => s_adc_channel_fifo_hg(v_adc).full,
    --almost_full => s_adc_channel_fifo_hg(v_adc).almost_full,
    wr_ack => s_adc_channel_fifo_hg(v_adc).wr_ack,
    overflow => s_adc_channel_fifo_hg(v_adc).overflow,
    empty => s_adc_channel_fifo_hg(v_adc).empty,
    --almost_empty => s_adc_channel_fifo_hg(v_adc).almost_empty,
    valid => s_adc_channel_fifo_hg(v_adc).valid,
    underflow => s_adc_channel_fifo_hg(v_adc).underflow,
    --rd_data_count => s_adc_channel_fifo_hg(v_adc).rd_data_count,
    --wr_data_count => s_adc_channel_fifo_hg(v_adc).wr_data_count,
    sbiterr => s_adc_channel_fifo_hg(v_adc).sbiterr,
    dbiterr => s_adc_channel_fifo_hg(v_adc).dbiterr,
    wr_rst_busy => s_adc_channel_fifo_hg(v_adc).wr_rst_busy,
    rd_rst_busy => s_adc_channel_fifo_hg(v_adc).rd_rst_busy
  );
--s_adc_channel_fifo_hg(v_adc).srst <= '0';
s_adc_channel_fifo_hg(v_adc).wr_clk <= s_bitclk_div(v_adc);
s_adc_channel_fifo_hg(v_adc).rd_clk <= p_clknet_in.clk40;
s_adc_channel_fifo_hg(v_adc).din <= s_adc_input_hg_temp(v_adc);
--s_adc_channel_fifo_hg(v_adc).wr_en <= '1';
--s_adc_channel_fifo_hg(v_adc).rd_en <= '1';
s_adc_channel_fifo_hg(v_adc).injectdbiterr <= '0';
s_adc_channel_fifo_hg(v_adc).injectsbiterr <= '0';
s_hg_adc_output_word(v_adc) <= s_adc_channel_fifo_hg(v_adc).dout;

i_sr_lg : sr_ram_adc_iddr
  port map (
    d => s_adc_channel_sr_lg(v_adc).d,
    clk => s_adc_channel_sr_lg(v_adc).clk,
    ce => s_adc_channel_sr_lg(v_adc).ce,
    sclr => s_adc_channel_sr_lg(v_adc).sclr,
    sset => s_adc_channel_sr_lg(v_adc).sset,
    q => s_adc_channel_sr_lg(v_adc).q
  );
s_adc_channel_sr_lg(v_adc).clk <= s_bitclk_se(v_adc);
s_adc_channel_sr_lg(v_adc).d <= s_bitslice_lg_sr(v_adc)(0) & s_bitslice_lg_sr(v_adc)(1);
s_adc_channel_sr_lg(v_adc).sclr <= '0';
s_adc_channel_sr_lg(v_adc).ce <= '1';
s_adc_channel_sr_lg(v_adc).sset <= '0';



i_adc_fifo_lg : fifo_adc_readout
  port map (
    srst => s_adc_channel_fifo_lg(v_adc).srst,
    wr_clk => s_adc_channel_fifo_lg(v_adc).wr_clk,
    rd_clk => s_adc_channel_fifo_lg(v_adc).rd_clk,
    din => s_adc_channel_fifo_lg(v_adc).din,
    wr_en => s_adc_channel_fifo_lg(v_adc).wr_en,
    rd_en => s_adc_channel_fifo_lg(v_adc).rd_en,
    injectdbiterr => s_adc_channel_fifo_lg(v_adc).injectdbiterr,
    injectsbiterr => s_adc_channel_fifo_lg(v_adc).injectsbiterr,
    --sleep => s_adc_channel_fifo_lg(v_adc).sleep,
    dout => s_adc_channel_fifo_lg(v_adc).dout,
    full => s_adc_channel_fifo_lg(v_adc).full,
    --almost_full => s_adc_channel_fifo_lg(v_adc).almost_full,
    wr_ack => s_adc_channel_fifo_lg(v_adc).wr_ack,
    overflow => s_adc_channel_fifo_lg(v_adc).overflow,
    empty => s_adc_channel_fifo_lg(v_adc).empty,
    --almost_empty => s_adc_channel_fifo_lg(v_adc).almost_empty,
    valid => s_adc_channel_fifo_lg(v_adc).valid,
    underflow => s_adc_channel_fifo_lg(v_adc).underflow,
    --rd_data_count => s_adc_channel_fifo_lg(v_adc).rd_data_count,
    --wr_data_count => s_adc_channel_fifo_lg(v_adc).wr_data_count,
    sbiterr => s_adc_channel_fifo_lg(v_adc).sbiterr,
    dbiterr => s_adc_channel_fifo_lg(v_adc).dbiterr,
    wr_rst_busy => s_adc_channel_fifo_lg(v_adc).wr_rst_busy,
    rd_rst_busy => s_adc_channel_fifo_lg(v_adc).rd_rst_busy
  );

--s_adc_channel_fifo_lg(v_adc).rst <= '0';
s_adc_channel_fifo_lg(v_adc).wr_clk <= s_bitclk_div(v_adc);
s_adc_channel_fifo_lg(v_adc).rd_clk <= p_clknet_in.clk40;
s_adc_channel_fifo_lg(v_adc).din <= s_adc_input_lg_temp(v_adc);
--s_adc_channel_fifo_lg(v_adc).wr_en <= '1';
--s_adc_channel_fifo_lg(v_adc).rd_en <= '1';
--s_adc_channel_fifo_lg(v_adc).injectdbiterr <= '0';
--s_adc_channel_fifo_lg(v_adc).injectsbiterr <= '0';
s_lg_adc_output_word(v_adc) <= s_adc_channel_fifo_lg(v_adc).dout;

--i_vio_adc_fifo_ch0 : vio_adc_fifo
--  port map (
--    clk => s_bitclk_se(0),
--    probe_in0 => s_adc_channel_fifo_lg(v_adc).rd_data_count,
--    probe_in1 => s_adc_channel_fifo_lg(v_adc).wr_data_count,
--    probe_in2(0) => s_adc_channel_fifo_lg(v_adc).full,
--    probe_in3(0) => s_adc_channel_fifo_lg(v_adc).almost_full,--s_adc_channel_fifo_lg(v_adc).sleep, --'0', --s_adc_channel_fifo_lg(v_adc).almost_full,
--    probe_in4(0) => s_adc_channel_fifo_lg(v_adc).wr_ack,
--    probe_in5(0) => s_adc_channel_fifo_lg(v_adc).overflow,
--    probe_in6(0) => s_adc_channel_fifo_lg(v_adc).injectsbiterr,
--    probe_in7(0) => s_adc_channel_fifo_lg(v_adc).empty,
--    probe_in8(0) => s_adc_channel_fifo_lg(v_adc).almost_empty,
--    probe_in9(0) => s_adc_channel_fifo_lg(v_adc).valid,
--    probe_in10(0) => s_adc_channel_fifo_lg(v_adc).underflow,
--    probe_in11(0) => s_adc_channel_fifo_lg(v_adc).sbiterr,
--    probe_in12(0) => s_adc_channel_fifo_lg(v_adc).dbiterr,
--    probe_in13(0) => s_adc_channel_fifo_lg(v_adc).wr_rst_busy,
--    probe_in14(0) => s_adc_channel_fifo_lg(v_adc).rd_rst_busy,
--    probe_out0(0) => s_adc_channel_fifo_lg_debug(v_adc).srst,
--    probe_out1(0) => s_adc_channel_fifo_lg_debug(v_adc).wr_en,
--    probe_out2(0) => s_adc_channel_fifo_lg_debug(v_adc).rd_en,
--    probe_out3(0) => s_adc_channel_fifo_lg_debug(v_adc).injectdbiterr,
--    probe_out4(0) => s_adc_channel_fifo_lg_debug(v_adc).injectsbiterr
--  );

s_adc_channel_fifo_lg_debug(v_adc).srst <='0';
s_adc_channel_fifo_lg_debug(v_adc).rd_en <='1';
s_adc_channel_fifo_lg_debug(v_adc).wr_en <='1';

s_adc_channel_fifo_hg_debug(v_adc).srst <='0';
s_adc_channel_fifo_hg_debug(v_adc).rd_en <='1';
s_adc_channel_fifo_hg_debug(v_adc).wr_en <='1';

s_adc_channel_fifo_fc_debug(v_adc).srst <='0';
s_adc_channel_fifo_fc_debug(v_adc).rd_en <='1';
s_adc_channel_fifo_fc_debug(v_adc).wr_en <='1';


i_sr_fc : sr_ram_adc_iddr
  port map (
    d => s_adc_channel_sr_fc(v_adc).d,
    clk => s_adc_channel_sr_fc(v_adc).clk,
    ce => s_adc_channel_sr_fc(v_adc).ce,
    sclr => s_adc_channel_sr_fc(v_adc).sclr,
    sset => s_adc_channel_sr_fc(v_adc).sset,
    q => s_adc_channel_sr_fc(v_adc).q
  );
s_adc_channel_sr_fc(v_adc).clk <= s_bitclk_se(v_adc);
s_adc_channel_sr_fc(v_adc).d <= s_bitslice_fc_sr(v_adc)(0) & s_bitslice_fc_sr(v_adc)(1);
s_adc_channel_sr_fc(v_adc).sclr <= '0';
s_adc_channel_sr_fc(v_adc).ce <= '1';
s_adc_channel_sr_fc(v_adc).sset <= '0';

i_adc_fifo_fc : fifo_adc_readout
  port map (
    srst => s_adc_channel_fifo_fc(v_adc).srst,
    wr_clk => s_adc_channel_fifo_fc(v_adc).wr_clk,
    rd_clk => s_adc_channel_fifo_fc(v_adc).rd_clk,
    din => s_adc_channel_fifo_fc(v_adc).din,
    wr_en => s_adc_channel_fifo_fc(v_adc).wr_en,
    rd_en => s_adc_channel_fifo_fc(v_adc).rd_en,
    injectdbiterr => s_adc_channel_fifo_fc(v_adc).injectdbiterr,
    injectsbiterr => s_adc_channel_fifo_fc(v_adc).injectsbiterr,
    --sleep => s_adc_channel_fifo_fc(v_adc).sleep,
    dout => s_adc_channel_fifo_fc(v_adc).dout,
    full => s_adc_channel_fifo_fc(v_adc).full,
    --almost_full => s_adc_channel_fifo_fc(v_adc).almost_full,
    wr_ack => s_adc_channel_fifo_fc(v_adc).wr_ack,
    overflow => s_adc_channel_fifo_fc(v_adc).overflow,
    empty => s_adc_channel_fifo_fc(v_adc).empty,
    --almost_empty => s_adc_channel_fifo_fc(v_adc).almost_empty,
    valid => s_adc_channel_fifo_fc(v_adc).valid,
    underflow => s_adc_channel_fifo_fc(v_adc).underflow,
    --rd_data_count => s_adc_channel_fifo_fc(v_adc).rd_data_count,
    --wr_data_count => s_adc_channel_fifo_fc(v_adc).wr_data_count,
    sbiterr => s_adc_channel_fifo_fc(v_adc).sbiterr,
    dbiterr => s_adc_channel_fifo_fc(v_adc).dbiterr,
    wr_rst_busy => s_adc_channel_fifo_fc(v_adc).wr_rst_busy,
    rd_rst_busy => s_adc_channel_fifo_fc(v_adc).rd_rst_busy
  );

--s_adc_channel_fifo_fc(v_adc).srst <= '0';
s_adc_channel_fifo_fc(v_adc).wr_clk <= s_bitclk_div(v_adc);
s_adc_channel_fifo_fc(v_adc).rd_clk <= p_clknet_in.clk40;
s_adc_channel_fifo_fc(v_adc).din <= s_adc_input_fc_temp(v_adc);
--s_adc_channel_fifo_fc(v_adc).wr_en <= '1';
--s_adc_channel_fifo_fc(v_adc).rd_en <= '1';
s_adc_channel_fifo_fc(v_adc).injectdbiterr <= '0';
s_adc_channel_fifo_fc(v_adc).injectsbiterr <= '0';
s_fc_output_word(v_adc) <= s_adc_channel_fifo_fc(v_adc).dout;

        proc_shift_in : process(s_bitclk_se(v_adc)) -- odd data bits clocked in on rising edge of adc clocks 
        begin


            if rising_edge(s_bitclk_se(v_adc)) then
                --if p_adc_mode_in = '0' then
                
                    s_adc_input_hg_temp(v_adc) <= s_adc_input_hg_temp(v_adc)(11 downto 0) & s_adc_channel_sr_hg(v_adc).q;
                    s_adc_input_lg_temp(v_adc) <= s_adc_input_lg_temp(v_adc)(11 downto 0) & s_adc_channel_sr_lg(v_adc).q;
                    s_adc_input_fc_temp(v_adc) <= s_adc_input_fc_temp(v_adc)(11 downto 0) & s_adc_channel_sr_fc(v_adc).q;
                --else 
            
            end if;
        end process;

--ila_adc_fifo_lg : ila_adc_fifo
--port map (
--	clk => s_bitclk(v_adc),

--	probe0 => s_adc_channel_sr_lg(v_adc).d, 
--	probe1 => s_adc_channel_sr_lg(v_adc).q, 
--	probe2 => s_adc_channel_fifo_lg(v_adc).din, 
--	probe3 => s_adc_channel_fifo_lg(v_adc).dout, 
--	probe4 => s_adc_channel_fifo_lg(v_adc).rd_data_count,--(others => '0'),--s_adc_channel_fifo_lg(0).rd_data_count, 
--	probe5 => s_adc_channel_fifo_lg(v_adc).wr_data_count,--(others => '0'),--s_adc_channel_fifo_lg(0).wr_data_count, 
--	probe6(0) => s_adc_channel_fifo_lg(v_adc).full, 
--	probe7(0) => s_adc_channel_fifo_lg(v_adc).overflow, 
--	probe8(0) => s_adc_channel_fifo_lg(v_adc).empty, 
--	probe9(0) => s_adc_channel_fifo_lg(v_adc).almost_empty,--s_adc_channel_fifo_lg(0).sleep,--s_adc_channel_fifo_lg(0).almost_empty, 
--	probe10(0) => s_adc_channel_fifo_lg(v_adc).valid, 
--	probe11(0) => s_adc_channel_fifo_lg(v_adc).underflow, 
--	probe12(0) => s_adc_channel_fifo_lg(v_adc).sbiterr, 
--	probe13(0) => s_adc_channel_fifo_lg(v_adc).dbiterr, 
--	probe14(0) => s_adc_channel_fifo_lg(v_adc).wr_rst_busy,
--	probe15(0) => s_adc_channel_fifo_lg(v_adc).almost_full,
--	probe16(0) => s_adc_channel_fifo_lg(v_adc).rd_clk,
--	probe17(0) => s_adc_channel_fifo_lg(v_adc).wr_clk,
--	probe18(0) => s_adc_channel_fifo_lg(v_adc).rd_rst_busy
	
	
--);

--ila_adc_fifo_fc : ila_adc_fifo
--port map (
--	clk => s_bitclk(v_adc),

--	probe0 => s_adc_channel_sr_fc(v_adc).d, 
--	probe1 => s_adc_channel_sr_fc(v_adc).q, 
--	probe2 => s_adc_channel_fifo_fc(v_adc).din, 
--	probe3 => s_adc_channel_fifo_fc(v_adc).dout, 
--	probe4 => s_adc_channel_fifo_fc(v_adc).rd_data_count,--(others => '0'),--s_adc_channel_fifo_lg(0).rd_data_count, 
--	probe5 => s_adc_channel_fifo_fc(v_adc).wr_data_count,--(others => '0'),--s_adc_channel_fifo_lg(0).wr_data_count, 
--	probe6(0) => s_adc_channel_fifo_fc(v_adc).full, 
--	probe7(0) => s_adc_channel_fifo_fc(v_adc).overflow, 
--	probe8(0) => s_adc_channel_fifo_fc(v_adc).empty, 
--	probe9(0) => s_adc_channel_fifo_fc(v_adc).almost_empty,--s_adc_channel_fifo_lg(0).sleep,--s_adc_channel_fifo_lg(0).almost_empty, 
--	probe10(0) => s_adc_channel_fifo_fc(v_adc).valid, 
--	probe11(0) => s_adc_channel_fifo_fc(v_adc).underflow, 
--	probe12(0) => s_adc_channel_fifo_fc(v_adc).sbiterr, 
--	probe13(0) => s_adc_channel_fifo_fc(v_adc).dbiterr, 
--	probe14(0) => s_adc_channel_fifo_fc(v_adc).wr_rst_busy,
--	probe15(0) => s_adc_channel_fifo_fc(v_adc).almost_full,
--	probe16(0) => s_adc_channel_fifo_fc(v_adc).rd_clk,
--	probe17(0) => s_adc_channel_fifo_fc(v_adc).wr_clk,
--	probe18(0) => s_adc_channel_fifo_fc(v_adc).rd_rst_busy
	
	
--);


proc_phase_reset : process(s_bitclk_se(v_adc)) -- capture adc words synchronous to adc (bit) clock
type t_reset_sm is (st_initialize, st_release_fc_serdes, st_reset_divclk, st_count_bitslips, st_check_fc, st_idle);
variable v_reset_sm : t_reset_sm := st_initialize;
variable v_counter : integer := 0;
variable v_dbitslips, v_dbitslips_offset : integer :=0; 
variable v_phase_offset : integer:= 0;
variable  v_fc_tries: integer := 0;
constant max_fc_tries : integer := 1024;
variable v_idelay_fc : integer := 0;

begin

    if rising_edge(s_bitclk_se(v_adc)) then
        if (p_master_reset_in = '0') then
            case v_reset_sm is
                when st_initialize =>
                    if v_counter < 14*16*64 then
                        s_phase_offset(v_adc)<="11";
                        s_channel_frame_missalignemt(v_adc)<='1';
                        v_reset_sm :=st_reset_divclk;
                        v_dbitslips:=0;
                        v_dbitslips_offset:=0;
                        v_counter:=0;
                        
                        s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                        s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                        s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                        s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                        s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                        s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                        s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';
                        s_channel_frame_missalignemt(v_adc) <= '0';
                
                        s_lg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                        s_hg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                
                        s_hg_idelay_ctrl_reset_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);
                        s_hg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_load(v_adc);
                        s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
                        
                        s_lg_idelay_ctrl_reset_from_sm(v_adc) <= s_lg_idelay_ctrl_reset_from_sm(v_adc); 
                        s_lg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_load(v_adc);
                        s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
                        
                        s_adc_channel_fifo_hg(v_adc).wr_en <= '1';
                        s_adc_channel_fifo_hg(v_adc).rd_en <= '1';
                        s_adc_channel_fifo_lg(v_adc).wr_en <= '1';
                        s_adc_channel_fifo_lg(v_adc).rd_en <= '1';
                        s_adc_channel_fifo_fc(v_adc).wr_en <= '1';
                        s_adc_channel_fifo_fc(v_adc).rd_en <= '1';
                        
                        s_adc_channel_fifo_fc(v_adc).srst <= '1';
                        s_adc_channel_fifo_lg(v_adc).srst <= '1';
                        s_adc_channel_fifo_lg(v_adc).srst <= '1';
                    else
                        v_counter:= v_counter+1;
                    end if;
                when st_reset_divclk =>
                    s_channel_frame_missalignemt(v_adc)<='1';
                    s_phase_offset(v_adc)<="11";
                    if v_dbitslips_offset = v_dbitslips then
                        s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                        v_reset_sm :=st_check_fc;
                    else
                        s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                        v_reset_sm :=st_reset_divclk;
                    end if;
                    if v_dbitslips<7 then
                        v_dbitslips:=v_dbitslips+1;
                    else
                        v_dbitslips:=0;
                    end if;
                    v_counter:=0;
                    s_adc_channel_fifo_hg(v_adc).wr_en <= '1';
                    s_adc_channel_fifo_hg(v_adc).rd_en <= '1';
                    s_adc_channel_fifo_lg(v_adc).wr_en <= '1';
                    s_adc_channel_fifo_lg(v_adc).rd_en <= '1';
                    s_adc_channel_fifo_fc(v_adc).wr_en <= '1';
                    s_adc_channel_fifo_fc(v_adc).rd_en <= '1';
                    
                    s_adc_channel_fifo_fc(v_adc).srst <= '1';
                    s_adc_channel_fifo_hg(v_adc).srst <= '1';
                    s_adc_channel_fifo_lg(v_adc).srst <= '1';
                
                when st_check_fc =>
                    s_channel_frame_missalignemt(v_adc)<='1';
                    if v_dbitslips<7 then
                        v_dbitslips:=v_dbitslips+1;
                    else
                        v_dbitslips:=0;
                    end if;
                    s_adc_channel_fifo_hg(v_adc).wr_en <= '1';
                    s_adc_channel_fifo_hg(v_adc).rd_en <= '1';
                    s_adc_channel_fifo_lg(v_adc).wr_en <= '1';
                    s_adc_channel_fifo_lg(v_adc).rd_en <= '1';
                    s_adc_channel_fifo_fc(v_adc).wr_en <= '1';
                    s_adc_channel_fifo_fc(v_adc).rd_en <= '1';
                    
                    s_adc_channel_fifo_fc(v_adc).srst <= '0';
                    s_adc_channel_fifo_hg(v_adc).srst <= '0';
                    s_adc_channel_fifo_lg(v_adc).srst <= '0';                
                    
                    if v_counter < 14*16*64 then
                        if s_adc_channel_fifo_fc(v_adc).din(8 downto 5) = "1100" then
                            v_reset_sm :=st_idle;
                            s_phase_offset(v_adc)<="01";
--                        elsif s_adc_channel_fifo_fc(v_adc).din(8 downto 5) = "1000" then
--                            v_reset_sm :=st_idle;
--                            s_phase_offset(v_adc)<="00";
                        elsif s_adc_channel_fifo_fc(v_adc).din(8 downto 5) = "1110" then
                            v_reset_sm :=st_idle;
                            s_phase_offset(v_adc)<="10";
                        else
                            v_reset_sm :=st_reset_divclk;
                            s_phase_offset(v_adc)<="11";
                            if v_dbitslips_offset<7 then
                                v_dbitslips_offset:=v_dbitslips_offset+1;
                            else
                                v_dbitslips_offset:=0;
                            end if;

                            s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                        end if;
                    else
                        v_counter:=v_counter+1;
                    end if;
                    
                
                when st_idle =>
                    s_channel_frame_missalignemt(v_adc)<='0';
                    
                    s_adc_channel_fifo_hg(v_adc).wr_en <= s_adc_channel_fifo_lg_debug(v_adc).wr_en;
                    s_adc_channel_fifo_hg(v_adc).rd_en <= s_adc_channel_fifo_lg_debug(v_adc).rd_en;
                    s_adc_channel_fifo_lg(v_adc).wr_en <= s_adc_channel_fifo_lg_debug(v_adc).wr_en;
                    s_adc_channel_fifo_lg(v_adc).rd_en <= s_adc_channel_fifo_lg_debug(v_adc).rd_en;
                    s_adc_channel_fifo_fc(v_adc).wr_en <= s_adc_channel_fifo_lg_debug(v_adc).wr_en;
                    s_adc_channel_fifo_fc(v_adc).rd_en <= s_adc_channel_fifo_lg_debug(v_adc).rd_en;
                    
                    s_adc_channel_fifo_fc(v_adc).srst <= s_adc_channel_fifo_lg_debug(v_adc).srst; 
                    s_adc_channel_fifo_hg(v_adc).srst <= s_adc_channel_fifo_lg_debug(v_adc).srst;
                    s_adc_channel_fifo_lg(v_adc).srst <= s_adc_channel_fifo_lg_debug(v_adc).srst;   
                    v_counter:=0;
                    if (p_adc_readout_control_in.channel_reset(v_adc) = '1') then
                        v_reset_sm :=st_initialize;
                    end if;
                    
                when others =>
                    s_channel_frame_missalignemt(v_adc)<='1';
                    v_reset_sm :=st_initialize;
                    
            end case;
        else
            v_counter:=0;
            v_reset_sm:=st_initialize;
        end if;
    end if;

end process;




end generate;

--ila_adc_fifo_ch0 : ila_adc_fifo
--port map (
--	clk => s_bitclk(0),

--	probe0 => s_adc_channel_sr_lg(0).d, 
--	probe1 => s_adc_channel_sr_lg(0).q, 
--	probe2 => s_adc_channel_fifo_lg(0).din, 
--	probe3 => s_adc_channel_fifo_lg(0).dout, 
--	probe4 => s_adc_channel_fifo_lg(0).rd_data_count,--(others => '0'),--s_adc_channel_fifo_lg(0).rd_data_count, 
--	probe5 => s_adc_channel_fifo_lg(0).wr_data_count,--(others => '0'),--s_adc_channel_fifo_lg(0).wr_data_count, 
--	probe6(0) => s_adc_channel_fifo_lg(0).full, 
--	probe7(0) => s_adc_channel_fifo_lg(0).overflow, 
--	probe8(0) => s_adc_channel_fifo_lg(0).empty, 
--	probe9(0) => s_adc_channel_fifo_lg(0).almost_empty,--s_adc_channel_fifo_lg(0).sleep,--s_adc_channel_fifo_lg(0).almost_empty, 
--	probe10(0) => s_adc_channel_fifo_lg(0).valid, 
--	probe11(0) => s_adc_channel_fifo_lg(0).underflow, 
--	probe12(0) => s_adc_channel_fifo_lg(0).sbiterr, 
--	probe13(0) => s_adc_channel_fifo_lg(0).dbiterr, 
--	probe14(0) => s_adc_channel_fifo_lg(0).wr_rst_busy,
--	probe15(0) => s_adc_channel_fifo_lg(0).almost_full,
--	probe16(0) => s_adc_channel_fifo_lg(0).rd_clk,
--	probe17(0) => s_adc_channel_fifo_lg(0).wr_clk,
--	probe18(0) => s_adc_channel_fifo_lg(0).rd_rst_busy
	
	
--);






gen_phase_offset: for v_adc in 0 to 5 generate

s_adc_readout.hg_data(v_adc)<= s_adc_channel_fifo_hg(v_adc).dout; --s_hg_adc_output_word;
s_adc_readout.lg_data(v_adc)<= s_adc_channel_fifo_lg(v_adc).dout; --s_lg_adc_output_word;
s_adc_readout.fc_data(v_adc)<= s_adc_channel_fifo_fc(v_adc).dout; --s_fc_output_word; 


end generate;    



        s_adc_readout.readout_initialized<= s_channel_frame_missalignemt(0)
                                            and s_channel_frame_missalignemt(1)
                                            and s_channel_frame_missalignemt(2)
                                            and s_channel_frame_missalignemt(3)
                                            and s_channel_frame_missalignemt(4)
                                            and s_channel_frame_missalignemt(5);
        s_adc_readout.mb_adc_config_control.mode <= '0';
        s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
        s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
        s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
        s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"b5";     
        s_adc_readout.mb_adc_config_control.adc_registers(3) <= x"00";
        s_adc_readout.mb_adc_config_control.adc_registers(4) <= x"00";


end behavioral;





----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/27/2020 07:28:16 PM
-- Design Name: 
-- Module Name: db6_adc_interface_fifo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

---- Uncomment the following library declaration if using
---- arithmetic functions with Signed or Unsigned values
----use IEEE.NUMERIC_STD.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx leaf cells in this code.
----library UNISIM;
----use UNISIM.VComponents.all;

--entity db6_adc_interface_fifo is
----  Port ( );
--end db6_adc_interface_fifo;

--architecture Behavioral of db6_adc_interface_fifo is

--begin


--end Behavioral;
