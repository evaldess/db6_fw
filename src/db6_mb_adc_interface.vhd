----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Saturio
--           Sam Silverstein
--           Fernando Carrio 
-- 
-- Create Date: 10/10/2018 01:07:45 PM
-- Design Name: 
-- Module Name: db6_mb_adc_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_mb_adc_interface is
  Port ( 
  p_master_reset_in		 	: in  std_logic;
  p_clk40_in             : in  std_logic;
  p_clk_120_in         : in  std_logic;
  p_clk_240_in         : in  std_logic;
  p_refclk_in             : in  std_logic;
  p_adc_data0_p_in     : in  std_logic_vector (5 downto 0);
  p_adc_data0_n_in     : in  std_logic_vector (5 downto 0);
  p_adc_data1_p_in     : in  std_logic_vector (5 downto 0);
  p_adc_data1_n_in     : in  std_logic_vector (5 downto 0);
  p_adc_bitclk_p_in     : in  std_logic_vector (5 downto 0);
  p_adc_bitclk_n_in     : in  std_logic_vector (5 downto 0);
  p_adc_frameclk_p_in : in  std_logic_vector (5 downto 0);
  p_adc_frameclk_n_in : in  std_logic_vector (5 downto 0);
  p_clkdiv_out        : out std_logic;
  p_framealigndone_out    : out std_logic_vector (5 downto 0);
  p_adcdata_out        : out t_adc_data_type
  );
  
end db6_mb_adc_interface;

architecture Behavioral of db6_mb_adc_interface is

	signal s_intclk_aux : std_logic_vector(5 downto 0);
	signal s_intclkdiv_aux : std_logic_vector(5 downto 0);
	signal s_intclkdiv_aux_i : std_logic_vector(5 downto 0);
	
	signal s_intclk : std_logic_vector(11 downto 0);
	signal s_intclkdiv : std_logic_vector(11 downto 0);

	signal s_control0           			 		: std_logic_vector(35 downto 0);
	signal s_control1           			 		: std_logic_vector(35 downto 0);
	signal s_async_out           			 	: std_logic_vector(63 downto 0);
	signal s_sync_out           			 		: std_logic_vector(31 downto 0);
	signal s_async_in           			 		: std_logic_vector(63 downto 0);
	signal s_data_p           			 		: std_logic_vector(11 downto 0);
	signal s_data_n             		    		: std_logic_vector(11 downto 0);
	signal s_adc_frameclk_p_buf           	: std_logic_vector(5 downto 0);
	signal s_adc_frameclk_n_buf           	: std_logic_vector(5 downto 0);
	signal s_adc_bitclk_n_buf           		: std_logic_vector(5 downto 0);
	signal s_adc_bitclk_p_buf           		: std_logic_vector(5 downto 0);
	signal s_adc_bitclk_p_buf_i           	: std_logic_vector(11 downto 0);
	signal s_adc_bitclk_n_buf_i           	: std_logic_vector(11 downto 0);
	signal s_wordaligned           				: std_logic_vector(11 downto 0);
	signal s_framealigndonei						: std_logic_vector (5 downto 0);
	signal s_rst_bufr_o								: std_logic_vector (5 downto 0);
	signal s_resetiser_o							: std_logic_vector (5 downto 0);
	--type data_out_type is array (0 to 11) of std_logic_vector(13 downto 0);
	signal s_data_out								:t_adc_data_type;
	signal s_data_frame								:t_adc_data_type;
	signal s_adcdataouti							:t_adc_data_type;
	signal s_extern									:std_logic:='0';
--	signal reset									:std_logic:='0';
	signal s_resetconfig									:std_logic:='0';
	signal s_oversampled							:std_logic_vector (3 downto 0);
	
	type t_type_count is array (0 to 5) of std_logic_vector(4 downto 0);
	type t_type_data is array (0 to 5) of std_logic_vector(7 downto 0);
	signal s_countfrombit							:t_type_count;
	signal s_countout								:t_type_count;
	signal s_bitserdesout								:t_type_data;
	signal s_bitclkoutmon								:std_logic_vector(5 downto 0);
	signal s_bitclkoutmonbuff								:std_logic_vector(5 downto 0);

	signal s_reset_adc								:std_logic_vector (11 downto 0);
	signal s_reset_adc_frame						:std_logic_vector (5 downto 0);
	
--	signal clk0										:std_logic := '0';
--	signal clk120									:std_logic := '0';
--	signal clk240									:std_logic := '0';
	signal s_rstbufr									:std_logic_vector(5 downto 0) := (others=>'0');

--attribute keep_hierarchy : string;
--attribute keep_hierarchy of behavioral : architecture is "yes";	

attribute keep : string;
attribute keep of s_data_out, s_intclkdiv_aux: signal is "true";

begin


p_clkdiv_out <= s_intclkdiv(0);
--reset 		<= async_out(0);
--resetconfig <= async_out(1); -- not used
--extern 		<= async_out(2);
--clk0			<= clk_i;
i_idelayctrl_34 : idelayctrl
	port map (
		rdy 		=> open,
		refclk 	=> p_refclk_in,
		rst 		=> s_async_out(0)
	);

i_delayctrl_33 : idelayctrl
	port map (
		rdy 		=> open,
		refclk 	=> p_refclk_in,
		rst 		=> s_async_out(0)
	);
	
i_delayctrl_32 : idelayctrl
	port map (
		rdy 		=> open,
		refclk 	=> p_refclk_in,
		rst 		=> s_async_out(0)
	);

--process(clk_i)
--begin
--	if rising_edge(clk_i) then
		p_adcdata_out <= s_adcdataouti;
--	end if;
--end process;
--icon_inst : entity work.icon
--  port map (
--    control0 => control0,
--    control1 => control1
--	 );
gen_clk: for i in 0 to 5 generate

	s_intclk(2*i) 		<= s_intclk_aux(i);
	s_intclk(2*i+1) 		<= s_intclk_aux(i);
	s_intclkdiv(2*i) 	<= s_intclkdiv_aux(i);
	s_intclkdiv(2*i+1) 	<= s_intclkdiv_aux(i);
	
	s_reset_adc(2*i) 	<= s_reset_adc_frame(i);
	s_reset_adc(2*i+1) 	<= s_reset_adc_frame(i);
	
end generate gen_clk;


--gen_adc_bnk: for i in 0 to 11 generate
--	i_adc_dual: entity adc_lib.adc_dual
--	port map(
--		reset               => reset_adc(i),
--		adc_data_p          => data_p(i),
--		adc_data_n          => data_n(i),
--		intclk 				  => intclk(i),
--		intclkdiv 			  => intclkdiv(i),
--		refclk	 			  => refclk,	
--		cnt_value_i	 		  => s_countfrombit(3),	
--		clkena              => '1',      
--		data0_out           => data_out(i)
--	);
--	i_tri_phase_aligner: entity work.aligner3phadc port map(
--		reset_i	=> not(framealigndonei(i/2)),
--		data_i	=> data_out(i),
--		sync_i	=> data_out(i)(3),
--		clk_i		=> clk_i,
--		clk120_i => clk_120_i,
--		clk240_i => clk_240_i,
--		ready_o 	=> open,
--		data_o 	=> adcdataouti(i)
--	);
--end generate gen_adc_bnk;



--inst_pllaligner : entity work.pllaligner
--  port map
--   (
--    clk_in1 	=> clk_i,
--    clk_out1 	=> clk0,
--    clk_out2 	=> clk120,
--    clk_out3 	=> clk240,
--    reset  		=> reset_i,
--    locked 		=> open);


gen_adc_frame_bnk: for i in 0 to 5 generate

		s_rstbufr(i) 				<= s_rst_bufr_o(i);
		s_reset_adc_frame(i) 	<= s_resetiser_o(i);

--adc_frame_inst : entity adc_lib.adc_frame
--	port map (
--		datd0_n			=> adc_frameclk_n_buf(i),		-- in 
--		datd0_p			=> adc_frameclk_p_buf(i),		-- in 
--		datclk_bufio	=> bitclkoutmonbuff(i),				-- in 
--		datclk			=> intclk(2*i),				-- in 
--		datclkdiv		=> intclkdiv(2*i),			-- in 
--		refclk	 		=>	refclk,	
--		clk_i		 		=>	clk_i,	
--		datrstin			=> reset_adc_frame(i),				-- in 
--		datena			=> '1',				-- in 
--		datdone			=> '1',		-- in 
--		framealigndone	=> framealigndonei(i),		-- in 
--		rst_bufr_o		=> rst_bufr_o(i),		-- in 
--		resetiser_o		=> resetiser_o(i),		-- in 
--		datout			=> data_frame(i));	
--end generate gen_adc_frame_bnk;


p_framealigndone_out <= s_framealigndonei;

	gen_buf:
		for i in 0 to 5 generate
		begin
		
		ibufds_diff_out_data0 : ibufds_diff_out
		generic map (
			iostandard => "default")
		port map (
			o   => s_data_p(2*i),
			ob  => s_data_n(2*i),
			i   => p_adc_data0_p_in(i),
			ib  => p_adc_data0_n_in(i)
		);  
		ibufds_diff_out_data1 : ibufds_diff_out
		generic map (
			iostandard => "default")
		port map (
			o   => s_data_p(2*i+1),
			ob  => s_data_n(2*i+1),
			i   => p_adc_data1_p_in(i),
			ib  => p_adc_data1_n_in(i)
		);  
		ibufds_diff_out_frameclk : ibufds_diff_out
		generic map (
			iostandard => "default")
		port map (
			o   => s_adc_frameclk_p_buf(i),
			ob  => s_adc_frameclk_n_buf(i),
			i   => p_adc_frameclk_p_in(i),
			ib  => p_adc_frameclk_n_in(i)
		);  

		ibufds_diff_out_bitclk : ibufds_diff_out
		generic map (
			iostandard => "default")
		port map (
			o   => s_adc_bitclk_p_buf(i),
			ob  => s_adc_bitclk_n_buf(i),
			i   => p_adc_bitclk_p_in(i),
			ib  => p_adc_bitclk_n_in(i)
		);  
	end generate gen_buf;

gen_buffers:
		for i in 0 to 5 generate
		begin

    i_bufg_bitclkoutmonbuff : bufg
	port map (
		o => s_bitclkoutmonbuff(i),
		i => s_adc_bitclk_p_buf_i(i)
	);
	
	i_bufg_intclk_aux : bufg
	port map (
		o => s_intclk_aux(i),
		i => s_adc_bitclk_p_buf_i(i)
	);
	
	i_bufg_intclkdiv_aux : bufgce_div
	generic map (
		bufgce_divide 	=> 7
		)
	port map (
		o   => s_intclkdiv_aux(i),
		ce  => '1',
		clr => s_rstbufr(i),
		i   => s_adc_bitclk_p_buf_i(i)
	);

idelaye2_inst : idelaye3
generic map (
--   cinvctrl_sel 				=> "false",
   delay_src 					=> "idatain",
--   high_performance_mode 	=> "true",	
   delay_type 				=> "fixed", 
   delay_value 				=> 0,      
--   pipe_sel 					=> "false", 
   refclk_frequency 			=> 195.0   
--   signal_pattern 			=> "clock"
)
port map (
   cntvalueout => open, 			
   dataout 		=> s_adc_bitclk_p_buf_i(i),
   casc_in      => '0',
   casc_return  => '0',
   clk 			=> p_refclk_in,       			
   ce 			=> '0',  
   en_vtc       => '0',         		
--   cinvctrl 	=> '0',       				
   cntvaluein 	=> s_countfrombit(0),   				
   datain 		=> '0',           		
   idatain 		=> s_adc_bitclk_p_buf(i), 
   inc 			=> '1',                	
   load 			=> '1',			
--   ldpipeen 	=> '0',       				
   rst 		=> '0'           			
);
end generate gen_buffers;

s_countfrombit(0) 			<= s_async_out(7 downto 3);
s_countfrombit(1) 			<= s_async_out(12 downto 8);
s_countfrombit(2) 			<= s_async_out(17 downto 13);
s_countfrombit(3) 			<= s_async_out(22 downto 18);
s_countfrombit(4) 			<= s_async_out(27 downto 23);
s_countfrombit(5) 			<= s_async_out(32 downto 28);

s_async_in(4 downto 0)  	<= s_countout(0);
s_async_in(9 downto 5)  	<= s_countout(1);
s_async_in(14 downto 10)  <= s_countout(2);
s_async_in(19 downto 15)  <= s_countout(3);
s_async_in(24 downto 20)  <= s_countout(4);
s_async_in(29 downto 25)  <= s_countout(5);


end Behavioral;
