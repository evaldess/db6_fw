----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/24/2020 12:43:56 PM
-- Design Name: 
-- Module Name: db6_commbus_ddr_encoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

library tilecal;
use tilecal.db6_design_package.all;

--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

-- uncomment the following library declaration if instantiating
-- any xilinx primitives in this code.
library unisim;
use unisim.vcomponents.all;

--library gbt;
--use gbt.vendor_specific_gbt_bank_package.all;

entity db6_commbus_ddr_encoder is
  port (
        p_master_reset_in : in std_logic;
        p_loopback_mode_in : in std_logic;
		p_clknet_in : in t_db_clknet;
        p_db_reg_rx_in : in t_db_reg_rx;
        --p_gbt_tx_data_out       : out std_logic_vector(115 downto 0);
        p_commbus_encoder_interface_out : out t_commbus_encoder_interface;
		
		--interfaces
		p_commbus_ddr_in : in t_commbus_ddr;
		p_mb_interface_in : in t_mb_interface;
		p_sem_interface_in : in t_sem_interface;
		p_tdo_remote_in	            : in	std_logic;
		p_system_management_interface_in : in t_system_management_interface;
		p_gbtx_interface_in : in t_gbtx_interface;
		p_serial_id_interface_in : in t_serial_id_interface
		);
		

end db6_commbus_ddr_encoder;

architecture behavioral of db6_commbus_ddr_encoder is

    signal s_commbus_encoder_interface : t_commbus_encoder_interface;
    
    attribute keep : string;
    attribute dont_touch : string;
    attribute keep of s_commbus_encoder_interface  : signal is "true";
    attribute dont_touch of s_commbus_encoder_interface : signal is "true";    

    signal s_db_reg_tx_in      	: t_db_reg_tx;
    signal s_encoder_fifo : t_fifo_commbus_encoder;

--COMPONENT fifo_commbus
--  PORT (
--    clk : IN STD_LOGIC;
--    srst : IN STD_LOGIC;
--    din : IN STD_LOGIC_VECTOR(115 DOWNTO 0);
--    wr_en : IN STD_LOGIC;
--    rd_en : IN STD_LOGIC;
--    injectdbiterr : IN STD_LOGIC;
--    injectsbiterr : IN STD_LOGIC;
--    sleep : IN STD_LOGIC;
--    dout : OUT STD_LOGIC_VECTOR(115 DOWNTO 0);
--    full : OUT STD_LOGIC;
--    wr_ack : OUT STD_LOGIC;
--    overflow : OUT STD_LOGIC;
--    empty : OUT STD_LOGIC;
--    valid : OUT STD_LOGIC;
--    underflow : OUT STD_LOGIC;
--    sbiterr : OUT STD_LOGIC;
--    dbiterr : OUT STD_LOGIC;
--    wr_rst_busy : OUT STD_LOGIC;
--    rd_rst_busy : OUT STD_LOGIC
--  );
--END COMPONENT;

--COMPONENT sr_ram_commbus
--  PORT (
--    D : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
--    CLK : IN STD_LOGIC;
--    CE : IN STD_LOGIC;
--    SCLR : IN STD_LOGIC;
--    SSET : IN STD_LOGIC;
--    Q : OUT STD_LOGIC_VECTOR(119 DOWNTO 0)
--  );
--END COMPONENT;

    
begin

-- register connections
s_db_reg_tx_in(stb_mb) <= p_mb_interface_in.mb_driver.rxword_out.q1(15 downto 0) & p_mb_interface_in.mb_driver.rxword_out.q0(15 downto 0);
s_db_reg_tx_in(stb_mb_q0) <= "00" & p_mb_interface_in.mb_driver.txword_in(23 downto 12) & p_mb_interface_in.mb_driver.rxword_out.q0;
s_db_reg_tx_in(stb_mb_q1) <= "00" & p_mb_interface_in.mb_driver.txword_in(23 downto 12) & p_mb_interface_in.mb_driver.rxword_out.q1;
s_db_reg_tx_in(stb_db_cfbstrobe) <= p_db_reg_rx_in(cfb_strobe_reg);

--on the sm
--s_db_reg_tx_in(stb_db_xadc)    		<= (others=> '0'); --s_temp_data;--5   -- 32-bit xadc sensor data

s_db_reg_tx_in(stb_sem)(31) <= p_sem_interface_in.status_heartbeat;
s_db_reg_tx_in(stb_sem)(30) <= p_sem_interface_in.status_initialization;
s_db_reg_tx_in(stb_sem)(29) <= p_sem_interface_in.status_observation;
s_db_reg_tx_in(stb_sem)(28) <= p_sem_interface_in.status_correction;
s_db_reg_tx_in(stb_sem)(27) <= p_sem_interface_in.status_classification;
s_db_reg_tx_in(stb_sem)(26) <= p_sem_interface_in.status_injection;
s_db_reg_tx_in(stb_sem)(25) <= p_sem_interface_in.status_diagnostic_scan;
s_db_reg_tx_in(stb_sem)(24) <= p_sem_interface_in.status_detect_only;
s_db_reg_tx_in(stb_sem)(23) <= p_sem_interface_in.status_essential;
s_db_reg_tx_in(stb_sem)(22) <= p_sem_interface_in.status_uncorrectable;
s_db_reg_tx_in(stb_sem)(21) <= p_sem_interface_in.uart_tx; --(others=> '0'); --s_sem_data_out;--6
s_db_reg_tx_in(stb_sem)(20 downto 13) <= p_sem_interface_in.monitor_txdata;
s_db_reg_tx_in(stb_sem)(12) <= p_sem_interface_in.monitor_txwrite;
s_db_reg_tx_in(stb_sem)(11) <= p_sem_interface_in.monitor_txfull;

s_db_reg_tx_in(stb_db_sem_icap)    		<= p_sem_interface_in.icap_out;--(others=> '0');

s_db_reg_tx_in(stb_db_xadc_control)(31)         		<= p_system_management_interface_in.xadc_control.reset_in;--(others=> '0'); --s_hv_status;--8
s_db_reg_tx_in(stb_db_xadc_control)(30)         		<= p_system_management_interface_in.xadc_control.user_temp_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(29)         		<= p_system_management_interface_in.xadc_control.vccint_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(28)         		<= p_system_management_interface_in.xadc_control.vccaux_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(27)         		<= p_system_management_interface_in.xadc_control.user_supply0_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(26)         		<= p_system_management_interface_in.xadc_control.user_supply1_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(25)         		<= p_system_management_interface_in.xadc_control.user_supply2_alarm_out;
--s_db_reg_tx_in(stb_db_xadc_control)(24)         		<= p_system_management_interface_in.xadc_control.user_supply3_alarm_out;
s_db_reg_tx_in(stb_db_xadc_control)(23)         		<= p_system_management_interface_in.xadc_control.jtaglocked_out;
s_db_reg_tx_in(stb_db_xadc_control)(22)         		<= p_system_management_interface_in.xadc_control.jtagmodified_out;
s_db_reg_tx_in(stb_db_xadc_control)(21)         		<= p_system_management_interface_in.xadc_control.jtagbusy_out;

s_db_reg_tx_in(stb_gbtxa_reg)(31 downto 30)                   <=p_gbtx_interface_in.gbtx_side;
s_db_reg_tx_in(stb_gbtxa_reg)(29)                   <=p_gbtx_interface_in.busy;
s_db_reg_tx_in(stb_gbtxa_reg)(28 downto 27)                   <=p_clknet_in.gbtx_rxready;
s_db_reg_tx_in(stb_gbtxa_reg)(26 downto 25)                   <=p_clknet_in.gbtx_datavalid;
s_db_reg_tx_in(stb_gbtxa_reg)(7 downto 0)                   <=p_gbtx_interface_in.gbtxa_dpram.doutb;
s_db_reg_tx_in(stb_gbtxa_reg)(16 downto 8)                   <=p_gbtx_interface_in.gbtxa_dpram.addrb;

s_db_reg_tx_in(stb_gbtxb_reg)(31 downto 30)                   <=p_gbtx_interface_in.gbtx_side;
s_db_reg_tx_in(stb_gbtxb_reg)(29)                   <=p_gbtx_interface_in.busy;
s_db_reg_tx_in(stb_gbtxb_reg)(28 downto 27)                   <=p_clknet_in.gbtx_rxready;
s_db_reg_tx_in(stb_gbtxb_reg)(26 downto 25)                   <=p_clknet_in.gbtx_datavalid;
s_db_reg_tx_in(stb_gbtxb_reg)(7 downto 0)                   <=p_gbtx_interface_in.gbtxb_dpram.doutb;
s_db_reg_tx_in(stb_gbtxb_reg)(16 downto 8)                   <=p_gbtx_interface_in.gbtxb_dpram.addrb;

s_db_reg_tx_in(stb_cis_config)    	<= p_db_reg_rx_in(cfb_cis_config); --(others=> '0'); --s_db_cis_config_reg;									--9
-- cs
s_db_reg_tx_in(stb_cs_t)       <= (others=> '0'); --s_cs_timestamp;
s_db_reg_tx_in(stb_cs_s)       <= (others=> '0'); --s_cs_status;

s_db_reg_tx_in(stb_db_serial_id_msb) <= p_serial_id_interface_in.crc_read(7 downto 0) & p_serial_id_interface_in.family_code(7 downto 0) & p_serial_id_interface_in.serial_number(47 downto 32);
s_db_reg_tx_in(stb_db_serial_id_lsb) <= p_serial_id_interface_in.serial_number(31 downto 0);
s_db_reg_tx_in(stb_db_serial_id_status)(15 downto 0) <= p_serial_id_interface_in.crc_calculated(7 downto 0) & p_serial_id_interface_in.crc_read(7 downto 0);
s_db_reg_tx_in(stb_db_serial_id_status)(31) <= p_serial_id_interface_in.busy;
s_db_reg_tx_in(stb_db_serial_id_status)(30) <= p_serial_id_interface_in.crc_ok;

s_db_reg_tx_in(stb_gbt_encoder)(0) <=s_encoder_fifo.rst;
s_db_reg_tx_in(stb_gbt_encoder)(1) <= s_encoder_fifo.wr_en;
s_db_reg_tx_in(stb_gbt_encoder)(2) <= s_encoder_fifo.injectdbiterr;
s_db_reg_tx_in(stb_gbt_encoder)(3) <= s_encoder_fifo.injectsbiterr;
s_db_reg_tx_in(stb_gbt_encoder)(4) <= s_encoder_fifo.sleep;
s_db_reg_tx_in(stb_gbt_encoder)(5) <= s_encoder_fifo.full;
s_db_reg_tx_in(stb_gbt_encoder)(6) <= s_encoder_fifo.wr_ack;
s_db_reg_tx_in(stb_gbt_encoder)(7) <= s_encoder_fifo.overflow;
s_db_reg_tx_in(stb_gbt_encoder)(8) <= s_encoder_fifo.valid;
s_db_reg_tx_in(stb_gbt_encoder)(9) <= s_encoder_fifo.underflow;
s_db_reg_tx_in(stb_gbt_encoder)(10) <= s_encoder_fifo.sbiterr;
s_db_reg_tx_in(stb_gbt_encoder)(11) <= s_encoder_fifo.wr_rst_busy;
s_db_reg_tx_in(stb_gbt_encoder)(12) <= s_encoder_fifo.rd_rst_busy;



-- assembling data words
s_encoder_fifo.clk <= p_clknet_in.clk80;
s_encoder_fifo.rst <= p_master_reset_in;
s_encoder_fifo.wr_en <= '1';
s_encoder_fifo.rd_en <= '1';
s_encoder_fifo.injectdbiterr <= '0';
s_encoder_fifo.injectsbiterr <= '0';
s_encoder_fifo.sleep <= '0';

p_commbus_encoder_interface_out <= s_commbus_encoder_interface;

s_commbus_encoder_interface.commbus_tx_data_out <= s_encoder_fifo.dout;

--i_fifo_gbt_encoder : fifo_gbt_encoder
--  PORT MAP (
--    clk => s_encoder_fifo.clk,
--    srst => s_encoder_fifo.rst,
--    din => s_encoder_fifo.din,
--    wr_en => s_encoder_fifo.wr_en,
--    rd_en => s_encoder_fifo.rd_en,
--    injectdbiterr => s_encoder_fifo.injectdbiterr,
--    injectsbiterr => s_encoder_fifo.injectsbiterr,
--    sleep => s_encoder_fifo.sleep,
--    dout => s_encoder_fifo.dout,
--    full => s_encoder_fifo.full,
--    wr_ack => s_encoder_fifo.wr_ack,
--    overflow => s_encoder_fifo.overflow,
--    empty => s_encoder_fifo.empty,
--    valid => s_encoder_fifo.valid,
--    underflow => s_encoder_fifo.underflow,
--    sbiterr => s_encoder_fifo.sbiterr,
--    dbiterr => s_encoder_fifo.dbiterr,
--    wr_rst_busy => s_encoder_fifo.wr_rst_busy,
--    rd_rst_busy => s_encoder_fifo.rd_rst_busy
--  );

--i_sr_ram_commbus : sr_ram_commbus
--  PORT MAP (
--    D => s_encoder_fifo.din,
--    CLK => s_encoder_fifo.clk,
--    CE => '1',
--    SCLR => s_encoder_fifo.rst ,
--    SSET => '0',
--    Q => s_encoder_fifo.dout
--  );


    proc_db_data_assembler : process(p_commbus_ddr_in.bitclk_tx)
    variable v_counter : integer range 0 to 63 := 0;
    begin
        
        if rising_edge(p_commbus_ddr_in.bitclk_tx) then
            if p_master_reset_in = '1' then
                if p_commbus_ddr_in.bitcnt_tx = 60 then
                    v_counter:=v_counter+1;
                  
                    if p_loopback_mode_in = '0' then
                        s_encoder_fifo.dout(119 downto 16)<= s_encoder_fifo.din(119 downto 16);
                        s_encoder_fifo.dout(15 downto 0)    <= tilecal.db6_design_package.tile_link_crc_compute(s_encoder_fifo.din(115 downto 16))(15 downto 0);    
                        --assemble data
                        --s_encoder_fifo.dout(15 downto 0)    <= tilecal.db6_design_package.tile_link_crc_compute(s_encoder_fifo.din(115 downto 16))(15 downto 0);
                        if v_counter<c_number_of_cfgbus_regs then
                            s_encoder_fifo.din(15+32 downto 16)   <= p_db_reg_rx_in(v_counter);
                        else
                            s_encoder_fifo.din(15+32 downto 16)   <= (others=> '1');
                        end if;
                        
                        if v_counter<c_number_of_gbttx_regs then
                            s_encoder_fifo.din(15+32+32 downto 16+32) <= s_db_reg_tx_in(v_counter);
                        else
                            s_encoder_fifo.din(15+32+32 downto 16+32) <= (others=> '1');
                        end if;
                        
                        s_encoder_fifo.din(85 downto 80)   <= std_logic_vector(to_unsigned(v_counter,6));
                        
                        s_encoder_fifo.din(89 downto 86)             <= "0000";  -- bcr
                        s_encoder_fifo.din(90)             <= p_tdo_remote_in;
                        s_encoder_fifo.din(115 downto 91)             <= (others=> '0');
                        s_encoder_fifo.din(119 downto 116) <= "0101";
                    else
                        s_encoder_fifo.dout(115 downto 16)<= x"F" & std_logic_vector(to_unsigned(v_counter,32)) & std_logic_vector(to_unsigned(v_counter,32))& std_logic_vector(to_unsigned(v_counter,32));
                        s_encoder_fifo.dout(15 downto 0)    <= tilecal.db6_design_package.tile_link_crc_compute(x"F" & std_logic_vector(to_unsigned(v_counter,32)) & std_logic_vector(to_unsigned(v_counter,32))& std_logic_vector(to_unsigned(v_counter,32)))(15 downto 0);
                        s_encoder_fifo.dout(119 downto 116) <= "0110"; 
                    end if;
    
                end if;
            else
                s_encoder_fifo.dout <= x"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
                v_counter:=0;
            end if;
        end if;
    end process;
 
end behavioral;


