----------------------------------------------------------------------------------
-- Company: Stockholm University
-- Engineer:    Eduardo Valdes Santurio 
-- 
-- Create Date: 06/22/2020 04:30:34 PM
-- Design Name: 
-- Module Name: i_db6_commbus_ddr_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_commbus_ddr_interface is
  port ( 
  
        p_clknet_in : in t_db_clknet;
        p_master_reset_in : in std_logic;
        p_db_reg_rx_in : in t_db_reg_rx;
        
        p_commbus_ddr_tx_out  : out t_diff_pair;
        p_commbus_ddr_loopback_tx_out  : out t_diff_pair;
        p_commbus_ddr_clk_out  : out t_diff_pair;
        p_commbus_ddr_rx_in  : in t_diff_pair;
        p_commbus_ddr_loopback_rx_in  : in t_diff_pair;
        p_commbus_ddr_clk_in  : in t_diff_pair;
        
        p_commbus_ddr_interface_out : out t_commbus_ddr_interface; 
        
        --tdo from other fpga
        p_tdo_remote_in	            : in	std_logic;
        
        --interfaces
        p_mb_interface_in : in t_mb_interface;
        p_sem_interface_in : in t_sem_interface;
        p_system_management_interface_in : in t_system_management_interface;
        p_gbtx_interface_in : in t_gbtx_interface;
        p_serial_id_interface_in : in t_serial_id_interface
        --p_commbus_ddr_loopback_tx_out : out t_diff_pair_vector(0 downto 0);
        --p_commbus_ddr_loopback_rx_in : in t_diff_pair_vector(0 downto 0)
  
  );
end db6_commbus_ddr_interface;

architecture Behavioral of db6_commbus_ddr_interface is

attribute keep : string;
attribute dont_touch : string;

signal s_commbus_ddr, s_commbus_ddr_loopback : t_commbus_ddr;

type t_commbus_word_tmr is array (2 downto 0) of std_logic_vector(119 downto 0);
constant c_fc_shape : t_commbus_word_tmr := (x"FFFFFFFFFFFFFFF000000000000000", x"FFFFFFFFFFFFFFF000000000000000", x"FFFFFFFFFFFFFFF000000000000000");
signal s_commbus_ddr_rx_delay, s_commbus_ddr_tx_delay, s_commbus_ddr_loopback_rx_delay, s_commbus_ddr_loopback_tx_delay, s_commbus_ddr_tx_delay_clk_out : t_delay_control;
signal s_commbus_ddr_rx_sr, s_commbus_ddr_tx_sr, s_commbus_ddr_loopback_rx_sr, s_commbus_ddr_loopback_tx_sr : t_sr;
signal s_commbus_bitslice_ddr_rx : std_logic_vector(119 downto 0);
signal s_commbus_word_ddr_rx_fc : std_logic_vector(119 downto 0);
signal s_commbus_word_ddr_tx, s_commbus_word_ddr_tx_fc : std_logic_vector(119 downto 0);

signal s_commbus_encoder_interface, s_commbus_encoder_interface_loopback : t_commbus_encoder_interface;
signal s_commbus_decoder_interface, s_commbus_decoder_interface_loopback : t_commbus_decoder_interface;

component fifo_commbus_rx
  port (
    srst : in std_logic;
    wr_clk : in std_logic;
    rd_clk : in std_logic;
    din : in std_logic_vector(13 downto 0);
    wr_en : in std_logic;
    rd_en : in std_logic;
    injectdbiterr : in std_logic;
    injectsbiterr : in std_logic;
    dout : out std_logic_vector(13 downto 0);
    full : out std_logic;
    wr_ack : out std_logic;
    overflow : out std_logic;
    empty : out std_logic;
    valid : out std_logic;
    underflow : out std_logic;
    sbiterr : out std_logic;
    dbiterr : out std_logic;
    wr_rst_busy : out std_logic;
    rd_rst_busy : out std_logic
  );
end component;

component sr_ram_commbus_rx
  port (
    d : in std_logic_vector(1 downto 0);
    clk : in std_logic;
    ce : in std_logic;
    sclr : in std_logic;
    sset : in std_logic;
    q : out std_logic_vector(1 downto 0)
  );
end component;

COMPONENT sr_ram_commbus
  PORT (
    D : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    CLK : IN STD_LOGIC;
    CE : IN STD_LOGIC;
    SCLR : IN STD_LOGIC;
    SSET : IN STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END COMPONENT;

component pll_commbus
port
 (-- Clock in ports
  -- Clock out ports
  p_commbusclk_out          : out    std_logic;
  p_bitclk_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_locked_out            : out    std_logic;
  p_clk_in           : in     std_logic
 );
end component;

signal s_pll_commbus_control : t_mmcm_control;
--signal s_commbus_ddr_clk_in, s_commbus_ddr_clk_out : std_logic;
--signal s_bitclk, s_frameclk, s_commbusclk80, s_commbusclk : std_logic;
signal s_ddr_tx_shape, s_ddr_rx_shape, s_ddr_loopback_tx_shape, s_ddr_rx_loopback_shape, s_bitclk_shape : std_logic_vector(1 downto 0) := ('1','0');

signal s_bufgce_div_ctrl_reset_from_sm, s_idelay_ctrl_reset_from_sm : std_logic := '0';
signal s_idelay_ctrl_load_from_sm, s_idelay_ctrl_en_vtc_from_sm : std_logic := '0';
signal s_idelay_count_in_from_sm, s_idelay_count_in_from_hw : integer range 0 to 511;

signal s_commbus_ddr_interface, s_commbus_ddr_interface_loopback : t_commbus_ddr_interface;
attribute keep of s_commbus_ddr_interface : signal is "true";
attribute dont_touch of s_commbus_ddr_interface : signal is "true";


COMPONENT fifo_commbus
  PORT (
    clk : IN STD_LOGIC;
    srst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    injectdbiterr : IN STD_LOGIC;
    injectsbiterr : IN STD_LOGIC;
    --sleep : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
    full : OUT STD_LOGIC;
    wr_ack : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC;
    sbiterr : OUT STD_LOGIC;
    dbiterr : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

signal s_commbus_fifo : t_fifo; 

begin

p_commbus_ddr_interface_out <= s_commbus_ddr_interface;

    i_ibufgds_commbus_ddr_clk_in : ibufgds -- ADC bit clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_commbus_ddr.bitclk_rx,             -- Buffer diff_p output
        I  => p_commbus_ddr_clk_in.p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_commbus_ddr_clk_in.n  -- Diff_n buffer input (connect directly to top-level port)
        );

--    i_bufgce_div : bufgce_div
--    generic map (
--    bufgce_divide => 8, -- 1-8
--    -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
--        is_ce_inverted => '0', -- optional inversion for ce
--        is_clr_inverted => '0', -- optional inversion for clr
--        is_i_inverted => '0' -- optional inversion for i
--    )
--    port map (
--        o => s_commbusclk80, -- 1-bit output: buffer
--        ce => '1', -- 1-bit input: buffer enable
--        clr => s_bufgce_div_ctrl_reset_from_sm, -- 1-bit input: asynchronous clear
--        i => s_commbus_ddr_interface.bitclk_rx -- 1-bit input: buffer
--    );

    i_ibufds_commbus_ddr_rx : ibufds  
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_commbus_ddr_rx_delay.idatain,             -- Buffer diff_p output
        I  => p_commbus_ddr_rx_in.p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_commbus_ddr_rx_in.n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_idelaye3_commbus_ddr_rx : idelaye3
      generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 240.48, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_commbus_ddr_rx_delay.casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_commbus_ddr_rx_delay.cntvalueout, -- 9-bit output: counter value output
        dataout => s_commbus_ddr_rx_delay.dataout, -- 1-bit output: delayed data output
        casc_in => s_commbus_ddr_rx_delay.casc_in,-- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_commbus_ddr_rx_delay.casc_return, -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_commbus_ddr_rx_delay.ce, -- 1-bit input: active high enable increment/decrement input
        clk => s_commbus_ddr.bitclk_rx, --s_commbus_ddr_rx_delay.clk, -- 1-bit input: clock input
        cntvaluein => s_commbus_ddr_rx_delay.cntvaluein, -- 9-bit input: counter value input
        datain => s_commbus_ddr_rx_delay.datain,  -- 1-bit input: data input from the iobuf
        en_vtc => s_commbus_ddr_rx_delay.en_vtc, -- 1-bit input: keep delay constant over vt
        idatain => s_commbus_ddr_rx_delay.idatain,  -- 1-bit input: data input from the logic
        inc => s_commbus_ddr_rx_delay.inc,  -- 1-bit input: increment / decrement tap delay input
        load => s_commbus_ddr_rx_delay.load,  -- 1-bit input: load delay_value input
        rst => s_commbus_ddr_rx_delay.rst  -- 1-bit input: asynchronous reset to the delay_value
);
--s_commbus_ddr_rx_delay.clk<= s_commbus_ddr_interface.bitclk_rx;
s_commbus_ddr_rx_delay.rst <= s_idelay_ctrl_reset_from_sm; --p_adc_readout_control_in.lg_idelay_ctrl_reset; 
s_commbus_ddr_rx_delay.load <= s_idelay_ctrl_load_from_sm; --p_adc_readout_control_in.lg_idelay_load;
s_commbus_ddr_rx_delay.en_vtc <= s_idelay_ctrl_en_vtc_from_sm; --p_adc_readout_control_in.lg_idelay_en_vtc;
s_commbus_ddr_rx_delay.cntvaluein <= std_logic_vector(to_unsigned(s_idelay_count_in_from_sm + s_idelay_count_in_from_hw,9));-- p_adc_readout_control_in.lg_idelay_count;
--s_adc_readout.lg_idelay_count <= s_commbus_ddr_rx_in.cntvalueout;

i_iddre1_commbus_rx : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
    q1 => s_ddr_rx_shape(0), -- 1-bit output: registered parallel output 1
    q2 => s_ddr_rx_shape(1), -- 1-bit output: registered parallel output 2
    c => s_commbus_ddr.bitclk_rx, -- 1-bit input: high-speed clock
    cb => s_commbus_ddr.bitclk_rx, -- 1-bit input: inversion of high-speed clock c
    d => s_commbus_ddr_rx_delay.dataout, -- 1-bit input: serial data input
    r => s_commbus_ddr_rx_delay.rst -- 1-bit input: active high async reset
);


i_sr_commbus_ddr_rx : sr_ram_commbus
  port map (
    d => s_commbus_ddr_rx_sr.d,
    clk => s_commbus_ddr.bitclk_rx,--s_commbus_ddr_rx_sr.clk,
    ce => s_commbus_ddr_rx_sr.ce,
    sclr => s_commbus_ddr_rx_sr.sclr,
    sset => s_commbus_ddr_rx_sr.sset,
    q => s_commbus_ddr_rx_sr.q
  );
--s_commbus_ddr_rx_sr.clk <= s_commbus_ddr.bitclk_rx;
s_commbus_ddr_rx_sr.d <= s_ddr_rx_shape(0) & s_ddr_rx_shape(1);
s_commbus_ddr_rx_sr.sclr <= '0';
s_commbus_ddr_rx_sr.ce <= '1';
s_commbus_ddr_rx_sr.sset <= '0';


proc_shift_in : process(s_commbus_ddr.bitclk_rx) -- odd data bits clocked in on rising edge of bitclock
variable v_counter : integer := 0;
variable v_previous_frameclk : std_logic := '1';
variable v_commbus_bitslice_ddr_rx : std_logic_vector(119 downto 0);
begin
    if rising_edge(s_commbus_ddr.bitclk_rx) then
        if p_master_reset_in = '0' then
            if (s_commbus_ddr_rx_sr.q(0) = '0') and (v_previous_frameclk) = '1' then 
                s_commbus_ddr.bitcnt_rx<=0;
                if s_commbus_ddr.bitcnt_rx=120 then
                    case v_commbus_bitslice_ddr_rx(119 downto 116) is

                        when "0110" =>
                        
                            if s_commbus_ddr.locked_counter = 15 then
                                s_commbus_ddr.locked <= '1';
                                s_commbus_ddr.datavalid <= '0';
                                s_commbus_ddr.dataloopback <= '1';
                                s_commbus_ddr.datareset <= '0';
                                s_commbus_ddr.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            else
                                s_commbus_ddr.locked_counter<=s_commbus_ddr.locked_counter+1;
                            end if;
                            
                        when "0101" =>
                            if s_commbus_ddr.locked_counter = 15 then
                                s_commbus_ddr.locked <= '1';
                                s_commbus_ddr.datavalid <= '1';
                                s_commbus_ddr.dataloopback <= '0';
                                s_commbus_ddr.datareset <= '0';
                                s_commbus_ddr.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            else
                                s_commbus_ddr.locked_counter<=s_commbus_ddr.locked_counter+1;
                            end if;
                            
                        when "1010" =>
                            if s_commbus_ddr.locked_counter = 15 then
                                s_commbus_ddr.locked <= '1';
                                s_commbus_ddr.datavalid <= '0';
                                s_commbus_ddr.dataloopback <= '0';
                                s_commbus_ddr.datareset <= '1';
                                s_commbus_ddr.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            else
                                s_commbus_ddr.locked_counter<=s_commbus_ddr.locked_counter+1;
                            end if;
                                                    
                        when others =>
                        
                            if s_commbus_ddr.locked = '1' and s_commbus_ddr.locked_counter>0 then
                                s_commbus_ddr.locked_counter<=s_commbus_ddr.locked_counter-1;
                            else
                                s_commbus_ddr.locked_counter<=0;
                            end if;
                            
                            s_commbus_ddr.locked <= '0';
                            s_commbus_ddr.datavalid <= '0';
                            s_commbus_ddr.dataloopback <= '0';
                            s_commbus_ddr.datareset <= '0';
                            s_commbus_ddr.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            
                    end case; 
--                    if v_commbus_bitslice_ddr_rx(119 downto 116) = "0110" then
--                        s_commbus_ddr.locked <= '1';
--                        s_commbus_ddr.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
--                    else
--                        s_commbus_ddr.locked <= '0';
--                    end if;
                --else
                end if;
            --elsif (s_commbus_ddr_rx_sr.q(0) = '1') and v_previous_frameclk = '0' then
            else
                s_commbus_ddr.bitcnt_rx<=s_commbus_ddr.bitcnt_rx+1;
            end if;
    
            v_previous_frameclk := s_commbus_ddr_rx_sr.q(0);
            v_commbus_bitslice_ddr_rx := v_commbus_bitslice_ddr_rx(118 downto 0) & s_commbus_ddr_rx_sr.q(1);
        else
            v_counter:= 0;
        end if;
        
    end if;
end process;


i_db6_commbus_ddr_decoder : entity tilecal.db6_commbus_ddr_decoder
  port map(
        p_master_reset_in => p_master_reset_in,
		p_clknet_in => p_clknet_in,
        p_db_reg_rx_in => p_db_reg_rx_in,

		--interfaces
		p_commbus_ddr_in => s_commbus_ddr,
		p_commbus_decoder_interface_out => s_commbus_decoder_interface,
		p_clknet_out => s_commbus_ddr_interface.clknet,
		p_db_reg_rx_out => s_commbus_ddr_interface.db_reg_rx,
		p_mb_interface_out => s_commbus_ddr_interface.mb_interface,
		p_sem_interface_out => s_commbus_ddr_interface.sem_interface,
		p_tdo_remote_out => s_commbus_ddr_interface.tdo_remote,
		p_system_management_interface_out => s_commbus_ddr_interface.system_management_interface,
		p_gbtx_interface_out => s_commbus_ddr_interface.gbtx_interface,
		p_serial_id_interface_out => s_commbus_ddr_interface.serial_id_interface
		);

s_commbus_ddr_interface.commbus_decoder_interface <= s_commbus_decoder_interface;     

--i_fifo_commbus_rx : fifo_commbus
--  PORT MAP (
--    clk => s_frameclk,
--    srst => s_commbus_fifo.srst,
--    din => s_commbus_fifo.din,
--    wr_en => s_commbus_fifo.wr_en,
--    rd_en => s_commbus_fifo.rd_en,
--    injectdbiterr => s_commbus_fifo.injectdbiterr,
--    injectsbiterr => s_commbus_fifo.injectsbiterr,
--    --sleep => s_commbus_fifo.sleep,
--    dout => s_commbus_fifo.dout,
--    full => s_commbus_fifo.full,
--    wr_ack => s_commbus_fifo.wr_ack,
--    overflow => s_commbus_fifo.overflow,
--    empty => s_commbus_fifo.empty,
--    valid => s_commbus_fifo.valid,
--    underflow => s_commbus_fifo.underflow,
--    sbiterr => s_commbus_fifo.sbiterr,
--    dbiterr => s_commbus_fifo.dbiterr,
--    wr_rst_busy => s_commbus_fifo.wr_rst_busy,
--    rd_rst_busy => s_commbus_fifo.rd_rst_busy
--  );






--transmitter
    --s_commbus_ddr_interface.refclk_tx <= p_clknet_in.clk80;
    
    i_pll_commbus_clk_out : pll_commbus
       port map ( 
      -- Clock out ports  
       p_commbusclk_out => s_commbus_ddr.refclk_tx,
       p_bitclk_out => s_commbus_ddr.bitclk_tx,
      -- Dynamic reconfiguration ports             
       p_daddr_in => s_pll_commbus_control.daddr_in,
       p_dclk_in => p_clknet_in.clk40,
       p_den_in => s_pll_commbus_control.den_in,
       p_din_in => s_pll_commbus_control.din_in,
       p_dout_out => s_pll_commbus_control.dout_out,
       p_drdy_out => s_pll_commbus_control.drdy_out,
       p_dwe_in => s_pll_commbus_control.dwe_in,
      -- Status and control signals                
       p_reset_in => s_pll_commbus_control.reset_in,
       p_locked_out => s_pll_commbus_control.locked_out,
       -- Clock in ports
       p_clk_in => p_clknet_in.clk80
     );

    i_odelaye3_commbus_ddr_clk_out : odelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- (time, count)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- output delay tap setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 240.48, -- idelayctrl clock input frequency in mhz (values).
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
    )
    port map (
        casc_out => s_commbus_ddr_tx_delay_clk_out.casc_out, -- 1-bit output: cascade delay output to idelay input cascade
        cntvalueout => s_commbus_ddr_tx_delay_clk_out.cntvalueout, -- 9-bit output: counter value output
        dataout => s_commbus_ddr_tx_delay_clk_out.dataout, -- 1-bit output: delayed data from odatain input port
        casc_in => s_commbus_ddr_tx_delay_clk_out.casc_in , -- 1-bit input: cascade delay input from slave idelay cascade_out
        casc_return => s_commbus_ddr_tx_delay_clk_out.casc_return, -- 1-bit input: cascade delay returning from slave idelay dataout
        ce => s_commbus_ddr_tx_delay_clk_out.ce , -- 1-bit input: active high enable increment/decrement input
        clk => s_commbus_ddr.bitclk_tx, -- 1-bit input: clock input
        cntvaluein => s_commbus_ddr_tx_delay_clk_out.cntvaluein, -- 9-bit input: counter value input
        en_vtc => s_commbus_ddr_tx_delay_clk_out.en_vtc, -- 1-bit input: keep delay constant over vt
        inc => s_commbus_ddr_tx_delay_clk_out.inc, -- 1-bit input: increment / decrement tap delay input
        load => s_commbus_ddr_tx_delay_clk_out.load, -- 1-bit input: load delay_value input
        odatain => s_commbus_ddr_tx_delay_clk_out.odatain, -- 1-bit input: data input
        rst => s_commbus_ddr_tx_delay_clk_out.rst -- 1-bit input: asynchronous reset to the delay_value
    );
    
        s_commbus_ddr_tx_delay_clk_out.casc_in <= '0';
        s_commbus_ddr_tx_delay_clk_out.casc_return <= '0';
        s_commbus_ddr_tx_delay_clk_out.ce <= '0';
        s_commbus_ddr_tx_delay_clk_out.cntvaluein <= (others => '0');
        s_commbus_ddr_tx_delay_clk_out.en_vtc <= '1';
        s_commbus_ddr_tx_delay_clk_out.inc <= '0';
        s_commbus_ddr_tx_delay_clk_out.load <= '0';
        s_commbus_ddr_tx_delay_clk_out.rst <= '0';
    
    i_oddre1_commbus_clk_out : oddre1
    generic map (
        is_c_inverted => '0', -- optional inversion for c
        is_d1_inverted => '0', -- optional inversion for d1
        is_d2_inverted => '0', -- optional inversion for d2
        srval => '0' -- initializes the oddre1 flip-flops to the specified value (1'b0, 1'b1)
    )
        port map (
        q => s_commbus_ddr_tx_delay_clk_out.odatain, -- 1-bit output: data output to iob
        c => s_commbus_ddr.bitclk_tx, -- 1-bit input: high-speed clock input
        d1 => s_bitclk_shape(0), -- 1-bit input: parallel data input 1
        d2 => s_bitclk_shape(1), -- 1-bit input: parallel data input 2
        sr => p_master_reset_in -- 1-bit input: active high async reset
    );

    i_OBUFDS_commbus_clk_out : OBUFDS
        port map (
        O => p_commbus_ddr_clk_out.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_commbus_ddr_clk_out.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_commbus_ddr_tx_delay_clk_out.dataout-- s_clknet_out.mb_clk40(1) -- s_gbt40_q1_delay_control.dataout--s_clknet_out.mb_clk40(1) -- 1-bit input: Buffer input
        );


    i_odelaye3_commbus_ddr_data_out : odelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- (time, count)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- output delay tap setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 240.48, -- idelayctrl clock input frequency in mhz (values).
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
    )
    port map (
        casc_out => s_commbus_ddr_tx_delay.casc_out, -- 1-bit output: cascade delay output to idelay input cascade
        cntvalueout => s_commbus_ddr_tx_delay.cntvalueout, -- 9-bit output: counter value output
        dataout => s_commbus_ddr_tx_delay.dataout, -- 1-bit output: delayed data from odatain input port
        casc_in => s_commbus_ddr_tx_delay.casc_in , -- 1-bit input: cascade delay input from slave idelay cascade_out
        casc_return => s_commbus_ddr_tx_delay.casc_return, -- 1-bit input: cascade delay returning from slave idelay dataout
        ce => s_commbus_ddr_tx_delay.ce , -- 1-bit input: active high enable increment/decrement input
        clk => s_commbus_ddr.bitclk_tx, -- 1-bit input: clock input
        cntvaluein => s_commbus_ddr_tx_delay.cntvaluein, -- 9-bit input: counter value input
        en_vtc => s_commbus_ddr_tx_delay.en_vtc, -- 1-bit input: keep delay constant over vt
        inc => s_commbus_ddr_tx_delay.inc, -- 1-bit input: increment / decrement tap delay input
        load => s_commbus_ddr_tx_delay.load, -- 1-bit input: load delay_value input
        odatain => s_commbus_ddr_tx_delay.odatain, -- 1-bit input: data input
        rst => s_commbus_ddr_tx_delay.rst -- 1-bit input: asynchronous reset to the delay_value
    );
    
        s_commbus_ddr_tx_delay.casc_in <= '0';
        s_commbus_ddr_tx_delay.casc_return <= '0';
        s_commbus_ddr_tx_delay.ce <= '0';
        s_commbus_ddr_tx_delay.cntvaluein <= (others => '0');
        s_commbus_ddr_tx_delay.en_vtc <= '1';
        s_commbus_ddr_tx_delay_clk_out.inc <= '0';
        s_commbus_ddr_tx_delay.load <= '0';
        s_commbus_ddr_tx_delay.rst <= '0';
    
    i_oddre1_commbus_data_out : oddre1
    generic map (
        is_c_inverted => '0', -- optional inversion for c
        is_d1_inverted => '0', -- optional inversion for d1
        is_d2_inverted => '0', -- optional inversion for d2
        srval => '0' -- initializes the oddre1 flip-flops to the specified value (1'b0, 1'b1)
    )
        port map (
        q => s_commbus_ddr_tx_delay.odatain, -- 1-bit output: data output to iob
        c => s_commbus_ddr.bitclk_tx, -- 1-bit input: high-speed clock input
        d1 => s_ddr_tx_shape(0), -- 1-bit input: parallel data input 1
        d2 => s_ddr_tx_shape(1), -- 1-bit input: parallel data input 2
        sr => p_master_reset_in -- 1-bit input: active high async reset
    );

    i_OBUFDS_commbus_data_out : OBUFDS
        port map (
        O => p_commbus_ddr_tx_out.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_commbus_ddr_tx_out.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_commbus_ddr_tx_delay.dataout-- s_clknet_out.mb_clk40(1) -- s_gbt40_q1_delay_control.dataout--s_clknet_out.mb_clk40(1) -- 1-bit input: Buffer input
        );


    s_commbus_word_ddr_tx_fc<= (c_fc_shape(0) and c_fc_shape(1)) or (c_fc_shape(1) and c_fc_shape(2)) or (c_fc_shape(2) and c_fc_shape(0));
    
    proc_tx : process(s_commbus_ddr.bitclk_tx)
    --variable v_counter: integer :=0;
    variable v_commbus_word_ddr_tx : std_logic_vector(119 downto 0);
    begin
    
        if rising_edge(s_commbus_ddr.bitclk_tx) then
            if p_master_reset_in = '0' then
                if s_commbus_ddr.bitcnt_tx<120 then
                    s_commbus_ddr.bitcnt_tx<= s_commbus_ddr.bitcnt_tx+1;
                else
                    s_commbus_ddr.bitcnt_tx<= 0;
                    v_commbus_word_ddr_tx:=s_commbus_encoder_interface.commbus_tx_data_out;
                end if;
                
                s_ddr_tx_shape(0) <= s_commbus_word_ddr_tx_fc(119-s_commbus_ddr.bitcnt_tx);
                s_commbus_ddr.frameclk_tx <= s_commbus_word_ddr_tx_fc(119-s_commbus_ddr.bitcnt_tx);
                s_ddr_tx_shape(1) <= v_commbus_word_ddr_tx(119-s_commbus_ddr.bitcnt_tx);
            else
                s_commbus_ddr.bitcnt_tx <= 0;
                s_ddr_tx_shape <= "11";
            end if;
        end if;
    
    end process;
    
    i_db6_commbus_ddr_encoder : entity tilecal.db6_commbus_ddr_encoder
      port map (
            p_master_reset_in => p_master_reset_in,
            p_loopback_mode_in => '0', 
            p_clknet_in => p_clknet_in,
            p_db_reg_rx_in => p_db_reg_rx_in,
            
            p_commbus_encoder_interface_out => s_commbus_encoder_interface, 
            
            --interfaces
            p_commbus_ddr_in => s_commbus_ddr,
            p_mb_interface_in => p_mb_interface_in,
            p_sem_interface_in => p_sem_interface_in,
            p_tdo_remote_in	=> p_tdo_remote_in,
            p_system_management_interface_in => p_system_management_interface_in,
            p_gbtx_interface_in => p_gbtx_interface_in,
            p_serial_id_interface_in => p_serial_id_interface_in
            );

    s_commbus_ddr_interface.commbus_encoder_interface <= s_commbus_encoder_interface;


    --loopback interface!
    
    
    s_commbus_ddr_loopback.bitclk_tx<=s_commbus_ddr_loopback.bitclk_tx;
    s_commbus_ddr_loopback.bitclk_rx<=s_commbus_ddr_loopback.bitclk_tx;
    
    
    
        i_ibufds_commbus_ddr_loopback_rx : ibufds  
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_commbus_ddr_loopback_rx_delay.idatain,             -- Buffer diff_p output
        I  => p_commbus_ddr_loopback_rx_in.p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_commbus_ddr_loopback_rx_in.n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_idelaye3_commbus_ddr_loopback_rx : idelaye3
      generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 240.48, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_commbus_ddr_loopback_rx_delay.casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_commbus_ddr_loopback_rx_delay.cntvalueout, -- 9-bit output: counter value output
        dataout => s_commbus_ddr_loopback_rx_delay.dataout, -- 1-bit output: delayed data output
        casc_in => s_commbus_ddr_loopback_rx_delay.casc_in,-- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_commbus_ddr_loopback_rx_delay.casc_return, -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_commbus_ddr_loopback_rx_delay.ce, -- 1-bit input: active high enable increment/decrement input
        clk => s_commbus_ddr_loopback.bitclk_rx, --s_commbus_ddr_rx_delay.clk, -- 1-bit input: clock input
        cntvaluein => s_commbus_ddr_loopback_rx_delay.cntvaluein, -- 9-bit input: counter value input
        datain => s_commbus_ddr_loopback_rx_delay.datain,  -- 1-bit input: data input from the iobuf
        en_vtc => s_commbus_ddr_loopback_rx_delay.en_vtc, -- 1-bit input: keep delay constant over vt
        idatain => s_commbus_ddr_loopback_rx_delay.idatain,  -- 1-bit input: data input from the logic
        inc => s_commbus_ddr_loopback_rx_delay.inc,  -- 1-bit input: increment / decrement tap delay input
        load => s_commbus_ddr_loopback_rx_delay.load,  -- 1-bit input: load delay_value input
        rst => s_commbus_ddr_loopback_rx_delay.rst  -- 1-bit input: asynchronous reset to the delay_value
);
--s_commbus_ddr_rx_delay.clk<= s_commbus_ddr_interface.bitclk_rx;
s_commbus_ddr_loopback_rx_delay.rst <= s_idelay_ctrl_reset_from_sm; --p_adc_readout_control_in.lg_idelay_ctrl_reset; 
s_commbus_ddr_loopback_rx_delay.load <= s_idelay_ctrl_load_from_sm; --p_adc_readout_control_in.lg_idelay_load;
s_commbus_ddr_loopback_rx_delay.en_vtc <= s_idelay_ctrl_en_vtc_from_sm; --p_adc_readout_control_in.lg_idelay_en_vtc;
s_commbus_ddr_loopback_rx_delay.cntvaluein <= std_logic_vector(to_unsigned(s_idelay_count_in_from_sm + s_idelay_count_in_from_hw,9));-- p_adc_readout_control_in.lg_idelay_count;
--s_adc_readout.lg_idelay_count <= s_commbus_ddr_rx_in.cntvalueout;

i_iddre1_commbus_loopback_rx : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
    q1 => s_ddr_rx_loopback_shape(0), -- 1-bit output: registered parallel output 1
    q2 => s_ddr_rx_loopback_shape(1), -- 1-bit output: registered parallel output 2
    c => s_commbus_ddr_loopback.bitclk_rx, -- 1-bit input: high-speed clock
    cb => s_commbus_ddr_loopback.bitclk_rx, -- 1-bit input: inversion of high-speed clock c
    d => s_commbus_ddr_loopback_rx_delay.dataout, -- 1-bit input: serial data input
    r => s_commbus_ddr_loopback_rx_delay.rst -- 1-bit input: active high async reset
);


i_sr_commbus_ddr_loopback_rx : sr_ram_commbus
  port map (
    d => s_commbus_ddr_loopback_rx_sr.d,
    clk => s_commbus_ddr_loopback.bitclk_rx,--s_commbus_ddr_rx_sr.clk,
    ce => s_commbus_ddr_loopback_rx_sr.ce,
    sclr => s_commbus_ddr_loopback_rx_sr.sclr,
    sset => s_commbus_ddr_loopback_rx_sr.sset,
    q => s_commbus_ddr_loopback_rx_sr.q
  );
--s_commbus_ddr_rx_sr.clk <= s_commbus_ddr.bitclk_rx;
s_commbus_ddr_loopback_rx_sr.d <= s_ddr_rx_loopback_shape(0) & s_ddr_rx_loopback_shape(1);
s_commbus_ddr_loopback_rx_sr.sclr <= '0';
s_commbus_ddr_loopback_rx_sr.ce <= '1';
s_commbus_ddr_loopback_rx_sr.sset <= '0';


proc_shift_in_loopback : process(s_commbus_ddr_loopback.bitclk_rx) -- odd data bits clocked in on rising edge of bitclock
--variable v_counter : integer := 0;
variable v_previous_frameclk : std_logic := '1';
variable v_commbus_bitslice_ddr_rx : std_logic_vector(119 downto 0);
begin
    if rising_edge(s_commbus_ddr_loopback.bitclk_rx) then
        if p_master_reset_in = '0' then
            if (s_commbus_ddr_loopback_rx_sr.q(0) = '0') and (v_previous_frameclk) = '1' then 
                s_commbus_ddr_loopback.bitcnt_rx<=0;
                if s_commbus_ddr_loopback.bitcnt_rx=120 then
                
                    case v_commbus_bitslice_ddr_rx(119 downto 116) is
                        when "0110" =>
                        
                            if s_commbus_ddr_loopback.locked_counter = 15 then
                                s_commbus_ddr_loopback.locked <= '1';
                                s_commbus_ddr_loopback.datavalid <= '0';
                                s_commbus_ddr_loopback.dataloopback <= '1';
                                s_commbus_ddr_loopback.datareset <= '0';
                                s_commbus_ddr_loopback.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            else
                                s_commbus_ddr_loopback.locked_counter<=s_commbus_ddr_loopback.locked_counter+1;
                            end if;
                            
                        when "0101" =>
                            if s_commbus_ddr_loopback.locked_counter = 15 then
                                s_commbus_ddr_loopback.locked <= '1';
                                s_commbus_ddr_loopback.datavalid <= '1';
                                s_commbus_ddr_loopback.dataloopback <= '0';
                                s_commbus_ddr_loopback.datareset <= '0';
                                s_commbus_ddr_loopback.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            else
                                s_commbus_ddr_loopback.locked_counter<=s_commbus_ddr_loopback.locked_counter+1;
                            end if;
                            
                        when "1010" =>
                            if s_commbus_ddr_loopback.locked_counter = 15 then
                                s_commbus_ddr_loopback.locked <= '1';
                                s_commbus_ddr_loopback.datavalid <= '0';
                                s_commbus_ddr_loopback.dataloopback <= '0';
                                s_commbus_ddr_loopback.datareset <= '1';
                                s_commbus_ddr_loopback.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            else
                                s_commbus_ddr_loopback.locked_counter<=s_commbus_ddr_loopback.locked_counter+1;
                            end if;
                                                    
                        when others =>
                        
                            if s_commbus_ddr_loopback.locked = '1' and s_commbus_ddr_loopback.locked_counter>0 then
                                s_commbus_ddr_loopback.locked_counter<=s_commbus_ddr_loopback.locked_counter-1;
                            else
                                s_commbus_ddr_loopback.locked_counter<=0;
                            end if;
                            
                            s_commbus_ddr_loopback.locked <= '0';
                            s_commbus_ddr_loopback.datavalid <= '0';
                            s_commbus_ddr_loopback.dataloopback <= '0';
                            s_commbus_ddr_loopback.datareset <= '0';
                            s_commbus_ddr_loopback.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
                            
                    end case;                
--                    if v_commbus_bitslice_ddr_rx(119 downto 116) = "0110" then
--                        s_commbus_ddr_loopback.locked <= '1';
--                        s_commbus_ddr_loopback.commbus_rx_data_in <= v_commbus_bitslice_ddr_rx;
--                    else
--                        s_commbus_ddr_loopback.locked <= '0';
--                    end if;
                --else
                end if;
            --elsif (s_commbus_ddr_loopback_rx_sr.q(0) = '1') and v_previous_frameclk = '0' then
            else
                s_commbus_ddr_loopback.bitcnt_rx<=s_commbus_ddr_loopback.bitcnt_rx+1;
            end if;
    
            v_previous_frameclk := s_commbus_ddr_loopback_rx_sr.q(0);
            v_commbus_bitslice_ddr_rx := v_commbus_bitslice_ddr_rx(118 downto 0) & s_commbus_ddr_loopback_rx_sr.q(1);
        else
            --v_counter:= 0;
            s_commbus_ddr_loopback.locked_counter <= 0;
        end if;
        
    end if;
end process;


i_db6_commbus_ddr_loopback_decoder : entity tilecal.db6_commbus_ddr_decoder
  port map(
        p_master_reset_in => p_master_reset_in,
		p_clknet_in => p_clknet_in,
        p_db_reg_rx_in => p_db_reg_rx_in,

		--interfaces
		p_commbus_ddr_in => s_commbus_ddr_loopback,
		p_commbus_decoder_interface_out => s_commbus_decoder_interface_loopback,
		p_clknet_out => s_commbus_ddr_interface_loopback.clknet,
		p_db_reg_rx_out => s_commbus_ddr_interface_loopback.db_reg_rx,
		p_mb_interface_out => s_commbus_ddr_interface_loopback.mb_interface,
		p_sem_interface_out => s_commbus_ddr_interface_loopback.sem_interface,
		p_tdo_remote_out => s_commbus_ddr_interface_loopback.tdo_remote,
		p_system_management_interface_out => s_commbus_ddr_interface_loopback.system_management_interface,
		p_gbtx_interface_out => s_commbus_ddr_interface_loopback.gbtx_interface,
		p_serial_id_interface_out => s_commbus_ddr_interface_loopback.serial_id_interface
		);

    
    
    
    
    
    
    i_odelaye3_commbus_ddr_loopback_data_out : odelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- (time, count)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- output delay tap setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 240.48, -- idelayctrl clock input frequency in mhz (values).
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
    )
    port map (
        casc_out => s_commbus_ddr_loopback_tx_delay.casc_out, -- 1-bit output: cascade delay output to idelay input cascade
        cntvalueout => s_commbus_ddr_loopback_tx_delay.cntvalueout, -- 9-bit output: counter value output
        dataout => s_commbus_ddr_loopback_tx_delay.dataout, -- 1-bit output: delayed data from odatain input port
        casc_in => s_commbus_ddr_loopback_tx_delay.casc_in , -- 1-bit input: cascade delay input from slave idelay cascade_out
        casc_return => s_commbus_ddr_loopback_tx_delay.casc_return, -- 1-bit input: cascade delay returning from slave idelay dataout
        ce => s_commbus_ddr_loopback_tx_delay.ce , -- 1-bit input: active high enable increment/decrement input
        clk => s_commbus_ddr_loopback.bitclk_tx, -- 1-bit input: clock input
        cntvaluein => s_commbus_ddr_loopback_tx_delay.cntvaluein, -- 9-bit input: counter value input
        en_vtc => s_commbus_ddr_loopback_tx_delay.en_vtc, -- 1-bit input: keep delay constant over vt
        inc => s_commbus_ddr_loopback_tx_delay.inc, -- 1-bit input: increment / decrement tap delay input
        load => s_commbus_ddr_loopback_tx_delay.load, -- 1-bit input: load delay_value input
        odatain => s_commbus_ddr_loopback_tx_delay.odatain, -- 1-bit input: data input
        rst => s_commbus_ddr_loopback_tx_delay.rst -- 1-bit input: asynchronous reset to the delay_value
    );
    
        s_commbus_ddr_loopback_tx_delay.casc_in <= '0';
        s_commbus_ddr_loopback_tx_delay.casc_return <= '0';
        s_commbus_ddr_loopback_tx_delay.ce <= '0';
        s_commbus_ddr_loopback_tx_delay.cntvaluein <= (others => '0');
        s_commbus_ddr_loopback_tx_delay.en_vtc <= '1';
        s_commbus_ddr_loopback_tx_delay.inc <= '0';
        s_commbus_ddr_loopback_tx_delay.load <= '0';
        s_commbus_ddr_loopback_tx_delay.rst <= '0';
    
    i_oddre1_commbus_loopback_data_out : oddre1
    generic map (
        is_c_inverted => '0', -- optional inversion for c
        is_d1_inverted => '0', -- optional inversion for d1
        is_d2_inverted => '0', -- optional inversion for d2
        srval => '0' -- initializes the oddre1 flip-flops to the specified value (1'b0, 1'b1)
    )
        port map (
        q => s_commbus_ddr_loopback_tx_delay.odatain, -- 1-bit output: data output to iob
        c => s_commbus_ddr_loopback.bitclk_tx, -- 1-bit input: high-speed clock input
        d1 => s_ddr_loopback_tx_shape(0), -- 1-bit input: parallel data input 1
        d2 => s_ddr_loopback_tx_shape(1), -- 1-bit input: parallel data input 2
        sr => p_master_reset_in -- 1-bit input: active high async reset
    );

    i_OBUFDS_commbus_loopback_data_out : OBUFDS
        port map (
        O => p_commbus_ddr_loopback_tx_out.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_commbus_ddr_loopback_tx_out.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_commbus_ddr_loopback_tx_delay.dataout-- s_clknet_out.mb_clk40(1) -- s_gbt40_q1_delay_control.dataout--s_clknet_out.mb_clk40(1) -- 1-bit input: Buffer input
        );


    
    proc_loopback_tx : process(s_commbus_ddr_loopback.bitclk_tx)
    --variable v_counter: integer :=0;
    variable v_commbus_word_ddr_tx : std_logic_vector(119 downto 0);
    begin
    
        if rising_edge(s_commbus_ddr_loopback.bitclk_tx) then
            if p_master_reset_in = '0' then
            
                if s_commbus_ddr_loopback.bitcnt_tx<120 then
                    s_commbus_ddr_loopback.bitcnt_tx<= s_commbus_ddr_loopback.bitcnt_tx+1;
                else
                    s_commbus_ddr_loopback.bitcnt_tx<= 0;
                    v_commbus_word_ddr_tx:=s_commbus_encoder_interface.commbus_tx_data_out;
                end if;
                
                s_ddr_loopback_tx_shape(0) <= s_commbus_word_ddr_tx_fc(119-s_commbus_ddr_loopback.bitcnt_tx);
                s_commbus_ddr_loopback.frameclk_tx <= s_commbus_word_ddr_tx_fc(119-s_commbus_ddr_loopback.bitcnt_tx);
                s_ddr_loopback_tx_shape(1) <= v_commbus_word_ddr_tx(119-s_commbus_ddr_loopback.bitcnt_tx);
            else
                s_commbus_ddr_loopback.bitcnt_tx <= 0;
                s_ddr_loopback_tx_shape<= "11";
            end if;
            
        end if;
    
    end process;
    
    i_db6_commbus_ddr_loopback_encoder : entity tilecal.db6_commbus_ddr_encoder
      port map (
            p_master_reset_in => p_master_reset_in,
            p_loopback_mode_in => '1', 
            p_clknet_in => p_clknet_in,
            p_db_reg_rx_in => p_db_reg_rx_in,
            
            p_commbus_encoder_interface_out => s_commbus_encoder_interface_loopback, 
            
            --interfaces
            p_commbus_ddr_in => s_commbus_ddr_loopback,
            p_mb_interface_in => p_mb_interface_in,
            p_sem_interface_in => p_sem_interface_in,
            p_tdo_remote_in	=> p_tdo_remote_in,
            p_system_management_interface_in => p_system_management_interface_in,
            p_gbtx_interface_in => p_gbtx_interface_in,
            p_serial_id_interface_in => p_serial_id_interface_in
            );


    
end Behavioral;
