----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/29/2020 05:27:41 PM
-- Design Name: 
-- Module Name: db6_sem - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;
library sem;
use sem.all;

entity db6_sem_interface is
  Port ( 
    p_clknet_in : in t_db_clknet;
    p_db_reg_rx_in : in t_db_reg_rx;
    p_sem_interface_out : out t_sem_interface
  );
end db6_sem_interface;

architecture Behavioral of db6_sem_interface is

begin


	i_sem_wrapper : entity sem_wrapper
	port map (
		clk                  => p_clknet_in.clk80,
		
        p_status_heartbeat => p_sem_interface_out.status_heartbeat,
        p_status_initialization => p_sem_interface_out.status_initialization,
        p_status_observation => p_sem_interface_out.status_observation,
        p_status_correction => p_sem_interface_out.status_correction,
        p_status_classification => p_sem_interface_out.status_classification,
        p_status_injection => p_sem_interface_out.status_injection,
        p_status_diagnostic_scan => p_sem_interface_out.status_diagnostic_scan,
        p_status_detect_only => p_sem_interface_out.status_detect_only,
        p_status_essential => p_sem_interface_out.status_essential,
        p_status_uncorrectable => p_sem_interface_out.status_uncorrectable,
        
        -- UART interface
        --uart : t_uart;
		uart_tx              => p_sem_interface_out.uart_tx,
		p_monitor_txdata     => p_sem_interface_out.monitor_txdata,
		p_monitor_txwrite     => p_sem_interface_out.monitor_txwrite,
		p_monitor_txfull     => p_sem_interface_out.monitor_txfull,
		
		uart_rx              => p_db_reg_rx_in(cfb_sem_control)(0),

--      icap
        p_icap_out          => p_sem_interface_out.icap_out
        -- Command interface
--        command_strobe => p_sem_interface_out.command_strobe,
--        command_busy => p_sem_interface_out.command_busy,
--        command_code => p_sem_interface_out.command_code,
        
        -- Routed system clock 
--        icap_clk_out => p_sem_interface_out.icap_clk_out,
        
        -- ICAP arbitration interface
--        cap_rel => p_sem_interface_out.cap_rel,
--        cap_gnt => p_sem_interface_out.cap_gnt,
--        cap_req => p_sem_interface_out.cap_req,
        
        -- Auxiliary interface
--        aux_error_cr_ne => p_sem_interface_out.aux_error_cr_ne,
--        aux_error_cr_es => p_sem_interface_out.aux_error_cr_es,
--        aux_error_uc => p_sem_interface_out.aux_error_uc
		
	);


end Behavioral;
