-----------------------------------------------------------------------------------------------------------------------------
-- Company: Stockholm University
-- Engineer: Eduardo Valdes Santurio
--           Sam Silverstein
-- 
-- Create Date: 25/05/2018 11:17:52 PM
-- Design Name: 
-- Module Name: db_configbus_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity db6_configbus_interface is
    Port (     
               p_master_reset_in        : in    std_logic;
               p_clknet_in              : in    t_db_clknet;
               p_cfgbus_data_local_in       : in t_cfgbus_data_in;
               p_cfgbus_data_remote_in       : in t_cfgbus_data_in;      
               p_gbt_encoder_interface_in         : in t_gbt_encoder_interface;
               
               p_db_reg_rx_out      : out t_db_reg_rx;
               
               p_bcr_out    : out t_bcr;
               
               p_leds_out : out std_logic_vector(3 downto 0)
    );

end db6_configbus_interface;

architecture Behavioral of db6_configbus_interface is

attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;

    --debug
--    COMPONENT vio_db_registers
--      PORT (
--        clk : IN STD_LOGIC;
--        probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_out0 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
--      );
--    END COMPONENT;
    
--    signal s_debug_businput_value : STD_LOGIC_VECTOR(31 DOWNTO 0);
--    signal s_debug_register_value : STD_LOGIC_VECTOR(31 DOWNTO 0);
--    signal s_debug_config_bus_ila : STD_LOGIC_VECTOR(31 DOWNTO 0);
--    signal s_debug_bus_ila : STD_LOGIC_VECTOR(31 DOWNTO 0);
--    signal s_debug_register_number : STD_LOGIC_VECTOR(31 DOWNTO 0);
        
 
    signal s_cfgbus_data_local, s_cfgbus_data_remote, 
           s_cfgbus_data_local_se, s_cfgbus_data_remote_se,
           s_cfgbus_data_local_delayed, s_cfgbus_data_remote_delayed : std_logic_vector(7 downto 0);
    
    type t_configbus_bitslice is array (7 downto 0) of std_logic_vector(1 downto 0);
    signal s_cfgbus_local_iddr, s_cfgbus_remote_iddr : t_configbus_bitslice;
    
    signal s_db_cfgbus_data_shift_register, s_db_cfgbus_data_shift_register_local, s_db_cfgbus_data_shift_register_remote : std_logic_vector(31 downto 0);
    signal s_db_cfgbus_address_shift_register, s_db_cfgbus_address_shift_register_local, s_db_cfgbus_address_shift_register_remote : std_logic_vector(15 downto 0);
    signal s_db_cfgbus_datavalid_shift_register, s_db_cfgbus_datavalid_shift_register_local, s_db_cfgbus_datavalid_shift_register_remote : std_logic_vector(4 downto 0);
    signal s_db_cfgbus_byte_shift_register, s_db_cfgbus_byte_shift_register_local, s_db_cfgbus_byte_shift_register_remote : std_logic_vector(7 downto 0);
    
    constant c_db_reg_rx : t_db_reg_rx:= (
        cfb_integrator_interval => x"0000000a",
        cfb_tx_control => "11111000000000000000000000001111",
        cfb_strobe_lenght => "00000000000000000000000100000000",
        cfb_db_reg_mask => (others=>'1'),
        adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
        adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
        adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
        adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
        adc_config_module => "00000000000000000000000000011100",
        adc_readout_idelay3_0 => "00000100000000000000010000000000",
        adc_readout_idelay3_1 => "00000100000000000000010000000000",
        adc_readout_idelay3_2 => "00000100000000000000010000000000",
        adc_readout_idelay3_3 => "00000100000000000000010000000000",
        adc_readout_idelay3_4 => "00000100000000000000010000000000",
        adc_readout_idelay3_5 => "00000100000000000000010000000000",
    
        others=>(others=>'0')
    );
    
    type t_db_reg_rx_array is array(0 to 2) of t_db_reg_rx;
    signal s_db_reg_rx, s_db_reg_rx_local, s_db_reg_rx_remote, s_db_reg_rx_commbus : t_db_reg_rx_array:= (others=>(c_db_reg_rx));
    --signal s_db_reg_rx, s_db_reg_rx_local, s_db_reg_rx_remote, s_db_reg_rx_commbus : t_db_reg_rx:= (c_db_reg_rx);
    --signal s_db_reg_rx_out : t_db_reg_rx:= (c_db_reg_rx);
    
    type t_std_logic_tmr is array(0 to 2) of std_logic;
    signal s_strobe_bit_cfgbus, s_strobe_bit_cfgbus_local, s_strobe_bit_cfgbus_remote : t_std_logic_tmr := (others => '0');
    signal s_bcr_locked, s_bcr_locked_local, s_bcr_locked_remote : t_std_logic_tmr := (others => '0');
    signal s_bcr, s_bcr_local, s_bcr_remote : t_std_logic_tmr := (others => '0');
    
    type t_db_reg_tmr is array(0 to 2) of std_logic_vector(31 downto 0);
    signal s_strobe_reg, s_strobe_reg_local, s_strobe_reg_remote : t_db_reg_tmr := (others => (others =>'0'));
    signal s_lhc_bunch_counter, s_lhc_bunch_counter_local, s_lhc_bunch_counter_remote : t_db_reg_tmr := (others => (others =>'0'));
    signal s_gbt_sc_address : t_db_reg_tmr := (others => (others =>'0'));
    
    type t_cfgbus_delay_control_array is array (0 to 7) of t_delay_control;
    signal s_cfgbus_local_delay_control, s_cfgbus_remote_delay_control: t_cfgbus_delay_control_array := (others => c_delay_control);
    
    --signal s_bcr_out : t_bcr;
    
    attribute keep of s_db_reg_rx, s_db_reg_rx_local, s_db_reg_rx_remote : signal is "TRUE";
    attribute dont_touch of s_db_reg_rx, s_db_reg_rx_local, s_db_reg_rx_remote : signal is "TRUE";

    
begin
    
    --output ports
    p_leds_out(3)<=s_cfgbus_remote_iddr(7)(1);
    p_leds_out(2)<=s_cfgbus_local_iddr(7)(1);
    p_leds_out(1)<=s_cfgbus_remote_iddr(7)(1);
    p_leds_out(0)<=s_cfgbus_local_iddr(7)(1);
    
    gen_configbus : for k in 0 to 7 generate
    begin
    i_ibufds_cfbl : ibufds
         generic map (
           diff_term => true,
           iostandard => "sub_lvds")
         port map (
           o  => s_cfgbus_data_local_se(k),             -- buffer diff_p output
           i  => p_cfgbus_data_local_in(k).p, -- diff_p buffer input (connect directly to top-level port)
           ib => p_cfgbus_data_local_in(k).n -- diff_n buffer input (connect directly to top-level port)
           );
       
    i_ibufds_cfbr : ibufds
         generic map (
           diff_term => true,
           iostandard => "sub_lvds")
         port map (
           o  => s_cfgbus_data_remote_se(k),             -- buffer diff_p output
           i  => p_cfgbus_data_remote_in(k).p, -- diff_p buffer input (connect directly to top-level port)
           ib => p_cfgbus_data_remote_in(k).n  -- diff_n buffer input (connect directly to top-level port)
           );
           
    i_idelaye3_configbus_local_in : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 400.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_cfgbus_local_delay_control(k).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_cfgbus_local_delay_control(k).cntvalueout, -- 9-bit output: counter value output
        dataout => s_cfgbus_data_local_delayed(k), -- 1-bit output: delayed data output
        casc_in => s_cfgbus_local_delay_control(k).casc_in, -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_cfgbus_local_delay_control(k).casc_return, -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_cfgbus_local_delay_control(k).ce, -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.osc_clk40, -- 1-bit input: clock input
        cntvaluein => s_cfgbus_local_delay_control(k).cntvaluein, -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_cfgbus_local_delay_control(k).en_vtc, -- 1-bit input: keep delay constant over vt
        idatain => s_cfgbus_data_local_se(k), -- 1-bit input: data input from the logic
        inc => s_cfgbus_local_delay_control(k).inc, -- 1-bit input: increment / decrement tap delay input
        load => s_cfgbus_local_delay_control(k).load, -- 1-bit input: load delay_value input
        rst => p_master_reset_in -- 1-bit input: asynchronous reset to the delay_value
);

    s_cfgbus_local_delay_control(k).casc_in <= '0';
    s_cfgbus_local_delay_control(k).casc_return <= '0';
    s_cfgbus_local_delay_control(k).ce <= '0';
    s_cfgbus_local_delay_control(k).cntvaluein <= (others => '0');
    s_cfgbus_local_delay_control(k).en_vtc <= '1';
    s_cfgbus_local_delay_control(k).inc <= '0';
    s_cfgbus_local_delay_control(k).load <= '0';
    s_cfgbus_local_delay_control(k).rst <= '0';



    i_iddre1_configbus_local_in : iddre1
    generic map (
        ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
        is_c_inverted => '0', -- optional inversion for c
        is_cb_inverted => '1' -- optional inversion for c
    )
    port map (
        q1 => s_cfgbus_local_iddr(k)(1), -- 1-bit output: registered parallel output 1
        q2 => s_cfgbus_local_iddr(k)(0), -- 1-bit output: registered parallel output 2
        c => p_clknet_in.cfgbus_clk40_local, -- 1-bit input: high-speed clock
        cb => p_clknet_in.cfgbus_clk40_local, -- 1-bit input: inversion of high-speed clock c
        d => s_cfgbus_data_local_delayed(k), -- 1-bit input: serial data input
        r => p_master_reset_in -- 1-bit input: active high async reset
);

    i_idelaye3_configbus_remote_in : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 400.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_cfgbus_remote_delay_control(k).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_cfgbus_remote_delay_control(k).cntvalueout, -- 9-bit output: counter value output
        dataout => s_cfgbus_data_remote_delayed(k), -- 1-bit output: delayed data output
        casc_in => s_cfgbus_remote_delay_control(k).casc_in, -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return => s_cfgbus_remote_delay_control(k).casc_return, -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_cfgbus_remote_delay_control(k).ce, -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.osc_clk40, -- 1-bit input: clock input
        cntvaluein => s_cfgbus_remote_delay_control(k).cntvaluein, -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_cfgbus_remote_delay_control(k).en_vtc, -- 1-bit input: keep delay constant over vt
        idatain => s_cfgbus_data_remote_se(k), -- 1-bit input: data input from the logic
        inc => s_cfgbus_remote_delay_control(k).inc, -- 1-bit input: increment / decrement tap delay input
        load => s_cfgbus_remote_delay_control(k).load, -- 1-bit input: load delay_value input
        rst => p_master_reset_in -- 1-bit input: asynchronous reset to the delay_value
    );

    s_cfgbus_remote_delay_control(k).casc_in <= '0';
    s_cfgbus_remote_delay_control(k).casc_return <= '0';
    s_cfgbus_remote_delay_control(k).ce <= '0';
    s_cfgbus_remote_delay_control(k).cntvaluein <= (others => '0');
    s_cfgbus_remote_delay_control(k).en_vtc <= '1';
    s_cfgbus_remote_delay_control(k).inc <= '0';
    s_cfgbus_remote_delay_control(k).load <= '0';
    s_cfgbus_remote_delay_control(k).rst <= '0';

    i_iddre1_configbus_remote_in : iddre1
    generic map (
        ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
        is_c_inverted => '0', -- optional inversion for c
        is_cb_inverted => '1' -- optional inversion for c
    )
    port map (
        q1 => s_cfgbus_remote_iddr(k)(1), -- 1-bit output: registered parallel output 1
        q2 => s_cfgbus_remote_iddr(k)(0), -- 1-bit output: registered parallel output 2
        c => p_clknet_in.cfgbus_clk40_remote, -- 1-bit input: high-speed clock
        cb => p_clknet_in.cfgbus_clk40_remote, -- 1-bit input: inversion of high-speed clock c
        d => s_cfgbus_data_remote_delayed(k), -- 1-bit input: serial data input
        r => p_master_reset_in -- 1-bit input: active high async reset
    );

           

    

    -- Enter each new 8-bit ConfigBus word to the input shift register:
    
        proc_config_bus_shift_in_data_local : process(p_clknet_in.cfgbus_clk40_local)
        begin
            
            if falling_edge(p_clknet_in.cfgbus_clk40_local) then

                if p_master_reset_in = '0' then
                    --shift in data and orbit cross from the local or remote configbus
                    s_db_cfgbus_datavalid_shift_register_local <= s_db_cfgbus_datavalid_shift_register_local(3 downto 0) & s_cfgbus_local_iddr(6)(1);

                    s_db_cfgbus_address_shift_register_local <=  
                        s_cfgbus_local_iddr(5)(1)& s_cfgbus_local_iddr(4)(1) &  s_cfgbus_local_iddr(3)(1)& s_cfgbus_local_iddr(2)(1)
                        & s_db_cfgbus_address_shift_register_local(15 downto 4);
                    s_db_cfgbus_data_shift_register_local <= 
                        s_cfgbus_local_iddr(7)(0)& s_cfgbus_local_iddr(6)(0)& s_cfgbus_local_iddr(5)(0)& s_cfgbus_local_iddr(4)(0) &
                        s_cfgbus_local_iddr(3)(0)& s_cfgbus_local_iddr(2)(0)& s_cfgbus_local_iddr(1)(0)& s_cfgbus_local_iddr(0)(0) &
                        s_db_cfgbus_data_shift_register_local(31 downto 8);
                    s_db_cfgbus_byte_shift_register_local <= s_cfgbus_local_iddr(1)(1)& s_cfgbus_local_iddr(0)(1) & s_db_cfgbus_byte_shift_register_local(7 downto 2);
                   
                    
                 else    

                    s_db_cfgbus_datavalid_shift_register_local <= (others=> '0');
                    s_db_cfgbus_address_shift_register_local <= (others=> '0');
                    s_db_cfgbus_data_shift_register_local <= (others=> '0');
                    s_db_cfgbus_byte_shift_register_local <= (others=> '0');
                    
                end if;

            end if; -- clock edge
        
        end process;

        proc_config_bus_shift_in_data_remote : process(p_clknet_in.cfgbus_clk40_remote)
        begin
            
            if falling_edge(p_clknet_in.cfgbus_clk40_remote) then
                if p_master_reset_in = '0' then
                    s_db_cfgbus_datavalid_shift_register_remote <= s_db_cfgbus_datavalid_shift_register_remote(3 downto 0) & s_cfgbus_remote_iddr(6)(1);            
                    s_db_cfgbus_address_shift_register_remote <=  
                        s_cfgbus_remote_iddr(5)(1)& s_cfgbus_remote_iddr(4)(1) &  s_cfgbus_remote_iddr(3)(1)& s_cfgbus_remote_iddr(2)(1)
                        & s_db_cfgbus_address_shift_register_remote(15 downto 4);
                    s_db_cfgbus_data_shift_register_remote <= 
                        s_cfgbus_remote_iddr(7)(0)& s_cfgbus_remote_iddr(6)(0)& s_cfgbus_remote_iddr(5)(0)& s_cfgbus_remote_iddr(4)(0) &
                        s_cfgbus_remote_iddr(3)(0)& s_cfgbus_remote_iddr(2)(0)& s_cfgbus_remote_iddr(1)(0)& s_cfgbus_remote_iddr(0)(0) &
                        s_db_cfgbus_data_shift_register_remote(31 downto 8);
                    s_db_cfgbus_byte_shift_register_remote <= s_cfgbus_remote_iddr(1)(1)& s_cfgbus_remote_iddr(0)(1) & s_db_cfgbus_byte_shift_register_remote(7 downto 2);
                    
                else
                    
                    s_db_cfgbus_datavalid_shift_register_remote <= (others=> '0');
                    s_db_cfgbus_address_shift_register_remote <= (others=> '0');
                    s_db_cfgbus_data_shift_register_remote <= (others=> '0');
                    s_db_cfgbus_byte_shift_register_remote <= (others=> '0');
                                    
                end if;
            end if;
        end process;
   end generate gen_configbus;
   
   
   
   
gen_cfgbus_tmr : for tmr in 0 to 2 generate
begin
        s_gbt_sc_address(tmr) <= p_gbt_encoder_interface_in.sc_address;
       
        proc_register_data_configbus_local : process(p_clknet_in.cfgbus_clk40_local)--s_clk40_cfgbus)
        variable v_cfg_db_advanced_mode_address, v_cfg_db_advanced_mode_register_value : std_logic_vector(31 downto 0);
       
        begin
         
         if rising_edge(p_clknet_in.cfgbus_clk40_local) then--s_clk40_cfgbus) then
            
            --check that all clocks are locked
             if (p_clknet_in.locked_db = '1') then-- and (p_master_reset_in = '0') then
                
                 if s_db_cfgbus_datavalid_shift_register_local(4 downto 0) = "01110" then
                    
                    s_db_reg_rx_local(tmr)(cfb_command_counter)<=std_logic_vector(to_unsigned(to_integer(unsigned(s_db_reg_rx_local(tmr)(cfb_command_counter)))+1,32));
                    case  s_db_cfgbus_address_shift_register_local(11 downto 0) is
                        when c_db_reg_rx_lut(cfb_db_reg_mask)=>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            s_db_reg_rx_local(tmr)(cfb_db_reg_mask)<= s_db_cfgbus_data_shift_register_local;
                            
                        when c_db_reg_rx_lut(cfb_strobe_reg)=>
                            s_strobe_bit_cfgbus_local(tmr)<='1';
                            s_db_reg_rx_local(tmr)(cfb_strobe_reg)<=s_db_cfgbus_data_shift_register_local;
                        
                        when c_db_reg_rx_lut(cfb_db_advanced_reg_address)=>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            s_db_reg_rx_local(tmr)(cfb_db_advanced_reg_address)<=(s_db_cfgbus_data_shift_register_local);
                            s_db_reg_rx_local(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_local;
                                                                
                        when c_db_reg_rx_lut(cfb_db_advanced_reg_value)=>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            s_db_reg_rx_local(tmr)(cfb_db_advanced_reg_value)<=(s_db_cfgbus_data_shift_register_local);
                            s_db_reg_rx_local(tmr)(to_integer(unsigned(s_db_reg_rx_local(tmr)(cfb_db_advanced_reg_address))))<=
                                (s_db_cfgbus_data_shift_register_local and s_db_reg_rx_local(tmr)(cfb_db_reg_mask))
                                or (s_db_reg_rx_local(tmr)(to_integer(unsigned(s_db_reg_rx_local(tmr)(cfb_db_advanced_reg_address)))) and (not s_db_reg_rx_local(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_local(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_local;
                            
                        when c_db_reg_rx_lut(cfb_cis_config) =>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            s_db_reg_rx_local(tmr)(cfb_cis_config)<= (s_db_cfgbus_data_shift_register_local and s_db_reg_rx_local(tmr)(cfb_db_reg_mask)) 
                                or (s_db_reg_rx_local(tmr)(cfb_cis_config) and (not s_db_reg_rx_local(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_local(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_local;
                            
                        when c_db_reg_rx_lut(cfb_cs_command) =>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            s_db_reg_rx_local(tmr)(cfb_cs_command)<= (s_db_cfgbus_data_shift_register_local and s_db_reg_rx_local(tmr)(cfb_db_reg_mask)) 
                                or (s_db_reg_rx_local(tmr)(cfb_cs_command) and (not s_db_reg_rx_local(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_local(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_local;
                            
                        when c_db_reg_rx_lut(cfb_integrator_interval) =>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            s_db_reg_rx_local(tmr)(cfb_integrator_interval)<= (s_db_cfgbus_data_shift_register_local and s_db_reg_rx_local(tmr)(cfb_db_reg_mask)) 
                                or (s_db_reg_rx_local(tmr)(cfb_integrator_interval) and (not s_db_reg_rx_local(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_local(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_local;
                        when c_db_reg_rx_lut(cfb_command_counter) =>
                                
                        when c_db_reg_rx_lut(cfb_wr_strobe) =>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                            
                        when others=>
                            s_strobe_bit_cfgbus_local(tmr)<='0';
                                s_db_reg_rx_local(tmr)(to_integer(unsigned(s_db_cfgbus_address_shift_register_local(11 downto 0))))<= (s_db_cfgbus_data_shift_register_local and s_db_reg_rx_local(tmr)(cfb_db_reg_mask)) 
                                    or (s_db_reg_rx_local(tmr)(to_integer(unsigned(s_db_cfgbus_address_shift_register_local(11 downto 0)))) and (not s_db_reg_rx_local(tmr)(cfb_db_reg_mask)));  
                            s_db_reg_rx_local(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_local;
                            
                    end case;
                    
                else
                    s_strobe_bit_cfgbus_local(tmr)<='0';                 
                end if;
            end if;
        end if;
            
    end process;
        
        proc_register_data_configbus_remote : process(p_clknet_in.cfgbus_clk40_remote)--s_clk40_cfgbus)
        variable v_cfg_db_advanced_mode_address, v_cfg_db_advanced_mode_register_value : std_logic_vector(31 downto 0);
       
        begin

         if rising_edge(p_clknet_in.cfgbus_clk40_remote) then--s_clk40_cfgbus) then
            
            --check that all clocks are locked
             if (p_clknet_in.locked_db = '1') then-- and (p_master_reset_in = '0') then
             
                 if s_db_cfgbus_datavalid_shift_register_remote(4 downto 0) = "01110" then
                    
                    s_db_reg_rx_remote(tmr)(cfb_command_counter)<=std_logic_vector(to_unsigned(to_integer(unsigned(s_db_reg_rx_remote(tmr)(cfb_command_counter)))+1,32));
                    case  s_db_cfgbus_address_shift_register_remote(11 downto 0) is
                        when c_db_reg_rx_lut(cfb_db_reg_mask)=>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)<= s_db_cfgbus_data_shift_register_remote;
                            
                        when c_db_reg_rx_lut(cfb_strobe_reg)=>
                            s_strobe_bit_cfgbus_remote(tmr)<='1';
                            s_db_reg_rx_remote(tmr)(cfb_strobe_reg)<=s_db_cfgbus_data_shift_register_remote;
                        
                        when c_db_reg_rx_lut(cfb_db_advanced_reg_address)=>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            s_db_reg_rx_remote(tmr)(cfb_db_advanced_reg_address)<=(s_db_cfgbus_data_shift_register_remote);
                            s_db_reg_rx_remote(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_remote;
                                                                
                        when c_db_reg_rx_lut(cfb_db_advanced_reg_value)=>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            s_db_reg_rx_remote(tmr)(cfb_db_advanced_reg_value)<=(s_db_cfgbus_data_shift_register_remote);
                            s_db_reg_rx_remote(tmr)(to_integer(unsigned(s_db_reg_rx_remote(tmr)(cfb_db_advanced_reg_address))))<=
                                (s_db_cfgbus_data_shift_register_remote and s_db_reg_rx_remote(tmr)(cfb_db_reg_mask))
                                or (s_db_reg_rx_remote(tmr)(to_integer(unsigned(s_db_reg_rx_remote(tmr)(cfb_db_advanced_reg_address)))) and (not s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_remote(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_remote;
                            
                        when c_db_reg_rx_lut(cfb_cis_config) =>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            s_db_reg_rx_remote(tmr)(cfb_cis_config)<= (s_db_cfgbus_data_shift_register_remote and s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)) 
                                or (s_db_reg_rx_remote(tmr)(cfb_cis_config) and (not s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_remote(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_remote;
                            
                        when c_db_reg_rx_lut(cfb_cs_command) =>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            s_db_reg_rx_remote(tmr)(cfb_cs_command)<= (s_db_cfgbus_data_shift_register_remote and s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)) 
                                or (s_db_reg_rx_remote(tmr)(cfb_cs_command) and (not s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_remote(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_remote;
                            
                        when c_db_reg_rx_lut(cfb_integrator_interval) =>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            s_db_reg_rx_remote(tmr)(cfb_integrator_interval)<= (s_db_cfgbus_data_shift_register_remote and s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)) 
                                or (s_db_reg_rx_remote(tmr)(cfb_integrator_interval) and (not s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)));
                            s_db_reg_rx_remote(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_remote;
                        when c_db_reg_rx_lut(cfb_command_counter) =>
                                
                        when c_db_reg_rx_lut(cfb_wr_strobe) =>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                            
                        when others=>
                            s_strobe_bit_cfgbus_remote(tmr)<='0';
                                s_db_reg_rx_remote(tmr)(to_integer(unsigned(s_db_cfgbus_address_shift_register_remote(11 downto 0))))<= (s_db_cfgbus_data_shift_register_remote and s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)) 
                                    or (s_db_reg_rx_remote(tmr)(to_integer(unsigned(s_db_cfgbus_address_shift_register_remote(11 downto 0)))) and (not s_db_reg_rx_remote(tmr)(cfb_db_reg_mask)));  
                            s_db_reg_rx_remote(tmr)(cfb_wr_strobe)(15 downto 0)<= s_db_cfgbus_address_shift_register_remote;
                            
                    end case;
                    
                else
                    s_strobe_bit_cfgbus_remote(tmr)<='0';                 
                end if;
                 
             else

             end if;
         end if;
        end process;


    proc_strobe_manager_local : process(p_clknet_in.cfgbus_clk40_local)
    variable v_counter : integer := 0;
    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
    variable v_reg_buffer : std_logic_vector(31 downto 0);
    variable v_strobe_lenght : integer := 0;
    variable v_clk_domain_cross_counter : integer :=0;
    constant c_domain_cross_ratio : integer := 40000000/100;
    begin
        if rising_edge(p_clknet_in.cfgbus_clk40_local) then
             --s_strobe_bit<= s_strobe_bit_configbus or s_strobe_bit_commbus;
             case sm_strobe_manager is
                when st_idle=>
                    if (s_strobe_bit_cfgbus_local(tmr)='1') then
                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                        v_counter := 0;
                        if s_db_reg_rx_local(tmr)(cfb_strobe_lenght) = x"FFFFFFFF" then
                            sm_strobe_manager := st_idle;
                        else
                            sm_strobe_manager := st_wait_for_strobe_bit;
                        end if;
                        v_reg_buffer:= s_db_reg_rx_local(tmr)(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx_remote(tmr)(cfb_strobe_lenght)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));

                    else
                        if s_db_reg_rx_local(tmr)(cfb_strobe_lenght) = x"FFFFFFFF" then
                            s_strobe_reg_local(tmr) <= s_db_reg_rx_local(tmr)(cfb_strobe_reg);
                        else
                            s_strobe_reg_local(tmr) <= (others=>'0');
                        end if;                    
                    end if;

                    
                when st_wait_for_strobe_bit =>
                    v_clk_domain_cross_counter:=0;
                    v_counter:=0;
                    if (s_strobe_bit_cfgbus_local(tmr)='0') then
                        sm_strobe_manager:=st_propagate_strobe;
                    else
                        sm_strobe_manager := st_wait_for_strobe_bit;
                    end if;
                when st_propagate_strobe =>
                    if v_clk_domain_cross_counter < c_domain_cross_ratio then
                        v_clk_domain_cross_counter := v_clk_domain_cross_counter +1;
                        s_strobe_reg(tmr) <= v_reg_buffer;
                    else
                        v_clk_domain_cross_counter:=0;
                        if v_counter < v_strobe_lenght then
                            v_counter:=v_counter+1;
                            s_strobe_reg(tmr) <= v_reg_buffer;
                        else
                            sm_strobe_manager := st_idle;
                            v_counter :=0;
                            s_strobe_reg(tmr) <= (others=>'0');
                        end if;
                    end if;
                when others=>
                    sm_strobe_manager := st_idle;
            end case;
        end if;

    end process;

    proc_strobe_manager_remote : process(p_clknet_in.cfgbus_clk40_remote)
    variable v_counter : integer := 0;
    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
    variable v_reg_buffer : std_logic_vector(31 downto 0);
    variable v_strobe_lenght : integer := 0;
    variable v_clk_domain_cross_counter : integer :=0;
    constant c_domain_cross_ratio : integer := 40000000/100;
    begin
        if rising_edge(p_clknet_in.cfgbus_clk40_remote) then
             --s_strobe_bit<= s_strobe_bit_configbus or s_strobe_bit_commbus;
             case sm_strobe_manager is
                when st_idle=>
                    if (s_strobe_bit_cfgbus_remote(tmr) ='1') then
                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                        v_counter := 0;
                        if s_db_reg_rx_remote(tmr)(cfb_strobe_lenght) = x"FFFFFFFF" then
                            sm_strobe_manager := st_idle;
                        else
                            sm_strobe_manager := st_wait_for_strobe_bit;
                        end if;
                        v_reg_buffer:= s_db_reg_rx_remote(tmr)(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx_remote(tmr)(cfb_strobe_lenght)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));

                    else
                        if s_db_reg_rx_remote(tmr)(cfb_strobe_lenght) = x"FFFFFFFF" then
                            s_strobe_reg_remote(tmr) <= s_db_reg_rx_remote(tmr)(cfb_strobe_reg);
                        else
                            s_strobe_reg_remote(tmr) <= (others=>'0');
                        end if;                    
                    end if;

                    
                when st_wait_for_strobe_bit =>
                    v_clk_domain_cross_counter:=0;
                    v_counter:=0;
                    if (s_strobe_bit_cfgbus_remote(tmr)='0') then
                        sm_strobe_manager:=st_propagate_strobe;
                    else
                        sm_strobe_manager := st_wait_for_strobe_bit;
                    end if;
                when st_propagate_strobe =>
                    if v_clk_domain_cross_counter < c_domain_cross_ratio then
                        v_clk_domain_cross_counter := v_clk_domain_cross_counter +1;
                        s_strobe_reg(tmr) <= v_reg_buffer;
                    else
                        v_clk_domain_cross_counter:=0;
                        if v_counter < v_strobe_lenght then
                            v_counter:=v_counter+1;
                            s_strobe_reg(tmr) <= v_reg_buffer;
                        else
                            sm_strobe_manager := st_idle;
                            v_counter :=0;
                            s_strobe_reg(tmr) <= (others=>'0');
                        end if;
                    end if;
                when others=>
                    sm_strobe_manager := st_idle;
            end case;
        end if;

    end process;


    proc_sync_to_orbit_local: process(p_clknet_in.cfgbus_clk40_local)
    constant c_watchdog_tries : integer := 31;
    variable v_counter_local : integer := 0;
    variable v_bcrlock_local: std_logic := '0';
    variable v_maxcount : integer := 0;
    variable v_bcr_watchdog : integer:= 0;

    begin
        
        if rising_edge(p_clknet_in.cfgbus_clk40_local) then
            
            s_lhc_bunch_counter_local(tmr) <= std_logic_vector(to_unsigned(v_counter_local,32));
            s_bcr_locked_local(tmr) <=  v_bcrlock_local;
            s_bcr_local(tmr) <= s_cfgbus_local_iddr(7)(1);
        
        end if;
        
        if rising_edge(p_clknet_in.cfgbus_clk40_local) then
            if s_cfgbus_local_iddr(7)(1) = '1' then
                v_maxcount := v_counter_local;
                if (v_maxcount = (c_lhc_bunches_between_bcr-1)) then
                    v_bcrlock_local := '1';
                    
                 else
                    v_bcrlock_local := '0';

                end if;
                v_counter_local := 0;                
            else
                v_counter_local := v_counter_local + 1;
           
            end if;

        end if; -- Clock edge
    
    end process; -- proc_sync_to_orbit

    proc_sync_to_orbit_remote: process(p_clknet_in.cfgbus_clk40_remote)
    constant c_watchdog_tries : integer := 31;
    variable v_counter_remote : integer := 0;
    variable v_bcrlock_remote: std_logic := '0';
    variable v_maxcount : integer := 0;
    variable v_bcr_watchdog : integer:= 0;

    begin
        
        if rising_edge(p_clknet_in.cfgbus_clk40_remote) then
            
            s_lhc_bunch_counter_remote(tmr) <= std_logic_vector(to_unsigned(v_counter_remote,32));
            s_bcr_locked_remote(tmr) <=  v_bcrlock_remote;
            s_bcr_remote(tmr) <= s_cfgbus_remote_iddr(7)(1);
        
        end if;
        
        if rising_edge(p_clknet_in.cfgbus_clk40_remote) then
            if s_cfgbus_remote_iddr(7)(1) = '1' then
                v_maxcount := v_counter_remote;
                if (v_maxcount = (c_lhc_bunches_between_bcr-1)) then
                    v_bcrlock_remote := '1';
                    
                 else
                    v_bcrlock_remote := '0';

                end if;
                v_counter_remote := 0;                
            else
                v_counter_remote := v_counter_remote + 1;
           
            end if;

        end if; -- Clock edge
    
    end process; -- proc_sync_to_orbit


end generate gen_cfgbus_tmr;

    proc_cfgbus_side: process(p_clknet_in.cfgbus_clk40)
    begin
        if rising_edge(p_clknet_in.cfgbus_clk40) then
            if p_clknet_in.clksel = '1' then
                s_bcr <= s_bcr_local;
                s_bcr_locked <= s_bcr_locked_local; 
                --s_db_reg_rx<=s_db_reg_rx_local;
                s_db_reg_rx(0)(0 to cfb_strobe_reg-1) <= s_db_reg_rx_local(0)(0 to cfb_strobe_reg-1);
                s_db_reg_rx(1)(0 to cfb_strobe_reg-1) <= s_db_reg_rx_local(1)(0 to cfb_strobe_reg-1);
                s_db_reg_rx(2)(0 to cfb_strobe_reg-1) <= s_db_reg_rx_local(2)(0 to cfb_strobe_reg-1);
                s_db_reg_rx(0)(bc_number)<=s_lhc_bunch_counter_local(0);
                s_db_reg_rx(1)(bc_number)<=s_lhc_bunch_counter_local(1);
                s_db_reg_rx(2)(bc_number)<=s_lhc_bunch_counter_local(2);
                s_db_reg_rx(0)(cfb_strobe_reg) <= s_strobe_reg_local(0);
                s_db_reg_rx(1)(cfb_strobe_reg) <= s_strobe_reg_local(1);
                s_db_reg_rx(2)(cfb_strobe_reg) <= s_strobe_reg_local(2);
                s_db_reg_rx(0)(bc_number+1 to cfb_gbt_sc_address-1) <= s_db_reg_rx_local(0)(bc_number+1 to cfb_gbt_sc_address-1);
                s_db_reg_rx(1)(bc_number+1 to cfb_gbt_sc_address-1) <= s_db_reg_rx_local(1)(bc_number+1 to cfb_gbt_sc_address-1);
                s_db_reg_rx(2)(bc_number+1 to cfb_gbt_sc_address-1) <= s_db_reg_rx_local(2)(bc_number+1 to cfb_gbt_sc_address-1);
                s_db_reg_rx(0)(cfb_gbt_sc_address)<=s_gbt_sc_address(0);
                s_db_reg_rx(1)(cfb_gbt_sc_address)<=s_gbt_sc_address(1);
                s_db_reg_rx(2)(cfb_gbt_sc_address)<=s_gbt_sc_address(2);
            else
                s_bcr <= s_bcr_remote;
                s_bcr_locked <= s_bcr_locked_remote;
                --s_db_reg_rx<=s_db_reg_rx_remote;
                s_db_reg_rx(0)(0 to cfb_strobe_reg-1) <= s_db_reg_rx_remote(0)(0 to cfb_strobe_reg-1);
                s_db_reg_rx(1)(0 to cfb_strobe_reg-1) <= s_db_reg_rx_remote(1)(0 to cfb_strobe_reg-1);
                s_db_reg_rx(2)(0 to cfb_strobe_reg-1) <= s_db_reg_rx_remote(2)(0 to cfb_strobe_reg-1);
                s_db_reg_rx(0)(bc_number)<=s_lhc_bunch_counter_remote(0);
                s_db_reg_rx(1)(bc_number)<=s_lhc_bunch_counter_remote(1);
                s_db_reg_rx(2)(bc_number)<=s_lhc_bunch_counter_remote(2);
                s_db_reg_rx(0)(cfb_strobe_reg) <= s_strobe_reg_remote(0);
                s_db_reg_rx(1)(cfb_strobe_reg) <= s_strobe_reg_remote(1);
                s_db_reg_rx(2)(cfb_strobe_reg) <= s_strobe_reg_remote(2);
                s_db_reg_rx(0)(bc_number+1 to cfb_gbt_sc_address-1) <= s_db_reg_rx_remote(0)(bc_number+1 to cfb_gbt_sc_address-1);
                s_db_reg_rx(1)(bc_number+1 to cfb_gbt_sc_address-1) <= s_db_reg_rx_remote(1)(bc_number+1 to cfb_gbt_sc_address-1);
                s_db_reg_rx(2)(bc_number+1 to cfb_gbt_sc_address-1) <= s_db_reg_rx_remote(2)(bc_number+1 to cfb_gbt_sc_address-1);
                s_db_reg_rx(0)(cfb_gbt_sc_address)<=s_gbt_sc_address(0);
                s_db_reg_rx(1)(cfb_gbt_sc_address)<=s_gbt_sc_address(1);
                s_db_reg_rx(2)(cfb_gbt_sc_address)<=s_gbt_sc_address(2);
            end if;
        end if;
    end process;
    
    
 

    p_bcr_out.bcr<=(s_bcr(0) and s_bcr(1)) or (s_bcr(1) and s_bcr(2)) or (s_bcr(2) and s_bcr(0));
    p_bcr_out.bcr_locked<=(s_bcr_locked(0) and s_bcr_locked(1)) or (s_bcr_locked(1) and s_bcr_locked(2)) or (s_bcr_locked(2) and s_bcr_locked(0));
    p_bcr_out.bcr_local<=(s_bcr_local(0) and s_bcr_local(1)) or (s_bcr_local(1) and s_bcr_local(2)) or (s_bcr_local(2) and s_bcr_local(0));
    p_bcr_out.bcr_locked_local<=(s_bcr_locked_local(0) and s_bcr_locked_local(1)) or (s_bcr_locked_local(1) and s_bcr_locked_local(2)) or (s_bcr_locked_local(2) and s_bcr_locked_local(0));
    p_bcr_out.bcr_remote<=(s_bcr_remote(0) and s_bcr_remote(1)) or (s_bcr_remote(1) and s_bcr_remote(2)) or (s_bcr_remote(2) and s_bcr_remote(0));
    p_bcr_out.bcr_locked_remote<=(s_bcr_locked_remote(0) and s_bcr_locked_remote(1)) or (s_bcr_locked_remote(1) and s_bcr_locked_remote(2)) or (s_bcr_locked_remote(2) and s_bcr_locked_remote(0));
    p_bcr_out.count_local<= (s_lhc_bunch_counter_local(0) and s_lhc_bunch_counter_local(1)) or (s_lhc_bunch_counter_local(1) and s_lhc_bunch_counter_local(2)) or (s_lhc_bunch_counter_local(2) and s_lhc_bunch_counter_local(0));
    p_bcr_out.count_remote<= (s_lhc_bunch_counter_remote(0) and s_lhc_bunch_counter_remote(1)) or (s_lhc_bunch_counter_remote(1) and s_lhc_bunch_counter_remote(2)) or (s_lhc_bunch_counter_remote(2) and s_lhc_bunch_counter_remote(0));
    p_bcr_out.count<= (s_db_reg_rx(0)(bc_number) and s_db_reg_rx(1)(bc_number))or (s_db_reg_rx(1)(bc_number) and s_db_reg_rx(2)(bc_number)) or (s_db_reg_rx(2)(bc_number) and s_db_reg_rx(0)(bc_number));
    
gen_tmr_output : for reg in 0 to c_number_of_cfgbus_regs-1 generate    
begin
    
    p_db_reg_rx_out(reg) <= (s_db_reg_rx(0)(reg) and s_db_reg_rx(1)(reg)) or (s_db_reg_rx(1)(reg) and s_db_reg_rx(2)(reg)) or (s_db_reg_rx(2)(reg) and s_db_reg_rx(0)(reg));
    
end generate gen_tmr_output; 
    
end Behavioral;
    