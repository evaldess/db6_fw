----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santutio
-- 
-- Create Date: 04/24/2020 12:22:46 AM
-- Design Name: 
-- Module Name: db6_adc_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_adc_interface is
  Port ( 
        p_master_reset_in : in std_logic;
        --clock
        p_clknet_in                        : in t_db_clknet;
        --inputs
        p_adc_bitclk_in : in t_adc_clk_in;
        p_adc_frameclk_in : in t_adc_clk_in;
        p_adc_lg_data_in : in t_adc_data_in;
        p_adc_hg_data_in : in t_adc_data_in;
        --control
        p_adc_readout_control_in : in t_adc_readout_control;
        --output
        p_adc_readout_out       : out t_adc_readout;
        p_leds_out      : out std_logic_vector(3 downto 0)
  );
end db6_adc_interface;

architecture Behavioral of db6_adc_interface is

attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;
    
signal s_frameclk_pll : std_logic_vector(5 downto 0);
signal s_bitclk_se, s_bitclk_div : std_logic_vector(5 downto 0);

--attribute IOB of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
attribute keep of s_bitclk_se, s_bitclk_div : signal is "TRUE";
attribute dont_touch of s_bitclk_se, s_bitclk_div : signal is "TRUE";
    
    constant c_adc_test_pattern : std_logic_vector(13 downto 0) := "00" & x"cab";
    
    signal s_bufgce_div_ctrl_reset : std_logic_vector(5 downto 0) := (others => '0'); 

    signal s_idelay_ctrl_rdy, s_idelay_ctrl_reset : std_logic := '0';

    
    signal s_lg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_sm, s_lg_idelay_count_in_from_sm : t_idelay_integer_array;
    signal s_fc_idelay_count_in_from_sm : t_idelay_count := (others => (others => '0'));
    signal s_lg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal  s_hg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_idelay_ctrl_load, s_lg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_load, s_hg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_load, s_fc_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');  
    signal s_lg_idelay_ctrl_en_vtc, s_lg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_en_vtc, s_hg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_en_vtc, s_fc_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    
    
    signal s_channel_frame_missalignemt : std_logic_vector (5 downto 0) := (others => '1');
    
    --signal s_frame_strobe : std_logic_vector (5 downto 0);
    

    signal s_readout_lg_sr,s_readout_hg_sr : t_adc_sr;
    signal s_bitslice_lg_sr, s_bitslice_hg_sr, s_bitslice_fc_sr : t_bitslice_sr;
    signal s_adc_readout : t_adc_readout :=
    (
        lg_bitslip => (others => "0111"),
        hg_bitslip => (others => "0111"),
        lg_idelay_count => (others =>"000000000"),
        hg_idelay_count => (others =>"000000000"),
        fc_idelay_count => (others =>"000000000"),
        fc_data =>(others=>"00000000000000"),
        lg_data =>(others=>"00000000000000"),
        hg_data =>(others=>"00000000000000"),
        channel_missed_bit_count=>(others=>(others=>'0')),
        channel_frame_missalignemt => (others=>'0'),
        readout_initialized => '0',
        
        mb_adc_config_control => c_adc_register_init_config,
        
        leds => "0000"
    );
    attribute keep of s_adc_readout          : signal is "true";    
    
    type t_adc_readout_delay_control_array is array (0 to 5) of t_delay_control;
    signal s_lg_delay_control_array, s_hg_delay_control_array, s_fc_delay_control_array : t_adc_readout_delay_control_array := (others => c_delay_control);
    
    signal s_fc_bitslips_from_sm, s_hg_bitslips_from_sm, s_lg_bitslips_from_sm, s_hg_bitslips_from_hw, s_lg_bitslips_from_hw : t_bitslips_integer_array;
    signal s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word, s_fc_output_word : t_adc_data;
    
    signal s_adc_output_temp, s_adc_input_fc_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;

    type t_pll_bitslice_control_array is array (0 to 5) of t_mmcm_control;
    signal s_pll_bitslice_control_array : t_pll_bitslice_control_array;
    
    component pll_bitslice
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out1          : out    std_logic;
      clk_out2          : out    std_logic;
      -- Dynamic reconfiguration ports
      p_daddr_in             : in     std_logic_vector(6 downto 0);
      p_dclk_in              : in     std_logic;
      p_den_in               : in     std_logic;
      p_din_in                : in     std_logic_vector(15 downto 0);
      p_dout_out                : out    std_logic_vector(15 downto 0);
      p_dwe_in               : in     std_logic;
      p_drdy_out              : out    std_logic;
      -- Status and control signals
      p_reset_in             : in     std_logic;
      p_locked_out            : out    std_logic;
      p_clk280_in           : in     std_logic
     );
    end component;
    
begin

--readout connections
p_adc_readout_out <= s_adc_readout;


gen_adc_data_diff_to_se : for i in 0 to 5 generate

    i_IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_lg_delay_control_array(i).idatain,             -- Buffer diff_p output
        I  => p_adc_lg_data_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_lg_data_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_IBUFDS_DATA1 : IBUFDS -- ADC output High gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_hg_delay_control_array(i).idatain,             -- Buffer diff_p output
        I  => p_adc_hg_data_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_hg_data_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_fc_delay_control_array(i).idatain,             -- Buffer diff_p output
        I  => p_adc_frameclk_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_frameclk_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );

    i_IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_bitclk_se(i),             -- Buffer diff_p output
        I  => p_adc_bitclk_in(i).p,  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_bitclk_in(i).n  -- Diff_n buffer input (connect directly to top-level port)
        );


    bufgce_div_inst : bufgce_div
    generic map (
        bufgce_divide => 7, -- 1-8
        -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
        is_ce_inverted => '0', -- optional inversion for ce
        is_clr_inverted => '0', -- optional inversion for clr
        is_i_inverted => '0' -- optional inversion for i
        )
        port map (
        o => s_bitclk_div(i), -- 1-bit output: buffer
        ce => '1', -- 1-bit input: buffer enable
        clr => s_bufgce_div_ctrl_reset(i), -- 1-bit input: asynchronous clear
        i => s_bitclk_se(i) -- 1-bit input: buffer
    );

--    i_pll_bitslice : pll_bitslice
--   port map ( 
--      -- Clock out ports  
--       clk_out1 => s_bitclk(i),
--       clk_out2 => s_frameclk_pll(i),
--      -- Dynamic reconfiguration ports             
--       p_daddr_in => s_pll_bitslice_control_array(i).daddr_in,
--       p_dclk_in => p_clknet_in.osc_clk40,
--       p_den_in => s_pll_bitslice_control_array(i).den_in,
--       p_din_in => s_pll_bitslice_control_array(i).din_in,
--       p_dout_out => s_pll_bitslice_control_array(i).dout_out,
--       p_drdy_out => s_pll_bitslice_control_array(i).drdy_out,
--       p_dwe_in => s_pll_bitslice_control_array(i).dwe_in,
--      -- Status and control signals                
--       p_reset_in => s_pll_bitslice_control_array(i).reset_in,
--       p_locked_out => s_pll_bitslice_control_array(i).locked_out,
--       -- Clock in ports
--       p_clk280_in => s_bitclk_se(i)
-- );
    
    
--    s_pll_bitslice_control_array(i).daddr_in <= (others =>'0');
--    s_pll_bitslice_control_array(i).den_in <= '0';
--    s_pll_bitslice_control_array(i).din_in <= (others =>'0');
--    s_pll_bitslice_control_array(i).dwe_in <= '0';
--    s_pll_bitslice_control_array(i).psclk_in <= '0';
--    s_pll_bitslice_control_array(i).psen_in <= '0';
--    s_pll_bitslice_control_array(i).psincdec_in <= '0';
--    s_pll_bitslice_control_array(i).reset_in <= '0';
--    s_pll_bitslice_control_array(i).cddcreq_in <= '0';

    


end generate;


-- Generate ADC data input registers and output word mapping,
-- The output bits from each ADC is stored in four shift registers (two even and two odd).


s_idelay_ctrl_reset <= p_master_reset_in;

proc_load_hw_bitslips: process(p_adc_readout_control_in.db_side)
begin
    if p_adc_readout_control_in.db_side = "01" then

        s_lg_bitslips_from_hw <= (14,14,14,14,14,14);
        s_hg_bitslips_from_hw <= (14,14,14,14,14,14);

        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);
        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,0);
    
    elsif p_adc_readout_control_in.db_side = "10" then
        
        s_lg_bitslips_from_hw <= (14,14,14,14,14,14);
        s_hg_bitslips_from_hw <= (14,14,14,14,14,14);
        
        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);
        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,0);

    else

    end if;

end process;

gen_adc_channels: for v_adc in 0 to 5 generate

    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);
    s_adc_readout.hg_bitslip(v_adc) <= p_adc_readout_control_in.hg_bitslip(v_adc);
    s_adc_readout.lg_bitslip(v_adc) <= p_adc_readout_control_in.lg_bitslip(v_adc);
--p_channel_frame_missalignemt_out(v_adc) <= s_channel_frame_missalignemt(v_adc);
    


i_idelaye3_data_lg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_lg_delay_control_array(v_adc).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_lg_delay_control_array(v_adc).cntvalueout,--s_lg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_lg_delay_control_array(v_adc).dataout,--s_adc_lg_data_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => s_lg_delay_control_array(v_adc).casc_in,--'0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_lg_delay_control_array(v_adc).casc_return,--'0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_lg_delay_control_array(v_adc).ce, --'0', -- 1-bit input: active high enable increment/decrement input
        clk => s_lg_delay_control_array(v_adc).clk,--s_bitclk(v_adc), -- 1-bit input: clock input
        cntvaluein => s_lg_delay_control_array(v_adc).cntvaluein, --s_lg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => s_lg_delay_control_array(v_adc).datain, --'0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_lg_delay_control_array(v_adc).en_vtc, --s_lg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_lg_delay_control_array(v_adc).idatain, --s_adc_lg_data_se(v_adc), -- 1-bit input: data input from the logic
        inc => s_lg_delay_control_array(v_adc).inc, --'0', -- 1-bit input: increment / decrement tap delay input
        load => s_lg_delay_control_array(v_adc).load, --s_lg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_lg_delay_control_array(v_adc).rst --s_lg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_lg_delay_control_array(v_adc).clk<= s_bitclk_se(v_adc);
s_lg_delay_control_array(v_adc).rst <= s_lg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc); 
s_lg_delay_control_array(v_adc).load <= s_lg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_load(v_adc);
s_lg_delay_control_array(v_adc).en_vtc <= s_lg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
s_lg_delay_control_array(v_adc).cntvaluein <= std_logic_vector(to_unsigned(s_lg_idelay_count_in_from_sm(v_adc) + s_lg_idelay_count_in_from_hw(v_adc),9));-- p_adc_readout_control_in.lg_idelay_count(v_adc);
s_adc_readout.lg_idelay_count(v_adc) <= s_lg_delay_control_array(v_adc).cntvalueout;


i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_hg_delay_control_array(v_adc).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_hg_delay_control_array(v_adc).cntvalueout,--s_hg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_hg_delay_control_array(v_adc).dataout,--s_adc_hg_data_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => s_hg_delay_control_array(v_adc).casc_in,--'0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_hg_delay_control_array(v_adc).casc_return,--'0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_hg_delay_control_array(v_adc).ce, --'0', -- 1-bit input: active high enable increment/decrement input
        clk => s_hg_delay_control_array(v_adc).clk,--s_bitclk(v_adc), -- 1-bit input: clock input
        cntvaluein => s_hg_delay_control_array(v_adc).cntvaluein, --s_hg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => s_hg_delay_control_array(v_adc).datain, --'0',--s_data_hg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_hg_delay_control_array(v_adc).en_vtc, --s_hg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_hg_delay_control_array(v_adc).idatain, --s_adc_hg_data_se(v_adc), -- 1-bit input: data input from the logic
        inc => s_hg_delay_control_array(v_adc).inc, --'0', -- 1-bit input: increment / decrement tap delay input
        load => s_hg_delay_control_array(v_adc).load, --s_hg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_hg_delay_control_array(v_adc).rst --s_hg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_hg_delay_control_array(v_adc).clk<= s_bitclk_se(v_adc);
s_hg_delay_control_array(v_adc).rst <= s_hg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc); 
s_hg_delay_control_array(v_adc).load <= s_hg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_load(v_adc);
s_hg_delay_control_array(v_adc).en_vtc <= s_hg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
s_hg_delay_control_array(v_adc).cntvaluein <= std_logic_vector(to_unsigned(s_hg_idelay_count_in_from_sm(v_adc) + s_hg_idelay_count_in_from_hw(v_adc),9));-- p_adc_readout_control_in.hg_idelay_count(v_adc);
s_adc_readout.hg_idelay_count(v_adc) <= s_hg_delay_control_array(v_adc).cntvalueout;


i_idelaye3_data_fc : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => s_fc_delay_control_array(v_adc).casc_out, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_fc_delay_control_array(v_adc).cntvalueout,--s_fc_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_fc_delay_control_array(v_adc).dataout,--s_adc_fc_data_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => s_fc_delay_control_array(v_adc).casc_in,--'0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  s_fc_delay_control_array(v_adc).casc_return,--'0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => s_fc_delay_control_array(v_adc).ce, --'0', -- 1-bit input: active high enable increment/decrement input
        clk => s_fc_delay_control_array(v_adc).clk,--s_bitclk(v_adc), -- 1-bit input: clock input
        cntvaluein => s_fc_delay_control_array(v_adc).cntvaluein, --s_fc_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => s_fc_delay_control_array(v_adc).datain, --'0',--s_data_fc(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_fc_delay_control_array(v_adc).en_vtc, --s_fc_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_fc_delay_control_array(v_adc).idatain, --s_adc_fc_data_se(v_adc), -- 1-bit input: data input from the logic
        inc => s_fc_delay_control_array(v_adc).inc, --'0', -- 1-bit input: increment / decrement tap delay input
        load => s_fc_delay_control_array(v_adc).load, --s_fc_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_fc_delay_control_array(v_adc).rst --s_fc_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_fc_delay_control_array(v_adc).clk<= s_bitclk_se(v_adc);
s_fc_delay_control_array(v_adc).rst <= p_adc_readout_control_in.fc_idelay_ctrl_reset(v_adc) or s_fc_idelay_ctrl_reset_from_sm(v_adc);
s_fc_delay_control_array(v_adc).load <= s_fc_idelay_ctrl_load_from_sm(v_adc);--p_adc_readout_control_in.fc_idelay_load(v_adc);
s_fc_delay_control_array(v_adc).en_vtc <= s_fc_idelay_ctrl_en_vtc_from_sm(v_adc);-- p_adc_readout_control_in.fc_idelay_en_vtc(v_adc);
s_fc_delay_control_array(v_adc).cntvaluein <= s_fc_idelay_count_in_from_sm(v_adc); --p_adc_readout_control_in.fc_idelay_count(v_adc);
s_adc_readout.fc_idelay_count(v_adc) <= s_fc_delay_control_array(v_adc).cntvalueout;

i_iddre1_lg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_lg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_lg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => s_bitclk_se(v_adc), -- 1-bit input: high-speed clock
cb => s_bitclk_se(v_adc), -- 1-bit input: inversion of high-speed clock c
d => s_lg_delay_control_array(v_adc).dataout, -- 1-bit input: serial data input
r => s_lg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);

i_iddre1_hg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_hg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_hg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => s_bitclk_se(v_adc), -- 1-bit input: high-speed clock
cb => s_bitclk_se(v_adc), -- 1-bit input: inversion of high-speed clock c
d => s_hg_delay_control_array(v_adc).dataout, -- 1-bit input: serial data input
r => s_hg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);

i_iddre1_fc : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_fc_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_fc_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => s_bitclk_se(v_adc), -- 1-bit input: high-speed clock
cb => s_bitclk_se(v_adc), -- 1-bit input: inversion of high-speed clock c
d => s_fc_delay_control_array(v_adc).dataout, -- 1-bit input: serial data input
r => s_fc_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);


--i_iserdese3_fc : iserdese3
--generic map (
--SIM_DEVICE => "ULTRASCALE_PLUS",
--data_width => 4, -- parallel data width (4,8)
--fifo_enable => "true", -- enables the use of the fifo
--fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
--is_clk_b_inverted => '1', -- optional inversion for clk_b
--is_clk_inverted => '0', -- optional inversion for clk
--is_rst_inverted => '0' -- optional inversion for rst
--)
--port map (

--fifo_empty => open, -- 1-bit output: fifo empty flag
--internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
--q => s_bitslice_fc_sr(v_adc), -- 8-bit registered output
--clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
--clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
--d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
--fifo_rd_clk => '0', -- 1-bit input: fifo read clock
--fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
--rst => p_idelay_ctrl_reset_hg_in(v_adc) -- 1-bit input: asynchronous reset

--);
--s_iserdes_ctrl_reset_fc(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc) and p_idelay_ctrl_reset_lg_in(v_adc);


    proc_shift_in : process(s_bitclk_se(v_adc)) -- Odd data bits clocked in on rising edge of adc clocks 
    begin


        if rising_edge(s_bitclk_se(v_adc)) then
            --if p_adc_mode_in = '0' then
            
                s_adc_input_hg_temp(v_adc) <= s_adc_input_hg_temp(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 2) downto 0) & s_bitslice_hg_sr(v_adc)(0) & s_bitslice_hg_sr(v_adc)(1);
                s_adc_input_lg_temp(v_adc) <= s_adc_input_lg_temp(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 2) downto 0) & s_bitslice_lg_sr(v_adc)(0) & s_bitslice_lg_sr(v_adc)(1);
                s_adc_input_fc_temp(v_adc) <= s_adc_input_fc_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 2) downto 0) & s_bitslice_fc_sr(v_adc)(0) & s_bitslice_fc_sr(v_adc)(1);
            --else 
        
        end if;
    end process;



    proc_frame_capture : process(s_bitclk_div(v_adc))
    constant c_phase_offset : integer:= 0;
    begin
        if rising_edge(s_bitclk_div(v_adc)) then
            s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)(c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto (c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.hg_bitslip(v_adc)))));
            s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto (c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.lg_bitslip(v_adc)))));
            s_fc_output_word(v_adc) <= s_adc_input_fc_temp(v_adc)(c_phase_offset + s_fc_bitslips_from_sm(v_adc) + (c_adc_bit_number-1)  downto (c_phase_offset + s_fc_bitslips_from_sm(v_adc)));

            s_adc_readout.lg_data(v_adc) <= s_lg_adc_output_word(v_adc);
            s_adc_readout.hg_data(v_adc) <= s_hg_adc_output_word(v_adc);
        
            --12 bits
            --s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
            --s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));    

        end if;
        
    end process;




    proc_phase_reset : process(p_clknet_in.db_clk40) -- Capture ADC words synchronous to ADC (bit) clock
    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
    type t_clk_edge_sync_sm is (st_wait_falling_edge, st_wait_rising_edge, st_rising_edge);
    variable v_clk_edge_sync_sm : t_clk_edge_sync_sm := st_wait_falling_edge;
    variable v_frameclk_sm, v_clk40db_sm : t_frameclk_sm := st_low;
    type t_reset_sm is (st_initialize, st_release_fc_serdes, st_reset_divclk, st_reset_idelay, st_reset_iserdes, st_disable_vtc, 
    st_load_value, st_wait_load, st_enable_vtc, st_calculate_fc_bitslips, st_check_fc_bitslip_stability, st_idle);
    variable v_reset_sm : t_reset_sm := st_initialize;
    variable v_counter : integer := 0;
    constant c_phase_offset : integer:= 0;
    variable  v_fc_tries: integer := 0;
    constant max_fc_tries : integer := 1024;
    variable v_idelay_fc : integer := 0;
   
        
    begin
        if rising_edge(p_clknet_in.db_clk40) then
        
                    if p_master_reset_in = '0' then


                        s_adc_readout.channel_missed_bit_count(v_adc) <=  std_logic_vector(to_signed(s_fc_bitslips_from_sm(v_adc), 31));
                        
                        case v_reset_sm is
                    
                            when st_initialize =>
                                s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                                
                                s_channel_frame_missalignemt(v_adc) <= '1';
                                
                                v_reset_sm:=st_release_fc_serdes;
                                
                            when st_release_fc_serdes =>
                                s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';

                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                                
                                s_channel_frame_missalignemt(v_adc) <= '1';
                                --if (v_counter < 28) then
                                --    v_counter:=v_counter+1;
                                --else
                                    v_reset_sm:=st_reset_divclk;
                                --    v_counter:=0;
                                --end if;
                            
                            when st_reset_divclk => 
--                                case v_clk_edge_sync_sm is
--                                    when st_wait_falling_edge =>
--                                        if s_fc_output_word(v_adc)(1 downto 0) = "00" then
--                                            v_clk_edge_sync_sm:=st_wait_rising_edge;
--                                        end if;
--                                    when st_wait_rising_edge =>
--                                        if s_fc_output_word(v_adc)(1 downto 0) = "11" then
--                                            v_clk_edge_sync_sm:=st_rising_edge;
--                                        end if;                                   
--                                    when st_rising_edge =>
                                        s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                                        s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                                        s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                        s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                                        s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                        s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                        s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';
        
                                        s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                        s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                                        
                                        s_channel_frame_missalignemt(v_adc) <= '1';
                                        
                                        v_reset_sm:=st_reset_idelay;   
--                                    when others=>
--                                end case;
                          
                                                        
                            when st_reset_idelay => 
                                s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

                                s_channel_frame_missalignemt(v_adc) <= '1';  
                                
                                v_reset_sm:=st_reset_iserdes;                        
                                                            
                            when st_reset_iserdes =>
                                s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';

                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

                                s_channel_frame_missalignemt(v_adc) <= '1';
                                
                                v_reset_sm:=st_disable_vtc;                        
                                
                            when st_disable_vtc =>

                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
                                s_channel_frame_missalignemt(v_adc) <= '1';
                                
                                v_reset_sm:=st_load_value;
                                
                            when st_load_value =>
                            
                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '1';
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '1';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';    

                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '1';
                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
                                s_channel_frame_missalignemt(v_adc) <= '1';                        
                            
                                v_reset_sm:=st_wait_load;


                            when st_wait_load =>

                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';  

                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                 
                                s_channel_frame_missalignemt(v_adc) <= '1';

                                v_reset_sm:=st_enable_vtc;
                                
                            when st_enable_vtc =>

                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                 
                                s_channel_frame_missalignemt(v_adc) <= '1';

                                v_reset_sm:=st_calculate_fc_bitslips;
                            when st_calculate_fc_bitslips=>
                                
                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1'; 
                                
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
                                v_fc_tries := 0;
                                
                                if s_fc_output_word(v_adc) = "11111110000000" then
                                    --v_counter :=0;
                                    v_reset_sm:=st_check_fc_bitslip_stability;
                                    
                                else
                                    if s_fc_bitslips_from_sm(v_adc) < (((c_oversamples-1)*c_adc_bit_number)-c_phase_offset) then
                                        s_fc_bitslips_from_sm(v_adc)<=s_fc_bitslips_from_sm(v_adc)+1;
                                        --v_counter :=s_fc_bitslips_from_sm(v_adc)+1;--v_counter+1;
                                    else
                                        s_fc_bitslips_from_sm(v_adc) <= 0;
                                        --v_counter :=0;
                                    end if;
                                    
                                end if;
                                s_channel_frame_missalignemt(v_adc) <= '1';
                            
                            when st_check_fc_bitslip_stability =>
                                
                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));
    
                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1'; 
                                
                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
                                if v_fc_tries < max_fc_tries then
                                    
                                    if s_fc_output_word(v_adc) = "11111110000000" then
                                        v_fc_tries:=v_fc_tries+1;
                                        v_reset_sm:=st_check_fc_bitslip_stability;
                                    else
                                        v_fc_tries:=0;
                                        if v_idelay_fc <512 then
                                            v_idelay_fc:= v_idelay_fc+1;
                                            
                                        else
                                            v_idelay_fc:=0;
                                        end if;
                                        v_reset_sm:=st_reset_iserdes;
                                    end if;
                                    
                                else
                                    v_fc_tries:=0;
                                    v_reset_sm:=st_idle;
                                end if;
                                
                                s_channel_frame_missalignemt(v_adc) <= not s_channel_frame_missalignemt(v_adc);--'1';
                                
                            when st_idle =>
                                
                                s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                                --s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                                --s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                                --s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                                --s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';
                                s_channel_frame_missalignemt(v_adc) <= '0';
    
                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));

                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);
                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_load(v_adc);
                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
                                
                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= s_lg_idelay_ctrl_reset_from_sm(v_adc); 
                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_load(v_adc);
                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
                                
                            
                            when others=>
                                v_reset_sm:=st_initialize;
                        end case;
                        
                    
                    else
                    
                        v_reset_sm:=st_initialize;
                        s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                        s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                        s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                        s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                        s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                        s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                        s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';
                        s_channel_frame_missalignemt(v_adc) <= '1';
            
                    end if;
                                            
                

                             
        end if;
    end process;


end generate;


--proc_adc_pattern_config : process(p_clknet_in.clk40)
--variable v_counter : integer := 0;
--type t_sm_adc_pattern_config is (st_startup, st_config_pattern, st_wait_for_config, st_check_config, st_config_default, st_idle);
--variable v_sm_adc_pattern_config : t_sm_adc_pattern_config := st_startup;
 
--begin
--    if rising_edge(p_clknet_in.clk40) then
    
--        case v_sm_adc_pattern_config is
        
--            when st_startup=>
--                if p_adc_readout_control_in.adc_config_done = '1' then
--                    v_sm_adc_pattern_config :=st_config_pattern;
--                    s_adc_readout.mb_adc_config_control.mode <= '1';
--                    s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
--                    s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
--                    s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
--                    s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"B5";     
--                    s_adc_readout.mb_adc_config_control.adc_registers(3) <= "10" & c_adc_test_pattern(13 downto 8);
--                    s_adc_readout.mb_adc_config_control.adc_registers(4) <= c_adc_test_pattern(13 downto 8);
--                    --c_adc_test_pattern
--                end if;
--            when st_config_pattern =>
            
--                s_adc_readout.mb_adc_config_control.mode <= '1';
--                s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
--                s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
--                s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
--                s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"B5";     
--                s_adc_readout.mb_adc_config_control.adc_registers(3) <= "10" & c_adc_test_pattern(13 downto 8);
--                s_adc_readout.mb_adc_config_control.adc_registers(4) <= c_adc_test_pattern(13 downto 8);
--                s_adc_readout.mb_adc_config_control.trigger_mb_adc_config <= '1';
--                if p_adc_readout_control_in.adc_config_done = '0' then
--                    v_sm_adc_pattern_config:= st_wait_for_config;
--                end if;
               
                    
--            when st_wait_for_config=>
--                s_adc_readout.mb_adc_config_control.trigger_mb_adc_config <= '0';
--                if p_adc_readout_control_in.adc_config_done = '1' then
--                    v_sm_adc_pattern_config:= st_check_config;
--                end if;
--            when st_check_config =>
--                if v_counter < 40000000 then
--                    if s_channel_frame_missalignemt(0) = '1'
--                        or (s_channel_frame_missalignemt(1)  = '1')
--                        or (s_channel_frame_missalignemt(2)  = '1')
--                        or (s_channel_frame_missalignemt(3)  = '1')
--                        or (s_channel_frame_missalignemt(4)  = '1')
--                        or (s_channel_frame_missalignemt(5) = '1')  then
                        
--                    else
--                        v_sm_adc_pattern_config :=st_startup;
--                    end if;
--                end if;
            
            
--            when others => 
--                v_sm_adc_pattern_config :=st_idle;
--        end case;
    
--    end if;


--end process;


--proc_bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
--begin
--    if falling_edge(p_clknet_in.clk40) then
--        s_adc_readout.lg_data <= s_lg_adc_output_word;
--        s_adc_readout.hg_data <= s_hg_adc_output_word;--s_fc_output_word;

--    end if;
--end process;

    

        s_adc_readout.readout_initialized<= s_channel_frame_missalignemt(0)
                                            and s_channel_frame_missalignemt(1)
                                            and s_channel_frame_missalignemt(2)
                                            and s_channel_frame_missalignemt(3)
                                            and s_channel_frame_missalignemt(4)
                                            and s_channel_frame_missalignemt(5);
        s_adc_readout.mb_adc_config_control.mode <= '0';
        s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
        s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
        s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
        s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"B5";     
        s_adc_readout.mb_adc_config_control.adc_registers(3) <= x"00";
        s_adc_readout.mb_adc_config_control.adc_registers(4) <= x"00";


end Behavioral;


------------------------------------------------------------------------------------
---- Company: 
---- Engineer: Eduardo Valdes Santutio
---- 
---- Create Date: 04/24/2020 12:22:46 AM
---- Design Name: 
---- Module Name: db6_adc_interface - Behavioral
---- Project Name: 
---- Target Devices: 
---- Tool Versions: 
---- Description: 
---- 
---- Dependencies: 
---- 
---- Revision:
---- Revision 0.01 - File Created
---- Additional Comments:
---- 
------------------------------------------------------------------------------------


--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

---- Uncomment the following library declaration if using
---- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--library tilecal;
--use tilecal.db6_design_package.all;

--entity db6_adc_interface is
--  Port ( 
--        p_master_reset_in : in std_logic;
--        --clock
--        p_clknet_in                        : in t_db_clknet;
--        --inputs
--        p_adc_bitclk_in : in t_adc_clk_in;
--        p_adc_frameclk_in : in t_adc_clk_in;
--        p_adc_lg_data_in : in t_adc_data_in;
--        p_adc_hg_data_in : in t_adc_data_in;
--        --control
--        p_adc_readout_control_in : in t_adc_readout_control;
--        --output
--        p_adc_readout_out       : out t_adc_readout
--  );
--end db6_adc_interface;

--architecture Behavioral of db6_adc_interface is

--attribute IOB: string;
--attribute keep: string;
--attribute dont_touch: string;
    
--signal s_adc_lg_data_se, s_adc_lg_data_delayed : std_logic_vector(5 downto 0);
--signal s_adc_hg_data_se, s_adc_hg_data_delayed : std_logic_vector(5 downto 0);
--signal s_frameclk_se, s_frameclk_delayed : std_logic_vector(5 downto 0);
--signal s_bitclk_se, s_bitclk, s_bitclk_div : std_logic_vector(5 downto 0);

----attribute IOB of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
--attribute keep of s_adc_lg_data_se, s_adc_lg_data_delayed, s_adc_hg_data_se, s_adc_hg_data_delayed, s_frameclk_se, s_frameclk_delayed, s_bitclk_se, s_bitclk, s_bitclk_div : signal is "TRUE";
--attribute dont_touch of s_adc_lg_data_se, s_adc_lg_data_delayed, s_adc_hg_data_se, s_adc_hg_data_delayed, s_frameclk_se, s_frameclk_delayed, s_bitclk_se, s_bitclk, s_bitclk_div : signal is "TRUE";
    
--    constant c_adc_test_pattern : std_logic_vector(13 downto 0) := "00" & x"cab";
    
--    signal s_bufgce_div_ctrl_reset : std_logic_vector(5 downto 0) := (others => '0'); 

--    signal s_idelay_ctrl_rdy, s_idelay_ctrl_reset : std_logic := '0';

    
--    signal s_lg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_sm, s_lg_idelay_count_in_from_sm : t_idelay_integer_array;
--    signal s_lg_idelay_count_in, s_lg_idelay_count_out : t_idelay_count := (others => (others => '0'));
--    signal s_hg_idelay_count_in, s_hg_idelay_count_out : t_idelay_count := (others => (others => '0'));
--    signal s_fc_idelay_count_in, s_fc_idelay_count_out, s_fc_idelay_count_in_from_sm : t_idelay_count := (others => (others => '0'));
--    signal s_lg_idelay_ctrl_reset, s_lg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_hg_idelay_ctrl_reset, s_hg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_fc_idelay_ctrl_reset, s_fc_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

--    signal s_lg_idelay_ctrl_load, s_lg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_hg_idelay_ctrl_load, s_hg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_fc_idelay_ctrl_load, s_fc_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');  
--    signal s_lg_idelay_ctrl_en_vtc, s_lg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_hg_idelay_ctrl_en_vtc, s_hg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_fc_idelay_ctrl_en_vtc, s_fc_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

--    signal s_lg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_hg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
--    signal s_fc_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    
    
--    signal s_channel_frame_missalignemt : std_logic_vector (5 downto 0) := (others => '1');
    
--    signal s_frame_strobe : std_logic_vector (5 downto 0);
    

--    signal s_readout_lg_sr,s_readout_hg_sr : t_adc_sr;
--    signal s_bitslice_lg_sr, s_bitslice_hg_sr, s_bitslice_fc_sr : t_bitslice_sr;
--    signal s_adc_readout : t_adc_readout :=
--    (
--        lg_bitslip => (others => "0111"),
--        hg_bitslip => (others => "0111"),
--        lg_idelay_count => (others =>"000000000"),
--        hg_idelay_count => (others =>"000000000"),
--        fc_idelay_count => (others =>"000000000"),
--        lg_data =>(others=>"00000000000000"),
--        hg_data =>(others=>"00000000000000"),
--        channel_missed_bit_count=>(others=>(others=>'0')),
--        channel_frame_missalignemt => (others=>'0'),
--        readout_initialized => '0',
        
--        mb_adc_config_control => c_adc_register_init_config
--    );
--    attribute keep of s_adc_readout          : signal is "true";    
    
--    signal s_fc_bitslips_from_sm, s_hg_bitslips_from_sm, s_lg_bitslips_from_sm, s_hg_bitslips_from_hw, s_lg_bitslips_from_hw : t_bitslips_integer_array;
--    signal s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word, s_fc_output_word : t_adc_data;
    
--    signal s_adc_output_temp, s_adc_input_fc_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;

--begin

----readout connections
--p_adc_readout_out <= s_adc_readout;


--gen_adc_data_diff_to_se : for i in 0 to 5 generate

--    i_IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_adc_lg_data_se(i),             -- Buffer diff_p output
--        I  => p_adc_lg_data_in(i)(0),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_lg_data_in(i)(1)  -- Diff_n buffer input (connect directly to top-level port)
--        );

--    i_IBUFDS_DATA1 : IBUFDS -- ADC output High gain
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_adc_hg_data_se(i),             -- Buffer diff_p output
--        I  => p_adc_hg_data_in(i)(0),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_hg_data_in(i)(1)  -- Diff_n buffer input (connect directly to top-level port)
--        );

--    i_IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_frameclk_se(i),             -- Buffer diff_p output
--        I  => p_adc_frameclk_in(i)(0),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_frameclk_in(i)(1)  -- Diff_n buffer input (connect directly to top-level port)
--        );

--    i_IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_bitclk_se(i),             -- Buffer diff_p output
--        I  => p_adc_bitclk_in(i)(0),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_bitclk_in(i)(1)  -- Diff_n buffer input (connect directly to top-level port)
--        );


--    i_bufg_bitclk : bufg
--        port map (
--            I => s_bitclk_se(i),
--            O => s_bitclk(i)
--        );
    


--    bufgce_div_inst : bufgce_div
--    generic map (
--        bufgce_divide => 7, -- 1-8
--        -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
--        is_ce_inverted => '0', -- optional inversion for ce
--        is_clr_inverted => '0', -- optional inversion for clr
--        is_i_inverted => '0' -- optional inversion for i
--        )
--        port map (
--        o => s_bitclk_div(i), -- 1-bit output: buffer
--        ce => '1', -- 1-bit input: buffer enable
--        clr => s_bufgce_div_ctrl_reset(i), -- 1-bit input: asynchronous clear
--        i => s_bitclk_se(i) -- 1-bit input: buffer
--    );


--end generate;


---- Generate ADC data input registers and output word mapping,
---- The output bits from each ADC is stored in four shift registers (two even and two odd).


--s_idelay_ctrl_reset <= p_master_reset_in;

--proc_load_hw_bitslips: process(p_adc_readout_control_in.db_side)
--begin
--    if p_adc_readout_control_in.db_side = "01" then

--        s_lg_bitslips_from_hw <= (14,14,14,14,14,14);
--        s_hg_bitslips_from_hw <= (14-1,14,14,14,14,14);

--        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);
--        s_hg_idelay_count_in_from_hw <= (60,0,0,0,0,0);
    
--    elsif p_adc_readout_control_in.db_side = "10" then
        
--        s_lg_bitslips_from_hw <= (14-1,14,14,14,14,14);
--        s_hg_bitslips_from_hw <= (14,14,14,14,14,14);
        
--        s_lg_idelay_count_in_from_hw <= (60,0,0,0,0,60);
--        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,200);

--    else

--    end if;

--end process;

--gen_adc_channels: for v_adc in 0 to 5 generate

--    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);
--    s_adc_readout.hg_bitslip(v_adc) <= p_adc_readout_control_in.hg_bitslip(v_adc);
--    s_adc_readout.lg_bitslip(v_adc) <= p_adc_readout_control_in.lg_bitslip(v_adc);
----p_channel_frame_missalignemt_out(v_adc) <= s_channel_frame_missalignemt(v_adc);
    


--i_idelaye3_data_lg : idelaye3
--    generic map (
--        SIM_DEVICE => "ULTRASCALE",
--        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
--        delay_format => "count", -- units of the delay_value (time, count)
--        delay_src => "idatain", -- delay input (idatain, datain)
--        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
--        delay_value => 0, -- input delay value setting
--        is_clk_inverted => '0', -- optional inversion for clk
--        is_rst_inverted => '0', -- optional inversion for rst
--        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
--        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
--        )
--        port map (
--        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
--        cntvalueout => s_lg_idelay_count_out(v_adc), -- 9-bit output: counter value output
--        dataout => s_adc_lg_data_delayed(v_adc), -- 1-bit output: delayed data output
--        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
--        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
--        ce => '0', -- 1-bit input: active high enable increment/decrement input
--        clk => s_bitclk(v_adc), -- 1-bit input: clock input
--        cntvaluein => s_lg_idelay_count_in(v_adc), -- 9-bit input: counter value input
--        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
--        en_vtc => s_lg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
--        idatain => s_adc_lg_data_se(v_adc), -- 1-bit input: data input from the logic
--        inc => '0', -- 1-bit input: increment / decrement tap delay input
--        load => s_lg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
--        rst => s_lg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
--);
--s_lg_idelay_ctrl_reset(v_adc) <= s_lg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc); 
--s_lg_idelay_ctrl_load(v_adc) <= s_lg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_load(v_adc);
--s_lg_idelay_ctrl_en_vtc(v_adc) <= s_lg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
--s_lg_idelay_count_in(v_adc) <= std_logic_vector(to_unsigned(s_lg_idelay_count_in_from_sm(v_adc) + s_lg_idelay_count_in_from_hw(v_adc),9));-- p_adc_readout_control_in.lg_idelay_count(v_adc);
--s_adc_readout.lg_idelay_count(v_adc) <= s_lg_idelay_count_out(v_adc);


--i_idelaye3_data_hg : idelaye3
--    generic map (
--        SIM_DEVICE => "ULTRASCALE",
--        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
--        delay_format => "count", -- units of the delay_value (time, count)
--        delay_src => "idatain", -- delay input (idatain, datain)
--        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
--        delay_value => 0, -- input delay value setting
--        is_clk_inverted => '0', -- optional inversion for clk
--        is_rst_inverted => '0', -- optional inversion for rst
--        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
--        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
--        )
--        port map (
--        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
--        cntvalueout => s_hg_idelay_count_out(v_adc), -- 9-bit output: counter value output
--        dataout => s_adc_hg_data_delayed(v_adc), -- 1-bit output: delayed data output
--        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
--        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
--        ce => '0', -- 1-bit input: active high enable increment/decrement input
--        clk => s_bitclk(v_adc), -- 1-bit input: clock input
--        cntvaluein => s_hg_idelay_count_in(v_adc), -- 9-bit input: counter value input
--        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
--        en_vtc => s_hg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
--        idatain => s_adc_hg_data_se(v_adc), -- 1-bit input: data input from the logic
--        inc => '0', -- 1-bit input: increment / decrement tap delay input
--        load => s_hg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
--        rst => s_hg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
--);
--s_hg_idelay_ctrl_reset(v_adc) <= s_hg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);
--s_hg_idelay_ctrl_load(v_adc) <= s_hg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_load(v_adc);
--s_hg_idelay_ctrl_en_vtc(v_adc) <= s_hg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
--s_hg_idelay_count_in(v_adc) <= std_logic_vector(to_signed(s_hg_idelay_count_in_from_sm(v_adc) + s_hg_idelay_count_in_from_hw(v_adc),9)); --p_adc_readout_control_in.hg_idelay_count(v_adc);
--s_adc_readout.hg_idelay_count(v_adc) <= s_hg_idelay_count_out(v_adc);


--i_idelaye3_data_fc : idelaye3
--    generic map (
--        SIM_DEVICE => "ULTRASCALE",
--        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
--        delay_format => "count", -- units of the delay_value (time, count)
--        delay_src => "idatain", -- delay input (idatain, datain)
--        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
--        delay_value => 0, -- input delay value setting
--        is_clk_inverted => '0', -- optional inversion for clk
--        is_rst_inverted => '0', -- optional inversion for rst
--        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
--        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
--        )
--        port map (
--        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
--        cntvalueout => s_fc_idelay_count_out(v_adc), -- 9-bit output: counter value output
--        dataout => s_frameclk_delayed(v_adc), -- 1-bit output: delayed data output
--        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
--        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
--        ce => '0', -- 1-bit input: active high enable increment/decrement input
--        clk => s_bitclk(v_adc), -- 1-bit input: clock input
--        cntvaluein => s_fc_idelay_count_in(v_adc), -- 9-bit input: counter value input
--        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
--        en_vtc => s_fc_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
--        idatain => s_frameclk_se(v_adc), -- 1-bit input: data input from the logic
--        inc => '0', -- 1-bit input: increment / decrement tap delay input
--        load => s_fc_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
--        rst => s_fc_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
--);

--s_fc_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.fc_idelay_ctrl_reset(v_adc) or s_fc_idelay_ctrl_reset_from_sm(v_adc);
--s_fc_idelay_ctrl_load(v_adc) <= s_fc_idelay_ctrl_load_from_sm(v_adc);--p_adc_readout_control_in.fc_idelay_load(v_adc);
--s_fc_idelay_ctrl_en_vtc(v_adc) <= s_fc_idelay_ctrl_en_vtc_from_sm(v_adc);-- p_adc_readout_control_in.fc_idelay_en_vtc(v_adc);
--s_fc_idelay_count_in(v_adc) <= s_fc_idelay_count_in_from_sm(v_adc); --p_adc_readout_control_in.fc_idelay_count(v_adc);
--s_adc_readout.fc_idelay_count(v_adc) <= s_fc_idelay_count_out(v_adc);

--i_iddre1_lg : iddre1
--generic map (
--ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
--is_c_inverted => '0', -- optional inversion for c
--is_cb_inverted => '1' -- optional inversion for c
--)
--port map (
--q1 => s_bitslice_lg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
--q2 => s_bitslice_lg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
--c => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--cb => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock c
--d => s_adc_lg_data_delayed(v_adc), -- 1-bit input: serial data input
--r => s_lg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
--);


----s_iserdes_ctrl_reset_lg(v_adc) <= p_idelay_ctrl_reset_lg_in(v_adc);

--i_iddre1_hg : iddre1
--generic map (
--ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
--is_c_inverted => '0', -- optional inversion for c
--is_cb_inverted => '1' -- optional inversion for c
--)
--port map (
--q1 => s_bitslice_hg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
--q2 => s_bitslice_hg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
--c => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--cb => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock c
--d => s_adc_hg_data_delayed(v_adc), -- 1-bit input: serial data input
--r => s_hg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
--);
----s_iserdes_ctrl_reset_hg(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc);

--i_iddre1_fc : iddre1
--generic map (
--ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
--is_c_inverted => '0', -- optional inversion for c
--is_cb_inverted => '1' -- optional inversion for c
--)
--port map (
--q1 => s_bitslice_fc_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
--q2 => s_bitslice_fc_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
--c => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--cb => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock c
--d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
--r => s_fc_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
--);


----i_iserdese3_fc : iserdese3
----generic map (
----SIM_DEVICE => "ULTRASCALE_PLUS",
----data_width => 4, -- parallel data width (4,8)
----fifo_enable => "true", -- enables the use of the fifo
----fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
----is_clk_b_inverted => '1', -- optional inversion for clk_b
----is_clk_inverted => '0', -- optional inversion for clk
----is_rst_inverted => '0' -- optional inversion for rst
----)
----port map (

----fifo_empty => open, -- 1-bit output: fifo empty flag
----internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
----q => s_bitslice_fc_sr(v_adc), -- 8-bit registered output
----clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
----clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
----clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
----d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
----fifo_rd_clk => '0', -- 1-bit input: fifo read clock
----fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
----rst => p_idelay_ctrl_reset_hg_in(v_adc) -- 1-bit input: asynchronous reset

----);
----s_iserdes_ctrl_reset_fc(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc) and p_idelay_ctrl_reset_lg_in(v_adc);


--        proc_shift_in : process(s_bitclk(v_adc)) -- Odd data bits clocked in on rising edge of adc clocks 
--        begin


--            if rising_edge(s_bitclk(v_adc)) then
--                --if p_adc_mode_in = '0' then
                
--                    s_adc_input_hg_temp(v_adc) <= s_adc_input_hg_temp(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 2) downto 0) & s_bitslice_hg_sr(v_adc)(0) & s_bitslice_hg_sr(v_adc)(1);
--                    s_adc_input_lg_temp(v_adc) <= s_adc_input_lg_temp(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 2) downto 0) & s_bitslice_lg_sr(v_adc)(0) & s_bitslice_lg_sr(v_adc)(1);
--                    s_adc_input_fc_temp(v_adc) <= s_adc_input_fc_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 2) downto 0) & s_bitslice_fc_sr(v_adc)(0) & s_bitslice_fc_sr(v_adc)(1);
--                --else 
            
--            end if;
--        end process;



--    proc_frame_capture : process(s_bitclk_div(v_adc))
--    constant c_phase_offset : integer:= 0;
--    begin
--        if rising_edge(s_bitclk_div(v_adc)) then
--            s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)(c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto (c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.hg_bitslip(v_adc)))));
--            s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto (c_phase_offset + s_fc_bitslips_from_sm(v_adc) + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(s_adc_readout.lg_bitslip(v_adc)))));
--            s_fc_output_word(v_adc) <= s_adc_input_fc_temp(v_adc)(c_phase_offset + s_fc_bitslips_from_sm(v_adc) + (c_adc_bit_number-1)  downto (c_phase_offset + s_fc_bitslips_from_sm(v_adc)));

        
--            --12 bits
--            --s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
--            --s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));    

--        end if;
        
--    end process;

--    proc_phase_reset : process(s_bitclk(v_adc)) -- Capture ADC words synchronous to ADC (bit) clock
--    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
--    type t_clk_edge_sync_sm is (st_wait_falling_edge, st_wait_rising_edge, st_rising_edge);
--    variable v_clk_edge_sync_sm : t_clk_edge_sync_sm := st_wait_falling_edge;
--    variable v_frameclk_sm, v_clk40db_sm : t_frameclk_sm := st_low;
--    type t_reset_sm is (st_initialize, st_release_fc_serdes, st_reset_divclk, st_reset_idelay, st_reset_iserdes, st_disable_vtc, 
--    st_load_value, st_wait_load, st_enable_vtc, st_calculate_fc_bitslips, st_check_fc_bitslip_stability, st_idle);
--    variable v_reset_sm : t_reset_sm := st_initialize;
--    variable v_counter : integer := 0;
--    constant c_phase_offset : integer:= 0;
--    variable  v_fc_tries: integer := 0;
--    constant max_fc_tries : integer := 1024;
--    variable v_idelay_fc : integer := 0;
   
        
--    begin
--        if rising_edge(s_bitclk(v_adc)) then
        
--            --p_debug_pin_out(v_adc)<= s_frameclk(v_adc);

----            case v_clk40db_sm is
----                when st_rising_edge =>
----                    if p_clknet_in.clk40 = '1' then -- Capture when frame clock has just gone high
----                        v_clk40db_sm := st_high;
----                    else
----                        v_clk40db_sm := st_falling_edge;
----                    end if;
----                when st_falling_edge =>
----                    s_adc_readout.lg_data(v_adc) <= s_lg_adc_output_word(v_adc);
----                    s_adc_readout.hg_data(v_adc) <= s_hg_adc_output_word(v_adc);
----                    if p_clknet_in.clk40 = '0' then -- Capture when frame clock has just gone high
----                        v_clk40db_sm := st_low;
----                    else
----                        v_clk40db_sm := st_rising_edge;
----                    end if;
----                when st_high =>
----                    if p_clknet_in.clk40 = '0' then
----                        v_clk40db_sm := st_falling_edge;
----                    end if;                    
----                when st_low =>
----                    if p_clknet_in.clk40 = '1' then
----                        v_clk40db_sm := st_rising_edge;
----                    end if;    
----                when others =>
                
----            end case;

--            case v_frameclk_sm is
--                when st_rising_edge =>
                              
--                    if p_master_reset_in = '0' then

--                        if p_clknet_in.db_clk40 = '1' then--s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
--                            v_frameclk_sm := st_high;
--                        else
--                            v_frameclk_sm := st_falling_edge;
--                        end if;
                        
--                        s_adc_readout.channel_missed_bit_count(v_adc) <=  std_logic_vector(to_signed(s_fc_bitslips_from_sm(v_adc), 31));
                        
--                        case v_reset_sm is
                    
--                            when st_initialize =>
--                                s_bufgce_div_ctrl_reset(v_adc) <=  '1';
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
--                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                                
--                                s_channel_frame_missalignemt(v_adc) <= '1';
                                
--                                v_reset_sm:=st_release_fc_serdes;
                                
--                            when st_release_fc_serdes =>
--                                s_bufgce_div_ctrl_reset(v_adc) <=  '1';
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';

--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                                
--                                s_channel_frame_missalignemt(v_adc) <= '1';
--                                --if (v_counter < 28) then
--                                --    v_counter:=v_counter+1;
--                                --else
--                                    v_reset_sm:=st_reset_divclk;
--                                --    v_counter:=0;
--                                --end if;
                            
--                            when st_reset_divclk => 
----                                case v_clk_edge_sync_sm is
----                                    when st_wait_falling_edge =>
----                                        if s_fc_output_word(v_adc)(1 downto 0) = "00" then
----                                            v_clk_edge_sync_sm:=st_wait_rising_edge;
----                                        end if;
----                                    when st_wait_rising_edge =>
----                                        if s_fc_output_word(v_adc)(1 downto 0) = "11" then
----                                            v_clk_edge_sync_sm:=st_rising_edge;
----                                        end if;                                   
----                                    when st_rising_edge =>
--                                        s_bufgce_div_ctrl_reset(v_adc) <=  '0';
--                                        s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                                        s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                        s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                                        s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                        s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                        s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';
        
--                                        s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
--                                        s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                                        
--                                        s_channel_frame_missalignemt(v_adc) <= '1';
                                        
--                                        v_reset_sm:=st_reset_idelay;   
----                                    when others=>
----                                end case;
                          
                                                        
--                            when st_reset_idelay => 
--                                s_bufgce_div_ctrl_reset(v_adc) <=  '0';
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
--                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
--                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

--                                s_channel_frame_missalignemt(v_adc) <= '1';  
                                
--                                v_reset_sm:=st_reset_iserdes;                        
                                                            
--                            when st_reset_iserdes =>
--                                s_bufgce_div_ctrl_reset(v_adc) <=  '0';
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
--                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
--                                s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
--                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';

--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

--                                s_channel_frame_missalignemt(v_adc) <= '1';
                                
--                                v_reset_sm:=st_disable_vtc;                        
                                
--                            when st_disable_vtc =>

--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
--                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
--                                s_channel_frame_missalignemt(v_adc) <= '1';
                                
--                                v_reset_sm:=st_load_value;
                                
--                            when st_load_value =>
                            
--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
--                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '1';
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '1';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';    

--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '1';
--                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
--                                s_channel_frame_missalignemt(v_adc) <= '1';                        
                            
--                                v_reset_sm:=st_wait_load;


--                            when st_wait_load =>

--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
--                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';  

--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                                 
--                                s_channel_frame_missalignemt(v_adc) <= '1';

--                                v_reset_sm:=st_enable_vtc;
                                
--                            when st_enable_vtc =>

--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
--                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                 
--                                s_channel_frame_missalignemt(v_adc) <= '1';

--                                v_reset_sm:=st_calculate_fc_bitslips;
--                            when st_calculate_fc_bitslips=>
                                
--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
--                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1'; 
                                
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
--                                v_fc_tries := 0;
                                
--                                if s_fc_output_word(v_adc) = "11111110000000" then
--                                    --v_counter :=0;
--                                    v_reset_sm:=st_check_fc_bitslip_stability;
                                    
--                                else
--                                    if s_fc_bitslips_from_sm(v_adc) < (((c_oversamples-1)*c_adc_bit_number)-c_phase_offset) then
--                                        s_fc_bitslips_from_sm(v_adc)<=s_fc_bitslips_from_sm(v_adc)+1;
--                                        --v_counter :=s_fc_bitslips_from_sm(v_adc)+1;--v_counter+1;
--                                    else
--                                        s_fc_bitslips_from_sm(v_adc) <= 0;
--                                        --v_counter :=0;
--                                    end if;
                                    
--                                end if;
--                                s_channel_frame_missalignemt(v_adc) <= '1';
                            
--                            when st_check_fc_bitslip_stability =>
                                
--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
--                                s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));
    
--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1'; 
                                
--                                s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                                s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                                s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                                
--                                if v_fc_tries < max_fc_tries then
                                    
--                                    if s_fc_output_word(v_adc) = "11111110000000" then
--                                        v_fc_tries:=v_fc_tries+1;
--                                        v_reset_sm:=st_check_fc_bitslip_stability;
--                                    else
--                                        v_fc_tries:=0;
--                                        if v_idelay_fc <512 then
--                                            v_idelay_fc:= v_idelay_fc+1;
                                            
--                                        else
--                                            v_idelay_fc:=0;
--                                        end if;
--                                        v_reset_sm:=st_reset_iserdes;
--                                    end if;
                                    
--                                else
--                                    v_fc_tries:=0;
--                                    v_reset_sm:=st_idle;
--                                end if;
                                
--                                s_channel_frame_missalignemt(v_adc) <= not s_channel_frame_missalignemt(v_adc);--'1';
                                
--                            when st_idle =>
                                
--                                s_bufgce_div_ctrl_reset(v_adc) <=  '0';
--                                --s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
--                                s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
--                                --s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
--                                s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
--                                --s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
--                                --s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';
--                                s_channel_frame_missalignemt(v_adc) <= '0';
    
--                                s_lg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
--                                s_hg_idelay_count_in_from_sm(v_adc) <= v_idelay_fc + to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));

--                                s_hg_idelay_ctrl_reset_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);
--                                s_hg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_load(v_adc);
--                                s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
                                
--                                s_lg_idelay_ctrl_reset_from_sm(v_adc) <= s_lg_idelay_ctrl_reset_from_sm(v_adc); 
--                                s_lg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_load(v_adc);
--                                s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
                                
                            
--                            when others=>
--                                v_reset_sm:=st_initialize;
--                        end case;
                        
                    
--                    else
                    
--                        v_reset_sm:=st_initialize;
--                        s_bufgce_div_ctrl_reset(v_adc) <=  '1';
--                        s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                        s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                        s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
--                        s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
--                        s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
--                        s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';
--                        s_channel_frame_missalignemt(v_adc) <= '1';
            
--                    end if;
                                            
                    
--                when st_falling_edge =>
                
--                    s_adc_readout.lg_data(v_adc) <= s_lg_adc_output_word(v_adc);
--                    s_adc_readout.hg_data(v_adc) <= s_hg_adc_output_word(v_adc);
                 
--                    if p_clknet_in.db_clk40  = '0' then--s_frameclk(v_adc) = '0' then -- Capture when frame clock has just gone high
--                        v_frameclk_sm := st_low;
--                    else
--                        v_frameclk_sm := st_rising_edge;
--                    end if; 
--                when st_low =>
--                    if p_clknet_in.db_clk40  = '1' then--s_frameclk(v_adc) = '1' then
--                        v_frameclk_sm := st_rising_edge;
--                    end if;
--                when st_high =>
--                    if p_clknet_in.db_clk40  = '0' then--s_frameclk(v_adc) = '0' then
--                        v_frameclk_sm := st_falling_edge;
--                    end if;                    
                
--            end case;
            
            
--        end if;
--    end process;


--end generate;


----proc_adc_pattern_config : process(p_clknet_in.clk40)
----variable v_counter : integer := 0;
----type t_sm_adc_pattern_config is (st_startup, st_config_pattern, st_wait_for_config, st_check_config, st_config_default, st_idle);
----variable v_sm_adc_pattern_config : t_sm_adc_pattern_config := st_startup;
 
----begin
----    if rising_edge(p_clknet_in.clk40) then
    
----        case v_sm_adc_pattern_config is
        
----            when st_startup=>
----                if p_adc_readout_control_in.adc_config_done = '1' then
----                    v_sm_adc_pattern_config :=st_config_pattern;
----                    s_adc_readout.mb_adc_config_control.mode <= '1';
----                    s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
----                    s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
----                    s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
----                    s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"B5";     
----                    s_adc_readout.mb_adc_config_control.adc_registers(3) <= "10" & c_adc_test_pattern(13 downto 8);
----                    s_adc_readout.mb_adc_config_control.adc_registers(4) <= c_adc_test_pattern(13 downto 8);
----                    --c_adc_test_pattern
----                end if;
----            when st_config_pattern =>
            
----                s_adc_readout.mb_adc_config_control.mode <= '1';
----                s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
----                s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
----                s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
----                s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"B5";     
----                s_adc_readout.mb_adc_config_control.adc_registers(3) <= "10" & c_adc_test_pattern(13 downto 8);
----                s_adc_readout.mb_adc_config_control.adc_registers(4) <= c_adc_test_pattern(13 downto 8);
----                s_adc_readout.mb_adc_config_control.trigger_mb_adc_config <= '1';
----                if p_adc_readout_control_in.adc_config_done = '0' then
----                    v_sm_adc_pattern_config:= st_wait_for_config;
----                end if;
               
                    
----            when st_wait_for_config=>
----                s_adc_readout.mb_adc_config_control.trigger_mb_adc_config <= '0';
----                if p_adc_readout_control_in.adc_config_done = '1' then
----                    v_sm_adc_pattern_config:= st_check_config;
----                end if;
----            when st_check_config =>
----                if v_counter < 40000000 then
----                    if s_channel_frame_missalignemt(0) = '1'
----                        or (s_channel_frame_missalignemt(1)  = '1')
----                        or (s_channel_frame_missalignemt(2)  = '1')
----                        or (s_channel_frame_missalignemt(3)  = '1')
----                        or (s_channel_frame_missalignemt(4)  = '1')
----                        or (s_channel_frame_missalignemt(5) = '1')  then
                        
----                    else
----                        v_sm_adc_pattern_config :=st_startup;
----                    end if;
----                end if;
            
            
----            when others => 
----                v_sm_adc_pattern_config :=st_idle;
----        end case;
    
----    end if;


----end process;


----proc_bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
----begin
----    if falling_edge(p_clknet_in.clk40) then
----        s_adc_readout.lg_data <= s_lg_adc_output_word;
----        s_adc_readout.hg_data <= s_hg_adc_output_word;--s_fc_output_word;

----    end if;
----end process;

    

--        s_adc_readout.readout_initialized<= s_channel_frame_missalignemt(0)
--                                            and s_channel_frame_missalignemt(1)
--                                            and s_channel_frame_missalignemt(2)
--                                            and s_channel_frame_missalignemt(3)
--                                            and s_channel_frame_missalignemt(4)
--                                            and s_channel_frame_missalignemt(5);
--        s_adc_readout.mb_adc_config_control.mode <= '0';
--        s_adc_readout.mb_adc_config_control.mb_fpga_select <= "100";
--        s_adc_readout.mb_adc_config_control.mb_pmt_select <= "11";
--        s_adc_readout.mb_adc_config_control.adc_registers(1) <= x"00";
--        s_adc_readout.mb_adc_config_control.adc_registers(2) <= x"B5";     
--        s_adc_readout.mb_adc_config_control.adc_registers(3) <= x"00";
--        s_adc_readout.mb_adc_config_control.adc_registers(4) <= x"00";


--end Behavioral;


