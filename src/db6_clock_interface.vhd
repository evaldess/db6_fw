----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Sam Silverstein 
--         : Eduardo Valdes
-- Create Date: 08/28/2018 02:56:16 PM
-- Design Name: 
-- Module Name: db6_clock_interface - Behavioral
-- Project Name: tilecal daughterboard rev 5 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:  db6_clock_interface. 
--                       
-- 
----------------------------------------------------------------------------------

-- common libraries --
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
-- XILINX libraries --
library UNISIM;
use UNISIM.VCOMPONENTS.all;
-- user defined libraries --

library tilecal;
use tilecal.db6_design_package.all;


entity db6_clock_interface is
    generic (
        g_priority_side : std_logic_vector(1 downto 0) := "01";                 --! side to priotitize
        g_num_gth_links                 : integer := 1                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
        );
    port(
    --clocks
    p_clkin_in                       : in t_db_clkin;
    p_clknet_out                        : out t_db_clknet;

    --control signals
    p_master_reset_in   : in std_logic;
    p_db_reg_rx_in  : in t_db_reg_rx
    --p_clk_sel_in   : in std_logic
    );

end db6_clock_interface;



architecture RTL of db6_clock_interface is

attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;

--components

component mmcm_osc_clk
port
 (-- Clock in ports
  clk_in1           : in     std_logic;
  -- Clock out ports
  p_clk_40_out          : out    std_logic;
  p_clk_80_out          : out    std_logic;
  p_clk_160_out          : out    std_logic;
  p_clk_280_out          : out    std_logic;
  p_clk_40_90_out          : out    std_logic;
  p_clk_40_180_out          : out    std_logic;
  p_clk_40_270_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Dynamic phase shift ports
  p_psclk_in             : in     std_logic;
  p_psen_in              : in     std_logic;
  p_psincdec_in          : in     std_logic;
  p_psdone_out            : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_input_clk_stopped_out : out    std_logic;
  p_clkfb_stopped_out : out    std_logic;
  p_locked_out            : out    std_logic;
  p_cddcdone_out          : out    std_logic; 
  p_cddcreq_in           : in     std_logic
  
 );
end component;

component pll_osc_clk
port
 (-- Clock in ports
  -- Clock out ports
  p_clk40_out          : out    std_logic;
  p_clk80_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_locked_out            : out    std_logic;
  p_clk_in           : in     std_logic
 );
end component;

component mmcm_gbt40_db
port
 (-- Clock in ports
  p_clk_in         : in     std_logic;
  --clk_in2_p         : in     std_logic;
  --clk_in2_n         : in     std_logic;
  --p_clk_sel_in           : in     std_logic;
  -- Clock out ports
  p_clk40_out          : out    std_logic;
  p_clk80_out          : out    std_logic;
  p_clk160_out          : out    std_logic;
  p_clk280_out          : out    std_logic;
  p_clk40_90_out          : out    std_logic;
  p_clk40_180_out          : out    std_logic;
  p_clk40_270_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Dynamic phase shift ports
  p_psclk_in             : in     std_logic;
  p_psen_in              : in     std_logic;
  p_psincdec_in          : in     std_logic;
  p_psdone_out            : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_input_clk_stopped_out : out    std_logic;
  p_clkfb_stopped_out : out    std_logic;
  p_locked_out            : out    std_logic;
  p_cddcdone_out          : out    std_logic; 
  p_cddcreq_in           : in     std_logic;
  clk_in1_p         : in     std_logic;
  clk_in1_n         : in     std_logic
 );
end component;

component mmcm_gbt40_cfgbus
port
 (-- Clock in ports
  p_clk_in         : in     std_logic;
  --clk_in2           : in     std_logic;
  --p_clk_sel_in           : in     std_logic;
  -- Clock out ports
  p_clk40_out          : out    std_logic;
  p_clk40_90_out          : out    std_logic;
  p_clk40_180_out          : out    std_logic;
  p_clk40_270_out          : out    std_logic;
  p_clk80_out          : out    std_logic;
  p_clk160_out          : out    std_logic;
  p_clk280_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Dynamic phase shift ports
  p_psclk_in             : in     std_logic;
  p_psen_in              : in     std_logic;
  p_psincdec_in          : in     std_logic;
  p_psdone_out            : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_input_clk_stopped_out : out    std_logic;
  p_clkfb_stopped_out : out    std_logic;
  p_locked_out            : out    std_logic;
  p_cddcdone_out          : out    std_logic; 
  p_cddcreq_in           : in     std_logic
  --clk_in1           : in     std_logic
 );
end component;


component mmcm_gbt40_mb
port
 (-- Clock in ports
  p_clk_in         : in     std_logic;
  --clk_in2_p         : in     std_logic;
  --clk_in2_n         : in     std_logic;
  --p_clk_sel_in           : in     std_logic;
  -- Clock out ports
  p_clk40_out          : out    std_logic;
  p_clk40_90_out          : out    std_logic;
  p_clk40_180_out          : out    std_logic;
  p_clk40_270_out          : out    std_logic;
  p_clk80_out          : out    std_logic;
  p_clk280_out          : out    std_logic;
  p_clk560_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Dynamic phase shift ports
  p_psclk_in             : in     std_logic;
  p_psen_in              : in     std_logic;
  p_psincdec_in          : in     std_logic;
  p_psdone_out            : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_input_clk_stopped_out : out    std_logic;
  p_clkfb_stopped_out : out    std_logic;
  p_locked_out            : out    std_logic;
  p_cddcdone_out          : out    std_logic; 
  p_cddcreq_in           : in     std_logic
  --clk_in1_p         : in     std_logic;
  --clk_in1_n         : in     std_logic
 );
end component;


--signals
signal s_clknet_out : t_db_clknet;
attribute keep of s_clknet_out : signal is "TRUE";
attribute dont_touch of s_clknet_out : signal is "TRUE";

signal s_db_clkin_local : std_logic_vector(0 downto 0);

signal s_pll_osc, s_mmcm_osc, s_mmcm_gbt40_db, s_mmcm_gbt40_cfgbus, s_mmcm_gbt40_mb_q0, s_mmcm_gbt40_mb_q1 : t_mmcm_control;

signal s_gbt40_q0_delay_control, s_gbt40_q1_delay_control: t_delay_control;
signal s_gbt40_q0_clk_shape,s_gbt40_q1_clk_shape : std_logic_vector(1 downto 0) := ('1','0');

signal s_osc_clk100, s_clk40, s_tp_clk40_q0_local, s_tp_clk40_q1_local, s_tp_clk40_q0_remote, s_tp_clk40_q1_remote : std_logic;

type t_bufg_gt_sync is record
    cesync : std_logic; -- 1-bit output: synchronized ce
    clrsync : std_logic; -- 1-bit output: synchronized clr
    ce : std_logic; -- 1-bit input: asynchronous enable
    clk : std_logic; -- 1-bit input: clock
    clr : std_logic; -- 1-bit input: asynchronous clea
end record;
type t_bufg_gt_sync_gth_outclkfabric is array (g_num_gth_links-1 downto 0) of t_bufg_gt_sync;
signal s_bufg_gt_sync_gth_txoutclkfabric, s_bufg_gt_sync_gth_rxoutclkfabric : t_bufg_gt_sync_gth_outclkfabric;

type t_bufg_gt is record
    o : std_logic; -- 1-bit output: buffer
    ce : std_logic; -- 1-bit input: buffer enable
    cemask  : std_logic; -- 1-bit input: ce mask
    clr  : std_logic; -- 1-bit input: asynchronous clear
    clrmask : std_logic; -- 1-bit input: clr mask
    div : std_logic; -- 3-bit input: dymanic divide value
    i  : std_logic; -- 1-bit input: buffer
end record;
type t_bufg_gt_gth_outclkfabric is array (g_num_gth_links-1 downto 0) of t_bufg_gt;
signal s_bufg_gt_gth_txoutclkfabric, s_bufg_gt_gth_rxoutclkfabric : t_bufg_gt_gth_outclkfabric;

--signal s_cesync, s_clrsync : std_logic_vector(c_gbt_bank_number_of_links-1 downto 0);

--attribute keep of p_clk_sel_in, s_gbt40_q0_delay_control, s_gbt40_q1_delay_control, s_osc_clk100, s_clk40, s_tp_clk40_q0_local, s_tp_clk40_q1_local, s_tp_clk40_q0_remote, s_tp_clk40_q1_remote : signal is "TRUE";
--attribute dont_touch of p_clk_sel_in, s_gbt40_q0_delay_control, s_gbt40_q1_delay_control, s_osc_clk100, s_clk40, s_tp_clk40_q0_local, s_tp_clk40_q1_local, s_tp_clk40_q0_remote, s_tp_clk40_q1_remote : signal is "TRUE";

--attribute dont_touch of p_clk_sel_in : signal is "TRUE";
--attribute keep of p_clk_sel_in : signal is "TRUE";

begin  -- RTL


-- general signal connections
p_clknet_out<=s_clknet_out;

s_clknet_out.bcr<=p_clkin_in.bcr;
s_clknet_out.clksel <= p_clkin_in.clksel;
s_clknet_out.qpllclksel <= p_clkin_in.qpllclksel;
s_clknet_out.cpllclksel <= p_clkin_in.cpllclksel;
s_clknet_out.txsysclksel <= p_clkin_in.txsysclksel;
s_clknet_out.rxsysclksel <= p_clkin_in.rxsysclksel;
s_clknet_out.rxoutclksel <= p_clkin_in.rxoutclksel;
s_clknet_out.txoutclksel <= p_clkin_in.txoutclksel;

g_refclksc : for c in 0 to (1) generate
    i_gth_refclk_local: ibufds_gte3
     generic map
     (
         refclk_en_tx_path=>'0',
         refclk_hrow_ck_sel=>"01",
         refclk_icntl_rx=>"00"
     )
     port map
     (
        ceb => '0',
        i  => p_clkin_in.gth_refclk_gbtx_local(c).p,
        ib => p_clkin_in.gth_refclk_gbtx_local(c).n,
        o => s_clknet_out.gth_refclk_local(c),
        odiv2 => s_clknet_out.gth_refclkdiv2_local(c)
     );
    
    i_gth_refclk_remote: ibufds_gte3
     generic map
     (
         refclk_en_tx_path=>'0',
         refclk_hrow_ck_sel=>"01",
         refclk_icntl_rx=>"00"
     )
     port map
     (
        ceb => '0',
        i  => p_clkin_in.gth_refclk_gbtx_remote(c).p,
        ib => p_clkin_in.gth_refclk_gbtx_remote(c).n,
        o => s_clknet_out.gth_refclk_remote(c),
        odiv2 => s_clknet_out.gth_refclkdiv2_remote(c)
     );
end generate;

    s_clknet_out.gth_txwordclk_out<=p_clkin_in.gth_txwordclk_out;
    s_clknet_out.gth_rxwordclk_out<=p_clkin_in.gth_rxwordclk_out;



g_outclkfabric : for g in 0 to (g_num_gth_links-1) generate
--i_bufg_gth_txoutclkfabric : bufg
--port map (
--    o => s_clknet_out.gth_txoutclkfabric_out(g), -- 1-bit output: Buffer
--    i => p_clkin_in.gth_txoutclkfabric_out(g) -- 1-bit input: Buffer 
--);
--i_bufg_gth_rxoutclkfabric : bufg
--port map (
--    o => s_clknet_out.gth_rxoutclkfabric_out(g), -- 1-bit output: Buffer
--    i => p_clkin_in.gth_rxoutclkfabric_out(g) -- 1-bit input: Buffer 
--);
s_bufg_gt_sync_gth_txoutclkfabric(g).clk<= p_clkin_in.gth_txoutclkfabric_out(g);
s_bufg_gt_sync_gth_rxoutclkfabric(g).clk<= p_clkin_in.gth_rxoutclkfabric_out(g);

s_bufg_gt_sync_gth_txoutclkfabric(g).clr <= '0';
s_bufg_gt_sync_gth_txoutclkfabric(g).ce <= '1';
s_bufg_gt_sync_gth_rxoutclkfabric(g).clr <= '0';
s_bufg_gt_sync_gth_rxoutclkfabric(g).ce <= '1';

i_bufg_gt_sync_gth_txoutclkfabric : bufg_gt_sync
port map (
    cesync => s_bufg_gt_sync_gth_txoutclkfabric(g).cesync, -- 1-bit output: synchronized ce
    clrsync => s_bufg_gt_sync_gth_txoutclkfabric(g).clrsync, -- 1-bit output: synchronized clr
    ce => s_bufg_gt_sync_gth_txoutclkfabric(g).ce, -- 1-bit input: asynchronous enable
    clk => s_bufg_gt_sync_gth_txoutclkfabric(g).clk, -- 1-bit input: clock
    clr => s_bufg_gt_sync_gth_txoutclkfabric(g).clr -- 1-bit input: asynchronous clear
);

i_bufg_gt_sync_gth_rxoutclkfabric : bufg_gt_sync
port map (
    cesync => s_bufg_gt_sync_gth_rxoutclkfabric(g).cesync, -- 1-bit output: synchronized ce
    clrsync => s_bufg_gt_sync_gth_rxoutclkfabric(g).clrsync, -- 1-bit output: synchronized clr
    ce => s_bufg_gt_sync_gth_rxoutclkfabric(g).ce, -- 1-bit input: asynchronous enable
    clk => s_bufg_gt_sync_gth_rxoutclkfabric(g).clk, -- 1-bit input: clock
    clr => s_bufg_gt_sync_gth_rxoutclkfabric(g).clr -- 1-bit input: asynchronous clear
);


i_bufg_gt_gth_txoutclkfabric : bufg_gt
port map (
    o => s_clknet_out.gth_txoutclkfabric_out(g), -- 1-bit output: buffer
    ce => s_bufg_gt_sync_gth_txoutclkfabric(g).cesync, -- 1-bit input: buffer enable
    cemask => '1', -- 1-bit input: ce mask
    clr => s_bufg_gt_sync_gth_txoutclkfabric(g).clrsync, -- 1-bit input: asynchronous clear
    clrmask => '0', -- 1-bit input: clr mask
    div => "000", -- 3-bit input: dymanic divide value
    i => p_clkin_in.gth_txoutclkfabric_out(g) -- 1-bit input: buffer
);

i_bufg_gt_gth_rxoutclkfabric : bufg_gt
port map (
    o => s_clknet_out.gth_rxoutclkfabric_out(g), -- 1-bit output: buffer
    ce => s_bufg_gt_sync_gth_rxoutclkfabric(g).cesync, -- 1-bit input: buffer enable
    cemask => '1', -- 1-bit input: ce mask
    clr => s_bufg_gt_sync_gth_rxoutclkfabric(g).clrsync, -- 1-bit input: asynchronous clear
    clrmask => '0', -- 1-bit input: clr mask
    div => "000", -- 3-bit input: dymanic divide value
    i => p_clkin_in.gth_rxoutclkfabric_out(g) -- 1-bit input: buffer
);

end generate;

s_clknet_out.db_side <= p_clkin_in.db_side;
s_clknet_out.gbtx_rxready <= p_clkin_in.gbtx_rxready;
s_clknet_out.gbtx_datavalid <= p_clkin_in.gbtx_datavalid;

s_clknet_out.osc_clk100 <= s_osc_clk100;

------------------------------------
-- MMCM IP component declarations --
------------------------------------
--https://www.xilinx.com/support/documentation/ip_documentation/clk_wiz/v6_0/pg065-clk-wiz.pdf
--https://www.xilinx.com/support/documentation/sw_manuals/xilinx2014_1/ug974-vivado-ultrascale-libraries.pdf

 i_ibufds_osc_clk : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_osc_clk100,  -- clock buffer output
    i => p_clkin_in.osc_clkin.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.osc_clkin.n -- diff_n clock buffer input (connect directly to top-level port)
 );

--  i_mmcm_osc_clk : mmcm_osc_clk
--   port map ( 
--  -- Clock in ports
--   clk_in1 => s_osc_clk100,
--  -- Clock out ports  
--   p_clk_40_out => s_clknet_out.osc_clk40,
--   p_clk_80_out => s_clknet_out.osc_clk80,
--   p_clk_160_out => s_clknet_out.osc_clk160,
--   p_clk_280_out => s_clknet_out.osc_clk280,
--   p_clk_40_90_out => s_clknet_out.osc_clk40_phase_90,
--   p_clk_40_180_out => s_clknet_out.osc_clk40_phase_180,
--   p_clk_40_270_out => s_clknet_out.osc_clk40_phase_270,
--  -- Dynamic reconfiguration ports             
--   p_daddr_in => s_mmcm_osc.daddr_in,
--   p_dclk_in => s_osc_clk100,
--   p_den_in => s_mmcm_osc.den_in,
--   p_din_in => s_mmcm_osc.din_in,
--   p_dout_out => s_mmcm_osc.dout_out,
--   p_drdy_out => s_mmcm_osc.drdy_out,
--   p_dwe_in => s_mmcm_osc.dwe_in,
--   --Dynamic phase shift ports
--   p_psclk_in => s_mmcm_osc.psclk_in,
--   p_psen_in => s_mmcm_osc.psen_in,
--   p_psincdec_in => s_mmcm_osc.psincdec_in,
--   p_psdone_out => s_mmcm_osc.psdone_out,
--  -- Status and control signals                
--   p_reset_in => s_mmcm_osc.reset_in,
--   p_locked_out => s_mmcm_osc.locked_out,
--   p_input_clk_stopped_out=> s_mmcm_osc.input_clk_stopped_out,
--   p_clkfb_stopped_out=> s_mmcm_osc.clkfb_stopped_out,    
--   p_cddcdone_out=> s_mmcm_osc.cddcdone_out,
--   p_cddcreq_in => s_mmcm_osc.cddcreq_in

-- );

--s_mmcm_osc.daddr_in <= (others =>'0');
--s_mmcm_osc.den_in <= '0';
--s_mmcm_osc.din_in <= (others =>'0');
--s_mmcm_osc.dwe_in <= '0';
--s_mmcm_osc.psclk_in <= '0';
--s_mmcm_osc.psen_in <= '0';
--s_mmcm_osc.psincdec_in <= '0';
--s_mmcm_osc.reset_in <= '0';
--s_mmcm_osc.cddcreq_in <= '0';
--s_clknet_out.locked_osc <= s_mmcm_osc.locked_out;


i_pll_osc_clk : pll_osc_clk
   port map ( 
  -- Clock out ports  
   p_clk40_out => s_clknet_out.osc_clk40,
   p_clk80_out => s_clknet_out.osc_clk80,
  -- Dynamic reconfiguration ports             
   p_daddr_in => s_pll_osc.daddr_in,
   p_dclk_in => s_osc_clk100,
   p_den_in => s_pll_osc.den_in,
   p_din_in => s_pll_osc.din_in,
   p_dout_out => s_pll_osc.dout_out,
   p_drdy_out => s_pll_osc.drdy_out,
   p_dwe_in => s_pll_osc.dwe_in,
  -- Status and control signals                
   p_reset_in => s_pll_osc.reset_in,
   p_locked_out => s_pll_osc.locked_out,
   -- Clock in ports
   p_clk_in => s_osc_clk100
 );


s_pll_osc.daddr_in <= (others =>'0');
s_pll_osc.den_in <= '0';
s_pll_osc.din_in <= (others =>'0');
s_pll_osc.dwe_in <= '0';
s_pll_osc.psclk_in <= '0';
s_pll_osc.psen_in <= '0';
s_pll_osc.psincdec_in <= '0';
s_pll_osc.reset_in <= '0';
s_pll_osc.cddcreq_in <= '0';
s_clknet_out.locked_osc <= s_mmcm_osc.locked_out;

-- i_mmcm_gbt40_db : mmcm_gbt40_db
--    port map ( 
--    -- Clock in ports
--    clk_in2_p => p_clkinputs_in.db_clkin_remote.p,
--    clk_in2_n => p_clkinputs_in.db_clkin_remote.n,
--    clk_in1_p => p_clkinputs_in.db_clkin_local.p,
--    clk_in1_n => p_clkinputs_in.db_clkin_local.n,
--    p_clk_sel_in => p_clk_sel_in,
--   -- Clock out ports
--    p_clk40_out => s_clknet_out.db_clk40,
--    p_clk80_out => s_clknet_out.db_clk80,
--    p_clk160_out => s_clknet_out.db_clk160,
--    p_clk280_out => s_clknet_out.db_clk280,
--    p_clk40_90_out => s_clknet_out.db_clk40_phase_90,
--    p_clk40_180_out => s_clknet_out.db_clk40_phase_180,
--    p_clk40_270_out => s_clknet_out.db_clk40_phase_270,
--    -- Dynamic reconfiguration ports
--    p_daddr_in => s_mmcm_gbt40_db.daddr_in,
--    p_dclk_in => s_clknet_out.osc_clk40,
--    p_den_in => s_mmcm_gbt40_db.den_in,
--    p_din_in => s_mmcm_gbt40_db.din_in,
--    p_dout_out => s_mmcm_gbt40_db.dout_out,
--    p_drdy_out => s_mmcm_gbt40_db.drdy_out,
--    p_dwe_in => s_mmcm_gbt40_db.dwe_in,
--    --Dynamic phase shift ports
--    p_psclk_in => s_mmcm_gbt40_db.psclk_in,
--    p_psen_in => s_mmcm_gbt40_db.psen_in,
--    p_psincdec_in => s_mmcm_gbt40_db.psincdec_in,
--    p_psdone_out => s_mmcm_gbt40_db.psdone_out,
--   -- Status and control signals                
--    p_reset_in => s_mmcm_gbt40_db.reset_in,
--    p_locked_out => s_mmcm_gbt40_db.locked_out,
--    p_input_clk_stopped_out=> s_mmcm_gbt40_db.input_clk_stopped_out,
--    p_clkfb_stopped_out=> s_mmcm_gbt40_db.clkfb_stopped_out,
--    p_cddcdone_out=> s_mmcm_gbt40_db.cddcdone_out,
--    p_cddcreq_in => s_mmcm_gbt40_db.cddcreq_in

--  );
--s_mmcm_gbt40_db.daddr_in <= (others =>'0');
--s_mmcm_gbt40_db.den_in <= '0';
--s_mmcm_gbt40_db.din_in <= (others =>'0');
--s_mmcm_gbt40_db.dwe_in <= '0';
--s_mmcm_gbt40_db.psclk_in <= '0';
--s_mmcm_gbt40_db.psen_in <= '0';
--s_mmcm_gbt40_db.psincdec_in <= '0';
--s_mmcm_gbt40_db.reset_in <= '0';
--s_mmcm_gbt40_db.cddcreq_in <= '0';
--s_clknet_out.locked_db <= s_mmcm_gbt40_db.locked_out;



--s_clknet_out.db_clk40<=s_clknet_out.cfgbus_clk40;
--s_clknet_out.db_clk40_phase_90<=s_clknet_out.cfgbus_clk40_phase_90;
--s_clknet_out.db_clk40_phase_180<=s_clknet_out.cfgbus_clk40_phase_180;
--s_clknet_out.db_clk40_phase_270<=s_clknet_out.cfgbus_clk40_phase_270;
--s_clknet_out.db_clk80<=s_clknet_out.cfgbus_clk80;
--s_clknet_out.db_clk160<=s_clknet_out.cfgbus_clk160;
--s_clknet_out.db_clk280<=s_clknet_out.cfgbus_clk280;

 i_ibufds_cfgbus_local : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_clknet_out.cfgbus_clk40_local,  -- clock buffer output
    i => p_clkin_in.cfgbus_clkin_local.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.cfgbus_clkin_local.n -- diff_n clock buffer input (connect directly to top-level port)
 );

 i_ibufds_cfgbus_remote : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_clknet_out.cfgbus_clk40_remote,  -- clock buffer output
    i => p_clkin_in.cfgbus_clkin_remote.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.cfgbus_clkin_remote.n -- diff_n clock buffer input (connect directly to top-level port)
 );

i_BUFGMUX_CTRL_cfgbus : BUFGMUX_CTRL
port map (
    O =>s_clknet_out.cfgbus_clk40,--s_clk40, -- 1-bit output: Clock output
    I0 => s_clknet_out.cfgbus_clk40_local, -- 1-bit input: Clock input (S=0)
    I1 => s_clknet_out.cfgbus_clk40_remote, -- 1-bit input: Clock input (S=1)
    S => p_clkin_in.clksel -- 1-bit input: Clock select
);

i_BUFGMUX_CTRL_db_clk40 : BUFGMUX_CTRL
port map (
    O =>s_clknet_out.clk40,--s_clk40, -- 1-bit output: Clock output
    I0 => s_clknet_out.osc_clk40, -- 1-bit input: Clock input (S=0)
    I1 => s_clknet_out.cfgbus_clk40, -- 1-bit input: Clock input (S=1)
    S => s_mmcm_gbt40_cfgbus.locked_out -- 1-bit input: Clock select
);

i_BUFGMUX_CTRL_db_clk80 : BUFGMUX_CTRL
port map (
    O => s_clknet_out.clk80,--s_clk40, -- 1-bit output: Clock output
    I0 => s_clknet_out.osc_clk80, -- 1-bit input: Clock input (S=0)
    I1 => s_clknet_out.cfgbus_clk80, -- 1-bit input: Clock input (S=1)
    S => s_mmcm_gbt40_cfgbus.locked_out -- 1-bit input: Clock select
);


 i_mmcm_gbt40_cfgbus : mmcm_gbt40_cfgbus
    port map ( 
    -- Clock in ports
    p_clk_in => s_clknet_out.cfgbus_clk40,
--    clk_in2_p => p_clkinputs_in.cfgbus_clkin_remote.p,
--    clk_in2_n => p_clkinputs_in.cfgbus_clkin_remote.n,
--    clk_in1_p => p_clkinputs_in.cfgbus_clkin_local.p,
--    clk_in1_n => p_clkinputs_in.cfgbus_clkin_local.n,
--    clk_in2 => s_clknet_out.cfgbus_clk40_remote,
--    clk_in1 => s_clknet_out.cfgbus_clk40_local,
    --p_clk_sel_in => p_clk_sel_in,
   -- Clock out ports  
    p_clk40_out => s_clknet_out.db_clk40,
    p_clk40_90_out => s_clknet_out.cfgbus_clk40_phase_90,
    p_clk40_180_out => s_clknet_out.cfgbus_clk40_phase_180,
    p_clk40_270_out => s_clknet_out.cfgbus_clk40_phase_270,
    p_clk80_out => s_clknet_out.cfgbus_clk80,
    p_clk160_out => s_clknet_out.cfgbus_clk160,
    p_clk280_out => s_clknet_out.cfgbus_clk280,
    -- Dynamic reconfiguration ports
    p_daddr_in => s_mmcm_gbt40_cfgbus.daddr_in,
    p_dclk_in => s_clknet_out.osc_clk40,
    p_den_in => s_mmcm_gbt40_cfgbus.den_in,
    p_din_in => s_mmcm_gbt40_cfgbus.din_in,
    p_dout_out => s_mmcm_gbt40_cfgbus.dout_out,
    p_drdy_out => s_mmcm_gbt40_cfgbus.drdy_out,
    p_dwe_in => s_mmcm_gbt40_cfgbus.dwe_in,
    --Dynamic phase shift ports
    p_psclk_in => s_mmcm_gbt40_cfgbus.psclk_in,
    p_psen_in => s_mmcm_gbt40_cfgbus.psen_in,
    p_psincdec_in => s_mmcm_gbt40_cfgbus.psincdec_in,
    p_psdone_out => s_mmcm_gbt40_cfgbus.psdone_out,
   -- Status and control signals                
    p_reset_in => s_mmcm_gbt40_cfgbus.reset_in,
    p_locked_out => s_mmcm_gbt40_cfgbus.locked_out,
    p_input_clk_stopped_out=> s_mmcm_gbt40_cfgbus.input_clk_stopped_out,
    p_clkfb_stopped_out=> s_mmcm_gbt40_cfgbus.clkfb_stopped_out,
    p_cddcdone_out=> s_mmcm_gbt40_cfgbus.cddcdone_out,
    p_cddcreq_in => s_mmcm_gbt40_cfgbus.cddcreq_in

  );
  
s_mmcm_gbt40_cfgbus.daddr_in <= (others =>'0');
s_mmcm_gbt40_cfgbus.den_in <= '0';
s_mmcm_gbt40_cfgbus.din_in <= (others =>'0');
s_mmcm_gbt40_cfgbus.dwe_in <= '0';
s_mmcm_gbt40_cfgbus.psclk_in <= '0';
s_mmcm_gbt40_cfgbus.psen_in <= '0';
s_mmcm_gbt40_cfgbus.psincdec_in <= '0';
s_mmcm_gbt40_cfgbus.reset_in <= '0';
s_mmcm_gbt40_cfgbus.cddcreq_in <= '0';
s_clknet_out.locked_cfgbus <= s_mmcm_gbt40_cfgbus.locked_out;



 i_ibufds_mmcm_gbt40_mb_q0_local : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o =>  s_clknet_out.mb_clk40_q0_local,  -- clock buffer output
    i => p_clkin_in.mb_q0_clkin_local.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.mb_q0_clkin_local.n -- diff_n clock buffer input (connect directly to top-level port)
 );

 i_ibufds_mmcm_gbt40_mb_q0_remote : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_clknet_out.mb_clk40_q0_remote,  -- clock buffer output
    i => p_clkin_in.mb_q0_clkin_remote.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.mb_q0_clkin_remote.n -- diff_n clock buffer input (connect directly to top-level port)
 );

i_BUFGMUX_CTRL_mmcm_gbt40_mb_q0 : BUFGMUX_CTRL
port map (
    O =>s_clknet_out.mb_clk40_q0,--s_clk40, -- 1-bit output: Clock output
    I0 => s_clknet_out.mb_clk40_q0_local, -- 1-bit input: Clock input (S=0)
    I1 => s_clknet_out.mb_clk40_q0_remote, -- 1-bit input: Clock input (S=1)
    S => p_clkin_in.clksel -- 1-bit input: Clock select
);


 i_mmcm_gbt40_mb_q0 : mmcm_gbt40_mb
    port map ( 
    -- Clock in ports
    p_clk_in => s_clknet_out.mb_clk40_q0,
--    clk_in2_p => p_clkinputs_in.mb_q0_clkin_remote.p,
--    clk_in2_n => p_clkinputs_in.mb_q0_clkin_remote.n,
--    clk_in1_p => p_clkinputs_in.mb_q0_clkin_local.p,
--    clk_in1_n => p_clkinputs_in.mb_q0_clkin_local.n,
--    p_clk_sel_in => p_clk_sel_in,
   -- Clock out ports  
    p_clk40_out => s_clknet_out.mb_clk40.q0,
    p_clk40_90_out => s_clknet_out.mb_clk40_phase_90.q0,
    p_clk40_180_out => s_clknet_out.mb_clk40_phase_180.q0,
    p_clk40_270_out => s_clknet_out.mb_clk40_phase_270.q0,
    p_clk80_out => s_clknet_out.mb_clk80.q0,
    p_clk280_out => s_clknet_out.mb_clk280.q0,
    p_clk560_out => s_clknet_out.mb_clk560.q0,
        -- Dynamic reconfiguration ports
    p_daddr_in => s_mmcm_gbt40_mb_q0.daddr_in,
    p_dclk_in => s_clknet_out.osc_clk40,
    p_den_in => s_mmcm_gbt40_mb_q0.den_in,
    p_din_in => s_mmcm_gbt40_mb_q0.din_in,
    p_dout_out => s_mmcm_gbt40_mb_q0.dout_out,
    p_drdy_out => s_mmcm_gbt40_mb_q0.drdy_out,
    p_dwe_in => s_mmcm_gbt40_mb_q0.dwe_in,
    --Dynamic phase shift ports
    p_psclk_in => s_mmcm_gbt40_mb_q0.psclk_in,
    p_psen_in => s_mmcm_gbt40_mb_q0.psen_in,
    p_psincdec_in => s_mmcm_gbt40_mb_q0.psincdec_in,
    p_psdone_out => s_mmcm_gbt40_mb_q0.psdone_out,
   -- Status and control signals                
    p_reset_in => s_mmcm_gbt40_mb_q0.reset_in,
    p_locked_out => s_mmcm_gbt40_mb_q0.locked_out,
    p_input_clk_stopped_out=> s_mmcm_gbt40_mb_q0.input_clk_stopped_out,
    p_clkfb_stopped_out=> s_mmcm_gbt40_mb_q0.clkfb_stopped_out,
    p_cddcdone_out=> s_mmcm_gbt40_mb_q0.cddcdone_out,
    p_cddcreq_in => s_mmcm_gbt40_mb_q0.cddcreq_in

  );
s_mmcm_gbt40_mb_q0.daddr_in <= (others =>'0');
s_mmcm_gbt40_mb_q0.den_in <= '0';
s_mmcm_gbt40_mb_q0.din_in <= (others =>'0');
s_mmcm_gbt40_mb_q0.dwe_in <= '0';
s_mmcm_gbt40_mb_q0.psclk_in <= '0';
s_mmcm_gbt40_mb_q0.psen_in <= '0';
s_mmcm_gbt40_mb_q0.psincdec_in <= '0';
s_mmcm_gbt40_mb_q0.reset_in <= '0';
s_mmcm_gbt40_mb_q0.cddcreq_in <= '0';
s_clknet_out.locked_mb_q0 <= s_mmcm_gbt40_mb_q0.locked_out;

 i_ibufds_mmcm_gbt40_mb_q1_local : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_clknet_out.mb_clk40_q1_local,  -- clock buffer output
    i => p_clkin_in.mb_q1_clkin_local.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.mb_q1_clkin_local.n -- diff_n clock buffer input (connect directly to top-level port)
 );

 i_ibufds_mmcm_gbt40_mb_q1_remote : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_clknet_out.mb_clk40_q1_remote,  -- clock buffer output
    i => p_clkin_in.mb_q1_clkin_remote.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.mb_q1_clkin_remote.n -- diff_n clock buffer input (connect directly to top-level port)
 );

i_BUFGMUX_CTRL_mmcm_gbt40_mb_q1 : BUFGMUX_CTRL
port map (
    O =>s_clknet_out.mb_clk40_q1,--s_clk40, -- 1-bit output: Clock output
    I0 => s_clknet_out.mb_clk40_q1_local, -- 1-bit input: Clock input (S=0)
    I1 => s_clknet_out.mb_clk40_q1_remote, -- 1-bit input: Clock input (S=1)
    S => p_clkin_in.clksel -- 1-bit input: Clock select
);

 i_mmcm_gbt40_mb_q1 : mmcm_gbt40_mb
    port map ( 
    -- Clock in ports
    p_clk_in => s_clknet_out.mb_clk40_q1,
--    clk_in2_p => p_clkinputs_in.mb_q1_clkin_remote.p,
--    clk_in2_n => p_clkinputs_in.mb_q1_clkin_remote.n,
--    clk_in1_p => p_clkinputs_in.mb_q1_clkin_local.p,
--    clk_in1_n => p_clkinputs_in.mb_q1_clkin_local.n,
--    p_clk_sel_in => p_clk_sel_in,
    -- Clock out ports  
    p_clk40_out => s_clknet_out.mb_clk40.q1,
    p_clk40_90_out => s_clknet_out.mb_clk40_phase_90.q1,
    p_clk40_180_out => s_clknet_out.mb_clk40_phase_180.q1,
    p_clk40_270_out => s_clknet_out.mb_clk40_phase_270.q1,
    p_clk80_out => s_clknet_out.mb_clk80.q1,
    p_clk280_out => s_clknet_out.mb_clk280.q1,
    p_clk560_out => s_clknet_out.mb_clk560.q1,
    -- Dynamic reconfiguration ports
    p_daddr_in => s_mmcm_gbt40_mb_q1.daddr_in,
    p_dclk_in => s_clknet_out.osc_clk40,
    p_den_in => s_mmcm_gbt40_mb_q1.den_in,
    p_din_in => s_mmcm_gbt40_mb_q1.din_in,
    p_dout_out => s_mmcm_gbt40_mb_q1.dout_out,
    p_drdy_out => s_mmcm_gbt40_mb_q1.drdy_out,
    p_dwe_in => s_mmcm_gbt40_mb_q1.dwe_in,
    --Dynamic phase shift ports
    p_psclk_in => s_mmcm_gbt40_mb_q1.psclk_in,
    p_psen_in => s_mmcm_gbt40_mb_q1.psen_in,
    p_psincdec_in => s_mmcm_gbt40_mb_q1.psincdec_in,
    p_psdone_out => s_mmcm_gbt40_mb_q1.psdone_out,
   -- Status and control signals                
    p_reset_in => s_mmcm_gbt40_mb_q1.reset_in,
    p_locked_out => s_mmcm_gbt40_mb_q1.locked_out,
    p_input_clk_stopped_out=> s_mmcm_gbt40_mb_q1.input_clk_stopped_out,
    p_clkfb_stopped_out=> s_mmcm_gbt40_mb_q1.clkfb_stopped_out,
    p_cddcdone_out=> s_mmcm_gbt40_mb_q1.cddcdone_out,
    p_cddcreq_in => s_mmcm_gbt40_mb_q1.cddcreq_in

  );
s_mmcm_gbt40_mb_q1.daddr_in <= (others =>'0');
s_mmcm_gbt40_mb_q1.den_in <= '0';
s_mmcm_gbt40_mb_q1.din_in <= (others =>'0');
s_mmcm_gbt40_mb_q1.dwe_in <= '0';
s_mmcm_gbt40_mb_q1.psclk_in <= '0';
s_mmcm_gbt40_mb_q1.psen_in <= '0';
s_mmcm_gbt40_mb_q1.psincdec_in <= '0';
s_mmcm_gbt40_mb_q1.reset_in <= '0';
s_mmcm_gbt40_mb_q1.cddcreq_in <= '0';
s_clknet_out.locked_mb_q1 <= s_mmcm_gbt40_mb_q1.locked_out;


i_OBUFDS_mb_q0 : OBUFDS
    port map (
    O => s_clknet_out.mb_clk40_q0_dp.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
    OB => s_clknet_out.mb_clk40_q0_dp.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
    I => s_gbt40_q0_delay_control.dataout--s_clknet_out.mb_clk40(0) --s_gbt40_q0_delay_control.dataout--s_clknet_out.mb_clk40(0) -- 1-bit input: Buffer input
    );

i_OBUFDS_mb_q1 : OBUFDS
    port map (
    O => s_clknet_out.mb_clk40_q1_dp.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
    OB => s_clknet_out.mb_clk40_q1_dp.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
    I => s_gbt40_q1_delay_control.dataout-- s_clknet_out.mb_clk40(1) -- s_gbt40_q1_delay_control.dataout--s_clknet_out.mb_clk40(1) -- 1-bit input: Buffer input
    );

i_odelaye3_gbt40_q0 : odelaye3
generic map (
    SIM_DEVICE => "ULTRASCALE",
    cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
    delay_format => "count", -- (time, count)
    delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
    delay_value => 0, -- output delay tap setting
    is_clk_inverted => '0', -- optional inversion for clk
    is_rst_inverted => '0', -- optional inversion for rst
    refclk_frequency => 400.0, -- idelayctrl clock input frequency in mhz (values).
    update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
)
port map (
    casc_out => s_gbt40_q0_delay_control.casc_out, -- 1-bit output: cascade delay output to idelay input cascade
    cntvalueout => s_gbt40_q0_delay_control.cntvalueout, -- 9-bit output: counter value output
    dataout => s_gbt40_q0_delay_control.dataout, -- 1-bit output: delayed data from odatain input port
    casc_in => s_gbt40_q0_delay_control.casc_in , -- 1-bit input: cascade delay input from slave idelay cascade_out
    casc_return => s_gbt40_q0_delay_control.casc_return, -- 1-bit input: cascade delay returning from slave idelay dataout
    ce => s_gbt40_q0_delay_control.ce , -- 1-bit input: active high enable increment/decrement input
    clk => s_clknet_out.mb_clk40.q0, -- 1-bit input: clock input
    cntvaluein => s_gbt40_q0_delay_control.cntvaluein, -- 9-bit input: counter value input
    en_vtc => s_gbt40_q0_delay_control.en_vtc, -- 1-bit input: keep delay constant over vt
    inc => s_gbt40_q0_delay_control.inc, -- 1-bit input: increment / decrement tap delay input
    load => s_gbt40_q0_delay_control.load, -- 1-bit input: load delay_value input
    odatain => s_gbt40_q0_delay_control.odatain, -- 1-bit input: data input
    rst => s_gbt40_q0_delay_control.rst -- 1-bit input: asynchronous reset to the delay_value
);

    s_gbt40_q0_delay_control.casc_in <= '0';
    s_gbt40_q0_delay_control.casc_return <= '0';
    s_gbt40_q0_delay_control.ce <= '0';
    s_gbt40_q0_delay_control.cntvaluein <= (others => '0');
    s_gbt40_q0_delay_control.en_vtc <= '1';
    s_gbt40_q0_delay_control.inc <= '0';
    s_gbt40_q0_delay_control.load <= '0';
    s_gbt40_q0_delay_control.rst <= '0';
    
i_oddre1_gbt40_q0 : oddre1
generic map (
    is_c_inverted => '0', -- optional inversion for c
    is_d1_inverted => '0', -- optional inversion for d1
    is_d2_inverted => '0', -- optional inversion for d2
    srval => '0' -- initializes the oddre1 flip-flops to the specified value (1'b0, 1'b1)
)
    port map (
    q => s_gbt40_q0_delay_control.odatain, -- 1-bit output: data output to iob
    c => s_clknet_out.mb_clk40.q0, -- 1-bit input: high-speed clock input
    d1 => s_gbt40_q0_clk_shape(0), -- 1-bit input: parallel data input 1
    d2 => s_gbt40_q0_clk_shape(1), -- 1-bit input: parallel data input 2
    sr => p_master_reset_in -- 1-bit input: active high async reset
);

i_odelaye3_gbt40_q1 : odelaye3
generic map (
    SIM_DEVICE => "ULTRASCALE",
    cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
    delay_format => "count", -- (time, count)
    delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
    delay_value => 0, -- output delay tap setting
    is_clk_inverted => '0', -- optional inversion for clk
    is_rst_inverted => '0', -- optional inversion for rst
    refclk_frequency => 400.0, -- idelayctrl clock input frequency in mhz (values).
    update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
)
port map (
    casc_out => s_gbt40_q1_delay_control.casc_out, -- 1-bit output: cascade delay output to idelay input cascade
    cntvalueout => s_gbt40_q1_delay_control.cntvalueout, -- 9-bit output: counter value output
    dataout => s_gbt40_q1_delay_control.dataout, -- 1-bit output: delayed data from odatain input port
    casc_in => s_gbt40_q1_delay_control.casc_in , -- 1-bit input: cascade delay input from slave idelay cascade_out
    casc_return => s_gbt40_q1_delay_control.casc_return, -- 1-bit input: cascade delay returning from slave idelay dataout
    ce => s_gbt40_q1_delay_control.ce , -- 1-bit input: active high enable increment/decrement input
    clk => s_clknet_out.mb_clk40.q1, -- 1-bit input: clock input
    cntvaluein => s_gbt40_q1_delay_control.cntvaluein, -- 9-bit input: counter value input
    en_vtc => s_gbt40_q1_delay_control.en_vtc, -- 1-bit input: keep delay constant over vt
    inc => s_gbt40_q1_delay_control.inc, -- 1-bit input: increment / decrement tap delay input
    load => s_gbt40_q1_delay_control.load, -- 1-bit input: load delay_value input
    odatain => s_gbt40_q1_delay_control.odatain, -- 1-bit input: data input
    rst => s_gbt40_q1_delay_control.rst -- 1-bit input: asynchronous reset to the delay_value
);

    s_gbt40_q1_delay_control.casc_in <= '0';
    s_gbt40_q1_delay_control.casc_return <= '0';
    s_gbt40_q1_delay_control.ce <= '0';
    s_gbt40_q1_delay_control.cntvaluein <= (others => '0');
    s_gbt40_q1_delay_control.en_vtc <= '1';
    s_gbt40_q1_delay_control.inc <= '0';
    s_gbt40_q1_delay_control.load <= '0';
    s_gbt40_q1_delay_control.rst <= '0';
    
i_oddre1_gbt40_q1 : oddre1
generic map (
    is_c_inverted => '0', -- optional inversion for c
    is_d1_inverted => '0', -- optional inversion for d1
    is_d2_inverted => '0', -- optional inversion for d2
    srval => '0' -- initializes the oddre1 flip-flops to the specified value (1'b0, 1'b1)
)
    port map (
    q => s_gbt40_q1_delay_control.odatain, -- 1-bit output: data output to iob
    c => s_clknet_out.mb_clk40.q1, -- 1-bit input: high-speed clock input
    d1 => s_gbt40_q1_clk_shape(0), -- 1-bit input: parallel data input 1
    d2 => s_gbt40_q1_clk_shape(1), -- 1-bit input: parallel data input 2
    sr => p_master_reset_in -- 1-bit input: active high async reset
);



i_BUFGMUX_CTRL_tp_clk40_q0 : BUFGMUX_CTRL
port map (
    O =>s_clknet_out.tp_clk40.q0,--s_clk40, -- 1-bit output: Clock output
    I0 => s_tp_clk40_q0_local, -- 1-bit input: Clock input (S=0)
    I1 => s_tp_clk40_q0_remote, -- 1-bit input: Clock input (S=1)
    S => p_clkin_in.clksel -- 1-bit input: Clock select
);

i_BUFGMUX_CTRL_tp_clk40_q1 : BUFGMUX_CTRL
port map (
    O =>s_clknet_out.tp_clk40.q1,--s_clk40, -- 1-bit output: Clock output
    I0 => s_tp_clk40_q1_local, -- 1-bit input: Clock input (S=0)
    I1 => s_tp_clk40_q1_remote, -- 1-bit input: Clock input (S=1)
    S => p_clkin_in.clksel -- 1-bit input: Clock select
);


 i_ibufds_tp_clk40_q0_local : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_tp_clk40_q0_local,  -- clock buffer output
    i => p_clkin_in.tp_q0_clkin_local.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.tp_q0_clkin_local.n -- diff_n clock buffer input (connect directly to top-level port)
 );

 i_ibufds_tp_clk40_q1_local : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_tp_clk40_q1_local,  -- clock buffer output
    i => p_clkin_in.tp_q1_clkin_local.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.tp_q1_clkin_local.n -- diff_n clock buffer input (connect directly to top-level port)
 );

 i_ibufds_tp_clk40_q0_remote : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_tp_clk40_q0_remote,  -- clock buffer output
    i => p_clkin_in.tp_q0_clkin_remote.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.tp_q0_clkin_remote.n -- diff_n clock buffer input (connect directly to top-level port)
 );

 i_ibufds_tp_clk40_q1_remote : ibufds
 generic map (
    diff_term => true, -- differential termination
    iostandard => "sub_lvds")
 port map (
    o => s_tp_clk40_q1_remote,  -- clock buffer output
    i => p_clkin_in.tp_q1_clkin_remote.p,  -- diff_p clock buffer input (connect directly to top-level port)
    ib => p_clkin_in.tp_q1_clkin_remote.n -- diff_n clock buffer input (connect directly to top-level port)
 );




--    i_BUFGMUX_CTRL_db_clk80 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk80, -- 1-bit output: Clock output
--        I0 => s_clknet_out.osc_clk80, -- 1-bit input: Clock input (S=0)
--        I1 => s_clknet_out.db_clk80, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_gbt40_db.locked_out -- 1-bit input: Clock select
--    );
    
--    i_BUFGMUX_CTRL_db_clk160 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk160, -- 1-bit output: Clock output
--        I0 => s_clknet_out.osc_clk160, -- 1-bit input: Clock input (S=0)
--        I1 => s_clknet_out.db_clk160, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_gbt40_db.locked_out -- 1-bit input: Clock select
--    );
    
--    i_BUFGMUX_CTRL_db_clk280 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk280, -- 1-bit output: Clock output
--        I0 => s_clknet_out.osc_clk280, -- 1-bit input: Clock input (S=0)
--        I1 => s_clknet_out.db_clk280, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_gbt40_db.locked_out -- 1-bit input: Clock select
--    );

--    i_BUFGMUX_CTRL_db_clk40_phase_90 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk40_phase_90, -- 1-bit output: Clock output
--        I0 => s_clknet_out.osc_clk40_phase_90, -- 1-bit input: Clock input (S=0)
--        I1 => s_clknet_out.db_clk40_phase_90, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_gbt40_db.locked_out -- 1-bit input: Clock select
--    );

--    i_BUFGMUX_CTRL_db_clk40_phase_180 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk40_phase_180, -- 1-bit output: Clock output
--        I0 => s_clknet_out.osc_clk40_phase_180, -- 1-bit input: Clock input (S=0)
--        I1 => s_clknet_out.db_clk40_phase_180, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_gbt40_db.locked_out -- 1-bit input: Clock select
--    );

--    i_BUFGMUX_CTRL_db_clk40_phase_270 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk40_phase_270, -- 1-bit output: Clock output
--        I0 => s_clknet_out.osc_clk40_phase_270, -- 1-bit input: Clock input (S=0)
--        I1 => s_clknet_out.db_clk40_phase_270, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_gbt40_db.locked_out -- 1-bit input: Clock select
--    );

--    i_BUFGMUX_CTRL_clk100 : BUFGMUX_CTRL
--    port map (
--        O => s_clknet_out.clk40, -- 1-bit output: Clock output
--        I0 => s_osc_clk100, -- 1-bit input: Clock input (S=0)
--        I1 => s_clk40, -- 1-bit input: Clock input (S=1)
--        S => s_mmcm_osc.locked_out -- 1-bit input: Clock select
--    );



end RTL;