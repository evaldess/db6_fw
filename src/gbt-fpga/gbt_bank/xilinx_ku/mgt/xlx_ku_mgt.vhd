-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific transceiver
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

--! Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
library tilecal;
use tilecal.db6_design_package.all;

--! @brief MGT - Transceiver
--! @details 
--! The MGT module provides the interface to the transceivers to send the GBT-links via
--! high speed links (@4.8Gbps)
entity mgt is
   generic (
      NUM_LINKS                                      : integer := 1
   );                                                
   port (
       --=============--
       -- Clocks      --
       --=============--
       p_clknet_in                  : in t_db_clknet;
       --MGT_REFCLK_i                 : in  std_logic_vector(1 to NUM_LINKS);
       p_rxoutclkfabric_out         : out std_logic_vector(1 to NUM_LINKS);
       p_txoutclkfabric_out         : out std_logic_vector(1 to NUM_LINKS);
       
       MGT_RXUSRCLK_o               : out std_logic_vector(1 to NUM_LINKS);
       MGT_TXUSRCLK_o               : out std_logic_vector(1 to NUM_LINKS);
       
       --=============--
       -- Resets      --
       --=============--
       MGT_TXRESET_i                : in  std_logic_vector(1 to NUM_LINKS);
       MGT_RXRESET_i                : in  std_logic_vector(1 to NUM_LINKS);
       
       --=============--
       -- Status      --
       --=============--
       MGT_TXREADY_o                : out std_logic_vector(1 to NUM_LINKS);
       MGT_RXREADY_o                : out std_logic_vector(1 to NUM_LINKS);

       RX_HEADERLOCKED_o            : out std_logic_vector(1 to NUM_LINKS);
       RX_HEADERFLAG_o              : out std_logic_vector(1 to NUM_LINKS);
       MGT_RSTCNT_o                 : out gbt_reg8_A(1 to NUM_LINKS);
       
         --==============--
         -- Control      --
         --==============--
       MGT_AUTORSTEn_i              : in  std_logic_vector(1 to NUM_LINKS);
       MGT_AUTORSTONEVEN_i          : in  std_logic_vector(1 to NUM_LINKS);
         
       --==============--
       -- Data         --
       --==============--
       MGT_USRWORD_i                : in  word_mxnbit_A(1 to NUM_LINKS);
       MGT_USRWORD_o                : out word_mxnbit_A(1 to NUM_LINKS);
       
       --=============================--
       -- Device specific connections --
       --=============================--
       MGT_DEVSPEC_i                : in  mgtDeviceSpecific_i_R;
       MGT_DEVSPEC_o                : out mgtDeviceSpecific_o_R
   
   );
end mgt;

--! @brief MGT - Transceiver
--! @details The MGT module implements all the logic required to send the GBT frame on high speed
--! links: resets modules for the transceiver, Tx PLL and alignement logic to align the received word with the 
--! GBT frame header.
architecture structural of mgt is
   --================================ Signal Declarations ================================--

   --==============================--
   -- RX phase alignment (bitslip) --
   --==============================--
   
   
       signal rx_wordclk_sig                         : std_logic_vector(1 to NUM_LINKS);
       signal tx_wordclk_sig                         : std_logic_vector(1 to NUM_LINKS);
       
       signal rxoutclk_sig                           : std_logic_vector(1 to NUM_LINKS);
       signal txoutclk_sig                           : std_logic_vector(1 to NUM_LINKS);
       
       signal rx_reset_done                          : std_logic_vector(1 to NUM_LINKS);
       signal tx_reset_done                          : std_logic_vector(1 to NUM_LINKS);
       
       signal rxResetDone_r3                         : std_logic_vector         (1 to NUM_LINKS);
       signal txResetDone_r2                         : std_logic_vector         (1 to NUM_LINKS);
       signal rxResetDone_r2                         : std_logic_vector         (1 to NUM_LINKS);
       signal txResetDone_r                          : std_logic_vector         (1 to NUM_LINKS);   
       signal rxResetDone_r                          : std_logic_vector         (1 to NUM_LINKS);    
       
       signal rxfsm_reset_done                       : std_logic_vector(1 to NUM_LINKS);
       signal txfsm_reset_done                       : std_logic_vector(1 to NUM_LINKS);
       
       signal txuserclkRdy                           : std_logic_vector(1 to NUM_LINKS);
       signal rxuserclkRdy                           : std_logic_vector(1 to NUM_LINKS);
       
       signal gtwiz_buffbypass_tx_reset_in_s         : std_logic_vector(1 to NUM_LINKS);
       signal gtwiz_buffbypass_rx_reset_in_s         : std_logic_vector(1 to NUM_LINKS);
       
       signal rxpmaresetdone                         : std_logic_vector(1 to NUM_LINKS);
       signal txpmaresetdone                         : std_logic_vector(1 to NUM_LINKS);
          
       signal run_to_rxBitSlipControl                : std_logic_vector         (1 to NUM_LINKS);
       signal rxBitSlip_from_rxBitSlipControl        : std_logic_vector         (1 to NUM_LINKS);
       signal rxBitSlip_to_gtx                       : std_logic_vector         (1 to NUM_LINKS);   
       signal done_from_rxBitSlipControl             : std_logic_vector         (1 to NUM_LINKS);
                 
       type rstBitSlip_FSM_t                 is (idle, reset_tx, reset_rx);
       type rstBitSlip_FSM_t_A              is array (natural range <>) of rstBitSlip_FSM_t; 
       signal rstBitSlip_FSM                : rstBitSlip_FSM_t_A(1 to NUM_LINKS);
            
       signal mgtRst_from_bitslipCtrl       : std_logic_vector(1 to NUM_LINKS);
       signal rx_reset_sig                                  : std_logic_vector(1 to NUM_LINKS);
       signal tx_reset_sig                                  : std_logic_vector(1 to NUM_LINKS);
       
       signal resetGtxRx_from_rxBitSlipControl            : std_logic_vector         (1 to NUM_LINKS);   
       signal resetGtxTx_from_rxBitSlipControl            : std_logic_vector         (1 to NUM_LINKS);  
       
       signal txprgdivresetdone_int      : std_logic_vector(1 to NUM_LINKS);
       signal gtwiz_userclk_tx_reset_int, not_gtwiz_userclk_tx_reset_int : std_logic_vector(1 to NUM_LINKS);
       signal gtwiz_userclk_rx_reset_int, not_gtwiz_userclk_rx_reset_int : std_logic_vector(1 to NUM_LINKS);
       signal gtwiz_userclk_tx_active_int, not_gtwiz_userclk_tx_active_int: std_logic_vector(1 to NUM_LINKS);
       signal bitslip_pattern_reset : std_logic_vector(1 to NUM_LINKS);
       signal gtwiz_userclk_rx_active_int: std_logic_vector(1 to NUM_LINKS);
       signal rxBuffBypassRst            : std_logic_vector(1 to NUM_LINKS);
       signal resetAllMgt                : std_logic_vector(1 to NUM_LINKS);
       
       signal MGT_USRWORD_s              : gbt_reg40_A(1 to NUM_LINKS);
       signal bitSlipCmd_to_bitSlipCtrller : std_logic_vector(1 to NUM_LINKS);
       signal ready_from_bitSlipCtrller    : std_logic_vector(1 to NUM_LINKS);
 
       signal rx_headerlocked_s            : std_logic_vector(1 to NUM_LINKS);
       signal rx_bitslipIsEven_s           : std_logic_vector(1 to NUM_LINKS);


COMPONENT xlx_ku_mgt_ip
  PORT (
    gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
    drpaddr_common_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    drpclk_common_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpdi_common_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drpen_common_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpwe_common_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtgrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtgrefclk1_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk11_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpdo_common_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    drprdy_common_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    cpllrefclksel_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    drpaddr_in : IN STD_LOGIC_VECTOR(26 DOWNTO 0);
    drpclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpdi_in : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    drpen_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpwe_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtgrefclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtrefclk1_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    loopback_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    rxoutclksel_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    rxpolarity_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxslide_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxsysclksel_in : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    rxusrclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxusrclk2_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    txdiffctrl_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    txmaincursor_in : IN STD_LOGIC_VECTOR(20 DOWNTO 0);
    txoutclksel_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    txpolarity_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    txpostcursor_in : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    txprecursor_in : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    txsysclksel_in : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    txusrclk_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    txusrclk2_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cplllock_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpdo_out : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    drprdy_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxoutclk_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxoutclkfabric_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txoutclk_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txoutclkfabric_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)

  );
END COMPONENT;

signal s_ku_mgt : t_ku_mgt;


          
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--
   
    
    s_ku_mgt.gtwiz_reset_rx_pll_and_datapath_in <= "0";
    s_ku_mgt.gtwiz_reset_tx_datapath_in <= "0";
    s_ku_mgt.gtwiz_reset_all_in <= "0";
    s_ku_mgt.gtwiz_buffbypass_tx_start_user_in <= "0";
    s_ku_mgt.gtwiz_buffbypass_rx_start_user_in <= "0";
    
    s_ku_mgt.gtgrefclk0_in(0) <= p_clknet_in.clk160;
    s_ku_mgt.gtgrefclk1_in(0) <= p_clknet_in.clk160;
    
    s_ku_mgt.gtwiz_userclk_tx_active_in(0) <= gtwiz_userclk_tx_active_int(1);
    s_ku_mgt.gtwiz_userclk_rx_active_in(0) <= gtwiz_userclk_tx_active_int(1);
    s_ku_mgt.gtwiz_buffbypass_tx_reset_in(0) <= gtwiz_buffbypass_tx_reset_in_s(1);

    --s_ku_mgt.gtwiz_buffbypass_tx_error_out
    s_ku_mgt.gtwiz_buffbypass_rx_reset_in(0) <= gtwiz_buffbypass_rx_reset_in_s(1);
    
    --s_ku_mgt.gtwiz_buffbypass_rx_error_out(0)
    s_ku_mgt.gtwiz_reset_clk_freerun_in(0) <= MGT_DEVSPEC_i.reset_freeRunningClock(1);
    s_ku_mgt.gtwiz_reset_tx_pll_and_datapath_in(0) <= tx_reset_sig(1);
                 
    s_ku_mgt.gtwiz_reset_rx_datapath_in(0) <= rx_reset_sig(1);
    --s_ku_mgt.gtwiz_reset_rx_cdr_stable_out
                 
--    tx_reset_done(1) <= s_ku_mgt.gtwiz_reset_tx_done_out(0);
--    rx_reset_done(1) <= s_ku_mgt.gtwiz_reset_rx_done_out(0);
--    rxfsm_reset_done(1) <= s_ku_mgt.gtwiz_buffbypass_rx_done_out(0);
--    txfsm_reset_done(1) <= s_ku_mgt.gtwiz_buffbypass_tx_done_out(0);
--    rxoutclk_sig(1) <= s_ku_mgt.rxoutclk_out(0);
--    txoutclk_sig(1) <= s_ku_mgt.txoutclk_out(0);

             xlx_ku_mgt_std_i: xlx_ku_mgt_ip
             PORT MAP (          
                 rxusrclk_in => s_ku_mgt.rxusrclk_in,
                 rxusrclk2_in => s_ku_mgt.rxusrclk2_in,
                 txusrclk_in => s_ku_mgt.txusrclk_in,
                 txusrclk2_in =>  s_ku_mgt.txusrclk2_in,
                 rxoutclk_out => s_ku_mgt.rxoutclk_out,
                 rxoutclkfabric_out => s_ku_mgt.rxoutclkfabric_out,
                 txoutclk_out => s_ku_mgt.txoutclk_out,
                 txoutclkfabric_out => s_ku_mgt.txoutclkfabric_out,
                 
                 gtwiz_userclk_tx_active_in          => s_ku_mgt.gtwiz_userclk_tx_active_in,
                 gtwiz_userclk_rx_active_in          => s_ku_mgt.gtwiz_userclk_rx_active_in,                 
                 
                 gtwiz_buffbypass_tx_reset_in        => s_ku_mgt.gtwiz_buffbypass_tx_reset_in,
                 gtwiz_buffbypass_tx_start_user_in   => s_ku_mgt.gtwiz_buffbypass_tx_start_user_in,
                 gtwiz_buffbypass_tx_done_out        => s_ku_mgt.gtwiz_buffbypass_tx_done_out,
                 gtwiz_buffbypass_tx_error_out          => s_ku_mgt.gtwiz_buffbypass_tx_error_out,
                 
                 gtwiz_buffbypass_rx_reset_in        => s_ku_mgt.gtwiz_buffbypass_rx_reset_in,
                 gtwiz_buffbypass_rx_start_user_in   => s_ku_mgt.gtwiz_buffbypass_rx_start_user_in,
                 gtwiz_buffbypass_rx_done_out        => s_ku_mgt.gtwiz_buffbypass_rx_done_out,
                 gtwiz_buffbypass_rx_error_out          => s_ku_mgt.gtwiz_buffbypass_rx_error_out,
                 
                 gtwiz_reset_clk_freerun_in          => s_ku_mgt.gtwiz_reset_clk_freerun_in,
                 
                 gtwiz_reset_all_in                  => s_ku_mgt.gtwiz_reset_all_in,
                 
                 gtwiz_reset_tx_pll_and_datapath_in  => s_ku_mgt.gtwiz_reset_tx_pll_and_datapath_in,
                 gtwiz_reset_tx_datapath_in          => s_ku_mgt.gtwiz_reset_tx_datapath_in,
                 
                 gtwiz_reset_rx_pll_and_datapath_in  => s_ku_mgt.gtwiz_reset_rx_pll_and_datapath_in,--'0', -- Same PLL is used for TX and RX !
                 gtwiz_reset_rx_datapath_in          => s_ku_mgt.gtwiz_reset_rx_datapath_in,
                 gtwiz_reset_rx_cdr_stable_out          => s_ku_mgt.gtwiz_reset_rx_cdr_stable_out,
                 
                 gtwiz_reset_tx_done_out             => s_ku_mgt.gtwiz_reset_tx_done_out,
                 gtwiz_reset_rx_done_out             => s_ku_mgt.gtwiz_reset_rx_done_out,
                 
                 gtwiz_userdata_tx_in                   => s_ku_mgt.gtwiz_userdata_tx_in,
                 gtwiz_userdata_rx_out                  => s_ku_mgt.gtwiz_userdata_rx_out,
                 
                 gtrefclk01_in                      => s_ku_mgt.gtrefclk01_in,
                 gtrefclk11_in                      => s_ku_mgt.gtrefclk11_in,
                 qpll1refclksel_in                  => s_ku_mgt.qpll1refclksel_in,
                 qpll1fbclklost_out                 => s_ku_mgt.qpll1fbclklost_out,
                 qpll1lock_out                      => s_ku_mgt.qpll1lock_out,
                 qpll1outclk_out                    => s_ku_mgt.qpll1outclk_out,
                 qpll1outrefclk_out                 => s_ku_mgt.qpll1outrefclk_out,
                 qpll1refclklost_out                => s_ku_mgt.qpll1refclklost_out,
                 cpllrefclksel_in                   => s_ku_mgt.cpllrefclksel_in,
                 
                 drpaddr_in                             => s_ku_mgt.drpaddr_in,
                 drpclk_in                           => s_ku_mgt.drpclk_in,
                 drpdi_in                               => s_ku_mgt.drpdi_in,
                 drpen_in                            => s_ku_mgt.drpen_in,
                 drpwe_in                            => s_ku_mgt.drpwe_in,
                 gtgrefclk_in                           => s_ku_mgt.gtgrefclk_in,
                 drpdo_out                              => s_ku_mgt.drpdo_out,
                 drprdy_out                          => s_ku_mgt.drprdy_out,

                 drpaddr_common_in                  => s_ku_mgt.drpaddr_common_in,
                 drpclk_common_in                   => s_ku_mgt.drpclk_common_in,
                 drpdi_common_in                    => s_ku_mgt.drpdi_common_in,
                 drpen_common_in                    => s_ku_mgt.drpen_common_in,
                 drpwe_common_in                    => s_ku_mgt.drpwe_common_in,
                 drpdo_common_out                   => s_ku_mgt.drpdo_common_out,
                 drprdy_common_out                  => s_ku_mgt.drprdy_common_out,
                 
                 gthrxn_in                           => s_ku_mgt.gthrxn_in,
                 gthrxp_in                           => s_ku_mgt.gthrxp_in,
                 gthtxn_out                          => s_ku_mgt.gthtxn_out,
                 gthtxp_out                          => s_ku_mgt.gthtxp_out,
                 
                 gtrefclk0_in                        => s_ku_mgt.gtrefclk0_in,
                 gtrefclk1_in                        => s_ku_mgt.gtrefclk1_in,
                 
                 loopback_in                            => s_ku_mgt.loopback_in,
                 rxoutclksel_in                         => s_ku_mgt.rxoutclksel_in,
                 txoutclksel_in                         => s_ku_mgt.txoutclksel_in,   
                 rxpolarity_in                       => s_ku_mgt.rxpolarity_in,
                 txpolarity_in                       => s_ku_mgt.txpolarity_in,
                 
                 rxslide_in                          => s_ku_mgt.rxslide_in,
                 rxsysclksel_in                      => s_ku_mgt.rxsysclksel_in,
                 txdiffctrl_in                          => s_ku_mgt.txdiffctrl_in,
                 txmaincursor_in                        => s_ku_mgt.txmaincursor_in,
                 txpostcursor_in                        => s_ku_mgt.txpostcursor_in,
                 txprecursor_in                         => s_ku_mgt.txprecursor_in,
                 txsysclksel_in                         => s_ku_mgt.txsysclksel_in,
                 cplllock_out                           => s_ku_mgt.cplllock_out,
                 
                 rxpmaresetdone_out                  => s_ku_mgt.rxpmaresetdone_out,
                 txpmaresetdone_out                  => s_ku_mgt.txpmaresetdone_out,
                 
                 gtgrefclk0_in                       => s_ku_mgt.gtgrefclk0_in,
                 gtgrefclk1_in                       => s_ku_mgt.gtgrefclk1_in
             );
   
   
  --==================--
  -- Piro Assignments --
  --==================--  

    s_ku_mgt.gtrefclk01_in(0)                             <= p_clknet_in.gth_refclk_remote(0);
    s_ku_mgt.gtrefclk11_in(0)                             <= p_clknet_in.gth_refclk_local(0);

    s_ku_mgt.qpll1refclksel_in                            <= p_clknet_in.qpllclksel;

   gen_clk_signals : for c in 0 to num_links-1 generate  
    s_ku_mgt.gtgrefclk_in(c) <= p_clknet_in.clk160;
--    s_ku_mgt.gtgrefclk_in(1) <= p_clknet_in.clk160;
--    s_ku_mgt.gtgrefclk_in(2) <= p_clknet_in.clk160;
    
    s_ku_mgt.gtrefclk0_in(c)                             <= p_clknet_in.gth_refclk_remote(0);
--    s_ku_mgt.gtrefclk0_in(1)                             <= p_clknet_in.gth_refclk_local;
--    s_ku_mgt.gtrefclk0_in(2)                             <= p_clknet_in.gth_refclk_local;
    s_ku_mgt.gtrefclk1_in(c)                             <= p_clknet_in.gth_refclk_local(0);
--    s_ku_mgt.gtrefclk1_in(1)                             <= p_clknet_in.gth_refclk_remote;
--    s_ku_mgt.gtrefclk1_in(2)                             <= p_clknet_in.gth_refclk_remote;


    
 
    s_ku_mgt.cpllrefclksel_in(3*(c+1)-1 downto 3*c)                            <= p_clknet_in.cpllclksel;
    --s_ku_mgt.cpllrefclksel_in                            <= (p_clknet_in.cpllclksel & p_clknet_in.cpllclksel & p_clknet_in.cpllclksel);
    
    
    s_ku_mgt.txsysclksel_in(2*(c+1)-1 downto 2*c)                              <= p_clknet_in.txsysclksel;
    s_ku_mgt.rxsysclksel_in(2*(c+1)-1 downto 2*c)                              <= p_clknet_in.rxsysclksel;
--    s_ku_mgt.txsysclksel_in                              <= (p_clknet_in.txsysclksel & p_clknet_in.txsysclksel & p_clknet_in.txsysclksel);
--    s_ku_mgt.rxsysclksel_in                              <= (p_clknet_in.rxsysclksel & p_clknet_in.rxsysclksel & p_clknet_in.rxsysclksel);

    s_ku_mgt.txoutclksel_in(3*(c+1)-1 downto 3*c)                              <= p_clknet_in.txoutclksel;
    s_ku_mgt.rxoutclksel_in(3*(c+1)-1 downto 3*c)                              <= p_clknet_in.rxoutclksel;
--    s_ku_mgt.txoutclksel_in                              <= (p_clknet_in.txoutclksel & p_clknet_in.txoutclksel & p_clknet_in.txoutclksel);
--    s_ku_mgt.rxoutclksel_in                              <= (p_clknet_in.rxoutclksel & p_clknet_in.rxoutclksel & p_clknet_in.rxoutclksel);
   end generate;
   
   gtxLatOpt_gen: for i in 1 to NUM_LINKS generate
            
         
    s_ku_mgt.gtwiz_userdata_tx_in((i)*40-1 downto (i-1)*40)          <= MGT_USRWORD_i(i);
    MGT_USRWORD_s(i) <= s_ku_mgt.gtwiz_userdata_rx_out((i)*40-1 downto (i-1)*40);
    
    s_ku_mgt.drpaddr_in((i)*9-1 downto (i-1)*9)                             <= MGT_DEVSPEC_i.drp_addr(i);
    s_ku_mgt.drpclk_in(i-1)                             <= MGT_DEVSPEC_i.drp_clk(i);
    s_ku_mgt.drpdi_in((i)*16-1 downto (i-1)*16)                             <= MGT_DEVSPEC_i.drp_di(i);
    s_ku_mgt.drpen_in(i-1)                             <= MGT_DEVSPEC_i.drp_en(i);
    s_ku_mgt.drpwe_in(i-1)                             <= MGT_DEVSPEC_i.drp_we(i);
    MGT_DEVSPEC_o.drp_do(i) <= s_ku_mgt.drpdo_out((i)*16-1 downto (i-1)*16);
    MGT_DEVSPEC_o.drp_rdy(i) <= s_ku_mgt.drprdy_out(i-1);
    
    s_ku_mgt.gthrxn_in(i-1)                             <= MGT_DEVSPEC_i.rx_n(i);
    s_ku_mgt.gthrxp_in(i-1)                             <= MGT_DEVSPEC_i.rx_p(i);
    MGT_DEVSPEC_o.tx_n(i) <= s_ku_mgt.gthtxn_out(i-1);
    MGT_DEVSPEC_o.tx_p(i) <= s_ku_mgt.gthtxp_out(i-1);
    
--    s_ku_mgt.gtrefclk0_in(i-1)                             <= MGT_REFCLK_i(i);
--    s_ku_mgt.gtrefclk01_in(i-1)                             <= MGT_REFCLK_i(i);
    
    s_ku_mgt.loopback_in((i)*3-1 downto (i-1)*3)                             <= MGT_DEVSPEC_i.loopBack(i);        
    s_ku_mgt.rxpolarity_in(i-1)                             <= MGT_DEVSPEC_i.conf_rxPol(i);
    s_ku_mgt.txpolarity_in(i-1)                             <= MGT_DEVSPEC_i.conf_txPol(i);
    
    s_ku_mgt.rxslide_in(i-1)                             <= rxBitSlip_to_gtx(i);
    
    s_ku_mgt.txdiffctrl_in((i)*4-1 downto (i-1)*4)                             <= MGT_DEVSPEC_i.conf_diffCtrl(i);
    s_ku_mgt.txpostcursor_in((i)*5-1 downto (i-1)*5)                             <= MGT_DEVSPEC_i.conf_postCursor(i);
    s_ku_mgt.txprecursor_in((i)*5-1 downto (i-1)*5)                             <= MGT_DEVSPEC_i.conf_preCursor(i);
    
    rxpmaresetdone(i) <= s_ku_mgt.rxpmaresetdone_out(i-1);
    txpmaresetdone(i) <= s_ku_mgt.txpmaresetdone_out(i-1);
    
    tx_reset_done(i) <= s_ku_mgt.gtwiz_reset_tx_done_out(0);
    rx_reset_done(i) <= s_ku_mgt.gtwiz_reset_rx_done_out(0);
    rxfsm_reset_done(i) <= s_ku_mgt.gtwiz_buffbypass_rx_done_out(0);
    txfsm_reset_done(i) <= s_ku_mgt.gtwiz_buffbypass_tx_done_out(0);
    rxoutclk_sig(i) <= s_ku_mgt.rxoutclk_out(i-1);
    txoutclk_sig(i) <= s_ku_mgt.txoutclk_out(i-1);    
    
    s_ku_mgt.rxusrclk_in(i-1) <= rx_wordclk_sig(i);
    s_ku_mgt.rxusrclk2_in(i-1) <= rx_wordclk_sig(i);
    s_ku_mgt.txusrclk_in(i-1) <= tx_wordclk_sig(i);
    s_ku_mgt.txusrclk2_in(i-1) <= tx_wordclk_sig(i);
         
  --=============--
  -- Assignments --
  --=============--              
     MGT_TXREADY_o(i)          <= tx_reset_done(i) and txfsm_reset_done(i);
     MGT_RXREADY_o(i)          <= rx_reset_done(i) and rxfsm_reset_done(i) and done_from_rxBitSlipControl(i);
           
         MGT_RXUSRCLK_o(i)         <= rx_wordclk_sig(i);   
         MGT_TXUSRCLK_o(i)         <= tx_wordclk_sig(i);
          
         MGT_USRWORD_o(i)    <= MGT_USRWORD_s(i);
      
         rx_reset_sig(i)                  <= MGT_RXRESET_i(i) or resetGtxRx_from_rxBitSlipControl(i);
         tx_reset_sig(i)                  <= MGT_TXRESET_i(i) or resetGtxTx_from_rxBitSlipControl(i);      
          
--          txResetDoneSync: entity work.xlx_ku_mgt_ip_reset_synchronizer
--              PORT MAP(
--                clk_in                                   => tx_wordclk_sig(i),
--                rst_in                                   => tx_reset_done(i),
--                rst_out                                  => txResetDone_r2(i)
--              ); 
              
--          rxResetSync: entity work.xlx_ku_mgt_ip_reset_synchronizer
--            PORT MAP(
--              clk_in                                   => rx_wordClk_sig(i),
--              rst_in                                   => rx_reset_done(i),
--              rst_out                                  => rxResetDone_r3(i)
--            ); 
       
          rxBuffBypassRst(i) <= not(gtwiz_userclk_rx_active_int(i)) or not(txfsm_reset_done(i));
          
          resetDoneSynch_rx: entity work.xlx_ku_mgt_ip_reset_synchronizer
             PORT MAP(
               clk_in                                   => rx_wordClk_sig(i),
               rst_in                                   => rxBuffBypassRst(i),
               rst_out                                  => gtwiz_buffbypass_rx_reset_in_s(i)
             );
             
          not_gtwiz_userclk_tx_active_int(i)<= not(gtwiz_userclk_tx_active_int(i));
          resetSynch_tx: entity work.xlx_ku_mgt_ip_reset_synchronizer
             PORT MAP(
               clk_in                                   => tx_wordclk_sig(i),
               rst_in                                   => not_gtwiz_userclk_tx_active_int(i), --not(gtwiz_userclk_tx_active_int(i)),
               rst_out                                  => gtwiz_buffbypass_tx_reset_in_s(i)
             );
          
          gtwiz_userclk_tx_reset_int(i) <= not(txpmaresetdone(i));
          gtwiz_userclk_rx_reset_int(i) <= not(rxpmaresetdone(i));
          
          not_gtwiz_userclk_rx_reset_int(i) <= not(gtwiz_userclk_rx_reset_int(i));
          rxWordClkBuf_inst: bufg_gt
              port map (
                 O                                        => rx_wordclk_sig(i), 
                 I                                        => rxoutclk_sig(i),
                 CE                                       =>  not_gtwiz_userclk_rx_reset_int(i),--not(gtwiz_userclk_rx_reset_int(i)),
                 DIV                                      => "000",
                 CLR                                      => '0',
                 CLRMASK                                  => '0',
                 CEMASK                                   => '0'
              ); 

            not_gtwiz_userclk_tx_reset_int(i) <= not(gtwiz_userclk_tx_reset_int(i));                         
            txWordClkBuf_inst: bufg_gt
              port map (
                 O                                        => tx_wordclk_sig(i), 
                 I                                        => txoutclk_sig(i),
                 CE                                       => not_gtwiz_userclk_tx_reset_int(i),--not(gtwiz_userclk_tx_reset_int(i)),
                 DIV                                      => "000",
                 CLR                                      => '0',
                 CLRMASK                                  => '0',
                 CEMASK                                   => '0'
              ); 
          
          activetxUsrClk_proc: process(gtwiz_userclk_tx_reset_int(i), tx_wordclk_sig(i))
          begin
            if gtwiz_userclk_tx_reset_int(i) = '1' then
                gtwiz_userclk_tx_active_int(i) <= '0';
            elsif rising_edge(tx_wordclk_sig(i)) then
                gtwiz_userclk_tx_active_int(i) <= '1';
            end if;
            
          end process;
          
                    
          activerxUsrClk_proc: process(gtwiz_userclk_rx_reset_int(i), rx_wordclk_sig(i))
          begin
            if gtwiz_userclk_rx_reset_int(i) = '1' then
                gtwiz_userclk_rx_active_int(i) <= '0';
            elsif rising_edge(rx_wordclk_sig(i)) then
                gtwiz_userclk_rx_active_int(i) <= '1';
            end if;
          
          end process;
                    

             
          --====================--
          -- RX phase alignment --
          --====================--                   
            -- Reset on bitslip control module:
            -----------------------------------
               bitslipResetFSM_proc: PROCESS(MGT_DEVSPEC_i.reset_freeRunningClock(i), MGT_RXRESET_i(i))
                 variable timer :integer range 0 to GBTRX_BITSLIP_MGT_RX_RESET_DELAY;  
              variable rstcnt: unsigned(7 downto 0);              
               begin
         
                   if MGT_RXRESET_i(i) = '1' then
                        resetGtxRx_from_rxBitSlipControl(i) <= '0';
                        resetGtxTx_from_rxBitSlipControl(i) <= '0';
                        timer  := 0;
                        rstcnt := (others => '0');
                        rstBitSlip_FSM(i) <= idle;
                     
                   elsif rising_edge(MGT_DEVSPEC_i.reset_freeRunningClock(i)) then
                                     
                        case rstBitSlip_FSM(i) is
                           when idle      =>       resetGtxRx_from_rxBitSlipControl(i)     <= '0';
                                                   resetGtxTx_from_rxBitSlipControl(i)     <= '0';
                                                
                                                   if mgtRst_from_bitslipCtrl(i) = '1' then
                                                    
                                                       resetGtxRx_from_rxBitSlipControl(i) <= '1';
                                                       resetGtxTx_from_rxBitSlipControl(i) <= '1';
                                                       rstBitSlip_FSM(i) <= reset_tx;
                                                       timer := 0;
                                                       
                                                       rstcnt := rstcnt+1;
                                                    
                                                   end if;
                                                             
                           when reset_tx  => if timer = GBTRX_BITSLIP_MGT_RX_RESET_DELAY-1 then
                                                        resetGtxTx_from_rxBitSlipControl(i)     <= '0';
                                                        rstBitSlip_FSM(i)                       <= reset_rx;
                                                        timer                                   := 0;
                                                   else
                                                        timer := timer + 1;
                                                   end if;
                                                
                           when reset_rx  => if timer = GBTRX_BITSLIP_MGT_RX_RESET_DELAY-1 then
                                                         resetGtxRx_from_rxBitSlipControl(i)     <= '0';
                                                         rstBitSlip_FSM(i)                       <= idle;
                                                         timer                                   := 0;
                                                    else
                                                         timer := timer + 1;
                                                    end if;
                                                
                        end case;
                     
                       MGT_RSTCNT_o(i)   <= std_logic_vector(rstcnt);
                   end if;
                
               end process; 
            
            bitslip_pattern_reset(i)<= not(rx_reset_done(i) and rxfsm_reset_done(i));
            rxBitSlipControl: entity work.mgt_bitslipctrl 
               port map (
                       RX_RESET_I          => bitslip_pattern_reset(i),--not(rx_reset_done(i) and rxfsm_reset_done(i)),
                       RX_WORDCLK_I        => rx_wordclk_sig(i),
                       MGT_CLK_I           => MGT_DEVSPEC_i.reset_freeRunningClock(i),
                                      
                       RX_BITSLIPCMD_i     => bitSlipCmd_to_bitSlipCtrller(i),
                       RX_BITSLIPCMD_o     => rxBitSlip_to_gtx(i),
                       
                       RX_HEADERLOCKED_i   => rx_headerlocked_s(i),
                       RX_BITSLIPISEVEN_i  => rx_bitslipIsEven_s(i),
                       RX_RSTONBITSLIP_o   => mgtRst_from_bitslipCtrl(i),
                       RX_ENRST_i          => MGT_AUTORSTEn_i(i),
                       RX_RSTONEVEN_i      => MGT_AUTORSTONEVEN_i(i),
                       
                       DONE_o              => done_from_rxBitSlipControl(i),
                       READY_o             => ready_from_bitSlipCtrller(i)
               );
                   
               patternSearch: entity work.mgt_framealigner_pattsearch
                   port map (
                       RX_RESET_I                          => bitslip_pattern_reset(i),--not(rx_reset_done(i) and rxfsm_reset_done(i)),
                       RX_WORDCLK_I                        => rx_wordclk_sig(i),
                       
                       RX_BITSLIP_CMD_O                    => bitSlipCmd_to_bitSlipCtrller(i),
                       MGT_BITSLIPDONE_i                   => ready_from_bitSlipCtrller(i),
                             
                       RX_HEADER_LOCKED_O                  => rx_headerlocked_s(i),
                       RX_HEADER_FLAG_O                    => RX_HEADERFLAG_o(i),
                       RX_BITSLIPISEVEN_o                  => rx_bitslipIsEven_s(i), 
                       
                       RX_WORD_I                           => MGT_USRWORD_s(i)
                   );
                   
                   RX_HEADERLOCKED_o(i) <= rx_headerlocked_s(i);
                   
  end generate;     
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--