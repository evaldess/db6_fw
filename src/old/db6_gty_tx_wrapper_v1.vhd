
-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;


ENTITY db6_gty_tx_wrapper_v1 is
  PORT (
    clk40:        IN STD_LOGIC;
    oscclk  :     IN STD_LOGIC; -- 100 MHz oscillator clock for monitoring, reset
    tx_reset_in : IN STD_LOGIC;
    userclk_tx_srcclk_out : OUT STD_LOGIC;
    userclk_tx_usrclk_out : OUT STD_LOGIC;
    userclk_tx_usrclk2_out : OUT STD_LOGIC;
    userclk_tx_active_out : OUT STD_LOGIC;
    userdata_tx_in : IN STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC;
    gtrefclk1_in : IN STD_LOGIC;
    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 downto 0);
    gtytxn_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtytxp_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END db6_gty_tx_wrapper_v1;

Architecture structural of db6_gty_tx_wrapper_v1 is

-- Convert input clock signals to buffers

signal gtrefclk0_in_buf, gtrefclk1_in_buf : std_logic_vector(1 downto 0);
signal gtrefclk0_in_buf1, gtrefclk1_in_buf1 : std_logic_vector(0 downto 0);


COMPONENT gty_tx_2
  PORT (
    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk11_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtyrxn_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtyrxp_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    txdiffctrl_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txpostcursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txprecursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txswing_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtytxn_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtytxp_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END COMPONENT;

  signal gtpowergood, txuserrdy, gttxreset, cpllreset : std_logic_vector(1 downto 0) := "00";
  signal txresetdone : std_logic_vector(0 downto 0) := "0";
  signal tx_reset_powerup, tx_reset_clkchange, tx_reset, reset_bypass : std_logic;
  signal trigger_tx_reset : std_logic := '0';
  signal userclk_reset : std_logic_vector(0 downto 0) := "0";
  signal reset_buff_bypass : std_logic_vector(0 downto 0) := "0";
  signal tx_reset_done : std_logic_vector(0 downto 0) := "0";
  signal txpmaresetdone, txprgdivresetdone: std_logic_vector(1 downto 0);
  signal userclk_tx_usrclk_buf, userclk_tx_usrclk2_buf : std_logic;
  signal qpll1refclk_lastsel : std_logic_vector(2 downto 0);
  
  constant txdiffctrl : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1011010110"; -- Driver swing control (10110 = 809 mV)
  constant txpostcursor : STD_LOGIC_VECTOR(9 DOWNTO 0) :=  "1000010000"; -- Post-cursor TX pre-emphasis (10000 = 4.44 dB)
  constant txprecursor : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1000010000"; -- Pre-cursor TX pre-emphasis (1000 = 4.44 dB)
  constant txswing : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- TX swing control (0 for full-swing)

begin

    gtrefclk0_in_buf1(0) <=  gtrefclk0_in;
    gtrefclk1_in_buf1(0) <=  gtrefclk1_in;
    
    userclk_tx_usrclk_out <= userclk_tx_usrclk_buf;
    userclk_tx_usrclk2_out <= userclk_tx_usrclk2_buf;
        
    trigger_tx_reset <= tx_reset_in or tx_reset_powerup or tx_reset_clkchange;


 gty_reset_synch : process (oscclk)
        type gty_state is (powergood, release_reset, wait_lock, buffreset, idle);
        variable state : gty_state := powergood;
        variable counter : integer := 0;
    begin
        if rising_edge(oscclk) then
            case state is
                when powergood =>  -- wait for GTPOWERGOOD, then assert tx_reset
                    tx_reset <= '1';
                    if gtpowergood = "11" then
                        state := release_reset;
                        counter := 0;
                    end if;
                when release_reset =>  -- wait 2 us, then deassert tx_reset 
                    if counter > 200 then
                        tx_reset <= '0';
                        state := wait_lock;
                    end if;
                when wait_lock =>  -- wait for tx reset to be done, then reset buffer bypass
                    if tx_reset_done = "1" then
                            counter := 0;
                            reset_bypass <= '1';
                            state := buffreset;
                    end if;
                when buffreset => -- deassert GTTXRESET and wait for 20 us
                    if counter > 10 then
                        reset_bypass <= '0'; -- opytional signal for buffer bypass
                        state := idle;
                    end if;
                when others => -- idle state
                    tx_reset <= '0';
                    if trigger_tx_reset = '1' then
                        state := powergood;
                    end if;
            end case;
            counter := counter + 1;
        end if; -- clock edge
    end process;


monitor_clksel : process(oscclk)  -- Force a reset if the reference clock source has changed
        variable counter : integer := 0;
        variable state : integer := 0; -- 1 for reset condition, 0 otherwise
    begin
        if rising_edge (oscclk) then
            qpll1refclk_lastsel <= qpll1refclksel_in;
            counter := counter + 1;
            if state = 0 then  -- Check for changed clock source
                if qpll1refclk_lastsel /= qpll1refclksel_in then
                    tx_reset_clkchange <= '1';
                    state := 1;
                    counter := 0;
                else
                    tx_reset_clkchange <= '0';
                end if;
            else  -- Hold reset for about 100 ns
                if counter > 10 then
                    tx_reset_clkchange <='0';
                    state := 0;
                end if;
            end if;                
        end if; 
    end process;

reset_on_powerup : process (clk40)
        variable counter: integer := 0;
        variable state : std_logic := '0';  -- 0 at powerup, 1 when completed
    begin
        if rising_edge (clk40) then
            case state is
                when '0' =>
                    tx_reset_powerup <= '0';
                    counter := counter + 1;
                    if counter > 10000 then
                        state := '1';
                        tx_reset_powerup <= '1';
                    else
                        state := '0';
                        tx_reset_powerup <= '0';
                    end if;                
                when others =>
                    tx_reset_powerup <= '0';
                end case;
        end if;
    end process;

userclk_reset(0) <= not gtpowergood(0);

instantiate_gty : gty_tx_2
  PORT MAP (
    gtwiz_userclk_tx_reset_in => userclk_reset,
    gtwiz_userclk_tx_srcclk_out(0) => userclk_tx_srcclk_out,
    gtwiz_userclk_tx_usrclk_out(0) => userclk_tx_usrclk_buf,
    gtwiz_userclk_tx_usrclk2_out(0) => userclk_tx_usrclk2_buf,
    gtwiz_userclk_tx_active_out(0) => userclk_tx_active_out,
    gtwiz_userclk_rx_reset_in => "0",
    gtwiz_userclk_rx_srcclk_out => open,
    gtwiz_userclk_rx_usrclk_out => open,
    gtwiz_userclk_rx_usrclk2_out => open,
    gtwiz_userclk_rx_active_out => open,
    gtwiz_buffbypass_tx_reset_in => "0",
    gtwiz_buffbypass_tx_start_user_in => "0",
    gtwiz_buffbypass_tx_done_out => open,
    gtwiz_buffbypass_tx_error_out => open,
    gtwiz_buffbypass_rx_reset_in => "0",
    gtwiz_buffbypass_rx_start_user_in => "0",
    gtwiz_buffbypass_rx_done_out => open,
    gtwiz_buffbypass_rx_error_out => open,
    gtwiz_reset_clk_freerun_in(0) => oscclk,
    gtwiz_reset_all_in(0) => tx_reset,
    gtwiz_reset_tx_pll_and_datapath_in(0) => tx_reset,
    gtwiz_reset_tx_datapath_in(0) => tx_reset,
    gtwiz_reset_rx_pll_and_datapath_in(0) => tx_reset,
    gtwiz_reset_rx_datapath_in => "0",
    gtwiz_reset_rx_cdr_stable_out => open,
    gtwiz_reset_tx_done_out => tx_reset_done,
    gtwiz_reset_rx_done_out => open,
    gtwiz_userdata_tx_in => userdata_tx_in,
    gtwiz_userdata_rx_out => open,
    gtrefclk01_in => gtrefclk0_in_buf1,
    gtrefclk11_in => gtrefclk1_in_buf1,
    qpll1refclksel_in => qpll1refclksel_in,
    qpll1outclk_out => open,
    qpll1outrefclk_out => open,
    gtyrxn_in => "00",
    gtyrxp_in => "00",
    txdiffctrl_in => txdiffctrl,
    txpostcursor_in => txpostcursor,
    txprecursor_in => txprecursor,
    txswing_in => txswing,
    gtpowergood_out => gtpowergood,
    gtytxn_out => gtytxn_out,
    gtytxp_out => gtytxp_out,
    rxpmaresetdone_out => open,
    txpmaresetdone_out => txpmaresetdone,
    txprgdivresetdone_out => txprgdivresetdone
  );



END structural;