--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Samuel Silverstein    silver@fysik.su.se
--                        Eduardo Valdes Santurio
--                                                                                                 
-- Project Name:          ADC deserializer for LTC2264-12                                                                
-- Module Name:           ADC_top                                        
--                                                                                                 
-- Language:              VHDL                                                                 
--                                                                                                   --
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_adc_interface is

    Port ( 	
                p_master_reset_in          : in std_logic;
				p_clknet_in 			: in  t_db_clock_network;
				p_adc_data0_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data0_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_p_in : in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_n_in : in  STD_LOGIC_VECTOR (5 downto 0);
				
				p_idelay_ctrl_reset_lg_in : in std_logic_vector (5 downto 0);
				p_idelay_ctrl_reset_hg_in : in std_logic_vector (5 downto 0);
				p_idelay3_load_lg_in : in std_logic_vector (5 downto 0);
				p_idelay3_load_hg_in : in std_logic_vector (5 downto 0);
			    p_idelay_en_vtc_lg_in : in std_logic_vector (5 downto 0);
			    p_idelay_en_vtc_hg_in : in std_logic_vector (5 downto 0);
			    p_idelay_count_in_lg_in : in t_idelay_count;
			    p_idelay_count_in_hg_in : in t_idelay_count;
                p_idelay_count_in_lg_out : out t_idelay_count;
                p_idelay_count_in_hg_out : out t_idelay_count;
                p_bitslip_lg_in : in t_bitslip;
                p_bitslip_hg_in : in t_bitslip;
				--p_clkdiv_out		: out STD_LOGIC;
				p_adc_data_out		: out t_adc_data_type
				);
end db6_adc_interface;

architecture Behavioral of db6_adc_interface is
    


    signal s_data_lg, s_data_hg, s_data_lg_delayed, s_data_hg_delayed , s_bitclk_in, s_bitclk, s_frameclk : std_logic_vector (5 downto 0);
    --signal s_idelay_ctrl_rdy_data_lg_buf, s_idelay_ctrl_rdy_data_hg_buf : std_logic_vector (5 downto 0);
    --signal s_idelay_ctrl_rdy_data_bank64_buf, s_idelay_ctrl_rdy_data_bank65_buf, s_idelay_ctrl_rdy_data_bank66_buf : std_logic; 
    signal s_idelay_ctrl_rdy_data_global_buf, s_idelay_ctrl_rdy_data_global : std_logic;
    signal s_idelay_ctrl_reset_data_global_buf, s_idelay_ctrl_reset_data_global : std_logic;
    
    signal s_frame_strobe : std_logic_vector (5 downto 0);
    
    type t_adc_sr is array(5 downto 0) of std_logic_vector(13 downto 0);
    
    
    signal s_readout_sr_lg_odd, s_readout_sr_lg_even : t_adc_sr; -- Rising edge of bit clock times for odd bits
    signal s_readout_sr_hg_odd, s_readout_sr_hg_even : t_adc_sr; -- Falling edge of bit clock times for even bits
    signal s_frame_clk_odd, s_frame_clk_even : t_adc_sr; -- Falling edge of bit clock times for even bits
    signal s_adc_output_temp, s_frame_clk_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;
    signal s_adc_output_word, s_adc_word : t_adc_data_type;
    
--    signal s_idelay3_load_lg, s_idelay3_load_hg, s_idelay3_load_channel, s_idelay_en_vtc_lg, s_idelay_en_vtc_hg, s_idelay_en_vtc_channel, s_idelay_ctrl_reset_lg, s_idelay_ctrl_reset_hg   : std_logic_vector (5 downto 0);
--    signal s_idelay_count_in_lg, s_idelay_count_in_hg, s_idelay_count_out_lg,  s_idelay_count_out_hg : t_idelay_count;
--    signal s_bitslip_lg, s_bitslip_hg : t_bitslip;
    
    attribute keep : string;
    attribute keep of s_readout_sr_lg_odd : signal is "true";
    attribute keep of s_readout_sr_hg_odd : signal is "true";
    attribute keep of s_readout_sr_lg_even : signal is "true";
    attribute keep of s_readout_sr_hg_even : signal is "true";
    attribute keep of s_adc_word          : signal is "true";
    

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG


begin

p_adc_data_out <= s_adc_word;


diff_to_se_delays : for i in 0 to 5 generate

    IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_lg(i),             -- Buffer diff_p output
        I  => p_adc_data0_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data0_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_DATA1 : IBUFDS -- ADC output High gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_hg(i),             -- Buffer diff_p output
        I  => p_adc_data1_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data1_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );



i_idelaye3_data_lg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => p_idelay_count_in_lg_out(i), -- 9-bit output: counter value output
        dataout => s_data_lg_delayed(i), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => p_idelay_count_in_lg_in(i), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => p_idelay_en_vtc_lg_in(i), -- 1-bit input: keep delay constant over vt
        idatain => s_data_lg(i), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => p_idelay3_load_lg_in(i), -- 1-bit input: load delay_value input
        rst => p_idelay_ctrl_reset_lg_in(i) -- 1-bit input: asynchronous reset to the delay_value
);




i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
    )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => p_idelay_count_in_hg_out(i), -- 9-bit output: counter value output
        dataout => s_data_hg_delayed(i), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => p_idelay_count_in_hg_in(i), -- 9-bit input: counter value input
        datain => '0', --s_data_hg(i), -- 1-bit input: data input from the iobuf
        en_vtc => p_idelay_en_vtc_hg_in(i), -- 1-bit input: keep delay constant over vt
        idatain => s_data_hg(i), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => p_idelay3_load_hg_in(i), -- 1-bit input: load delay_value input
        rst => p_idelay_ctrl_reset_lg_in(i) -- 1-bit input: asynchronous reset to the delay_value
);


    IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_frameclk(i),             -- Buffer diff_p output
        I  => p_adc_frameclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_frameclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_bitclk_in(i),             -- Buffer diff_p output
        I  => p_adc_bitclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_bitclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    clock_buffer : bufg
        port map (
            I => s_bitclk_in(i),
            O => s_bitclk(i)
        );

end generate;

-- Generate ADC data input registers and output word mapping,
-- The output bits from each ADC is stored in four shift registers (two even and two odd).


gen_adc_input_gen: for v_adc in 0 to 5 generate

        sr_LG : process(s_bitclk(v_adc)) -- Odd data bits clocked in on rising edge of adc clocks 
        begin
            if rising_edge (s_bitclk(v_adc)) then
                s_readout_sr_lg_odd(v_adc) <= s_readout_sr_lg_odd(v_adc)(12 downto 0) & s_data_lg_delayed(v_adc);
                s_readout_sr_hg_odd(v_adc) <= s_readout_sr_hg_odd(v_adc)(12 downto 0) & s_data_hg_delayed(v_adc);
                --s_frame_clk_odd(v_adc) <= s_frame_clk_odd(v_adc)(13 downto 0) & s_frameclk(v_adc);
                
            end if;
        end process; -- sr_odd 

        sr_HG : process(s_bitclk(v_adc)) -- Even data bits clocked in on rising edge of adc clocks 
        begin
            if falling_edge (s_bitclk(v_adc)) then
                s_readout_sr_lg_even(v_adc) <= s_readout_sr_lg_even(v_adc)(12 downto 0) & s_data_lg_delayed(v_adc);
                s_readout_sr_hg_even(v_adc) <= s_readout_sr_hg_even(v_adc)(12 downto 0) & s_data_hg_delayed(v_adc);
                --s_frame_clk_even(v_adc) <= s_frame_clk_even(v_adc)(13 downto 0) & s_frameclk(v_adc);
            end if;
        end process; -- sr_even 

        -- Assemble the bits into contiguous 14-bit words 
        
        s_adc_input_hg_temp(v_adc) <=
                                        s_readout_sr_hg_odd(v_adc)(13) & s_readout_sr_hg_even(v_adc)(13) &
                                        s_readout_sr_hg_odd(v_adc)(12) & s_readout_sr_hg_even(v_adc)(12) &
                                        s_readout_sr_hg_odd(v_adc)(11) & s_readout_sr_hg_even(v_adc)(11) &
                                        s_readout_sr_hg_odd(v_adc)(10) & s_readout_sr_hg_even(v_adc)(10) &
                                        s_readout_sr_hg_odd(v_adc)(9) & s_readout_sr_hg_even(v_adc)(9) & 
                                        s_readout_sr_hg_odd(v_adc)(8) & s_readout_sr_hg_even(v_adc)(8) &
                                        s_readout_sr_hg_odd(v_adc)(7) & s_readout_sr_hg_even(v_adc)(7) & 
                                        s_readout_sr_hg_odd(v_adc)(6) & s_readout_sr_hg_even(v_adc)(6) &
                                        s_readout_sr_hg_odd(v_adc)(5) & s_readout_sr_hg_even(v_adc)(5) &
                                        s_readout_sr_hg_odd(v_adc)(4) & s_readout_sr_hg_even(v_adc)(4) &
                                        s_readout_sr_hg_odd(v_adc)(3) & s_readout_sr_hg_even(v_adc)(3) &
                                        s_readout_sr_hg_odd(v_adc)(2) & s_readout_sr_hg_even(v_adc)(2) &
                                        s_readout_sr_hg_odd(v_adc)(1) & s_readout_sr_hg_even(v_adc)(1) &
                                        s_readout_sr_hg_odd(v_adc)(0)   & s_readout_sr_hg_even(v_adc)(0);

        s_adc_input_lg_temp(v_adc) <=
                                        s_readout_sr_lg_odd(v_adc)(13) & s_readout_sr_lg_even(v_adc)(13) &
                                        s_readout_sr_lg_odd(v_adc)(12) & s_readout_sr_lg_even(v_adc)(12) &
                                        s_readout_sr_lg_odd(v_adc)(11) & s_readout_sr_lg_even(v_adc)(11) &
                                        s_readout_sr_lg_odd(v_adc)(10) & s_readout_sr_lg_even(v_adc)(10) &
                                        s_readout_sr_lg_odd(v_adc)(9) & s_readout_sr_lg_even(v_adc)(9) &
                                        s_readout_sr_lg_odd(v_adc)(8) & s_readout_sr_lg_even(v_adc)(8) &
                                        s_readout_sr_lg_odd(v_adc)(7) & s_readout_sr_lg_even(v_adc)(7) &
                                        s_readout_sr_lg_odd(v_adc)(6) & s_readout_sr_lg_even(v_adc)(6) &
                                        s_readout_sr_lg_odd(v_adc)(5) & s_readout_sr_lg_even(v_adc)(5) &
                                        s_readout_sr_lg_odd(v_adc)(4) & s_readout_sr_lg_even(v_adc)(4) &
                                        s_readout_sr_lg_odd(v_adc)(3) & s_readout_sr_lg_even(v_adc)(3) &
                                        s_readout_sr_lg_odd(v_adc)(2) & s_readout_sr_lg_even(v_adc)(2) &
                                        s_readout_sr_lg_odd(v_adc)(1) & s_readout_sr_lg_even(v_adc)(1) &
                                        s_readout_sr_lg_odd(v_adc)(0)   & s_readout_sr_lg_even(v_adc)(0);
--        s_frame_clk_temp(v_adc) <=
--                                        s_frame_clk_odd(v_adc)(13) & s_frame_clk_even(v_adc)(13) &
--                                        s_frame_clk_odd(v_adc)(12) & s_frame_clk_even(v_adc)(12) &
--                                        s_frame_clk_odd(v_adc)(11) & s_frame_clk_even(v_adc)(11) &
--                                        s_frame_clk_odd(v_adc)(10) & s_frame_clk_even(v_adc)(10) &
--                                        s_frame_clk_odd(v_adc)(9) & s_frame_clk_even(v_adc)(9) &
--                                        s_frame_clk_odd(v_adc)(8) & s_frame_clk_even(v_adc)(7) &
--                                        s_frame_clk_odd(v_adc)(7) & s_frame_clk_even(v_adc)(6) &
--                                        s_frame_clk_odd(v_adc)(6) & s_frame_clk_even(v_adc)(5) &
--                                        s_frame_clk_odd(v_adc)(5) & s_frame_clk_even(v_adc)(4) &
--                                        s_frame_clk_odd(v_adc)(4) & s_frame_clk_even(v_adc)(3) &
--                                        s_frame_clk_odd(v_adc)(2) & s_frame_clk_even(v_adc)(2) &
--                                        s_frame_clk_odd(v_adc)(1) & s_frame_clk_even(v_adc)(1) &
--                                        s_frame_clk_odd(v_adc)(0) & s_frame_clk_even(v_adc)(0);
                                        
                                        

        frame_capture : process(s_bitclk(v_adc)) -- Capture ADC words synchronous to ADC (bit) clock
            type t_frame_state is (low, high);
            variable v_frame : t_frame_state := low;
            variable v_counter : integer :=0;
        begin
            if rising_edge(s_bitclk(v_adc)) then
                case v_frame is
                    when low =>
                        if s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
                            s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1)  downto (to_integer(unsigned(p_bitslip_hg_in(v_adc)))) );
                            s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1)  downto (to_integer(unsigned(p_bitslip_lg_in(v_adc)))));
                            v_frame := high;
                        end if;
                    when high =>
                        if s_frameclk(v_adc) = '0' then
                            v_frame := low;
                        end if;
                end case;
            end if; 
        end process;  -- frame capture

end generate;

        bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
        begin
            if falling_edge(p_clknet_in.clk40) then
                s_adc_word <=  s_adc_output_word;
            end if;
        end process;


end Behavioral;


----=================================================================================================--
----##################################   Module Information   #######################################--
----=================================================================================================--
----                                                                                         
---- Company:               Stockholm University                                                        
---- Engineer:              Samuel Silverstein    silver@fysik.su.se
----                        Eduardo Valdes Santurio
----                                                                                                 
---- Project Name:          ADC deserializer for LTC2264-12                                                                
---- Module Name:           ADC_top                                        
----                                                                                                 
---- Language:              VHDL                                                                 
----                                                                                                   --
----
----=================================================================================================--
----#################################################################################################--
----=================================================================================================--

--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

--library UNISIM;
--use UNISIM.VComponents.all;
--library tilecal;
--use tilecal.db6_design_package.all;

--entity db6_adc_interface is
--    Port ( 	
--				p_clknet_in 			: in  t_db_clock_network;
--				p_db_reg_rx_in       : in    t_db_reg_rx;
--				p_adc_data0_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_data0_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_data1_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_data1_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_bitclk_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_bitclk_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_frameclk_p_in : in  STD_LOGIC_VECTOR (5 downto 0);
--				p_adc_frameclk_n_in : in  STD_LOGIC_VECTOR (5 downto 0);
--				--p_clkdiv_out		: out STD_LOGIC;
--				p_adc_data_out		: out t_adc_data_type
--				);
--end db6_adc_interface;

--architecture Behavioral of db6_adc_interface is

--    signal s_data_lg, s_data_hg, s_data_lg_delayed, s_data_hg_delayed , s_bitclk_in, s_bitclk, s_frameclk : std_logic_vector (5 downto 0);
--    --signal s_idelay_ctrl_rdy_data_lg_buf, s_idelay_ctrl_rdy_data_hg_buf : std_logic_vector (5 downto 0);
--    --signal s_idelay_ctrl_rdy_data_bank64_buf, s_idelay_ctrl_rdy_data_bank65_buf, s_idelay_ctrl_rdy_data_bank66_buf : std_logic; 
--    signal s_idelay_ctrl_rdy_data_global_buf, s_idelay_ctrl_rdy_data_global : std_logic;
--    signal s_idelay_ctrl_reset_data_global_buf, s_idelay_ctrl_reset_data_global : std_logic;
    
--    signal s_frame_strobe : std_logic_vector (5 downto 0);
    
    
    
--    signal s_readout_sr_lg_odd, s_readout_sr_lg_even : t_adc_sr; -- Rising edge of bit clock times for odd bits
--    signal s_readout_sr_hg_odd, s_readout_sr_hg_even : t_adc_sr; -- Falling edge of bit clock times for even bits
--    signal s_frame_clk_odd, s_frame_clk_even : t_adc_sr; -- Falling edge of bit clock times for even bits
--    signal s_adc_output_temp, s_frame_clk_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;
--    signal s_adc_output_word, s_adc_word : t_adc_data_type;
    
--    signal s_idelay3_load_lg, s_idelay3_load_hg, s_idelay3_load_channel, s_idelay_en_vtc_lg, s_idelay_en_vtc_hg, s_idelay_en_vtc_channel  : std_logic_vector (5 downto 0);
--    signal s_idelay_count_in_lg, s_idelay_count_in_hg, s_idelay_count_out_lg,  s_idelay_count_out_hg : t_idelay_count;
--    signal s_bitslip_lg, s_bitslip_hg : t_bitslip;
--    --type t_adc_temp_reg is array(5 downto 0) of std_logic_vector(13 downto 0);
--    --signal s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_temp_reg;
    
--    attribute keep : string;
--    attribute keep of s_readout_sr_lg_odd : signal is "true";
--    attribute keep of s_readout_sr_hg_odd : signal is "true";
--    attribute keep of s_readout_sr_lg_even : signal is "true";
--    attribute keep of s_readout_sr_hg_even : signal is "true";
--    attribute keep of s_adc_word          : signal is "true";
    

--begin

--p_adc_data_out <= s_adc_word;

--s_idelay_ctrl_reset_data_global<=p_db_reg_rx_in(cfb_strobe_reg)(7);
--s_idelay_count_in_lg(0)<= p_db_reg_rx_in(adc_readout_idelay3_0)(8 downto 0);
--s_idelay_count_in_hg(0)<= p_db_reg_rx_in(adc_readout_idelay3_0)(24 downto 16);
--s_idelay_count_in_lg(1)<= p_db_reg_rx_in(adc_readout_idelay3_1)(8 downto 0);
--s_idelay_count_in_hg(1)<= p_db_reg_rx_in(adc_readout_idelay3_1)(24 downto 16);
--s_idelay_count_in_lg(2)<= p_db_reg_rx_in(adc_readout_idelay3_2)(8 downto 0);
--s_idelay_count_in_hg(2)<= p_db_reg_rx_in(adc_readout_idelay3_2)(24 downto 16);
--s_idelay_count_in_lg(3)<= p_db_reg_rx_in(adc_readout_idelay3_3)(8 downto 0);
--s_idelay_count_in_hg(3)<= p_db_reg_rx_in(adc_readout_idelay3_3)(24 downto 16);
--s_idelay_count_in_lg(4)<= p_db_reg_rx_in(adc_readout_idelay3_4)(8 downto 0);
--s_idelay_count_in_hg(4)<= p_db_reg_rx_in(adc_readout_idelay3_4)(24 downto 16);
--s_idelay_count_in_lg(5)<= p_db_reg_rx_in(adc_readout_idelay3_5)(8 downto 0);
--s_idelay_count_in_hg(5)<= p_db_reg_rx_in(adc_readout_idelay3_5)(24 downto 16);

--s_idelay3_load_lg(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(9);
--s_idelay3_load_hg(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(25);
--s_idelay3_load_lg(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(9);
--s_idelay3_load_hg(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(25);
--s_idelay3_load_lg(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(9);
--s_idelay3_load_hg(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(25);
--s_idelay3_load_lg(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(9);
--s_idelay3_load_hg(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(25);
--s_idelay3_load_lg(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(9);
--s_idelay3_load_hg(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(25);
--s_idelay3_load_lg(5)<=p_db_reg_rx_in(adc_readout_idelay3_0)(9);
--s_idelay3_load_hg(5)<=p_db_reg_rx_in(adc_readout_idelay3_0)(25);

--s_idelay_en_vtc_lg(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(10);
--s_idelay_en_vtc_hg(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(26);
--s_idelay_en_vtc_lg(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(10);
--s_idelay_en_vtc_hg(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(26);
--s_idelay_en_vtc_lg(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(10);
--s_idelay_en_vtc_hg(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(26);
--s_idelay_en_vtc_lg(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(10);
--s_idelay_en_vtc_hg(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(26);
--s_idelay_en_vtc_lg(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(10);
--s_idelay_en_vtc_hg(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(26);
--s_idelay_en_vtc_lg(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(10);
--s_idelay_en_vtc_hg(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(26);

--s_bitslip_lg(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(14 downto 11);
--s_bitslip_hg(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(30 downto 27);
--s_bitslip_lg(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(14 downto 11);
--s_bitslip_hg(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(30 downto 27);
--s_bitslip_lg(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(14 downto 11);
--s_bitslip_hg(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(30 downto 27);
--s_bitslip_lg(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(14 downto 11);
--s_bitslip_hg(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(30 downto 27);
--s_bitslip_lg(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(14 downto 11);
--s_bitslip_hg(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(30 downto 27);
--s_bitslip_lg(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(14 downto 11);
--s_bitslip_hg(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(30 downto 27);

---- Differential to single-ended conversion of ADC inputs from FMC
--proc_delay_ctrl_reset : process(p_clknet_in.clk280)
--begin
--    if rising_edge(p_clknet_in.clk280) then
--        if p_clknet_in.locked_clk_db = '1' then
--            s_idelay_ctrl_reset_data_global_buf <=s_idelay_ctrl_reset_data_global;
--        else
--            s_idelay_ctrl_reset_data_global_buf <='1';
--        end if;
--    end if;
--end process;

------s_idelay_ctrl_reset_data_global<= not p_clknet_in.locked_db;
----s_idelay_ctrl_rdy_data_global<= not s_idelay_ctrl_rdy_data_global_buf;

----i_idelayctrl_data_global : idelayctrl
----      generic map (
----        SIM_DEVICE => "ULTRASCALE")
----        port map (
----        rdy => s_idelay_ctrl_rdy_data_global_buf, -- 1-bit output: ready output
----        refclk => p_clknet_in.clk280, -- 1-bit input: reference clock input
----        rst => s_idelay_ctrl_reset_data_global -- 1-bit input: active high reset input
----);

----i_idelayctrl_data_bank_bank64 : idelayctrl
----        port map (
----        rdy => s_idelay_ctrl_rdy_data_bank64_buf, -- 1-bit output: ready output
----        refclk => p_clknet_in.clk160_osc, -- 1-bit input: reference clock input
----        rst => '0' -- 1-bit input: active high reset input
----);

----i_idelayctrl_data_bank_bank65 : idelayctrl
----        port map (
----        rdy => s_idelay_ctrl_rdy_data_bank65_buf, -- 1-bit output: ready output
----        refclk => p_clknet_in.clk160_osc, -- 1-bit input: reference clock input
----        rst => '0' -- 1-bit input: active high reset input
----);


----i_idelayctrl_data_bank_bank66 : idelayctrl
----        port map (
----        rdy => s_idelay_ctrl_rdy_data_bank66_buf, -- 1-bit output: ready output
----        refclk => p_clknet_in.clk160_osc, -- 1-bit input: reference clock input
----        rst => '0' -- 1-bit input: active high reset input
----);



----s_idelay_ctrl_rdy_data_lg_buf(0)<= not s_idelay_ctrl_rdy_data_bank66_buf;
----s_idelay_ctrl_rdy_data_lg_buf(1)<= not s_idelay_ctrl_rdy_data_bank66_buf;
----s_idelay_ctrl_rdy_data_lg_buf(2)<= not s_idelay_ctrl_rdy_data_bank66_buf;
----s_idelay_ctrl_rdy_data_lg_buf(3)<= not s_idelay_ctrl_rdy_data_bank66_buf;
----s_idelay_ctrl_rdy_data_lg_buf(4)<= not s_idelay_ctrl_rdy_data_bank65_buf;
----s_idelay_ctrl_rdy_data_lg_buf(5)<= not s_idelay_ctrl_rdy_data_bank66_buf;

----s_idelay_ctrl_rdy_data_hg_buf(0)<= not s_idelay_ctrl_rdy_data_bank64_buf;
----s_idelay_ctrl_rdy_data_hg_buf(1)<= not s_idelay_ctrl_rdy_data_bank64_buf;
----s_idelay_ctrl_rdy_data_hg_buf(2)<= not s_idelay_ctrl_rdy_data_bank64_buf;
----s_idelay_ctrl_rdy_data_hg_buf(3)<= not s_idelay_ctrl_rdy_data_bank64_buf;
----s_idelay_ctrl_rdy_data_hg_buf(4)<= not s_idelay_ctrl_rdy_data_bank64_buf;
----s_idelay_ctrl_rdy_data_hg_buf(5)<= not s_idelay_ctrl_rdy_data_bank66_buf;

--diff_to_se_delays : for i in 0 to 5 generate

--    IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_data_lg(i),             -- Buffer diff_p output
--        I  => p_adc_data0_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_data0_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
--        );

--    IBUFDS_DATA1 : IBUFDS -- ADC output High gain
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_data_hg(i),             -- Buffer diff_p output
--        I  => p_adc_data1_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_data1_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
--        );




----i_idelaye3_data_lg : idelaye3
----    generic map (
----        SIM_DEVICE => "ULTRASCALE_PLUS",
----        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
----        delay_format => "time", -- units of the delay_value (time, count)
----        delay_src => "idatain", -- delay input (idatain, datain)
----        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
----        delay_value => 800, -- input delay value setting
----        is_clk_inverted => '0', -- optional inversion for clk
----        is_rst_inverted => '0', -- optional inversion for rst
----        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
----        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
----        )
----        port map (
----        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
----        cntvalueout => open, -- 9-bit output: counter value output
----        dataout => s_data_lg_delayed(i), -- 1-bit output: delayed data output
----        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
----        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
----        ce => '0', -- 1-bit input: active high enable increment/decrement input
----        clk => p_clknet_in.clk280, -- 1-bit input: clock input
----        cntvaluein => "100000000", -- 9-bit input: counter value input
----        datain => '0', -- 1-bit input: data input from the iobuf
----        en_vtc => '1', -- 1-bit input: keep delay constant over vt
----        idatain => s_data_lg(i), -- 1-bit input: data input from the logic
----        inc => '0', -- 1-bit input: increment / decrement tap delay input
----        load => '0', -- 1-bit input: load delay_value input
----        rst => s_idelay_ctrl_rdy_data_global -- 1-bit input: asynchronous reset to the delay_value
----);




----i_idelaye3_data_hg : idelaye3
----    generic map (
----        SIM_DEVICE => "ULTRASCALE_PLUS",
----        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
----        delay_format => "time", -- units of the delay_value (time, count)
----        delay_src => "idatain", -- delay input (idatain, datain)
----        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
----        delay_value => 800, -- input delay value setting
----        is_clk_inverted => '0', -- optional inversion for clk
----        is_rst_inverted => '0', -- optional inversion for rst
----        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
----        update_mode => "sync" -- determines when updates to the delay will take effect (async, manual, sync)
----        )
----        port map (
----        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
----        cntvalueout => open, -- 9-bit output: counter value output
----        dataout => s_data_hg_delayed(i), -- 1-bit output: delayed data output
----        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
----        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
----        ce => '0', -- 1-bit input: active high enable increment/decrement input
----        clk => p_clknet_in.clk280, -- 1-bit input: clock input
----        cntvaluein => "100000000", -- 9-bit input: counter value input
----        datain => '0', -- 1-bit input: data input from the iobuf
----        en_vtc => '1', -- 1-bit input: keep delay constant over vt
----        idatain => s_data_hg(i), -- 1-bit input: data input from the logic
----        inc => '0', -- 1-bit input: increment / decrement tap delay input
----        load => '0', -- 1-bit input: load delay_value input
----        rst => s_idelay_ctrl_rdy_data_global -- 1-bit input: asynchronous reset to the delay_value
----);

----s_idelay3_load_lg(i) <= s_idelay3_load_channel(i);
----s_idelay3_load_hg(i) <= s_idelay3_load_channel(i);

--s_data_lg_delayed(i)<=s_data_lg(i);
----i_idelaye3_data_lg : idelaye3
----    generic map (
----        SIM_DEVICE => "ULTRASCALE_PLUS",
----        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
----        delay_format => "count", -- units of the delay_value (time, count)
----        delay_src => "idatain", -- delay input (idatain, datain)
----        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
----        delay_value => 0, -- input delay value setting
----        is_clk_inverted => '0', -- optional inversion for clk
----        is_rst_inverted => '0', -- optional inversion for rst
----        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
----        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
----        )
----        port map (
----        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
----        cntvalueout => open,--s_idelay_count_out_lg(i), -- 9-bit output: counter value output
----        dataout => s_data_lg_delayed(i), -- 1-bit output: delayed data output
----        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
----        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
----        ce => '0', -- 1-bit input: active high enable increment/decrement input
----        clk => p_clknet_in.clk280, -- 1-bit input: clock input
----        cntvaluein => s_idelay_count_in_lg(i), -- 9-bit input: counter value input
----        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
----        en_vtc => s_idelay_en_vtc_lg(i),--,'1', -- 1-bit input: keep delay constant over vt
----        idatain => s_data_lg(i), -- 1-bit input: data input from the logic
----        inc => '0', -- 1-bit input: increment / decrement tap delay input
----        load => s_idelay3_load_lg(i), -- 1-bit input: load delay_value input
----        rst => s_idelay_ctrl_reset_data_global_buf -- 1-bit input: asynchronous reset to the delay_value
----);


--s_data_hg_delayed(i)<=s_data_hg(i);
----i_idelaye3_data_hg : idelaye3
----    generic map (
----        SIM_DEVICE => "ULTRASCALE_PLUS",
----        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
----        delay_format => "count", -- units of the delay_value (time, count)
----        delay_src => "idatain", -- delay input (idatain, datain)
----        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
----        delay_value => 0, -- input delay value setting
----        is_clk_inverted => '0', -- optional inversion for clk
----        is_rst_inverted => '0', -- optional inversion for rst
----        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
----        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
----    )
----        port map (
----        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
----        cntvalueout => open,--s_idelay_count_out_hg(i), -- 9-bit output: counter value output
----        dataout => s_data_hg_delayed(i), -- 1-bit output: delayed data output
----        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
----        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
----        ce => '0', -- 1-bit input: active high enable increment/decrement input
----        clk => p_clknet_in.clk280, -- 1-bit input: clock input
----        cntvaluein => s_idelay_count_in_hg(i), -- 9-bit input: counter value input
----        datain => '0', --s_data_hg(i), -- 1-bit input: data input from the iobuf
----        en_vtc => s_idelay_en_vtc_hg(i),--'1', -- 1-bit input: keep delay constant over vt
----        idatain => s_data_hg(i), -- 1-bit input: data input from the logic
----        inc => '0', -- 1-bit input: increment / decrement tap delay input
----        load => s_idelay3_load_hg(i), -- 1-bit input: load delay_value input
----        rst => s_idelay_ctrl_reset_data_global_buf -- 1-bit input: asynchronous reset to the delay_value
----);


--    IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_frameclk(i),             -- Buffer diff_p output
--        I  => p_adc_frameclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_frameclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
--        );

--    IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_bitclk_in(i),             -- Buffer diff_p output
--        I  => p_adc_bitclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_bitclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
--        );

--    clock_buffer : bufg
--        port map (
--            I => s_bitclk_in(i),
--            O => s_bitclk(i)
--        );

--end generate;

---- Generate ADC data input registers and output word mapping,
---- The output bits from each ADC is stored in four shift registers (two even and two odd).

--gen_adc_input_gen: for v_adc in 0 to 5 generate

--        sr_LG : process(s_bitclk(v_adc)) -- Odd data bits clocked in on rising edge of adc clocks 
--        begin
--            if rising_edge (s_bitclk(v_adc)) then
--                s_readout_sr_lg_odd(v_adc) <= s_readout_sr_lg_odd(v_adc)(12 downto 0) & s_data_lg_delayed(v_adc);
--                s_readout_sr_hg_odd(v_adc) <= s_readout_sr_hg_odd(v_adc)(12 downto 0) & s_data_hg_delayed(v_adc);
--                --s_frame_clk_odd(v_adc) <= s_frame_clk_odd(v_adc)(12 downto 0) & s_frameclk(v_adc);
                
--            end if;
--        end process; -- sr_odd 

--        sr_HG : process(s_bitclk(v_adc)) -- Even data bits clocked in on rising edge of adc clocks 
--        begin
--            if falling_edge (s_bitclk(v_adc)) then
--                s_readout_sr_lg_even(v_adc) <= s_readout_sr_lg_even(v_adc)(12 downto 0) & s_data_lg_delayed(v_adc);
--                s_readout_sr_hg_even(v_adc) <= s_readout_sr_hg_even(v_adc)(12 downto 0) & s_data_hg_delayed(v_adc);
--                --s_frame_clk_even(v_adc) <= s_frame_clk_even(v_adc)(12 downto 0) & s_frameclk(v_adc);
--            end if;
--        end process; -- sr_even 

--        -- Assemble the bits into contiguous 14-bit words 
        
--        s_adc_input_hg_temp(v_adc) <=
--                                        s_readout_sr_hg_odd(v_adc)(13) & s_readout_sr_hg_even(v_adc)(13) &
--                                        s_readout_sr_hg_odd(v_adc)(12) & s_readout_sr_hg_even(v_adc)(12) &
--                                        s_readout_sr_hg_odd(v_adc)(11) & s_readout_sr_hg_even(v_adc)(11) &
--                                        s_readout_sr_hg_odd(v_adc)(10) & s_readout_sr_hg_even(v_adc)(10) &
--                                        s_readout_sr_hg_odd(v_adc)(9) & s_readout_sr_hg_even(v_adc)(9) & 
--                                        s_readout_sr_hg_odd(v_adc)(8) & s_readout_sr_hg_even(v_adc)(8) &
--                                        s_readout_sr_hg_odd(v_adc)(7) & s_readout_sr_hg_even(v_adc)(7) & 
--                                        s_readout_sr_hg_odd(v_adc)(6) & s_readout_sr_hg_even(v_adc)(6) &
--                                        s_readout_sr_hg_odd(v_adc)(5) & s_readout_sr_hg_even(v_adc)(5) &
--                                        s_readout_sr_hg_odd(v_adc)(4) & s_readout_sr_hg_even(v_adc)(4) &
--                                        s_readout_sr_hg_odd(v_adc)(3) & s_readout_sr_hg_even(v_adc)(3) &
--                                        s_readout_sr_hg_odd(v_adc)(2) & s_readout_sr_hg_even(v_adc)(2) &
--                                        s_readout_sr_hg_odd(v_adc)(1) & s_readout_sr_hg_even(v_adc)(1) &
--                                        s_readout_sr_hg_odd(v_adc)(0)   & s_readout_sr_hg_even(v_adc)(0);

--        s_adc_input_lg_temp(v_adc) <=
--                                        s_readout_sr_lg_odd(v_adc)(13) & s_readout_sr_lg_even(v_adc)(13) &
--                                        s_readout_sr_lg_odd(v_adc)(12) & s_readout_sr_lg_even(v_adc)(12) &
--                                        s_readout_sr_lg_odd(v_adc)(11) & s_readout_sr_lg_even(v_adc)(11) &
--                                        s_readout_sr_lg_odd(v_adc)(10) & s_readout_sr_lg_even(v_adc)(10) &
--                                        s_readout_sr_lg_odd(v_adc)(9) & s_readout_sr_lg_even(v_adc)(9) &
--                                        s_readout_sr_lg_odd(v_adc)(8) & s_readout_sr_lg_even(v_adc)(8) &
--                                        s_readout_sr_lg_odd(v_adc)(7) & s_readout_sr_lg_even(v_adc)(7) &
--                                        s_readout_sr_lg_odd(v_adc)(6) & s_readout_sr_lg_even(v_adc)(6) &
--                                        s_readout_sr_lg_odd(v_adc)(5) & s_readout_sr_lg_even(v_adc)(5) &
--                                        s_readout_sr_lg_odd(v_adc)(4) & s_readout_sr_lg_even(v_adc)(4) &
--                                        s_readout_sr_lg_odd(v_adc)(3) & s_readout_sr_lg_even(v_adc)(3) &
--                                        s_readout_sr_lg_odd(v_adc)(2) & s_readout_sr_lg_even(v_adc)(2) &
--                                        s_readout_sr_lg_odd(v_adc)(1) & s_readout_sr_lg_even(v_adc)(1) &
--                                        s_readout_sr_lg_odd(v_adc)(0)   & s_readout_sr_lg_even(v_adc)(0);
----        s_frame_clk_temp(v_adc) <=
----                                        s_frame_clk_odd(v_adc)(13) & s_frame_clk_even(v_adc)(13) &
----                                        s_frame_clk_odd(v_adc)(12) & s_frame_clk_even(v_adc)(12) &
----                                        s_frame_clk_odd(v_adc)(11) & s_frame_clk_even(v_adc)(11) &
----                                        s_frame_clk_odd(v_adc)(10) & s_frame_clk_even(v_adc)(10) &
----                                        s_frame_clk_odd(v_adc)(9) & s_frame_clk_even(v_adc)(9) &
----                                        s_frame_clk_odd(v_adc)(8) & s_frame_clk_even(v_adc)(7) &
----                                        s_frame_clk_odd(v_adc)(7) & s_frame_clk_even(v_adc)(6) &
----                                        s_frame_clk_odd(v_adc)(6) & s_frame_clk_even(v_adc)(5) &
----                                        s_frame_clk_odd(v_adc)(5) & s_frame_clk_even(v_adc)(4) &
----                                        s_frame_clk_odd(v_adc)(4) & s_frame_clk_even(v_adc)(3) &
----                                        s_frame_clk_odd(v_adc)(2) & s_frame_clk_even(v_adc)(2) &
----                                        s_frame_clk_odd(v_adc)(1) & s_frame_clk_even(v_adc)(1) &
----                                        s_frame_clk_odd(v_adc)(0) & s_frame_clk_even(v_adc)(0);
                                        
                                        

--        frame_capture : process(s_bitclk(v_adc)) -- Capture ADC words synchronous to ADC (bit) clock
--            type t_frame_state is (low, high);
--            variable v_frame : t_frame_state := low;
--            variable v_counter : integer :=0;
--        begin
--            if rising_edge(s_bitclk(v_adc)) then
--                case v_frame is
--                    when low =>
--                        if s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
--                            s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)(to_integer(unsigned(s_bitslip_hg(v_adc))) + (c_adc_bit_number-1)  downto (to_integer(unsigned(s_bitslip_hg(v_adc)))) );
--                            s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(to_integer(unsigned(s_bitslip_lg(v_adc))) + (c_adc_bit_number-1)  downto (to_integer(unsigned(s_bitslip_lg(v_adc)))));
--                            v_frame := high;
--                        end if;
--                    when high =>
--                        if s_frameclk(v_adc) = '0' then
--                            v_frame := low;
--                        end if;
--                end case;
--            end if; 
--        end process;  -- frame capture

--end generate;

--        bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
--        begin
--            if falling_edge(p_clknet_in.clk40) then
--                s_adc_word <=  s_adc_output_word;
--            end if;
--        end process;



--end Behavioral;