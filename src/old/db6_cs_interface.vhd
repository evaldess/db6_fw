----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
--           Sam Silverstein
--           Ivan Pogrebnyak
--           Nikolay Shalanda
-- 
-- Create Date: 09/28/2018 04:35:34 PM
-- Design Name: 
-- Module Name: db6_cs_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity db6_cs_interface is
  port (
  p_clk40_in       : in std_logic;
  p_cs_nreq_p_out, p_cs_nreq_n_out : out std_logic;
  p_cs_miso_p_out, p_cs_miso_n_out : out std_logic;
  p_cs_mosi_p_in, p_cs_mosi_n_in : in  std_logic;
  p_cs_sclk_p_in, p_cs_sclk_n_in : in  std_logic;
  
  p_timestamp_out : out std_logic_vector (31 downto 0);
  p_status_out    : out std_logic_vector (31 downto 0); -- from cs
  p_command_in   : in  std_logic_vector (31 downto 0) -- to cs

);
end db6_cs_interface;

architecture Behavioral of db6_cs_interface is

signal s_cs_nreq, s_cs_miso, s_cs_mosi, s_cs_sclk : std_logic;
signal s_cs_mosi_d, s_cs_sclk_d : std_logic;
signal s_cs_mosi_fifo, s_cs_sclk_fifo : std_logic_vector(3 downto 0);

signal s_status_reg  : std_logic_vector(31 downto 0) := x"00000000"; -- from cs
signal s_command_reg : std_logic_vector(31 downto 0) := x"00000000"; -- to cs
  
signal s_spi_cnt : integer :=0 ;
signal s_timeout_cnt : integer :=0;
constant c_timeout : integer := 8192;

type t_sm_spi_driver is ( st_idle, st_receive, st_send, st_finish );
signal s_sm_spi_driver : t_sm_spi_driver := st_idle;

signal s_busy_flag : std_logic := '0';
signal s_nreq : std_logic := '1';

signal s_counter : integer :=0;
signal s_t_div : unsigned(10 downto 0) := (others => '0');

signal s_enable_buff,s_enable : std_logic := '0';

signal s_cs_nreq_p_out, s_cs_nreq_n_out :  std_logic;
signal s_cs_miso_p_out, s_cs_miso_n_out :  std_logic;
--signal s_cs_mosi_p_in, s_cs_mosi_n_in :   std_logic;
--signal s_cs_sclk_p_in, s_cs_sclk_n_in :   std_logic;

begin


--    i_cs_lvds_nreq : obufds
--    generic map (IOSTANDARD => "LVDS_25")
--    port map ( i => s_cs_nreq, o => s_cs_nreq_p_out, ob => s_cs_nreq_n_out );
--    i_cs_nreq_p : obuf
--    generic map (IOSTANDARD => "lvcmos25")
--    port map ( i => s_cs_nreq_p_out, o => p_cs_nreq_p_out);
--    i_cs_nreq_n : obuf
--    generic map (IOSTANDARD => "lvcmos25")
--    port map ( i => s_cs_nreq_n_out, o => p_cs_nreq_n_out);
    --uggly but no other way
    p_cs_nreq_p_out<=s_cs_nreq;
    p_cs_nreq_n_out<=not s_cs_nreq;
    
--    i_cs_lvds_miso : obufds
--    generic map (IOSTANDARD => "LVDS_25")
--    port map ( i => s_cs_miso, o => s_cs_miso_p_out, ob => s_cs_miso_n_out );
--    i_cs_miso_p : obuf
--    generic map (IOSTANDARD => "lvcmos25")
--    port map ( i => s_cs_miso_p_out, o => p_cs_miso_p_out);
--    i_cs_miso_n : obuf
--    generic map (IOSTANDARD => "lvcmos25")
--    port map ( i => s_cs_miso_n_out, o => p_cs_miso_n_out);
    --ugly but no other way
    p_cs_miso_p_out<=s_cs_miso;
    p_cs_miso_n_out<=not s_cs_miso;
    
    i_cs_lvds_mosi : ibufds
    generic map (IOSTANDARD => "LVDS_25")
    port map ( i => p_cs_mosi_p_in, ib => p_cs_mosi_n_in, o => s_cs_mosi );
    i_cs_lvds_sclk : ibufds
    generic map (IOSTANDARD => "LVDS_25")
    port map ( i => p_cs_sclk_p_in, ib => p_cs_sclk_n_in, o => s_cs_sclk );
    
    --nreq
    s_cs_nreq <= s_nreq;
    
    --handling of spikes on input signals
    s_cs_mosi_d <= s_cs_mosi_fifo(3);
    s_cs_sclk_d <= s_cs_sclk_fifo(3);

    proc_anti_spike: process(p_clk40_in)
    variable v_index_mosi : unsigned(3 downto 0) := (others => '0');
    variable v_index_sclk : unsigned(3 downto 0) := (others => '0');
    begin
        if rising_edge(p_clk40_in) then
            v_index_mosi := v_index_mosi + 1;
            v_index_sclk :=v_index_sclk + 1;
            
            if (s_cs_mosi xor s_cs_mosi_fifo(3)) = '1' then
                s_cs_mosi_fifo <= std_logic_vector(v_index_mosi);
            else
                s_cs_mosi_fifo(1 downto 0) <= (others => '0');
                v_index_mosi(1 downto 0) := (others => '0');
            end if;

            if (s_cs_sclk xor s_cs_sclk_fifo(3)) = '1' then
                s_cs_sclk_fifo <= std_logic_vector(v_index_sclk);
            else
                s_cs_sclk_fifo(1 downto 0) <= (others => '0');
                v_index_sclk(1 downto 0) := (others => '0');
            end if;
        
        end if;

    end process;
  
  
  
  
    proc_spi_bus_write: process(s_cs_sclk_d)
    begin
        if rising_edge(s_cs_sclk_d) then
        s_cs_miso <= s_command_reg(31 - s_spi_cnt);
        
            if (s_sm_spi_driver = st_idle) then
                s_spi_cnt <= 1;
            else
                s_spi_cnt <= s_spi_cnt + 1;
            end if;
        
        end if;
    end process;

    proc_spi_bus_read: process(s_cs_sclk_d)
    begin
        if falling_edge(s_cs_sclk_d) then
        
                s_status_reg(0) <= s_cs_mosi_d;
                s_status_reg(31 downto 1) <= s_status_reg(30 downto 0);
        
        end if;
    end process;
  


    proc_spi_driver: process(p_clk40_in)
    begin
        if rising_edge(p_clk40_in) then
        
            if s_cs_sclk_d = '1' then
                s_busy_flag <= '1';
            elsif (s_timeout_cnt > c_timeout) or (s_spi_cnt = 0) then
                s_busy_flag <= '0';
            end if;
            
            case s_sm_spi_driver is
                when st_idle =>
                    s_nreq <= '1';
                    if s_busy_flag = '1' then
                        s_sm_spi_driver <= st_receive;
                    elsif not (s_command_reg = p_command_in) then
                        s_command_reg <= p_command_in;
                        s_sm_spi_driver <= st_send;
                    end if;
                
                when st_send =>
                    s_nreq <= '0';
                        if (s_spi_cnt = 1) then
                            s_sm_spi_driver <= st_receive;
                        end if;
                
                when st_receive => 
                
                    if s_busy_flag = '0' then
                        s_sm_spi_driver <= st_finish;
                    end if;
                
                when st_finish =>
                
                    if (s_nreq = '0') then -- check code
                        if (s_status_reg = x"aa933955") then
                            p_status_out(31) <= '0';
                        else
                            p_status_out(31) <= '1';
                        end if;
                    else
                        p_status_out(31) <= '0';
                    end if;
                    
                    s_nreq <= '1';
                    p_status_out(30 downto 0) <= s_status_reg(30 downto 0);
                    s_sm_spi_driver <= st_idle;
                    
                    --for i in 0 to 31 loop
                    p_timestamp_out <= std_logic_vector(to_unsigned(s_counter,32));
                    --end loop;
                
            end case;
        
            if (s_sm_spi_driver = st_send) or (s_sm_spi_driver = st_receive) then
                if (s_timeout_cnt > c_timeout) then
                    s_timeout_cnt <= 0;
                    s_sm_spi_driver <= st_idle;
                elsif s_cs_sclk_d = '1' then
                    s_timeout_cnt <= 0;
                else
                    s_timeout_cnt <= s_timeout_cnt + 1;
                end if;
            end if;
        
        end if;
    end process;


    proc_clock_div: process(p_clk40_in)
    begin
        if rising_edge(p_clk40_in) then
            s_t_div <= s_t_div + 1;
            
            s_enable <= s_t_div(s_t_div'length-1);
            s_enable_buff <= s_enable;
            
            if (s_enable_buff = '0'	and s_enable='1') then
                s_counter <= s_counter + 1;
            end if;
        end if;
    
    end process;
  

end Behavioral;
