
-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.vendor_specific_gbt_bank_package.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity gbt_tx_gearbox_latopt is
   port (
  
      -- Reset:
      TX_RESET_I                                : in  std_logic;
  
      -- Clocks:
      TX_WORDCLK_I                              : in  std_logic;
      TX_FRAMECLK_I                             : in  std_logic;

      -- Control --
      TX_MGT_READY_I                            : in  std_logic;

		-- Status  --
		TX_GEARBOX_READY_O								: out std_logic;
		TX_PHALIGNED_O										: out std_logic;
		TX_PHCOMPUTED_O									: out std_logic;

      -- Frame & Word --
      TX_FRAME_I                                : in  std_logic_vector(239 downto 0);
      TX_WORD_O                                 : out std_logic_vector(79 downto 0)
   
   );
end gbt_tx_gearbox_latopt;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of gbt_tx_gearbox_latopt is

   --================================ Signal Declarations ================================--

   signal txFrame_from_frameInverter            : std_logic_vector (239 downto 0);
      
   signal txMgtReady_r2                         : std_logic;  
   signal txMgtReady_r                          : std_logic;  
   signal gearboxSyncReset                      : std_logic;  
   signal phase_aligned                         : std_logic := '0';
  
	--Monitoring
	signal txFrame_from_frameInverter_for_mon		: std_logic_vector (239 downto 0);
	signal txFrame_built_from_word					: std_logic_vector (239 downto 0);
	
	signal gearbox_address : integer range 0 to 3 := 0;
	
   --=====================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== User Logic =====================================--   
   
   --==============--
   -- Common logic --
   --==============--
   
   -- Comment: Bits are inverted to transmit the MSB first on the MGT.
   
   frameInverter: for i in 0 to 119 generate
      txFrame_from_frameInverter(i)             <= TX_FRAME_I(119-i);
      txFrame_from_frameInverter(120+i)         <= TX_FRAME_I(239-i);
   end generate;
   
  -- Status signals
   
  gearboxSyncReset <= TX_RESET_I;
   
  TX_GEARBOX_READY_O	<= not(gearboxSyncReset) and phase_aligned;
  TX_PHALIGNED_O     <= phase_aligned;
  
   --=====================--
   -- Word width (80 Bit) --
   --=====================--
   
      gbLatOpt80b: process(gearboxSyncReset, TX_WORDCLK_I)

      begin
         if rising_edge(TX_WORDCLK_I) then
            if gearboxSyncReset = '1' then
                TX_WORD_O                           <= (others => '0');
                phase_aligned                       <= '0';
                gearbox_address                     <= 0;
				TX_PHCOMPUTED_O						<= '0';
            else
                case gearbox_address is
                   when 0 =>					
                      TX_WORD_O                     		 <= txFrame_from_frameInverter( 79 downto  0);
                      -- Monitoring
                      TX_PHCOMPUTED_O	<= '0';
                      txFrame_from_frameInverter_for_mon	 <= txFrame_from_frameInverter;
                      txFrame_built_from_word(79 downto 0)   <= txFrame_from_frameInverter( 79 downto  0);
                      gearbox_address                       		 <= 1;
                   when 1 => 
                      TX_WORD_O                              <= txFrame_from_frameInverter( 159 downto  80);
                      -- Monitoring
                      TX_PHCOMPUTED_O	<= '0';
                      txFrame_built_from_word(159 downto 80) <= txFrame_from_frameInverter(159 downto 80);
                      gearbox_address                        <=2;
                   when 2 =>                
                      TX_WORD_O                             <= txFrame_from_frameInverter(239 downto  160);
                      TX_PHCOMPUTED_O	                    <= '1';
                      if (txFrame_built_from_word(159 downto 0) = txFrame_from_frameInverter_for_mon(159 downto 0)) then
                          phase_aligned <= '1';
                          gearbox_address                           <= 0;
                      else
                          phase_aligned <= '0';
                          gearbox_address                           <= 3;
                      end if;
                    when others =>     -- Not aligned....send a dummy 80 bit word to shift by one 120 MHz cycle
                        TX_WORD_O <= x"aaaaa55555aaaaa55555";
                        gearbox_address                             <= 0; -- Start over and try again 
                end case;
            end if; -- reset condition
         end if; -- clock edge
      end process;
   
   --=====================================================================================--
end behavioral;
