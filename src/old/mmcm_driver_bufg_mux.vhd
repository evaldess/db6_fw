----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes
-- 
-- Create Date: 09/08/2018 02:11:43 PM
-- Design Name: 
-- Module Name: mmcm_driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: constrols phase shifting through mmcmc drp
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- from https://www.xilinx.com/support/documentation/application_notes/xapp888_7Series_DynamicRecon.pdf
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity mmcm_driver_bufg_mux is
generic
    (
		g_number_of_mmcms         : integer := 2
     );
    Port ( 
           p_driver_clk_in : in std_logic;
           p_sm_clk_in : in std_logic;
           p_clkin1_in : in std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_clkin2_in : in std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_clk_in_sel_in : in std_logic;
           p_master_reset_in : in std_logic;
           p_mmcm_clk_control_in : in t_mmcm_clk_control_array;
           p_clk40_out : out std_logic_vector((g_number_of_mmcms-1) downto 0);
           --p_clk560_out : out std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_locked_out : out std_logic;
           p_leds_out : out std_logic_vector(3 downto 0)
           );
end mmcm_driver_bufg_mux;

architecture Behavioral of mmcm_driver_bufg_mux is


--https://www.xilinx.com/support/documentation/application_notes/xapp888_7Series_DynamicRecon.pdf


constant c_number_of_mmcms : integer := g_number_of_mmcms;


begin

p_locked_out <= not p_master_reset_in;
--p_clk560_out<= (others=>'0');

p_leds_out<=(not p_master_reset_in)& p_clk_in_sel_in & "00";

gen_mmcms : for i in 0 to (c_number_of_mmcms-1) generate

   
   -- https://www.xilinx.com/support/documentation/ip_documentation/clk_wiz/v6_0/pg065-clk-wiz.pdf page 10
   --'1' primary clock, '0' secundary clock
   i_BUFGMUX_CTRL : BUFGMUX_CTRL
    port map (
    O => p_clk40_out(i), -- 1-bit output: Clock output
    I0 => p_clkin2_in(i), -- 1-bit input: Clock input (S=0)
    I1 => p_clkin1_in(i), -- 1-bit input: Clock input (S=1)
    S => p_clk_in_sel_in -- 1-bit input: Clock select
);

--p_clk40_out(0) <= p_clkin1_in(0);
--p_clk40_out(1) <= p_clkin1_in(1);
   


end generate;
    
    




--debug
--i_vio_mmcm_control_debug : vio_mmcm_control_debug
--  PORT MAP (
--    clk => p_driver_clk_in,
--    probe_in0(0) => s_locked(0),
--    probe_in1(0) => s_drdy(0),
--    probe_in2 => s_mmcm_clk0_reg_read_debug(0)(0),
--    probe_in3 => s_mmcm_clk0_reg_read_debug(0)(1),
--    probe_out0 => s_enable_vio_debug_mode,
--    probe_out1(0) => s_trigger_read_operation,
--    probe_out2(0) => s_trigger_write_operation,
--    probe_out4 => s_mmcm_clk0_reg_write_debug(0)(0),
--    probe_out3 => s_mmcm_clk0_reg_write_debug(0)(1)
--  );


--i_ila_mmcm_control_debug : ila_mmcm_control_debug
--PORT MAP (
--	clk => p_driver_clk_in,



--	probe0(0) => s_dwe(0), 
--	probe1(0) => s_den(0), 
--	probe2(0) => s_drdy(0), 
--	probe3 => s_daddr(0), 
--	probe4 => s_din(0), 
--	probe5 => s_dout(0), 
--	probe6(0) => s_trigger_read_operation,
--	probe7(0) => s_trigger_write_operation
--);


end Behavioral;

