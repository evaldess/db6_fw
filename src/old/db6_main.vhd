----------------------------------------------------------------------------------
-- Company: 
-- Engineer:Eduardo Valdes
--          Sam Silverstein
--          Steffen Muschter
--          Henrik Åekrstedt
--          Alberto Valero
--          Fernando Carrio 
-- 
-- Create Date: 09/10/2018 11:55:23 AM
-- Design Name: 
-- Module Name: db6_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Daughterboard  revision 5 main user module
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



-- common libraries --
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_misc.all;
--use ieee.STD_LOGIC_ARITH.all;

use ieee.std_logic_arith.CONV_STD_LOGIC_VECTOR;

-- XILINX libraries --
library UNISIM;
use UNISIM.VCOMPONENTS.all;
--user defined libraries --
--location: userfiles/setup_design_package.vhd --
library tilecal;
use tilecal.db6_design_package.all;

--library adc_lib;
--use adc_lib.all;


entity db6_main is
  port (
    p_db_side_in             : in    std_logic_vector(1 downto 0);
    p_master_reset_in        : in    std_logic;
    p_adc_config_reset_in     : in    std_logic;
    p_adc_readout_reset_in        : in    std_logic;
    p_adc_readout_initialized_out   : out std_logic;
    p_adc_config_done_out   : out    std_logic;
    
    p_clknet_in              : in    t_db_clock_network;
    p_configbus_clknet       : in t_configbus_clock_network;
    p_db_reg_rx_in       : in    t_db_reg_rx;
    p_tdo_in			: in    std_logic;
    p_gbt_tx_data_out : out t_gbt_tx_data;

    --p_header_inout          : inout std_logic_vector(23 downto 0);
    p_xadc_voltages        : in    t_db_xadc_voltages(18 downto 0);
    --p_xadc_address_lut_index_out       : out   std_logic_vector(4 downto 0);
    p_adc_data1_n_in         : in    std_logic_vector(5 downto 0);
    p_adc_data1_p_in         : in    std_logic_vector(5 downto 0);
    p_adc_data0_n_in         : in    std_logic_vector(5 downto 0);
    p_adc_data0_p_in         : in    std_logic_vector(5 downto 0);
    p_adc_frameclk_n_in         : in    std_logic_vector(5 downto 0);
    p_adc_frameclk_p_in         : in    std_logic_vector(5 downto 0);
    p_adc_bitclk_n_in         : in    std_logic_vector(5 downto 0);
    p_adc_bitclk_p_in         : in    std_logic_vector(5 downto 0);
    


      p_sdata_p_in      : in    std_logic_vector(1 downto 0);
      p_sdata_n_in      : in    std_logic_vector(1 downto 0);
      p_sdata_out       : out   std_logic_vector(1 downto 0);
      p_tpl_p_out            : out   std_logic_vector(1 downto 0);
      p_tpl_n_out            : out   std_logic_vector(1 downto 0);
      p_sclk_out           : out   std_logic_vector(1 downto 0);
      p_tph_p_out             : out   std_logic_vector(1 downto 0);
      p_tph_n_out             : out   std_logic_vector(1 downto 0);
      p_ssel_p_out            : out   std_logic_vector(1 downto 0);
      p_ssel_n_out            : out   std_logic_vector(1 downto 0);

    -- connect hv_opto
--    hv_enable  : out std_logic_vector(5 downto 0);
--    spi_mosi_p : out std_logic;
--    spi_mosi_n : out std_logic;
--    spi_miso_p : in  std_logic;
--    spi_miso_n : in  std_logic;
--    spi_sclk_p : out std_logic;
--    spi_sclk_n : out std_logic;
--    spi_ssn_p  : out std_logic_vector(5 downto 0);
--    spi_ssn_n  : out std_logic_vector(5 downto 0);
--    trigger_o  : out std_logic;
    
    -- cs header for lvds signals
    p_cs_nreq_p_out, p_cs_nreq_n_out : out std_logic;
    p_cs_miso_p_out, p_cs_miso_n_out : out std_logic;
    p_cs_mosi_p_in, p_cs_mosi_n_in : in  std_logic;
    p_cs_sclk_p_in, p_cs_sclk_n_in : in  std_logic;
    
    p_pgood_in             : in    std_logic_vector(3 downto 0);
	--debug
	p_leds_adc_config_out  : out   std_logic_vector(3 downto 0);
	p_leds_adc_readout_out  : out   std_logic_vector(3 downto 0);
	p_leds_out             : out   std_logic_vector(3 downto 0)
    );

end db6_main;

architecture rtl of db6_main is

    -- xadc control
     signal s_xadc_sc_control	         : std_logic_vector(15 downto 0);
     signal p_xadc_address_lut_index_out : std_logic_vector(4 downto 0);
     signal s_db_xadc_values : t_db_xadc_data;
     signal s_db_xadc_voltages : t_db_xadc_voltages(18 downto 0);

    -- function to reverse_any_vector
    function f_reverse_std_logic_vector (a: in std_logic_vector)
        return std_logic_vector is
          variable result: std_logic_vector(a'range);
          alias aa: std_logic_vector(a'reverse_range) is a;
        begin
          for i in aa'range loop
            result(i) := aa(i);
          end loop;
          return result;
    end; 
	

---------------------------------------------------
-------------------- internal db signals
---------------------------------------------------

  --signal s_led_flag    : std_logic := '0';


---------------------------------------------------
-------------------- integrator signals
---------------------------------------------------
  signal s_intg_dfr_0_i : std_logic;      -- integrator fpga 0 signals
  signal s_intg_dck_0_i : std_logic;
  signal s_intg_do_0_i  : std_logic;

  signal s_intg_dfr_1_i : std_logic;      -- integrator fpga 1 signals
  signal s_intg_dck_1_i : std_logic;
  signal s_intg_do_1_i  : std_logic;

  signal s_gbt_integrator        : std_logic_vector (4 downto 0);
  signal s_integrator_config_reg : std_logic_vector(31 downto 0) := x"0000000a";  -- default read integrator every 10 orbits

-- outputs (used to be sent to chipscope)

	signal s_integ1dataout   :  std_logic_vector(15 downto 0);
	signal s_integ2dataout   :  std_logic_vector(15 downto 0);
	signal s_integ1eor	   :  std_logic;
	signal s_integ2eor	   :  std_logic;
	signal s_cntorbit_o      :  std_logic_vector (7 downto 0);

---------------------------------------------------
-------------------- spi interface to hvopto
---------------------------------------------------
  signal s_spi_wren_i        : std_logic;
  signal s_spi_wren_i_strobe : std_logic;
  signal s_spi_do_valid_o    : std_logic;
  signal s_spi_do_o          : std_logic_vector(15 downto 0);
  signal s_spi_ssel_o        : std_logic;
  signal s_spi_sck_o         : std_logic;
  signal s_spi_mosi_o        : std_logic;
  signal s_spi_miso_i        : std_logic;
  signal s_spi_rst_i         : std_logic;
  signal s_hv_enable_o       : std_logic;


---------------------------------------------------
-------------------- db mb communication
---------------------------------------------------
  signal s_ssel, s_sclk, s_sdata_in, s_sdata_out  : std_logic_vector(1 downto 0);
  --signal s_reset_dbtomb : std_logic := '0';
  signal s_outwd0       : std_logic_vector(17 downto 0);
  signal s_outwd1       : std_logic_vector(17 downto 0);
  signal s_mb_done_0_o : std_logic;
  signal s_mb_done_1_o : std_logic;
  signal s_mb_sm_init_state : std_logic_vector(3 downto 0);

	signal s_framealigndone        				: std_logic_vector(5 downto 0);

--    signal s_adc_data_out						:t_adc_data_type;
--	signal s_data_frame							:t_adc_data_type;
	
    signal s_adcs_config_done									:std_logic:='0';
    constant c_adc_registers_init : t_adc_registers :=  (x"10", x"00", x"b5", x"00", x"00");
    signal s_adc_registers: t_adc_registers := c_adc_registers_init;
    
--    signal s_idelay3_load_lg, s_idelay3_load_hg, s_idelay3_load_channel, s_idelay_en_vtc_lg, s_idelay_en_vtc_hg, s_idelay_en_vtc_channel, s_idelay_ctrl_reset_lg, s_idelay_ctrl_reset_hg   : std_logic_vector (5 downto 0);
--    signal s_idelay_count_in_lg, s_idelay_count_in_hg, s_idelay_count_out_lg,  s_idelay_count_out_hg : t_idelay_count;
--    signal s_bitslip_lg, s_bitslip_hg : t_bitslip;

  signal s_adc_readout                      : t_adc_readout;
  signal s_adc_readout_control : t_adc_readout_control;
  
  signal s_adc_register_config_from_configbus, s_adc_register_config_from_readout : t_adc_register_config := c_adc_register_init_config;

	
---------------------------------------------------
-------------------- srod db communication
---------------------------------------------------
  signal s_phase_in       : std_logic_vector(31 downto 0):=(others=>'0');
  
  signal s_fe_data        : std_logic_vector(31 downto 0);
  signal s_fe_command     : std_logic_vector(15 downto 0);
  signal s_adc_data       : std_logic_vector(31 downto 0);
  signal s_spi_data       : std_logic_vector(31 downto 0);
  signal s_integ_data_in  : std_logic_vector(31 downto 0);
  signal s_integ_data_out : std_logic_vector(31 downto 0);
  signal s_hv_command     : std_logic_vector(15 downto 0);
  signal s_sem_data_in    : std_logic_vector(31 downto 0);
  signal s_sem_data_out   : std_logic_vector(31 downto 0);
  signal s_temp_data      : std_logic_vector(31 downto 0);


---------------------------------------------------
-------------------- configuration and control registers
---------------------------------------------------
  
  signal s_db_reg_tx : t_db_reg_tx;
  
  --signal s_db_config_reg : t_config_reg_array;
  --signal s_db_config_reg_3 : std_logic_vector(31 downto 0);  --  reset register, mb(1), cmd_cnt(0)
  signal s_db_cis_config_reg : std_logic_vector(31 downto 0); -- cis control.

  signal s_gbt_tmr_count : integer range 0 to 1023 := 0;
  signal s_gbt_status : gbt_status;

  signal s_adc_mon_temp, s_bk_mon_temp : std_logic_vector(1 downto 0);


---------------------------------------------------
-------------------- high voltage (main_hv)
---------------------------------------------------

  signal s_hv_status_temp, s_hv_status  : std_logic_vector(31 downto 0);
  signal s_hv_command_in, s_hv_command_temp : std_logic_vector(31 downto 0);

---------------------------------------------------
-------------------- cs
---------------------------------------------------
signal s_cs_status, s_cs_command, s_cs_timestamp : std_logic_vector(31 downto 0);

---------------------------------------------------
-------------------- adc
---------------------------------------------------
  signal s_adc_readout_reset, s_adc_config_reset     : std_logic;
  
  signal s_cis_reset : std_logic;

  signal s_cis_enable     : std_logic                     := '0';
  signal s_cis_gain       : std_logic                     := '0';
  signal s_cis_bcid_charge       	: std_logic_vector(11 downto 0) := x"000";
  signal s_cis_bcid_discharge       : std_logic_vector(11 downto 0) := x"000";
  signal s_tphi           : std_logic_vector(1 downto 0);
  signal s_tpli           : std_logic_vector(1 downto 0);
  signal s_cis_tmr_count  : integer range 0 to 1023       := 0;
  signal s_cis_tmr_inject : std_logic_vector(1 downto 0)  := "00";
  signal s_cis_tmr_in     : std_logic_vector(11 downto 0);
  signal s_cis_tmr_out    : std_logic_vector(3 downto 0);
  
  signal s_adcs_config									:std_logic:='0';

---------------------------------------------------
-------------------- sem
---------------------------------------------------
    
  signal sem_fail : std_logic;
  signal sem_err, sem_cor, sem_ess : std_logic_vector(15 downto 0);
  signal s_sem_reset : std_logic;

   attribute keep : string;
   attribute keep of s_mb_sm_init_state : signal is "true";

COMPONENT ila_adc_readout

PORT (
	clk : IN STD_LOGIC;



	probe0 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe6 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe7 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe8 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe9 : IN STD_LOGIC_VECTOR(13 DOWNTO 0); 
	probe10 : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
	probe11 : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
	probe12 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe13 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe14 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe15 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe16 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe17 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	probe18 : IN STD_LOGIC_VECTOR(15 DOWNTO 0)
);
END COMPONENT  ;

begin  -- rtl

-- reset signals
 s_adc_readout_reset <= p_master_reset_in or p_adc_readout_reset_in or p_db_reg_rx_in(cfb_strobe_reg)(7);
 s_sem_reset <=p_master_reset_in or p_db_reg_rx_in(cfb_strobe_reg)(8);
 s_cis_reset <= p_master_reset_in or p_db_reg_rx_in(cfb_strobe_reg)(8);
 
  --######################--
  --## adc control      ##--
  --######################--



	i_db6_adc_interface: entity tilecal.db6_adc_interface_v3

  port map(   p_master_reset_in      => s_adc_readout_reset,
              --p_adc_mode_in          => p_db_reg_rx_in(cfb_db_control)(0),
              p_adc_readout_out =>   s_adc_readout,
              p_adc_readout_control_in => s_adc_readout_control,
              p_clknet_in             => p_clknet_in,
              p_adc_data0_p_in     => p_adc_data0_p_in,
              p_adc_data0_n_in     => p_adc_data0_n_in,
              p_adc_data1_p_in     => p_adc_data1_p_in,
              p_adc_data1_n_in     => p_adc_data1_n_in,
              p_adc_bitclk_p_in     => p_adc_bitclk_p_in,
              p_adc_bitclk_n_in     => p_adc_bitclk_n_in,
              p_adc_frameclk_p_in => p_adc_frameclk_p_in,
              p_adc_frameclk_n_in => p_adc_frameclk_n_in,
              
--                p_idelay_ctrl_reset_lg_in => s_idelay_ctrl_reset_lg,
--                p_idelay_ctrl_reset_hg_in =>  s_idelay_ctrl_reset_hg,
--                p_idelay_ctrl_reset_fc_in =>  (others => '0'),
--                p_idelay3_load_lg_in => s_idelay3_load_lg,
--                p_idelay3_load_hg_in => s_idelay3_load_hg,
--                p_idelay3_load_fc_in => (others => '0'),
--                p_idelay_en_vtc_lg_in => s_idelay_en_vtc_lg,
--                p_idelay_en_vtc_hg_in => s_idelay_en_vtc_hg,
--                p_idelay_en_vtc_fc_in => (others => '0'),
--                p_idelay_count_in_lg_in => s_idelay_count_in_lg,
--                p_idelay_count_in_hg_in => s_idelay_count_in_hg,
--                p_idelay_count_in_fc_in => (others=>(others => '0')),
--                p_idelay_count_in_lg_out => s_idelay_count_out_lg,
--                p_idelay_count_in_hg_out => s_idelay_count_out_hg,
--                p_idelay_count_in_fc_out => open,
--                p_bitslip_lg_in => s_bitslip_lg,
--                p_bitslip_hg_in => s_bitslip_hg,
--                p_channel_frame_missalignemt_out => s_channel_frame_missalignemt,
              
--                p_adc_data_out        => s_adc_data_out,
              
              p_debug_pin_out => open
  ); 

--        i_ila_adc_readout : ila_adc_readout
--        PORT MAP (
--            clk => p_clknet_in.clk40,
        
--            probe0 => s_adc_readout.lg_data(0), 
--            probe1 => s_adc_readout.hg_data(0),
--            probe2 => s_adc_readout.lg_data(1),
--            probe3 => s_adc_readout.hg_data(1),
--            probe4 => s_adc_readout.lg_data(2), 
--            probe5 => s_adc_readout.hg_data(2),
--            probe6 => s_adc_readout.lg_data(3),
--            probe7 => s_adc_readout.hg_data(3),
--            probe8 => s_adc_readout.lg_data(4),
--            probe9 => s_adc_readout.hg_data(4),
--            probe10 => s_adc_readout.lg_data(5),
--            probe11 => s_adc_readout.hg_data(5),
--            probe12 => s_adc_readout.channel_frame_missalignemt, --s_channel_frame_missalignemt
--            probe13 => s_adc_readout.channel_missed_bit_count(0)(15 downto 0),
--            probe14 => s_adc_readout.channel_missed_bit_count(0)(15 downto 0),
--            probe15 => s_adc_readout.channel_missed_bit_count(0)(15 downto 0),
--            probe16 => s_adc_readout.channel_missed_bit_count(0)(15 downto 0),
--            probe17 => s_adc_readout.channel_missed_bit_count(0)(15 downto 0),
--            probe18 => s_adc_readout.channel_missed_bit_count(0)(15 downto 0)
            
--        );
        s_adc_readout_control.db_side <= p_db_side_in;
        
        s_adc_readout_control.lg_idelay_count(0)<= p_db_reg_rx_in(adc_readout_idelay3_0)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(0)<= p_db_reg_rx_in(adc_readout_idelay3_0)(24 downto 16);
        s_adc_readout_control.lg_idelay_count(1)<= p_db_reg_rx_in(adc_readout_idelay3_1)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(1)<= p_db_reg_rx_in(adc_readout_idelay3_1)(24 downto 16);
        s_adc_readout_control.lg_idelay_count(2)<= p_db_reg_rx_in(adc_readout_idelay3_2)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(2)<= p_db_reg_rx_in(adc_readout_idelay3_2)(24 downto 16);
        s_adc_readout_control.lg_idelay_count(3)<= p_db_reg_rx_in(adc_readout_idelay3_3)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(3)<= p_db_reg_rx_in(adc_readout_idelay3_3)(24 downto 16);
        s_adc_readout_control.lg_idelay_count(4)<= p_db_reg_rx_in(adc_readout_idelay3_4)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(4)<= p_db_reg_rx_in(adc_readout_idelay3_4)(24 downto 16);
        s_adc_readout_control.lg_idelay_count(5)<= p_db_reg_rx_in(adc_readout_idelay3_5)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(5)<= p_db_reg_rx_in(adc_readout_idelay3_5)(24 downto 16);
        
        s_adc_readout_control.lg_idelay_ctrl_reset(0)<= p_db_reg_rx_in(adc_readout_idelay3_0)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(0)<= p_db_reg_rx_in(adc_readout_idelay3_0)(31);
        s_adc_readout_control.lg_idelay_ctrl_reset(1)<= p_db_reg_rx_in(adc_readout_idelay3_1)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(1)<= p_db_reg_rx_in(adc_readout_idelay3_1)(31);
        s_adc_readout_control.lg_idelay_ctrl_reset(2)<= p_db_reg_rx_in(adc_readout_idelay3_2)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(2)<= p_db_reg_rx_in(adc_readout_idelay3_2)(31);
        s_adc_readout_control.lg_idelay_ctrl_reset(3)<= p_db_reg_rx_in(adc_readout_idelay3_3)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(3)<= p_db_reg_rx_in(adc_readout_idelay3_3)(31);
        s_adc_readout_control.lg_idelay_ctrl_reset(4)<= p_db_reg_rx_in(adc_readout_idelay3_4)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(4)<= p_db_reg_rx_in(adc_readout_idelay3_4)(31);
        s_adc_readout_control.lg_idelay_ctrl_reset(5)<= p_db_reg_rx_in(adc_readout_idelay3_5)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(5)<= p_db_reg_rx_in(adc_readout_idelay3_5)(31);        
        
        s_adc_readout_control.lg_idelay_load(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(9);
        s_adc_readout_control.hg_idelay_load(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(25);
        s_adc_readout_control.lg_idelay_load(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(9);
        s_adc_readout_control.hg_idelay_load(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(25);
        s_adc_readout_control.lg_idelay_load(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(9);
        s_adc_readout_control.hg_idelay_load(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(25);
        s_adc_readout_control.lg_idelay_load(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(9);
        s_adc_readout_control.hg_idelay_load(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(25);
        s_adc_readout_control.lg_idelay_load(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(9);
        s_adc_readout_control.hg_idelay_load(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(25);
        s_adc_readout_control.lg_idelay_load(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(9);
        s_adc_readout_control.hg_idelay_load(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(25);
        
        s_adc_readout_control.lg_idelay_en_vtc(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(10);
        s_adc_readout_control.hg_idelay_en_vtc(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(26);
        s_adc_readout_control.lg_idelay_en_vtc(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(10);
        s_adc_readout_control.hg_idelay_en_vtc(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(26);
        s_adc_readout_control.lg_idelay_en_vtc(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(10);
        s_adc_readout_control.hg_idelay_en_vtc(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(26);
        s_adc_readout_control.lg_idelay_en_vtc(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(10);
        s_adc_readout_control.hg_idelay_en_vtc(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(26);
        s_adc_readout_control.lg_idelay_en_vtc(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(10);
        s_adc_readout_control.hg_idelay_en_vtc(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(26);
        s_adc_readout_control.lg_idelay_en_vtc(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(10);
        s_adc_readout_control.hg_idelay_en_vtc(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(26);
        
        s_adc_readout_control.lg_bitslip(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(14 downto 11);
        s_adc_readout_control.hg_bitslip(0)<=p_db_reg_rx_in(adc_readout_idelay3_0)(30 downto 27);
        s_adc_readout_control.lg_bitslip(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(14 downto 11);
        s_adc_readout_control.hg_bitslip(1)<=p_db_reg_rx_in(adc_readout_idelay3_1)(30 downto 27);
        s_adc_readout_control.lg_bitslip(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(14 downto 11);
        s_adc_readout_control.hg_bitslip(2)<=p_db_reg_rx_in(adc_readout_idelay3_2)(30 downto 27);
        s_adc_readout_control.lg_bitslip(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(14 downto 11);
        s_adc_readout_control.hg_bitslip(3)<=p_db_reg_rx_in(adc_readout_idelay3_3)(30 downto 27);
        s_adc_readout_control.lg_bitslip(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(14 downto 11);
        s_adc_readout_control.hg_bitslip(4)<=p_db_reg_rx_in(adc_readout_idelay3_4)(30 downto 27);
        s_adc_readout_control.lg_bitslip(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(14 downto 11);
        s_adc_readout_control.hg_bitslip(5)<=p_db_reg_rx_in(adc_readout_idelay3_5)(30 downto 27);


--	i_db6_adc_interface: entity tilecal.db6_adc_interface

--	port map(
--                p_clknet_in 			=> p_clknet_in,
--                p_db_reg_rx_in => p_db_reg_rx_in,
--				p_adc_data0_p_in 	=> p_adc_data0_p_in,
--				p_adc_data0_n_in 	=> p_adc_data0_n_in,
--				p_adc_data1_p_in 	=> p_adc_data1_p_in,
--				p_adc_data1_n_in 	=> p_adc_data1_n_in,
--				p_adc_bitclk_p_in 	=> p_adc_bitclk_p_in,
--				p_adc_bitclk_n_in 	=> p_adc_bitclk_n_in,
--				p_adc_frameclk_p_in => p_adc_frameclk_p_in,
--				p_adc_frameclk_n_in => p_adc_frameclk_n_in,
--				p_adc_data_out		=> s_adc_data_out
--	);

--  ######################--
--  ## SEM control      ##--
----  ######################--

--i_sem_interface : entity tilecal.sem_error_mgm

--port map(
--clknet => p_clknet_in,
--reset => s_sem_reset,
--fail => sem_fail,
--errors_tot => sem_err,
--errors_cor => sem_cor
--);

--hv_trigger_gen : process(clknet.clk40db)  -- not used at the moment...deprecated?
--begin
--	if rising_edge(clknet.clk40db) then
--		if to_integer(unsigned(data_out(0)(13 downto 2)))> 3000 then
--			trigger_o <= '1';
--		else
--			trigger_o <= '0';
--		end if;
--	end if;
--end process;

 -- ###################################--
 -- ## error management              ##--
 -- ###################################--

--  error_mgm : entity work.error_mgm
--    port map(
--      clk_in         => p_clknet_in.clk100,
--      error_comm_in  => s_sem_data_in,
--      error_comm_out => s_sem_data_out,
--      gbt_status_in  => s_gbt_status,
--      cis_tmr_count  => s_cis_tmr_count,
--      gbt_tmr_count  => s_gbt_tmr_count
--      );


  --###################################--
  --## cis control                   ##--
  --###################################--

	s_cis_enable 				<= s_db_cis_config_reg(0);
	s_cis_gain 				<= s_db_cis_config_reg(1);  
	s_cis_bcid_charge 		<= s_db_cis_config_reg(13 downto 2);  
	s_cis_bcid_discharge 	    <= s_db_cis_config_reg(25 downto 14);

gen_tph: for i in 0 to 1 generate
    i_tph_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "DIFF_HSTL_I_18")
        port map (
        O => p_tph_p_out(i), -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_tph_n_out(i), -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_tphi(i) -- 1-bit input: Buffer input
        );
end generate;

gen_tpl: for i in 0 to 1 generate
    i_tpl_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "DIFF_HSTL_I_18")
        port map (
        O => p_tpl_p_out(i), -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_tpl_n_out(i), -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_tpli(i) -- 1-bit input: Buffer input
        );
end generate;


  --################################--
  --## charge injection interface ##--
  --################################--
  
  i_db6_cis_interface : entity tilecal.db6_cis_interface 
    port map(
      p_master_reset_in      => s_cis_reset,	
      p_clk40_in      => p_clknet_in.clk40,
      p_cis_enable_in => s_cis_enable,
      p_cis_gain_in   => s_cis_gain,
      p_cis_bcid_charge_in      => s_cis_bcid_charge,
      p_cis_bcid_discharge_in   => s_cis_bcid_discharge,
      p_bcr_in =>       p_configbus_clknet.bcr,
      p_bc_number_in   => p_db_reg_rx_in(bc_number)(15 downto 0),
      p_tph_out        => s_tphi,
      p_tpl_out        => s_tpli
      );


  --###########################--
  --## gbt data mapping area ##--
  --###########################--

  --map registers to be transmitted to the ppr (legacy mapping)
  
  s_db_reg_tx.mb_w         		<= s_outwd1(15 downto 0) & s_outwd0(15 downto 0); --0
  
  proc_version_debug : process(p_db_reg_rx_in(cfb_db_debug)(31))
    begin
        if p_db_reg_rx_in(cfb_db_debug)(31) = '0' then
            s_db_reg_tx.db_version_w 		<= c_fw_version;
        else
            --s_db_reg_tx.db_version_w        <= p_db_side_in & p_db_reg_rx_in(adv_cfg_firmware_version)(29 downto 0);
        end if;
    end process;
    
  s_db_reg_tx.mb_0_w       		<= "00" & s_fe_data(23 downto 12) & s_outwd0;     --2 outwd: (17 .. 0)
  s_db_reg_tx.mb_1_w       		<= "00" & s_fe_data(23 downto 12) & s_outwd1;     --3
  s_db_reg_tx.db_reg3_w    		<= p_db_reg_rx_in(cfb_strobe_reg);							  --4
  s_db_reg_tx.db_temp_w    		<= s_temp_data;--5   -- 32-bit xadc sensor data
  s_db_reg_tx.db_sem_w     		<= s_sem_data_out;--6
  s_db_reg_tx.db_reg6_w    		<= s_hv_status;--7
  s_db_reg_tx.hv_w         		<= s_hv_status;--8
  s_db_reg_tx.cis_config_w    	<= s_db_cis_config_reg;									--9
  -- cs
  s_db_reg_tx.cs_t_w       <= s_cs_timestamp;
  s_db_reg_tx.cs_s_w       <= s_cs_status;
  
  --fix this two in cs and id module
  --gbt_reg_tx(cs_c_w)       <= gbt_reg_rx(cs_c_r);
  -- id ( read bytes and reverse them, page 4 =>  https://datasheets.maximintegrated.com/en/ds/ds28cm00.pdf)
  --s_gbt_reg_tx(db_serial_id_lsb) <= f_reverse_std_logic_vector(s_serial_id_array(3)) & f_reverse_std_logic_vector(s_serial_id_array(2)) & f_reverse_std_logic_vector(s_serial_id_array(1)) & f_reverse_std_logic_vector(s_serial_id_array(0)); 
  --s_gbt_reg_tx(db_serial_id_msb) <= f_reverse_std_logic_vector(s_serial_id_array(7)) & f_reverse_std_logic_vector(s_serial_id_array(6)) & f_reverse_std_logic_vector(s_serial_id_array(5)) & f_reverse_std_logic_vector(s_serial_id_array(4));
  
--read legacy configuration/control registers from configbus
  --s_xadc_sc_control 		<= s_db_config_reg(db_temp_r);
  --s_phase_in 					<= s_db_config_reg(cfb_mb_phase_config);
  s_db_cis_config_reg 		<= p_db_reg_rx_in(cfb_cis_config);  
  --s_db_config_reg_3 			<= p_db_reg_rx_in(cfb_strobe_reg); 
  s_integrator_config_reg   	<= p_db_reg_rx_in(cfb_integrator_interval);
  --s_hv_command_temp           <= p_db_reg_rx_in(cfb_hv_control);
  s_cs_command 				<= p_db_reg_rx_in(cfb_cs_command);
  
  --s_reset_dbtomb 				<= '1' when s_db_config_reg_3(0) = '1' else '0';
  --s_led_flag     				<= '1' when s_db_config_reg_3(0) = '1' else '0';

  s_adc_mon_temp <= ((s_framealigndone(0)or s_framealigndone(1)or s_framealigndone(2)) & (s_framealigndone(3)or s_framealigndone(4)or s_framealigndone(5)));
  s_bk_mon_temp <= s_mb_done_1_o & s_mb_done_0_o;
  
  
  
  --################################--
  --## gbt uplink word assembler  ##--
  --################################--

  i_db6_gbt_word_assembler : entity tilecal.db6_gbt_word_assembler
    --generic map(tmr_enable => false)
    port map(
      p_db_side_in      => p_db_side_in,
      p_clknet_in       => p_clknet_in,
      --p_clk40_in        => p_clknet_in.clk40,
      --p_clk80_in        => p_clknet_in.clk80,
      p_db_reg_rx_in    => p_db_reg_rx_in,
      --p_db_xadc_voltages_in => s_db_xadc_voltages, 
      p_bcr_in          => p_configbus_clknet.bcr,
      p_tdo_in          => p_tdo_in,                                        --rx --clk40 clock domain
      --gbt_data
      p_gbt_tx_data_out  => p_gbt_tx_data_out,
      p_db_reg_tx_in    => s_db_reg_tx,                                             --clk40 clock domain
      p_adc_data_in     => s_adc_readout,                                              --clk40 clock domain
      p_adc_mon_in    => s_adc_mon_temp,                                                --clk40 clock domain
      p_gbt_integrator_in => s_gbt_integrator,                              --clk40 clock domain
      p_bk_done_in        => s_bk_mon_temp                      --clk40 clock domain
      --tmr_cnt        => gbt_tmr_count
      );



 -- ###########################--
 -- ## temperature and xadc  ##--
 -- ###########################--

  xadc_ro_proc : process (p_clknet_in.clk40)
  variable v_adc_sc_control: integer :=0;
  begin
    
    if (rising_edge(p_clknet_in.clk40)) then
		s_temp_data(31 downto 16) <= p_pgood_in & std_logic_vector(to_unsigned(v_adc_sc_control, 12)); -- four pgood bits, plus 12b xadc readout address 
		s_temp_data(15 downto 0) <= s_db_xadc_voltages(v_adc_sc_control); --  15-bit value of readout data

		v_adc_sc_control := v_adc_sc_control+1;		
		if v_adc_sc_control = c_n_db_xadc_channels then
		  v_adc_sc_control := 0;
        end if;

    end if;
  end process xadc_ro_proc;



  --###########################--
  --## spi                   ##--
  --###########################--

--  spi_config : process (clknet.clk40db)
--  begin
--    if rising_edge(p_clknet_in.clk40) then

--      case s_spi_data(31 downto 16) is
--        when x"0000" => s_spi_wren_i <= '0';
--                        s_spi_rst_i <= '0';
--        when x"0001" => s_spi_wren_i <= '1';
--        when x"1000" => s_spi_rst_i  <= '1';
--        when others  => null;
--      end case;

--    end if;
--  end process spi_config;


  --###########################--
  --## integrator            ##--
  --###########################--

--  integrator_wrapper : entity work.integrator_wrapper
--    port map(
--      reset                 => s_master_reset,
--      clk40                 => s_clknet.clk40,
--      clk80                 => s_clknet.clk80,
--      integrator_config_reg => s_db_config_reg(cfb_integrator_interval),
--      gbt_integrator        => s_gbt_integrator,
--      endoforbit            => p_orbit_strobe_in,
--      i2c_sda               => bus_4_i2c_sda,
--      i2c_scl               => bus_4_i2c_scl,
--	  integ1dataout         => integ1dataout,
--      integ2dataout         => integ2dataout,
--	  integ1eor			    => integ1eor,
--	  integ2eor			    => integ2eor,
--	  bcnumber              => s_db_config_reg(bc_number)(15 downto 0),
--	  cntorbit_o 		    => s_cntorbit_o
--      );


--  --#########################################--
--  --## spi communication to hvopto section ##--
--  --#########################################--


--  spi_strobe_timing : entity work.strobe_timing
--    generic map (
--      counter_max => "001")
--    port map (
--      reset_in   => '0',
--      clk_in     => clknet.clk40db,
--      strobe_in  => spi_wren_i,
--      strobe_out => spi_wren_i_strobe);

--  spi_master_1 : entity work.spi_master
--    generic map (
--      n              => 16,
--      cpol           => '0',
--      cpha           => '0',
--      prefetch       => 2,
--      spi_2x_clk_div => 2)
--    port map (
--      sclk_i        => clknet.clk40db,
--      pclk_i        => clknet.clk40db,
--      rst_i         => spi_rst_i,
--      -- serial interface --
--      spi_ssel_o    => spi_ssel_o,
--      spi_sck_o     => spi_sck_o,
--      spi_mosi_o    => spi_mosi_o,
--      spi_miso_i    => spi_miso_i,
--      -- parallel interface --
--      di_req_o      => open,
--      di_i          => spi_data(15 downto 0),
--      wren_i        => spi_wren_i_strobe,
--      wr_ack_o      => open,
--      do_valid_o    => spi_do_valid_o,
--      do_o          => spi_do_o,
--      -- debug ports --
--      sck_ena_o     => open,
--      sck_ena_ce_o  => open,
--      do_transfer_o => open,
--      wren_o        => open,
--      rx_bit_reg_o  => open,
--      state_dbg_o   => open,
--      core_clk_o    => open,
--      core_n_clk_o  => open,
--      core_ce_o     => open,
--      core_n_ce_o   => open,
--      sh_reg_dbg_o  => open);


  --###########################--
  --## mb driver             ##--
  --###########################--
       

gen_ssel: for i in 0 to 1 generate
    i_ssel_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "LVDS")
        port map (
        O => p_ssel_p_out(i), -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_ssel_n_out(i), -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_ssel(i) -- 1-bit input: Buffer input
        );
end generate;

--gen_sdata_out: for i in 0 to 1 generate
--    i_sdata_out_OBUFDS : OBUFDS
--    generic map (IOSTANDARD => "LVDS")
--        port map (
--        O => p_sdata_p_out(i), -- 1-bit output: Diff_p output (connect directly to top-level port)
--        OB => p_sdata_n_out(i), -- 1-bit output: Diff_n output (connect directly to top-level port)
--        I => s_sdata_out(i) -- 1-bit input: Buffer input
--        );
--end generate;

--gen_sclk: for i in 0 to 1 generate
--    i_sclk_OBUFDS : OBUFDS
--    generic map (IOSTANDARD => "LVDS_18")
--        port map (
--        O => p_sclk_p_out(i), -- 1-bit output: Diff_p output (connect directly to top-level port)
--        OB => p_sclk_n_out(i), -- 1-bit output: Diff_n output (connect directly to top-level port)
--        I => s_sclk(i) -- 1-bit input: Buffer input
--        );
--end generate;

gen_sdata_in: for i in 0 to 1 generate
    i_sdata_in_iBUFDS : iBUFDS
    generic map (IOSTANDARD => "LVDS_18",
                DIFF_TERM => TRUE)
        port map (
        O => s_sdata_in(i), 
        I => p_sdata_p_in(i),
        Ib => p_sdata_n_in(i)
        );
end generate;

  i_db6_mb_driver : entity tilecal.db6_mb_driver
    port map (
    
        p_db_side_in     => p_db_side_in,
        p_master_reset_in => p_master_reset_in,
        p_clknet_in       => p_clknet_in,
        p_ssel_out        => s_ssel,
        p_sclk_out        => p_sclk_out,
        p_sdata_out      => p_sdata_out,
        p_sdata_in      => s_sdata_in,
        p_fe_data_in    => s_fe_data,
        p_outwd1_out    => s_outwd1,
        p_outwd0_out    => s_outwd0,
        p_done_1_out    => s_mb_done_1_o,
        p_done_0_out    => s_mb_done_0_o
        );


  --###########################--
  --## user ios              ##--
  --###########################--

	p_leds_out <= "0000";
  
  proc_dcs_checksum_verifier : process (p_clknet_in.clk40, s_hv_command_temp)
		variable v_checksum_data : std_logic_vector (22 downto 0) := (others=>'0');
		variable v_checksum_calc : integer range 0 to 255 :=0;
		variable v_n : integer range 3 to 22 := 3;
    begin
        if rising_edge(p_clknet_in.clk40) then
            v_checksum_data (22 downto 3) := s_hv_command_temp (19 downto 8) & s_hv_command_temp (7 downto 0);
            v_checksum_calc := 0;
            v_n := 3;
            for v_n in 3 to 22 loop
                if v_checksum_data(v_n) = '1' then
                    v_checksum_calc := v_checksum_calc + v_n;
                end if;
            end loop;
            
            if std_logic_vector(to_unsigned(v_checksum_calc, 8)) = s_hv_command_temp (31 downto 24) and s_hv_command_temp (31 downto 24) /= x"00" then
                s_hv_command_in <= s_hv_command_temp;
            else --checksum verification failed or checksum = 0
                s_hv_command_in <= (others=>'0'); -- value that is not used in the dcs, therefore the fsm jumps this data.
            end if;
        end if; -- clock edge
  end process;
  
  proc_dcs_checksum_generator : process (p_clknet_in.clk40, s_hv_status_temp)
		variable v_checksum_data : std_logic_vector (22 downto 0) := (others=>'0');
		variable v_checksum_calc : integer range 0 to 255 :=0;
		variable v_n : integer range 3 to 22 := 3;
   begin
        if rising_edge(p_clknet_in.clk40) then
    
            v_checksum_data (22 downto 3) := s_hv_status_temp (11 downto 0) & s_hv_status_temp (19 downto 12);
            v_checksum_calc := 0;
            v_n := 3;
            for n in 3 to 22 loop
                if v_checksum_data(n) = '1' then
                    v_checksum_calc := v_checksum_calc + n;
                end if;
            end loop;
            s_hv_status (31 downto 24) <= std_logic_vector(to_unsigned(v_checksum_calc, 8));
            s_hv_status (23 downto 0) <= s_hv_status_temp(23 downto 0);
        end if;
  end process;

    --###########################--
	--## dcs for hv control    ##--
	--###########################--
	
--	hv_opto_inst : entity work.hv_opto
--  generic map ( hv_opto_id => x"1" )
--	port map (
--		hv_enable  => hv_enable ,
--		spi_mosi_p => spi_mosi_p,
--		spi_mosi_n => spi_mosi_n,
--		spi_miso_p => spi_miso_p,
--		spi_miso_n => spi_miso_n,
--		spi_sclk_p => spi_sclk_p,
--		spi_sclk_n => spi_sclk_n,
--		spi_ssn_p  => spi_ssn_p,
--		spi_ssn_n  => spi_ssn_n,
--		status     => hv_status_temp,
--		command    => db_config_reg(cfb_hv_control),
--		command_strb => db_config_reg(cfb_strobe_reg)(11),
--		clk        => clknet.clk40db,
--		reset      => spi_rst_i
--	);


  --###########################--
  --## cesium interface      ##--
  --###########################--

        i_db6_cs_interface : entity tilecal.db6_cs_interface
        port map (
            p_clk40_in => p_clknet_in.clk40,
            p_cs_nreq_p_out => p_cs_nreq_p_out,
            p_cs_nreq_n_out => p_cs_nreq_n_out,
            p_cs_miso_p_out => p_cs_miso_p_out, 
            p_cs_miso_n_out => p_cs_miso_n_out,
            p_cs_mosi_p_in => p_cs_mosi_p_in,
            p_cs_mosi_n_in => p_cs_mosi_n_in,
            p_cs_sclk_p_in => p_cs_sclk_p_in,
            p_cs_sclk_n_in => p_cs_sclk_n_in,
            
            p_status_out    => s_cs_status,
            p_command_in   => s_cs_command,
            p_timestamp_out => s_cs_timestamp
        );


  --###########################--
  --## config adcs           ##--
  --###########################--
	
p_adc_config_done_out<=s_adcs_config_done;
p_adc_readout_initialized_out <= s_adcs_config_done;
s_adc_readout_control.adc_config_done <= s_adcs_config_done;
s_adc_config_reset <= p_adc_config_reset_in or p_db_reg_rx_in(cfb_strobe_reg)(9);

s_adc_register_config_from_configbus.mb_fpga_select <= p_db_reg_rx_in(adc_config_module)(2 downto 0);
s_adc_register_config_from_configbus.mb_pmt_select <= p_db_reg_rx_in(adc_config_module)(4 downto 3);
s_adc_register_config_from_configbus.adc_registers(1) <= p_db_reg_rx_in(adc_register_config)(31 downto 24);
s_adc_register_config_from_configbus.adc_registers(2) <= p_db_reg_rx_in(adc_register_config)(23 downto 16);     
s_adc_register_config_from_configbus.adc_registers(3) <= p_db_reg_rx_in(adc_register_config)(15 downto 8);
s_adc_register_config_from_configbus.adc_registers(4) <= p_db_reg_rx_in(adc_register_config)(7 downto 0);
s_adc_register_config_from_configbus.mode <= p_db_reg_rx_in(adc_config_module)(31);
s_adc_register_config_from_configbus.trigger_mb_adc_config <= p_db_reg_rx_in(adc_config_module)(30);

s_adc_register_config_from_readout <= s_adc_readout.mb_adc_config_control;

    i_adc_config_driver : entity tilecal.db6_adc_config_driver 
    port map( 
        p_master_reset_in    => s_adc_config_reset, --p_adc_config_reset_in,--p_db_reg_rx_in(cfb_strobe_reg)(9),
        --p_mode_in            => s_adc_config_mode,
        p_adc_register_config_from_readout_in => s_adc_register_config_from_readout,
        p_adc_register_config_from_configbus_in => s_adc_register_config_from_configbus,
        --p_mb_fpga_select     => --"100",--p_db_reg_rx_in(adc_config_module)(2 downto 0),
        --p_mb_pmt_select      => "11",--p_db_reg_rx_in(adc_config_module)(4 downto 3),
        p_clknet_in 			=> p_clknet_in,
        --p_db_reg_rx_in       => p_db_reg_rx_in,
        p_adc_config_done_out   => s_adcs_config_done,
        --p_mb_fpga_sel_in        => p_db_reg_rx_in(adc_config_module)(2 downto 0),
        --p_mb_pmt_sel_in        => p_db_reg_rx_in(adc_config_module)(4 downto 3),
        p_fe_data_out           => s_fe_data,
        p_fe_data_in            =>  p_db_reg_rx_in(cfb_mb_adc_config),
        p_leds_out => p_leds_adc_config_out
  
  );


end rtl;