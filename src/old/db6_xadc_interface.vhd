----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes
--         : Sam Silverstein
-- 
-- Create Date: 09/13/2018 12:24:37 AM
-- Design Name: 
-- Module Name: db6_xadc_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_xadc_interface is
    Port ( 
    p_clk_in : in STD_LOGIC; -- 40 MHz
    p_master_reset_in : in std_logic;
    p_xadc_mode_in : in std_logic;
    p_db_xadc_voltages : out t_db_xadc_voltages(18 downto 0);
    
    --xadc pins to top level
     p_xadc_ad0p_in     : in    std_logic;
     p_xadc_ad0n_in     : in    std_logic;
     p_xadc_ad1p_in     : in    std_logic;
     p_xadc_ad1n_in     : in    std_logic;
     p_xadc_ad2p_in     : in    std_logic;
     p_xadc_ad2n_in     : in    std_logic;
     p_xadc_ad3p_in     : in    std_logic;
     p_xadc_ad3n_in     : in    std_logic;
     p_xadc_ad4p_in     : in    std_logic;
     p_xadc_ad4n_in     : in    std_logic;
     p_xadc_ad5p_in     : in    std_logic;
     p_xadc_ad5n_in     : in    std_logic;
     p_xadc_ad6p_in     : in    std_logic;
     p_xadc_ad6n_in     : in    std_logic;
     p_xadc_ad7p_in     : in    std_logic;
     p_xadc_ad7n_in     : in    std_logic;
     p_xadc_ad8p_in     : in    std_logic;
     p_xadc_ad8n_in     : in    std_logic;
     p_xadc_ad9p_in     : in    std_logic;
     p_xadc_ad9n_in     : in    std_logic;
     p_xadc_ad10p_in    : in    std_logic;
     p_xadc_ad10n_in    : in    std_logic;
     p_xadc_ad11p_in    : in    std_logic;
     p_xadc_ad11n_in    : in    std_logic;
     p_xadc_ad12p_in    : in    std_logic;
     p_xadc_ad12n_in    : in    std_logic;
     p_xadc_ad13p_in    : in    std_logic;
     p_xadc_ad13n_in    : in    std_logic;
     p_xadc_ad14p_in    : in    std_logic;
     p_xadc_ad14n_in    : in    std_logic;
     p_xadc_ad15p_in    : in    std_logic;
     p_xadc_ad15n_in    : in    std_logic;
     p_xadc_vp_in    : in    std_logic;  -- these last two xadc inputs are for the dedicated 
     p_xadc_vn_in    : in    std_logic;  -- differential xadc inputs in bank 0
     
     --power good inputs
     p_pgood_in     : in std_logic_vector(3 downto 0);    
    
    --leds and debug_out
     p_leds_out : out  std_logic_vector(3 downto 0)
    
    );
end db6_xadc_interface;

architecture Behavioral of db6_xadc_interface is

--https://www.xilinx.com/support/documentation/user_guides/ug580-ultrascale-sysmon.pdf
COMPONENT xadc_system_managment
  PORT (
  dclk_in : IN STD_LOGIC;
  reset_in : IN STD_LOGIC;
  vp : IN STD_LOGIC;
  vn : IN STD_LOGIC;
  vauxp0 : IN STD_LOGIC;
--  vauxn0 : IN STD_LOGIC;
  vauxp1 : IN STD_LOGIC;
--  vauxn1 : IN STD_LOGIC;
  vauxp2 : IN STD_LOGIC;
--  vauxn2 : IN STD_LOGIC;
  vauxp3 : IN STD_LOGIC;
--  vauxn3 : IN STD_LOGIC;
  vauxp4 : IN STD_LOGIC;
--  vauxn4 : IN STD_LOGIC;
  vauxp5 : IN STD_LOGIC;
--  vauxn5 : IN STD_LOGIC;
  vauxp6 : IN STD_LOGIC;
--  vauxn6 : IN STD_LOGIC;
  vauxp7 : IN STD_LOGIC;
--  vauxn7 : IN STD_LOGIC;
  vauxp8 : IN STD_LOGIC;
--  vauxn8 : IN STD_LOGIC;
  vauxp9 : IN STD_LOGIC;
--  vauxn9 : IN STD_LOGIC;
  vauxp10 : IN STD_LOGIC;
--  vauxn10 : IN STD_LOGIC;
  vauxp11 : IN STD_LOGIC;
--  vauxn11 : IN STD_LOGIC;
  vauxp12 : IN STD_LOGIC;
--  vauxn12 : IN STD_LOGIC;
  vauxp13 : IN STD_LOGIC;
--  vauxn13 : IN STD_LOGIC;
  vauxp14 : IN STD_LOGIC;
--  vauxn14 : IN STD_LOGIC;
  vauxp15 : IN STD_LOGIC;
  vauxn15 : IN STD_LOGIC;
  user_temp_alarm_out : OUT STD_LOGIC;
  vccint_alarm_out : OUT STD_LOGIC;
  vccaux_alarm_out : OUT STD_LOGIC;
  ot_out : OUT STD_LOGIC;
  channel_out : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
  eoc_out : OUT STD_LOGIC;
  alarm_out : OUT STD_LOGIC;
  eos_out : OUT STD_LOGIC;
  busy_out : OUT STD_LOGIC
  --adc_data_master : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
);
END COMPONENT;

signal s_busy_out,s_den_in, s_dwe_in, s_drdy_out, s_reset_in : std_logic;
signal s_daddr_in : std_logic_vector(7 downto 0);
signal s_channel_out : std_logic_vector(5 downto 0);
signal s_eoc, s_eoc_d : std_logic := '0';
signal s_di_in, s_do_out, s_adc_data_master : std_logic_vector(15 downto 0);
signal s_db_drp_xadc_addresses : t_db_xadc_drp_addresses(0 to c_n_db_xadc_channels - 1) :=
(
    "000" & x"0",   -- temperature
    "000" & x"1",   -- vccint
    "000" & x"2",   -- vccaux
    "001" & x"0", -- mb 10v
    "001" & x"1", -- mb 1.2v
    "001" & x"2", -- mb 2.5v
    "001" & x"3", -- mb 1.8v
    "001" & x"4", -- mb +5v
    "001" & x"5", -- mb -5v
    "001" & x"6", --mon 3.3v
    "001" & x"7", --mon 2.5v
    "001" & x"8", --mon 1.8v
    "001" & x"9", --mon 1,5v
    "001" & x"a", --mon 1.2v
    "001" & x"b", --mon 0.9v
    "001" & x"c", --mon 0.85v
    "001" & x"d", --sense_1
    "001" & x"e", --sense_2
    "001" & x"f"  --sense_3
);

signal s_db_xadc_data : t_db_xadc_data;
signal s_db_xadc_voltages : t_db_xadc_voltages(18 downto 0);

--debug

COMPONENT vio_xadc_interface_debug
  PORT (
    clk : IN STD_LOGIC;
    probe_out0 : out STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out1 : out STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in0 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe_in1 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in2 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in3 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in4 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in5 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in6 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in7 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in8 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in9 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in10 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in11 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in12 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in13 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in14 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in15 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in16 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in17 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in18 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in19 : in STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

signal s_xadc_mode_debug : std_logic := '1';
signal s_reset_debug : std_logic;

COMPONENT ila_xadc_interface_debug

PORT (
	clk : IN STD_LOGIC;

	probe0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(5 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END COMPONENT  ;

    attribute keep : string;
    attribute keep of s_db_xadc_voltages : signal is "true";
    attribute keep of s_eoc : signal is "true";
    attribute keep of s_channel_out : signal is "true";
    attribute keep of s_do_out : signal is "true";



begin

p_db_xadc_voltages <= s_db_xadc_voltages;
s_di_in <= (others => '0');

s_daddr_in <= "00000000";

i_xadc_system_managment : xadc_system_managment
  PORT MAP (
    dclk_in => p_clk_in,
    reset_in => s_reset_in,
    vp => p_xadc_vp_in,
    vn => p_xadc_vn_in,
    vauxp0 => p_xadc_ad0p_in,
 --   vauxn0 => p_xadc_ad0n_in,
    vauxp1 => p_xadc_ad1p_in,
 --   vauxn1 => p_xadc_ad1n_in,
    vauxp2 => p_xadc_ad2p_in,
 --   vauxn2 => p_xadc_ad2n_in,
    vauxp3 => p_xadc_ad3p_in,
 --   vauxn3 => p_xadc_ad3n_in,
    vauxp4 => p_xadc_ad4p_in,
 --   vauxn4 => p_xadc_ad4n_in,
    vauxp5 => p_xadc_ad5p_in,
 --   vauxn5 => p_xadc_ad5n_in,
    vauxp6 => p_xadc_ad6p_in,
 --   vauxn6 => p_xadc_ad6n_in,
    vauxp7 => p_xadc_ad7p_in,
 --   vauxn7 => p_xadc_ad7n_in,
    vauxp8 => p_xadc_ad8p_in,
 --   vauxn8 => p_xadc_ad8n_in,
    vauxp9 => p_xadc_ad9p_in,
 --   vauxn9 => p_xadc_ad9n_in,
    vauxp10 => p_xadc_ad10p_in,
 --   vauxn10 => p_xadc_ad10n_in,
    vauxp11 => p_xadc_ad11p_in,
 --   vauxn11 => p_xadc_ad11n_in,
    vauxp12 => p_xadc_ad12p_in,
 --   vauxn12 => p_xadc_ad12n_in,
    vauxp13 => p_xadc_ad13p_in,
 --   vauxn13 => p_xadc_ad13n_in,
    vauxp14 => p_xadc_ad14p_in,
 --   vauxn14 => p_xadc_ad14n_in,
    vauxp15 => p_xadc_ad15p_in,
    vauxn15 => p_xadc_ad15n_in,
    user_temp_alarm_out => open,
    vccint_alarm_out => open,
    vccaux_alarm_out => open,
    ot_out => open,
    channel_out => s_channel_out,
    eoc_out => s_eoc,
    alarm_out => open,
    eos_out => open,
    busy_out => s_busy_out
    --adc_data_master => s_adc_data_master
  );


proc_read_xadc : process (p_clk_in)
variable v_selected_channel : integer := 0; 
type t_sm_xadc_ram_read is (st_set_address, st_wait_for_drdy,st_read_value);
variable sm_xadc_ram_read : t_sm_xadc_ram_read := st_set_address;
variable v_new_conversion : std_logic := '0';

  begin
    if (rising_edge(p_clk_in)) then

        p_leds_out <= s_db_xadc_data.temperature(15) & (s_db_xadc_data.temperature(14) or s_db_xadc_data.temperature(13) or s_db_xadc_data.temperature(12)) & s_db_xadc_data.temperature(11) & s_db_xadc_data.temperature(10);
        
        s_db_xadc_data.db_pgood_1 <= p_pgood_in(0);
        s_db_xadc_data.db_pgood_2 <= p_pgood_in(1);
        s_db_xadc_data.db_pgood_3 <= p_pgood_in(2);
        s_db_xadc_data.db_pgood_4 <= p_pgood_in(3);

        -- xadc is running in continuous mode, so check each clock cycle for a new end-of-conversion
        
        s_eoc_d <= s_eoc;
        
        if (s_eoc_d = '0' and s_eoc = '1') then
            v_new_conversion := '1';
        else
            v_new_conversion := '0';
        end if;
       
        if p_master_reset_in = '1' then
            s_reset_in <= '1';
            s_den_in<= '0';
            s_dwe_in<= '0';
        else
            s_reset_in <= '0' or s_reset_debug;
            
        if v_new_conversion = '1' then                
                    case s_channel_out is
                        when "000000" =>
                            s_db_xadc_voltages(0) <= s_adc_data_master;
                        when "000001" =>
                            s_db_xadc_voltages(1) <= s_adc_data_master;
                        when "000010" =>
                            s_db_xadc_voltages(2) <= s_adc_data_master;
                        when "010000" =>
                            s_db_xadc_voltages(3) <= s_adc_data_master;
                        when "010001" =>
                            s_db_xadc_voltages(4) <= s_adc_data_master;
                        when "010010" =>
                            s_db_xadc_voltages(5) <= s_adc_data_master;
                        when "010011" =>
                            s_db_xadc_voltages(6) <= s_adc_data_master;
                        when "010100" =>
                            s_db_xadc_voltages(7) <= s_adc_data_master;
                        when "010101" =>
                            s_db_xadc_voltages(8) <= s_adc_data_master;
                        when "010110" =>
                            s_db_xadc_voltages(9) <= s_adc_data_master;
                        when "010111" =>
                            s_db_xadc_voltages(10) <= s_adc_data_master;
                        when "011000" =>
                            s_db_xadc_voltages(11) <= s_adc_data_master;
                        when "011001" =>
                            s_db_xadc_voltages(12) <= s_adc_data_master;
                        when "011010" =>
                            s_db_xadc_voltages(13) <= s_adc_data_master;
                        when "011011" =>
                            s_db_xadc_voltages(14) <= s_adc_data_master;
                        when "011100" =>
                            s_db_xadc_voltages(15) <= s_adc_data_master;
                        when "011101" =>
                            s_db_xadc_voltages(16) <= s_adc_data_master;
                        when "011110" =>
                            s_db_xadc_voltages(17) <= s_adc_data_master;
                        when "011111" =>
                            s_db_xadc_voltages(18) <= s_adc_data_master;
                        when others=>
                 end case; -- s_channel_out
            end if;    -- v_new_conversion               
    
       end if; -- p_master_reset
    end if; -- clock edge
end process;

s_db_xadc_data.temperature <= s_db_xadc_voltages(0);
s_db_xadc_data.vccint <= s_db_xadc_voltages(1);
s_db_xadc_data.vccaux <= s_db_xadc_voltages(2);
s_db_xadc_data.mb_10v_voltage <= s_db_xadc_voltages(3);
s_db_xadc_data.mb_1v2_voltage <= s_db_xadc_voltages(4);
s_db_xadc_data.mb_2v5_voltage <= s_db_xadc_voltages(5);
s_db_xadc_data.mb_1v8_voltage <= s_db_xadc_voltages(6);
s_db_xadc_data.mb_5v_voltage <= s_db_xadc_voltages(7);
s_db_xadc_data.mb_5vn_voltage <= s_db_xadc_voltages(8);
s_db_xadc_data.db_3v3_current <= s_db_xadc_voltages(9);
s_db_xadc_data.db_2v5_current <= s_db_xadc_voltages(10);
s_db_xadc_data.db_1v8_current <= s_db_xadc_voltages(11);
s_db_xadc_data.db_1v5_current <= s_db_xadc_voltages(12);
s_db_xadc_data.db_1v2_current <= s_db_xadc_voltages(13);
s_db_xadc_data.db_0v9_current <= s_db_xadc_voltages(14);
s_db_xadc_data.db_0v85_current <= s_db_xadc_voltages(15);
s_db_xadc_data.db_sense_1 <= s_db_xadc_voltages(16);
s_db_xadc_data.db_sense_2 <= s_db_xadc_voltages(17);
s_db_xadc_data.db_sense_3 <= s_db_xadc_voltages(18);


--debug

--   i_vio_xadc_interface_debug : vio_xadc_interface_debug
--        port map (
--        clk => p_clk_in,
--        probe_out0(0) => s_reset_debug,
--        probe_out1(0) => s_xadc_mode_debug,
--        probe_in0 => p_pgood_in,
--        probe_in1 => s_db_mon_data.temperature,
--        probe_in2 => s_db_mon_data.vccint,
--        probe_in3 => s_db_mon_data.vccaux,
--        probe_in4 => s_db_mon_data.mb_10v_voltage,
--        probe_in5 => s_db_mon_data.mb_1v2_voltage,
--        probe_in6 => s_db_mon_data.mb_2v5_voltage,
--        probe_in7 => s_db_mon_data.mb_1v8_voltage,
--        probe_in8 => s_db_mon_data.mb_5v_voltage,
--        probe_in9 => s_db_mon_data.mb_5vn_voltage,
--        probe_in10 => s_db_mon_data.db_3v3_current,
--        probe_in11 => s_db_mon_data.db_2v5_current,
--        probe_in12 => s_db_mon_data.db_1v8_current,
--        probe_in13 => s_db_mon_data.db_1v5_current,
--        probe_in14 => s_db_mon_data.db_1v2_current,
--        probe_in15 => s_db_mon_data.db_0v9_current,
--        probe_in16 => s_db_mon_data.db_0v85_current,
--        probe_in17 => s_db_mon_data.db_sense_1,
--        probe_in18 => s_db_mon_data.db_sense_1,
--        probe_in19 => s_db_mon_data.db_sense_1
--      ); 


--i_ila_xadc_interface_debug : ila_xadc_interface_debug
--PORT MAP (
--	clk => p_clk_in,



--	probe0 => s_adc_data_master, 
--	probe1 => s_di_in, 
--	probe2 => s_do_out, 
--	probe3 => s_daddr_in, 
--	probe4 => s_channel_out, 
--	probe5(0) => s_dwe_in, 
--	probe6(0) => s_busy_out, 
--	probe7(0) => s_den_in, 
--	probe8(0) => s_drdy_out,
--	probe9(0) => s_reset_in
--);

end Behavioral;
