----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
--           Samuel Silverstein
--           Alberto Valero
-- Create Date: 10/03/2018 05:14:49 PM
-- Design Name: 
-- Module Name: db6_cis_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity db6_cis_interface is
  Port ( 
        p_clk40_in              : in std_logic;
        p_master_reset_in       : in std_logic;
        p_cis_enable_in         : in std_logic;
        p_cis_gain_in           : in std_logic;
        p_cis_bcid_charge_in    : in std_logic_vector(11 downto 0):=x"000";
        p_cis_bcid_discharge_in : in std_logic_vector(11 downto 0);
        p_bcr_in         : in std_logic;
        p_bc_number_in            : in std_logic_vector(15 downto 0);
        p_tph_out               : out std_logic_vector(1 downto 0);
        p_tpl_out               : out std_logic_vector(1 downto 0)
  );
  
  
end db6_cis_interface;

architecture Behavioral of db6_cis_interface is

begin

proc_cis_driver : process(p_clk40_in)
type t_sm_cis is (st_idle , st_charge_lg, st_charge_hg);
variable sm_cis, sm_cis_next   : t_sm_cis ;
begin
	if (rising_edge(p_clk40_in)) then
	
		if (p_master_reset_in = '1' or p_cis_enable_in ='0') then
			sm_cis := st_idle;
		else
			sm_cis := sm_cis_next;
			
				case sm_cis is
                    when st_idle => 
                        p_tph_out     <= "00";
                        p_tpl_out        <= "00";
                        if (p_bc_number_in(11 downto 0) = p_cis_bcid_charge_in and p_cis_enable_in ='1') then
                            if (p_cis_gain_in ='0') then
                                sm_cis_next := st_charge_lg;
                            else
                                sm_cis_next := st_charge_hg;
                            end if;
                        else
                            sm_cis_next := st_idle;
                        end if;

                    when st_charge_lg => 
                        p_tph_out     <= "00";
                        p_tpl_out    <= "11";
                        if ( p_bc_number_in(11 downto 0) = p_cis_bcid_discharge_in) then
                            sm_cis_next := st_idle;
                        else 
                            sm_cis_next := st_charge_lg;
                        end if;

                    when st_charge_hg => 
                        p_tph_out     <= "11";
                        p_tpl_out    <= "00";
                        if ( p_bc_number_in(11 downto 0) = p_cis_bcid_discharge_in) then
                            sm_cis_next := st_idle;
                        else 
                            sm_cis_next := st_charge_hg;
                        end if;
                
                end case;

			
		end if;
	end if;
end process;



end Behavioral;
