--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Samuel Silverstein    silver@fysik.su.se
--                        Eduardo Valdes Santurio
--                                                                                                 
-- Project Name:          ADC deserializer for LTC2264-12                                                                
-- Module Name:           ADC_top                                        
--                                                                                                 
-- Language:              VHDL                                                                 
--                                                                                                   --
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_adc_interface_v5 is

    Port ( 	
                p_master_reset_in          : in std_logic;
--                p_adc_mode_in              : in std_logic;
				p_clknet_in 			: in  t_db_clock_network;
				p_adc_readout_out       : out t_adc_readout;
				p_adc_readout_control_in : in t_adc_readout_control;

				p_adc_data0_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data0_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_p_in : in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_n_in : in  STD_LOGIC_VECTOR (5 downto 0);

				p_debug_pin_out : out std_logic_vector(5 downto 0)
				);
end db6_adc_interface_v5;

architecture Behavioral of db6_adc_interface_v5 is

    signal s_channel_reset, s_channel_locked : std_logic_vector (5 downto 0) := (others => '0');
    signal s_data_lg, s_data_hg, s_data_lg_delayed, s_data_hg_delayed , s_bitclk_in, s_bitclk, s_frameclk, s_frameclk_to_bufg ,s_frameclk_delayed: std_logic_vector (5 downto 0) := (others => '0');
    attribute IOB: string;
    attribute keep: string;
    attribute dont_touch: string;
    attribute IOB of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
    attribute keep of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
    attribute dont_touch of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
    --attribute keep of s_adc_readout          : signal is "true";    
    
    signal s_bufgce_div_ctrl_reset, s_bitclk_div : std_logic_vector(5 downto 0) := (others => '0'); 

    signal s_idelay_ctrl_rdy, s_idelay_ctrl_reset : std_logic := '0';

    signal s_lg_idelay_count_in, s_lg_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_hg_idelay_count_in, s_hg_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_fc_idelay_count_in, s_fc_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_lg_idelay_ctrl_reset, s_lg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_reset, s_hg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_reset, s_fc_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_idelay_ctrl_load : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_load : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_load : std_logic_vector (5 downto 0) := (others => '0');  
    signal s_lg_idelay_ctrl_en_vtc : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_en_vtc : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_en_vtc : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    
    
    signal s_channel_frame_missalignemt : std_logic_vector (5 downto 0) := (others => '1');
    
    signal s_frame_strobe : std_logic_vector (5 downto 0);
    

    signal s_readout_lg_sr,s_readout_hg_sr : t_adc_sr;
    signal s_bitslice_lg_sr, s_bitslice_hg_sr, s_bitslice_fc_sr : t_bitslice_sr;
    signal s_adc_readout : t_adc_readout :=
    (
    lg_bitslip => (others => "0111"),
    hg_bitslip => (others => "0111"),
    lg_idelay_count => (others =>"000000000"),
    hg_idelay_count => (others =>"000000000"),
    fc_idelay_count => (others =>"000000000"),
    lg_data =>(others=>"00000000000000"),
    hg_data =>(others=>"00000000000000"),
    channel_missed_bit_count=>(others=>(others=>'0')),
    channel_frame_missalignemt => (others=>'0')
    );
    
    
    signal s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word, s_fc_output_word : t_adc_data;
    
    type t_adc_oversample_data_array is array (1 downto 0) of t_adc_oversample_data_type;
    signal s_adc_input_fc_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_array;
    


COMPONENT ila_fc_sampling

PORT (
	clk : IN STD_LOGIC;



	probe0 : IN STD_LOGIC_VECTOR(27 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(27 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(27 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(27 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(27 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
	probe6 : IN STD_LOGIC_VECTOR(1 DOWNTO 0)
);
END COMPONENT  ;
type t_phase_flag  is array (5 downto 0) of std_logic_vector(1 downto 0); 
signal s_phase_flag : t_phase_flag := (others=>("00"));
    

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG


begin

p_adc_readout_out <= s_adc_readout;

-- Differential to single-ended conversion of ADC inputs from FMC

diff_to_se : for i in 0 to 5 generate

    IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_lg(i),             -- Buffer diff_p output
        I  => p_adc_data0_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data0_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_DATA1 : IBUFDS -- ADC output High gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_hg(i),             -- Buffer diff_p output
        I  => p_adc_data1_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data1_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_frameclk(i),             -- Buffer diff_p output
        I  => p_adc_frameclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_frameclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

--    IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
--      generic map (
--        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
--      port map (
--        O  => s_bitclk_in(i),             -- Buffer diff_p output
--        I  => p_adc_bitclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
--        IB => p_adc_bitclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
--        );




--    clock_buffer : bufg
--        port map (
--            I => s_bitclk_in(i),
--            O => s_bitclk(i)
--        );
    


--    bufgce_div_inst : bufgce_div
--    generic map (
--        bufgce_divide => 7, -- 1-8
--        -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
--        is_ce_inverted => '0', -- optional inversion for ce
--        is_clr_inverted => '0', -- optional inversion for clr
--        is_i_inverted => '0' -- optional inversion for i
--        )
--        port map (
--        o => s_bitclk_div(i), -- 1-bit output: buffer
--        ce => '1', -- 1-bit input: buffer enable
--        clr => s_bufgce_div_ctrl_reset(i), -- 1-bit input: asynchronous clear
--        i => s_bitclk_in(i) -- 1-bit input: buffer
--    );


end generate;

-- Generate ADC data input registers and output word mapping,
-- The output bits from each ADC is stored in four shift registers (two even and two odd).


s_idelay_ctrl_reset <= p_master_reset_in;

gen_adc_channels_quad_0: for v_adc in 0 to 2 generate

    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);
    s_adc_readout.hg_bitslip(v_adc) <= p_adc_readout_control_in.hg_bitslip(v_adc);
    s_adc_readout.lg_bitslip(v_adc) <= p_adc_readout_control_in.lg_bitslip(v_adc);
--p_channel_frame_missalignemt_out(v_adc) <= s_channel_frame_missalignemt(v_adc);
    


i_idelaye3_data_lg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_lg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_data_lg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => s_lg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_lg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_lg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_lg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_lg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);
s_lg_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc) or s_lg_idelay_ctrl_reset_from_sm(v_adc);
s_lg_idelay_ctrl_load(v_adc) <= p_adc_readout_control_in.lg_idelay_load(v_adc);
s_lg_idelay_ctrl_en_vtc(v_adc) <= p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
s_lg_idelay_count_in(v_adc) <= p_adc_readout_control_in.lg_idelay_count(v_adc);
s_adc_readout.lg_idelay_count(v_adc) <= s_lg_idelay_count_out(v_adc);


i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_hg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_data_hg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => s_hg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_hg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_hg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_hg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_hg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);
s_hg_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc) or s_hg_idelay_ctrl_reset_from_sm(v_adc);
s_hg_idelay_ctrl_load(v_adc) <= p_adc_readout_control_in.hg_idelay_load(v_adc);
s_hg_idelay_ctrl_en_vtc(v_adc) <= p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
s_hg_idelay_count_in(v_adc) <= p_adc_readout_control_in.hg_idelay_count(v_adc);
s_adc_readout.hg_idelay_count(v_adc) <= s_hg_idelay_count_out(v_adc);


i_idelaye3_data_fc : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_fc_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_frameclk_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => s_fc_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_fc_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_frameclk(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_fc_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_fc_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_fc_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.fc_idelay_ctrl_reset(v_adc) or s_fc_idelay_ctrl_reset_from_sm(v_adc);
s_fc_idelay_ctrl_load(v_adc) <= '0'; -- p_adc_readout_control_in.fc_idelay_load(v_adc);
s_fc_idelay_ctrl_en_vtc(v_adc) <= '1'; -- p_adc_readout_control_in.fc_idelay_en_vtc(v_adc);
s_fc_idelay_count_in(v_adc) <= "000000000";--p_adc_readout_control_in.fc_idelay_count(v_adc);
s_adc_readout.fc_idelay_count(v_adc) <= s_fc_idelay_count_out(v_adc);

i_iddre1_lg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_lg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_lg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => p_clknet_in.clk560mb(0), -- 1-bit input: high-speed clock
cb => p_clknet_in.clk560mb(0), -- 1-bit input: inversion of high-speed clock c
d => s_data_lg_delayed(v_adc), -- 1-bit input: serial data input
r => s_lg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);


--s_iserdes_ctrl_reset_lg(v_adc) <= p_idelay_ctrl_reset_lg_in(v_adc);

i_iddre1_hg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_hg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_hg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => p_clknet_in.clk560mb(0), -- 1-bit input: high-speed clock
cb => p_clknet_in.clk560mb(0), -- 1-bit input: inversion of high-speed clock c
d => s_data_hg_delayed(v_adc), -- 1-bit input: serial data input
r => s_hg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);
--s_iserdes_ctrl_reset_hg(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc);

i_iddre1_fc : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_fc_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_fc_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => p_clknet_in.clk560mb(0), -- 1-bit input: high-speed clock
cb => p_clknet_in.clk560mb(0), -- 1-bit input: inversion of high-speed clock c
d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
r => s_fc_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);





        proc_shift_in : process(p_clknet_in.clk560mb(0)) -- Odd data bits clocked in on rising edge of adc clocks 
        begin


            if rising_edge(p_clknet_in.clk560mb(0)) then
                --if p_adc_mode_in = '0' then
                
                    s_adc_input_hg_temp(0)(v_adc) <= s_adc_input_hg_temp(0)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_hg_sr(v_adc)(1);
                    s_adc_input_lg_temp(0)(v_adc) <= s_adc_input_lg_temp(0)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_lg_sr(v_adc)(1);
                    s_adc_input_fc_temp(0)(v_adc) <= s_adc_input_fc_temp(0)(v_adc)(((c_oversamples*c_adc_bit_number -1) - 1) downto 0) & s_bitslice_fc_sr(v_adc)(1);

                    s_adc_input_hg_temp(1)(v_adc) <= s_adc_input_hg_temp(1)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_hg_sr(v_adc)(0);
                    s_adc_input_lg_temp(1)(v_adc) <= s_adc_input_lg_temp(1)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_lg_sr(v_adc)(0);
                    s_adc_input_fc_temp(1)(v_adc) <= s_adc_input_fc_temp(1)(v_adc)(((c_oversamples*c_adc_bit_number -1) - 1) downto 0) & s_bitslice_fc_sr(v_adc)(0);
                    
                    
                --else 
            
            end if;
        end process;


    p_debug_pin_out(v_adc) <= s_phase_flag(v_adc)(1) and s_phase_flag(v_adc)(0);
    
    proc_frame_capture : process(p_clknet_in.clk560mb(0))
    constant c_phase_offset : integer:= 7;
    begin
        if rising_edge(p_clknet_in.clk560mb(0)) then
            
            if (s_adc_input_fc_temp(0)(v_adc)(13 downto 0) = "11111110000000")  and (s_adc_input_fc_temp(1)(v_adc)(13 downto 0) = "11111110000000") then
                s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
                s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
                s_phase_flag(v_adc)<= "11";
            
            elsif (s_adc_input_fc_temp(0)(v_adc)(13 downto 0) = "11111110000000") then-- and (s_adc_input_fc_temp(0) = "11111110000000") then
                s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(0)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
                s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(0)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
                s_phase_flag(v_adc)<= "01";
            elsif (s_adc_input_fc_temp(1)(v_adc)(13 downto 0) = "11111110000000") then
                s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
                s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
                s_phase_flag(v_adc)<= "10";
            else
                s_phase_flag(v_adc)<= "00";
            end if;
            
            --12 bits
            --s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
            --s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));    

        end if;
        
    end process;
    
    i_ila_fc_sampling : ila_fc_sampling
    PORT MAP (
        clk => p_clknet_in.clk280,
   
    
        probe0 => s_adc_input_fc_temp(0)(v_adc)(27 downto 0), 
        probe1 => s_adc_input_fc_temp(1)(v_adc)(27 downto 0), 
        probe2 => s_adc_input_lg_temp(0)(v_adc)(27 downto 0), 
        probe3 => s_adc_input_lg_temp(1)(v_adc)(27 downto 0), 
        probe4 => s_adc_input_hg_temp(0)(v_adc)(27 downto 0), 
        probe5 => s_adc_input_hg_temp(1)(v_adc)(27 downto 0),
        probe6 => s_phase_flag(v_adc)
);

    proc_phase_reset : process(p_clknet_in.clk40) -- Capture ADC words synchronous to ADC (bit) clock
    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
    variable v_frameclk_sm : t_frameclk_sm := st_low;
    type t_reset_sm is (st_initialize, st_reset_divclk, st_reset_idelay, st_reset_iserdes, st_idle);
    variable v_reset_sm : t_reset_sm := st_initialize;
    variable v_counter : integer := 0;
   
        
    begin
        if rising_edge(p_clknet_in.clk40) then
        
            --p_debug_pin_out(v_adc)<= s_frameclk(v_adc);

                if p_master_reset_in = '0' then

                    
                    case v_reset_sm is
                
                        when st_initialize =>
                            s_channel_reset(v_adc) <=  '1';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            
                            v_reset_sm:=st_reset_divclk;
                            
                        when st_reset_divclk => 
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '1';

                            
                            v_reset_sm:=st_reset_idelay;                             
                                                    
                        when st_reset_idelay => 
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '1';
                              
                            
                            v_reset_sm:=st_reset_iserdes;                        
                                                        
                        when st_reset_iserdes =>
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '0';
                            
                            
                            v_reset_sm:=st_idle;                        
                            
                        when st_idle =>
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_channel_frame_missalignemt(v_adc) <= '0';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '0';
                            

                       
                        when others=>
                            v_reset_sm:=st_initialize;
                    end case;
                    
                
                else
                
                    v_reset_sm:=st_initialize;
                    s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                    s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                    s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                    s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_channel_frame_missalignemt(v_adc) <= '1';
        
                end if;
                                        
            
        end if;
    end process;


end generate;


gen_adc_channels_quad_1: for v_adc in 3 to 5 generate

    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);
    s_adc_readout.hg_bitslip(v_adc) <= p_adc_readout_control_in.hg_bitslip(v_adc);
    s_adc_readout.lg_bitslip(v_adc) <= p_adc_readout_control_in.lg_bitslip(v_adc);
--p_channel_frame_missalignemt_out(v_adc) <= s_channel_frame_missalignemt(v_adc);
    


i_idelaye3_data_lg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_lg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_data_lg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => s_lg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_lg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_lg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_lg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_lg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);
s_lg_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc) or s_lg_idelay_ctrl_reset_from_sm(v_adc);
s_lg_idelay_ctrl_load(v_adc) <= p_adc_readout_control_in.lg_idelay_load(v_adc);
s_lg_idelay_ctrl_en_vtc(v_adc) <= p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
s_lg_idelay_count_in(v_adc) <= p_adc_readout_control_in.lg_idelay_count(v_adc);
s_adc_readout.lg_idelay_count(v_adc) <= s_lg_idelay_count_out(v_adc);


i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_hg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_data_hg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => s_hg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_hg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_hg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_hg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_hg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);
s_hg_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc) or s_hg_idelay_ctrl_reset_from_sm(v_adc);
s_hg_idelay_ctrl_load(v_adc) <= p_adc_readout_control_in.hg_idelay_load(v_adc);
s_hg_idelay_ctrl_en_vtc(v_adc) <= p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
s_hg_idelay_count_in(v_adc) <= p_adc_readout_control_in.hg_idelay_count(v_adc);
s_adc_readout.hg_idelay_count(v_adc) <= s_hg_idelay_count_out(v_adc);


i_idelaye3_data_fc : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_fc_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_frameclk_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk280, -- 1-bit input: clock input
        cntvaluein => s_fc_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_fc_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_frameclk(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_fc_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_fc_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_fc_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.fc_idelay_ctrl_reset(v_adc) or s_fc_idelay_ctrl_reset_from_sm(v_adc);
s_fc_idelay_ctrl_load(v_adc) <= '0'; -- p_adc_readout_control_in.fc_idelay_load(v_adc);
s_fc_idelay_ctrl_en_vtc(v_adc) <= '1'; -- p_adc_readout_control_in.fc_idelay_en_vtc(v_adc);
s_fc_idelay_count_in(v_adc) <= "000000000";--p_adc_readout_control_in.fc_idelay_count(v_adc);
s_adc_readout.fc_idelay_count(v_adc) <= s_fc_idelay_count_out(v_adc);

i_iddre1_lg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_lg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_lg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => p_clknet_in.clk560mb(1), -- 1-bit input: high-speed clock
cb => p_clknet_in.clk560mb(1), -- 1-bit input: inversion of high-speed clock c
d => s_data_lg_delayed(v_adc), -- 1-bit input: serial data input
r => s_lg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);


--s_iserdes_ctrl_reset_lg(v_adc) <= p_idelay_ctrl_reset_lg_in(v_adc);

i_iddre1_hg : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_hg_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_hg_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => p_clknet_in.clk560mb(1), -- 1-bit input: high-speed clock
cb => p_clknet_in.clk560mb(1), -- 1-bit input: inversion of high-speed clock c
d => s_data_hg_delayed(v_adc), -- 1-bit input: serial data input
r => s_hg_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);
--s_iserdes_ctrl_reset_hg(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc);

i_iddre1_fc : iddre1
generic map (
ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
is_c_inverted => '0', -- optional inversion for c
is_cb_inverted => '1' -- optional inversion for c
)
port map (
q1 => s_bitslice_fc_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
q2 => s_bitslice_fc_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
c => p_clknet_in.clk560mb(1), -- 1-bit input: high-speed clock
cb => p_clknet_in.clk560mb(1), -- 1-bit input: inversion of high-speed clock c
d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
r => s_fc_iserdes_ctrl_reset(v_adc) -- 1-bit input: active high async reset
);





        proc_shift_in : process(p_clknet_in.clk560mb(1)) -- Odd data bits clocked in on rising edge of adc clocks 
        begin


            if rising_edge(p_clknet_in.clk560mb(1)) then
                --if p_adc_mode_in = '0' then
                
                    s_adc_input_hg_temp(0)(v_adc) <= s_adc_input_hg_temp(0)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_hg_sr(v_adc)(1);
                    s_adc_input_lg_temp(0)(v_adc) <= s_adc_input_lg_temp(0)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_lg_sr(v_adc)(1);
                    s_adc_input_fc_temp(0)(v_adc) <= s_adc_input_fc_temp(0)(v_adc)(((c_oversamples*c_adc_bit_number -1) - 1) downto 0) & s_bitslice_fc_sr(v_adc)(1);

                    s_adc_input_hg_temp(1)(v_adc) <= s_adc_input_hg_temp(1)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_hg_sr(v_adc)(0);
                    s_adc_input_lg_temp(1)(v_adc) <= s_adc_input_lg_temp(1)(v_adc)((((c_oversamples*c_adc_bit_number) -1) - 1) downto 0) & s_bitslice_lg_sr(v_adc)(0);
                    s_adc_input_fc_temp(1)(v_adc) <= s_adc_input_fc_temp(1)(v_adc)(((c_oversamples*c_adc_bit_number -1) - 1) downto 0) & s_bitslice_fc_sr(v_adc)(0);
                    
                    
                --else 
            
            end if;
        end process;


    p_debug_pin_out(v_adc) <= s_phase_flag(v_adc)(1) and s_phase_flag(v_adc)(0);
    
    proc_frame_capture : process(p_clknet_in.clk560mb(1))
    constant c_phase_offset : integer:= 7;
    begin
        if rising_edge(p_clknet_in.clk560mb(1)) then
            
            if (s_adc_input_fc_temp(0)(v_adc)(13 downto 0) = "11111110000000")  and (s_adc_input_fc_temp(1)(v_adc)(13 downto 0) = "11111110000000") then
                s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
                s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
                s_phase_flag(v_adc)<= "11";
            
            elsif (s_adc_input_fc_temp(0)(v_adc)(13 downto 0) = "11111110000000") then-- and (s_adc_input_fc_temp(0) = "11111110000000") then
                s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(0)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
                s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(0)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
                s_phase_flag(v_adc)<= "01";
            elsif (s_adc_input_fc_temp(1)(v_adc)(13 downto 0) = "11111110000000") then
                s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
                s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(1)(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
                s_phase_flag(v_adc)<= "10";
            else
                s_phase_flag(v_adc)<= "00";
            end if;
            
            --12 bits
            --s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
            --s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));    

        end if;
        
    end process;
    
    i_ila_fc_sampling : ila_fc_sampling
    PORT MAP (
        clk => p_clknet_in.clk280,
   
    
        probe0 => s_adc_input_fc_temp(0)(v_adc)(27 downto 0), 
        probe1 => s_adc_input_fc_temp(1)(v_adc)(27 downto 0), 
        probe2 => s_adc_input_lg_temp(0)(v_adc)(27 downto 0), 
        probe3 => s_adc_input_lg_temp(1)(v_adc)(27 downto 0), 
        probe4 => s_adc_input_hg_temp(0)(v_adc)(27 downto 0), 
        probe5 => s_adc_input_hg_temp(1)(v_adc)(27 downto 0),
        probe6 => s_phase_flag(v_adc)
);

    proc_phase_reset : process(p_clknet_in.clk40) -- Capture ADC words synchronous to ADC (bit) clock
    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
    variable v_frameclk_sm : t_frameclk_sm := st_low;
    type t_reset_sm is (st_initialize, st_reset_divclk, st_reset_idelay, st_reset_iserdes, st_idle);
    variable v_reset_sm : t_reset_sm := st_initialize;
    variable v_counter : integer := 0;
   
        
    begin
        if rising_edge(p_clknet_in.clk40) then
        
            --p_debug_pin_out(v_adc)<= s_frameclk(v_adc);

                if p_master_reset_in = '0' then

                    
                    case v_reset_sm is
                
                        when st_initialize =>
                            s_channel_reset(v_adc) <=  '1';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            
                            v_reset_sm:=st_reset_divclk;
                            
                        when st_reset_divclk => 
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '1';

                            
                            v_reset_sm:=st_reset_idelay;                             
                                                    
                        when st_reset_idelay => 
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '1';
                              
                            
                            v_reset_sm:=st_reset_iserdes;                        
                                                        
                        when st_reset_iserdes =>
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '0';
                            
                            
                            v_reset_sm:=st_idle;                        
                            
                        when st_idle =>
                            s_channel_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_channel_frame_missalignemt(v_adc) <= '0';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <= '0';
                            

                       
                        when others=>
                            v_reset_sm:=st_initialize;
                    end case;
                    
                
                else
                
                    v_reset_sm:=st_initialize;
                    s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                    s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                    s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '1';
                    s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_channel_frame_missalignemt(v_adc) <= '1';
        
                end if;
                                        
            
        end if;
    end process;


end generate;



        bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
        begin
            if falling_edge(p_clknet_in.clk40) then
                s_adc_readout.lg_data <= s_lg_adc_output_word;
                s_adc_readout.hg_data <= s_hg_adc_output_word;
            end if;
        end process;


end Behavioral;
