----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/30/2018 11:51:14 PM
-- Design Name: 
-- Module Name: mmcm_cfgbus - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity mmcm_cfgbus is
    Port ( p_clkin1_p_in : in STD_LOGIC;
           p_clkin1_n_in : in STD_LOGIC;
           p_clkin2_p_in : in STD_LOGIC;
           p_clkin2_n_in : in STD_LOGIC;
           p_clkcfgbus_out: out std_logic;
           p_clksel_in : in STD_LOGIC;
           p_locked_out : out std_logic;
           p_reset_in : in STD_LOGIC);
end mmcm_cfgbus;


architecture Behavioral of mmcm_cfgbus is

signal s_clkin1_cfgbus_gbuf, s_clkin2_cfgbus_gbuf : std_logic;
--signal s_clkin_cfgbus : std_logic;

begin
--clk buffers
  i_ds_cfbc_loc : ibufds
  generic map (
    diff_term => true,
    iostandard => "sub_lvds")
  port map (
    o  => s_clkin1_cfgbus_gbuf,             -- buffer diff_p output
    i  => p_clkin1_p_in, -- diff_p buffer input (connect directly to top-level port)
    ib => p_clkin1_n_in  -- diff_n buffer input (connect directly to top-level port)
    );

 i_ds_cfbc_rem : ibufds
      generic map (
        diff_term => true,
        iostandard => "sub_lvds")
      port map (
        o  => s_clkin2_cfgbus_gbuf,             -- buffer diff_p output
        i  => p_clkin2_p_in, -- diff_p buffer input (connect directly to top-level port)
        ib => p_clkin2_n_in  -- diff_n buffer input (connect directly to top-level port)
        );

bufgmux_inst : bufgmux
generic map (
clk_sel_type => "async" -- sync, async
)
port map (
    o => p_clkcfgbus_out, -- 1-bit output: clock output
    i0 => s_clkin1_cfgbus_gbuf, -- 1-bit input: clock input (s=0)
    i1 => s_clkin2_cfgbus_gbuf, -- 1-bit input: clock input (s=1)
    s => p_clksel_in -- 1-bit input: clock select
);

p_locked_out<= not p_reset_in;

--proc_choose_config_bus_clocks : process(p_clksel_in,p_reset_in)
--begin
--    if p_reset_in = '0' then
--        case p_clksel_in is
--            when '0'=>
--                p_clkcfgbus_out<=s_clkin1_cfgbus_gbuf;
--                --s_clkin_cfgbus<==s_clkin1_cfgbus_gbuf;
--            when '1'=>
--                p_clkcfgbus_out<=s_clkin2_cfgbus_gbuf;
--                --s_clkin_cfgbus<==s_clkin2_cfgbus_gbuf;
--            when  others=>
--        end case;
--        p_locked_out<= '1';
--    else
--        p_clkcfgbus_out<='0';
--        p_locked_out<= '0';
--    end if;
--end process;




--proc_lock_to_signal : process(s_clkin_cfgbus)
--begin
--    if rising_edge 


end Behavioral;
