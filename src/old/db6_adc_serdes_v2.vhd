--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Samuel Silverstein    silver@fysik.su.se
--                        Eduardo Valdes Santurio
--                                                                                                 
-- Project Name:          ADC deserializer for LTC2264-12                                                                
-- Module Name:           ADC_top                                        
--                                                                                                 
-- Language:              VHDL                                                                 
--                                                                                                   --
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_adc_interface_v11 is

    Port ( 	
                p_master_reset_in          : in std_logic;
--                p_adc_mode_in              : in std_logic;
				p_clknet_in 			: in  t_db_clock_network;
				p_adc_readout_out       : out t_adc_readout;
				p_adc_readout_control_in : in t_adc_readout_control;

				p_adc_data0_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data0_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_p_in : in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_n_in : in  STD_LOGIC_VECTOR (5 downto 0);

				p_debug_pin_out : out std_logic_vector(5 downto 0)
				);
end db6_adc_interface_v11;

architecture Behavioral of db6_adc_interface_v11 is


    signal s_data_lg, s_data_hg, s_data_lg_delayed, s_data_hg_delayed , s_bitclk_in, s_bitclk, s_frameclk, s_frameclk_to_bufg ,s_frameclk_delayed: std_logic_vector (5 downto 0) := (others => '0');
    signal s_bufgce_div_ctrl_reset, s_bitclk_div : std_logic_vector(5 downto 0) := (others => '0'); 
    attribute IOB: string;
    attribute keep: string;
    attribute dont_touch: string;
    attribute IOB of s_bitclk_div, s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
    attribute keep of s_bitclk_div, s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";
    attribute dont_touch of s_data_lg, s_data_hg, s_frameclk, s_data_lg_delayed, s_data_hg_delayed : signal is "TRUE";

    signal s_idelay_ctrl_rdy, s_idelay_ctrl_reset : std_logic := '0';

    signal s_lg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_hw, s_hg_idelay_count_in_from_sm, s_lg_idelay_count_in_from_sm : t_idelay_integer_array;
    signal s_lg_idelay_count_in, s_lg_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_hg_idelay_count_in, s_hg_idelay_count_out : t_idelay_count := (others => (others => '0'));
    signal s_fc_idelay_count_in, s_fc_idelay_count_out, s_fc_idelay_count_in_from_sm : t_idelay_count := (others => (others => '0'));
    signal s_lg_idelay_ctrl_reset, s_lg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_reset, s_hg_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_reset, s_fc_idelay_ctrl_reset_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_idelay_ctrl_load, s_lg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_load, s_hg_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_load, s_fc_idelay_ctrl_load_from_sm : std_logic_vector (5 downto 0) := (others => '0');  
    signal s_lg_idelay_ctrl_en_vtc, s_lg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_idelay_ctrl_en_vtc, s_hg_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_idelay_ctrl_en_vtc, s_fc_idelay_ctrl_en_vtc_from_sm : std_logic_vector (5 downto 0) := (others => '0');  

    signal s_lg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_hg_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    signal s_fc_iserdes_ctrl_reset : std_logic_vector (5 downto 0) := (others => '0');
    
    
    signal s_channel_frame_missalignemt : std_logic_vector (5 downto 0) := (others => '1');
    
    signal s_frame_strobe : std_logic_vector (5 downto 0);
    

    signal s_readout_lg_sr,s_readout_hg_sr : t_adc_sr;
    signal s_bitslice_lg_sr, s_bitslice_hg_sr, s_bitslice_fc_sr : t_bitslice_sr;
    signal s_adc_readout : t_adc_readout;
    signal s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word, s_fc_output_word : t_adc_data;
    
    signal s_fc_bitslips_from_sm, s_hg_bitslips_from_sm, s_lg_bitslips_from_sm, s_hg_bitslips_from_hw, s_lg_bitslips_from_hw : t_bitslips_integer_array;
    

    signal s_adc_output_temp, s_adc_input_fc_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;
    signal s_frameclk_div_sample : std_logic_vector(6 downto 0) :="0000000";
    
    --attribute keep : string;
    --attribute keep of s_adc_word          : signal is "true";
    
    COMPONENT vio_channel_tunning
      PORT (
        clk : IN STD_LOGIC;
        probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in2 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        probe_in3 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        probe_in4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in7 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        probe_in8 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        probe_in9 : IN STD_LOGIC_VECTOR(3 DOWNTO 0)        
      );
    END COMPONENT;
    
    signal s_hg_bitslips_debug, s_lg_bitslips_debug : t_bitslip;

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG


begin

p_adc_readout_out <= s_adc_readout;

proc_load_hw_bitslips: process(p_adc_readout_control_in.db_side)
begin
    if p_adc_readout_control_in.db_side = "01" then

        s_lg_bitslips_from_hw <= (0,0,0,0,0,0);
        s_hg_bitslips_from_hw <= (-1,0,0,0,0,0);
        
        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);
        s_hg_idelay_count_in_from_hw <= (60,0,0,0,0,0);
    
    elsif p_adc_readout_control_in.db_side = "10" then

        s_lg_bitslips_from_hw <= (-1,0,0,0,0,-1);
        s_hg_bitslips_from_hw <= (0,0,0,0,0,-1);

        s_lg_idelay_count_in_from_hw <= (60,0,0,0,0,60);
        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,60);        

    else

        s_lg_bitslips_from_hw <= (0,0,0,0,0,0);
        s_hg_bitslips_from_hw <= (0,0,0,0,0,0);
        
        s_lg_idelay_count_in_from_hw <= (0,0,0,0,0,0);
        s_hg_idelay_count_in_from_hw <= (0,0,0,0,0,0);

    end if;

end process;




-- Differential to single-ended conversion of ADC inputs from FMC

diff_to_se : for i in 0 to 5 generate

    IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_lg(i),             -- Buffer diff_p output
        I  => p_adc_data0_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data0_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_DATA1 : IBUFDS -- ADC output High gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_hg(i),             -- Buffer diff_p output
        I  => p_adc_data1_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data1_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_frameclk(i),             -- Buffer diff_p output
        I  => p_adc_frameclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_frameclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_bitclk_in(i),             -- Buffer diff_p output
        I  => p_adc_bitclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_bitclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );


    clock_buffer : bufg
        port map (
            I => s_bitclk_in(i),
            O => s_bitclk(i)
        );
    


    bufgce_div_inst : bufgce_div
    generic map (
        bufgce_divide => 2, -- 1-8
        -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
        is_ce_inverted => '0', -- optional inversion for ce
        is_clr_inverted => '0', -- optional inversion for clr
        is_i_inverted => '0' -- optional inversion for i
        )
        port map (
        o => s_bitclk_div(i), -- 1-bit output: buffer
        ce => '1', -- 1-bit input: buffer enable
        clr => s_bufgce_div_ctrl_reset(i), -- 1-bit input: asynchronous clear
        i => s_bitclk_in(i) -- 1-bit input: buffer
    );


end generate;

-- Generate ADC data input registers and output word mapping,
-- The output bits from each ADC is stored in four shift registers (two even and two odd).


s_idelay_ctrl_reset <= p_master_reset_in;

gen_adc_channels: for v_adc in 0 to 5 generate

    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);
    s_adc_readout.hg_bitslip(v_adc) <= p_adc_readout_control_in.hg_bitslip(v_adc);
    s_adc_readout.lg_bitslip(v_adc) <= p_adc_readout_control_in.lg_bitslip(v_adc);

    


i_idelaye3_data_lg : idelaye3
        generic map (
            SIM_DEVICE => "ULTRASCALE_PLUS",
            cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
            delay_format => "count", -- units of the delay_value (time, count)
            delay_src => "idatain", -- delay input (idatain, datain)
            delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
            delay_value => 0, -- input delay value setting
            is_clk_inverted => '0', -- optional inversion for clk
            is_rst_inverted => '0', -- optional inversion for rst
            refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
            update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
            )
            port map (
            casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
            cntvalueout => s_lg_idelay_count_out(v_adc), -- 9-bit output: counter value output
            dataout => s_data_lg_delayed(v_adc), -- 1-bit output: delayed data output
            casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
            casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
            ce => '0', -- 1-bit input: active high enable increment/decrement input
            clk => s_bitclk_div(v_adc), -- 1-bit input: clock input
            cntvaluein => s_lg_idelay_count_in(v_adc), -- 9-bit input: counter value input
            datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
            en_vtc => s_lg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
            idatain => s_data_lg(v_adc), -- 1-bit input: data input from the logic
            inc => '0', -- 1-bit input: increment / decrement tap delay input
            load => s_lg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
            rst => s_lg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
    );
    
s_lg_idelay_ctrl_reset(v_adc) <= s_lg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc); 
s_lg_idelay_ctrl_load(v_adc) <= s_lg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_load(v_adc);
s_lg_idelay_ctrl_en_vtc(v_adc) <= s_lg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
s_lg_idelay_count_in(v_adc) <= std_logic_vector(to_unsigned(s_lg_idelay_count_in_from_sm(v_adc) + s_lg_idelay_count_in_from_hw(v_adc),9));-- p_adc_readout_control_in.lg_idelay_count(v_adc);
s_adc_readout.lg_idelay_count(v_adc) <= s_lg_idelay_count_out(v_adc);



i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_hg_idelay_count_out(v_adc), -- 9-bit output: counter value output
        dataout => s_data_hg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => s_bitclk_div(v_adc), -- 1-bit input: clock input
        cntvaluein => s_hg_idelay_count_in(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => s_hg_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_hg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => s_hg_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_hg_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

s_hg_idelay_ctrl_reset(v_adc) <= s_hg_idelay_ctrl_reset_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);
s_hg_idelay_ctrl_load(v_adc) <= s_hg_idelay_ctrl_load_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_load(v_adc);
s_hg_idelay_ctrl_en_vtc(v_adc) <= s_hg_idelay_ctrl_en_vtc_from_sm(v_adc); --p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
s_hg_idelay_count_in(v_adc) <= std_logic_vector(to_unsigned(s_hg_idelay_count_in_from_sm(v_adc) + s_hg_idelay_count_in_from_hw(v_adc),9)); --p_adc_readout_control_in.hg_idelay_count(v_adc);
s_adc_readout.hg_idelay_count(v_adc) <= s_hg_idelay_count_out(v_adc);


--i_idelaye3_data_fc : idelaye3
--    generic map (
--        SIM_DEVICE => "ULTRASCALE_PLUS",
--        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
--        delay_format => "count", -- units of the delay_value (time, count)
--        delay_src => "idatain", -- delay input (idatain, datain)
--        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
--        delay_value => 0, -- input delay value setting
--        is_clk_inverted => '0', -- optional inversion for clk
--        is_rst_inverted => '0', -- optional inversion for rst
--        refclk_frequency => 280.0, -- idelayctrl clock input frequency in mhz (values)
--        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
--        )
--        port map (
--        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
--        cntvalueout => s_fc_idelay_count_out(v_adc), -- 9-bit output: counter value output
--        dataout => s_frameclk_delayed(v_adc), -- 1-bit output: delayed data output
--        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
--        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
--        ce => '0', -- 1-bit input: active high enable increment/decrement input
--        clk => s_bitclk_div(v_adc), -- 1-bit input: clock input
--        cntvaluein => "111111111",--s_fc_idelay_count_in(v_adc), -- 9-bit input: counter value input
--        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
--        en_vtc => s_fc_idelay_ctrl_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
--        idatain => s_frameclk(v_adc), -- 1-bit input: data input from the logic
--        inc => '0', -- 1-bit input: increment / decrement tap delay input
--        load => s_fc_idelay_ctrl_load(v_adc), -- 1-bit input: load delay_value input
--        rst => s_fc_idelay_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset to the delay_value
--);

--s_fc_idelay_ctrl_reset(v_adc) <= p_adc_readout_control_in.fc_idelay_ctrl_reset(v_adc) or s_fc_idelay_ctrl_reset_from_sm(v_adc);
--s_fc_idelay_ctrl_load(v_adc) <= s_fc_idelay_ctrl_load_from_sm(v_adc);--p_adc_readout_control_in.fc_idelay_load(v_adc);
--s_fc_idelay_ctrl_en_vtc(v_adc) <= s_fc_idelay_ctrl_en_vtc_from_sm(v_adc);-- p_adc_readout_control_in.fc_idelay_en_vtc(v_adc);
--s_fc_idelay_count_in(v_adc) <= s_fc_idelay_count_in_from_sm(v_adc); --p_adc_readout_control_in.fc_idelay_count(v_adc);
--s_adc_readout.fc_idelay_count(v_adc) <= s_fc_idelay_count_out(v_adc);


i_iserdese3_lg : iserdese3
generic map (
SIM_DEVICE => "ULTRASCALE_PLUS",
data_width => 4, -- parallel data width (4,8)
fifo_enable => "false", -- enables the use of the fifo
fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
is_clk_b_inverted => '1', -- optional inversion for clk_b
is_clk_inverted => '0', -- optional inversion for clk
is_rst_inverted => '0' -- optional inversion for rst
)
port map (

fifo_empty => open, -- 1-bit output: fifo empty flag
internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
q => s_bitslice_lg_sr(v_adc), -- 8-bit registered output
clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
d => s_data_lg_delayed(v_adc), -- 1-bit input: serial data input
fifo_rd_clk => '0', -- 1-bit input: fifo read clock
fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
rst => s_lg_iserdes_ctrl_reset(v_adc)    -- 1-bit input: asynchronous reset

);
--s_iserdes_ctrl_reset_lg(v_adc) <= p_idelay_ctrl_reset_lg_in(v_adc);

i_iserdese3_hg : iserdese3
generic map (
SIM_DEVICE => "ULTRASCALE_PLUS",
data_width =>4, -- parallel data width (4,8)
fifo_enable => "false", -- enables the use of the fifo
fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
is_clk_b_inverted => '1', -- optional inversion for clk_b
is_clk_inverted => '0', -- optional inversion for clk
is_rst_inverted => '0' -- optional inversion for rst
)
port map (

fifo_empty => open, -- 1-bit output: fifo empty flag
internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
q => s_bitslice_hg_sr(v_adc), -- 8-bit registered output
clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
d => s_data_hg_delayed(v_adc), -- 1-bit input: serial data input
fifo_rd_clk => '0', -- 1-bit input: fifo read clock
fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
rst => s_hg_iserdes_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset

);



--i_iddre1_fc : iddre1
--generic map (
--ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
--is_c_inverted => '0', -- optional inversion for c
--is_cb_inverted => '1' -- optional inversion for c
--)
--port map (
--q1 => s_bitslice_fc_sr(v_adc)(0), -- 1-bit output: registered parallel output 1
--q2 => s_bitslice_fc_sr(v_adc)(1), -- 1-bit output: registered parallel output 2
--c => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--cb => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock c
--d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
--r => s_iserdes_ctrl_reset_fc(v_adc) -- 1-bit input: active high async reset
--);


--i_iserdese3_fc : iserdese3
--generic map (
--SIM_DEVICE => "ULTRASCALE_PLUS",
--data_width => 4, -- parallel data width (4,8)
--fifo_enable => "true", -- enables the use of the fifo
--fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
--is_clk_b_inverted => '1', -- optional inversion for clk_b
--is_clk_inverted => '0', -- optional inversion for clk
--is_rst_inverted => '0' -- optional inversion for rst
--)
--port map (

--fifo_empty => open, -- 1-bit output: fifo empty flag
--internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
--q => s_bitslice_fc_sr(v_adc), -- 8-bit registered output
--clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
--clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
--d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
--fifo_rd_clk => '0', -- 1-bit input: fifo read clock
--fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
--rst => s_fc_iserdes_ctrl_reset(v_adc) -- 1-bit input: asynchronous reset

--);
--s_iserdes_ctrl_reset_fc(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc) and p_idelay_ctrl_reset_lg_in(v_adc);


    proc_shift_in : process(s_bitclk_div(v_adc)) -- Odd data bits clocked in on rising edge of adc clocks
    --type t_divclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
    --variable v_divclk_sm : t_divclk_sm := st_low;
    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
    variable v_frameclk_sm  : t_frameclk_sm := st_low;
    type t_phase_adjust_sm is (st_initializing, st_reseting, st_working);
    variable v_phase_adjust_sm : t_phase_adjust_sm := st_initializing; 
    variable v_counter : integer:=0;
    variable v_phase : std_logic := '0';
    variable v_frame_clk_sr : std_logic_vector(6 downto 0);
    --variable v_previous_delayed_fc : std_logic := '0';
    begin
    
    
        if rising_edge(s_bitclk_div(v_adc)) then


            s_adc_input_hg_temp(v_adc) <= s_adc_input_hg_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_hg_sr(v_adc)(0) & s_bitslice_hg_sr(v_adc)(1) & s_bitslice_hg_sr(v_adc)(2) & s_bitslice_hg_sr(v_adc)(3);
            s_adc_input_lg_temp(v_adc) <= s_adc_input_lg_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_lg_sr(v_adc)(0) & s_bitslice_lg_sr(v_adc)(1) & s_bitslice_lg_sr(v_adc)(2) & s_bitslice_lg_sr(v_adc)(3);
            --s_adc_input_fc_temp(v_adc) <= s_adc_input_fc_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_fc_sr(v_adc)(0) & s_bitslice_fc_sr(v_adc)(1) & s_bitslice_fc_sr(v_adc)(2) & s_bitslice_fc_sr(v_adc)(3);
            --s_frameclk_div_sample<= s_frameclk_div_sample(5 downto 0) & s_frameclk(v_adc);
--            if p_master_reset_in = '0' then
--                case v_phase_adjust_sm is
--                    when st_initializing=>
--                        if s_frameclk(v_adc) = '0' then
--                            v_phase_adjust_sm:= st_reseting;
--                            v_counter:=0;
--                            v_phase := '0';
--                        end if;
--                    when st_reseting=>
--                        v_frame_clk_sr:=v_frame_clk_sr(5 downto 0)&s_frameclk(v_adc);
--                        if v_frame_clk_sr = "0110011" then
--                            v_counter:=0;
--                            v_phase_adjust_sm := st_working;
--                            v_phase := '0';
--                        end if;
----                        if s_frameclk(v_adc) = '1' then
----                            v_counter:=0;
----                            v_phase_adjust_sm := st_working;
----                            v_phase := '0';
----                        end if;
--                    when st_working=>
--                        if v_counter < 4 then
--                            v_counter:=v_counter+1;
--                        else
--                            v_counter:=0;
--                            v_phase := not v_phase;
--                            if v_phase = '0' then
--                                s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(13+10 downto 0+10);
--                                s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(13+10 downto 0+10);
--                            else                        
--                                s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(13+10+2 downto 0+10+2);
--                                s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(13+10+2 downto 0+10+2);
--                            end if;
--                        end if;
--                    when others =>
--                        v_phase_adjust_sm := st_initializing;
--                end case;
--            else
--                v_phase_adjust_sm := st_initializing;
--            end if;

--            case v_frameclk_sm is
--                when st_rising_edge =>
--                    if s_frameclk_delayed(v_adc) = '0' then 
--                        v_frameclk_sm := st_falling_edge;
--                    else
--                        v_frameclk_sm := st_high;
--                    end if;
                    
--                when st_high =>
--                    if s_frameclk_delayed(v_adc) = '0' then 
--                        v_frameclk_sm := st_falling_edge;
--                    else
--                        v_frameclk_sm := st_high;
--                    end if;

                
--                when st_falling_edge =>

----                    s_adc_input_hg_temp(v_adc) <= s_adc_input_hg_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_hg_sr(v_adc)(0) & s_bitslice_hg_sr(v_adc)(1) & s_bitslice_hg_sr(v_adc)(2) & s_bitslice_hg_sr(v_adc)(3);
----                    s_adc_input_lg_temp(v_adc) <= s_adc_input_lg_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_lg_sr(v_adc)(0) & s_bitslice_lg_sr(v_adc)(1) & s_bitslice_lg_sr(v_adc)(2) & s_bitslice_lg_sr(v_adc)(3);
----                    s_adc_input_fc_temp(v_adc) <= s_adc_input_fc_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_fc_sr(v_adc)(0) & s_bitslice_fc_sr(v_adc)(1) & s_bitslice_fc_sr(v_adc)(2) & s_bitslice_fc_sr(v_adc)(3);


--                    if s_frameclk_delayed(v_adc) = '1' then 
--                        v_frameclk_sm := st_rising_edge;
--                    else
--                        v_frameclk_sm := st_low;
--                    end if;
                    

                
--                when st_low =>
--                    if s_frameclk_delayed(v_adc) = '1' then 
--                        v_frameclk_sm := st_rising_edge;
--                    else
--                        v_frameclk_sm := st_low;
--                    end if;

                
--                when others =>
--                    v_frameclk_sm := st_low;
                    
--            end case;
        
        end if;
    end process; -- sr_odd



--    proc_frame_capture : process(s_bitclk_div(v_adc))
--    constant c_phase_offset : integer:= 0;
--    begin
--        if rising_edge(s_bitclk_div(v_adc)) then
--            s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.hg_bitslip(v_adc)))));
--            s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc))) + (c_adc_bit_number-1)  downto c_phase_offset +(to_integer(unsigned(s_adc_readout.lg_bitslip(v_adc)))));
            
--            --12 bits
--            --s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
--            --s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));    

--        end if;
        
--    end process;

proc_phase_reset : process(s_bitclk(v_adc)) -- Capture ADC words synchronous to ADC (bit) clock
type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
variable v_frameclk_sm, v_clk40db_sm : t_frameclk_sm := st_low;
type t_reset_sm is (st_initialize, st_reset_divclk, st_reset_idelay, st_reset_iserdes, st_disable_vtc, 
st_load_value, st_wait_load, st_enable_vtc, st_calculate_fc_bitslips, st_check_fc_bitslip_stability, st_idle);
variable v_reset_sm : t_reset_sm := st_initialize;
variable v_counter : integer := 0;
constant c_phase_offset : integer:= 0;
variable  v_fc_tries: integer := 0;
constant max_fc_tries : integer := 1024;
variable v_idelay_fc : integer := 0;

    
begin
    if rising_edge(s_bitclk(v_adc)) then
    
        --p_debug_pin_out(v_adc)<= s_frameclk(v_adc);
        case v_clk40db_sm is
            when st_rising_edge =>
                if p_clknet_in.clk40 = '1' then -- Capture when frame clock has just gone high
                    v_clk40db_sm := st_high;
                else
                    v_clk40db_sm := st_falling_edge;
                end if;
            when st_falling_edge =>
                s_adc_readout.lg_data(v_adc) <= s_lg_adc_output_word(v_adc);
                s_adc_readout.hg_data(v_adc) <= s_hg_adc_output_word(v_adc);
                if p_clknet_in.clk40 = '0' then -- Capture when frame clock has just gone high
                    v_clk40db_sm := st_low;
                else
                    v_clk40db_sm := st_rising_edge;
                end if;
            when st_high =>
                if p_clknet_in.clk40 = '0' then
                    v_clk40db_sm := st_falling_edge;
                end if;                    
            when st_low =>
                if p_clknet_in.clk40 = '1' then
                    v_clk40db_sm := st_rising_edge;
                end if;    
            when others =>
            
        end case;
        
        case v_frameclk_sm is
            when st_rising_edge =>
                          
                if p_master_reset_in = '0' then

                    if s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
                        v_frameclk_sm := st_high;
                    else
                        v_frameclk_sm := st_falling_edge;
                    end if;
                    
                    s_adc_readout.channel_missed_bit_count(v_adc) <=  std_logic_vector(to_unsigned(s_fc_bitslips_from_sm(v_adc), 31));
                    
                    case v_reset_sm is
                
                        when st_initialize =>
                            s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <= '0'; --'1';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0'; --'1';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                            s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                            
                            s_channel_frame_missalignemt(v_adc) <= '1';

                            if p_adc_readout_control_in.adc_config_done= '1' then
                                v_reset_sm:=st_reset_divclk;
                            else
                                v_reset_sm:=st_initialize;
                            end if;
                            
                            
                        when st_reset_divclk => 
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0'; --'1';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <= '0'; --'1';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                            s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  
                            
                            s_channel_frame_missalignemt(v_adc) <= '1';

                            if p_adc_readout_control_in.adc_config_done= '1' then
                                v_reset_sm:=st_reset_idelay;
                            else
                                v_reset_sm:=st_initialize;
                            end if;                                
                                                         
                                                    
                        when st_reset_idelay => 
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';

                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

                            s_channel_frame_missalignemt(v_adc) <= '1';  
                            
                            v_reset_sm:=st_reset_iserdes;                        
                                                        
                        when st_reset_iserdes =>
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <=  '0';
                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';

                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

                            s_channel_frame_missalignemt(v_adc) <= '1';
                            
                            if p_adc_readout_control_in.adc_config_done= '1' then
                                v_reset_sm:=st_disable_vtc;
                            else
                                v_reset_sm:=st_initialize;
                            end if;                                 
                                                    
                            
                        when st_disable_vtc =>

                            s_lg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                            s_hg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                            s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                            s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                            
                            s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                            s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                            
--                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                            s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                            s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                            
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            
                            if p_adc_readout_control_in.adc_config_done= '1' then
                                v_reset_sm:=st_load_value;
                            else
                                v_reset_sm:=st_initialize;
                            end if;                                  
                            
                            
                        when st_load_value =>
                        
                            s_lg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                            s_hg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                            s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                            s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_load_from_sm(v_adc) <= '1';
                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                            
                            s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                            s_lg_idelay_ctrl_load_from_sm(v_adc) <= '1';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';    

--                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                            s_fc_idelay_ctrl_load_from_sm(v_adc) <= '1';
--                            s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                            
                            s_channel_frame_missalignemt(v_adc) <= '1';                        

                            if p_adc_readout_control_in.adc_config_done= '1' then
                                v_reset_sm:=st_wait_load;
                            else
                                v_reset_sm:=st_initialize;
                            end if;                       
                            


                        when st_wait_load =>

                            s_lg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                            s_hg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                            s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                            s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                            
                            s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                            s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';  

--                            s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
--                            s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
--                            s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '0';
                             
                            s_channel_frame_missalignemt(v_adc) <= '1';

                            --if (s_lg_idelay_count_in(v_adc)) = s_lg_idelay_count_out(v_adc) then
                                
                                if p_adc_readout_control_in.adc_config_done= '1' then
                                    v_reset_sm:=st_enable_vtc;
                                else
                                    v_reset_sm:=st_initialize;
                                end if;
                            
                            --end if;
                            
                        when st_enable_vtc =>

                            s_lg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                            s_hg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));
                            --s_fc_idelay_count_in_from_sm(v_adc) <= std_logic_vector(to_unsigned(v_idelay_fc, 9));

                            s_hg_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                            
                            s_lg_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                            s_lg_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';  

                            --s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0'; 
                            --s_fc_idelay_ctrl_load_from_sm(v_adc) <= '0';
                            --s_fc_idelay_ctrl_en_vtc_from_sm(v_adc) <= '1';
                             
                            s_channel_frame_missalignemt(v_adc) <= '1';

                            if p_adc_readout_control_in.adc_config_done= '1' then
                                v_reset_sm:=st_idle;
                            else
                                v_reset_sm:=st_initialize;
                            end if;
                            

                            
                        when st_idle =>
                            --s_channel_frame_missalignemt(v_adc) <= '0';
                            if s_bitclk_div(v_adc) = '0' then
                                s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(13+10 + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.hg_bitslip(v_adc))) 
                                                            downto 0+10 + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.hg_bitslip(v_adc))));
                                s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(13+10 + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.lg_bitslip(v_adc))) 
                                                            downto 0+10 + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.lg_bitslip(v_adc))));
                                s_channel_frame_missalignemt(v_adc) <= '1';
                            else
                                s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(13+2+10 + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.hg_bitslip(v_adc)))
                                                            downto 0+2+10 + s_hg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.hg_bitslip(v_adc))));
                                s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(13+2+10 + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.lg_bitslip(v_adc)))
                                                            downto 0+2+10 + s_lg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.lg_bitslip(v_adc))));
                                s_channel_frame_missalignemt(v_adc) <= '0';
                            end if;
                            
                            
--                            case s_adc_input_fc_temp(v_adc)(17 downto 0) is
--                                when "000011111110000000" =>
--                                    s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(13 downto 0);
--                                    s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(13 downto 0);
--                                    s_fc_output_word(v_adc)<=s_adc_input_fc_temp(v_adc)(13 downto 0);
--                                    s_channel_frame_missalignemt(v_adc) <= '0';
--                                when "000111111100000001" =>
--                                    s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(14 downto 1);
--                                    s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(14 downto 1);
--                                    s_fc_output_word(v_adc)<=s_adc_input_fc_temp(v_adc)(14 downto 1);
--                                    s_channel_frame_missalignemt(v_adc) <= '0';
--                                when "001111111000000011" =>
--                                    s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(15 downto 2);
--                                    s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(15 downto 2);
--                                    s_fc_output_word(v_adc)<=s_adc_input_fc_temp(v_adc)(15 downto 2);
--                                    s_channel_frame_missalignemt(v_adc) <= '0';
--                                when "011111110000000111" =>
--                                    s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(16 downto 3);
--                                    s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(16 downto 3);
--                                    s_fc_output_word(v_adc)<=s_adc_input_fc_temp(v_adc)(16 downto 3);
--                                    s_channel_frame_missalignemt(v_adc) <= '0';
--                                when "111111100000001111" =>
--                                    s_hg_adc_output_word(v_adc)<=s_adc_input_hg_temp(v_adc)(17 downto 4);
--                                    s_lg_adc_output_word(v_adc)<=s_adc_input_lg_temp(v_adc)(17 downto 4);
--                                    s_fc_output_word(v_adc)<=s_adc_input_fc_temp(v_adc)(17 downto 4);
--                                    s_channel_frame_missalignemt(v_adc) <= '0';
                                
--                                when others =>
--                                    s_hg_adc_output_word(v_adc)<=x"FFF";--s_adc_input_hg_temp(v_adc)(18 downto 1);
--                                    s_lg_adc_output_word(v_adc)<=x"FFF";--s_adc_input_lg_temp(v_adc)(18 downto 1);
--                                    s_fc_output_word(v_adc)<=x"FFF";--s_adc_input_fc_temp(v_adc)(18 downto 1);
--                                    s_channel_frame_missalignemt(v_adc) <= '1';

--                                end case;
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            
                            
                            --s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_lg_iserdes_ctrl_reset(v_adc)  <= p_adc_readout_control_in.lg_idelay_ctrl_reset(v_adc);-- '0';
                            --s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0';
                            s_hg_iserdes_ctrl_reset(v_adc)  <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);-- '0';
                            --s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '0';
                            --s_fc_iserdes_ctrl_reset(v_adc)  <=  '0';
                            --s_channel_frame_missalignemt(v_adc) <= '0';

                            s_lg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.lg_idelay_count(v_adc)));
                            s_hg_idelay_count_in_from_sm(v_adc) <= to_integer(unsigned(p_adc_readout_control_in.hg_idelay_count(v_adc)));

                            s_hg_idelay_ctrl_reset_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_ctrl_reset(v_adc);
                            s_hg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_load(v_adc);
                            s_hg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.hg_idelay_en_vtc(v_adc);
                            
                            --s_lg_idelay_ctrl_reset_from_sm(v_adc) <= s_lg_idelay_ctrl_reset_from_sm(v_adc); 
                            s_lg_idelay_ctrl_load_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_load(v_adc);
                            s_lg_idelay_ctrl_en_vtc_from_sm(v_adc) <= p_adc_readout_control_in.lg_idelay_en_vtc(v_adc);
                            
                        
                        when others=>
                            v_reset_sm:=st_initialize;
                    end case;
                    
                
                else
                
                    v_reset_sm:=st_initialize;
                    s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                    s_lg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0'; --'1';
                    s_lg_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_hg_idelay_ctrl_reset_from_sm(v_adc)  <=  '0'; --'1';
                    s_hg_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_fc_idelay_ctrl_reset_from_sm(v_adc) <= '1';
                    s_fc_iserdes_ctrl_reset(v_adc)  <=  '1';
                    s_channel_frame_missalignemt(v_adc) <= '1';
        
                end if;
                                        
                
            when st_falling_edge =>
             
                if s_frameclk(v_adc) = '0' then -- Capture when frame clock has just gone high
                    v_frameclk_sm := st_low;
                else
                    v_frameclk_sm := st_rising_edge;
                end if; 
            when st_low =>
                if s_frameclk(v_adc) = '1' then
                    v_frameclk_sm := st_rising_edge;
                end if;
            when st_high =>
                if s_frameclk(v_adc) = '0' then
                    v_frameclk_sm := st_falling_edge;
                end if;                    
            
        end case;
        
        
    end if;
end process;

--s_hg_bitslips_debug(v_adc)<= std_logic_vector(to_signed(s_hg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.hg_bitslip(v_adc))),4));
--s_lg_bitslips_debug(v_adc)<= std_logic_vector(to_signed(s_lg_bitslips_from_hw(v_adc) + to_integer(signed(p_adc_readout_control_in.lg_bitslip(v_adc))),4));

--i_vio_channel_tunning : vio_channel_tunning
--  PORT MAP (
--    clk => p_clknet_in.clk40,
--    probe_in0(0) => s_hg_idelay_ctrl_load(v_adc),
--    probe_in1(0) => s_hg_idelay_ctrl_en_vtc(v_adc),
--    probe_in2 => s_hg_idelay_count_in(v_adc),
--    probe_in3 => s_hg_idelay_count_out(v_adc),
--    probe_in4 => s_hg_bitslips_debug(v_adc),
--    probe_in5(0) => s_lg_idelay_ctrl_load(v_adc),
--    probe_in6(0) => s_lg_idelay_ctrl_en_vtc(v_adc),
--    probe_in7 => s_lg_idelay_count_in(v_adc),
--    probe_in8 => s_lg_idelay_count_out(v_adc),
--    probe_in9 => s_lg_bitslips_debug(v_adc)    
--  );


end generate;


--        bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
--        begin
--            if falling_edge(p_clknet_in.clk40) then
--                s_adc_readout.lg_data <= s_lg_adc_output_word;
--                s_adc_readout.hg_data <= s_hg_adc_output_word;
--            end if;
--        end process;


end Behavioral;

