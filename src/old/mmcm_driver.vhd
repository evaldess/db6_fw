----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes
-- 
-- Create Date: 09/08/2018 02:11:43 PM
-- Design Name: 
-- Module Name: mmcm_driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: constrols phase shifting through mmcmc drp
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- from https://www.xilinx.com/support/documentation/application_notes/xapp888_7Series_DynamicRecon.pdf
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity mmcm_driver is
generic
    (
		g_number_of_mmcms         : integer := 2
     );
    Port ( 
           p_driver_clk_in : in std_logic;
           p_sm_clk_in : in std_logic;
           p_clkin1_in : in std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_clkin2_in : in std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_clk_in_sel_in : in std_logic;
           p_master_reset_in : in std_logic;
           p_mmcm_clk_control_in : in t_mmcm_clk_control_array;
           p_clk40_out : out std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_clk560_out : out std_logic_vector((g_number_of_mmcms-1) downto 0);
           p_locked_out : out std_logic;
           p_leds_out : out std_logic_vector(3 downto 0)
           );
end mmcm_driver;

architecture Behavioral of mmcm_driver is


--https://www.xilinx.com/support/documentation/application_notes/xapp888_7Series_DynamicRecon.pdf
component mmcm_mb
port
 (-- Clock in ports
  p_clkin2_in           : in     std_logic;
  p_clk_in_sel_in           : in     std_logic;
  -- Clock out ports
  p_clk40_out          : out    std_logic;
  --p_clk560_out         : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Status and control signals
  p_reset_in             : in     std_logic;
  p_locked_out            : out    std_logic;
  p_clkin1_in           : in     std_logic
 );
end component;

signal s_master_reset: std_logic;
--signal s_locked : std_logic;



--drp
constant c_number_of_mmcms : integer := g_number_of_mmcms;

type t_reg_data is array (0 to (c_number_of_mmcms-1)) of t_mmcm_clk_register_configuration;
type t_reg_address is array (0 to (c_number_of_mmcms-1)) of t_mmcm_clk_register_address;
signal s_mmcm_clk0_reg_read, s_mmcm_clk0_reg_write : t_reg_data;
signal s_mmcm_clk0_reg_address : t_reg_address := ( others => ((std_logic_vector(to_unsigned(8,7))),std_logic_vector(to_unsigned(9,7))));--,
                                                    --1 => ((std_logic_vector(to_unsigned(8,7))),std_logic_vector(to_unsigned(9,7)))); 

type t_daddr is array (0 to 1) of std_logic_vector(6 downto 0); 
signal s_daddr : t_daddr;
type t_data is array (0 to 1) of std_logic_vector(15 downto 0);
signal s_din, s_dout : t_data;

type t_std_logic_array is array (0 to (c_number_of_mmcms-1)) of std_logic;
signal s_locked : t_std_logic_array;
signal s_den, s_dwe, s_drdy : t_std_logic_array;
signal s_trigger_write_operation : std_logic;
signal s_trigger_read_operation : std_logic;


--debug
signal s_enable_vio_debug_mode : std_logic_vector(0 downto 0):="0";
signal s_mmcm_clk0_reg_read_debug, s_mmcm_clk0_reg_write_debug : t_reg_data;

COMPONENT vio_mmcm_control_debug
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out3 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe_out4 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ila_mmcm_control_debug

PORT (
	clk : IN STD_LOGIC;



	probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(6 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END COMPONENT  ;

begin

--s_master_reset <= p_master_reset_in;
gen_mmcms : for i in 0 to (c_number_of_mmcms-1) generate

   
    i_mmcm_mb : mmcm_mb
       port map ( 
       p_clkin2_in => p_clkin2_in(i),
       p_clk_in_sel_in => p_clk_in_sel_in,
      -- Clock out ports  
       p_clk40_out => p_clk40_out(i),
       --p_clk560_out => p_clk560_out(i),
      -- Dynamic reconfiguration ports             
       p_daddr_in => s_daddr(i),
       p_dclk_in => p_driver_clk_in,
       p_den_in => s_den(i),
       p_din_in => s_din(i),
       p_dout_out => s_dout(i),
       p_drdy_out => s_drdy(i),
       p_dwe_in => s_dwe(i),
      -- Status and control signals                
       p_reset_in => s_master_reset,
       p_locked_out => s_locked(i),
       -- Clock in ports
       p_clkin1_in => p_clkin1_in(i)
     );

end generate;
    
    

    proc_mmcm_controller:process(p_sm_clk_in, s_locked, s_drdy)
    type t_sm_mmcm_driver is (st_initialize,st_wait_locked,st_wait_unlocked,st_wait_drdy,st_read_regs, st_write_regs, st_idle, st_debounce_operation);
    variable v_sm_mmcm_driver : t_sm_mmcm_driver := st_initialize;
    variable v_sm_next_state :t_sm_mmcm_driver := st_initialize;
    variable v_register_counter : integer:=0;
    constant c_max_register : integer :=2;
    variable v_mmcm_clk_control_buffer: t_mmcm_clk_control_array;
    variable v_drdy, v_locked : std_logic;
    variable v_daddr : std_logic_vector(6 downto 0);
    variable v_mmcm_clk0_reg_address : t_daddr;
    begin
    
    p_locked_out <= v_locked;
    for i in 0 to (c_number_of_mmcms-1) loop
        if i = 0 then
            v_locked := s_locked(i);
            v_drdy := s_drdy(i);
            v_daddr := s_daddr(i); 
            v_mmcm_clk0_reg_address(0) := s_mmcm_clk0_reg_address(i)(0);
            v_mmcm_clk0_reg_address(1) := s_mmcm_clk0_reg_address(i)(1);
        else
            v_drdy := v_drdy and s_drdy(i);
            v_locked := v_locked and s_locked(i);
            v_daddr := v_daddr and s_daddr(i);
            v_mmcm_clk0_reg_address(0) := v_mmcm_clk0_reg_address(0) and s_mmcm_clk0_reg_address(i)(0);
            v_mmcm_clk0_reg_address(1) := v_mmcm_clk0_reg_address(1) and s_mmcm_clk0_reg_address(i)(1);
        end if;
        
    end loop;
        
        if rising_edge(p_sm_clk_in) then
            
            
            if p_master_reset_in = '1' then
                s_master_reset<=p_master_reset_in;      
                
                for i in 0 to (c_number_of_mmcms-1) loop      
                    s_daddr(i)<= (others=>'0');
                    s_din(i)<= (others=>'0');
                    --s_master_reset<='1';
                    s_dwe(i)<='0';
                    s_den(i)<='0';
                end loop;
                v_sm_mmcm_driver := st_initialize;
            else
                case v_sm_mmcm_driver is
                    when st_initialize=>
                    
                        s_master_reset<='0';
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "000";
                        v_sm_mmcm_driver:= st_wait_locked;
                        --v_sm_next_state:= st_read_regs;
                        v_mmcm_clk_control_buffer:= p_mmcm_clk_control_in;

                    when st_wait_locked =>
                    
                        s_master_reset<='0';
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "001";
                        if (v_locked = '1') then
                            for i in 0 to (c_number_of_mmcms-1) loop   
                                s_dwe(i)<='0'; --set read
                                s_den(i)<='1'; --set dram module enable
                            end loop;
                            v_register_counter:=0;
                            v_sm_mmcm_driver:=st_read_regs;
                            v_sm_next_state:=st_idle;
                        end if;
                        
                    when st_wait_unlocked =>
                        s_master_reset<='1';
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "010";
                        if (v_locked = '0') then
                            v_register_counter:=0;
                            v_sm_mmcm_driver:=st_write_regs;
                            v_sm_next_state:=st_initialize;
                        end if;
                            
                    when st_read_regs=>
                    
                        s_master_reset<='0';
                        v_mmcm_clk_control_buffer:= p_mmcm_clk_control_in;
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "011";
                        
                        for i in 0 to (c_number_of_mmcms-1) loop   
                            s_dwe(i)<='0'; --set read
                            s_den(i)<='1'; --set dram module enable
                        end loop;
                        
                        --checks if the propper reg is selected in the input ports of the dram and chooses it if not 
                        if (v_daddr/=v_mmcm_clk0_reg_address(0)) then
                            for i in 0 to (c_number_of_mmcms-1) loop   
                                s_daddr(i)<=s_mmcm_clk0_reg_address(i)(0);
                                s_mmcm_clk0_reg_read(i)(0)<=s_dout(i);
                            end loop;
                        else
                        --continues to loop over the rest of the registers
                        
                            for i in 0 to (c_number_of_mmcms-1) loop
                                s_mmcm_clk0_reg_read(i)(v_register_counter)<=s_dout(i);
                                s_daddr(i)<=s_mmcm_clk0_reg_address(i)(v_register_counter);
                            end loop;
                                
                            if v_register_counter<c_max_register then
                                v_register_counter:=v_register_counter+1;
                            else
                                v_sm_mmcm_driver:=v_sm_next_state;
                                v_register_counter:=0;
                            end if;
                        end if;

                    when st_write_regs=>
                    
                        s_master_reset<='1';
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "100";
                        v_mmcm_clk_control_buffer:= p_mmcm_clk_control_in;
                        
                        for i in 0 to (c_number_of_mmcms-1) loop   
                            s_dwe(i)<='1'; --set write
                        end loop;
                        
                        --checks if the propper reg is selected in the input ports of the dram and chooses it if not 
                        if (v_daddr/=v_mmcm_clk0_reg_address(0)) then
                            for i in 0 to (c_number_of_mmcms-1) loop   
                                s_daddr(i)<=s_mmcm_clk0_reg_address(i)(0);
                                s_din(i)<=s_mmcm_clk0_reg_write(i)(0);
                            end loop;
                        else
                        --continues to loop over the rest of the registers
                        
                            for i in 0 to (c_number_of_mmcms-1) loop
                                s_din(i)<=s_mmcm_clk0_reg_write(i)(v_register_counter);
                                s_daddr(i)<=s_mmcm_clk0_reg_address(i)(v_register_counter);
                            end loop;
                                
                            if v_register_counter<c_max_register then
                                v_register_counter:=v_register_counter+1;
                            else
                                v_sm_mmcm_driver:=v_sm_next_state;
                                v_register_counter:=0;
                            end if;
                        end if;
                        
                    when st_debounce_operation=>
                    
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "101";
                        v_mmcm_clk_control_buffer:= p_mmcm_clk_control_in;
                        if (s_trigger_write_operation = '0') and (s_trigger_read_operation='0') then
                            v_sm_mmcm_driver := v_sm_next_state;
                        end if;
                        
                    when st_idle=>
                    
                        for i in 0 to (c_number_of_mmcms-1) loop   
                            s_dwe(i)<='0';
                            s_den(i)<='1';
                        end loop;
                        p_leds_out<= (s_locked(1) and s_locked(0)) & "110";
    
                        --checks for changes in the input parameters and updates the mmcm registers
                        if (v_mmcm_clk_control_buffer /= p_mmcm_clk_control_in) then
                            v_mmcm_clk_control_buffer:= p_mmcm_clk_control_in;
                            v_sm_mmcm_driver:=st_wait_unlocked;
                            v_sm_next_state:=st_write_regs;
                        end if;
                        
                        --register update
                        for i in 0 to (c_number_of_mmcms-1) loop
                            s_mmcm_clk0_reg_read_debug(i)(0) <= s_mmcm_clk0_reg_read(i)(0);
                            s_mmcm_clk0_reg_read_debug(i)(1) <= s_mmcm_clk0_reg_read(i)(1);
                        end loop;
                        
                        case s_enable_vio_debug_mode is
                            when "0" =>
                                for i in 0 to (c_number_of_mmcms-1) loop   
                                    --PHASE MUX [15:13] Chooses an initial phase offset for the clock output, the resolution is equal to 1/8 VCO period.
                                    s_mmcm_clk0_reg_write(i)(0) <= (s_mmcm_clk0_reg_read(i)(0)) and (p_mmcm_clk_control_in(i).phase_mux&"1111111111111");
                                    --DELAY TIME [5:0] [5:0] Phase offset with a resolution equal to the VCO period.
                                    s_mmcm_clk0_reg_write(i)(1) <= (s_mmcm_clk0_reg_read(i)(1)) and ("1111111111"&p_mmcm_clk_control_in(i).delay_time);
                                end loop;
                            when "1" =>
                                for i in 0 to (c_number_of_mmcms-1) loop   
                                    --PHASE MUX [15:13] Chooses an initial phase offset for the clock output, the resolution is equal to 1/8 VCO period.
                                    s_mmcm_clk0_reg_write(i)(0) <= s_mmcm_clk0_reg_write_debug(i)(0);
                                    --DELAY TIME [5:0] [5:0] Phase offset with a resolution equal to the VCO period.
                                    s_mmcm_clk0_reg_write(i)(1) <= s_mmcm_clk0_reg_write_debug(i)(1);
                                end loop;
                                    
                            when others =>
                                s_enable_vio_debug_mode<= "0" ;
                        end case;
                        
                        
                        --for advanced purpoces
                        if s_trigger_write_operation = '1' then
                            v_sm_mmcm_driver:= st_debounce_operation;
                            v_sm_next_state:= st_wait_unlocked;
                            v_register_counter:=0;
                        end if;
                        --for advanced purpoces
                        if s_trigger_read_operation = '1' then
                            v_sm_mmcm_driver:= st_debounce_operation;
                            v_sm_next_state:= st_wait_locked;
                            v_register_counter:=0;
                        end if;
                       
                                                       
                    when others =>
                        v_sm_mmcm_driver:= st_initialize;
    
                end case;
            
            end if;
        
        end if;
    end process;



--debug
--i_vio_mmcm_control_debug : vio_mmcm_control_debug
--  PORT MAP (
--    clk => p_driver_clk_in,
--    probe_in0(0) => s_locked(0),
--    probe_in1(0) => s_drdy(0),
--    probe_in2 => s_mmcm_clk0_reg_read_debug(0)(0),
--    probe_in3 => s_mmcm_clk0_reg_read_debug(0)(1),
--    probe_out0 => s_enable_vio_debug_mode,
--    probe_out1(0) => s_trigger_read_operation,
--    probe_out2(0) => s_trigger_write_operation,
--    probe_out4 => s_mmcm_clk0_reg_write_debug(0)(0),
--    probe_out3 => s_mmcm_clk0_reg_write_debug(0)(1)
--  );


--i_ila_mmcm_control_debug : ila_mmcm_control_debug
--PORT MAP (
--	clk => p_driver_clk_in,



--	probe0(0) => s_dwe(0), 
--	probe1(0) => s_den(0), 
--	probe2(0) => s_drdy(0), 
--	probe3 => s_daddr(0), 
--	probe4 => s_din(0), 
--	probe5 => s_dout(0), 
--	probe6(0) => s_trigger_read_operation,
--	probe7(0) => s_trigger_write_operation
--);


end Behavioral;
