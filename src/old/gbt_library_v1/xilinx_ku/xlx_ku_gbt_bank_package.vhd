-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific package
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief vendor_specific_gbt_bank_package - Device specific parameter (Package)
--! @details 
--! The vendor_specific_gbt_bank_package package contains the constant values used to configure the device specific parameters
--! and the record used to connect the transceiver's signals, which are device specific
package vendor_specific_gbt_bank_package is
   
   --=================================== GBT Bank setup ==================================--
   -- Device dependant configuration (modifications are not recommended)
   constant MAX_NUM_GBT_LINK                    : integer := 4;            --! Maximum number of links per bank
  
   constant GBTRX_BITSLIP_MIN_DLY               : integer := 40;           --! Minimum number of clock cycle to wait for a bitslip action
   constant GBTRX_BITSLIP_MGT_RX_RESET_DELAY    : integer := 25e4;         --! Minimum number of clock cycle to wait for an MGT reset action
  
   -- PCS WordSize dependant
   constant WORD_WIDTH                          : integer := 40;           --! MGT word size (20 [240MHz] / 40 [120MHz])
   constant GBT_WORD_RATIO                      : integer := 3;            --! Size ratio between GBT-Frame word (120bit) and MGT word size (120/WORD_WIDTH = 6 [240MHz] / 3 [120MHz])
   constant GBTRX_BITSLIP_NBR_MAX               : integer := 39;           --! Maximum number of bitslip before going back to the first position (WORD_WIDTH-1)
   constant GBT_GEARBOXWORDADDR_SIZE            : integer :=  5;           --! Size in bit of the ram address used for the gearbox [Std] : Log2((120 * 8)/WORD_WIDTH)    
   
   constant RX_GEARBOXSYNCSHIFT_COUNT           : integer := 1;            --! Number of clock cycle between the Rx gearbox and the Descrambler (multicyle to allow word decoding in more than one clock cycle). This constant shall be used to fix the multicycle constraint
   --=====================================================================================--


   

     
   
   --=====================================================================================--
   
   --================================ Record Declarations ================================--   
   
      
   --================================= Array Declarations ================================--
   type gbt_devspec_reg16_A                                     is array (natural range <>) of std_logic_vector(15 downto 0);
   type gbt_devspec_reg9_A                                      is array (natural range <>) of std_logic_vector(8 downto 0);
   type gbt_devspec_reg5_A                                      is array (natural range <>) of std_logic_vector(4 downto 0);
   type gbt_devspec_reg4_A                                      is array (natural range <>) of std_logic_vector(3 downto 0);
   type gbt_devspec_reg3_A                                      is array (natural range <>) of std_logic_vector(2 downto 0);
   
   --================================ Record Declarations ================================--   
   
   type mgtDeviceSpecific_i_R is
   record
      rx_p                                      : std_logic_vector(1 to MAX_NUM_GBT_LINK);                                 
      rx_n                                      : std_logic_vector(1 to MAX_NUM_GBT_LINK);  
      ------------------------------------------
      reset_freeRunningClock                    : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------     
      loopBack                                  : gbt_devspec_reg3_A(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------
      tx_reset                                  : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      rx_reset                                  : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------               
      conf_diffCtrl                             : gbt_devspec_reg4_A(1 to MAX_NUM_GBT_LINK);
      conf_postCursor                           : gbt_devspec_reg5_A(1 to MAX_NUM_GBT_LINK);
      conf_preCursor                            : gbt_devspec_reg5_A(1 to MAX_NUM_GBT_LINK);
      conf_txPol                                : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      conf_rxPol                                : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------      
      drp_addr                                  : gbt_devspec_reg9_A(1 to MAX_NUM_GBT_LINK);  
      drp_clk                                   : std_logic_vector(1 to MAX_NUM_GBT_LINK);  
      drp_en                                    : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      drp_di                                    : gbt_devspec_reg16_A(1 to MAX_NUM_GBT_LINK);
      drp_we                                    : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------      
      prbs_txSel                                : gbt_devspec_reg3_A(1 to MAX_NUM_GBT_LINK);
      prbs_rxSel                                : gbt_devspec_reg3_A(1 to MAX_NUM_GBT_LINK);
      prbs_txForceErr                           : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      prbs_rxCntReset                           : std_logic_vector(1 to MAX_NUM_GBT_LINK);
   end record;

   type mgtDeviceSpecific_o_R is
   record
      tx_p                                      : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      tx_n                                      : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------
      rxCdrLock                                 : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------
      rx_phMonitor                              : gbt_devspec_reg5_A(1 to MAX_NUM_GBT_LINK);
      rx_phSlipMonitor                          : gbt_devspec_reg5_A(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------
      rxWordClkReady                            : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      ------------------------------------------                  
      drp_rdy                                   : std_logic_vector(1 to MAX_NUM_GBT_LINK);
      drp_do                                    : gbt_devspec_reg16_A(1 to MAX_NUM_GBT_LINK);      
      ------------------------------------------                  
      prbs_rxErr                                : std_logic_vector(1 to MAX_NUM_GBT_LINK);
   end record;   



--=================================================================================================--
--##################################   Added by Piro         ######################################--
--=================================================================================================--

  -- Common:
	----------
	constant ENABLED										: integer := 1;
	constant DISABLED										: integer := 0;

   constant WORD_ADDR_MSB                       : integer :=  4;
   constant WORD_ADDR_PS_CHECK_MSB              : integer :=  1;
   constant GBTRX_BITSLIP_NBR_MSB               : integer :=  5;
   constant RXFRAMECLK_STEPS_MSB                : integer :=  1;
   constant RXFRAMECLK_STEPS_NBR_MAX            : integer :=  3;  
   constant GBT_READY_DLY                       : integer := 100;

   type mgtLink_i_R is
   record
      rx_p                                      : std_logic;                                 
      rx_n                                      : std_logic;         
      ------------------------------------------     
      loopBack                                  : std_logic_vector( 2 downto 0);              
      ------------------------------------------
      tx_reset                                  : std_logic; 
      rx_reset                                  : std_logic;             
      ------------------------------------------
      rxBitSlip_enable                          : std_logic; 
      rxBitSlip_ctrl                            : std_logic; 
      rxBitSlip_nbr                             : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
      rxBitSlip_run                             : std_logic;
      rxBitSlip_oddRstEn                        : std_logic;
      ------------------------------------------               
      conf_diffCtrl                             : std_logic_vector( 3 downto 0);
      conf_postCursor                           : std_logic_vector( 4 downto 0);
      conf_preCursor                            : std_logic_vector( 4 downto 0);
      conf_txPol                                : std_logic;
      conf_rxPol                                : std_logic;
      ------------------------------------------      
      drp_addr                                  : std_logic_vector( 8 downto 0);  
      drp_en                                    : std_logic;   
      drp_di                                    : std_logic_vector(15 downto 0); 
      drp_we                                    : std_logic;      
      ------------------------------------------      
      prbs_txSel                                : std_logic_vector( 2 downto 0);
      prbs_rxSel                                : std_logic_vector( 2 downto 0);
      prbs_txForceErr                           : std_logic;
      prbs_rxCntReset                           : std_logic;       
   end record;

   type mgtLink_o_R is
   record
      tx_p                                      : std_logic;
      tx_n                                      : std_logic;
      ------------------------------------------                  
      cpllLock                                  : std_logic;
      ------------------------------------------                  
      tx_resetDone                              : std_logic;      
      rx_resetDone                              : std_logic;      
      tx_fsmResetDone                           : std_logic; 
      rx_fsmResetDone                           : std_logic; 
      ------------------------------------------
      rxCdrLock                                 : std_logic; 
      ------------------------------------------
      rx_phMonitor                              : std_logic_vector(4 downto 0);
      rx_phSlipMonitor                          : std_logic_vector(4 downto 0);  
      ------------------------------------------
      rxBitSlip_oddRstNbr                       : std_logic_vector(7 downto 0);
      ------------------------------------------            
      rxWordClkReady                            : std_logic;      
      ------------------------------------------
      ready                                     : std_logic;
      ------------------------------------------                  
      drp_rdy                                   : std_logic;  
      drp_do                                    : std_logic_vector(15 downto 0);      
      ------------------------------------------                  
      prbs_rxErr                                : std_logic;
   end record;   

   type mgtCommon_i_R is
   record
      dummy_i                                   : std_logic;                                         
   end record;   
   
   type mgtCommon_o_R is
   record
      dummy_o                                   : std_logic;                                         
   end record;   
   type mgtLink_i_R_A                           is array (natural range <>) of mgtLink_i_R;                          
   type mgtLink_o_R_A                           is array (natural range <>) of mgtLink_o_R;    
   
   type mgt_i_R is
   record
      mgtCommon                                 : mgtCommon_i_R;
      mgtLink                                   : mgtLink_i_R_A(1 to MAX_NUM_GBT_LINK);
   end record;  
   
   type mgt_o_R is
   record
      mgtCommon                                 : mgtCommon_o_R;
      mgtLink                                   : mgtLink_o_R_A(1 to MAX_NUM_GBT_LINK);
   end record;  
   
  type rxReadyFsmStateLatOpt_T                 is (s0_idle, s1_rxWordClkCheck, s2_gbtRxReadyMonitoring);
   
  type gbt_bank_user_setup_R is 
   record
   
      -- Number of links:
      -------------------
      
      -- Comment:   The number of links per GBT Bank is device dependant (up to FOUR links on Kintex 7 & Virtex 7).  
      
      NUM_LINKS                                 : integer;
      
      -- GBT Bank optimization:
      -------------------------

      -- Comment:   (0 -> STANDARD | 1 -> LATENCY)  
      
      TX_OPTIMIZATION                           : integer range 0 to 1; 
      RX_OPTIMIZATION                           : integer range 0 to 1; 
      
      -- GBT encodings:
      -----------------
      
      -- Comment:   (0 -> GBT_FRAME | 1 -> WIDE_BUS | 2 -> GBT_8B10B)
      
      TX_ENCODING                               : integer range 0 to 2;
      RX_ENCODING                               : integer range 0 to 2;
      
      -- GTX reference clock:
      -----------------------
      
      -- Comment:   * Allowed STANDARD GTX frequencies: 96MHz, 120MHz, 150MHz, 160MHz, 192MHz, 200MHz, 240MHz,
      --                                                300MHz, 320MHz, 400MHz, 480MHz and 600MHz   
      --  
      --            * Note!! The reference clock frequency of the LATENCY-OPTIMIZED MGT can not be set by 
      --              the user. For Kintex 7 & Virtex 7 GTX, it is fixed to 120MHz.   
      
--    STD_MGT_REFCLK_FREQ                       : integer; 
      
      -- GTX buffer bypass alignment mode: 
      ------------------------------------
      
      RX_GTX_BUFFBYPASS_MANUAL                  : boolean;
      
      -- GTX reset FSMs reference clock:
      ----------------------------------
      
      STABLE_CLOCK_PERIOD                       : integer;
      
      -- Simulation:        
      -------------- 
      
      SIMULATION                                : boolean;
   	SIM_GTRESET_SPEEDUP                       : boolean;        

   end record;

   type gbt_bank_user_setup_R_A                 is array (natural range <>) of gbt_bank_user_setup_R;   

   --=====================================================================================-- 
end vendor_specific_gbt_bank_package;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--