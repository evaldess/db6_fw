-----------------------------------------------------------------------------------------------------------------------------
-- Company: Stockholm University
-- Engineer: Eduardo Valdes Santurio
--           Sam Silverstein
-- 
-- Create Date: 25/05/2018 11:17:52 PM
-- Design Name: 
-- Module Name: db_configbus_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity db6_configbus_interface is
    Port (     --p_db_side_in             : in std_logic_vector(1 downto 0);
               p_master_reset_in        : in    std_logic;
               p_clknet_in              : in    t_db_clock_network;
               p_configbus_clknet_out   : out   t_configbus_clock_network;
               p_configbus_local_in       : in std_logic_vector (7 downto 0);
               p_configbus_remote_in       : in std_logic_vector (7 downto 0);      
               --p_gbtx_rxready_in           : in std_logic_vector(1 downto 0); -- 0 local, 1 remote 
               --p_system_gbtx_rxready_in    : in std_logic_vector(1 downto 0); -- 0 local, 1 remote
               --p_bcr_out                   :out std_logic_vector(1 downto 0); -- 0 local, 1 remote
               --p_bcr_out                   :out std_logic;
               p_db_reg_rx_out      : out t_db_reg_rx;
               -- p_orbit_strobe_out       : out std_logic;
               --p_configbus_ready_out    : out std_logic;
               p_leds_out : out std_logic_vector(3 downto 0)
    );

end db6_configbus_interface;

architecture Behavioral of db6_configbus_interface is
    --debug
    COMPONENT vio_db_registers
      PORT (
        clk : IN STD_LOGIC;
        probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_out0 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END COMPONENT;
    
    signal s_debug_businput_value : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_debug_register_value : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_debug_config_bus_ila : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_debug_bus_ila : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_debug_register_number : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    COMPONENT ila_cfgbus_signals_debug
    
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(71 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe6 : IN STD_LOGIC_VECTOR(11 DOWNTO 0)
    );
    END COMPONENT  ;
    
    
    
    signal s_db_config_registers : t_configuration_register_array:= (
        adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
        adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
        adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
        adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
        others=>(others=>'0')
    );
    
    --signal s_use_remote_bus : std_logic := '0';  -- 0 local, 1 remote  
    --signal s_orbit_strobe_int : std_Logic := '0';
    
    --signal s_db_configbus_input_buffer : std_logic_vector(7 downto 0);
    
    signal s_db_configbus_shift_register : std_logic_vector(71 downto 0);
    
    signal s_db_configbus_register_data_buffer : std_logic_vector(31 downto 0);
    signal s_db_configbus_register_data : std_logic_vector(31 downto 0);
    
    signal s_lhc_bunches_counter : std_logic_vector(31 downto 0);
    
    signal s_orbit_cross_shift_register : std_logic_vector(2*c_lhc_bunches_between_bcr downto 0);

    signal s_orbit_cross : std_logic;
    signal s_pre_bcr : std_logic;
    signal s_pos_bcr : std_logic;
    signal s_bcr : std_logic;
    
    --signal s_valid_aligned :  std_logic_vector(3 downto 0);
    
    --signal s_data_byte_aligned : std_logic_vector(7 downto 0);
     
    signal s_data_address_buffer :std_logic_vector(15 downto 0);
    signal s_data_address : std_logic_vector(15 downto 0);
        
    signal s_data_aligned: std_logic_vector(11 downto 0);
    signal s_strobe_bit : std_logic;
    
    --signal s_vio_debug_word : std_logic_vector(31 downto 0);
    
    -- for the proc_sync_to_orbit, 
    --signal v_sm_clk40_align : std_logic_vector(1 downto 0);
    
    --clk
    signal s_clk40_cfgbus : std_logic :='1';
    signal s_clk40_cfgbus_locked : std_logic;
    signal s_watchdog_clk40_cfgbus, s_reset_watchdog_clk40_cfgbus : std_logic :='0';
    signal s_configbus_clk_net : t_configbus_clock_network;
    
    signal s_chosen_cfgbus_clk80 : std_logic := '0';
    
    attribute keep : string;
    attribute keep of s_db_config_registers : signal is "true";
    
    
begin
    
    --output ports
    p_configbus_clknet_out<=s_configbus_clk_net;
    p_leds_out(3)<=s_clk40_cfgbus_locked;
    p_leds_out(2)<=s_bcr;
    p_leds_out(1)<=s_clk40_cfgbus_locked;
    p_leds_out(0)<=s_bcr;
    
    s_configbus_clk_net.clk80_cfgbus<=p_clknet_in.clk80;
    
        proc_config_bus_shift_in_data : process(s_configbus_clk_net.clk80_cfgbus)
        begin
            
            if rising_edge(s_configbus_clk_net.clk80_cfgbus) then

                if p_master_reset_in = '0' then
                    --shift in data and orbit cross from the local or remote configbus
                    if p_clknet_in.refclksel = '0' then           
                        s_db_configbus_shift_register<= s_db_configbus_shift_register(63 downto 0) & p_configbus_local_in;
                        s_pre_bcr <=  p_configbus_local_in(7);
                        
                    else
                        s_db_configbus_shift_register<= s_db_configbus_shift_register(63 downto 0) & p_configbus_remote_in;
                        s_pre_bcr <=  p_configbus_remote_in(7);
    
                    end if;
                    --Bunch crossing
                    s_pos_bcr <= s_pre_bcr;  -- Is this buffering necessary?
                    
                    --Data buffer
                    s_db_configbus_register_data_buffer <= s_db_configbus_shift_register(15 downto 8) & s_db_configbus_shift_register(31 downto 24) & s_db_configbus_shift_register(47 downto 40) & s_db_configbus_shift_register(63 downto 56);
                    
                    --Aligned bit number and data byte (?)
                    s_data_aligned<= s_db_configbus_shift_register(70) & s_db_configbus_shift_register(54) & s_db_configbus_shift_register(38) & s_db_configbus_shift_register(22) & s_db_configbus_shift_register(65 downto 64) & s_db_configbus_shift_register(49 downto 48) & s_db_configbus_shift_register(33 downto 32) & s_db_configbus_shift_register(17 downto 16);
                    
                    -- Aligned byte address
                    s_data_address_buffer<=s_db_configbus_shift_register(69 downto 66) & s_db_configbus_shift_register(53 downto 50) & s_db_configbus_shift_register(37 downto 34) & s_db_configbus_shift_register(21 downto 18);
                
                else    
                    s_data_address_buffer<= (others=> '0');
                    s_data_aligned<= (others=> '0');
                    s_db_configbus_register_data_buffer<= (others=> '0');
                    s_db_configbus_shift_register <= (others=> '0');
                    
                end if;

            end if;
        
        end process;
        
    
        proc_get_register_data : process(s_configbus_clk_net.clk80_cfgbus)--s_clk40_cfgbus)
            variable v_cfg_db_advanced_mode_address, v_cfg_db_advanced_mode_register_value : std_logic_vector(31 downto 0);
            --variable v_cfg_db_advanced_mode_data : std_logic_vector(31 downto 0);
            type t_cfg_db_advanced_mode_sm is (st_idle, st_get_address, st_get_data,st_get_mask);
            variable sm_cfg_db_advanced_mode : t_cfg_db_advanced_mode_sm := st_idle;
            constant c_cfg_db_advanced_mode_startup_address : integer := 17;
        begin
         
         if rising_edge(s_configbus_clk_net.clk80_cfgbus) then--s_clk40_cfgbus) then
            
            --s_debug_businput_value<=s_db_configbus_register_data_buffer(0);
            --check that all clocks are locked
            if (p_clknet_in.locked_db = '1') then-- and (p_master_reset_in = '0') then
                --check alignment    
                 if s_data_aligned = "111100011011" then
                    --check address -- i check first and last byte only but can be done differently
                    if (s_data_address_buffer(15 downto 12)/="0000") and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(11 downto 8)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(7 downto 4)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(3 downto 0)) then
                         --s_db_configbus_advanced_mode_register_address
                         if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfg_db_advanced_mode,4)) then
                         
                            if (s_db_configbus_register_data_buffer = x"FFFFFFFF") then
                                sm_cfg_db_advanced_mode := st_idle;
                            else
                                case sm_cfg_db_advanced_mode is 
                                    when st_idle=>
                                        s_db_config_registers(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=x"01234567";
                                        s_strobe_bit<='0';
                                        if to_integer(unsigned(s_db_configbus_register_data_buffer)) > (c_cfg_db_advanced_mode_startup_address-1) then
                                            v_cfg_db_advanced_mode_address := s_db_configbus_register_data_buffer;
                                            sm_cfg_db_advanced_mode:= st_get_address;
                                        else
                                            sm_cfg_db_advanced_mode:=st_idle;
                                        end if;
                                    when st_get_address=>
                                        s_db_config_registers(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=s_db_configbus_register_data_buffer;
                                        s_strobe_bit<='0';
                                        if (v_cfg_db_advanced_mode_address = s_db_configbus_register_data_buffer) then
                                            sm_cfg_db_advanced_mode:= st_get_data;
                                        else
                                            sm_cfg_db_advanced_mode := st_idle;
                                        end if;
                                    when st_get_data=>
                                        s_db_config_registers(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=x"89abcdef";
                                        s_strobe_bit<='0';
                                        --s_db_configbus_register_data<=s_db_configbus_register_data_buffer;
                                        --s_data_address<=v_cfg_db_advanced_mode_address(15 downto 0);
                                        v_cfg_db_advanced_mode_register_value:=s_db_configbus_register_data_buffer;
                                        sm_cfg_db_advanced_mode := st_get_mask;
                                    when st_get_mask=>
                                            s_db_config_registers(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=x"abababab";
                                            s_strobe_bit<='1';
                                            --s_db_configbus_register_data<=s_db_configbus_register_data_buffer;
                                            --s_data_address<=v_cfg_db_advanced_mode_address(15 downto 0);
                                            if s_db_configbus_register_data_buffer = (x"00000000") then
                                                s_db_config_registers(to_integer(unsigned(v_cfg_db_advanced_mode_address)))<=v_cfg_db_advanced_mode_register_value;
                                            else
                                                s_db_config_registers(to_integer(unsigned(v_cfg_db_advanced_mode_address))) <= v_cfg_db_advanced_mode_register_value and s_db_configbus_register_data_buffer;
                                            end if;
                                            sm_cfg_db_advanced_mode := st_idle;
                                    when others=>
                                end case;
                            end if;
                         else
                             sm_cfg_db_advanced_mode := st_idle;
                             s_db_config_registers(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=s_db_configbus_register_data_buffer;
                             --s_db_configbus_register_data<=s_db_configbus_register_data_buffer;
                             --s_data_address<= "000000000000"&s_data_address_buffer(3 downto 0);
                             s_strobe_bit<='1';                         
                         end if;

    
                    end if;
                 else
                     s_strobe_bit<='0';
                 end if;
             else
             end if;
         end if;
        end process;

--old code
--   proc_move_data_to_register_array : process(s_configbus_clk_net.clk_shiftdata)--p_clknet_in.clk100)
--    begin
--        if rising_edge(s_configbus_clk_net.clk_shiftdata) then
        
----            if p_master_reset_in = '0' then
----                s_db_config_registers(bc_number)<= s_lhc_bunches_counter;
----                if (s_strobe_bit='1') then
----                    s_db_config_registers(to_integer(unsigned(s_data_address(15 downto 0))))<=s_db_configbus_register_data;
----                end if;
----            else
----                s_db_config_registers(bc_number)<= s_lhc_bunches_counter;
----            end if;
        
--        s_debug_register_value<=s_db_config_registers(to_integer(unsigned(s_debug_register_number)));
--        s_debug_businput_value <=s_strobe_bit & s_strobe_bit & p_clknet_in.locked_db & s_db_config_registers(0)(28 downto 0);
--        end if;
--    end process;


        s_configbus_clk_net.clk40_recovered<=s_clk40_cfgbus;
        s_configbus_clk_net.clk40_locked<=s_clk40_cfgbus_locked;
        --this would work but the signal wouls be only 12.5 ns long
        --s_configbus_clk_net.bcr(g)<=s_pre_bcr(g);
        
        proc_sync_to_orbit: process(s_configbus_clk_net.clk80_cfgbus)
        constant c_watchdog_tries : integer := 31;
        variable v_counter : integer := 0;
        variable v_bcr_watchdog : integer:= 0;
        type t_sm_sync_bcr is (st_get_a_bcr, st_verify_bcr_sync, st_working);
        variable st_sync_bcr : t_sm_sync_bcr :=st_get_a_bcr;

        begin
            if rising_edge(s_configbus_clk_net.clk80_cfgbus) then
                
                s_orbit_cross_shift_register<=s_orbit_cross_shift_register(2*c_lhc_bunches_between_bcr-1 downto 0) & s_pre_bcr;
                
                if (p_clknet_in.locked_db = '1') then -- and (p_master_reset_in = '0')) then

                    case st_sync_bcr is

                        when st_get_a_bcr=> -- working
                            
                            s_clk40_cfgbus<='0';
                            s_clk40_cfgbus_locked<= '0';
                            s_configbus_clk_net.bcr<='0';
                            
                            if (s_orbit_cross_shift_register(2*c_lhc_bunches_between_bcr) = '1') and (s_orbit_cross_shift_register(0) = '1') then
                                v_counter:=0;
                                v_bcr_watchdog:=0;
                                st_sync_bcr:=st_verify_bcr_sync;
    
                            else
                                v_counter:= v_counter+1;
                                v_bcr_watchdog:=0;
                                
                            end if;
                        when st_verify_bcr_sync=>  --checking for 10 consecutive bcr
                            s_clk40_cfgbus<='0';
                            s_clk40_cfgbus_locked<= '0';
                            s_configbus_clk_net.bcr<='0';
                            
                            if (v_counter<((2*c_lhc_bunches_between_bcr)-1)) then
                                v_counter:=v_counter+1;
                            else
                                v_counter:=0;
                                
                                if (s_orbit_cross_shift_register(2*c_lhc_bunches_between_bcr) = '1') and (s_orbit_cross_shift_register(0) = '1') then
                                    if v_bcr_watchdog<c_watchdog_tries then
                                        v_bcr_watchdog:=v_bcr_watchdog+1;
                                    else
                                        st_sync_bcr:=st_working;
                                        v_bcr_watchdog:=0;
                                    end if;
                                    
                                else
    
                                    st_sync_bcr:=st_get_a_bcr;
                                    v_bcr_watchdog:=0;
                                    
                                end if;
                            end if;
                        when st_working=>
                            if (v_counter<((2*c_lhc_bunches_between_bcr)-1)) then
                                v_counter:=v_counter+1;
                                s_configbus_clk_net.bcr<=s_pre_bcr;
                            else
                                v_counter:=0;
                                s_configbus_clk_net.bcr<=s_pos_bcr;
                                if (s_orbit_cross_shift_register(2*c_lhc_bunches_between_bcr) = '1') and (s_orbit_cross_shift_register(0) = '1') then
                                    st_sync_bcr:=st_working;
                                else
                                    st_sync_bcr:=st_get_a_bcr;
                                end if;
                            end if;
                            
                            s_clk40_cfgbus<= not s_clk40_cfgbus;
                            s_clk40_cfgbus_locked<= '1';
                             
                        when others=>
                            st_sync_bcr:=st_get_a_bcr;
                    end case;
                    
                    else
                        st_sync_bcr:=st_get_a_bcr;
                        s_clk40_cfgbus<='0';
                        s_clk40_cfgbus_locked<= '0';
                        s_configbus_clk_net.bcr<='0';
                    
                end if;       
            end if;
        
        end process;

    proc_br_counter:process(s_configbus_clk_net.clk40_recovered,s_configbus_clk_net.clk40_locked)
    variable v_lhc_bunches_counter : integer range 0 to c_lhc_bunches_between_bcr := 0;
    begin
        if rising_edge(s_configbus_clk_net.clk40_recovered) then
            if s_configbus_clk_net.clk40_locked = '1' then
                --p_bcr_out<=s_pre_bcr;
                s_bcr<=s_pre_bcr;
                s_lhc_bunches_counter<=std_logic_vector(to_unsigned(v_lhc_bunches_counter,32));
                if s_pre_bcr = '1' then
                    v_lhc_bunches_counter := 0;
                else
                    v_lhc_bunches_counter := v_lhc_bunches_counter +1;
                end if;
            else
                --p_bcr_out<='0';
            end if;
        end if;
    end process;



    --end generate;
    
    --assign the proper register values to the record.
    -- previously done by: p_db_config_reg_out <= s_db_config_registers;
    
    p_db_reg_rx_out.db_register_zero <= s_db_config_registers(cfg_register_zero);
    p_db_reg_rx_out.mb_adc_config <= s_db_config_registers(cfb_mb_adc_config);
    p_db_reg_rx_out.mb_phase_config <= s_db_config_registers(cfb_mb_phase_config);
    p_db_reg_rx_out.cs_config <= s_db_config_registers(cfb_cs_config);
    p_db_reg_rx_out.cs_command <= s_db_config_registers(cfb_cs_command);
    p_db_reg_rx_out.integrator_interval <= s_db_config_registers(cfb_integrator_interval);
    p_db_reg_rx_out.db_bc_num_offset <= s_db_config_registers(cfb_bc_num_offset);
    p_db_reg_rx_out.db_sem_control <= s_db_config_registers(cfb_sem_control);
    p_db_reg_rx_out.hv_control <= s_db_config_registers(cfb_hv_control);
    p_db_reg_rx_out.gbtx_reg_config <= s_db_config_registers(cfb_gbtx_reg_config);
    p_db_reg_rx_out.db_control <= s_db_config_registers(cfb_db_control);
    p_db_reg_rx_out.db_tx_control <= s_db_config_registers(cfb_tx_control);
    p_db_reg_rx_out.mb_control <= s_db_config_registers(cfb_mb_control);
    p_db_reg_rx_out.db_debug <= s_db_config_registers(cfb_db_debug);
    p_db_reg_rx_out.db_advanced_mode <= s_db_config_registers(cfb_db_debug);
    p_db_reg_rx_out.db_strobe_reg <= s_db_config_registers(cfb_strobe_reg);
    p_db_reg_rx_out.db_bc_number <= s_lhc_bunches_counter;
    p_db_reg_rx_out.db_gty_txdiffctrl <= s_db_config_registers(adv_cfg_gty_txdiffctrl);
    p_db_reg_rx_out.db_gty_txpostcursor <= s_db_config_registers(adv_cfg_gty_txpostcursor);
    p_db_reg_rx_out.db_gty_txprecursor <= s_db_config_registers(adv_cfg_gty_txprecursor);
    p_db_reg_rx_out.db_gty_txmaincursor <= s_db_config_registers(adv_cfg_gty_txmaincursor);
    p_db_reg_rx_out.db_txrawtestword <= s_db_config_registers(adv_cfg_txrawtestword);
    
     
--    p_orbit_strobe_out <= s_orbit_strobe_int;


--debug
--    i_vio_db_registers : vio_db_registers
--      PORT MAP (
--        clk => s_configbus_clk_net.clk80_cfgbus,
--        probe_in0 => (s_debug_register_number),
--        probe_in1 => s_debug_register_value,
--        probe_in2 => s_debug_businput_value,
--        probe_out0 => s_debug_register_number
--      );


--    i_ila_cfgbus_signals_debug : ila_cfgbus_signals_debug
--    PORT MAP (
--        clk => s_configbus_clk_net.clk80_cfgbus,
    
--        probe0 => s_db_configbus_shift_register, 
--        probe1 => s_db_configbus_register_data_buffer, 
--        probe2 => s_lhc_bunches_counter, 
--        probe3 => s_valid_aligned, 
--        probe4 => s_data_address_buffer, 
--        probe5 => s_data_byte_aligned,
--        probe6 => s_data_aligned
--    );

    --s_valid_aligned<= s_strobe_bit&v_sm_clk40_align&'0';
    
end Behavioral;



    