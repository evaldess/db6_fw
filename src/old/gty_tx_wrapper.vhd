----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/28/2018 09:01:41 PM
-- Design Name: 
-- Module Name: gty_tx_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

library tilecal;
use tilecal.db6_design_package.all;

ENTITY gty_tx_wrapper is
  PORT (
    p_gty_clknet_in                         : in t_db_gty_clock_inputs;
    p_oscclk_in  :     IN STD_LOGIC; -- 100 MHz oscillator clock for monitoring, reset
    p_clk40_locked_in                            : in  std_logic;
    p_clk40_in:        IN STD_LOGIC;
    p_tx_reset_in : IN STD_LOGIC;
    p_userclk_tx_srcclk_out : OUT STD_LOGIC;
    p_userclk_tx_usrclk_out : OUT STD_LOGIC;
    p_userclk_tx_usrclk2_out : OUT STD_LOGIC;
    p_userclk_tx_active_out : OUT STD_LOGIC;
    p_gty_txready_out           : out STD_LOGIC;
    p_qpll1lock_out         : out STD_LOGIC;
    p_userdata_tx_in : IN STD_LOGIC_VECTOR(159 DOWNTO 0);
    --p_gtrefclk0_in : IN STD_LOGIC;
    --p_gtrefclk1_in : IN STD_LOGIC;
    p_qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 downto 0);
    p_gtytxn_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    p_gtytxp_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    
    --transceiver tuning
    p_gty_tx_control_in : in t_gty_tx_control 
  );
END gty_tx_wrapper;

Architecture structural of gty_tx_wrapper is

-- Convert input clock signals to buffers

signal s_gtrefclk0_in_buf, s_gtrefclk1_in_buf : std_logic_vector(0 downto 0);
--signal s_gtrefclk0_in_buf1, s_gtrefclk1_in_buf1 : std_logic_vector(0 downto 0);


COMPONENT gty_tx
  PORT (
    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk11_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll0pd_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1pd_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    cpllpd_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtyrxn_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtyrxp_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxpd_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txdiffctrl_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txelecidle_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    txinhibit_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    txmaincursor_in : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    txpd_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpostcursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txprecursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtytxn_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtytxp_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END COMPONENT;
  
  signal s_refclk_loc, s_refclk_rem : std_logic_vector(0 downto 0);

  signal s_gtpowergood, s_txuserrdy, s_gttxreset, s_cpllreset : std_logic_vector(1 downto 0) := "00";
  signal s_txresetdone : std_logic_vector(1 downto 0) := "00";
  signal s_tx_reset_powerup, s_tx_reset_clkchange, s_tx_reset, s_reset_bypass, s_manual_reset : std_logic_vector(0 downto 0);
  --signal s_trigger_tx_reset : std_logic := '0';
  signal s_userclk_reset : std_logic_vector(0 downto 0) := "0";
  signal s_reset_buff_bypass : std_logic_vector(0 downto 0) := "0";
  signal s_tx_reset_done : std_logic_vector(0 downto 0) := "0";
  signal s_qpll1lock : std_logic_vector(0 downto 0) := "0";
  signal s_txpmaresetdone, s_txprgdivresetdone: std_logic_vector(1 downto 0);
  signal s_oscclk_buf, s_userclk_tx_usrclk_buf, s_userclk_tx_usrclk2_buf, s_userclk_tx_srcclk_buf : std_logic_vector(0 downto 0) := "0";
  signal s_userclk_tx_active : std_logic_vector(0 downto 0) := "0";
  signal s_qpll1refclk_lastsel : std_logic_vector(2 downto 0);
  signal s_tx_active_out : std_logic:= '0';
  signal s_gty_txready_out : std_logic:= '0';
  
  signal s_word_inject_mode : std_logic;
  signal s_word_to_inject, s_userdata_tx_in : STD_LOGIC_VECTOR(159 DOWNTO 0);
  
  constant c_txdiffctrl : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1011010110"; -- Driver swing control (10110 = 809 mV)
  constant c_txpostcursor : STD_LOGIC_VECTOR(9 DOWNTO 0) :=  "1000010000"; -- Post-cursor TX pre-emphasis (10000 = 4.44 dB)
  constant c_txprecursor : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1000010000"; -- Pre-cursor TX pre-emphasis (1000 = 4.44 dB)
  constant c_txswing : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- TX swing control (0 for full-swing)

  signal s_txdiffctrl : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1011010110"; -- Driver swing control (10110 = 809 mV)
  signal s_txpostcursor : STD_LOGIC_VECTOR(9 DOWNTO 0) :=  "1000010000"; -- Post-cursor TX pre-emphasis (10000 = 4.44 dB)
  signal s_txprecursor : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1000010000"; -- Pre-cursor TX pre-emphasis (1000 = 4.44 dB)
  signal s_txswing : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- use for pciexpress
  signal s_txmaincursor : STD_LOGIC_VECTOR(13 DOWNTO 0) := "10000001000000"; -- main cursos for signal swing control
  signal s_txelecidle  : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- set tx outputs to common mode
  signal s_txinhibit : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- set tx outputs to 1

  signal s_txdiffctrl_from_vio : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1011010110"; -- Driver swing control (10110 = 809 mV)
  signal s_txpostcursor_from_vio : STD_LOGIC_VECTOR(9 DOWNTO 0) :=  "1000010000"; -- Post-cursor TX pre-emphasis (10000 = 4.44 dB)
  signal s_txprecursor_from_vio : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1000010000"; -- Pre-cursor TX pre-emphasis (1000 = 4.44 dB)
  signal s_txswing_from_vio : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- TX swing control (0 for full-swing)
  

begin

    s_oscclk_buf(0)<=p_oscclk_in;
    s_refclk_loc <= p_gty_clknet_in.gty_refclk_loc;
    s_refclk_rem <= p_gty_clknet_in.gty_refclk_rem;
    
    p_userclk_tx_usrclk_out <= s_userclk_tx_usrclk_buf(0);
    p_userclk_tx_usrclk2_out <= s_userclk_tx_usrclk2_buf(0);
    p_gty_txready_out <= s_gty_txready_out;
    --s_trigger_tx_reset <= p_tx_reset_in;-- or s_tx_reset_powerup or s_tx_reset_clkchange;

 --data mux
 proc_data_mux:process(s_word_inject_mode)
 begin
     if s_word_inject_mode = '1' then
         s_userdata_tx_in<=s_word_to_inject;
     else
         s_userdata_tx_in<=p_userdata_tx_in;
     end if;
 end process;
 
 proc_gty_reset_sync : process (p_oscclk_in)
        type sm_gty is (st_get_clk40_locked,st_get_powergood, st_release_reset, st_wait_lock,st_wait_reset_done, st_buffreset, st_idle);
        variable st_gty : sm_gty := st_get_powergood;
        variable v_counter : integer := 0;
        constant c_reset_timeout : integer := 500000;
        constant c_powergood_timeout : integer := 200000;
        constant c_tx_reset_timeout: integer := 200000;
        constant c_buffer_reset_timeout : integer := 2000000;
    begin
        if rising_edge(p_oscclk_in) then
            p_qpll1lock_out<=s_qpll1lock(0);
            if (p_tx_reset_in = '0') and (s_manual_reset = "0") then
                v_counter := v_counter + 1;
                case st_gty is
                    when st_get_clk40_locked =>  -- wait for GTPOWERGOOD, then assert tx_reset
                     
                        s_gty_txready_out <= '0';
                        s_tx_reset <= "1";
                        if (p_clk40_locked_in='1') then
                            st_gty := st_get_powergood;
                        end if;
                    when st_get_powergood =>  -- wait for GTPOWERGOOD, then assert tx_reset
                        s_gty_txready_out <= '0';
                        s_tx_reset <= "1";
                        if (s_gtpowergood = "11") then
                            st_gty := st_release_reset;
                            v_counter := 0;
                        else
                            if v_counter>c_powergood_timeout then                             
                                if (s_gtpowergood(0) = '1') or (s_gtpowergood(1) = '1') then
                                    st_gty := st_release_reset;
                                    v_counter := 0;
                                end if;
                            end if;
                        end if;
                    when st_release_reset =>  -- wait 5 us, then deassert tx_reset
                        s_gty_txready_out <= '0'; 
                        if v_counter > c_reset_timeout then
                            s_tx_reset <= "0";
                            st_gty := st_wait_lock;
                            v_counter := 0;
                        end if;
                    when st_wait_lock =>  -- wait for tx reset to be done, then reset buffer bypass
                        s_tx_reset <= "0";
                        s_gty_txready_out <= '0';
                        if s_qpll1lock = "1" then
                            v_counter := 0;
                            st_gty := st_wait_reset_done;
                        end if;                    
                    when st_wait_reset_done =>
                        s_tx_reset <= "0";
                        s_gty_txready_out <= '0';
                        if s_txresetdone ="11" then
                            v_counter := 0;
                            st_gty := st_buffreset;
                        else
                            if v_counter>c_tx_reset_timeout then
                                if (s_txresetdone(0) or s_txresetdone(1)) = '1' then
                                    v_counter := 0;
                                    st_gty := st_buffreset;
                                end if;
                            end if;
                        end if;
                    when st_buffreset => -- deassert GTTXRESET and wait for 20 us
                        s_tx_reset <= "0";
                        s_gty_txready_out <= '0';
                        if v_counter > c_buffer_reset_timeout then
                            s_reset_bypass <= "0"; -- optional signal for buffer bypass
                            st_gty := st_idle;
                        else
                            s_reset_bypass <= "1";
                        end if;
                    when st_idle => -- idle state
                        s_tx_reset <= "0";
                        s_gty_txready_out <= '1';
                    when others=>
                end case;
            else
                
                s_gty_txready_out <= '0';
                s_tx_reset <= "1";
                st_gty := st_get_clk40_locked;
            end if;
        end if; -- clock edge
    end process;

i_gty_tx : gty_tx
  PORT MAP (
    gtwiz_userclk_tx_reset_in => s_tx_reset,
    gtwiz_userclk_tx_srcclk_out => s_userclk_tx_srcclk_buf,
    gtwiz_userclk_tx_usrclk_out => s_userclk_tx_usrclk_buf,
    gtwiz_userclk_tx_usrclk2_out => s_userclk_tx_usrclk2_buf,
    gtwiz_userclk_tx_active_out => s_userclk_tx_active,
    gtwiz_userclk_rx_reset_in => "1", --keep reset state
    gtwiz_userclk_rx_srcclk_out => open,
    gtwiz_userclk_rx_usrclk_out => open,
    gtwiz_userclk_rx_usrclk2_out => open,
    gtwiz_userclk_rx_active_out => open,
    gtwiz_buffbypass_tx_reset_in => "0",
    gtwiz_buffbypass_tx_start_user_in => "1",
    gtwiz_buffbypass_tx_done_out => open,
    gtwiz_buffbypass_tx_error_out => open,
    gtwiz_buffbypass_rx_reset_in => "1",
    gtwiz_buffbypass_rx_start_user_in => "0",
    gtwiz_buffbypass_rx_done_out => open,
    gtwiz_buffbypass_rx_error_out => open,
    gtwiz_reset_clk_freerun_in(0) => p_oscclk_in,
    gtwiz_reset_all_in => s_tx_reset,
    gtwiz_reset_tx_pll_and_datapath_in => s_tx_reset,
    gtwiz_reset_tx_datapath_in => s_tx_reset,
    gtwiz_reset_rx_pll_and_datapath_in => "1",
    gtwiz_reset_rx_datapath_in => "1",
    gtwiz_reset_rx_cdr_stable_out => open,
    gtwiz_reset_tx_done_out => s_tx_reset_done,
    gtwiz_reset_rx_done_out => open,
    gtwiz_userdata_tx_in => s_userdata_tx_in,
    gtwiz_userdata_rx_out => open,
    gtrefclk01_in => s_refclk_loc,
    gtrefclk11_in => s_refclk_rem,
    qpll0pd_in => "1",
    qpll1pd_in => "0",
    qpll1refclksel_in => p_qpll1refclksel_in,
    qpll1lock_out => s_qpll1lock,
    qpll1outclk_out => open,
    qpll1outrefclk_out => open,
    cpllpd_in => "11",
    gtyrxn_in => "00",
    gtyrxp_in => "00",
    rxpd_in => "1111",
    txdiffctrl_in => p_gty_tx_control_in.txdiffctrl,
    txelecidle_in => s_txelecidle,
    txinhibit_in => s_txinhibit,
    txmaincursor_in => p_gty_tx_control_in.txmaincursor,
    txpd_in => "0000",
    txpostcursor_in => p_gty_tx_control_in.txpostcursor,
    txprecursor_in => p_gty_tx_control_in.txprecursor,
    gtpowergood_out => s_gtpowergood,
    gtytxn_out => p_gtytxn_out,
    gtytxp_out => p_gtytxp_out,
    rxpmaresetdone_out => open,
    txpmaresetdone_out => open,
    txprgdivresetdone_out => open,
    txresetdone_out => s_txresetdone
  );





END structural;
