----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2018 01:11:47 PM
-- Design Name: 
-- Module Name: mmcm_osc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mmcm_osc_v1 is
    Port ( p_clk_p_in : in STD_LOGIC;
           p_clk_n_in : in STD_LOGIC;
           --p_clk560_out : out STD_LOGIC;
           p_clk400_out : out STD_LOGIC;
           --p_clk280_out : out STD_LOGIC;
           --p_clk160_out : out STD_LOGIC;
           p_clk100_out : out STD_LOGIC;
           --p_clk80_out : out std_logic;
           p_clk40_out : out std_logic;
           --p_clk16_out : out std_logic;
           p_clk10_out : out std_logic;
           --p_clk8_out : out STD_LOGIC;
           p_clk1_out : out STD_LOGIC;
           p_clk100khz_out : out STD_LOGIC;
           p_clk100hz_out : out STD_LOGIC;
           p_clk10hz_out : out STD_LOGIC;
           p_clk1hz_out : out STD_LOGIC;
           p_clk5hz_out : out STD_LOGIC;
           p_clk2_5hz_out : out STD_LOGIC);
end mmcm_osc_v1;

architecture Behavioral of mmcm_osc_v1 is

  component mmcm_osc_clk
  port
   (-- Clock in ports
    -- Clock out ports
    --p_clk_8_out          : out    std_logic;
    --p_clk_16_out          : out    std_logic;
    p_clk_40_out          : out    std_logic;
    --p_clk_80_out          : out    std_logic;
    --p_clk_160_out          : out    std_logic;
    p_clk_400_out          : out    std_logic;
    --p_oscclk_in           : in     std_logic
    clk_in1_p         : in     std_logic;
    clk_in1_n         : in     std_logic
   );
  end component;

component pll_osc_clk
port
 (-- Clock in ports
  -- Clock out ports
  p_clk_40_out          : out    std_logic;
  p_clk_400_out          : out    std_logic;
  clk_in1_p         : in     std_logic;
  clk_in1_n         : in     std_logic
 );
end component;
  

signal s_clk400_osc, s_clk560_osc, s_clk280_osc, s_clk160_osc,s_clk40_osc,s_clk80_osc,s_clk16_osc, s_clk8_osc, s_clk1_osc, s_clk100khz_osc,s_clk100_osc, s_clk10_osc, s_clk100hz_osc, s_clk1hz_osc, s_clk5hz_osc, s_clk10hz_osc, s_clk2_5hz_osc : std_logic := '0';

begin


i_mmcm_osc_clk : mmcm_osc_clk
   port map ( 
  -- Clock out ports
   --p_clk_8_out => s_clk8_osc,
   --p_clk_16_out => s_clk16_osc,
   p_clk_40_out => s_clk40_osc,
   --p_clk_80_out => s_clk80_osc,  
   --p_clk_160_out => s_clk160_osc,
   p_clk_400_out => s_clk400_osc,
   -- Clock in ports
   --p_oscclk_in => p_clkinputs_in.osc
   clk_in1_p => p_clk_p_in,
   clk_in1_n => p_clk_n_in
 );



-- i_pll_osc_clk : pll_osc_clk
--    port map ( 
---- Clock out ports  
-- p_clk_40_out => s_clk40_osc,
-- p_clk_400_out => s_clk400_osc,
-- -- Clock in ports
-- clk_in1_p =>p_clk_p_in,
-- clk_in1_n =>p_clk_n_in
--);

--p_clk560_out <= s_clk560_osc;
p_clk400_out <= s_clk400_osc;
--p_clk280_out <= s_clk280_osc;
--p_clk160_out <= s_clk160_osc;
p_clk100_out <= s_clk100_osc;
--p_clk80_out <= s_clk80_osc;
p_clk40_out <= s_clk40_osc;
--p_clk16_out <= s_clk16_osc;
p_clk10_out <= s_clk10_osc;
p_clk1_out <= s_clk1_osc;
--p_clk8_out <= s_clk8_osc;
p_clk100khz_out <= s_clk100khz_osc;
p_clk100hz_out <= s_clk100hz_osc;
p_clk1hz_out <= s_clk1hz_osc;
p_clk10hz_out <= s_clk10hz_osc;
p_clk5hz_out <= s_clk5hz_osc;
p_clk2_5hz_out <= s_clk2_5hz_osc;



-- i_clk_osc : ibufgds

-- generic map (
--    diff_term => true, -- differential termination
--    iostandard => "sub_lvds")
-- port map (
--    o => s_osc_gbuf_i,  -- clock buffer output
--    i => p_osc_p_in,  -- diff_p clock buffer input (connect directly to top-level port)
--    ib => p_osc_n_in -- diff_n clock buffer input (connect directly to top-level port)
-- );

--i_osc_buf : bufg port map (I => s_osc_gbuf_i, O => s_osc_gbuf_o);





-- generate 100 MHz clock
proc_generate_100MHz : process(s_clk400_osc)
variable v_counter: std_logic :='0';
begin
    if rising_edge(s_clk400_osc) then
        if v_counter = '0' then
            v_counter:= '1';
        else
            v_counter:= '0';
            s_clk100_osc <= not s_clk100_osc;
        end if;
    end if;
end process;

-- generate 10 MHz slow clock
proc_generate_10MHz : process(s_clk40_osc)
variable v_counter: std_logic :='0';
begin
    if rising_edge(s_clk40_osc) then
        if v_counter = '0' then
            v_counter:= '1';
        else
            v_counter:= '0';
            s_clk10_osc <= not s_clk10_osc;
        end if;
    end if;
end process;


-- generate 1 MHz slow clock
proc_generate_1MHz : process(s_clk10_osc)
variable v_counter: integer :=0;
begin
    if rising_edge(s_clk10_osc) then
        if v_counter<4 then
            v_counter:= v_counter + 1;
        else
            v_counter:= 0;
            s_clk1_osc <= not s_clk1_osc;
        end if;
    end if;
end process;

-- generate 100 kHz slow clock
proc_generate_100khz : process(s_clk1_osc)
variable v_counter: integer :=0;
begin
    if rising_edge(s_clk1_osc) then
        if v_counter< 4 then
            v_counter:= v_counter + 1;
        else
            v_counter:= 0;
            s_clk100khz_osc <= not s_clk100khz_osc;
        end if;
    end if;
end process;

-- generate 100 Hz slow clock
proc_generate_100hz : process(s_clk100khz_osc)
variable v_counter: integer :=0;
begin
    if rising_edge(s_clk100khz_osc) then
        if v_counter< 499 then
            v_counter:= v_counter + 1;
        else
            v_counter:= 0;
            s_clk100hz_osc <= not s_clk100hz_osc;
        end if;
    end if;
end process;

-- generate 10 Hz slow clock
proc_generate_0_1hz : process(s_clk100hz_osc)
variable v_counter: integer :=0;
begin
    if rising_edge(s_clk100hz_osc) then
        if v_counter<4 then
            v_counter:= v_counter + 1;
        else
            v_counter:= 0;
            s_clk10hz_osc <= not s_clk10hz_osc;
        end if;
    end if;
end process;



-- generate 1 Hz slow clock
proc_generate_1hz : process(s_clk10hz_osc)
variable v_counter: integer :=0;
begin
    if rising_edge(s_clk10hz_osc) then
        if v_counter<4 then
            v_counter:= v_counter + 1;
        else
            v_counter:= 0;
            s_clk1hz_osc <= not s_clk1hz_osc;
        end if;
    end if;
end process;

-- generate 5 Hz slow clock
proc_generate_5hz : process(s_clk10hz_osc)
begin
    if rising_edge(s_clk10hz_osc) then
            s_clk5hz_osc <= not s_clk5hz_osc;
    end if;
end process;

-- generate 2.5 Hz slow clock
proc_generate_2_5hz : process(s_clk5hz_osc)
begin
    if rising_edge(s_clk5hz_osc) then
            s_clk2_5hz_osc <= not s_clk2_5hz_osc;
    end if;
end process;

end Behavioral;
