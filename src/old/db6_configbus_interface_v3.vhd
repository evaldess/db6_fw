-----------------------------------------------------------------------------------------------------------------------------
-- Company: Stockholm University
-- Engineer: Eduardo Valdes Santurio
--           Sam Silverstein
-- 
-- Create Date: 25/05/2018 11:17:52 PM
-- Design Name: 
-- Module Name: db_configbus_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity db6_configbus_interface_v3 is
    Port (     --p_db_side_in             : in std_logic_vector(1 downto 0);
               p_master_reset_in        : in    std_logic;
               p_clknet_in              : in    t_db_clock_network;
               p_configbus_clknet_out  : out   t_configbus_clock_network;
               p_configbus_local_in       : in std_logic_vector (7 downto 0);
               p_configbus_remote_in       : in std_logic_vector (7 downto 0);      
               p_db_reg_rx_out      : out t_db_reg_rx;
               p_db_reg_rx_opposite_fpga_out      : out t_db_reg_rx;
               p_mb_jtag_out : out t_mb_jtag;
               p_commbus_remote_in : in t_commbus;
               p_leds_out : out std_logic_vector(3 downto 0)
    );

end db6_configbus_interface_v3;


architecture Behavioral of db6_configbus_interface_v3 is   
    
    
    constant c_db_reg_rx : t_db_reg_rx:= ( 
    cfb_tx_control => "11111000000000000000000000001111",
    cfb_db_reg_mask => (others=>'1'),
    adv_cfg_gth_txdiffctrl =>"00000000000000000000001000010000",
    adv_cfg_gth_txpostcursor =>"00000000000000000000001000010000",
    adv_cfg_gth_txprecursor =>"00000000000000000000001000010000",
    adv_cfg_gth_txmaincursor =>"00000000000000000010000001000000",
    adc_config_module => "00000000000000000000000000011100",
    adc_readout_idelay3_0 => "00000100000000000000010000000000",
    adc_readout_idelay3_1 => "00000100000000000000010000000000",
    adc_readout_idelay3_2 => "00000100000000000000010000000000",
    adc_readout_idelay3_3 => "00000100000000000000010000000000",
    adc_readout_idelay3_4 => "00000100000000000000010000000000",
    adc_readout_idelay3_5 => "00000100000000000000010000000000",

    others=>(others=>'0')
    );
    
 
    signal s_db_reg_rx, s_db_reg_rx_configbus_loc, s_db_reg_rx_configbus_rem, s_db_reg_rx_commbus : t_db_reg_rx:= c_db_reg_rx;
--    (
--    cfb_tx_control => "00000000000000000000000000001111",
--    adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
--    adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
--    adv_cfg_firmware_version => c_fw_version,
--    others=>(others=>'0')
--    );

--    signal s_db_reg_rx_buffer : t_db_reg_rx:= (
--    adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
--    adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
--    adv_cfg_firmware_version => c_fw_version,
--    others=>(others=>'0')
--    );
    --signal s_db_reg_rx_commbus : t_db_reg_rx;
    signal s_db_reg_rx_opposite_fpga : t_db_reg_rx;
    signal s_configbus_local_delayed, s_configbus_remote_delayed : std_logic_vector(7 downto 0);
    type t_configbus_bitslice is array (7 downto 0) of std_logic_vector(1 downto 0);
    signal s_configbus_local_iddr, s_configbus_remote_iddr : t_configbus_bitslice;
    
--    IOBs for when the pis are propperly connected
--    attribute IOB: string;
--    attribute keep: string;
--    attribute dont_touch: string;
--    attribute IOB of s_configbus_local_iddr, s_configbus_remote_iddr, s_configbus_local_delayed, s_configbus_remote_delayed: signal is "TRUE";
--    attribute keep of s_configbus_local_iddr, s_configbus_remote_iddr, s_configbus_local_delayed, s_configbus_remote_delayed: signal is "TRUE";
--    attribute dont_touch of s_configbus_local_iddr, s_configbus_remote_iddr, s_configbus_local_delayed, s_configbus_remote_delayed : signal is "TRUE";
    
    --signal s_use_remote_bus : std_logic := '0';  -- 0 local, 1 remote  
    --signal s_orbit_strobe_int : std_Logic := '0';
    
    --signal s_db_configbus_input_buffer : std_logic_vector(7 downto 0);
    
    signal s_db_configbus_shift_register_local, s_db_configbus_shift_register_remote : std_logic_vector(71 downto 0);
    
    signal s_db_configbus_register_data_buffer_local, s_db_configbus_register_data_buffer_remote : std_logic_vector(31 downto 0);
    signal s_db_configbus_register_data : std_logic_vector(31 downto 0);
    
    signal s_lhc_bunches_counter : std_logic_vector(31 downto 0);
    signal s_strobe_reg, s_strobe_reg_loc, s_strobe_reg_rem, s_strobe_reg_cfgbus, s_strobe_reg_commbus : std_logic_vector(31 downto 0);
    signal s_strobe_lenght : std_logic_vector(7 downto 0);
    signal s_strobe_data : std_logic_vector(23 downto 0);
    
    signal s_orbit_cross_shift_register : std_logic_vector(2*c_lhc_bunches_between_bcr downto 0);

    signal s_orbit_cross : std_logic;
    signal s_pre_bcr : std_logic;
    signal s_pos_bcr : std_logic;
    signal s_bcr : std_logic;
    signal s_bcr_bit_loc, s_bcr_bit_rem : std_logic;
    signal s_bcr_locked_loc, s_bcr_locked_rem : std_logic;
    signal s_lhc_bunch_counter_loc, s_lhc_bunch_counter_rem : std_logic_vector(31 downto 0);
    
    signal s_data_address_buffer_local, s_data_address_buffer_remote :std_logic_vector(7 downto 0);
    signal s_mb_jtag_buffer_local, s_mb_jtag_buffer_remote :std_logic_vector(7 downto 0);
    signal s_mb_jtag, s_mb_jtag_loc, s_mb_jtag_rem : t_mb_jtag;
    --signal s_data_address : std_logic_vector(15 downto 0);
        
    signal s_data_aligned_local, s_data_aligned_remote: std_logic_vector(11 downto 0);
    signal s_strobe_bit_configbus_loc, s_strobe_bit_configbus_rem, s_strobe_bit_commbus : std_logic;
    
  
    --clk
    --signal s_clk40_cfgbus : std_logic :='1';
    --signal s_clk40_cfgbus_locked : std_logic;
--    signal s_watchdog_clk40_cfgbus, s_reset_watchdog_clk40_cfgbus : std_logic :='0';
    signal s_configbus_clk_net : t_configbus_clock_network;
    
--    signal s_chosen_cfgbus_clk80 : std_logic := '0';
    
--    COMPONENT i_vio_configbus_debug_v2
--      PORT (
--        clk : IN STD_LOGIC;
--        probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in7 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in8 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in9 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in11 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in12 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in13 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in14 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in15 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in16 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in17 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in18 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in19 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in20 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in21 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in22 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in23 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in24 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in25 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in26 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in27 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in28 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in29 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in30 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in31 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in32 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in33 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
        
--      );
--    END COMPONENT;

--COMPONENT ila_strobe_reg_debug

--PORT (
--	clk : IN STD_LOGIC;



--	probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--	probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
--	probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--	probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
--);
--END COMPONENT  ;


--COMPONENT ila_configbus_iddr

--PORT (
--	clk : IN STD_LOGIC;



--	probe0 : IN STD_LOGIC_VECTOR(71 DOWNTO 0); 
--	probe1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0); 
--	probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--	probe3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
--);
--END COMPONENT  ;


--signal s_ila_debug_counter : std_logic_vector(7 downto 0);

begin
    gen_configbus_inputs: for i in 0 to 7 generate
    
    i_idelaye3_configbus_local_in : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 400.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => open, -- 9-bit output: counter value output
        dataout => s_configbus_local_delayed(i), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk400_osc, -- 1-bit input: clock input
        cntvaluein => "000000000", -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => '1', -- 1-bit input: keep delay constant over vt
        idatain => p_configbus_local_in(i), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => '0', -- 1-bit input: load delay_value input
        rst => p_master_reset_in -- 1-bit input: asynchronous reset to the delay_value
);

i_iddre1_configbus_local_in : iddre1
generic map (
    ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
    is_c_inverted => '0', -- optional inversion for c
    is_cb_inverted => '1' -- optional inversion for c
)
port map (
    q1 => s_configbus_local_iddr(i)(1), -- 1-bit output: registered parallel output 1
    q2 => s_configbus_local_iddr(i)(0), -- 1-bit output: registered parallel output 2
    c => p_clknet_in.clk40_cfgbus_loc(0), -- 1-bit input: high-speed clock
    cb => p_clknet_in.clk40_cfgbus_loc(0), -- 1-bit input: inversion of high-speed clock c
    d => s_configbus_local_delayed(i), -- 1-bit input: serial data input
    r => p_master_reset_in -- 1-bit input: active high async reset
);

    i_idelaye3_configbus_remote_in : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "fixed", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 400.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => open, -- 9-bit output: counter value output
        dataout => s_configbus_remote_delayed(i), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => p_clknet_in.clk400_osc, -- 1-bit input: clock input
        cntvaluein => "000000000", -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => '1', -- 1-bit input: keep delay constant over vt
        idatain => p_configbus_remote_in(i), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => '0', -- 1-bit input: load delay_value input
        rst => p_master_reset_in -- 1-bit input: asynchronous reset to the delay_value
);

i_iddre1_configbus_remote_in : iddre1
generic map (
    ddr_clk_edge => "SAME_EDGE_PIPELINED", -- iddre1 mode (opposite_edge, same_edge, same_edge_pipelined)
    is_c_inverted => '0', -- optional inversion for c
    is_cb_inverted => '1' -- optional inversion for c
)
port map (
    q1 => s_configbus_remote_iddr(i)(1), -- 1-bit output: registered parallel output 1
    q2 => s_configbus_remote_iddr(i)(0), -- 1-bit output: registered parallel output 2
    c => p_clknet_in.clk40_cfgbus_rem(0), -- 1-bit input: high-speed clock
    cb => p_clknet_in.clk40_cfgbus_rem(0), -- 1-bit input: inversion of high-speed clock c
    d => s_configbus_remote_delayed(i), -- 1-bit input: serial data input
    r => p_master_reset_in -- 1-bit input: active high async reset
);

end generate;
    
    --output ports
    p_configbus_clknet_out<=s_configbus_clk_net;
    p_leds_out(3)<=s_configbus_clk_net.bcr_locked;
    p_leds_out(2)<=s_configbus_clk_net.bcr;
    p_leds_out(1)<=s_configbus_remote_iddr(7)(1);
    p_leds_out(0)<=s_configbus_local_iddr(7)(1);
    --p_db_reg_rx_out<=s_db_reg_rx_configbus;
    
    i_BUFGMUX_CTRL_clk40 : BUFGMUX_CTRL
    port map (
        O => s_configbus_clk_net.clk40_cfgbus(0), -- 1-bit output: Clock output
        I0 => p_clknet_in.clk40_cfgbus_rem(0), -- 1-bit input: Clock input (S=0)
        I1 => p_clknet_in.clk40_cfgbus_loc(0), -- 1-bit input: Clock input (S=1)
        S => p_clknet_in.refclksel -- 1-bit input: Clock select
    );

    i_BUFGMUX_CTRL_clk80 : BUFGMUX_CTRL
    port map (
        O => s_configbus_clk_net.clk80_cfgbus(0), -- 1-bit output: Clock output
        I0 => p_clknet_in.clk80_cfgbus_rem(0), -- 1-bit input: Clock input (S=0)
        I1 => p_clknet_in.clk80_cfgbus_loc(0), -- 1-bit input: Clock input (S=1)
        S => p_clknet_in.refclksel -- 1-bit input: Clock select
    );
    
    --s_configbus_clk_net.clk80_cfgbus<=p_clknet_in.clk80;
    --s_configbus_clk_net.clk40_cfgbus<=p_clknet_in.clk40_cfgbus;
    
        --s_bcr_bit <= s_pre_bcr or s_pos_bcr;
        proc_config_bus_shift_in_data_local : process(p_clknet_in.clk40_cfgbus_loc(0))
        begin
            
            if rising_edge(p_clknet_in.clk40_cfgbus_loc(0)) then

                if p_master_reset_in = '0' then
                    --shift in data and orbit cross

                    s_db_configbus_shift_register_local<= s_db_configbus_shift_register_local(55 downto 0) & 
                    s_configbus_local_iddr(7)(1)& s_configbus_local_iddr(6)(1)& s_configbus_local_iddr(5)(1)& s_configbus_local_iddr(4)(1) &
                    s_configbus_local_iddr(3)(1)& s_configbus_local_iddr(2)(1)& s_configbus_local_iddr(1)(1)& s_configbus_local_iddr(0)(1) &
                    s_configbus_local_iddr(7)(0)& s_configbus_local_iddr(6)(0)& s_configbus_local_iddr(5)(0)& s_configbus_local_iddr(4)(0) &
                    s_configbus_local_iddr(3)(0)& s_configbus_local_iddr(2)(0)& s_configbus_local_iddr(1)(0)& s_configbus_local_iddr(0)(0);
                    

                    
                    --data buffer
                    s_db_configbus_register_data_buffer_local <= s_db_configbus_shift_register_local(15-8 downto 8-8) & s_db_configbus_shift_register_local(31-8 downto 24-8) & 
                    s_db_configbus_shift_register_local(47-8 downto 40-8) & s_db_configbus_shift_register_local(63-8 downto 56-8);
                    
              
                    --valid_bit and byte number alignment
                    s_data_aligned_local <= s_db_configbus_shift_register_local(70-8) & s_db_configbus_shift_register_local(54-8) & s_db_configbus_shift_register_local(38-8) & 
                    s_db_configbus_shift_register_local(22-8) & s_db_configbus_shift_register_local(65-8 downto 64-8) & s_db_configbus_shift_register_local(49-8 downto 48-8) & 
                    s_db_configbus_shift_register_local(33-8 downto 32-8) & s_db_configbus_shift_register_local(17-8 downto 16-8);
                    s_data_address_buffer_local<=s_db_configbus_shift_register_local(69-8 downto 66-8) & s_db_configbus_shift_register_local(53-8 downto 50-8);-- & s_db_configbus_shift_register_local(37 downto 34) & s_db_configbus_shift_register_local(21 downto 18);
                    s_mb_jtag_buffer_local<= s_db_configbus_shift_register_local(37-8 downto 34-8) & s_db_configbus_shift_register_local(21-8 downto 18-8);


                else    
                    s_data_address_buffer_local<= (others=> '0');
                    s_data_aligned_local<= (others=> '0');
                    s_db_configbus_register_data_buffer_local<= (others=> '0');
                    s_db_configbus_shift_register_local <= (others=> '0');
                    
                end if;

            end if;
        
        end process;
        
        proc_config_bus_shift_in_data_remote : process(p_clknet_in.clk40_cfgbus_rem(0))
        begin
            
            if rising_edge(p_clknet_in.clk40_cfgbus_rem(0)) then

                if p_master_reset_in = '0' then
                    --shift in data and orbit cross

                    s_db_configbus_shift_register_remote<= s_db_configbus_shift_register_remote(55 downto 0) & 
                    s_configbus_remote_iddr(7)(1)& s_configbus_remote_iddr(6)(1)& s_configbus_remote_iddr(5)(1)& s_configbus_remote_iddr(4)(1) &
                    s_configbus_remote_iddr(3)(1)& s_configbus_remote_iddr(2)(1)& s_configbus_remote_iddr(1)(1)& s_configbus_remote_iddr(0)(1) &
                    s_configbus_remote_iddr(7)(0)& s_configbus_remote_iddr(6)(0)& s_configbus_remote_iddr(5)(0)& s_configbus_remote_iddr(4)(0) &
                    s_configbus_remote_iddr(3)(0)& s_configbus_remote_iddr(2)(0)& s_configbus_remote_iddr(1)(0)& s_configbus_remote_iddr(0)(0);
                    

                    
                    --data buffer
                    s_db_configbus_register_data_buffer_remote <= s_db_configbus_shift_register_remote(15-8 downto 8-8) & s_db_configbus_shift_register_remote(31-8 downto 24-8) & 
                    s_db_configbus_shift_register_remote(47-8 downto 40-8) & s_db_configbus_shift_register_remote(63-8 downto 56-8);
                    
              
                    --valid_bit and byte number alignment
                    s_data_aligned_remote <= s_db_configbus_shift_register_remote(70-8) & s_db_configbus_shift_register_remote(54-8) & s_db_configbus_shift_register_remote(38-8) & 
                    s_db_configbus_shift_register_remote(22-8) & s_db_configbus_shift_register_remote(65-8 downto 64-8) & s_db_configbus_shift_register_remote(49-8 downto 48-8) & 
                    s_db_configbus_shift_register_remote(33-8 downto 32-8) & s_db_configbus_shift_register_remote(17-8 downto 16-8);
                    s_data_address_buffer_remote<=s_db_configbus_shift_register_remote(69-8 downto 66-8) & s_db_configbus_shift_register_remote(53-8 downto 50-8);-- & s_db_configbus_shift_register_remote(37 downto 34) & s_db_configbus_shift_register_remote(21 downto 18);
                    s_mb_jtag_buffer_remote<= s_db_configbus_shift_register_remote(37-8 downto 34-8) & s_db_configbus_shift_register_remote(21-8 downto 18-8);


                else    
                    s_data_address_buffer_remote<= (others=> '0');
                    s_data_aligned_remote<= (others=> '0');
                    s_db_configbus_register_data_buffer_remote<= (others=> '0');
                    s_db_configbus_shift_register_remote <= (others=> '0');
                    
                end if;

            end if;
        
        end process;

        proc_register_data_configbus_loc : process(p_clknet_in.clk40_cfgbus_loc(0))
        begin
         
         if rising_edge(p_clknet_in.clk40_cfgbus_loc(0)) then--s_clk40_cfgbus) then
            
               
            --check alignment    
             if s_data_aligned_local = "111100011011" then
                --assign mb jtag signals

                s_mb_jtag_loc.tck <= s_mb_jtag_buffer_local(3);
                s_mb_jtag_loc.tms <= s_mb_jtag_buffer_local(2);
                s_mb_jtag_loc.tdi <= s_mb_jtag_buffer_local(1);
                --check address -- i check first and last byte only but can be done differently
                --if (s_data_address_buffer(15 downto 12)/="0000") and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(11 downto 8)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(7 downto 4)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(3 downto 0)) then
                     --s_db_configbus_advanced_mode_register_address
                     

                     
                     if s_data_address_buffer_local(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_advanced_reg_value,4)) then
                        s_db_reg_rx_configbus_loc(to_integer(unsigned(s_db_reg_rx(cfb_db_advanced_reg_address))))<=
                        (s_db_configbus_register_data_buffer_local and s_db_reg_rx(cfb_db_reg_mask))
                        or (s_db_reg_rx(to_integer(unsigned(s_db_reg_rx(cfb_db_advanced_reg_address)))) and (not s_db_reg_rx(cfb_db_reg_mask)));
                        s_strobe_bit_configbus_loc<='0';
                     
                     elsif s_data_address_buffer_local(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_reg_mask,4)) then
                        s_db_reg_rx_configbus_loc(to_integer(unsigned(s_data_address_buffer_local(3 downto 0))))<=s_db_configbus_register_data_buffer_local;
                        s_strobe_bit_configbus_loc<='0';
                     elsif s_data_address_buffer_local(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_advanced_reg_address,4)) then
                        s_db_reg_rx_configbus_loc(to_integer(unsigned(s_data_address_buffer_local(3 downto 0))))<=(s_db_configbus_register_data_buffer_local);                   
                        
                     elsif s_data_address_buffer_local(3 downto 0) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                        s_strobe_bit_configbus_loc<='1';
                        s_db_reg_rx_configbus_loc(to_integer(unsigned(s_data_address_buffer_local(3 downto 0))))<=s_db_configbus_register_data_buffer_local;
                        
                     else
                        s_strobe_bit_configbus_loc<='0'; 
                        s_db_reg_rx_configbus_loc(to_integer(unsigned(s_data_address_buffer_local(3 downto 0))))<=
                            (s_db_configbus_register_data_buffer_local and s_db_reg_rx(cfb_db_reg_mask))
                            or (s_db_reg_rx(to_integer(unsigned(s_data_address_buffer_local(3 downto 0)))) and (not s_db_reg_rx(cfb_db_reg_mask)));   
                     
                     end if;
                 end if;

            end if;
    end process;

    proc_register_data_configbus_rem : process(p_clknet_in.clk40_cfgbus_rem(0))
        begin
         
         if rising_edge(p_clknet_in.clk40_cfgbus_rem(0)) then--s_clk40_cfgbus) then
                --check alignment    
                 if s_data_aligned_remote = "111100011011" then
                    --assign mb jtag signals

                    s_mb_jtag_rem.tck <= s_mb_jtag_buffer_remote(3);
                    s_mb_jtag_rem.tms <= s_mb_jtag_buffer_remote(2);
                    s_mb_jtag_rem.tdi <= s_mb_jtag_buffer_remote(1);
                    --check address -- i check first and last byte only but can be done differently
                    --if (s_data_address_buffer(15 downto 12)/="0000") and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(11 downto 8)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(7 downto 4)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(3 downto 0)) then
                         --s_db_configbus_advanced_mode_register_address
                         

                         
                         if s_data_address_buffer_remote(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_advanced_reg_value,4)) then
                            s_db_reg_rx_configbus_rem(to_integer(unsigned(s_db_reg_rx(cfb_db_advanced_reg_address))))<=
                            (s_db_configbus_register_data_buffer_remote and s_db_reg_rx(cfb_db_reg_mask))
                            or (s_db_reg_rx(to_integer(unsigned(s_db_reg_rx(cfb_db_advanced_reg_address)))) and (not s_db_reg_rx(cfb_db_reg_mask)));
                            s_strobe_bit_configbus_rem<='0';
                         
                         elsif s_data_address_buffer_remote(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_reg_mask,4)) then
                            s_db_reg_rx_configbus_rem(to_integer(unsigned(s_data_address_buffer_remote(3 downto 0))))<=s_db_configbus_register_data_buffer_remote;
                            s_strobe_bit_configbus_rem<='0';
                         elsif s_data_address_buffer_remote(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_advanced_reg_address,4)) then
                            s_db_reg_rx_configbus_rem(to_integer(unsigned(s_data_address_buffer_remote(3 downto 0))))<=(s_db_configbus_register_data_buffer_remote);                   
                            
                         elsif s_data_address_buffer_remote(3 downto 0) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                            s_strobe_bit_configbus_rem<='1';
                            s_db_reg_rx_configbus_rem(to_integer(unsigned(s_data_address_buffer_remote(3 downto 0))))<=s_db_configbus_register_data_buffer_remote;
                            
                         else
                            s_strobe_bit_configbus_rem<='0'; 
                            s_db_reg_rx_configbus_rem(to_integer(unsigned(s_data_address_buffer_remote(3 downto 0))))<=
                                (s_db_configbus_register_data_buffer_remote and s_db_reg_rx(cfb_db_reg_mask))
                                or (s_db_reg_rx(to_integer(unsigned(s_data_address_buffer_remote(3 downto 0)))) and (not s_db_reg_rx(cfb_db_reg_mask)));   
                         
                         end if;

                end if;
 
             end if;

        end process;


    proc_strobe_manager_loc : process(p_clknet_in.clk40_cfgbus_loc(0))
    variable v_counter : integer := 0;
    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
    variable v_reg_buffer : std_logic_vector(31 downto 0);
    variable v_strobe_lenght : integer := 0;
    variable v_clk_domain_cross_counter : integer :=0;
    constant c_domain_cross_ratio : integer := 40000000/100;
    begin
        if rising_edge(p_clknet_in.clk40_cfgbus_loc(0)) then

             case sm_strobe_manager is
                when st_idle=>
                    if (s_strobe_bit_configbus_loc='1') then
                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                        v_counter := 0;
                        sm_strobe_manager := st_wait_for_strobe_bit;
                        v_reg_buffer:= s_db_reg_rx_configbus_loc(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx_configbus_loc(cfb_strobe_reg)(31 downto 16)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));
                    end if;
                    s_strobe_reg_loc <= (others=>'0');
                when st_wait_for_strobe_bit =>
                    v_clk_domain_cross_counter:=0;
                    v_counter:=0;
                    if (s_strobe_bit_configbus_loc='0') then
                        sm_strobe_manager:=st_propagate_strobe;
                    else
                        sm_strobe_manager := st_wait_for_strobe_bit;
                    end if;
                when st_propagate_strobe =>
                    if v_clk_domain_cross_counter < c_domain_cross_ratio then
                        v_clk_domain_cross_counter := v_clk_domain_cross_counter +1;
                        s_strobe_reg_loc <= v_reg_buffer;
                    else
                        v_clk_domain_cross_counter:=0;
                        if v_counter < v_strobe_lenght then
                            v_counter:=v_counter+1;
                            s_strobe_reg_loc <= v_reg_buffer;
                        else
                            sm_strobe_manager := st_idle;
                            v_counter :=0;
                            s_strobe_reg_loc <= (others=>'0');
                        end if;
                    end if;
                when others=>
                    sm_strobe_manager := st_idle;
            end case;
        end if;

    end process;
    
    proc_strobe_manager_rem : process(p_clknet_in.clk40_cfgbus_rem(0))
    variable v_counter : integer := 0;
    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
    variable v_reg_buffer : std_logic_vector(31 downto 0);
    variable v_strobe_lenght : integer := 0;
    variable v_clk_domain_cross_counter : integer :=0;
    constant c_domain_cross_ratio : integer := 40000000/100;
    begin
        if rising_edge(p_clknet_in.clk40_cfgbus_rem(0)) then

             case sm_strobe_manager is
                when st_idle=>
                    if (s_strobe_bit_configbus_rem='1') then
                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                        v_counter := 0;
                        sm_strobe_manager := st_wait_for_strobe_bit;
                        v_reg_buffer:= s_db_reg_rx_configbus_rem(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx_configbus_rem(cfb_strobe_reg)(31 downto 16)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));
                    end if;
                    s_strobe_reg_rem <= (others=>'0');
                when st_wait_for_strobe_bit =>
                    v_clk_domain_cross_counter:=0;
                    v_counter:=0;
                    if (s_strobe_bit_configbus_rem='0') then
                        sm_strobe_manager:=st_propagate_strobe;
                    else
                        sm_strobe_manager := st_wait_for_strobe_bit;
                    end if;
                when st_propagate_strobe =>
                    if v_clk_domain_cross_counter < c_domain_cross_ratio then
                        v_clk_domain_cross_counter := v_clk_domain_cross_counter +1;
                        s_strobe_reg_rem <= v_reg_buffer;
                    else
                        v_clk_domain_cross_counter:=0;
                        if v_counter < v_strobe_lenght then
                            v_counter:=v_counter+1;
                            s_strobe_reg_rem <= v_reg_buffer;
                        else
                            sm_strobe_manager := st_idle;
                            v_counter :=0;
                            s_strobe_reg_rem <= (others=>'0');
                        end if;
                    end if;
                when others=>
                    sm_strobe_manager := st_idle;
            end case;
        end if;

    end process;

        
        proc_sync_to_orbit_loc: process(p_clknet_in.clk40_cfgbus_loc(0))
        constant c_watchdog_tries : integer := 31;
        variable v_counter : integer := 0;
        variable v_maxcount : integer := 0;
        variable v_bcr_watchdog : integer:= 0;
        type t_sm_sync_bcr is (st_get_a_bcr, st_verify_bcr_sync, st_working);
        variable st_sync_bcr : t_sm_sync_bcr :=st_get_a_bcr;

        begin
            

            s_bcr_bit_loc<= s_configbus_local_iddr(7)(1);
            
            if rising_edge(p_clknet_in.clk40_cfgbus_loc(0)) then
                if s_bcr_bit_loc = '1' then
                    v_maxcount := v_counter;
                    if (v_maxcount = (c_lhc_bunches_between_bcr-1)) then
                        s_bcr_locked_loc <='1';
                    else
                        s_bcr_locked_loc <='0';
                    end if;
                    v_counter := 0;                
                else
                    v_counter := v_counter + 1;
                end if;
                
                s_lhc_bunch_counter_loc <= std_logic_vector(to_unsigned(v_counter,32));
                
                
            end if; -- Clock edge
        
        end process; -- proc_sync_to_orbit
        
        proc_sync_to_orbit_rem: process(p_clknet_in.clk40_cfgbus_rem(0))
        constant c_watchdog_tries : integer := 31;
        variable v_counter : integer := 0;
        variable v_maxcount : integer := 0;
        variable v_bcr_watchdog : integer:= 0;
        type t_sm_sync_bcr is (st_get_a_bcr, st_verify_bcr_sync, st_working);
        variable st_sync_bcr : t_sm_sync_bcr :=st_get_a_bcr;

        begin
            

            s_bcr_bit_rem<= s_configbus_remote_iddr(7)(1);
            
            if rising_edge(p_clknet_in.clk40_cfgbus_rem(0)) then
                if s_bcr_bit_rem = '1' then
                    v_maxcount := v_counter;
                    if (v_maxcount = (c_lhc_bunches_between_bcr-1)) then
                        s_bcr_locked_rem <='1';
                    else
                        s_bcr_locked_rem <='0';
                    end if;
                    v_counter := 0;                
                else
                    v_counter := v_counter + 1;
                end if;
                
                s_lhc_bunch_counter_rem <= std_logic_vector(to_unsigned(v_counter,32));
                
                
            end if; -- Clock edge
        
        end process; -- proc_sync_to_orbit

    
    --assign the propper register values to the record previously done by: p_db_config_reg_out <= s_db_config_registers;
    --p_db_reg_rx_out<=s_db_config_registers;
    
    proc_mux_all : process(p_clknet_in.refclksel)
    begin
        if p_clknet_in.refclksel = '0' then
            s_db_reg_rx<=s_db_reg_rx_configbus_rem;
            p_db_reg_rx_out(bc_number)<=s_lhc_bunch_counter_rem;
            p_mb_jtag_out<= s_mb_jtag_rem;
            s_strobe_reg<=s_strobe_reg_rem;
            s_configbus_clk_net.bcr <= s_bcr_bit_rem;
            s_configbus_clk_net.bcr <= s_bcr_locked_rem;
        else
            s_db_reg_rx<=s_db_reg_rx_configbus_loc;
            p_db_reg_rx_out(bc_number)<=s_lhc_bunch_counter_loc;
            p_mb_jtag_out<= s_mb_jtag_loc;
            s_strobe_reg<=s_strobe_reg_loc;
            s_configbus_clk_net.bcr <= s_bcr_bit_loc;
            s_configbus_clk_net.bcr <= s_bcr_locked_loc;
        end if;
    end process;
    
    

    p_db_reg_rx_out(0 to cfb_strobe_reg-1) <= s_db_reg_rx(0 to cfb_strobe_reg-1);
    
    p_db_reg_rx_out(cfb_strobe_reg) <= s_strobe_reg;
    p_db_reg_rx_out(bc_number+1 to c_number_of_cfb_bus_regs) <= s_db_reg_rx(bc_number+1 to c_number_of_cfb_bus_regs);
    p_db_reg_rx_opposite_fpga_out<= s_db_reg_rx_opposite_fpga;
    
--   i_ila_strobe_reg_debug : ila_strobe_reg_debug
--    PORT MAP (
--        clk => p_clknet_in.clk40_osc,
    
--        probe0(0) => s_strobe_bit, 
--        probe1(31 downto 24) => s_strobe_lenght,
--        probe1(23 downto 0) => s_strobe_data,
--        probe2 => s_strobe_reg,
--        probe3 => s_ila_debug_counter
--    );

   
   
--   p_db_reg_rx_out.db_register_zero <= s_db_config_registers(cfg_register_zero);
--   p_db_reg_rx_out.mb_adc_config <= s_db_config_registers(cfb_mb_adc_config);
--   p_db_reg_rx_out.mb_phase_config <= s_db_config_registers(cfb_mb_phase_config);
--   p_db_reg_rx_out.cs_config <= s_db_config_registers(cfb_cs_config);
--   p_db_reg_rx_out.cs_command <= s_db_config_registers(cfb_cs_command);
--   p_db_reg_rx_out.integrator_interval <= s_db_config_registers(cfb_integrator_interval);
--   p_db_reg_rx_out.db_bc_num_offset <= s_db_config_registers(cfb_bc_num_offset);
--   p_db_reg_rx_out.db_sem_control <= s_db_config_registers(cfb_sem_control);
--   p_db_reg_rx_out.hv_control <= s_db_config_registers(cfb_hv_control);
--   p_db_reg_rx_out.gbtx_reg_config <= s_db_config_registers(cfb_gbtx_reg_config);
--   p_db_reg_rx_out.db_control <= s_db_config_registers(cfb_db_control);
--   p_db_reg_rx_out.db_tx_control <= s_db_config_registers(cfb_tx_control);
--   p_db_reg_rx_out.mb_control <= s_db_config_registers(cfb_mb_control);
--   p_db_reg_rx_out.db_debug <= s_db_config_registers(cfb_db_debug);
--   p_db_reg_rx_out.db_advanced_mode <= s_db_config_registers(cfb_db_debug);
--   p_db_reg_rx_out.db_strobe_reg <= s_db_config_registers(cfb_strobe_reg);
--   p_db_reg_rx_out.db_bc_number <= s_lhc_bunch_counter;
--   p_db_reg_rx_out.db_gty_txdiffctrl <= s_db_config_registers(adv_cfg_gty_txdiffctrl);
--   p_db_reg_rx_out.db_gty_txpostcursor <= s_db_config_registers(adv_cfg_gty_txpostcursor);
--   p_db_reg_rx_out.db_gty_txprecursor <= s_db_config_registers(adv_cfg_gty_txprecursor);
--   p_db_reg_rx_out.db_gty_txmaincursor <= s_db_config_registers(adv_cfg_gty_txmaincursor);
--   p_db_reg_rx_out.db_txrawtestword <= s_db_config_registers(adv_cfg_txrawtestword);
    
--i_i_vio_configbus_debug_v2 : i_vio_configbus_debug_v2
--  PORT MAP (
--    clk => p_clknet_in.clk40,
--    probe_in0 => s_db_reg_rx(0),
--    probe_in1 => s_db_reg_rx(1),
--    probe_in2 => s_db_reg_rx(2),
--    probe_in3 => s_db_reg_rx(3),
--    probe_in4 => s_db_reg_rx(4),
--    probe_in5 => s_db_reg_rx(5),
--    probe_in6 => s_db_reg_rx(6),
--    probe_in7 => s_db_reg_rx(7),
--    probe_in8 => s_db_reg_rx(8),
--    probe_in9 => s_db_reg_rx(9),
--    probe_in10 => s_db_reg_rx(10),
--    probe_in11 => s_db_reg_rx(11),
--    probe_in12 => s_db_reg_rx(12),
--    probe_in13 => s_db_reg_rx(13),
--    probe_in14 => s_db_reg_rx(14),
--    probe_in15 => s_db_reg_rx(15),
--    probe_in16 => s_db_reg_rx(16),
--    probe_in17 => s_db_reg_rx(17),
--    probe_in18 => s_db_reg_rx(18),
--    probe_in19 => s_db_reg_rx(19),
--    probe_in20 => s_db_reg_rx(20),
--    probe_in21 => s_db_reg_rx(21),
--    probe_in22 => s_db_reg_rx(22),
--    probe_in23 => s_db_reg_rx(23),
--    probe_in24 => s_db_reg_rx(24),
--    probe_in25 => s_db_reg_rx(25),
--    probe_in26 => s_db_reg_rx(26),
--    probe_in27 => s_db_reg_rx(27),
--    probe_in28 => s_db_reg_rx(28),
--    probe_in29 => s_db_reg_rx(29),
--    probe_in30 => s_db_reg_rx(30),                                                  
--    probe_in31 => s_db_reg_rx(31),
--    probe_in32 => s_db_reg_rx(32),
--    probe_in33 => s_db_reg_rx(33)
    
    
--  );


--i_ila_configbus_iddr : ila_configbus_iddr
--PORT MAP (
--	clk => s_configbus_clk_net.clk40_cfgbus,


--	probe0 => s_db_configbus_shift_register, 
--	probe1 => s_data_aligned, 
--	probe2 => s_data_address_buffer,
--	probe3 => s_db_configbus_register_data_buffer
--);
        
end Behavioral;



    