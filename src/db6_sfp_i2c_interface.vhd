----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/06/2020 01:05:44 PM
-- Design Name: 
-- Module Name: db6_sfp_i2c_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library tilecal;
use tilecal.db6_design_package.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity db6_sfp_i2c_interface is
  port ( 
    p_clknet_in 		: in t_db_clknet;
    p_master_reset_in  : in std_logic;
    p_db_reg_rx_in : in t_db_reg_rx;
    p_gbt_encoder_interface_in        : in t_gbt_encoder_interface;
    
    p_sfp_control_in : in t_sfp_control;
    
    p_sfp_abs_in                : in std_logic_vector(1 downto 0);
    p_sfp_los_in                : in std_logic_vector(1 downto 0);
    p_sfp_tx_fault_in                : in std_logic_vector(1 downto 0);    
    
    p_sfp_i2c_interface_out : out t_sfp_interface;
    p_scl_inout 	: inout std_logic_vector(1 downto 0);
    p_sda_inout    : inout std_logic_vector(1 downto 0);
        
    p_leds_out : out std_logic_vector(3 downto 0)
  
  );
end db6_sfp_i2c_interface;

architecture Behavioral of db6_sfp_i2c_interface is

COMPONENT blk_mem_tdpram_1kx18_8b256
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    clkb : IN STD_LOGIC;
    rstb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    sleep : IN STD_LOGIC;
    rsta_busy : OUT STD_LOGIC;
    rstb_busy : OUT STD_LOGIC
  );
END COMPONENT;


signal s_sfp0_dpram_read, s_sfp0_dpram_write : t_sfp_dpram_tmr;
signal s_sfp1_dpram_read, s_sfp1_dpram_write : t_sfp_dpram_tmr;

signal s_sfp0_dpram, s_sfp1_dpram : t_sfp_dpram;

type t_sfp_i2c is array (1 downto 0) of t_i2c; 
signal s_i2c : t_sfp_i2c;

signal s_leds_out : std_logic_vector(3 downto 0);

begin


gen_tmr : for tmr in 0 to 2 generate

    s_sfp0_dpram_read(tmr).clka <= p_clknet_in.clk80;
    s_sfp0_dpram_read(tmr).clkb <= p_clknet_in.clk80;
    s_sfp0_dpram_read(tmr).sleep <= '0';
    
    s_sfp0_dpram_write(tmr).clka <= p_clknet_in.clk80;
    s_sfp0_dpram_write(tmr).clkb <= p_clknet_in.clk80;
    s_sfp0_dpram_write(tmr).sleep <= '0';
    
    s_sfp1_dpram_read(tmr).clka <= p_clknet_in.clk80;
    s_sfp1_dpram_read(tmr).clkb <= p_clknet_in.clk80;
    s_sfp1_dpram_read(tmr).sleep <= '0';
    
    s_sfp1_dpram_write(tmr).clka <= p_clknet_in.clk80;
    s_sfp1_dpram_write(tmr).clkb <= p_clknet_in.clk80;
    s_sfp1_dpram_write(tmr).sleep <= '0';
    
    
    i_t_sfp0_dpram_read : blk_mem_tdpram_1kx18_8b256
      PORT MAP (
        clka => s_sfp0_dpram_read(tmr).clka,
        rsta => s_sfp0_dpram_read(tmr).rsta,
        ena => s_sfp0_dpram_read(tmr).ena,
        wea => s_sfp0_dpram_read(tmr).wea,
        addra => s_sfp0_dpram_read(tmr).addra,
        dina => s_sfp0_dpram_read(tmr).dina,
        douta => s_sfp0_dpram_read(tmr).douta,
        clkb => s_sfp0_dpram_read(tmr).clkb,
        rstb => s_sfp0_dpram_read(tmr).rstb,
        enb => s_sfp0_dpram_read(tmr).enb,
        web => s_sfp0_dpram_read(tmr).web,
        addrb => s_sfp0_dpram_read(tmr).addrb,
        dinb => s_sfp0_dpram_read(tmr).dinb,
        doutb => s_sfp0_dpram_read(tmr).doutb,
        sleep => s_sfp0_dpram_read(tmr).sleep
      );
    
    i_t_sfp1_dpram_read : blk_mem_tdpram_1kx18_8b256
      PORT MAP (
        clka => s_sfp1_dpram_read(tmr).clka,
        rsta => s_sfp1_dpram_read(tmr).rsta,
        ena => s_sfp1_dpram_read(tmr).ena,
        wea => s_sfp1_dpram_read(tmr).wea,
        addra => s_sfp1_dpram_read(tmr).addra,
        dina => s_sfp1_dpram_read(tmr).dina,
        douta => s_sfp1_dpram_read(tmr).douta,
        clkb => s_sfp1_dpram_read(tmr).clkb,
        rstb => s_sfp1_dpram_read(tmr).rstb,
        enb => s_sfp1_dpram_read(tmr).enb,
        web => s_sfp1_dpram_read(tmr).web,
        addrb => s_sfp1_dpram_read(tmr).addrb,
        dinb => s_sfp1_dpram_read(tmr).dinb,
        doutb => s_sfp1_dpram_read(tmr).doutb,
        sleep => s_sfp1_dpram_read(tmr).sleep
      );
    
    
    i_t_sfp0_dpram_write : blk_mem_tdpram_1kx18_8b256
      PORT MAP (
        clka => s_sfp0_dpram_write(tmr).clka,
        rsta => s_sfp0_dpram_write(tmr).rsta,
        ena => s_sfp0_dpram_write(tmr).ena,
        wea => s_sfp0_dpram_write(tmr).wea,
        addra => s_sfp0_dpram_write(tmr).addra,
        dina => s_sfp0_dpram_write(tmr).dina,
        douta => s_sfp0_dpram_write(tmr).douta,
        clkb => s_sfp0_dpram_write(tmr).clkb,
        rstb => s_sfp0_dpram_write(tmr).rstb,
        enb => s_sfp0_dpram_write(tmr).enb,
        web => s_sfp0_dpram_write(tmr).web,
        addrb => s_sfp0_dpram_write(tmr).addrb,
        dinb => s_sfp0_dpram_write(tmr).dinb,
        doutb => s_sfp0_dpram_write(tmr).doutb,
        sleep => s_sfp0_dpram_write(tmr).sleep
      );
    
    i_t_sfp1_dpram_write : blk_mem_tdpram_1kx18_8b256
      PORT MAP (
        clka => s_sfp1_dpram_write(tmr).clka,
        rsta => s_sfp1_dpram_write(tmr).rsta,
        ena => s_sfp1_dpram_write(tmr).ena,
        wea => s_sfp1_dpram_write(tmr).wea,
        addra => s_sfp1_dpram_write(tmr).addra,
        dina => s_sfp1_dpram_write(tmr).dina,
        douta => s_sfp1_dpram_write(tmr).douta,
        clkb => s_sfp1_dpram_write(tmr).clkb,
        rstb => s_sfp1_dpram_write(tmr).rstb,
        enb => s_sfp1_dpram_write(tmr).enb,
        web => s_sfp1_dpram_write(tmr).web,
        addrb => s_sfp1_dpram_write(tmr).addrb,
        dinb => s_sfp1_dpram_write(tmr).dinb,
        doutb => s_sfp1_dpram_write(tmr).doutb,
        sleep => s_sfp1_dpram_write(tmr).sleep
      );
    
    
    s_sfp0_dpram_write(tmr).rsta <= s_sfp0_dpram.rsta;
    s_sfp0_dpram_write(tmr).rstb <= s_sfp0_dpram.rstb;
    s_sfp0_dpram_read(tmr).rsta <= s_sfp0_dpram.rsta;
    s_sfp0_dpram_read(tmr).rstb <= s_sfp0_dpram.rstb;
    
    s_sfp1_dpram_write(tmr).rsta <= s_sfp0_dpram.rsta;
    s_sfp1_dpram_write(tmr).rstb <= s_sfp0_dpram.rstb;
    s_sfp1_dpram_read(tmr).rsta <= s_sfp0_dpram.rsta;
    s_sfp1_dpram_read(tmr).rstb <= s_sfp0_dpram.rstb;
    
    s_sfp0_dpram_write(tmr).ena <= s_sfp0_dpram.ena;
    s_sfp0_dpram_write(tmr).enb <= s_sfp0_dpram.enb;
    s_sfp0_dpram_read(tmr).ena <= s_sfp0_dpram.ena;
    s_sfp0_dpram_read(tmr).enb <= s_sfp0_dpram.enb;
    
    s_sfp1_dpram_write(tmr).ena <= s_sfp0_dpram.ena;
    s_sfp1_dpram_write(tmr).enb <= s_sfp0_dpram.enb;
    s_sfp1_dpram_read(tmr).ena <= s_sfp0_dpram.ena;
    s_sfp1_dpram_read(tmr).enb <= s_sfp0_dpram.enb;
    
    s_sfp0_dpram_write(tmr).addra <= s_sfp0_dpram.addra;
    s_sfp0_dpram_write(tmr).addrb <= s_sfp0_dpram.addrb;
    s_sfp0_dpram_read(tmr).addra <= s_sfp0_dpram.addra;
    s_sfp0_dpram_read(tmr).addrb <= s_sfp0_dpram.addrb;
    
    s_sfp1_dpram_write(tmr).addra <= s_sfp0_dpram.addra;
    s_sfp1_dpram_write(tmr).addrb <= s_sfp0_dpram.addrb;
    s_sfp1_dpram_read(tmr).addra <= s_sfp0_dpram.addra;
    s_sfp1_dpram_read(tmr).addrb <= s_sfp0_dpram.addrb;
    
    
    s_sfp0_dpram_read(tmr).dina <= s_sfp0_dpram.dina;
    s_sfp1_dpram_read(tmr).dina <= s_sfp1_dpram.dina;
    
end generate;

s_sfp0_dpram.douta <= (s_sfp0_dpram_write(0).douta and s_sfp0_dpram_write(1).douta) or (s_sfp0_dpram_write(1).douta and s_sfp0_dpram_write(2).douta) or (s_sfp0_dpram_write(2).douta and s_sfp0_dpram_write(0).douta);
s_sfp1_dpram.douta <= (s_sfp1_dpram_write(0).douta and s_sfp1_dpram_write(1).douta) or (s_sfp1_dpram_write(1).douta and s_sfp1_dpram_write(2).douta) or (s_sfp1_dpram_write(2).douta and s_sfp1_dpram_write(0).douta);

s_sfp0_dpram.doutb <= (s_sfp0_dpram_write(0).doutb and s_sfp0_dpram_write(1).doutb) or (s_sfp0_dpram_write(1).doutb and s_sfp0_dpram_write(2).doutb) or (s_sfp0_dpram_write(2).doutb and s_sfp0_dpram_write(0).doutb);
s_sfp1_dpram.doutb <= (s_sfp1_dpram_write(0).doutb and s_sfp1_dpram_write(1).doutb) or (s_sfp1_dpram_write(1).doutb and s_sfp1_dpram_write(2).doutb) or (s_sfp1_dpram_write(2).doutb and s_sfp1_dpram_write(0).doutb);



-----------------------------------------------------
-- main state machine to control gbtx              --
-----------------------------------------------------
main_sm : process(p_clknet_in.clk40)
    type t_control_sm is (st_initialize, st_idle, st_busy, st_stop, st_wait_for_bus_free, st_set_reset, st_set_address_msb, st_set_address_lsb, st_trigger_register_operation, st_debounce_trigger);
    variable v_control_sm : t_control_sm := st_initialize;
    variable v_counter : integer:=0;
    
    type t_write_gbtx_sm is (st_initialize, st_busy, st_idle);
    variable v_write_gbtx_sm : t_write_gbtx_sm:= st_idle;
    
    variable v_i2c_operation : std_logic;
    
    variable v_i2c_busy : std_logic;
    
    variable v_start_register, v_register_index, v_end_register : integer range 0 to 255 := 0;
    variable v_sfp0_register_index, v_sfp1_register_index : integer range 0 to 255 := 0;
    variable v_start_register_address, v_end_register_address : std_logic_vector(15 downto 0);
    
    variable v_sfp0_data_buffer, v_sfp1_data_buffer : std_logic_vector(7 downto 0);

begin

    if rising_edge (p_clknet_in.clk40) then
        
        if p_master_reset_in = '1' then
            v_control_sm:= st_initialize;
        end if;
        
        v_i2c_busy:=s_i2c(0).busy_out or s_i2c(1).busy_out;
        

        s_sfp0_dpram.addrb <= std_logic_vector(to_unsigned(v_sfp0_register_index,8));
        s_sfp0_dpram.dinb <= (others=>'0');
        s_sfp1_dpram.addrb <= std_logic_vector(to_unsigned(v_sfp1_register_index,8));
        s_sfp1_dpram.dinb <= (others=>'0');
        
        s_sfp0_dpram.enb <= '1';
        s_sfp0_dpram.web <= "0";
        s_sfp1_dpram.enb <= '1';
        s_sfp1_dpram.web <= "0";
        
        if p_gbt_encoder_interface_in.sc_address = c_db_reg_tx_lut(stb_sfp0_reg) then
            if v_sfp0_register_index < 255 then
                v_sfp0_register_index:=v_sfp0_register_index+1;
            else
                v_sfp0_register_index:=0;
            end if;
        elsif p_gbt_encoder_interface_in.sc_address = c_db_reg_tx_lut(stb_sfp1_reg) then
            if v_sfp1_register_index < 255 then
                v_sfp1_register_index:=v_sfp1_register_index+1;
            else
                v_sfp1_register_index:=0;
            end if;
        end if;


        p_sfp_i2c_interface_out.sfp0_dpram <= s_sfp0_dpram;
        p_sfp_i2c_interface_out.sfp1_dpram <= s_sfp1_dpram;
        
        
        case v_control_sm is
        
            when st_initialize=>
            
                v_register_index := 0; 
                s_sfp0_dpram.rsta <= '1';               
                s_sfp1_dpram.rstb <= '1';
                
                v_control_sm:=st_idle;
                p_sfp_i2c_interface_out.busy <= '0';
                --s_gbtx_control<=p_gbtx_control;
            when st_idle =>
                
                --s_gbtx_control<=p_gbtx_control;
                p_sfp_i2c_interface_out.busy <= '0';
                s_i2c(0).ena_in <= '0';
                s_i2c(0).reset <= '1';
                
                s_i2c(1).ena_in <= '0';
                s_i2c(1).reset <= '1';

                s_sfp0_dpram.rsta <= '0';               
                s_sfp1_dpram.rstb <= '0';
                
                --set up propper address range, gbtx has only 365 writeable regs and 4xx readeable
                if v_i2c_operation = '0' then
                    v_start_register := 0;
                    v_end_register := 255;
                else
                    v_start_register := 0;
                    v_end_register := 255;
                end if;              
                if p_db_reg_rx_in(cfb_gbt_sc_address) = c_db_reg_tx_lut(stb_sfp1_reg) then
                    if v_register_index<v_end_register then
                        v_register_index := v_register_index+1;
                    else
                        v_register_index := 0;
                    end if;
                else
                
                end if;
                
                s_sfp0_dpram.ena <= '1';
                s_sfp0_dpram.wea <= "0";
                s_sfp1_dpram.ena <= '1';
                s_sfp1_dpram.wea <= "0";


                s_sfp0_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp0_dpram.dina <= (others=>'0');
                s_sfp1_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp1_dpram.dina <= (others=>'0');


                s_leds_out(2 downto 0)<="000";

                --trigger code
                if (v_sfp0_register_index = 0 or v_sfp1_register_index = 0) or p_sfp_control_in.sfp_trigger_i2c_operation = '1' then
                  v_control_sm := st_set_reset;
                  v_register_index := 0;
                else
                  v_control_sm := st_idle;
                end if;
                    
     
            when st_set_reset =>
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                v_control_sm := st_set_address_lsb;
                v_register_index := 0;
                --s_verify_bus <='1';-- and s_probe_out0(15); 


                s_sfp0_dpram.ena <= '1';
                s_sfp0_dpram.wea <= "0";
                s_sfp1_dpram.ena <= '1';
                s_sfp1_dpram.wea <= "0";

                s_sfp0_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp0_dpram.dina <= (others=>'0');
                s_sfp1_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp1_dpram.dina <= (others=>'0');

                s_leds_out(2 downto 0)<="010";                                  
            
            when st_set_address_lsb =>
          --s_verify_bus <='1';--  and s_probe_out0(15); 
                p_sfp_i2c_interface_out.busy <= '0';  
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                
                s_i2c(0).ena_in <= '1';                            
                s_i2c(1).ena_in <= '1';
                s_i2c(0).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(1).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;
                s_i2c(0).rw_in <= '0';                           -- write operation  
                s_i2c(1).data_in  <= v_start_register_address(7 downto 0);    -- transmit lower 8 bits of starting register address
                s_i2c(0).rw_in <= '0';                           -- write operation  
                s_i2c(1).data_in  <= v_start_register_address(7 downto 0);    -- transmit lower 8 bits of starting register address
                
                if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then 
                    v_control_sm := st_set_address_msb;
                end if;

                s_sfp0_dpram.ena <= '1';
                s_sfp0_dpram.wea <= "0";
                s_sfp1_dpram.ena <= '1';
                s_sfp1_dpram.wea <= "0";

                s_sfp0_dpram.enb <= '1';
                s_sfp0_dpram.web <= "0";
                s_sfp1_dpram.enb <= '1';
                s_sfp1_dpram.web <= "0";

                s_sfp0_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp0_dpram.dina <= (others=>'0');
                s_sfp1_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp1_dpram.dina <= (others=>'0');

               
                s_leds_out(2 downto 0)<="011";
    
            when st_set_address_msb =>
                --s_verify_bus <='1';-- and s_probe_out0(15); 
                p_sfp_i2c_interface_out.busy <= '0';  
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                
                s_i2c(0).ena_in <= '1';                            
                s_i2c(0).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(0).rw_in <= '0';                           -- write operation  
                s_i2c(0).data_in  <= v_start_register_address(15 downto 8);    -- transmit lower 8 bits of starting register address

                s_i2c(1).ena_in <= '1';                            
                s_i2c(1).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(1).rw_in <= '0';                           -- write operation  
                s_i2c(1).data_in  <= v_start_register_address(15 downto 8);    -- transmit lower 8 bits of starting register address

                    
                if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then 
                    v_control_sm := st_trigger_register_operation;
                end if;
 
                s_sfp0_dpram.ena <= '1';
                s_sfp0_dpram.wea <= "0";
                s_sfp1_dpram.ena <= '1';
                s_sfp1_dpram.wea <= "0";

                s_sfp0_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp0_dpram.dina <= (others=>'0');
                s_sfp1_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp1_dpram.dina <= (others=>'0');

 
                s_leds_out(2 downto 0)<="100";
                  
            when st_trigger_register_operation =>
                --s_verify_bus <='1';-- and s_probe_out0(15); 
                p_sfp_i2c_interface_out.busy <= '0';  
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                
                s_i2c(0).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(1).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;
                
                s_i2c(0).rw_in <= v_i2c_operation; --'0';
                s_i2c(1).rw_in <= v_i2c_operation; --'0';

                s_sfp0_dpram.ena <= '1';
                s_sfp1_dpram.ena <= '1';
                s_sfp0_dpram.wea(0) <= v_i2c_operation;
                s_sfp1_dpram.wea(0) <= v_i2c_operation;

                s_sfp0_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp0_dpram.dina <= (others=>'0');
                s_sfp1_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,8));
                s_sfp1_dpram.dina <= (others=>'0');

                
                if v_i2c_operation = '0' then
                    s_i2c(0).data_in <= s_sfp0_dpram.douta; -- transmit 8 bits of register data
                    s_i2c(1).data_in <= s_sfp1_dpram.douta; -- transmit 8 bits of register data
                else
                    s_sfp0_dpram.dina <= v_sfp0_data_buffer;
                    s_sfp1_dpram.dina <= v_sfp1_data_buffer;
                    if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then
                            
                            v_sfp0_data_buffer := s_i2c(0).data_out; --read register
                            v_sfp1_data_buffer := s_i2c(1).data_out;
                    end if;
                end if;
                
                if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then
                
                  v_register_index := v_register_index + 1;
                  if v_register_index > v_end_register+1 then -- writing the final word....
                    v_control_sm := st_stop;  
                    s_i2c(0).ena_in <= '0';
                    s_i2c(1).ena_in <= '0';
                  else
                    v_control_sm := st_trigger_register_operation;
                    s_i2c(0).ena_in <= '1';
                    s_i2c(1).ena_in <= '1';
                  end if;                                            
                  
                end if;
    
                s_leds_out(2 downto 0)<="101";
    
            when st_stop => -- wait for i2c master to finish the last operation
                --s_verify_bus <='1';-- and s_probe_out0(15);               p_busy_out <= '1';
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                s_i2c(0).ena_in <= '0';
                s_i2c(1).ena_in <= '0';
                if (v_i2c_busy = '1') and ((s_i2c(0).busy_out = '0') and (s_i2c(1).busy_out = '0')) and p_sfp_control_in.sfp_trigger_i2c_operation = '0' then
                    v_control_sm := st_idle;
                end if;
                s_leds_out(2 downto 0)<="110";
            when others =>
                v_control_sm := st_idle;
                
            end case; 
    

    
    end if;
end process;


end Behavioral;
