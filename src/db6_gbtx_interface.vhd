--=================================================================================================--
--##################################   module information   #######################################--
--=================================================================================================--
--                                                                                         
-- company:               stockholm university                                                        
-- engineer:              eduardo valdes eduardo.valdes@cern.ch
-- engineer:              sam silverstein silver@fysik.su.se
--                                                                                                 
-- project name:          db_gbtx_interface                                                                
-- module name:                                                   
--                                                                                                 
-- language:              vhdl'93                                                                  
--                                                                                                   
-- target device:         xilinx kintex ultrascale                                                         
-- tool version:          vivado                                                               
--                                                                                                   
-- version:               1.0                                                                      
--
-- description:            
--
-- versions history:      date         version   author            			description
--
--                        22/03/2018   1.0       eduardo valdes santurio   	firmware for the controlling the i2c configuration/monitoring of the gbtx / tilecal daughterboard
--
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_gbtx_interface is
  port (       
    p_clknet_in 		: in t_db_clknet;
    p_master_reset_in  : in std_logic;
    
    p_cfgbus_data_local_in       : in t_cfgbus_data_in;
    p_cfgbus_data_remote_in       : in t_cfgbus_data_in;      
    p_db_reg_rx_out      : out t_db_reg_rx;
    p_bcr_out    : out t_bcr;
        
    p_db_reg_rx_in : in t_db_reg_rx;
    p_gbt_encoder_interface_in        : in t_gbt_encoder_interface;
    p_gbtx_control : in t_gbtx_control;
    p_gbtx_interface_out : out t_gbtx_interface;
    p_scl_inout 	: inout std_logic_vector(1 downto 0);
    p_sda_inout    : inout std_logic_vector(1 downto 0);
        
    p_leds_out : out std_logic_vector(3 downto 0)
            );
end db6_gbtx_interface;

architecture behavioral of db6_gbtx_interface is

begin


i_db6_gbtx_i2c_interface : entity tilecal.db6_gbtx_i2c_interface
  port map(       
        p_clknet_in => p_clknet_in,
        p_master_reset_in  => p_master_reset_in,
        p_db_reg_rx_in => p_db_reg_rx_in,
        p_gbt_encoder_interface_in => p_gbt_encoder_interface_in,
        
        p_gbtx_control => p_gbtx_control,
        p_gbtx_interface_out => p_gbtx_interface_out,
        p_scl_inout =>p_scl_inout,
        p_sda_inout =>p_sda_inout,
            
        p_leds_out => open
);


i_db6_configbus_interface : entity tilecal.db6_configbus_interface
    Port map (     
        p_master_reset_in => p_master_reset_in,
        p_clknet_in => p_clknet_in,
        p_cfgbus_data_local_in => p_cfgbus_data_local_in,
        p_cfgbus_data_remote_in => p_cfgbus_data_remote_in,
        p_gbt_encoder_interface_in => p_gbt_encoder_interface_in,      
        p_db_reg_rx_out => p_db_reg_rx_out,
        p_leds_out => open,
        p_bcr_out => p_bcr_out
    );
        


end behavioral;