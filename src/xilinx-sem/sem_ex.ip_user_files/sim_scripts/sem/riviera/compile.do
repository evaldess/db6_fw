vlib work
vlib riviera

vlib riviera/sem_ultra_v3_1_12
vlib riviera/xil_defaultlib

vmap sem_ultra_v3_1_12 riviera/sem_ultra_v3_1_12
vmap xil_defaultlib riviera/xil_defaultlib

vlog -work sem_ultra_v3_1_12  -v2k5 "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ipstatic/hdl/sem_ultra_v3_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ip/sem/synth/sem.v" \


vlog -work xil_defaultlib \
"glbl.v"

