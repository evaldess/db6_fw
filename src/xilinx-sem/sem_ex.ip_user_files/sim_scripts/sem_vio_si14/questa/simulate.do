onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib sem_vio_si14_opt

do {wave.do}

view wave
view structure
view signals

do {sem_vio_si14.udo}

run -all

quit -force
