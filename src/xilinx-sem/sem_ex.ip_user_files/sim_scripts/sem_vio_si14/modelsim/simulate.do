onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -L xil_defaultlib -L secureip -lib xil_defaultlib xil_defaultlib.sem_vio_si14

do {wave.do}

view wave
view structure
view signals

do {sem_vio_si14.udo}

run -all

quit -force
