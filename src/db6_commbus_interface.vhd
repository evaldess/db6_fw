----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/17/2020 10:51:48 PM
-- Design Name: 
-- Module Name: db6_commbus_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;


entity db6_commbus_interface is
   generic (   
        g_num_gth_links                 : integer := 1                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
   );
  Port ( 
        p_clknet_in : in t_db_clknet;
        p_master_reset_in : in std_logic;
        p_db_reg_rx_in : in t_db_reg_rx;
        
        p_commbus_gth_tx_out  : out t_diff_pair_vector(1 downto 0);
        p_commbus_gth_rx_in  : in t_diff_pair_vector(1 downto 0);
        p_commbus_gth_loopback_tx_out : out t_diff_pair_vector(0 downto 0);
        p_commbus_gth_loopback_rx_in : in t_diff_pair_vector(0 downto 0);
        
        p_commbus_ddr_tx_out  : out t_diff_pair;
        p_commbus_ddr_loopback_tx_out  : out t_diff_pair;
        p_commbus_ddr_clk_out  : out t_diff_pair;
        p_commbus_ddr_rx_in  : in t_diff_pair;
        p_commbus_ddr_loopback_rx_in  : in t_diff_pair;
        p_commbus_ddr_clk_in  : in t_diff_pair;
        
        --tdo from other fpga
        p_tdo_remote_in	            : in	std_logic;
        
        --interfaces
        p_gbt_encoder_interface_out         : out t_gbt_encoder_interface;        
        p_mb_interface_in : in t_mb_interface;
        p_sem_interface_in : in t_sem_interface;
        p_system_management_interface_in : in t_system_management_interface;
        p_gbtx_interface_in : in t_gbtx_interface;
        p_serial_id_interface_in : in t_serial_id_interface
        
        
      );
end db6_commbus_interface;

architecture Behavioral of db6_commbus_interface is

begin

i_db6_commbus_mgt_interface : entity tilecal.db6_commbus_mgt_interface
   generic map (   
        g_num_gth_links                 => g_num_gth_links                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
   )
  port map ( 
        p_clknet_in => p_clknet_in,
        p_master_reset_in => p_master_reset_in,
        p_db_reg_rx_in => p_db_reg_rx_in,
        
        p_commbus_gth_tx_out => p_commbus_gth_tx_out,
        p_commbus_gth_rx_in  => p_commbus_gth_rx_in,
        p_commbus_gth_loopback_tx_out => p_commbus_gth_loopback_tx_out,
        p_commbus_gth_loopback_rx_in => p_commbus_gth_loopback_rx_in,
        
                --tdo from remote fpga
        p_tdo_remote_in => p_tdo_remote_in,
        
        --interfaces
        p_mb_interface_in => p_mb_interface_in,
        p_sem_interface_in => p_sem_interface_in,
        p_system_management_interface_in => p_system_management_interface_in,
        p_gbtx_interface_in => p_gbtx_interface_in,
        p_serial_id_interface_in => p_serial_id_interface_in
        
  );

i_db6_commbus_ddr_interface : entity tilecal.db6_commbus_ddr_interface
  port map ( 
  
        p_clknet_in => p_clknet_in,
        p_master_reset_in => p_master_reset_in,
        p_db_reg_rx_in => p_db_reg_rx_in,
        
        p_commbus_ddr_tx_out => p_commbus_ddr_tx_out,
        p_commbus_ddr_loopback_tx_out => p_commbus_ddr_loopback_tx_out,
        p_commbus_ddr_clk_out => p_commbus_ddr_clk_out,
        p_commbus_ddr_rx_in => p_commbus_ddr_rx_in,
        p_commbus_ddr_loopback_rx_in => p_commbus_ddr_loopback_rx_in,
        p_commbus_ddr_clk_in => p_commbus_ddr_clk_in,
        
        --tdo from other fpga
        p_tdo_remote_in	=> p_tdo_remote_in,
        
        --interfaces
        p_mb_interface_in => p_mb_interface_in,
        p_sem_interface_in => p_sem_interface_in,
        p_system_management_interface_in => p_system_management_interface_in,
        p_gbtx_interface_in => p_gbtx_interface_in,
        p_serial_id_interface_in => p_serial_id_interface_in
        --p_commbus_ddr_loopback_tx_out : out t_diff_pair_vector(0 downto 0);
        --p_commbus_ddr_loopback_rx_in : in t_diff_pair_vector(0 downto 0)
  
  );




end Behavioral;
