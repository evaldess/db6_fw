-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Fri May 29 17:13:03 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sem_stub.vhdl
-- Design      : sem
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    status_heartbeat : out STD_LOGIC;
    status_initialization : out STD_LOGIC;
    status_observation : out STD_LOGIC;
    status_correction : out STD_LOGIC;
    status_classification : out STD_LOGIC;
    status_injection : out STD_LOGIC;
    status_diagnostic_scan : out STD_LOGIC;
    status_detect_only : out STD_LOGIC;
    status_essential : out STD_LOGIC;
    status_uncorrectable : out STD_LOGIC;
    monitor_txdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    monitor_txwrite : out STD_LOGIC;
    monitor_txfull : in STD_LOGIC;
    monitor_rxdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    monitor_rxread : out STD_LOGIC;
    monitor_rxempty : in STD_LOGIC;
    command_strobe : in STD_LOGIC;
    command_busy : out STD_LOGIC;
    command_code : in STD_LOGIC_VECTOR ( 39 downto 0 );
    icap_clk : in STD_LOGIC;
    icap_o : in STD_LOGIC_VECTOR ( 31 downto 0 );
    icap_csib : out STD_LOGIC;
    icap_rdwrb : out STD_LOGIC;
    icap_i : out STD_LOGIC_VECTOR ( 31 downto 0 );
    icap_prerror : in STD_LOGIC;
    icap_prdone : in STD_LOGIC;
    icap_avail : in STD_LOGIC;
    cap_rel : in STD_LOGIC;
    cap_gnt : in STD_LOGIC;
    cap_req : out STD_LOGIC;
    fecc_eccerrornotsingle : in STD_LOGIC;
    fecc_eccerrorsingle : in STD_LOGIC;
    fecc_endofframe : in STD_LOGIC;
    fecc_endofscan : in STD_LOGIC;
    fecc_crcerror : in STD_LOGIC;
    fecc_far : in STD_LOGIC_VECTOR ( 25 downto 0 );
    fecc_farsel : out STD_LOGIC_VECTOR ( 1 downto 0 );
    aux_error_cr_ne : in STD_LOGIC;
    aux_error_cr_es : in STD_LOGIC;
    aux_error_uc : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "status_heartbeat,status_initialization,status_observation,status_correction,status_classification,status_injection,status_diagnostic_scan,status_detect_only,status_essential,status_uncorrectable,monitor_txdata[7:0],monitor_txwrite,monitor_txfull,monitor_rxdata[7:0],monitor_rxread,monitor_rxempty,command_strobe,command_busy,command_code[39:0],icap_clk,icap_o[31:0],icap_csib,icap_rdwrb,icap_i[31:0],icap_prerror,icap_prdone,icap_avail,cap_rel,cap_gnt,cap_req,fecc_eccerrornotsingle,fecc_eccerrorsingle,fecc_endofframe,fecc_endofscan,fecc_crcerror,fecc_far[25:0],fecc_farsel[1:0],aux_error_cr_ne,aux_error_cr_es,aux_error_uc";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "sem_ultra_v3_1_12,Vivado 2019.2_AR72614";
begin
end;
