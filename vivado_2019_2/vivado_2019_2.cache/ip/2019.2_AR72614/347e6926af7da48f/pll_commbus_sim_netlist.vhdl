-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Mon Jun 22 19:27:41 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ pll_commbus_sim_netlist.vhdl
-- Design      : pll_commbus
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_commbus_clk_wiz is
  port (
    p_commbusclk_out : out STD_LOGIC;
    p_bitclk_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_clk_in : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_commbus_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_commbus_clk_wiz is
  signal clkfbout_pll_commbus : STD_LOGIC;
  signal p_bitclk_out_pll_commbus : STD_LOGIC;
  signal p_bitclk_out_pll_commbus_en_clk : STD_LOGIC;
  signal p_clk_in_pll_commbus : STD_LOGIC;
  signal p_commbusclk_out_pll_commbus : STD_LOGIC;
  signal p_commbusclk_out_pll_commbus_en_clk : STD_LOGIC;
  signal \^p_locked_out\ : STD_LOGIC;
  signal seq_reg1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of seq_reg1 : signal is "true";
  attribute async_reg : string;
  attribute async_reg of seq_reg1 : signal is "true";
  signal seq_reg2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg2 : signal is "true";
  attribute async_reg of seq_reg2 : signal is "true";
  signal NLW_clkf_buf_O_UNCONNECTED : STD_LOGIC;
  signal NLW_plle3_adv_inst_CLKFBIN_UNCONNECTED : STD_LOGIC;
  signal NLW_plle3_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_plle3_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_plle3_adv_inst_CLKOUTPHY_UNCONNECTED : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of clkf_buf : label is "BUFG";
  attribute BOX_TYPE of clkin1_ibuf : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibuf : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibuf : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibuf : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout1_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of plle3_adv_inst : label is "PRIMITIVE";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of plle3_adv_inst : label is "MLO";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \seq_reg1_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \seq_reg1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[7]\ : label is "yes";
begin
  p_locked_out <= \^p_locked_out\;
clkf_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "ASYNC",
      SIM_DEVICE => "ULTRASCALE"
    )
        port map (
      CE => '1',
      I => clkfbout_pll_commbus,
      O => NLW_clkf_buf_O_UNCONNECTED
    );
clkin1_ibuf: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => p_clk_in,
      O => p_clk_in_pll_commbus
    );
clkout1_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg1(7),
      I => p_commbusclk_out_pll_commbus,
      O => p_commbusclk_out
    );
clkout1_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_commbusclk_out_pll_commbus,
      O => p_commbusclk_out_pll_commbus_en_clk
    );
clkout2_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg2(7),
      I => p_bitclk_out_pll_commbus,
      O => p_bitclk_out
    );
clkout2_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_bitclk_out_pll_commbus,
      O => p_bitclk_out_pll_commbus_en_clk
    );
plle3_adv_inst: unisim.vcomponents.PLLE3_ADV
    generic map(
      CLKFBOUT_MULT => 12,
      CLKFBOUT_PHASE => 0.000000,
      CLKIN_PERIOD => 12.475000,
      CLKOUT0_DIVIDE => 12,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT1_DIVIDE => 2,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUTPHY_MODE => "VCO_2X",
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKFBIN_INVERTED => '0',
      IS_CLKIN_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER => 0.010000,
      STARTUP_WAIT => "FALSE"
    )
        port map (
      CLKFBIN => NLW_plle3_adv_inst_CLKFBIN_UNCONNECTED,
      CLKFBOUT => clkfbout_pll_commbus,
      CLKIN => p_clk_in_pll_commbus,
      CLKOUT0 => p_commbusclk_out_pll_commbus,
      CLKOUT0B => NLW_plle3_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => p_bitclk_out_pll_commbus,
      CLKOUT1B => NLW_plle3_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUTPHY => NLW_plle3_adv_inst_CLKOUTPHY_UNCONNECTED,
      CLKOUTPHYEN => '0',
      DADDR(6 downto 0) => p_daddr_in(6 downto 0),
      DCLK => p_dclk_in,
      DEN => p_den_in,
      DI(15 downto 0) => p_din_in(15 downto 0),
      DO(15 downto 0) => p_dout_out(15 downto 0),
      DRDY => p_drdy_out,
      DWE => p_dwe_in,
      LOCKED => \^p_locked_out\,
      PWRDWN => '0',
      RST => p_reset_in
    );
\seq_reg1_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg1(0)
    );
\seq_reg1_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(0),
      Q => seq_reg1(1)
    );
\seq_reg1_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(1),
      Q => seq_reg1(2)
    );
\seq_reg1_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(2),
      Q => seq_reg1(3)
    );
\seq_reg1_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(3),
      Q => seq_reg1(4)
    );
\seq_reg1_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(4),
      Q => seq_reg1(5)
    );
\seq_reg1_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(5),
      Q => seq_reg1(6)
    );
\seq_reg1_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_commbusclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(6),
      Q => seq_reg1(7)
    );
\seq_reg2_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg2(0)
    );
\seq_reg2_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(0),
      Q => seq_reg2(1)
    );
\seq_reg2_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(1),
      Q => seq_reg2(2)
    );
\seq_reg2_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(2),
      Q => seq_reg2(3)
    );
\seq_reg2_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(3),
      Q => seq_reg2(4)
    );
\seq_reg2_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(4),
      Q => seq_reg2(5)
    );
\seq_reg2_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(5),
      Q => seq_reg2(6)
    );
\seq_reg2_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_bitclk_out_pll_commbus_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(6),
      Q => seq_reg2(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    p_commbusclk_out : out STD_LOGIC;
    p_bitclk_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_clk_in : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_commbus_clk_wiz
     port map (
      p_bitclk_out => p_bitclk_out,
      p_clk_in => p_clk_in,
      p_commbusclk_out => p_commbusclk_out,
      p_daddr_in(6 downto 0) => p_daddr_in(6 downto 0),
      p_dclk_in => p_dclk_in,
      p_den_in => p_den_in,
      p_din_in(15 downto 0) => p_din_in(15 downto 0),
      p_dout_out(15 downto 0) => p_dout_out(15 downto 0),
      p_drdy_out => p_drdy_out,
      p_dwe_in => p_dwe_in,
      p_locked_out => p_locked_out,
      p_reset_in => p_reset_in
    );
end STRUCTURE;
