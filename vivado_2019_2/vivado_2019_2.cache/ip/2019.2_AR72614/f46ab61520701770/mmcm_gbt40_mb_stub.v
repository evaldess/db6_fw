// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Thu Apr 23 13:58:20 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_gbt40_mb_stub.v
// Design      : mmcm_gbt40_mb
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_in2_p, clk_in2_n, p_clk_in_sel_in, 
  p_clk40_out, p_clk40_90_out, p_clk40_180_out, p_clk40_270_out, p_clk80_out, p_clk280_out, 
  p_clk560_out, daddr, dclk, den, din, dout, drdy, dwe, psclk, psen, psincdec, psdone, reset, 
  input_clk_stopped, clkfb_stopped, p_locked_out, cddcdone, cddcreq, clk_in1_p, clk_in1_n)
/* synthesis syn_black_box black_box_pad_pin="clk_in2_p,clk_in2_n,p_clk_in_sel_in,p_clk40_out,p_clk40_90_out,p_clk40_180_out,p_clk40_270_out,p_clk80_out,p_clk280_out,p_clk560_out,daddr[6:0],dclk,den,din[15:0],dout[15:0],drdy,dwe,psclk,psen,psincdec,psdone,reset,input_clk_stopped,clkfb_stopped,p_locked_out,cddcdone,cddcreq,clk_in1_p,clk_in1_n" */;
  input clk_in2_p;
  input clk_in2_n;
  input p_clk_in_sel_in;
  output p_clk40_out;
  output p_clk40_90_out;
  output p_clk40_180_out;
  output p_clk40_270_out;
  output p_clk80_out;
  output p_clk280_out;
  output p_clk560_out;
  input [6:0]daddr;
  input dclk;
  input den;
  input [15:0]din;
  output [15:0]dout;
  output drdy;
  input dwe;
  input psclk;
  input psen;
  input psincdec;
  output psdone;
  input reset;
  output input_clk_stopped;
  output clkfb_stopped;
  output p_locked_out;
  output cddcdone;
  input cddcreq;
  input clk_in1_p;
  input clk_in1_n;
endmodule
