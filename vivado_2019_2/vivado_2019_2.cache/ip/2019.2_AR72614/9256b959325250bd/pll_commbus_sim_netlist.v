// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Jun 22 17:29:00 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ pll_commbus_sim_netlist.v
// Design      : pll_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_out1,
    clk_out2,
    p_daddr_in,
    p_dclk_in,
    p_den_in,
    p_din_in,
    p_dout_out,
    p_drdy_out,
    p_dwe_in,
    p_reset_in,
    p_locked_out,
    p_clk_in);
  output clk_out1;
  output clk_out2;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_reset_in;
  output p_locked_out;
  input p_clk_in;

  wire clk_out1;
  wire clk_out2;
  (* IBUF_LOW_PWR *) wire p_clk_in;
  wire [6:0]p_daddr_in;
  wire p_dclk_in;
  wire p_den_in;
  wire [15:0]p_din_in;
  wire [15:0]p_dout_out;
  wire p_drdy_out;
  wire p_dwe_in;
  wire p_locked_out;
  wire p_reset_in;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_commbus_clk_wiz inst
       (.clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .p_clk_in(p_clk_in),
        .p_daddr_in(p_daddr_in),
        .p_dclk_in(p_dclk_in),
        .p_den_in(p_den_in),
        .p_din_in(p_din_in),
        .p_dout_out(p_dout_out),
        .p_drdy_out(p_drdy_out),
        .p_dwe_in(p_dwe_in),
        .p_locked_out(p_locked_out),
        .p_reset_in(p_reset_in));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_commbus_clk_wiz
   (clk_out1,
    clk_out2,
    p_daddr_in,
    p_dclk_in,
    p_den_in,
    p_din_in,
    p_dout_out,
    p_drdy_out,
    p_dwe_in,
    p_reset_in,
    p_locked_out,
    p_clk_in);
  output clk_out1;
  output clk_out2;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_reset_in;
  output p_locked_out;
  input p_clk_in;

  wire clk_out1;
  wire clk_out1_pll_commbus;
  wire clk_out1_pll_commbus_en_clk;
  wire clk_out2;
  wire clk_out2_pll_commbus;
  wire clk_out2_pll_commbus_en_clk;
  wire clkfbout_pll_commbus;
  wire p_clk_in;
  wire p_clk_in_pll_commbus;
  wire [6:0]p_daddr_in;
  wire p_dclk_in;
  wire p_den_in;
  wire [15:0]p_din_in;
  wire [15:0]p_dout_out;
  wire p_drdy_out;
  wire p_dwe_in;
  wire p_locked_out;
  wire p_reset_in;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg1;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg2;
  wire NLW_clkf_buf_O_UNCONNECTED;
  wire NLW_plle3_adv_inst_CLKFBIN_UNCONNECTED;
  wire NLW_plle3_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_plle3_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_plle3_adv_inst_CLKOUTPHY_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "BUFG" *) 
  BUFGCE #(
    .CE_TYPE("ASYNC"),
    .SIM_DEVICE("ULTRASCALE")) 
    clkf_buf
       (.CE(1'b1),
        .I(clkfbout_pll_commbus),
        .O(NLW_clkf_buf_O_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibuf
       (.I(p_clk_in),
        .O(p_clk_in_pll_commbus));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout1_buf
       (.CE(seq_reg1[7]),
        .I(clk_out1_pll_commbus),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout1_buf_en
       (.CE(1'b1),
        .I(clk_out1_pll_commbus),
        .O(clk_out1_pll_commbus_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout2_buf
       (.CE(seq_reg2[7]),
        .I(clk_out2_pll_commbus),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout2_buf_en
       (.CE(1'b1),
        .I(clk_out2_pll_commbus),
        .O(clk_out2_pll_commbus_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* OPT_MODIFIED = "MLO" *) 
  PLLE3_ADV #(
    .CLKFBOUT_MULT(12),
    .CLKFBOUT_PHASE(0.000000),
    .CLKIN_PERIOD(12.475000),
    .CLKOUT0_DIVIDE(12),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT1_DIVIDE(2),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUTPHY_MODE("VCO_2X"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKFBIN_INVERTED(1'b0),
    .IS_CLKIN_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER(0.010000),
    .STARTUP_WAIT("FALSE")) 
    plle3_adv_inst
       (.CLKFBIN(NLW_plle3_adv_inst_CLKFBIN_UNCONNECTED),
        .CLKFBOUT(clkfbout_pll_commbus),
        .CLKIN(p_clk_in_pll_commbus),
        .CLKOUT0(clk_out1_pll_commbus),
        .CLKOUT0B(NLW_plle3_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_pll_commbus),
        .CLKOUT1B(NLW_plle3_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUTPHY(NLW_plle3_adv_inst_CLKOUTPHY_UNCONNECTED),
        .CLKOUTPHYEN(1'b0),
        .DADDR(p_daddr_in),
        .DCLK(p_dclk_in),
        .DEN(p_den_in),
        .DI(p_din_in),
        .DO(p_dout_out),
        .DRDY(p_drdy_out),
        .DWE(p_dwe_in),
        .LOCKED(p_locked_out),
        .PWRDWN(1'b0),
        .RST(p_reset_in));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[0] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(p_locked_out),
        .Q(seq_reg1[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[1] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[0]),
        .Q(seq_reg1[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[2] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[1]),
        .Q(seq_reg1[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[3] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[2]),
        .Q(seq_reg1[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[4] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[3]),
        .Q(seq_reg1[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[5] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[4]),
        .Q(seq_reg1[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[6] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[5]),
        .Q(seq_reg1[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[7] 
       (.C(clk_out1_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg1[6]),
        .Q(seq_reg1[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[0] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(p_locked_out),
        .Q(seq_reg2[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[1] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[0]),
        .Q(seq_reg2[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[2] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[1]),
        .Q(seq_reg2[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[3] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[2]),
        .Q(seq_reg2[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[4] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[3]),
        .Q(seq_reg2[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[5] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[4]),
        .Q(seq_reg2[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[6] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[5]),
        .Q(seq_reg2[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[7] 
       (.C(clk_out2_pll_commbus_en_clk),
        .CE(1'b1),
        .CLR(p_reset_in),
        .D(seq_reg2[6]),
        .Q(seq_reg2[7]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
