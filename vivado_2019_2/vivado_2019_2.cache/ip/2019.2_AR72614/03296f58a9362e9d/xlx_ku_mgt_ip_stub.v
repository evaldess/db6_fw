// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed Jun 17 18:16:45 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ xlx_ku_mgt_ip_stub.v
// Design      : xlx_ku_mgt_ip
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlx_ku_mgt_ip_gtwizard_top,Vivado 2019.2_AR72614" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(gtwiz_userclk_tx_reset_in, 
  gtwiz_userclk_tx_srcclk_out, gtwiz_userclk_tx_usrclk_out, 
  gtwiz_userclk_tx_usrclk2_out, gtwiz_userclk_tx_active_out, gtwiz_userclk_rx_reset_in, 
  gtwiz_userclk_rx_srcclk_out, gtwiz_userclk_rx_usrclk_out, 
  gtwiz_userclk_rx_usrclk2_out, gtwiz_userclk_rx_active_out, 
  gtwiz_buffbypass_tx_reset_in, gtwiz_buffbypass_tx_start_user_in, 
  gtwiz_buffbypass_tx_done_out, gtwiz_buffbypass_tx_error_out, 
  gtwiz_buffbypass_rx_reset_in, gtwiz_buffbypass_rx_start_user_in, 
  gtwiz_buffbypass_rx_done_out, gtwiz_buffbypass_rx_error_out, 
  gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, gtwiz_reset_tx_pll_and_datapath_in, 
  gtwiz_reset_tx_datapath_in, gtwiz_reset_rx_pll_and_datapath_in, 
  gtwiz_reset_rx_datapath_in, gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, 
  gtwiz_reset_rx_done_out, gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, gtrefclk01_in, 
  gtrefclk11_in, qpll1refclksel_in, qpll1fbclklost_out, qpll1outclk_out, 
  qpll1outrefclk_out, qpll1refclklost_out, cpllrefclksel_in, drpaddr_in, drpclk_in, drpdi_in, 
  drpen_in, drpwe_in, gtgrefclk_in, gthrxn_in, gthrxp_in, gtrefclk0_in, gtrefclk1_in, 
  loopback_in, rxoutclksel_in, rxpolarity_in, rxslide_in, rxsysclksel_in, txdiffctrl_in, 
  txmaincursor_in, txoutclksel_in, txpolarity_in, txpostcursor_in, txprecursor_in, 
  txsysclksel_in, drpdo_out, drprdy_out, gthtxn_out, gthtxp_out, gtpowergood_out, 
  rxoutclkfabric_out, rxpmaresetdone_out, txoutclkfabric_out, txpmaresetdone_out, 
  txprgdivresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_reset_in[0:0],gtwiz_userclk_tx_srcclk_out[0:0],gtwiz_userclk_tx_usrclk_out[0:0],gtwiz_userclk_tx_usrclk2_out[0:0],gtwiz_userclk_tx_active_out[0:0],gtwiz_userclk_rx_reset_in[0:0],gtwiz_userclk_rx_srcclk_out[0:0],gtwiz_userclk_rx_usrclk_out[0:0],gtwiz_userclk_rx_usrclk2_out[0:0],gtwiz_userclk_rx_active_out[0:0],gtwiz_buffbypass_tx_reset_in[0:0],gtwiz_buffbypass_tx_start_user_in[0:0],gtwiz_buffbypass_tx_done_out[0:0],gtwiz_buffbypass_tx_error_out[0:0],gtwiz_buffbypass_rx_reset_in[0:0],gtwiz_buffbypass_rx_start_user_in[0:0],gtwiz_buffbypass_rx_done_out[0:0],gtwiz_buffbypass_rx_error_out[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[119:0],gtwiz_userdata_rx_out[119:0],gtrefclk01_in[0:0],gtrefclk11_in[0:0],qpll1refclksel_in[2:0],qpll1fbclklost_out[0:0],qpll1outclk_out[0:0],qpll1outrefclk_out[0:0],qpll1refclklost_out[0:0],cpllrefclksel_in[8:0],drpaddr_in[26:0],drpclk_in[2:0],drpdi_in[47:0],drpen_in[2:0],drpwe_in[2:0],gtgrefclk_in[2:0],gthrxn_in[2:0],gthrxp_in[2:0],gtrefclk0_in[2:0],gtrefclk1_in[2:0],loopback_in[8:0],rxoutclksel_in[8:0],rxpolarity_in[2:0],rxslide_in[2:0],rxsysclksel_in[5:0],txdiffctrl_in[11:0],txmaincursor_in[20:0],txoutclksel_in[8:0],txpolarity_in[2:0],txpostcursor_in[14:0],txprecursor_in[14:0],txsysclksel_in[5:0],drpdo_out[47:0],drprdy_out[2:0],gthtxn_out[2:0],gthtxp_out[2:0],gtpowergood_out[2:0],rxoutclkfabric_out[2:0],rxpmaresetdone_out[2:0],txoutclkfabric_out[2:0],txpmaresetdone_out[2:0],txprgdivresetdone_out[2:0]" */;
  input [0:0]gtwiz_userclk_tx_reset_in;
  output [0:0]gtwiz_userclk_tx_srcclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk2_out;
  output [0:0]gtwiz_userclk_tx_active_out;
  input [0:0]gtwiz_userclk_rx_reset_in;
  output [0:0]gtwiz_userclk_rx_srcclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk2_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [119:0]gtwiz_userdata_tx_in;
  output [119:0]gtwiz_userdata_rx_out;
  input [0:0]gtrefclk01_in;
  input [0:0]gtrefclk11_in;
  input [2:0]qpll1refclksel_in;
  output [0:0]qpll1fbclklost_out;
  output [0:0]qpll1outclk_out;
  output [0:0]qpll1outrefclk_out;
  output [0:0]qpll1refclklost_out;
  input [8:0]cpllrefclksel_in;
  input [26:0]drpaddr_in;
  input [2:0]drpclk_in;
  input [47:0]drpdi_in;
  input [2:0]drpen_in;
  input [2:0]drpwe_in;
  input [2:0]gtgrefclk_in;
  input [2:0]gthrxn_in;
  input [2:0]gthrxp_in;
  input [2:0]gtrefclk0_in;
  input [2:0]gtrefclk1_in;
  input [8:0]loopback_in;
  input [8:0]rxoutclksel_in;
  input [2:0]rxpolarity_in;
  input [2:0]rxslide_in;
  input [5:0]rxsysclksel_in;
  input [11:0]txdiffctrl_in;
  input [20:0]txmaincursor_in;
  input [8:0]txoutclksel_in;
  input [2:0]txpolarity_in;
  input [14:0]txpostcursor_in;
  input [14:0]txprecursor_in;
  input [5:0]txsysclksel_in;
  output [47:0]drpdo_out;
  output [2:0]drprdy_out;
  output [2:0]gthtxn_out;
  output [2:0]gthtxp_out;
  output [2:0]gtpowergood_out;
  output [2:0]rxoutclkfabric_out;
  output [2:0]rxpmaresetdone_out;
  output [2:0]txoutclkfabric_out;
  output [2:0]txpmaresetdone_out;
  output [2:0]txprgdivresetdone_out;
endmodule
