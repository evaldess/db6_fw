// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Jun  2 17:49:20 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_management_stub.v
// Design      : system_management
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(daddr_in, den_in, di_in, dwe_in, do_out, drdy_out, 
  dclk_in, reset_in, jtagbusy_out, jtaglocked_out, jtagmodified_out, vauxp0, vauxn0, vauxp1, 
  vauxn1, vauxp2, vauxn2, vauxp3, vauxn3, vauxp4, vauxn4, vauxp5, vauxn5, vauxp6, vauxn6, vauxp7, vauxn7, 
  vauxp8, vauxn8, vauxp9, vauxn9, vauxp10, vauxn10, vauxp11, vauxn11, vauxp12, vauxn12, vauxp13, vauxn13, 
  vauxp14, vauxn14, vauxp15, vauxn15, vp, vn, busy_out, channel_out, eoc_out, eos_out, ot_out, 
  user_supply2_alarm_out, user_supply1_alarm_out, user_supply0_alarm_out, 
  vccaux_alarm_out, vccint_alarm_out, user_temp_alarm_out, vbram_alarm_out, muxaddr_out, 
  alarm_out)
/* synthesis syn_black_box black_box_pad_pin="daddr_in[7:0],den_in,di_in[15:0],dwe_in,do_out[15:0],drdy_out,dclk_in,reset_in,jtagbusy_out,jtaglocked_out,jtagmodified_out,vauxp0,vauxn0,vauxp1,vauxn1,vauxp2,vauxn2,vauxp3,vauxn3,vauxp4,vauxn4,vauxp5,vauxn5,vauxp6,vauxn6,vauxp7,vauxn7,vauxp8,vauxn8,vauxp9,vauxn9,vauxp10,vauxn10,vauxp11,vauxn11,vauxp12,vauxn12,vauxp13,vauxn13,vauxp14,vauxn14,vauxp15,vauxn15,vp,vn,busy_out,channel_out[5:0],eoc_out,eos_out,ot_out,user_supply2_alarm_out,user_supply1_alarm_out,user_supply0_alarm_out,vccaux_alarm_out,vccint_alarm_out,user_temp_alarm_out,vbram_alarm_out,muxaddr_out[4:0],alarm_out" */;
  input [7:0]daddr_in;
  input den_in;
  input [15:0]di_in;
  input dwe_in;
  output [15:0]do_out;
  output drdy_out;
  input dclk_in;
  input reset_in;
  output jtagbusy_out;
  output jtaglocked_out;
  output jtagmodified_out;
  input vauxp0;
  input vauxn0;
  input vauxp1;
  input vauxn1;
  input vauxp2;
  input vauxn2;
  input vauxp3;
  input vauxn3;
  input vauxp4;
  input vauxn4;
  input vauxp5;
  input vauxn5;
  input vauxp6;
  input vauxn6;
  input vauxp7;
  input vauxn7;
  input vauxp8;
  input vauxn8;
  input vauxp9;
  input vauxn9;
  input vauxp10;
  input vauxn10;
  input vauxp11;
  input vauxn11;
  input vauxp12;
  input vauxn12;
  input vauxp13;
  input vauxn13;
  input vauxp14;
  input vauxn14;
  input vauxp15;
  input vauxn15;
  input vp;
  input vn;
  output busy_out;
  output [5:0]channel_out;
  output eoc_out;
  output eos_out;
  output ot_out;
  output user_supply2_alarm_out;
  output user_supply1_alarm_out;
  output user_supply0_alarm_out;
  output vccaux_alarm_out;
  output vccint_alarm_out;
  output user_temp_alarm_out;
  output vbram_alarm_out;
  output [4:0]muxaddr_out;
  output alarm_out;
endmodule
