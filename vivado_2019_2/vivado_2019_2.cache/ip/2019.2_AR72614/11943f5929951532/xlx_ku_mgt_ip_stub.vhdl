-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed Jun 17 18:20:04 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ xlx_ku_mgt_ip_stub.vhdl
-- Design      : xlx_ku_mgt_ip
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    gtwiz_userclk_tx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_userclk_rx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_tx_reset_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_tx_start_user_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_tx_error_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_rx_reset_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_rx_start_user_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_rx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_buffbypass_rx_error_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_clk_freerun_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_all_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_cdr_stable_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_reset_rx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtwiz_userdata_tx_in : in STD_LOGIC_VECTOR ( 119 downto 0 );
    gtwiz_userdata_rx_out : out STD_LOGIC_VECTOR ( 119 downto 0 );
    gtrefclk01_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtrefclk11_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    qpll1refclksel_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    qpll1fbclklost_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    qpll1outclk_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    qpll1outrefclk_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    qpll1refclklost_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    cpllrefclksel_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    drpaddr_in : in STD_LOGIC_VECTOR ( 26 downto 0 );
    drpclk_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    drpdi_in : in STD_LOGIC_VECTOR ( 47 downto 0 );
    drpen_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    drpwe_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gtgrefclk_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gthrxn_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gthrxp_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gtrefclk0_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    gtrefclk1_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    loopback_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rxoutclksel_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rxpolarity_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxslide_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxsysclksel_in : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rxusrclk_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxusrclk2_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    txdiffctrl_in : in STD_LOGIC_VECTOR ( 11 downto 0 );
    txmaincursor_in : in STD_LOGIC_VECTOR ( 20 downto 0 );
    txoutclksel_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    txpolarity_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    txpostcursor_in : in STD_LOGIC_VECTOR ( 14 downto 0 );
    txprecursor_in : in STD_LOGIC_VECTOR ( 14 downto 0 );
    txsysclksel_in : in STD_LOGIC_VECTOR ( 5 downto 0 );
    txusrclk_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    txusrclk2_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
    drpdo_out : out STD_LOGIC_VECTOR ( 47 downto 0 );
    drprdy_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gthtxn_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gthtxp_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    gtpowergood_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rxoutclk_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rxoutclkfabric_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rxpmaresetdone_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    txoutclk_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    txoutclkfabric_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    txpmaresetdone_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
    txprgdivresetdone_out : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_buffbypass_tx_reset_in[0:0],gtwiz_buffbypass_tx_start_user_in[0:0],gtwiz_buffbypass_tx_done_out[0:0],gtwiz_buffbypass_tx_error_out[0:0],gtwiz_buffbypass_rx_reset_in[0:0],gtwiz_buffbypass_rx_start_user_in[0:0],gtwiz_buffbypass_rx_done_out[0:0],gtwiz_buffbypass_rx_error_out[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[119:0],gtwiz_userdata_rx_out[119:0],gtrefclk01_in[0:0],gtrefclk11_in[0:0],qpll1refclksel_in[2:0],qpll1fbclklost_out[0:0],qpll1outclk_out[0:0],qpll1outrefclk_out[0:0],qpll1refclklost_out[0:0],cpllrefclksel_in[8:0],drpaddr_in[26:0],drpclk_in[2:0],drpdi_in[47:0],drpen_in[2:0],drpwe_in[2:0],gtgrefclk_in[2:0],gthrxn_in[2:0],gthrxp_in[2:0],gtrefclk0_in[2:0],gtrefclk1_in[2:0],loopback_in[8:0],rxoutclksel_in[8:0],rxpolarity_in[2:0],rxslide_in[2:0],rxsysclksel_in[5:0],rxusrclk_in[2:0],rxusrclk2_in[2:0],txdiffctrl_in[11:0],txmaincursor_in[20:0],txoutclksel_in[8:0],txpolarity_in[2:0],txpostcursor_in[14:0],txprecursor_in[14:0],txsysclksel_in[5:0],txusrclk_in[2:0],txusrclk2_in[2:0],drpdo_out[47:0],drprdy_out[2:0],gthtxn_out[2:0],gthtxp_out[2:0],gtpowergood_out[2:0],rxoutclk_out[2:0],rxoutclkfabric_out[2:0],rxpmaresetdone_out[2:0],txoutclk_out[2:0],txoutclkfabric_out[2:0],txpmaresetdone_out[2:0],txprgdivresetdone_out[2:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "xlx_ku_mgt_ip_gtwizard_top,Vivado 2019.2_AR72614";
begin
end;
