// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Jun 22 17:48:56 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sr_ram_commbus_sim_netlist.v
// Design      : sr_ram_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_commbus,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "60" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JeOBpTHSG60FwJ4DwJPgj+RY48l9h3PdreRMjFYyIHaKM9YzkRpVI1I9f0tCY5uvgET2GfUWP6cn
O13wIXKfIhXu6GdWGcIsrSeZHwmGu80zn7yx07ioM4adurVCNCCftRAaOBJMlkgSvU8F3XGChayC
7kmLxpMj5vdkJZ4uumSPgB2SILk0RDdzEEyKokh9K4UbAsbf6fkgh6Ig/QcqXj1tBpDgQ7ECQldH
ZS3bfeR+jAtERhKrUBOlSddcX/pCDDe4ZHasVwIQlxEX+yPwysBdaqdiIM578g7CQ/NyqrYBtM0u
0jMaPaYpyz8j+ABw5UNi6in4xp3Awp387of+tA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
k59Pepwa1j0ebicvjZ89uyZWt9PNo9fyW4x9OvTPYiZ7Lh0JA/NonbhNBVd8ABJ4gjctz5fU+eb/
id58HbsMdEH1rSBatqLb5/ipnSQ6DamGorbk5VRwgl/lbTmfkRIonB/JqtN0ov84hN27slgEoA8k
lbYLMV6KeQYqhXnn+pX05hJzLmj65+dHe70hySpIvRRBVlviJgHXJgTFcwxdjpmCFrqXhTbiINT1
BBcJybg5ehh2rlolSpM2IchB03S9OBde0fkuR2Qk6W1blk2ES5iA+HsS1HqUKwPXxyUMyFOLT3Wk
ZXMBhhr2sb82qzhTt9rZm4981n+uaRQNFp6Kow==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9136)
`pragma protect data_block
zG0NgtVmX3/Insw00qhUGltuRlb/Vh8raeLyiPubkEfKIlZdhRfda6hRAEYsS/8oNOtTLLEYI+qd
Tr2u0sxsACQGMgcdhmfiOlWv/4D9Cy+VsNV3z9qor15Kf8K4Dyc1uoJdjg8Jz/+ca7sXZ5dlXlY8
FFTP864VlidO5y8JtrAa3oVW9QAZ2p4agD9U8SeK/Eel/k4shRaGpeYFegzT6pkB/b8QjxYX23pt
XBAKH9yiGlm+SkQ40P8wpYaq61/WAzvsvHWPavKOtpejU88L4ptV27CIcMQZvwpvVNcnZh3tTv+T
9Pxsi+4A1yiAikh4rj2jGp7ad22zDhXtF7EMPxpDJctCubjieGzk1WnU3Venx2YkxMG1/uYwSZlN
TiCfoHa0LDNLBbmqenM37m+PO3r32jpgwJdq2pgRim5q+ODELBRkXQ2heEOIjRlHYJl5//yMBs9V
9R0w0ZN5SQOsklcIveEWswlGLBlHXaznOPEG2n05AzWkTXK92fyGJpCrILfDZ1M4OyWdkrZnV2Nq
+jEE1h55QPBnlxr4Cgi204SbmSigMBMIpQcon5jYlZAbKgnFK/jd0nT3TBkHv/gpwr7x9Zz8pJgL
zD66ipb9MQ+dEH0HuWk+O+6VBSTBQgnicQbsF7jWUAhJIlD6y/A+Ma/piklSFkr5uKum85ukn0Y5
7Xehp5kZJ1DhdyllAc3XVVvdt61u7q3g/OAwCtcZQyKIR6IVZjBMtqc+W1lJduOYe+lyfehvj1HK
dteiM8917WnemeDSFu329D7b4u8pxTTaISFDzf/xYg0MBikE//kn5VTKy+26J9R/JsHvdpJ1tfxe
L8Edme1DGEJSgxWDrLrBPh0Q4Oai/ama1AJ8AzLa7iNO1zfLAjxcnoqnfY2/F/RIXy0nHWvfMwNJ
fjaxa4WbY7/ojML2ZuTqeg5CiDnP+HHaVE/MmB8cbZ3hUIIZBok0e37qx2XMsFZv8HmBAizoil9E
j1VLts9o4Lf/yiOBuKIYfZJnJiXkbymmjMxk83QPc8vTc7PzIeH6Jdv+bbuqo3OTSSDhuKY6hm4U
90WmJgwkUEuOM/gBMv48tolLEIKYRwsiGYkkXTIFBIBH6bUA32BPBKW774TwRC64+6tYpCjjjObb
hyMyYEDlpBDAVVpxvt0QGWrIcdZSvOv39YtcU8mwpFDtmqdsjwIS0G1dUjFa+7DiGpDF2HbudqBh
c0+DtuDdydGb3JnHX08RqDip4NWriWZ/RsIQhL/f+lGFGSSisB9rYTD+jQcJbvskLSx3UsxeESd9
ZJS54c6xzJkeFNTRqlxNqINTMtftb6S21Srhri3YEPXFeu4WS5eeQan+4nqAEnHsyio7f8OqZ7Qn
HN40ONU92XRY4Buc9S5kQSzM5UtYRMyd41l1AICe5CEmmoSDduuLlFwq6zlO/6BytHMwmZKbToZ6
Q49tkuXaoQTY8SfAAJ/NxN8yVciNJ1SFHVes3XfQSgbBKMW7TJRPCjN8Hok7qw26vXqtWdVAXxBh
FHPPpksc2seVrbaqHwivcQfIpfuOGeQ0WTcg8M0yjR9ufSpPNcox8pdu0ltlVC2hLHSb0xX/zkOB
pH/wCO6ry7rsF7N6sItsua8y2X2jlv/WzZrPrqY1jHbGhsEV+K9TVSify/fhvFhBxrjpEbw+7fLN
uqMaD1uWp9GJDNQYasLhj1ab13myt9SGCOyx/NVpIhSkv8Hz8EiuVdPyO27n4UF2tnv0ihPM8q/Q
DsuxcZpuy1AhQQNO4ZxheQWZf95alArz7NSQERKLjsjfwlM4XS4K7HnXtCdKnMv9gb9T7oFKTthY
WGEQVumEwmoGiLnocX7UKos7pVqaMx+LDNI6hHgdPclOm1E5WNg9JsFztJ0B7laP67dHQylqg+hf
4oWDkaZRd5j0/k5jc1cg7+lN1GlSfFdaad/HW1xPndhK0QoucaqlVbCW1VIifCwPKjDMN6MWNgdW
/fZhYJxprttqsuiV7T4ACCPKtgLH65US9WPWV1Bphr20x4E2E/5i5xDVKTMPkI91Smn7x7J3SD84
1o6EqiNlMMyu4KsDQvZvuJNk4vTubYw/BttOaiGUBQRIxNO92aLv7QxpgjOyyTruZ5gIuPATnRnl
lfxysEkaRlqDu6Tn3pq/pjMZX+9cjfC1cQ1AANcgJyIUKkLeSnJFNdOIwQjd4Us5tomvc3wT8eel
zxuVTDiGQw6/0L8miNUFn7q1KEoq9/kpbw5fZWtTVLnOSTv0lFsQEZw8xbCI667SvzzjgEl8v0Xp
ERvokL8igV7zR3RbhMxsBn79LenI8+TLttP1tM3fWmJ3eOe6e9CrKcoHVbHYjUQPY3SnaUG11mrc
ShdXWCSsYkG3YYJUgLK2GP7JIh/kBmZgedI9fk1pYxbW2ejjMIlmZkAt4axVma4+dzeB/TiQPixT
mE5LRCRVnTKYwFEOp253oXhUsSs4x9Dm9LEIjWKYeABNV1EMFEU9ACjWupBGSvIpLxfFarh7btT6
nQdbW4+sRl05VyX5/5IzRe+xOQzJ5uQWqAzmcchh+AZ9gkrpRnSLvizQJ8vYTnczCbdGHM7M4FVg
m34ox/OCMGqgLIq5JsU6bXnRG8An9HeD/MQYbW+Gu/HWq4H8p16drwqGIz7m1ACnkv4Sz2zCYZVB
K6TlFlTWLgW+Ub3PsWhdMT7a2tn8T9m5oockvU3Rf6XRMeSGiqw1Ma7phOE/iaJ89ICqyYPZygQl
vIUK1NIiINiILLRwEJG3YI+/t8h8vYqehPupV5xhGMGACmOSvbSHP59jOgpcyu2mA8EPINZIsNJM
zgmy3P8H32iwOdzlk1Xy2tT4x+AIBvUoJ6KYdexrj80JwH+Vh2+MZ2uRaFe46C61CgcyB4WFY/YY
dQUIoKZrrr4jLDMtzsgdUIQICAkFsNiCB+ixpg9a/G/lqd+g26sh9ztK+aMtatmlpaQIZJJYftNH
tktkjTY9VP2QnNXOT37JPhxNexVGfXGvhe/RR7YqNK8PBviOMjS/7HsxhQ265a+P8a5fRu4QIHXy
6kd2nUMUgnoRzoogX/OgPI1ICPUxSDT3aikagYBVw/OjArTQ16Ot/lIKumTk0vPSwJzLTMnAyE0e
zBpR6vHZmCIcfBbjDDht9PCTaZ+VwpBCsYemuBSZbcyf9RRKl1vF2mWdYCu2yG0XTNX3h6kLkmJ7
TnDY5SkdTJnzPadre6V6kn4Ge1w7dB99vF/i0fC3v8P/gXGGEqC8QQq2rPg9Oo0hOTKcFeecW4jA
yiDAoaYtyPz92sVs9kjwqC+gRXnAhgzRVCmw6IbXs/TUUCAfVC36S/Jsr5pHCjLO9Z6AZ65NRdS5
o3Zg/BU8RutEL5NXUi4LIuI5mFxL2aW5+FlqU8QFW1Y683izoKqw8VKJPbVAoZPJ4elwB9rWEnNd
dD3VCoSU+/ytLedeuoxUe8YyTtPy/vW0e9c78QAR4p+zRgvB1iq4hdxGR+Qt1gI2tQoHhSMpl4XH
EyIQOyPt8hLaZNX89JFfnmaXxGi/re8SHoDp7OybnTQFjYrYuBKvWVCDgzGUQ5OCOsctOBd4CeHB
TnuQUhj5ymTQrjXJ04TuNVAJQJoJLvRZimwDVwauLSywqUvBUG3BWr70izERVbUMlJ8UG6eW9fJF
MU+HuC00RBwDlykgPiXkiZyLrRzQCc/Ju3MqHL5JWjSumPLN7VtuONrNZiCzyiSASzQcqlmlxbcT
zz3/Go9pRqncP854pTGD5ft1XlGrwkvr49YtzviHDjmkwrnItqVDKNpax/mMZXBcvK0akbakjn6G
/V+9qKW/IrqSfuTUi1hUWLa9kVTdkgFB1hiytt1wSv7TmkNbR5Ug1yhT7LQ2t/A5Z6BgYfNXdFP8
nc4lqqZcyuTes1YD+oBeBQmsWFuA0mcxdo5JalIpniAhqHC7FmUeQwfxH9wbNPzVEnujILJr0ljp
dvnQD9j2drrEZubcEejR1KSllRVl3fg2qYvX2t8fbPpALsLz9e6rj41h5xcbF0EsPPhFlqWKfTGb
eH+DDEVScPVGqBlrCq1p4IvjLfa6nEUSfcpQQ5mwDvWB2dErAUyhS7jZf66NY/erwP/4Dq35QSzD
O4e7lmlY9Yw4FTkrMG7pbqN6JaWVhBB2dvT1zE9DhqEeNqyvHR7Q3b/IYgSSZT4b+dw4xSllf88N
bMpWnqiZ7OlcvoZZ/w/65EFkpcFzdhhKgwBKTF2TLBDpYoLKwgSsEIYWVgrOLJFgV+RpEcfkvm2k
8/xJFh9sUUHC/BQsJVa1zBnuF1MgtWEdEpk+LA+ueiU1C7K6YHOog5v8D5gfxYnUroYk3zsAqs1z
+R5Doc7GQC1mynoTxT1oNLKqAB2PUj52iS7iZ2Y30CUIx6PAJAw5gfkHlJbotFQObQLdLnhHoK5D
DjV00dpoc/VOlz/skZLdNyCZJ4BDi2ydRyNoanduTth5117h9L5KvjwVt/M4ykNA3vUtbDb8ZZwd
7YeAnitSkJlQYyAKO9mWRH2dPwGA+J9S84SqZQ5fOHrz8vJZUcDrHOOXCB0jdbjD2R5h2VJRgDpd
pj/3rnnfOr/3QuLlZlQKrZqRXlgxhJRjswQ9mPzARKnZa6H1YRk+mK9+Iw8hyg0bvh/BsxJ0P6Ke
FJ4m+rquwsCDhkUVznVPh+CkT64hOYFwUjX4B/gPNqR8tw2VSfKL1jOdu4/ln8QY0QC+2z7lOT+7
ZcUW/04kbR68Bg66jg8oDoiaKqJdBje79v89KFDGWwM3haorljfgdnaaTyzh07aa75TS8/XGX2Cp
67FzveRTxKIiPF7n30aitgswmzv120DRPyJFHyj/w1qMsS1nU2BevBQQb9RAC65k8ahnPZmn2Pk/
KSwzEixVuZsZnMhWG8sgCB149IRiDvkuFnonrPDSqFHQUjer+Y0qyxPnd9MSAC1KUfSGJohLas5K
VFvg3IjS7PGXI8HzJf0WNfS20MNhmmvTOKfusqRxrBGQh5yH6VUUN3ZiMqPzcn5qj8DdrodiFLlC
n82nsihiERohApRc+3xu8E/HnqIuwi2i6A/00PiewNnoHcPiTA+3p3v2h6OtGAAtBFglvpgnm2d4
D8rme41odjIb5LMDNnuK0QH5uungYrzclVgtlqOj8VOHrEWzX5m7xwlAj/eo8VTjNb6lBaGAdiPx
mFXQRrGJ2KeCrE1imlEKXgsYub5lktR0sNPfeEiuzzY0gTam7iTh8XMP/a72bcChuxlDaE0y+zgj
vCN0xLubhhO0Mv+F1yZAsBvchhMRvd0qSbxKLoDDSCpjtUk+BTd9WJcIuN3MZLYL29k9CoaSvZDW
+pU8soyNG277MxzC84eIIr9rOzziLiVgPImPSeupdNWQV6wcXLb5kr+wAvHyfZq7Ho7DEQ+mFQ9P
T6KTySdH98b/lJGsXrSN/UqU/UvBd0pAe4obnsFOVwwbfNDzzSAIrWUMxPU+rfTPa/FeUNS13Abm
1ILganGZOLOju4c6C1hQFyfbBmegERwcbBDZHe0U2vQAjmRLVZDy8ugljXNJbhWcEANuXvoVF/0c
AIi7vcGG0beKQBxTLoQcOBbl+iA20XGfDwNUQxMz4gQuUzTy36e8lg7Zlz14Pe6UVY++2Bek/J5r
4GN3a7WY6aACvDqXCwQc+nr2geu69yt9ML99Quk1gnIyEHBYU1S1igQ9BWIRNkSzCIQUFDnwLOk1
lo7w7Hpss14CZx8eQO2/Wt3UGu13t0LJoy2RtZcWzgakdtk0kCQ0rSW6FE8AUufa8YXcECf28R9g
WG0xKCXpXKUkPLemEm4Ui6eS0A+0RMsBplTBnpgIdjV80p7VTpIxXudhbgSxDS93ACcPXH1VpJ3G
aWwrCgGtyEtd1s5aWfKj5TH1fEc6WrtvPfMXq88+2F7+BdYKmdbaqP5Vab67dyQZ/VDDIFqCF+FZ
zfIUw+Wd5m4ZedjRNsY+BWkMJvk9GYXEzF5JKIQM+vuOys0ohIF34f3RZyGpvGucDoqKo3jUlpvM
hUbdaIqdbpolmBT2gotmCxDLmqvBSH5BBqP5vDGNWOC/TH51AKdoEoFTcQgF+QLBZg9DseHXMDFn
fDhXj8O0LNIMqve00gjyoc2HxKwx0mgICK1Un3ZVL/6wqXGLL5MR6tSJmy/RxuitbCxV3F9m4WSh
8OmawGCKm4Ud/R7O3PQs0lsY2qj8EV6TK23iKKTdiazEm2o7jagT+crFRxSvJbxR4cx+cteqGt+K
06u3Hc1R7X0P68zBjZXzdO8XsaLT0zn6yuV9SIP+dWkZfM1iP1gU+yBdzvUHifBnamZsXQAxQKyx
u12amr9cOfPCGBH1Uw4hQW38Qsn+U2P+wGJF9BbatfdfRrUAQePabK9hmHc7pZFqcDGMcCumu/X8
rf6+kvY+8iQkI+M509lGUTALXl3vfdrBgeFTz4RCMiAPp5/00xGSLdanKSrARRCfno+cLSoy02Px
BoZiz5L1dH8HYmuVNwMmE7iNKHyMJb7ShLRWzibbQpj+HG+SPhZEM20qJlXgnriawTx9Zqjv0G57
4i7eEEmuR+36Abn9wgebI7Eo8vf7WaLKb+krKpDwZTdshA/mz9oX0Kyi651iwZBM77uDoiNGNhkC
hvefPyQ+V7VQua1XV7nE7o2rYecKVquRsJBbWWBQehR1dEq99eFQ6m5RqHv/Pouq8YnmhWzmfP63
6n70k+5NUa1Ct+W2BAOlEkY/oVKgwKPsQRFfWNa1RRLkwzxyhsXlPV1niJ+gWONTWfMEMHURykbf
owi4TeeNWA62YeoQ4P8OsdM7hqsqebzeDXT/yziqlxo6d0I8ilgXDitjv+GJ9shHp32Y3LfyqT0E
Zp3Fx9CkWdIc9FpKHib+HjJwd9rCwhqpRZwW7InAlErTzE2nvrcrsh5ueZN6rgiwEk2Vw+QtsHfe
fYExv8xPqB6LZ1/4pEeoxIxSxdXr0vm55Avr/tXUGnoySjNOSn4h8I0mFbc3IyxtkWiKJtljzYe/
nLrjIAdu7/mRwGVnm1yIGZWGLGCWcxaq1pmA3ZaFHrgBRYD+t9qeJnd6wOOw/bbv4NXyLMTeWFUc
WkKWiDyCpWkPJl+1XXiUnGgt9GJ2u6alpr/VMGSOa3Toao9McG1OqFgioCGXhkTaxtSkMtgLHuZN
L3MazzCbaPjvjKv2irtLsrqaKM3SLl1KJuLpZv0waj4rovS6YqM6eWwBiYAjwsaIi7Z5N63bD0kK
PmqWlvEQ1QFFM4FZ0EKUqF9jtyl0uBbFCwyTZ9LJcYrobszgIUP5FZCVl+82OfKsKK5RAb+3rNnW
M5X6sQigHuyfZ6OZOiCncqjPX6GecSbnKSLkWshARH0OybytTKiILi8+t89ODhUjpglZiy2/KiIi
vf9pURuPbvj0/a1yePOeDx2YD1cyeQMEeqJkQ1n8rNtFpMag/M2K/tc3ZgclHLxGI1LBskRuYX3Q
qkYZsnoot+Embp0/T0+SZ+CGZvRrcc/O6BHjxF6117SeLMYnlr39QdwkkxbVMf1E3nBbMCqYmm94
+X0+NZ/YrV/d5kZxoRIKfqfC/0vv+CeFL1xtofLm2dgAOaPB5DR9g1EUGm2nFwoSsQtvw+FOngXn
4qN1cexm1OQwYOxtVhODSvnmr3ddbAKMCTUKjhT3m9rKryOh6EYXEuBAkinJ+nHjhMaqYrm8N7iR
f0czzKexuZN0VkMnY1SAYANnXaXIjwEwfBWqlAj8781UqCE9KowXq0jhrQCNRfMbjzgoN5I1xLhN
Vdy3I5i0VnuyzJbuQTnsIWXpZ/UQ52FzDKOwSCcK809UH+9+LB2r52ywo4j5EpB3FShd3OWjG0Er
j9FoN7BD2iflMKkoWWqi0MG1xIOuKTCWMt5U8wCugQFKDhtQdOz04z/ZleI7igAWrSPMNS0kbOFa
9p47GKDw/cDpVUTPYlBfet3CtoxltNXHSXk+HV+IgZ4xw0AsQ6XUaq7qCbOp327sRVbKfO6gvqEZ
NsfKe8xvHmvuMWOgWePS273jzhT1nJn/Z1npn/v5Zlu2glClhtdQ1/6ZLNBhU1JOWbg+IwT84DGn
35eBLFuLu2qHWayOWgQtHb++OtQZS0wvn/I0xvAEH0pScfkT8CqWCGKTUVzcKwKJ0xKXw/avlqo7
yJcirS6AnJaMGlDOv7AP0E2lzNdMt+4Fs5HmIQIw+8U+0TBGKEyPfn0MAug/KXwlUc8Wzuay9gTi
wd/pQSiO+pLOBjjf3l7kwQZfqiFusgNeNgk+DDTnnPqZh0i1g5y2QPdyW47/oUwz8/QzU87ZvCJM
l4ZbrVFzM92ZhO6Z3nBsPY1/xRTuiFLpM3i1IMxZ2eevrENpOZCitDRAb6NkvB6EvNKj1Miu3Tdt
fC9IgbIhAaEViKaFIzNo22AKQhpTxyb8ljULbqOgagsKc4mR9ue2l8SZxZfJ+Bzofbqfk+a+3mBh
oIEy2AGxCXPjQ2LK+lf5a7gS/BfvY9wTit4jZQDrTXns5USoHBDALSn/TURqVouOIVDcnMZd4ip3
f9tCjulSE191RyR0r0wb9N+hOmkjRDV5wCXt7rj++CVMJEYQOdx67EqBXmpAEyQSdOerzHuvTOmn
Z1A1V/zMiarlRNIlCQas5N7Ixux+maC23BTrnJGkduHQjZcshouO4QJ8drmaK0i0bg6z8eZKULn/
82059SgRcuA9FeDCpjW7AIlLBmBDkSe7koxweHq8vK8zumStmmy/qXBh7bDgvdvBLEpCNc1sXDz4
MaiRqB6cFWEgAwUPWSc6bXZwg647rhHgjeGlghvlYxsZdMHuhuUNmsczcx7rj19yvo7GlttPoL9C
CW/+rfhQMBHtdL4eDUpOUJn+Rj3nt/qpkmyI8QMTdGLyKLOP2uHXo3pcMbEHP2nOQk9EWY66+FCg
T2ZY8yXbh7EPY/AYauT7QHrc3WKJeu0teFosZDsmZTMfkjkHjJuLTj1sdewUQFuPiz4mRkG1e2ji
pC9ov9S3N7wZTCWpWmosxjycpc/89T2n92Wylrw/pf6dHhrGsZStmVkjH7A8qfMEJz5xM92xDkkO
2Wj3vLkcICLGZCAAHS2DJPKlHDYlNqb7NDHsm0qaj4h2rYxAKw1hqz9v8nTNh2kBFdc7O/sdLcwg
6o/00820kAUmIpK7FyhgLx0fqzuZT2Epi3b0DdrNGYE1jTWGIqoGzcZZn0jPi5sUN/Vj+4mxioYt
HBKDiJDYGS9PsDj6uE9IvrOIaXmL4LQwuBgCnqs5njGNwX/in69ZKsGjpsbxZR1IhiJsRNhuViU9
DV1PJcN7uSRAzpj8HLN5x5AN4xCAYYJOcgy68NQLgBdQjEcBgEpO0l72UKyknxdIDqR6q8cKhiRk
TMMKRLJerjG7XV4S9d+B4rCgGiCR5VW9++3e7NuMMXn3jVmCE4+yL7A9yWsY4i87plIKXt5v+sPa
GQ4KRKfmaTgtUpG+KlkYy2SwbwTuHHPM9Rrh62thmV0gKryGAyWWinwwY67zp2W4IgHgTPMLBGDO
SKkJiZMKdcONGp5B16th/18D++su16xrH3pCdhUz/ZcMJ9yRv9wOtwO9qfkNjBE2xfRkcxNlssVp
f31uuNGeUJB088a0hgcnbJpEh0mfYSbv6GH2RDMBTRKg/df9saKRcilgVWcr/6wRm0jmC81BEeQ6
jqewJ9OBDEoFZWRtOFrdaJAdUdoeawuV8HbzlbJGClZROn7/7s1WXFxxGR+Ug0wo5BrU0yqE6S7t
Op4MMGvMeJegkslNDMIHMg3CG1CmQ1gje8lcXAjjMKYPr6px5/uAJ/NOBh+H9kFTAeMP698RxELU
/8CG6UU07vAJv7oKc2NhXQ9SgDjwyy+gN5fVqs2tKayfoo8ppC+OqfGZBi3U90kKCD3XkNhCubaT
YZDdfsubX9AcXip2jLz/ukqA6/mFdScQi3cyG8pd8CEhN8eK92hpxWKI4gfwRtxv7LtZH/aZMWC+
I2fbatfAOz3Elqyuu1HOM4lbx04zacXtPbbNBikVIJMgBtG3c1MKAyH35bJQ0v0eH7OS/v7P+1rG
tWY8Tgj2Zr80N49tpr/772bJMpEOiJeM4CvG21J1VcMiK4iiCMxinWhsxigxubKG/kXFOWouCN4e
2HaE4Y7kR2DlF88i7Ws7h103RuXQwD0uIp3/PtXH8ZjOkiiDnVUSb/8K3kQblTda4PCmE72O/PHt
6MROlFUy0bukXzTTaFLNp3LxJud3FRqTA55Ry5Yud50WwNpP+k8Bdvbb9MXLSwVGv5QYIIsBcavF
04tMp3+ZalQIMsGEk5ceZqy6NYglnkUwTED7+2ex7iFESMt7Iy3ImDr4GzKH/OHCEpNXA9guWceV
V0FOngAyPW+tBNvaUJwy95njJFV9FXptV3Zy46M6LBvqWiEtfE6c8xcJS+NB3NUX5TwUbipcdzal
fJNm/iwdGMjEZd5zuvsgAApmbF5icEnswNQs1wLjWIoQdpL2zfaV15NbXxK40y2KIQ1Ib72CLdql
lQ8ogr29lMV+S9/EIopZXoos2zW74ZP7MpFnKPuBPTev7u+aiBe6+++4lTu2EeRn9rhlpX94527e
tmVLll3T1PwKx/Vikg/SRXALhPWJqr3nF2TE0Wcj1UCchzh0j7zCfFsrPl89cNLqIRav5PewF+wk
0xY4s66qzW5J2/75iDvxwvgIi4Q98V9Cownf8nLY7OBMt0ZFXsS+PM0J/80U1Ykq6BdhCKAXI55Y
m/P7YO0BMSlbkp1bB9mScQCTcb3AN4pSBcdUsLhrtdxTxsWR2jeRFfVw+Pmt1Kx40Fvojy5Y9Fqx
ZGkVsdEV+3Zh2nVnqNGC6BV0lMP9hfMlOi0dtcDVdLSLQdN/cxbozFMcOTx6WdxaRPnxDLUMLOK6
BiuVaPabzipRImezidF3SnL9tiuPKfxnpF+t+ri6Qc9cAPWkcaoiM8WCrS91G9VTjK+fKJNQ7w29
CLyd+rbUKrIH9BZLuI1A6KIPYgUv7xRWp/HOtyGgm0F0kmayXTrT1fr/BQbXh+hUtWS/Xd4JMjpz
57pzA1fZin186tNNixeVeOV9aPUDOjzYIA/maZsireQlDXlzWcdD+vju77FURHw9dqQZxdXuPOQR
RzTN2LbdEZpzn8Oq98PYyXnabFkQ+pUK/Q0hC1xH/+4xzvLaPcO1phxwFMNQSIqC0UdcNUBIufUd
yiXNzaEEJkqki3JumRkoA0PbtNoj97LQ5pCcCZYMOR+XADkv1MTZW/KZ9W4GOHjKySklp3R0FRDJ
0p22gBv2Mqm32039CTYpOS2V+hqhqb7fmDK4RbHz6Fk7bLYPt/46R8n20c/0u6u6QmU1HRCUtM5J
83y5qtG7GLAzHsw95YnzMrdUnPCf0OhM3T0WnQ9fjB5Uwm8j8efgWusltFMlT9WTf/MMNDTDFLtA
+/gC9e3HDx2C0dWdCCypdskWY6r+21+/3CI3trbYMxw6k80uYp2oRIOf9Eu350kLpg1dOecbkjm5
ojqAtJOEAb5tzYSi6XxYEEfr+L6jQumktvD8JD2hH1nKIHlMbRvYZATmQ2jvvIpQEg8TS8/kTX3w
ZLwOmimxwSrTMSirEoaNWGm61l6wu/TwxT3Rtcoh+U9bow/QnBC3tJXV8YSXqoh6z36iaJB7nrUG
BqnJA+/8ljdvu1CFIn0lYa4EQEN2Yw0UHJOkPJJca3lpKmTFGQ3BkQPnp/18KPtEeO7xJvkAOjXt
z5q9xeZDzq5pCkauiNimBjb+IxZDrGkkHsHB4ZpxiaQWRdyTY8BOpdSflzjWCPNw+TarM+mpxdwX
3Sa2NBZWTne+0klNO5muEgGY2ZBcM4XIPP3xaLzR77PXpiCp+Ze6bKPvnzATMliOifufA0OLK0lc
STUn7RK6XrkfWg3QR1kcBy7bv67sIz9L9Y49ZOqKSIcLDZwLeVMs+JFqVDooxaJSfJ5y5Qfj36yV
yU67F9HIZxwR8ErgboOYH+nEp+4+JH7bJ65P6NXvcayyi6p94257WyykehAiLdWszkjeTd+eAppR
gnlSBbRNYsIeD++pOapAoSM9nM0KZ8gUjABzkxfL/NLX/Z0wnYLtD5pnhL12b2K6UhMX/ezQbJ7W
Wh+aWXgowju78KSnYSdC5AwnZBGYLSY6UbOsTHdBDy55woqwhYg4cpm+zx+sk35gLL05raOFBhYx
CKSLYoevHuyF983rlS/GdA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
