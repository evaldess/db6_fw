// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Fri May 15 01:06:08 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_gbt40_mb_sim_netlist.v
// Design      : mmcm_gbt40_mb
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (p_clk40_out,
    p_clk40_90_out,
    p_clk40_180_out,
    p_clk40_270_out,
    p_clk80_out,
    p_clk280_out,
    p_clk560_out,
    p_daddr_in,
    p_dclk_in,
    p_den_in,
    p_din_in,
    p_dout_out,
    p_drdy_out,
    p_dwe_in,
    p_psclk_in,
    p_psen_in,
    p_psincdec_in,
    p_psdone_out,
    reset,
    p_input_clk_stopped_out,
    p_clkfb_stopped_out,
    p_locked_out,
    p_cddcdone_out,
    p_cddcreq_in,
    p_clk_in);
  output p_clk40_out;
  output p_clk40_90_out;
  output p_clk40_180_out;
  output p_clk40_270_out;
  output p_clk80_out;
  output p_clk280_out;
  output p_clk560_out;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_psclk_in;
  input p_psen_in;
  input p_psincdec_in;
  output p_psdone_out;
  input reset;
  output p_input_clk_stopped_out;
  output p_clkfb_stopped_out;
  output p_locked_out;
  output p_cddcdone_out;
  input p_cddcreq_in;
  input p_clk_in;

  wire p_cddcdone_out;
  wire p_cddcreq_in;
  wire p_clk280_out;
  wire p_clk40_180_out;
  wire p_clk40_270_out;
  wire p_clk40_90_out;
  wire p_clk40_out;
  wire p_clk560_out;
  wire p_clk80_out;
  (* IBUF_LOW_PWR *) wire p_clk_in;
  wire p_clkfb_stopped_out;
  wire [6:0]p_daddr_in;
  wire p_dclk_in;
  wire p_den_in;
  wire [15:0]p_din_in;
  wire [15:0]p_dout_out;
  wire p_drdy_out;
  wire p_dwe_in;
  wire p_input_clk_stopped_out;
  wire p_locked_out;
  wire p_psclk_in;
  wire p_psdone_out;
  wire p_psen_in;
  wire p_psincdec_in;
  wire reset;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mmcm_gbt40_mb_clk_wiz inst
       (.p_cddcdone_out(p_cddcdone_out),
        .p_cddcreq_in(p_cddcreq_in),
        .p_clk280_out(p_clk280_out),
        .p_clk40_180_out(p_clk40_180_out),
        .p_clk40_270_out(p_clk40_270_out),
        .p_clk40_90_out(p_clk40_90_out),
        .p_clk40_out(p_clk40_out),
        .p_clk560_out(p_clk560_out),
        .p_clk80_out(p_clk80_out),
        .p_clk_in(p_clk_in),
        .p_clkfb_stopped_out(p_clkfb_stopped_out),
        .p_daddr_in(p_daddr_in),
        .p_dclk_in(p_dclk_in),
        .p_den_in(p_den_in),
        .p_din_in(p_din_in),
        .p_dout_out(p_dout_out),
        .p_drdy_out(p_drdy_out),
        .p_dwe_in(p_dwe_in),
        .p_input_clk_stopped_out(p_input_clk_stopped_out),
        .p_locked_out(p_locked_out),
        .p_psclk_in(p_psclk_in),
        .p_psdone_out(p_psdone_out),
        .p_psen_in(p_psen_in),
        .p_psincdec_in(p_psincdec_in),
        .reset(reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mmcm_gbt40_mb_clk_wiz
   (p_clk40_out,
    p_clk40_90_out,
    p_clk40_180_out,
    p_clk40_270_out,
    p_clk80_out,
    p_clk280_out,
    p_clk560_out,
    p_daddr_in,
    p_dclk_in,
    p_den_in,
    p_din_in,
    p_dout_out,
    p_drdy_out,
    p_dwe_in,
    p_psclk_in,
    p_psen_in,
    p_psincdec_in,
    p_psdone_out,
    reset,
    p_input_clk_stopped_out,
    p_clkfb_stopped_out,
    p_locked_out,
    p_cddcdone_out,
    p_cddcreq_in,
    p_clk_in);
  output p_clk40_out;
  output p_clk40_90_out;
  output p_clk40_180_out;
  output p_clk40_270_out;
  output p_clk80_out;
  output p_clk280_out;
  output p_clk560_out;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_psclk_in;
  input p_psen_in;
  input p_psincdec_in;
  output p_psdone_out;
  input reset;
  output p_input_clk_stopped_out;
  output p_clkfb_stopped_out;
  output p_locked_out;
  output p_cddcdone_out;
  input p_cddcreq_in;
  input p_clk_in;

  wire clkfbout_buf_mmcm_gbt40_mb;
  wire clkfbout_mmcm_gbt40_mb;
  wire p_cddcdone_out;
  wire p_cddcreq_in;
  wire p_clk280_out;
  wire p_clk280_out_mmcm_gbt40_mb;
  wire p_clk280_out_mmcm_gbt40_mb_en_clk;
  wire p_clk40_180_out;
  wire p_clk40_180_out_mmcm_gbt40_mb;
  wire p_clk40_180_out_mmcm_gbt40_mb_en_clk;
  wire p_clk40_270_out;
  wire p_clk40_270_out_mmcm_gbt40_mb;
  wire p_clk40_270_out_mmcm_gbt40_mb_en_clk;
  wire p_clk40_90_out;
  wire p_clk40_90_out_mmcm_gbt40_mb;
  wire p_clk40_90_out_mmcm_gbt40_mb_en_clk;
  wire p_clk40_out;
  wire p_clk40_out_mmcm_gbt40_mb;
  wire p_clk40_out_mmcm_gbt40_mb_en_clk;
  wire p_clk560_out;
  wire p_clk560_out_mmcm_gbt40_mb;
  wire p_clk560_out_mmcm_gbt40_mb_en_clk;
  wire p_clk80_out;
  wire p_clk80_out_mmcm_gbt40_mb;
  wire p_clk80_out_mmcm_gbt40_mb_en_clk;
  wire p_clk_in;
  wire p_clk_in_mmcm_gbt40_mb;
  wire p_clkfb_stopped_out;
  wire [6:0]p_daddr_in;
  wire p_dclk_in;
  wire p_den_in;
  wire [15:0]p_din_in;
  wire [15:0]p_dout_out;
  wire p_drdy_out;
  wire p_dwe_in;
  wire p_input_clk_stopped_out;
  wire p_locked_out;
  wire p_psclk_in;
  wire p_psdone_out;
  wire p_psen_in;
  wire p_psincdec_in;
  wire reset;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg1;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg2;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg3;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg4;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg5;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg6;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg7;
  wire NLW_mmcme3_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcme3_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcme3_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcme3_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcme3_adv_inst_CLKOUT3B_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "BUFG" *) 
  BUFGCE #(
    .CE_TYPE("ASYNC"),
    .SIM_DEVICE("ULTRASCALE")) 
    clkf_buf
       (.CE(1'b1),
        .I(clkfbout_mmcm_gbt40_mb),
        .O(clkfbout_buf_mmcm_gbt40_mb));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibuf
       (.I(p_clk_in),
        .O(p_clk_in_mmcm_gbt40_mb));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout1_buf
       (.CE(seq_reg1[7]),
        .I(p_clk40_out_mmcm_gbt40_mb),
        .O(p_clk40_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout1_buf_en
       (.CE(1'b1),
        .I(p_clk40_out_mmcm_gbt40_mb),
        .O(p_clk40_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout2_buf
       (.CE(seq_reg2[7]),
        .I(p_clk40_90_out_mmcm_gbt40_mb),
        .O(p_clk40_90_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout2_buf_en
       (.CE(1'b1),
        .I(p_clk40_90_out_mmcm_gbt40_mb),
        .O(p_clk40_90_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout3_buf
       (.CE(seq_reg3[7]),
        .I(p_clk40_180_out_mmcm_gbt40_mb),
        .O(p_clk40_180_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout3_buf_en
       (.CE(1'b1),
        .I(p_clk40_180_out_mmcm_gbt40_mb),
        .O(p_clk40_180_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout4_buf
       (.CE(seq_reg4[7]),
        .I(p_clk40_270_out_mmcm_gbt40_mb),
        .O(p_clk40_270_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout4_buf_en
       (.CE(1'b1),
        .I(p_clk40_270_out_mmcm_gbt40_mb),
        .O(p_clk40_270_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout5_buf
       (.CE(seq_reg5[7]),
        .I(p_clk80_out_mmcm_gbt40_mb),
        .O(p_clk80_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout5_buf_en
       (.CE(1'b1),
        .I(p_clk80_out_mmcm_gbt40_mb),
        .O(p_clk80_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout6_buf
       (.CE(seq_reg6[7]),
        .I(p_clk280_out_mmcm_gbt40_mb),
        .O(p_clk280_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout6_buf_en
       (.CE(1'b1),
        .I(p_clk280_out_mmcm_gbt40_mb),
        .O(p_clk280_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout7_buf
       (.CE(seq_reg7[7]),
        .I(p_clk560_out_mmcm_gbt40_mb),
        .O(p_clk560_out));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    clkout7_buf_en
       (.CE(1'b1),
        .I(p_clk560_out_mmcm_gbt40_mb),
        .O(p_clk560_out_mmcm_gbt40_mb_en_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME3_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(28.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(24.950000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(28.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(28),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(90.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(28),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(180.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(28),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(270.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(14),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(4),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(2),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKFBIN_INVERTED(1'b0),
    .IS_CLKIN1_INVERTED(1'b0),
    .IS_CLKIN2_INVERTED(1'b0),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcme3_adv_inst
       (.CDDCDONE(p_cddcdone_out),
        .CDDCREQ(p_cddcreq_in),
        .CLKFBIN(clkfbout_buf_mmcm_gbt40_mb),
        .CLKFBOUT(clkfbout_mmcm_gbt40_mb),
        .CLKFBOUTB(NLW_mmcme3_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(p_clkfb_stopped_out),
        .CLKIN1(p_clk_in_mmcm_gbt40_mb),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(p_input_clk_stopped_out),
        .CLKOUT0(p_clk40_out_mmcm_gbt40_mb),
        .CLKOUT0B(NLW_mmcme3_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(p_clk40_90_out_mmcm_gbt40_mb),
        .CLKOUT1B(NLW_mmcme3_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(p_clk40_180_out_mmcm_gbt40_mb),
        .CLKOUT2B(NLW_mmcme3_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(p_clk40_270_out_mmcm_gbt40_mb),
        .CLKOUT3B(NLW_mmcme3_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(p_clk80_out_mmcm_gbt40_mb),
        .CLKOUT5(p_clk280_out_mmcm_gbt40_mb),
        .CLKOUT6(p_clk560_out_mmcm_gbt40_mb),
        .DADDR(p_daddr_in),
        .DCLK(p_dclk_in),
        .DEN(p_den_in),
        .DI(p_din_in),
        .DO(p_dout_out),
        .DRDY(p_drdy_out),
        .DWE(p_dwe_in),
        .LOCKED(p_locked_out),
        .PSCLK(p_psclk_in),
        .PSDONE(p_psdone_out),
        .PSEN(p_psen_in),
        .PSINCDEC(p_psincdec_in),
        .PWRDWN(1'b0),
        .RST(reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[0] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg1[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[1] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[0]),
        .Q(seq_reg1[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[2] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[1]),
        .Q(seq_reg1[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[3] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[2]),
        .Q(seq_reg1[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[4] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[3]),
        .Q(seq_reg1[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[5] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[4]),
        .Q(seq_reg1[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[6] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[5]),
        .Q(seq_reg1[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg1_reg[7] 
       (.C(p_clk40_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[6]),
        .Q(seq_reg1[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[0] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg2[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[1] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[0]),
        .Q(seq_reg2[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[2] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[1]),
        .Q(seq_reg2[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[3] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[2]),
        .Q(seq_reg2[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[4] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[3]),
        .Q(seq_reg2[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[5] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[4]),
        .Q(seq_reg2[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[6] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[5]),
        .Q(seq_reg2[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg2_reg[7] 
       (.C(p_clk40_90_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg2[6]),
        .Q(seq_reg2[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[0] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg3[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[1] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[0]),
        .Q(seq_reg3[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[2] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[1]),
        .Q(seq_reg3[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[3] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[2]),
        .Q(seq_reg3[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[4] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[3]),
        .Q(seq_reg3[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[5] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[4]),
        .Q(seq_reg3[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[6] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[5]),
        .Q(seq_reg3[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg3_reg[7] 
       (.C(p_clk40_180_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg3[6]),
        .Q(seq_reg3[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[0] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg4[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[1] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[0]),
        .Q(seq_reg4[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[2] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[1]),
        .Q(seq_reg4[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[3] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[2]),
        .Q(seq_reg4[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[4] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[3]),
        .Q(seq_reg4[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[5] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[4]),
        .Q(seq_reg4[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[6] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[5]),
        .Q(seq_reg4[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg4_reg[7] 
       (.C(p_clk40_270_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg4[6]),
        .Q(seq_reg4[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[0] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg5[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[1] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[0]),
        .Q(seq_reg5[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[2] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[1]),
        .Q(seq_reg5[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[3] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[2]),
        .Q(seq_reg5[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[4] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[3]),
        .Q(seq_reg5[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[5] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[4]),
        .Q(seq_reg5[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[6] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[5]),
        .Q(seq_reg5[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg5_reg[7] 
       (.C(p_clk80_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg5[6]),
        .Q(seq_reg5[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[0] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg6[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[1] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[0]),
        .Q(seq_reg6[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[2] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[1]),
        .Q(seq_reg6[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[3] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[2]),
        .Q(seq_reg6[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[4] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[3]),
        .Q(seq_reg6[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[5] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[4]),
        .Q(seq_reg6[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[6] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[5]),
        .Q(seq_reg6[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg6_reg[7] 
       (.C(p_clk280_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg6[6]),
        .Q(seq_reg6[7]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[0] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(p_locked_out),
        .Q(seq_reg7[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[1] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[0]),
        .Q(seq_reg7[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[2] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[1]),
        .Q(seq_reg7[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[3] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[2]),
        .Q(seq_reg7[3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[4] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[3]),
        .Q(seq_reg7[4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[5] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[4]),
        .Q(seq_reg7[5]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[6] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[5]),
        .Q(seq_reg7[6]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    \seq_reg7_reg[7] 
       (.C(p_clk560_out_mmcm_gbt40_mb_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg7[6]),
        .Q(seq_reg7[7]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
