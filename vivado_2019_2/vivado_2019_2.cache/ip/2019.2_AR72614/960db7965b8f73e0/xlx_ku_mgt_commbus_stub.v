// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Thu Jun 18 00:19:37 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ xlx_ku_mgt_commbus_stub.v
// Design      : xlx_ku_mgt_commbus
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlx_ku_mgt_commbus_gtwizard_top,Vivado 2019.2_AR72614" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(gtwiz_userclk_tx_active_in, 
  gtwiz_userclk_rx_active_in, gtwiz_buffbypass_tx_reset_in, 
  gtwiz_buffbypass_tx_start_user_in, gtwiz_buffbypass_tx_done_out, 
  gtwiz_buffbypass_tx_error_out, gtwiz_buffbypass_rx_reset_in, 
  gtwiz_buffbypass_rx_start_user_in, gtwiz_buffbypass_rx_done_out, 
  gtwiz_buffbypass_rx_error_out, gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, 
  gtwiz_reset_tx_pll_and_datapath_in, gtwiz_reset_tx_datapath_in, 
  gtwiz_reset_rx_pll_and_datapath_in, gtwiz_reset_rx_datapath_in, 
  gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, gtwiz_reset_rx_done_out, 
  gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, drpaddr_common_in, drpclk_common_in, 
  drpdi_common_in, drpen_common_in, drpwe_common_in, gtrefclk01_in, qpll1refclksel_in, 
  drpdo_common_out, drprdy_common_out, qpll1fbclklost_out, qpll1lock_out, qpll1outclk_out, 
  qpll1outrefclk_out, qpll1refclklost_out, gtgrefclk_in, gthrxn_in, gthrxp_in, rxusrclk_in, 
  rxusrclk2_in, txusrclk_in, txusrclk2_in, gthtxn_out, gthtxp_out, gtpowergood_out, 
  rxoutclk_out, rxpmaresetdone_out, txoutclk_out, txpmaresetdone_out, 
  txprgdivresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_buffbypass_tx_reset_in[0:0],gtwiz_buffbypass_tx_start_user_in[0:0],gtwiz_buffbypass_tx_done_out[0:0],gtwiz_buffbypass_tx_error_out[0:0],gtwiz_buffbypass_rx_reset_in[0:0],gtwiz_buffbypass_rx_start_user_in[0:0],gtwiz_buffbypass_rx_done_out[0:0],gtwiz_buffbypass_rx_error_out[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[119:0],gtwiz_userdata_rx_out[119:0],drpaddr_common_in[8:0],drpclk_common_in[0:0],drpdi_common_in[15:0],drpen_common_in[0:0],drpwe_common_in[0:0],gtrefclk01_in[0:0],qpll1refclksel_in[2:0],drpdo_common_out[15:0],drprdy_common_out[0:0],qpll1fbclklost_out[0:0],qpll1lock_out[0:0],qpll1outclk_out[0:0],qpll1outrefclk_out[0:0],qpll1refclklost_out[0:0],gtgrefclk_in[2:0],gthrxn_in[2:0],gthrxp_in[2:0],rxusrclk_in[2:0],rxusrclk2_in[2:0],txusrclk_in[2:0],txusrclk2_in[2:0],gthtxn_out[2:0],gthtxp_out[2:0],gtpowergood_out[2:0],rxoutclk_out[2:0],rxpmaresetdone_out[2:0],txoutclk_out[2:0],txpmaresetdone_out[2:0],txprgdivresetdone_out[2:0]" */;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [119:0]gtwiz_userdata_tx_in;
  output [119:0]gtwiz_userdata_rx_out;
  input [8:0]drpaddr_common_in;
  input [0:0]drpclk_common_in;
  input [15:0]drpdi_common_in;
  input [0:0]drpen_common_in;
  input [0:0]drpwe_common_in;
  input [0:0]gtrefclk01_in;
  input [2:0]qpll1refclksel_in;
  output [15:0]drpdo_common_out;
  output [0:0]drprdy_common_out;
  output [0:0]qpll1fbclklost_out;
  output [0:0]qpll1lock_out;
  output [0:0]qpll1outclk_out;
  output [0:0]qpll1outrefclk_out;
  output [0:0]qpll1refclklost_out;
  input [2:0]gtgrefclk_in;
  input [2:0]gthrxn_in;
  input [2:0]gthrxp_in;
  input [2:0]rxusrclk_in;
  input [2:0]rxusrclk2_in;
  input [2:0]txusrclk_in;
  input [2:0]txusrclk2_in;
  output [2:0]gthtxn_out;
  output [2:0]gthtxp_out;
  output [2:0]gtpowergood_out;
  output [2:0]rxoutclk_out;
  output [2:0]rxpmaresetdone_out;
  output [2:0]txoutclk_out;
  output [2:0]txpmaresetdone_out;
  output [2:0]txprgdivresetdone_out;
endmodule
