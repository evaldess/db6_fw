// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Thu Jun 18 17:53:37 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ xlx_ku_mgt_commbus_stub.v
// Design      : xlx_ku_mgt_commbus
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlx_ku_mgt_commbus_gtwizard_top,Vivado 2019.2_AR72614" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(gtwiz_userclk_tx_active_in, 
  gtwiz_userclk_rx_active_in, gtwiz_buffbypass_tx_reset_in, 
  gtwiz_buffbypass_tx_start_user_in, gtwiz_buffbypass_tx_done_out, 
  gtwiz_buffbypass_tx_error_out, gtwiz_buffbypass_rx_reset_in, 
  gtwiz_buffbypass_rx_start_user_in, gtwiz_buffbypass_rx_done_out, 
  gtwiz_buffbypass_rx_error_out, gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, 
  gtwiz_reset_tx_pll_and_datapath_in, gtwiz_reset_tx_datapath_in, 
  gtwiz_reset_rx_pll_and_datapath_in, gtwiz_reset_rx_datapath_in, 
  gtwiz_reset_qpll1lock_in, gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, 
  gtwiz_reset_rx_done_out, gtwiz_reset_qpll1reset_out, gtwiz_userdata_tx_in, 
  gtwiz_userdata_rx_out, cpllrefclksel_in, drpaddr_in, drpclk_in, drpdi_in, drpen_in, drpwe_in, 
  gtgrefclk_in, gthrxn_in, gthrxp_in, gtrefclk0_in, gtrefclk1_in, qpll0clk_in, qpll0refclk_in, 
  qpll1clk_in, qpll1refclk_in, rxoutclksel_in, rxsysclksel_in, rxusrclk_in, rxusrclk2_in, 
  txoutclksel_in, txsysclksel_in, txusrclk_in, txusrclk2_in, drpdo_out, drprdy_out, gthtxn_out, 
  gthtxp_out, gtpowergood_out, rxoutclk_out, rxpmaresetdone_out, txoutclk_out, 
  txpmaresetdone_out, txprgdivresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_buffbypass_tx_reset_in[0:0],gtwiz_buffbypass_tx_start_user_in[0:0],gtwiz_buffbypass_tx_done_out[0:0],gtwiz_buffbypass_tx_error_out[0:0],gtwiz_buffbypass_rx_reset_in[0:0],gtwiz_buffbypass_rx_start_user_in[0:0],gtwiz_buffbypass_rx_done_out[0:0],gtwiz_buffbypass_rx_error_out[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_qpll1lock_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_reset_qpll1reset_out[0:0],gtwiz_userdata_tx_in[119:0],gtwiz_userdata_rx_out[119:0],cpllrefclksel_in[8:0],drpaddr_in[26:0],drpclk_in[2:0],drpdi_in[47:0],drpen_in[2:0],drpwe_in[2:0],gtgrefclk_in[2:0],gthrxn_in[2:0],gthrxp_in[2:0],gtrefclk0_in[2:0],gtrefclk1_in[2:0],qpll0clk_in[2:0],qpll0refclk_in[2:0],qpll1clk_in[2:0],qpll1refclk_in[2:0],rxoutclksel_in[8:0],rxsysclksel_in[5:0],rxusrclk_in[2:0],rxusrclk2_in[2:0],txoutclksel_in[8:0],txsysclksel_in[5:0],txusrclk_in[2:0],txusrclk2_in[2:0],drpdo_out[47:0],drprdy_out[2:0],gthtxn_out[2:0],gthtxp_out[2:0],gtpowergood_out[2:0],rxoutclk_out[2:0],rxpmaresetdone_out[2:0],txoutclk_out[2:0],txpmaresetdone_out[2:0],txprgdivresetdone_out[2:0]" */;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input [0:0]gtwiz_reset_qpll1lock_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output [0:0]gtwiz_reset_qpll1reset_out;
  input [119:0]gtwiz_userdata_tx_in;
  output [119:0]gtwiz_userdata_rx_out;
  input [8:0]cpllrefclksel_in;
  input [26:0]drpaddr_in;
  input [2:0]drpclk_in;
  input [47:0]drpdi_in;
  input [2:0]drpen_in;
  input [2:0]drpwe_in;
  input [2:0]gtgrefclk_in;
  input [2:0]gthrxn_in;
  input [2:0]gthrxp_in;
  input [2:0]gtrefclk0_in;
  input [2:0]gtrefclk1_in;
  input [2:0]qpll0clk_in;
  input [2:0]qpll0refclk_in;
  input [2:0]qpll1clk_in;
  input [2:0]qpll1refclk_in;
  input [8:0]rxoutclksel_in;
  input [5:0]rxsysclksel_in;
  input [2:0]rxusrclk_in;
  input [2:0]rxusrclk2_in;
  input [8:0]txoutclksel_in;
  input [5:0]txsysclksel_in;
  input [2:0]txusrclk_in;
  input [2:0]txusrclk2_in;
  output [47:0]drpdo_out;
  output [2:0]drprdy_out;
  output [2:0]gthtxn_out;
  output [2:0]gthtxp_out;
  output [2:0]gtpowergood_out;
  output [2:0]rxoutclk_out;
  output [2:0]rxpmaresetdone_out;
  output [2:0]txoutclk_out;
  output [2:0]txpmaresetdone_out;
  output [2:0]txprgdivresetdone_out;
endmodule
