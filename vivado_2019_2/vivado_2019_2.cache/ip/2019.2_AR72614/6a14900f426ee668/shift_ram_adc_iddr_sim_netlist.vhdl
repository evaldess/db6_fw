-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed May 27 20:03:05 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ shift_ram_adc_iddr_sim_netlist.vhdl
-- Design      : shift_ram_adc_iddr
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OTm5CjpG/km3D3WLidx+2HAHwJ14h1YFhnsfRLbjVbOA9cL+3eHHCiC8srSbKNfiKqg44+FAfWiM
xIpeI28LwmtYE4TtfjQsjJNSMTeSB5YZCDHUTSh+wq4y/7huZirDgvcOH0puEE3mUCATa4b/952f
QGg2tTf+pc/VM26HQtFQqcQTKSDWwkTB/JHNwl5hcybAdcji9ScGRlaZZ6fsKGWzNYnxEGyxYYFQ
wvcj7WJxdIqjba15mWutNWXs+KlHhUOcEH3JcwvB48CX2FIQeXa/TG1ecr1a9atcfNXI273hVzw6
CpyiTW/nBYrIw5p7+CVuDwBhzFEmPktGMDCmQw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OsspvzRazVFf2c7qINWPzJA9XBR7qCFylwO+36uqOZigPrGQO+xbF9nljah50n5dWi0dxSu7uAhm
ths43nABSjHrzuKck35s5/22cH5dQWMFGkspMOtJwY82Jm5ZM1SSBPsQ7YqFxdy5AVwvMofsxy3W
m731lgDKMffUBCPLBTkSML0HjbYB3+qyHXgu09MujVQdcyWTKtumoiSdmirnXK/uCKTQX3sdb308
0sxTd0c/6ZEZ0pIFIIW6e/dTdAqiqFdGT0YyNS8KUg7rXossbBC+PYlGQpG/4ilLlYnWDb3O9I2Y
E3x2gxQgswTKjLWbRUnFFGQAXNY2TVNMpRl93w==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17728)
`protect data_block
Aohcq7OkJxAcLvzop9cAiKx9OhR4vNdHNQH+afta/AqPKS/qI5KpkhyN/nnBPD+ocF+SaLGnQTTo
in3DEcpCRt5J2AjWIviLK2IYc4wxqGOLCE5UK7Upj/GYVbKEBncYu0GRkSr2ZsUcO6U2sqKPFg/d
dmPqjf+RaR0NNJjnxaqg4sC2ZGLKjEHSmFLzefJ+KKGc1Qe0QANR3uFhENocnBrjT1u69/I+PEuv
s3i7V5AAxYqCAEAPoqhPQVboVu3RWMVY+bJNRezAUVuu/zM0pyAXabtTjQbPZHaVbUFvVXLJckh/
SZy9zr22Xz2I03mQLm0n0hBXRY7fl+KRMILKnE6ZbOTgtJMZwiDB5TZUM93U4p7pb+yqL/7VUSJN
6bHO0zvWI4rqwKE4VCOC8IbHa/tD3eIm57ZwRmLqcXJ+CytO/+31ODhcCPf9fHEQ0xa0gqax1Zrx
NurRje+Yuoj/6hG7CyyZwirycE9Cl2vBhD9zSjo5G6U32ZxGE6Xl9C5ZRzTGgJTpOHmxnMomHDzY
houy1lB1rzP98Ss8J84W+r7mr5DXqr2ay7mX3n4GR9EfoHRpVkqFzmSOcbrJFpA2kXKZ6FpaEcHH
QGzLABB5w7X3OPQDqaVr1/s0vZ0bs1jyFr+e26HxVCXcgKoVEg5ukrAz1jKHg0q3N98kwPaQEido
aLCk9kBGxVHa9Crr0Ry28SAjk6V5ublcbjsSNzJWe9bc4pWN+Aaz0mq3UGtNqxgGPMM09+qQRtr+
TovNaWeV8cVBotxTMfS+FnbwQV08PNI9FH98f9EJO568Sy/JhdMxq8c/brV99XTZZBD70VNPNfRy
eltBY6PRwrlmq+xiOmETcCLx0bXd40cJFPPbLlfbB06jJKkCasmwO2BIUsN/dL7yGiZ+xQ7iHmO3
fMLvs8I3ZSz1Qge8znM71SmDonmI4oNYKJeT7jqQNi+1RCi4lSaaPHsxNKmFYTcUFoIfARbSlEmm
mM4pC6qxl5/x6FVooeeJ8l2g17jC7gJRkgyJUhiLTr6DXFlsgWNee5Z9eRBLs0sAz0xnB1hI/mA6
cOee/xGG0Pk4Vb8iZeKvlech9Y5XOEPTffJl87ZmFEFAVsXT4vF8xUTmsifKqiCPRA7oBbfCVlUA
ZTN/xQ5+0FNLobdJJhc6e5zv8PSEoo/iNZ7/s6kveenjlb5NW7RMskHwfnjrMBQltQdmv47lU2et
CHwtrJRKdvMHEIIHbQ30T80wTsTuRrMie85nYbqnaJn6Eup6MKDXhpEEo3rPX6l8dHywT0KTmY0H
MERxUjE1SPPfpLyYG/BooXMuLhz1Ec0BDMlv3ocJXCFkw9fXEbaPQwXfI2hRZpAkl2Js72cdxTz7
GSPvO/WTGQTk2qu0LV7g7ZaWEHvGeLavIXLggtwBAkKRzjGs4rLp8Cu73Ao2WGJpX/qKizfRKJhj
idZIje4zKFw1EsM+ZTojHCs+jXniMh1kgtp+InNo/JNrZrnFB3DbouqeFn+XcmQq7b2dKOAafF/A
+74+pTZY3w+hUnM9syvTm+9VcwB2FKHv0yybBLynszBUTvS1oM8K8wXWNwf3ECN90if9xg0EsMkx
2szz6e9ZAphxv3Wax7QPJhpBwFfwcEc3vkfWA7NcftR4EMoC03YdmzRpMU2OO7nsLy3einqPeCa3
Dw/yVTtxVZvQ/1X44oEJJa4kOUvQGudd0ankzTEpsEiywKWlSZzDHsfJtUI9RVdMoL9nZvJM/zkT
OnYmRTgiK1oXVSK1zfUUqb5EvoFRBSta1yRTlUgzA6QakYlIVGZsxn+KJewRkinldxrkZUBXsCrZ
9LmGWm7kgMIMd+hNvsuTZnTFuvPxCQWK7crhPywhLg6t/SbZdL9tQg6u+OzupeZ745ODtuP3txTj
PRmRGlOFu4xyYgeqkC8+G5IL1bmmDaMVBb3yMHmy0LwI01OQO4yvcJuwDstNHf3L//poVIe1RbYZ
+UFztN3NE/zrEztBAlaUfq4ZzsGMZJULK2lzG10T/iVvl5o96EQN53A+bt99pC3AaZQ1v3cl78Vg
oYAHhzSh0DGR+iLhpVKTKzI6DocjA68m8b09kz6VJTTlFKbvP2znjpfyOdOI5j5xdgVd19+E3mmH
CfI6ScX0Bm7Q9tCpnb9STvBvos/A/h+h4mZAfLh13WOKfPnPq6fsvm7Aao1tqAcSy9Wy7WooVRPM
wnIDBGuS9N3LY1Y+D/IODtsi+sAdUVm0Yl4vux8dzY4y1HZTw/88qXVd60Woavz31seB8N2yarVn
bE1iM5pqi/EmK6E+SyVGOLRaRLAile/RB3eVXgek3GK589qRyiZ7ElbHCcGw1SH6LFJ7bPnwo3Ys
vXZgvd715YTyuYuyVw77F7rYzMB3um6hhabZXUFbEQ1wMDBexySxk2i/FGBZHAd3sH42iKHbFp33
NuCAN4IRhSDWcphSh2AS1t+WWv2Xta3hthUARNL488RomuWeha7SES2YEplaZZsmqe66uqBdOWXA
62uBfcKGDDP1WJyAtG8Qy34jeF9+6UIIeOtDVnAnAoXMbesE3dhAhdexK4bais0FfA3LmONzyIim
vWk3tukSaqDL/l7Lsc/0S11QT2v5EYIgD4fOLNyNjprQvRNLZfg2kmhYNDfwi40Gso6vgAiIZH1B
Ps8bxnNV31nDI5FKJGRs/kdV7ISwAhVTnyUPBG6u39O910C/FVgQm9TQsnb9xMEqJrbdF3H+hdo4
V0hqsUZzOqcROTP8AlEzgS/iifra9Y0YgNl6az6K0lSev9JI3DTCccrzFSRtVuBSFBofk6dlB32B
2ddzh8Hp+UUSCc48ZYbCexFBTfn3wKzuw5rJ/cLxmhS5kPVcRAYaVMXqYHeF+yqGLMp4q2n7PfuO
vDUHt3XGrHE+d7VyNZG1K9rGX2UG8ANnhk28w2nA6biODFXax05lwxYjOaPufmgqnUOmIBOjaNk1
qO4o+vEXtUM9ykW+kYb4q7fbUQ/GIMPWpgQHFyXmtFhrJfIj3gxUtHVgSg5Gu5qaxV9AFvvlvcE4
g3a7UGIAPGmxMvAKIImVX6ChCk9lnZU4HsoqmOpnmJnY64I8FIEHdCnSFfkHBteUodUXWT1hEE7s
Vu88AjIAX+24m1IiWKN26ofQQTko+VMquwgyITuOgCrPh6dLT7ZrQulFQ/TvfAIAe7mJYzUJrJMC
fR59dlV4X2JVikR8ppWe6Yk5yCivkMw+nSZMOWkeUizPzFbr4zwzbDNfT/RSEXRf+3m8N3nHnv8m
XFv0vTGhnw9nryo3xUTWXCdnozHmyucVen+9Qp0/QxhFnixKmN9/hmxU0+5cys57XfGJVXsGTjTu
UtUQVI2A50orauUGQp74kXQPfVMF2znEb6lB8GgUkYMTd1gHT2W+6WxC/93rOwhiSmxdw9Ae+skE
xv1fR52fpspjP0RezUpaQd0AcTl7H5+Gk82Ez3MYuTiKI4xh/+bV5ypzPFGQNPCBtehXRQo2b9+b
Vq/vkocobwT97mPrkpyloubKsFIdH8TMB2lP8EAaHaBNzXg9m917X5guFt/3xBLyIFAzV2fN+6CN
zehUBdpFVnAlgUiMQ0NIYrVL3Vjtr73RioeEI5QhaE/TKF4RKtYt0uTGTY8yFUYuGaT+eb5nPpVn
gqWbsTACVC0jThYY9ycOkRU8zwYtTzmKo/rHphl2JbbHzg825oq3+Jae10fK0QbhNmfxET5+M3NE
/UW3KbiXLj/VyeWu/ZXsiJ7P7TrkA/ibUDXovAjDZtUgimH/DqAXXM3cEmsdznVIm7h+gjSnEawS
C6G9zw/ZdheIvt1Tge/Oj4OrxYsTemcY+b3jIOKaD7qBo+AoqH5pbUuuRKJRiiSgxGAVFTWdxWwv
+805ty2VcGXgfRrWiTK6b0+1CpWzr5+FZ2BZ+XPHv9KJjoWsYYxjNbF9i18MBF9Vj9whVAad+JAc
lbFcTkRpdDCl9OJiqMKboYwy36FYQ8+uYBq2dZrtnII0+1txk/TzxMPv378nRhDxqMrwtBYN9rl1
f4N4tnvkQpEwEQBAU06OF47F/4GgwpImv3Eh8dC9y2z4hKQwfMO83XGOip9wQBLbWJxZviMvqnvl
ZRS/IqLpO45IJkMzjEEtCAWkwNt60qPKe/RGF0wa1QL/YUrApwDLAARv9BPqoRG2TavwNvQ+5CTe
VWKLxGn3+AsJyf1wKdqe7yBimNrZRtl1vhWT62/Cnt8kNpPgNx56tcnaD59c1Z8+nsKqAS7SPhph
GbjV69ozs2KXdgSRjeOzhCdLqI/GW/sGy08BvWHMYaBp+tLiRAN0EJ2neXDOE+nNuL7RowrpmaUu
hzdQpbn8SM2KIFyx3pOXV5e/2Q/mmsOHj9wzuHRoSWm8gwWhSKZlkAB3sfV71xO7nNcLme+RjWI+
MyZyxAuOmuhWD20Z+CDOal4C4w3mS/dyD+Lo/MmcO4/H+ZT3HPrx1Da19qwjTNqFUUk9cxtg0x3s
/EbiHPKWKVPyBcC2G5Q8u4l4h2bKq5eRtwYYSvg1T1tkpewCvekzobXp6y+Y2jYSArnYjACZR67q
DkIR2REVMSR5YUpcZ1n09zvU1TAiw2yBKlDAGrIXTn2hKRq4gzdf4+GCd1sLVqnZuRPC96x3x6SO
4jhjWxK0gp7qtgvE+PO0wCj6QLQERXWffHUsznD+vaKuV6WmG8NpJjR6SlwpcE87ntLlY9hrSgs7
w9OG+h+Kqbr9MVkuPOtW0HiIxCW8VgkIam+8dWtdONlPJwzZVSr81dklV7FkmW9q4hwcJ52kRgA1
kGZELaU8sfKMJDNWBh4T5ZFuHDOdm8VTTE1qYotj54nmJzxd5TpctWdbG7l17hf1Z+fa2glJAlAX
Jl5UTaN9tBYfOfuNcrCjbtVGofn8Coqwawy4uinE+8KP+wLS2vK8++fwoHgq49lrRWjV8r3LEADO
O3ptSHjz7x2fk3WodxUMHn64aak+MQHUV/pNuxpHp0w6llAolVXcOFXJXrxyDlnDyBBU7+GQwabj
wcp0qhY60h3+RJjjynNTkm6nMKWYFHJag33ijZOGZVF1px7ebhhjkbNcOVwyPAHceQ0AacMaVOZL
6hwKgqcDz7kLjg7EVwknFXDS2rtJ5umEUhLl/8i9amlauu5CZMX6ttq1XB4NGMCVY9MsGrlL8JBr
5XC+gsu68jt9YW7iR6b9VqbMMbXqJVp8Z0r7o+spbEO+SGo56ch0O2RVEPwu7OrIg7DfQf6pa0uX
IWI93WLNhCTJlLlWc7aNY27QroFkCmtBGFlsUHXu57aVO5jiFqbC+l+owZKRDMSDAika1YJNqQji
0c84OKbXL/pzpQHqahxxnbIG5/1Nb3NV6Uo3EYjqvmxO6OFA7+kU4vnMupe3y3PxegSDwePB9f00
IZUP4Fbr4Hv1jFGdDr+BGfDkTbXNJOOCm7qUxLivv6zKjcLkgkvDEhTWmSaaPY7f1AkdLqPa8b3b
pgyqfNer/pvrt5KxtSrYZGp/hyjgjI/VQ3/L7m0Nfg0+46A0uWaSOj5l07Ewr5R3zJRhOoUY2Qj0
kR1wBHcoZvUPhV1TF1cIaGDqfClEAa+n4pKb8Kjh8Rpds8PArbTdNEJpZbCsN26TghEL582jmhqK
iDwBcGWG2vwDcakfkLIzcQyMHEpk1Qbs+Dj/0y1lM1ByhbMcvwZH2sIZgqpbA+5IIYFTqdzLnetj
iUWBtC6aRo98jtVwOfi+yHRimFEvFKm2/or12vPRNGCElahB0i8OL9p1YS0ipxfYBaWdKkkQQ9Nk
T+Iab17Zfyhzrd5KHsbJtzTw7x6EqCGT6HdTGET6F/k8ZjnprSvW6wUxgp8jcnxrM7WMdTO+cWuj
Zrhnc29XXL4GYf/63WUbl2cC9eIsrms+i4JhowaZlPkPUvk9gBq+4edBhGnLcBWk62AHuTt/YUUN
TvzCDTHelQHL6zCYUj7yRVpkx52Wy7ydIuIxoHrVIhTvNEraKuZVwm3z+czIiCiwqoncs11DHyaS
ljGE13OIIdOh3NTwQ9BmsPIcTG29VEPQAPUSrDP30+I22Hj5O8LoSZb3rkdSxLQv2c+nou6bGxY3
Kg8ES31NGIARIvOFtpWwrHpLc07+3dZ16lL8VTRp9AvmUXUgSxURt7SIyckC4ApqXQZpO1GZTx5H
rzTevy9MqKInMLuWTLvQi9e29IQmMdXsS4KWPQhLjgwWCG1zC9AYoXEhc3FkMTHtgiCz9SJYJkh4
Z/MWbNlXrDkJSMPLfLABhc9NFu73jzJXDmowl5aaJngWZhibBrmwtoSO/22fnw3yQ3XgYOet2TAN
clzVtIEGBwnepp6dvCWVP9ErCZ+A8TcK2vrpfA+v/qkbeO8Y0jvLcgCN1WtNTI4qEjxwIW8d6Ett
JxeZtu55A9CKlvOEpyziCc+5vlSIEqmKEwH8tvOiFOgNYPVjDgZ4XV7VFljM16qu3Tipxm/2vPvs
Si5jx3Kt+a2XGjbolvFZkkY8wck6sQ1oI6E+ctSDz8BISrjDgxCmjziIJX2O5coZcr6FBLnxMr0w
foOUSEmpKofEn6q2sNT2rEzBthCo0chuYHsSdqPq3ofvRc3y8cqvUJ34Pzx+5LAZgHaH4rOmKR1k
GSm04klWevZTqUnfxtWdkxV3IH49kYyVFIT0mYIRU4yid4bs45vBoaN5HD4CyjaH9MKPnhD7zCVZ
B9QHfVoh7IvAHwbgUD8qeVx6IKyqRa1fgLby6FnEEx6/DmgYrH639AZ87v7WqNWg33P/d0V72T2U
A5JQa+iN1mhHAd1ZACwpRuiEwnyYAth1EOt5w2vSJclWDq71Ta2VQStfI1kHJtiiCgKEb1qD1KXB
6AG682+/8X5IUO1NBe+0FnTFiJSFINjJuWr5NIK7xeIzU7DXnc2Do+KaltXJnZIUWk6KiTiujR6e
OMKE6vxp4Weoj0ptZujgJ1ZsJlCphA4eP4JyOmxqOdYpUK+xTLB0zIkSyUCu3TaVAMlAJeA9KOhu
Oh3CUKtbAda8/0p6GYgkLWflSlDBYRsL7YcsID5v7BbCI+TbjKItUmRGYLiwLrOKi3e6Wl1BRRmA
3bz+z+GLzKrolnPuMhpLy/JiLuqCJ0QiS6wlw53W6rOmop2L1g3NYC0/F5+q/TFskm4E7ndt6PqK
oxKRY2e+KtBXWUczvf3iwhwVG6sBblazJaZLw7iqZjnuELfoF4Ap4urY2bcIRRoYm/6w/UnDFNTQ
kXXoMy9g+t2ywTblps0qLItAPpDvrytedwSJ+qpS2I5Zq5cmVWlJAWKaiyfqVVaPIgoFaBhih+a3
He+lPv1vtSGjr4Pb6UXCAw6VEXL52Yu0BnyDNMP+S9hgmuWDTzRX+zDT3ti1++b0FKF8DSqy7m6D
3K4sOtrea2b7CoUjtnOQ0DWhqCDDtVRnKu5ulDi0CH0D9rLdehXgmGmboQGMj7N4nhVEwJ3bmGek
/fKMVRYV+FOFr/kPuFgj32hu4ncOYLi1w1xCl4VnR6xYybMWCHK4EAp/qJtotCnZ2nmHhgXQFhgL
WEdu1lgmQAKUGx7o6eHL564J1YRwSUtzFoRfzKSodwYjCCKYPteFLkwLUQ6Zw9ayKpvWP0oNP9AB
6UXzLYKrCzeakMNfY4E9eDZ5qMHm7Kkn9HKkH6YlOWnu3D/qm4LDznK/M0aMhsNQ4qHp6Mf+Thrm
MZTDbTs8S6HREKhlJJISuDN8ip0ll1kAYkVLsnkglVCAqxNfcUZdlt/OZMsNIJA4hgHZiW3+L7UW
Yzz9FngbWSalpV9P4WjnYGlFUJQSj6DgfWLGtu/JMFaj+NB01GW2zi8PHDn7hQhRr75BjcMO3n2U
SOAo2F8YwBB04bRt72K49XiBR9wFQhJ9fPD1nwvtug8OJX1uCx1zjhJ/qjccfRMbsLReQkYIh1p/
y/2WBxvUlHe7dnzNMtrXDyVmR+fIQn9DKWkXJToxJHHIkO7o6IbiP9a2z1yVZLj6ZAljalYNJ+qs
AL0xC5DRK4VYCATvluke/vy8pc9lPTHq/0njRQAitn1Vu9ainYIbYIj14SFMTvqSZtrMfvP5nHqL
jAo70tBnkwyzJ5QsRvi4KdNUFzk3tz6+uER5q+k2nhgxjD/W5Zt+GOuPx7Ilb2I3CqMTCGxoNahj
WoSISMuDSx8jHVv/S9yVUoFj7JesGvBhqC4w2nZFAAdQIW4WR9grZrLmIuzdQKwp4WdZZWByNOTs
DuL+8+5m3gf6VMrvv2tIAsSVJGA48XVwYclW8cqbZXAUoz3q/l185IabMoTPvs0jXk8weVbmVAd3
Oz4CoyVFX9mF7dKLDjWwU56Gq/DdwNmlHJ7OOS+7U8MOwZmk8upROPv7CUWeGdw1PSb1c3x7+60G
BjwSYpuBezFGn8kkJjfUhZu2KvRMZatq/dsrLGHiYvVH0nKf0f7mZUNudMwpgs5TVtGv60ZkRLMI
A/SFxz5QvM2nXNreP6BZ5cGZE/XTaEBquDkwYCS337MKcZicOWNhOnUSBNWpbyi8v7+gpBZbF2vp
oWs3URkluSgIMfkF2+foGrhe6VGAkbBo6si1+Nzg+e6t2O0CRy+xqbyjQ0bO/AvhGtxK89ZCKN/f
4SnbH1syViXHM+uu1/Zzv9uesb9zO4n+W3Hs2K8Z+51mcKKlIdvE44f+nj0DmHiYbthWpur1ErsJ
bIhYSPb24YOaZz0eKKmB9Z/hcv4JFA+YaQ0xe1+Fga4SLpfAU6b4CO4hHRzkSVrDTFstcWk9YbFb
bliDc2/H6xMgIy4NLl0QCX8qBP120feyOnMBET9Ruuji9VpzwG6IC8Ku9RHdVxKKm96SWXctXjUd
VPnp6LoxjYE5vG+qbor9AZjb3qSQ1rkcLTT20hLrX4YiWKz31UAflQ81vaA9S39Qa0KqE77OuWyg
BKUHa2EHaWRrE8Q+zljxlzCEuodswWH+eZW+zxSRBrMnebsjUiGIvMMOgjantrn21VDxcMe3gU8y
xFlfi2bRWlpU+GMXlBm9HylOYEHZbIjaxKblP/WdrJUovpM8zwRXXg77hXzW8rvn9h5IYapMdyJ9
ArxnUhpeLvhfm7Ey+cfLtkWEvdt+84MmoiE+mULrX9Dyuwa6JAqOAvVJO5t61jxcxGdfMRO58fSq
+VUSPzV+EVoMBlZve+2xvaTGrcEsldtGzD/xnuiTGy8Bn0UeWea8RyO/fSBTlbEJgaUIWougpjQK
uNAGZijmkrslv9eqYl7c+7oZqGnM0QrfkcNAtgfHBKISZVOC9AzutWhwZMlDyEtdnlD58Lx45QBO
9pCx6xChaTfbun+vp2ABH95VshjjcNjXyBc6oC2rEJ4Fh0g4+OZqnoWL/F9MPx+bQrZ7UWPrLSPj
3PTcEKlLUOOMeIDcVn7NNW2MK3TLu6ni2QcGuw8A4ossi/+krlUijJ3PupMolfQeiaOo3QXYy8R1
cbDXGSE45CkfJRu3TtMAHcBzEBuW+VT5t3be0VIM6zHCNokLgd/0ZPc66+ovpr2nEEWJjvj5J6pL
rKnjYuio+KAj9DSOpW2G4gT9enfe8+gmnxRns2MXPWbkJnLjob8njlEvoGzRHM0OvePM5fZ2jgcC
o1wxKQXhDtPYWaScLmoJ0vWkdF+xPzlFwOAPo+IoC7VE9Tx1lp/q3XsCmg7aIFU/C0VO/E1z0o9G
IV1JK4kcUromxdY8q3RWm0Nok48lIuO9NS1WwDAk7Aa/KdFH6LnXEBsf0gk1+NZmpsF7LUmUU8u8
S5mLe6DbRNA2gqTWjrx1ZuAmUACPYG4wJlkoV5iT0k0u24DIYo/66LcNY4nHXGX8/0zSgqGeh7iC
FV8Q9bLq5XmBf3buiBmjiQuAUpAhADpnVBh5vwopI5GI4JA0hWap8Q+H+eNb88H9iHmmKFGRToZJ
XhWk7HjYL+XW2u4/aR5T6nBietCp0Q6Ir+I8Jzy1Yo/VVvZzD1V1PtBs2rxBF28a0SHsznYwG2qs
azuHby6Cl/r1JImBCs1BGOa3YH9KS9WIVPMpI7QbP2+ZA9eGBZhYnwFFZr3LjUDX2mkrg/aaA7v0
oPWn6DZz4yzBWOZEcjNARzuy8VDZSQiC6P/VrTPRFgBuq6rmQAqCzVxlMVwDy5niviUZn+aK1WTZ
SvpefDZ+hg5nLCgIHoz8QrErt9Jjy88mOQuSdO+MCfxzs0lc4Ahu0JlfK2Q3Xf60XJPcJVEHKg7t
Pc8lu/ekwYZfRmDClDbRa/kqGBzgj4oWdlsHS88ACnE10q+N5rHtNtR2LfYkjNqSfukdPfpNPpEf
rseUyv7SuvABkAsz+V7Na/FRFdfF7OSBhuFdw42XDDvK3yKTnAX8uRdRLAVpU7zRdx+ed2C7JGSW
vKuzOlbaNsvYvqUd+lUhxpIPPaf9v46I0xSin3yIdhpwLj4/z49p+vSbjeNZrMrmwdQVPC+hYiJ7
RQl9qfUIDsVLiisIMBPIML2MXVBoNP9zeIKkgUWP1ylFDMZlyhCe+2SlW0JKKnRCIwsxjGREzjXT
LnY6TTOiANmE5bTmIEDsJTG7JdzolaP/acarIllNGnTU+khiTZo76GVinCm5Ny2QpROCjX1z/HmS
9SvPam7TA2qLV2fOs829Yd4smgE78Q2AuHIULy38IvT0IP74gzNgiNztxA1NjlOgaWvSsbUcZxmM
m62851TXOthZuxJZlbedKZwFhjajAp26awqy5RdbnON7OB8WNkim2rXcaSCvUiRRt032+tV5325G
kXD2P9NmBCWecRm6lYR/0fTRCV3Zz+qIdvMhgmpBUtJkShztGJTMAtjVfZLRfW2n4tawKwy8x/Va
fa/onPD5QdaedAW+SP9x0uwk2l29oh0s8vOqeUZR8ilXW91f/q9nRfrhAxT8aMyHP5P3UgQ+DvIA
YUSSqk2UpqI/igU+OSIsM1PU7epwKdyr6kIz6yPAQNDttNeeE8XKVdu2K/BEuOU2wG9xhHzu/BHi
p0ERkpXliewVZ1iNnKvNeRyVOn9FYVOxD9BZ6/AD8YFMxnN8tl2MAYwU5VfwtYl9mJZF7DyXrCY/
wPpbmlcWeS+x29iCXjfJDymAHF9lnRsamedoUrGrYwiI+lmwN/vWybUwKP88oZ9rAlC8iA51/+fC
vjzo+xs7nxJXp09Ay5UqyYGRalswGogdZ2n1swoCjr5bwIGzkiTYr7hFNzx1uxIdkpQnfJpEUHxF
YRSkYGr5zjoKbHZt1SFpU2T1ZuhEelkHcpKEyfewKm4PUEq0k5XKxKmqM0LFhfo6EvhxJs3usBii
TogLgJI2IN7uHmTWaTCPDBOHjQtuS76w4j8TfkJw4eQNP3bR5OCoNtdKSVf1z7Bb9LuUPuy6I6jv
QiSm1arDC/2P1lH3TIMANz3AcEVVJW93XQR8NHYOajcjf7J+uQWGB9YEge2raQH5v21WRWxFRZpX
oLrydwaU6pozV5uk7dkVAycx0UGCKzqR+/Gcyxe1wGtEGZime01vodET1lZadaMk9UVpE1SauKFN
gnUTPHdSNpcLm/35zLqnt162RyKM4u+WN7p7SeMXt4mr0cuzcQWG96hGmEU7qqzDTQb8/E6eQRPU
rlAi6hwvsRkVCMGJZ+bydsNOzpLpUuy3d/MF65uf1btGezuWXh9VuuifpWVwpOEm1N7GEEkc51Zw
qH42z6ZxDP6OiKlqcj5hAS/+uQullcPwBasVfauVbA/3ShIlkQiyhM7iJPUg1VsE7gXz/zGyNRrP
xs0+feUqF1vx6Ws35wSAFFkWJeTM1ov4zYjmcpaJNOw16bfnmD33QDVkAcjb5Ak59JMx+2F6RpmD
yArVSVL+5bRcusrX4KJQvSrieY+AIttwmVPbRsk8yiQcaUbppQr4GGfNVU9yub1ldLL5fmn6Wbe8
cdDc3MWqg4oCgMLXpqkVPECv4utF74DXWBdPFNWaX7VoC5F61bTzH4L0oVJ9VBv/kBqh9pOwkVE9
e7HyY8kfuUY/vujmGmJiqY9XW8i0qD8Q4WP+ZTmVSQP+EFsNCio6DmkLglhxK+w9PYCpS53a2bi5
IepGXLdjmYiL2cl0KVGkiQAVU118yCD3aTmnwPIfiiDOPrvnGIPKNXfvmrjYruP3cKNEPg1MwULj
8lsJzqSqBJRR/48Ji1sizxqqzmE82bmHSCySllv06TPVn6W+0mA5rn4LC6AG7lO4weZ2nDuf8hgV
QUlhnLAc2NsvRdmjORoKXeXuvF63y9iWU6ME1uQxZi2xreRzX7CDibjf1AVWPhxTQuAROwHjEG5M
Kr43SHivLVYMx1UsbpIybOUiC3Ag9mL+7ubxK6ruvv2GcENkOlIwAg4G4bOFVIHm/OHsSRl9yb2V
X9EQdNZ2Ii5KX34hz+Dab9VSaTHBiNririmZEJEJtoMVbez5vw5tbmgVuPeTfTAIye4u6K0SiEh4
lK3iLAKe4SaHgy2vtIw4j83jSXV31hKjFar26kM3u24E590NlJFXbGkBDaf2pJddfrRcidxbNvuy
wWb4UE+t+VVwx7wKDSqVIliFdnXRGMa+v3jNEZ9QMo8DBezdnMx/MyzQUmKth9CQ3KHHB0tznKqM
peJAeZDefFcQ9MJkNEfyLJELjMT+Akv8A2Du+wjjNOfZxQJCDnbOI3imgl4ULjk+orTH5OvF/Nj8
6FHPFghVxsEm0XNL9TKGovar2Y5bE6nx7jS2ysV+rx1JxIeDAHvIxYxSEEr9yjcZ8UUHLx/V/SKJ
gGtQMwK0FWnwm00LoCD3kBfe58jFgFQY/ajx93ikufmfUyzs4pD7TrbWuseF+3oNEb5MPwSr/Bct
KC65bZNoKvP140bNVB42FtAL5y1xf85n8fvyBR5W2P3XjuEdgpdfpgxMWJH8eldoVpyhRt1AH3ub
VjkoHp++Gpn/rFmfn08M5bQWu8Scs5scguXb8D5zbOtML2ZAASSenNM5RHPJnxSw/RZaV6PudDNw
ZRbLvGg+bldPVTHywOb5zFeOwq3zuJSxRIFmuijyej5q6GHOSw4jH3o/RXrNzMKiEZ14c3zZeGrP
xhf3KaWXwVQU/KFWshelytU7V8ru33mwXac4QXw1aDfUqrCKjYAnYpar1LSBovO3xWGic6mAj4MJ
igzCuYxP8Q1d3qpymA9Kw5HzP1m6CVGJKtevHoH1toUL/Z4nUybL/w+ho23v07ZrRaPq2S76S1yF
Cn8m9dk9xwlrPSy2mr9aw+dp9+6Ai76FPbgSQ+I8ZHNm7bGY405Eh4SLTDrkpVy11lGYd4DFQ63V
D3zHVAl+TABQKxZZoBeJnfd8Xrs/876vGKQ1NxrAT6PDc6m7KVQvbH+03a2+10tLoi4aiiDsBL+K
zEF/KO3HQe8aegO2xR+vvM5MxNmPg6l2IdquNTek2u/gH5HeYn9dGCcOF7vJcoGT0PYzH5BaobXg
xGzve2hgnJH4TmLu14Is/CrgIJtqQ2NZsdVzZUxIFYfcDonjDmq1A3N5vsM28o/ut+k8p+C2Izrv
uL8FFWirHdfwXKlqcnP1ZIbOVqJGG8YwBbdjKDwKuLmejrjb/F7iqa5Stdxs/un4MwhQ2Z0mC0F1
z3c8efAl6MrMU4K6O4Xw+j/uaqL/ufLH6eDfu380geNYNqKwFuY+tMbtf7WmuW9nok/R9rRQE7FV
LyUb2BkVtlZ6c5ij2m6sfQdiGcuwln8e9l/vjeziAhLXI1X/ynTr4DPJov2gsE2iHev/azwWZDnY
gW4G7FNNqMgHQy5o114kEMI9WxbTbLj139aZ8Dy0ROp3pZpww09bJqnirezKdlqLvyvdznjaq1gj
EgTPL1w+v4sxcnB6teOfSkGk30yTs8REx9ruaRk1U1GzRN60DAGuPMUY+HEY21U7AWV3BDZqzNAN
cyXzQ063fDL6Z1O+do3CxUvyf6bPXPhA3sjINmJdqVRRBJ1Avu+6eK07UPBmSfMzSRsNdPODYWcF
86hBmF8C2QJmIL/SZQUMpeyBs5EqB1TIn29u58MKR+DNZznww+qFtD/AjwK5lr+P6z7eU8BcUvkx
ar9JZYKj0lxwHZ1s+ZcQtDz810Z1Jd4Jz7MCN1CUmPDZWp3hJs7nvQTAldUVDlcDHZ/03VYhocSI
hEgajA5vvNBDHyrQcsIuxbckyWITg9WYuwyReXf72pNLC0e7T0e8kMA0Iu/RpQJ4KzKBRTRBVSZx
DFfUSyoNfd9kUGg3+Gn6DyNuvfmljYjCHX9nmXSPvbTzIUOw1A02T5yKEqUCr5V7LZmphBvN0ZNv
KS99swHg2ukwW6PpeqGq9l6GABIHK9oEEivXrgA+5FJYhJwo5hwmfTq8pa735aPpPmoirShguahl
7LS1rylWQAjkVQ4ZHFpRJYQVakDIiYSmfHG0ea5ntAWEgqFhtO/CwcbNlURIQ4hvaT7btNf16j+1
/aF67Np0hZUcysH2gx71Ax6f3sd7CxT4IQ8VEbDl+BE2ymnIw0A+BLlyWbgVF05w94WAqzLI1wfY
WeIr1HK/5OCP0nUtWaEdO7TsMx8WqVax391sJKyfAGpzU66szzp+S7PU2vH81gzgNVV/CeYFGoKa
EaX4DJNbnBU7zdR+N280GrOzeQbl0W1droMvbAaJhpxawBM5pjI4obXfHCX5Dtt1Q4RZFfGhNfrG
Wvh1iDt9Unhst4yfzryjnisfY5BJuVuluhrJnuYykhkQLwasdsyrFqdcfScPTrced244wLXWl23+
K2ZrBX0gFi9Mizr4iLI8JNBXYlLZpPZQlxB8iROMtVtJ1Jh8WCWAmoxOoWn3UmP1+Le6ZOl4RSmD
CW9/2Tx0lUDjzzdWnpF6BneD7aQs72nIjgav7X8WnkhvBppYhQSsPrLHTWrWFcneNBWSc/pGsVa3
1/0djwGbOHo8AZ0cfh4evVtF+/5fNRyAxt8MIpPYtMSns+TGqIzZU+Wmp+yHY13MeSth3GXvY6bb
w7j34kA48g7mWlZ3v1+iJUWBGvOX7T1yPkf1mYszRawUZJo/Bml7EDY8L/jwTCmXUzQPSiXIWfTd
tGqzC1XPcpJuy+bpJVcNk8u5XayVLE7iLhIleT8nGfESsk8tSFgYI9juPIaB4dD8CWgq9xYBqYeI
p8paMfT8bZvXvla8I4yu8WkGAnwKGlsm8XR6/XeDVCv4gdjE0PTP/j4DK0qi4lEos2X5p6c3IXVL
mBNuKWF6o9Vq9tkP9u19I8KYKVUylGL/KfUCk2PPRCXh9LTxx4qpErRuEI/nHcIgrroTujWMPkXp
b/zIrS6Ah4yRDnekIXXNTCo2cfQhjVAozgYVWyTqrLfpCivTJYTvNEd5MCy4/aAm425Agb4tL8tj
CJR7heA9CdHkTRChVOQdmjDRZtYN8QPKQ9K5jTIUtumbJ22PVBK4cKpjzox8SO2T9yZUzjImLGyH
fftCYarNcuX9J7dM+ciJrfWzbNqYToDWrfVI80Yw4rPDrYctxsJXXCiZwBZCRvltRehX7FdPAZUN
+ZW7sQGYkLMT3jVOQr1EKqMruMV/GgaePaHUjB1BbTfRg82KIPYgY2nqU8LmE4X6K4YeW7Bmgqyu
Ldh4W7cSe+a5y97o9thrYszR/55JqZovJIcEumfX+Ex9fIk2gG/5+IZ2KvlzwvqiIjZYVsEjY7Et
M772kt4XJv8A/JhhtF+b9idZMM9l5LUJo0Hng7ZRlBTqm0jlQoFc9TAazWGNXkBWbqYxOLcHpGJj
IWPU7L6HwUp41ECMgwpuRVx8H/wHTMrBoMGlzJrARGeedd27ZCpjlmBzX12M01p3cO0Z2o3exhjY
bWcTbrp+ZZHBliCBuaB6/9mC/ywxUcpPVRlT+RnJ4SL799+PW71Y0dw/vEW0Wgy91qZ/CoVjhvKF
BdZFRhP8cFZc2MFaoqDEE0T/AWy6V9mnsdPYIlzI/4SXAV+iBB9DCeic8LKDeQfZvoghShpSDx5M
yfrejbFl8kaVHKDObZjFEcr+IZmMAAuUuV3h6C8vHYQdXBf4Rjn7RbGaJnBDkwK9nBzIQ7BkOv5h
8QGWXKoZaDdm15hl7unhC61qBB81FeimwynxQFm5TQx33TNXswhNPchyz5sXpKLhWswTwdXU27kD
J2l93O3SGdHA+aQHklrIuNEUHGnMYPTsNUDb9uLe+AJPLYwWmxRqTPRd85zKOfsVDh56jwhERie6
/NvD47yAuYwu/SnGlzzuc6fYIxQPGyWcXcnAG63yveYARN7VBfjpBiVWQ5aodKCt/6dcFqHUUZ/z
ttKc+iJx+TKavG/K0hQYoa6JZ0fEdUojHZzlI3mnnOmTJomXDZ9wl0LZzy7gK+sEEsTnEcO7QFKi
Jw7N4gX9NOA04ZjadIMUvHtEwlAK/J3W7h4WyGMj3Yv7givzHQXcg1CMECaEc58M4A8qfEDIIoKV
rXjoc9h+9uLUx4M4oSfMN2akO/HdSEk+hsVb/SsSu4Jdwvv+TQnh/vG/fIfWcBN6N3jdL6n0QfQ+
c/0CMNB8COlkV8cJx79QeSz+LZDZE135hGXdqcPN5jS9HuzRkZ7Dk3F7A5Lu+bIhRwkaebX5Nb5s
3Ds+1iAVhJ0Up8MRmTkcaQuC5TKB9zTVnj5PHV+Q90leABdKRCWoZr3FFLnu5dPJ4ahxFW6W2tjE
JcS5AeANMYLFzo2XkY24WVqks38DM6sC024vkCaiYhyQShDYunwBfcx7HloNy6APNF14nj8CV91w
WmyFDY7soI17cjea0vhRWSHocZ0X7yfv4SmHuaWxqpVPVqogJW8O75tTdAsMXdtejYqJShwKMYPw
flJC70fB446+AaE+FxgvoVnh+8F9+hXO44ZzcyeuVUhkin2BTv2LKC7qv3kemBD6aI1z7Zmjb4iw
+rkh2sVGrwqZiLfoDZnCBK6GOj2j02R2//L4ZAoUWYENjcjj890vuDhi6dR8URZXuDxl6jCWtNDn
QISSpxqUdOj5qMDH0fQbV6N2jGU2GNoM2mjyUT5/2jSuz9HpvGJpICMrkF32mx5R79NC578ujJFs
0I5HnjYMs39gHdIqqBfJ623rg8/zik06V88I90eB/5A3Ix6kA062E8FDDfHRx/dL2NmQnMT2d50r
a5LjvG8TIKQrCPFzArJNgNWzXZpgf1uRcGMMKaB+wSo3gLMMvAN9lCFXZYUTvkauq1Tvqd2So3nF
NrJBqOYA+aP3g2BkUkuTnjxXO6u0uzb24xY/zYekzHriRWb33AtiKU/+dI055Mg3Kh2QC+rmOzlW
qzwDc2FyY2EYl/J2wJsMj57sxxIXLR6RWIlaBsKQvKbJaqAOYUT4hQRH1EmmtJPge03jSXrSsPvW
EgavpWwebcpDi09ro7JOpyTJf7mOVcB2RFHedh75U0TQozxXkweaf8H4QR2iX4rWKhVmQmZDkq7C
njt6AdO+HhKodbMcXdXy8c0bfkxKwzj+yq8SODYE7C/VEmCI05c/jhf5j+GkzL/kKskUnThojSAI
FwsphEzTu5q9KaH+MMcNIqI4aznobdbttrymw4Yxa0tUYS+eLS3jAP1/cBUQh52CR77TEDw3xeTw
MZSqaZwPzfklo3S2taoTRdACThqzhWuwVcjPC53x7yfg1Iaz47vI0EVgM7jM3qcpFl+90cn1Q0b4
23aWACzY9d2Z4Z0+6kP+rWlxYeilEEK+vk8AuObK0vIGiE7K9+vuZtsZkIeeL/tw3CXpidOWuSz+
ow4V/I7T2bDfJAxlDMNYWHw06csyfQnLVm9zEc7vY+s6ohXdD2G+vNiKH+D98rfucRIlydbcjl+a
KlEaTbMsgK3/n2Yj3JuKqFazEvj/T7IqzdIp6/fCLDA4BeS7ZJJis9CcCcy1cvfKP+J2HE9+4vpy
MtJNCX5W5fPTskda03yjkcUSv5hXPyxlFFGIjC33j7MhJX8o5nQS0Iqfr49Wna6S8JcH1st25JMg
o7+V2J+0SBk+8uteXMKfshYorQzDNaRFhmt32AXiucKq7OGxMFByn81q/zHuX3R2NaLgOLQnNE3j
43AxsVloZNtXzTOnL7iNWxmRf5mV2NR6SKl2K8OuYHioLq7hCBFv2y6vWaf1bOmfZ3t1sfx0s7ci
jHKnrnpipkSC/rhxEC2uq7A3xgmWOW5FVdbNy6Z6TN1+A02lubXqDFoIviFbBUV6+iQL0S2B0WZX
+QarLrIAMSKVOkMMe1xQ5fufi9SY2tKPB8XCduhNAjc+jb6qABFufw+/5PPuj/S1ud/3eje6o7yE
7B+xoCXO3SOrU2v9GsgU9HeNCu2KWjrvYUUDZN7IQh7eH4iTmxhVuUVopsGPLUeBIDcjpgynZZ1C
3kjhCiOr/t7kyJmkEwhvfLPc7EDIkj13YjAdidNTWHDwR349paKXbF9CjxKA6yr1qBaX4yoCOx5W
1YiUzXpa+leESIbvYhskMRE937pNrR72ZQqJSNlGY34JTlik+dsE7o4gYHcms24+BMC5mdUIAtDf
93LndcznM5DZUT/DM6K3SmpO4Sh4FKfZqJl9ekWJtv0e83gRBX6Kq4yWPKgORyGq2ZTCkopF0x8U
vtFyYHVUAwOnqV2BhZvPlqIfV7YIBGTKgOGI7F0twh79ZP362vf2U64pd4gVmvd0fPEJtlv8/AQU
N/MTrqb5yQkW5zMH6jWpxOgrNj0QrUnB8kFuP3XfO9eHpj9U7S1mPzZNEUuyZ8+WHUZVgq1iB6b5
LfdfU1Kl40iBYuYAcQapNOU8A7CPlrIzYLnzO8Fz+amyyl3ynhPfxqqA7yvCuVxJni+/yJa4XB/8
Jhg2yByJOd2VPvOwtZIm4DtQZszkpuaCGEv7EiG7SguU38IDobonlbGXBjWMJP8mZV/w26APflcF
wq56EdvUHwxywwb9XDcY0ATqK9WhWWtugRncO+A5hHCKhGcQl3jbJgPEL9xyIUXZMwZ3DwWNiCy2
ruwsF0Rh3Yqzc+Sct6FybVcV4FXkqGzuqOpX4+GhGSMIBVkCuIUCn+cERFk7vdZLBAGWWAI7IYe+
NT6hTND1GAKaJ4E3n/LrlvA/1c8mExLD5ehoBnbw5is9ITSOfG80bdDWTUn7peIOLkMTD1ru5A2J
TlV+/4sAAE5h9tzSN6foymkDEYYhbaP7sw0UaDmIlagUjU74N3bqDa5BMdFwiW/+1oo5nxhLccIA
M0YAXUlWTsMqlHKJ9zgaxZncK9upOdomFWCTU11v2XfwWOBjlQwH9bncYZnzftXNzHmOr3iKyX+8
LohUnvhDYdZOHiEa932IslbUFcUipTgCxnSWtrlPCPzPe/zxIuYYk6ss/GOnb8WVdVgquvlNAUPk
ESl1DQrhea5Swdtg707s2GE2QfZ4m0Nzz2FSW5gzLTLh+t5BegFXuofjcEozL3XAJj30A9vTn40u
dzCAYF9FURvEMB9dULBwW+6Oxqb0AG0WyvvciY3X5VOs0U5Ig1YyiYZTvphm/un33LHNXtTnZ7lX
P0D4wujT7qJoR3MyUTyxqe/jHacVe1EpYlHems0DNzr8fQxFhIEq5J1eW83N7LW5hpY+8fysbxmn
fXNf44+oZcGNwDHGJWiosHMb2iax6Hp5Jqfnap6Ivtyqqy3TRFq+zIc11ob/Fw+YYVJ45cww1ROh
S8I4IxigLfWtFfA+HI564ETnmaT3/RbRx3D2LXHb7e29FvBYudNf107lLXLT1gxMbdOWjbD1pPsH
QQWCg21U4k7lZKJICrOmqM47sm6i9mK6l/Lh/O7MsXtA9okVgiT89OyaqBkH3frkBEHvR6kHmYzS
pWJMQmxqi59fKMB4u5Akh5cDEcVYnK4Lyaze8FuMrrPrYGT4z4cEJA5JUWvbLBrmEP9esxRIxC9h
9zbsNbuMFyXHbc+NYeE1kq90HQkXkgQA0rCveEO+gLpEehG9Ppg+pNbUoA3IYv1sqt7pZl/tWgBF
Mra5cVG0AFzsHcTDSOb8fgAbS79YzTxeE7Y4NugPptgbi8n3YRAO5Mi3wR+akm9vXiMATYGx5cGK
NyYm81SD7nDhiwVHgfPKOz9WMum9kHNpaeUJO/PVvtscnnSeYxfsgSaSjT/CYOwmed7dpe3igC1Y
1aOQmPi2LpO7mEXISFYu/T6WlFNzYtM1eFrIYVzp0L4R2WGA63E3VXMZ0cdHqwbWsPhdb8TOOCFn
mwKSvx2rEegWWq0I90wZI/7fnU0zRtjXlXjc4DCBEYSdFI8PhLYYzRVnjT56iTKOHeDOHihP/g5Z
sB5irrZQk8em0OHLLFDz188H84NnENU/YrlEEAYhZcoczPnSuSizYevsQ63nZ9DPbuG2uYYDMJOf
iSvewvZiP5VJWs/Vi6wIcEKk4YIunSEorsgbV3RR0j+3iswLRCCCXSKXKUyNWEio6CMVrQASl9NU
TlMW2wRoGhh6c1ty1l7UpWnrpMsxCQCkJOiJv+Md2TF8bXnIxXxJkuD+19Bl4geL9U6nA43a1DlE
Qk6TGcyCVf6tRi5GF3wMlXpldkE2WP6tqCGnb/m95pLZzjP5ies8wGt+JEnciAn432Smv4ICn6zF
PK/Bjj81nghpbCgIdLY/lr9aqSWQqLYvMwvA/HeHF6V4E80XAitEhML9b3JNxyzWUvQI0t3bu6Sr
bjC4OhViwT/xp84LtBpAs5dS6LrKsK1NjbzUX0C7tzvig0gFBZCQ8HxlKj18NBhDtopkdNy1+NE7
OxXlQlDafRZN98IZC1+HkmbK+Mu0iJtw8XaQdEJMwYQA8+Sojlf2FuWXH0VC/rj3sK+b2zhz7Nn/
0jzgtzi3dG1cBdmcXyroN9R1AkDWGqEXxjn6DRaJ5/oioPDY9aMZmhzyNTP8DtcJfNnbG4bgHWGr
bkwS9WZAGegbuke3GLBDZp3P6/xsQ0iOwva4hagavrB+goL9qV2/Uj/UlCNU4yHAQlTC36ljE6bZ
e/f11L1bhcb13W4xUI5p6fuxV/BzLICW4w5TDkpRF4G+87ZfkLg1dI7ufFMzaGfpeeIf8Yyyf2yl
HYlgkPx6ccs5mqrGUGspsb08+bOov7FBbz/r1Nc+NbIC0Lb8mPX73TA0OFA1eWayFqvCvsOwhg6Y
TXvoayi/6gndRhPlMaaRN5lIOuMgBGm09LUZVbL4BLSNEd/eghnx5yAXwLflKcxdtzXGNr68ztMH
p7gHD0AXxxqOqUHpYdnQZvFdcrriYJZ4nvlkdVJ1+jLVZAbAlOEx7XXT+iVFBM/kIx0ZbjzLtdhO
nfEt5Vy53ADEFwAhkjBnI378m1pOhF7kf/lOXC/p5jai4TBPsSzaIjkRziCGzgaq/r7kcGB5RPap
j2U6Bi7vaRKQwnwJqLKolOxt3m+foZK7ofqtcFJ+BeFiQOx+wBreSfNEaxkN/pzXFEzya45NL4UX
K9+ydYInq+/ckDSVQZOpc55QmUjHKpUn56T/mtS41WpRewY8TVlCSF+8ZiMKOICQMQCZPk0G1gYW
b49ZiBfa1NKp5cJtLfz/M2l0A5fuUqCEQv9HvLxKAD0Iwv7rr3BKF8QpGhF4QkzIp8ScTrm5iosX
MOB0lesnIhz5fHW7ZMogX/sAYKlrQfMSECvpKUB6iak0ZPEiE0UIrQb/tdmZDX3vRy1WqbtFs+In
uoT2WSzDDTjw1k7if6Mn13VZf+Fw0frLWcAdjHsYbFXkiYrnQIATGEJMJcDta+yrlYuhLffaqME1
jmjkjeJVoGgz0cFXhlxRCUGyuWUqipent5ViFTvveIHURPNycgtPn2psXWwtO9rbTFEv6cC9U4Cy
3CZVh1dPex8y3hPLxlOLpqJBbNX4je+W/pYTSrjCQjEqx/LkDKAlvW1cenbQS4ifCJWiPKX8Ra+Z
vUtWWT7XIwT9LUvHrGMq2l6s3uArCgjwvVV2Z8yUri8ej2HebUWv/iAs552AuBgEXN1yLWqIJIS9
we/lJBWDKeI0ZPlAiiRxdwoz7YTxS72n23zdkC0FIV5Uzm+PFxqz/x0Uo0howH8bokEMC+55tQqb
xFmsdiz8dwlDpd7e4lMtYd1trD/MjBDP5bne9yLvJ+SGdQUJ30SLW/crXAIUtwH0y3JZxok8e6ck
So+2iJ3QKBO4am0MWERygZwyye2cYIBZknawOb2P2KUNaq4CP/IeQ79Jt2VIp6fEqfGRaI5qP1ox
TTKPLy15N5oyLBiQaBMHNCrodU3o2W5A4ovlsnluNBzmsHej7yhubn5utHQa993Y41zyHCqcEQnN
maVZu0MVzI7p+QuIW1ezZVR8k3Q1wJus0NZBjk9/WdypKur5G/7KH46JAdfF0KvCR4PVnNeB6sDl
L1vU2f5+B3vjVwEW92OSfoDtJ2NhHaKei1cxPVJD+shRhTfowH3rxvQvJO6fM98HBOPjrU0xqh5o
lyjaFCTCX+FGmt43nbj6lHyAhQTDEObcC9/n6wIryFneKQyb/DLoUVY2FpEmtUcyPF5GneJ9DTPM
YjzCF4B432Q9r5gFZPUyl1IkfV/iVBl93DsdUGpvyV6+VqUfuqga41avQ4JsD759DvuseVeNnYf6
/LR0wcho3USZQh+RELXAU2QQwhhkh43JjtTlRJDopgghbkxrd9n6k60Ek5lsJ8/BWRF8yMDjAYhb
jwmDi9oaOfILyv0ASEFvmcZfdX4z3ZYr5txTD5Vpny/QMby/IoBg/LcagaCmiZedmQGUcZtNE57/
QW0Mm1yQfePH3ztfTFK1K26ZiDyeCbJwIWcWKsCrEdq6tGjisHjK7EduwMVsC11flJrjishZiML/
pY7CdZI9hiQ7P0zx/Byq0hwhJgskRngL2bkY/C+ndOswQhlGekycV4OXUZjSDSlLGylsYa+NI2O9
3tR9DKfUSr1+U3Y7Nh/2nn63aZVMNYlfn/VCDpvm8PU1af1WFBrIs1RWMYd+kZPQSoWA3HVfykx+
XK6+hH8tKkEPSTOJP2NQwsOYLnuQXQ24PcUq6VVkTkAjQ1LjlXzMVlli0MRZ2GUTSIcNDLN0qFs6
W9nasSP3J+tiAVj8YOn+SQ6CZWFTOJxMh4/IcOiOw2FGRopwJlBQkVQfVUCY+xjCceFaaJBb2oMv
Uo+QYuI9AhXdKf8qJMEQ3cV6CLIp0KnF5ivZcHQYdPzOy9z/jBR/WWr19R8q2RlKr3T+c6QE9qmm
7mCYi60YpA2LhxImTvwquOqY9ktFv1zW/MUasDsZTjkB0pktZfXc/M1DJUFLeWV3pbhmIGIPjoOZ
+gMWbZhghs0V1c0IpAG6W1+yO9nNI7ztIjPyc59+Dgi6H38Jnoxq/ZbcxZnpjvCM0NiCaRlxWHhn
HgMG4ZJJ7GHNPYI8+rwFzXEVDcFVdhKXaWGdfURofhuTp8tv7cqKqp+P8go8COLs8XBJPzx8mBn+
2GJpVP87BWtqLp3mIHs+RfxxMSigb0W12IIOjrDPi70Jtg93MfubHgHo1cxE1Z/0xicLLCXUfVTW
mEbim4F1FfovMLan+TXTQmXWGkUWVmD76mm76xrhc/Trg11oCXWJKgWrCBSQYXvGp3xCNbcxw0Hu
1xsW0+T4MBsm6tyvmI+6/itCxFvN6Uqif6pA8Uq57uDFRpjfL5QGgW3+r9vgVEUD/7z1irCbfAkP
nIWJKF7sMxZnZmhPgQgglw79WSDK+sIwNEOkLPqfZdH7Yhp8d/sGhjdM86I8Tysf2MgAzMfsToAr
PoJjVOoh7mCPtfbyWODf1x2vPydw3DVABIAb6sLOLs+tvKGaais8peYyybvYsfqc+qc09iFPSSpZ
Wg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 7;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 2;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "kintexu";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00";
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "00";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 2;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00";
  attribute c_depth of i_synth : label is 7;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "kintexu";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "shift_ram_adc_iddr,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 2;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 7;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "kintexu";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of SSET : signal is "xilinx.com:signal:data:1.0 sset_intf DATA";
  attribute x_interface_parameter of SSET : signal is "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
