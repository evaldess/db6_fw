// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed May 27 20:03:05 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ shift_ram_adc_iddr_sim_netlist.v
// Design      : shift_ram_adc_iddr
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "shift_ram_adc_iddr,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "7" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Fh2dfpMKdK/ed0AQs1X3CGm2LZxUODYXp34m776ilDEyY7QGUChyobkJRhPPajyUZ/5+uwEe+IxT
1SHMqUKbOIXbjYyc/pcnNOPBHco9Xp5GbguXrD/lcwhqpbiF6SmIKipohHDG5TlT8coXeUp4zsvX
UKM5u+b6/LX14SftdQNvkTNG1zk+K722THXYkv+i6Pt6RLZA3pMZtAeoGSjSsKQy0NBNKUsnk52M
qDtoNHHYM8LRK0lzwCLjqOZqy6a/z/Hf4Ut4YZOU+4xHHL/rchHGK5GNPSnTkL3welwoyju5oVBR
HqfjssWf/mZbRYQojNU6/SIivktiCfvS/wyZkA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QTn1pDwv3HU8BrIbbIGPtaXh72h3o32mK7rhPe+fNCxbgjkg+YQfEB7PZxBa9mck6gdFFqS3KOrc
s4RK4TVkF8nOR8xfywXYMHUVbB1P0Jmw4gvNOfb3HvrArtFEisawuukTQ0h0tBGBeWn80uQz1JZb
Q3EE1z3nasl6cYvRFle3OeumiJM0nC89Imt3WE8lmt1+N9TlhHk+AgvsyhqDxM7Mrn5c7JipNdhh
gzpDTr0jVoPbj5DTMe2sk4yhJN4UEW3mcFkijj1AOGMPYmJdOLda0GJF/901LcIYOSQwece0m6ET
9epS9o6AZEKbn1D/Ig9WRnaosSwxRlUgU+eX7A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7232)
`pragma protect data_block
f8rHgTu98h/itnM9KOeRM/B7nYrR+vz8xvusaVaginzuN0QjoTe7njRxMfy1QsFt30iyTf5NpZ80
EhIw/P/8T0Fi3vdltJN0zdI+b7DvldLG0iKjlWsMjdJ888jGWWXhQMrBsnlyVgw3UfpecrCq+LNM
KP3RxcXBaL8tUQ6zhjBw4DCLqfx+TSotJs7nVdTcaz5OmqnplMtXDLnPGD1Mkv6VksIH2pUrm06r
jcikKGPnuNVSmAT9Adojgiqd9PD6S6+HQnNBtreiQP6ozEQnbKZHox7d7/1tg6GU69BTqiWgrWDk
9oM69sfJlOX/JzmhLUEcRsUoqkjEp+4tMnFrubuLTmFJxcLwrm6HzxTglNxL6BR1us07+XCKYVaN
uQ40YoKf2vov0TEGfv7ceUrPUljeL+UL+ITOShKaya4L9WPrZBMePNFAvx5oQ/FFYVWi/8dijHqF
Q9ustifcX4FbML2uQ2JaCsMEfUccNVW6m1d9YHYnOlIy5jonzMVhE1D5iMYfVwkWfVYHwKNcDINS
MkSQ4kDOAjgbc2K691FfmjtasX0734QS0gfbKmsUsxawdVmVymYRxkGVeVT18jaPOl8u5prxyB58
4KQiNsTe8zg3hln3LyM1FzktnLu8rbHQgOdvVEZzmBtDZ3fe2wMuUrX+USO/eJEspAcIqvh9Ez54
6L7ltOjsUdcz8m8EMNqAQnY3g5aeXXvYy6UgLscBkaDEp2SR7/d3fE4iIF/8kclEiJqByyv3ph2H
R9g77LMG2hJ63vsoymH+hIAI1tYi7uFsoRfNmEl8fMdW/QgFdyucX+lGk1h2UbdRYdbNtx+QWS8e
ivkWmiqJmeqm6Xao3w4otqNGinzpAadOjx1ZWs8dV5ITkMEoK6mlewa9kYAec/ltWw9jGrGvgMRt
UeDw1n8W9ULiQXfCw4ox95ZJK1lQTaX9irVjHxiXtacH3PSBDC9kNYYtfPkh4jIgRrGdYFwVEGxv
vJvTRHFUgIIw0aFr8Lud9z5pc4ifL+6/YtfnBig5bjWHuJgQBpDOXYCRLe6XjTCP2gTLybqupawA
RAMkjciI1qKW1SMYfdA9Lhcmk63y9kfGIlbzyQTPz4o5fRuxbW9g7FzEGCW7I7U/BnmGkPZuYkkG
LwQvQX7ZNvdHLaJ2tAw+WUYquO4ilnj20nqWStTSBioNtjaSdf5HKFzjHwUJ5iP3P7qg6J2q4VRC
7lT5PiYQYngklxULxmvWyGQxXp9S1MazZIH2taqYTX3lZGLFJ+XRn/t24GnqVPCN2hSQf1uGIOYJ
N3HtkCWVWosn/vH9ir8kfzCJDla7URz8CgH3BpnVK/8ozBqTeXj3wjVWXUc0PBL1Bx7AeRFAKorl
QZXz73xZ1y18dG+Ww9tecL0Wkprb2qLg9VsfOxLO3btZxuM0nI00t7SvHEoaRhEKsjQtvVhYOu4k
1inxEAxkVXUtsSGiTH5I3R9EAMM5odk0lyOTq++JTc6SB5KCISFihYpDpM8BlU4ctkMpCOYmbUu+
+xIPCaBkwO7o1ulbdaSKlJYy1gdAb1htmagSDk3PxOfc02CFOPvJKJ4zyTXyL/oWyqOBXxXU0JNi
KcxBxGVPufsAyutpwvkhYrsH50XyGH8Qz5fWVBDhkyKM8w7swi+Rd10YdbdcnmwGwi1klyFy+/yY
fMYJxNjyvyNSOAeS9w2EjHIsg/HsVY4ZwU7xxbi8emBFcR3bTdfDD0QC25zJ+AF42VfxeB9E6wTh
jAc1B0EVvj5VO4Lax8X4R7WNVXg6BrcR+izy00e6MfUg81f93OyNCNWQ42m1jzpXbrXY4lRIxSks
tljIgtZQQ/LVnvOuoNZwxAAerJWIHfnH+54FZLhwBfXSzE5KqHhtIbX8pO4xOpOcboVSlgIwh73e
5UUVOUJIB5LvUfgXvhPADvuGvR3utreaheBs2besHpu2r9XxHHm5XqKf4MuY0a062UzIK7wCi5op
cu9dvpsGpGVZjo500FZJpEuYK+kLcMjvLQHxwqkfud2NYdCtH1CR7Y8maZLnH2jlQluu9nxjnAHO
E2X9F0RD/KbfQYWAdFjlYiMfQT/7NJ+QbF0yYj6EodUJCO18jc1Wbe5ThJr0JNXgCg6yIH6nVBBv
RdCh8Cr2eXXNR8Bf04XFRW5+MYEODujpRPBpwg/jfFewxKRlzuONVTrneqV+l5CSiZxONDfqXN9C
/C6pJF/n8XCkRf19Kylyr+XBWhO5mfkkOJF7y4t4VxgJCK5TepAS6ff8aazduLiu8v0PiQamDkKb
t5v+ai/YuEimk0nykgkz/gYZVBKDPzm8IIOCa84u5ziBX3xUemsvtstERuKTJo32/ucpHcdF29CM
Rkq3lRJ+1EYZvSyGiW1iavFLi6QrZPC6nKkclErPbFFtCWrL0aGesuc7P5oQhvR1fIWyzUe7Ts//
l9AXUp47aMNqeorUS0kMehgt16eGwjnwr8jIPN9ivF+qtG0q8SwnoCCvs7+zc9T0ySdhHCE/vilG
Q3OAwAe7P+Xe6RDRaviOsmOZGhS+kZmiS5l/NRJaw3EsyPyzKZ1S5ROzXlcwRw1unZbiXwoLGXLS
23lo6nfwdBmb+Jx/eRUZip3oxNdL8KrYM66lF24F4tcD5qfg7LCS0F3rEo52oNbbGapSLHYuvqwm
aDYrqFqTJySQ6gbZHnEDwMIo2K/r1tbSwsQiazTj8uXqyHzgEw8Z4LM8Zo7CEZbjrr8NnDfE1qAp
HqG9xTFhRtTsBaspVUlxZ+cuX+A8by1gG/akJ6T940DxoJgOx64+pWhoj0o87jk+bcvvrwJ2Aj0H
LQYYC5/fzl+4jdrMgf0qvK3d3hgqQmhOsw7V/VWTk5WButypUy/L5W/y7l+WGO4u/VKJn1/aEXKN
7umliVFXYJYYRseZxnVAAvxxHuQW0HfbZJb/cLwWF5ESvpzy4yvmjHXJPieegiLdrJOtabQusov2
3522E2weqlLNH6RWhWsyiA2Fsbn3vuwk1FOB12K4+e3G0fBoYe75vSpdwvjr4G83k1VrYTheV3zv
TZjH8x1p12dCY1wQBPhPi6JfGdvSn3TykOBb7cy/dcYLDz9XFVPWx2dmVbSLrj48qC8/DC85jLBQ
Tce4Jjcn+c/KjiawoSiXPluHNit/CqpQ+zWlr75yDq9+7dZJ+wv3/0QpfqR8wf2zP2+5oBklIaA3
morhCHTjLyiPYpa9QZBfnO5L1WnPgjgPND6LNUqXc7FiYxHTOplKue48KCCrHsko7MyIcu2fefMe
/oJF4mY6vxQlglLsSIz71D/68U8u67MXat9FhJNhZ3DA0lId/PeXZYgA7pKJrfN7wzPo+fNf7ifx
/ZpgtxIFQLM3F6PTzqF6ciN/T+6R5tCHUaX/F9y6HxWTr2GHhGALo5RZ/gHCimMtna+9BhaVASiI
cFoIyv53l2m7mx85UTYXw7yXlng92g+06o6bRTt8pAu9zeWDzcPXIXXqlPW1y7Zjf85h+XJ3um9J
7svafp3XPMBjlMcLV06Ef5oBzKvOyToWQgowt9TU1zG78KeXEdu8wf3C2O9stSfWqXEjw9WRGe02
xZy98hl0H25yQIq2AVTX7dPq2xjDgT4nv7jA1ih9o8g/ceTOF7DVDxRE8eH0gQG5WcZMp6ovY5fg
ENgZO+qDSHddPkoWLOgOuD2sGBuhJqMPCItNupWVqIGRziiNpghnq3MbJruPwKcQXlo4FiFri7pC
4pz3d5TzxQyGZSnLAB6nLfOoaxCbDVu1kBXkesraxVUJHM6cDKlSMucZWrLTsRL03p3laAOwThrU
xOvYM9NosCZevRDBQ/hN1RgH3XhudZ6S/DsgrY8o5qMCGWUsB+YVd3litfpEfIGYC+cGmALV0CgN
6av6fDulGvp9h5AdyQbiaE4aH6kgptOHnvn9Kint98DUoo+PyPG4gBrWTJgfFI3SEnRKMF6lbjdR
6OpBXnrKCZxVKVf4UgO0kp9XW4LrPwCvX5uy2NiJbpsCQ8TWPvXKeJxlCyP2LuDXd4pqsL+fq9Rf
27tkvpaFa6uwdi0naGOMuKCeHbULnlwxCJBAL0HfJ4hbNUeKmLJO9wT3IhASjEx+9MCvVbRpwALl
dOey2xEj3OmZ8Ie2q57gxH33UZWntzu69cMFBqXDt90Ia2cqB2DRdsUQ10R+8rEnRDNEj9XT4ZG3
b5Y95wUnKzAeyZJ8H2ArrtB/UGZ8bFgKnb6AoIF2QKEuVb4xb1cW2axs3FUHncddMoTsjYopP/95
IYWQ6KpED0GCRY8a3+L7+Vvhp0Zx7+7Aswd5BldJ4GHbbg092Py8aZhguSzZgZRzZwGHDl19ryyf
TP1jSgCjU9fvB5NCCDPSebq7pkWhXS1zvh0HWWQOEtlcQ2iwB6pL/w1EJ/iWjYTGE8ky3apfx2IB
z4UmjoFUZyyE/XdELrImdQCuaJbhkjuB2rA3/R4ex38y7zb9m37QeYMu3kHgwWRm8KdRm1HxIAEu
UbDLf2yli8WlBI/l2rYfANBIQ3kAsVdHvJU+MpaB95YvNTZbkLjmG0+/rwCZcpqBgUX22RehLRxt
yJy3BlUrcHq4A7AQf0CfaMoaFZZbUQfrd94z0tIy6hGgxdbYTIjTmE/0deegJCoS6GV5PCXFBJDr
ofylx3HUDqqXNh28BaTX4B/HdjEU3V/celCA4hLUcBYGYvA5ytLznYzXAqu9fOlP4J0ZTUeYPRCJ
OpjKB6LHLDwtFsaJ2Ye03sMy8ibFSkiLQEGb2cF9t0uHOT9ZEz7qBzUeh/2eIATd2Xsst0r5wG5l
bTHgkvx+QLtmAHHdTD1LWldSjQ6fv42aMzoSJcPAOrKFOydeC6mtCpk9da1PSSxahiTesbXd6OGQ
A8vCV8TAu9bTMnN9ujkipIeoy+FpmjVOHr/DEy04XbswjpYxbS1JDDSKzEWJbE8zajLeX+2vr4nF
AXcECpvn/phhuRqqqEb/3JspnfXhL87mKDwzfhNXsM6x8oFCRX5cGXcgAKIp+ql5CSt+pbF1bOQC
RPsfQZkpD6RpSGBOExFHphdZ+bkogLu1t9c8c0BqHzLYeE77eDDvHMutVof73iEg37Y50oIEBDCw
7gc9sQxCUus0gbDLmSzZ3diGwyK4K+m1ibqceIpvJtL/DDilKcw8roxPrvOE+vCpRRH6yLLImOt3
Wlh4aLLyS8/Yq+2Xc0tmIUzesgLUjPPlAStL8A7tv+l/hOCV5vVZXihY+ohbg7aaMX9GzKLoPrvm
np2VhrBydntWi6gMVheNuTwebgnFF+wLNBKS614fVKuvfSb8lUpCr9Ptp/LNaiO3heyva32WBCnJ
D3Je/Tm3FKvVM5HwrFFR+2306avJQW6TIi2zIxG4BsAbhzTc5YhItKb49dIJrSjEuE6SzFElJQIi
JkQ6m7gfkHo+75+cbprye/rt9+WlpwXwI3bd1INTOHnRMgQPRDfmZHSMdM2HHlF7BCqRpH4eGJyP
ElhbooAuI2dTC6X1rYnHLHCIMiXFDDi7iwLmTp525FdXT7bWUmWc1pgwtyNHolC58CHU/wRx0law
+YYcTz/j3iMpVdBSn4IuJIqz/upPf+LTtj8oBYRbKkSgOwQZ+c+rpSrpvumJO5V3pXiMtqKZlIeq
DFEg3KzMxaBwwNxFv1lL31kxIAbCOABhUrjfjiBqEWGH45H04Ky34rubreHGJwYjL0SQ1mdOLqD7
nLW/BtgjiepzUeS9nogSH4NQ4AQc9/N38BvgLf1F4VlHjp3HfMk08MawmQ8CymSk/W+XsexfnfFF
wwXOjGgZkzcOHTWJT5IfZ6lr9yLC4D4vPtguf1BcbVI8TDD56GdWlTurkzOFsf+s1GwKlBd2c3TE
3MBcodO4NGLWGL/+2r3qWJjy/57kGr1xKjRmXtnfpUgiYENhLJ7zZXEHB0aPyX44KiXjliIYCenX
gHwBx4hxMf3/xZEopmvFKLrP9yDYtsEHk7qxe4AyU/sjW+FOxBlZxOoEyslev64YiZ++5CS6uYRe
4fpg2JwNbNncT9SW6rBDDsDlCZZxcm4RouovO/kOsH9Cc3G2tSn6Nh6BE9FJgAIKHu3i1ebDm9xY
3HQ5EESs0XeEnLAuDDaqxo3HmwK9NLUV5zRpMXhD543845T/jMGdyDXs41uv/+MNOx42XhO9pLpT
le8oySEujDI4V6rAGkqBwSrjW0eERVNLytPh7BeKUlVCyeOfIfZp02UX1EcryHBXgg5MN7FEKwlM
h+INHYVwZNUmBEnmltOoQDqcAqu9qWSel+PoCEbClf+TPdEBQ7HXQqIoA3sDaJAvHMl0Z+HrmjTG
48Zm8mwRNh0duTXEgZKuT57166//1xeRMSE8r6JFDvH55OpEooam0FbyDyAZzunaAG0egon7GRdE
guzfg63ZtgiMCnWLrvRegHyOJZhwIYwEmczWam8lNSxg9iWGwVDgVoG1tEdSldiqvF9jzndGzuaz
SCh3mpfSS7W35AQUAxfG6HGG2IvwfVSjps/xl43irULOJ0yZV+Ba9n5gWfaU+69moEQnVal912Ci
Rf1FJLYwPLZHat5HtRhTQnEp8RQVeKO7eAbqxdPoSqCiieapSqnsEr3wSEEFT1LwLIb+aLt+d+eq
BNT0KXnjsDjcWDeXgW82OfX2IcrC0hzqoP/Mc9rwnDQ9sfrpGHtnjVv4tufAdpICZkbSITDmKZMM
QvQWq+8bPNp11tHn2b2TK643neMolKJRXj1YQYK4bLjbTZyhlkrsTmygtQ0jy5mfPUzwkhEtnhDD
mZCBXwkQfwbB8epXcD0bzJEETfbM7oB3w1ntVhCYjFFWEiP7cE0GEvpg1Th7VfUOF2RLnfveAED4
Ww7yLovZCULu49yFS+iGlm8/UZ+6HkPsyGqh4OkLAm7nSqCOcWnQA2WCJ25Ah/zAmVpbCWf4LX92
p7JRSFW40B9AXYKLResRbaBdChdDD5RnnKXHJFZ0rB6W5tjIqh8Be8Npx04C9flWLZznsRa5xeNP
DkceBNUnriqkPij9bE9EYvlRdVBWuEcKSaY0KchnFPFticWKssiOYg6oGLEXnOQtqQdZg5jCyrf+
ahU9hZTGgDCUjOU+j0BTRMH4w8ekAWm3tUrD79TC9px55jZrnn+egShOopQznFoJ+NZTCPhUGbF2
QTcxVkvTSMUK2OhqzLFp1qNgBXWEjZvatCjq4da7+oAC0IfUYXPR/O1x5NeeTvw2u1yQJl/j1KY0
14zRDuKnARUk51EDk5HhQRCcCtVDvwOp0UHBXjxU5lJ9Vlcn++2oOr+Ek+NnSWNPTYsaZSq5Nsou
YnJ+tMJyLZSaqfsQ2aN0sE+7HHY7Y0iMut6YY/Lz0KhddS6Nj2srl/+k8Yb8RlitYfoKQk2cUE6i
bjRBJ28vK/GQTb33zHgKnC3QcQLaU/Z/dcehVg0GN9jxFI5kC3JZFmbQ23MikeI3Xu9mTpLBzmxs
aNBXPOw0gabgYZkM95TQaEYXpPhAjtl7QZrtDJw4E06VuMjGgyOEW99KighYjnBmuim5lsayAJos
JwnMlhkthMuPi8Ysi76ky71uU/SDUCVpJ7fATd8IYsTmzATXm8BNT6FiWOn08/SF6C13VHrax10N
e9vH/4LmR634zez7GjL3JgbuR1ylqdOJMOjBVU/g1Z82yLbFCe8o7Uh0h6LChF1hHYAYuPXN/eZf
VHSF96HkzWX0JhtXUJf/OytZZll6XwSn4Sae+U9+PdGv4FMJpa9+BUojnN/1gisNAknIWFd/svY2
J2mG5yXJQX8RwhfKDJlfGoeIEnlP+svmTpUmt0E2V60aPbKjTSrSw98MVMwRuucyxJ3gOGkeWRjz
O5sv29mz6F2CVLqhVksGq+TFayngftsz7G9/zUuXBrIiA1snKzRdgLs/aLb4v87mRUOVFjDaBGG8
JsyJJG4Y8pGMONDfLwJp22OWeO+Q4mC2NVBrf5F+XIYtfutV582fZIjN0TF/SHhzHCOsr1EZW+py
LEgl26KC57LAi6FQEubzHp+ARmTIVtykH6SXNBlR+1jyv1gOG+wZKzeEnRPr1LmPVcw0wA/2rQW4
K+SQXxdMTj+eIPvgWU89INeVbZWQ2n6f6LYkht1VyuqecCHkpzI8Loqcfp2dlVldjwdqywa/V31w
bOd6cR6gHmZ3kVXOk3jFwARYPmmvxrSFeWk7zyvTLCPvPvj5/yjbU1GiFjddXRaFtdyXWS2LTmP9
MFTnpSg0zfGNA0LcYnEpds3SFicFcy3t2oi41agN5BGcVH81tMc3pEzhnZV25A9uURp1LjE7aVjV
lPy0swITJtEeGLXhTOY8t8/bkSLyu6WKmRdnfiueFNmGUHcOtRftCM+HAhEIrVCSyfxs31qIdK9X
oxsh3joG2FePvArIINj6ge8VkmeSldVrPcz2tsT+QwwH96+vonahT7DsfG6TWaQgvZ2x/1OtKaZw
7nFk9RMUirg66ut5MMVaCyJ1Bdx0FqNw1JtJKh1LMNcfD6mSfiebXVqu366+dTB1DYx0cPEpvRSa
hKQdJhsyyjQ58NffznUG2GjmZfOt4U6ZMmnA+TSCC6yVHG0jJ4M4gkliFo9UeYq9kbpbS+MGSDm5
5FZBDvTgIT7NWfKKydARLkfvk6bUu0J1eAlAGgHsnpm+CGdO8RYE438e6/TRx1vrAIj+oBwDIVyH
96p+cwzU5bUXNbbmIz4hVvyFtEVk4OB07r4QuywNOlccPc/l5RckKOlCRMXuw+AQ9fzXrPoMJXJR
yHPxcw2cFF00Efm5zpXKe31fv1xdkRsDMqzUh0lXjAnUWLpvF7iOCcdpsva90D8GW5bC4HjDwgNJ
u/65arjIrqH5QLK4xAy3muO1fEw1glrSGZzP8pkKLag0zJVUFeFP4xcIIuQlXfP75NIWmhvYVTWl
fYZxeuApZagKQXAMswKvOfAhetUcDI0rLH4m6XVGaAEMBQFisiSCD8ykfeXshu7iftTuSC3GhngD
ZxoPI+AtS87CgA49s9Yy5YDIIfak0lgWhpNAKcBo7oVf/YpjO6zwo+YxJFPihHmkWBw1gLcarerl
asCFCU5RKshZQuiEyszh2G8fPxjH1B6MIUlxI9KLESh5bLhASI26ZuqRuDhy2VjgtRkeC5dT3FAM
Wb9JjbEt3DCrX1GyMs4lLnVCKQtyViVOoDJWq1OqHQERK0PG+4JEqCigiYVsFx6H7HBYDFu/3ZCH
JkG1LGkg/CuZs7EV7mALubpl3gnCnZrCIGVLDQ3AVtF/Lf6cNPYZkBe26LuWymA0Hs74tgD90LSR
dzZhKOof1G1dVtBYbkplbUFqJbteYKaeDsU2gFAG/xCh0xSloxbKC7CIPiMGx/9UdOFEmt35JNXY
jquBAnqsIwtGBw7p0HnHZQ0WIg4ljIspjB4hLxaAW8H2UxvUyMmtD3nIeXcUUL7+BSEgLUf2oFPw
fXOQceF4bOjkBa9Bk72m+7DXKonsJbtjhjogh3Q9Ilw+P+7SuU/kdTfv3mx8fvRSxUgXwYH4Ex+J
Qjgbfmhw27kRxNVDs755GtNVDH4EmrbGgwki1j7e1Z4c7EV5zIHo7sHT/HObRAKuFR4/42ilAKin
vVRasVGDgTKzXTPBaKpjYKog+Dq+ya09/Yk94zsxuNHt4P0EWHCmy26Rc1xQIeexfEk=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
