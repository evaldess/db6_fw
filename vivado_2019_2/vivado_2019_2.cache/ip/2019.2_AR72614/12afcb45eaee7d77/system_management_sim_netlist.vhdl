-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Tue Jun  2 17:22:33 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_management_sim_netlist.vhdl
-- Design      : system_management
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_system_management_sysmon is
  port (
    daddr_in : in STD_LOGIC_VECTOR ( 7 downto 0 );
    den_in : in STD_LOGIC;
    di_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dwe_in : in STD_LOGIC;
    do_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    drdy_out : out STD_LOGIC;
    dclk_in : in STD_LOGIC;
    reset_in : in STD_LOGIC;
    jtagbusy_out : out STD_LOGIC;
    jtaglocked_out : out STD_LOGIC;
    jtagmodified_out : out STD_LOGIC;
    vauxp0 : in STD_LOGIC;
    vauxn0 : in STD_LOGIC;
    vauxp1 : in STD_LOGIC;
    vauxn1 : in STD_LOGIC;
    vauxp2 : in STD_LOGIC;
    vauxn2 : in STD_LOGIC;
    vauxp3 : in STD_LOGIC;
    vauxn3 : in STD_LOGIC;
    vauxp4 : in STD_LOGIC;
    vauxn4 : in STD_LOGIC;
    vauxp5 : in STD_LOGIC;
    vauxn5 : in STD_LOGIC;
    vauxp6 : in STD_LOGIC;
    vauxn6 : in STD_LOGIC;
    vauxp7 : in STD_LOGIC;
    vauxn7 : in STD_LOGIC;
    vauxp8 : in STD_LOGIC;
    vauxn8 : in STD_LOGIC;
    vauxp9 : in STD_LOGIC;
    vauxn9 : in STD_LOGIC;
    vauxp10 : in STD_LOGIC;
    vauxn10 : in STD_LOGIC;
    vauxp11 : in STD_LOGIC;
    vauxn11 : in STD_LOGIC;
    vauxp12 : in STD_LOGIC;
    vauxn12 : in STD_LOGIC;
    vauxp13 : in STD_LOGIC;
    vauxn13 : in STD_LOGIC;
    vauxp14 : in STD_LOGIC;
    vauxn14 : in STD_LOGIC;
    vauxp15 : in STD_LOGIC;
    vauxn15 : in STD_LOGIC;
    vp : in STD_LOGIC;
    vn : in STD_LOGIC;
    busy_out : out STD_LOGIC;
    channel_out : out STD_LOGIC_VECTOR ( 5 downto 0 );
    eoc_out : out STD_LOGIC;
    eos_out : out STD_LOGIC;
    ot_out : out STD_LOGIC;
    user_supply3_alarm_out : out STD_LOGIC;
    user_supply2_alarm_out : out STD_LOGIC;
    user_supply1_alarm_out : out STD_LOGIC;
    user_supply0_alarm_out : out STD_LOGIC;
    vccaux_alarm_out : out STD_LOGIC;
    vccint_alarm_out : out STD_LOGIC;
    user_temp_alarm_out : out STD_LOGIC;
    vbram_alarm_out : out STD_LOGIC;
    muxaddr_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    alarm_out : out STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_system_management_sysmon;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_system_management_sysmon is
  signal O : STD_LOGIC;
  signal VAUXN : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ibuf_an_vauxp10_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp11_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp12_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp13_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp14_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp15_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp1_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp2_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp3_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp4_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp5_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp6_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp7_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp8_n_0 : STD_LOGIC;
  signal ibuf_an_vauxp9_n_0 : STD_LOGIC;
  signal NLW_inst_sysmon_I2C_SCLK_TS_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_sysmon_I2C_SDA_TS_UNCONNECTED : STD_LOGIC;
  signal NLW_inst_sysmon_ALM_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 4 );
  attribute box_type : string;
  attribute box_type of ibuf_an_vauxn0 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn1 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn10 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn11 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn12 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn13 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn14 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn15 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn2 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn3 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn4 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn5 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn6 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn7 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn8 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxn9 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp0 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp1 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp10 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp11 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp12 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp13 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp14 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp15 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp2 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp3 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp4 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp5 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp6 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp7 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp8 : label is "PRIMITIVE";
  attribute box_type of ibuf_an_vauxp9 : label is "PRIMITIVE";
  attribute box_type of inst_sysmon : label is "PRIMITIVE";
begin
ibuf_an_vauxn0: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn0,
      O => VAUXN(0)
    );
ibuf_an_vauxn1: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn1,
      O => VAUXN(1)
    );
ibuf_an_vauxn10: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn10,
      O => VAUXN(10)
    );
ibuf_an_vauxn11: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn11,
      O => VAUXN(11)
    );
ibuf_an_vauxn12: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn12,
      O => VAUXN(12)
    );
ibuf_an_vauxn13: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn13,
      O => VAUXN(13)
    );
ibuf_an_vauxn14: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn14,
      O => VAUXN(14)
    );
ibuf_an_vauxn15: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn15,
      O => VAUXN(15)
    );
ibuf_an_vauxn2: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn2,
      O => VAUXN(2)
    );
ibuf_an_vauxn3: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn3,
      O => VAUXN(3)
    );
ibuf_an_vauxn4: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn4,
      O => VAUXN(4)
    );
ibuf_an_vauxn5: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn5,
      O => VAUXN(5)
    );
ibuf_an_vauxn6: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn6,
      O => VAUXN(6)
    );
ibuf_an_vauxn7: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn7,
      O => VAUXN(7)
    );
ibuf_an_vauxn8: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn8,
      O => VAUXN(8)
    );
ibuf_an_vauxn9: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxn9,
      O => VAUXN(9)
    );
ibuf_an_vauxp0: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp0,
      O => O
    );
ibuf_an_vauxp1: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp1,
      O => ibuf_an_vauxp1_n_0
    );
ibuf_an_vauxp10: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp10,
      O => ibuf_an_vauxp10_n_0
    );
ibuf_an_vauxp11: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp11,
      O => ibuf_an_vauxp11_n_0
    );
ibuf_an_vauxp12: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp12,
      O => ibuf_an_vauxp12_n_0
    );
ibuf_an_vauxp13: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp13,
      O => ibuf_an_vauxp13_n_0
    );
ibuf_an_vauxp14: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp14,
      O => ibuf_an_vauxp14_n_0
    );
ibuf_an_vauxp15: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp15,
      O => ibuf_an_vauxp15_n_0
    );
ibuf_an_vauxp2: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp2,
      O => ibuf_an_vauxp2_n_0
    );
ibuf_an_vauxp3: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp3,
      O => ibuf_an_vauxp3_n_0
    );
ibuf_an_vauxp4: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp4,
      O => ibuf_an_vauxp4_n_0
    );
ibuf_an_vauxp5: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp5,
      O => ibuf_an_vauxp5_n_0
    );
ibuf_an_vauxp6: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp6,
      O => ibuf_an_vauxp6_n_0
    );
ibuf_an_vauxp7: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp7,
      O => ibuf_an_vauxp7_n_0
    );
ibuf_an_vauxp8: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp8,
      O => ibuf_an_vauxp8_n_0
    );
ibuf_an_vauxp9: unisim.vcomponents.IBUF_ANALOG
     port map (
      I => vauxp9,
      O => ibuf_an_vauxp9_n_0
    );
inst_sysmon: unisim.vcomponents.SYSMONE1
    generic map(
      INIT_40 => X"3000",
      INIT_41 => X"2ED0",
      INIT_42 => X"0800",
      INIT_43 => X"2000",
      INIT_44 => X"0000",
      INIT_45 => X"DEDC",
      INIT_46 => X"000F",
      INIT_47 => X"000F",
      INIT_48 => X"7F01",
      INIT_49 => X"FFFF",
      INIT_4A => X"4F00",
      INIT_4B => X"FFFF",
      INIT_4C => X"0000",
      INIT_4D => X"0000",
      INIT_4E => X"0000",
      INIT_4F => X"0000",
      INIT_50 => X"B723",
      INIT_51 => X"4E81",
      INIT_52 => X"A147",
      INIT_53 => X"CB93",
      INIT_54 => X"AA5F",
      INIT_55 => X"4963",
      INIT_56 => X"9555",
      INIT_57 => X"AF7B",
      INIT_58 => X"4E81",
      INIT_59 => X"4963",
      INIT_5A => X"4963",
      INIT_5B => X"9A74",
      INIT_5C => X"4963",
      INIT_5D => X"451E",
      INIT_5E => X"451E",
      INIT_5F => X"91EB",
      INIT_60 => X"9A74",
      INIT_61 => X"4DA7",
      INIT_62 => X"9A74",
      INIT_63 => X"4D39",
      INIT_64 => X"0000",
      INIT_65 => X"0000",
      INIT_66 => X"0000",
      INIT_67 => X"0000",
      INIT_68 => X"98BF",
      INIT_69 => X"4BF2",
      INIT_6A => X"98BF",
      INIT_6B => X"4C5E",
      INIT_6C => X"0000",
      INIT_6D => X"0000",
      INIT_6E => X"0000",
      INIT_6F => X"0000",
      INIT_70 => X"0000",
      INIT_71 => X"0000",
      INIT_72 => X"0000",
      INIT_73 => X"0000",
      INIT_74 => X"0000",
      INIT_75 => X"0000",
      INIT_76 => X"0000",
      INIT_77 => X"0000",
      INIT_78 => X"0000",
      INIT_79 => X"0000",
      INIT_7A => X"0000",
      INIT_7B => X"0000",
      INIT_7C => X"0000",
      INIT_7D => X"0000",
      INIT_7E => X"0000",
      INIT_7F => X"0000",
      IS_CONVSTCLK_INVERTED => '0',
      IS_DCLK_INVERTED => '0',
      SIM_MONITOR_FILE => "design.dat",
      SYSMON_VUSER0_BANK => 44,
      SYSMON_VUSER0_MONITOR => "VCCO",
      SYSMON_VUSER1_BANK => 44,
      SYSMON_VUSER1_MONITOR => "VCCINT",
      SYSMON_VUSER2_BANK => 44,
      SYSMON_VUSER2_MONITOR => "VCCAUX",
      SYSMON_VUSER3_BANK => 65,
      SYSMON_VUSER3_MONITOR => "VCCO_BOT"
    )
        port map (
      ALM(15 downto 12) => NLW_inst_sysmon_ALM_UNCONNECTED(15 downto 12),
      ALM(11) => user_supply3_alarm_out,
      ALM(10) => user_supply2_alarm_out,
      ALM(9) => user_supply1_alarm_out,
      ALM(8) => user_supply0_alarm_out,
      ALM(7) => alarm_out,
      ALM(6 downto 4) => NLW_inst_sysmon_ALM_UNCONNECTED(6 downto 4),
      ALM(3) => vbram_alarm_out,
      ALM(2) => vccaux_alarm_out,
      ALM(1) => vccint_alarm_out,
      ALM(0) => user_temp_alarm_out,
      BUSY => busy_out,
      CHANNEL(5 downto 0) => channel_out(5 downto 0),
      CONVST => '0',
      CONVSTCLK => '0',
      DADDR(7 downto 0) => daddr_in(7 downto 0),
      DCLK => dclk_in,
      DEN => den_in,
      DI(15 downto 0) => di_in(15 downto 0),
      DO(15 downto 0) => do_out(15 downto 0),
      DRDY => drdy_out,
      DWE => dwe_in,
      EOC => eoc_out,
      EOS => eos_out,
      I2C_SCLK => '0',
      I2C_SCLK_TS => NLW_inst_sysmon_I2C_SCLK_TS_UNCONNECTED,
      I2C_SDA => '0',
      I2C_SDA_TS => NLW_inst_sysmon_I2C_SDA_TS_UNCONNECTED,
      JTAGBUSY => jtagbusy_out,
      JTAGLOCKED => jtaglocked_out,
      JTAGMODIFIED => jtagmodified_out,
      MUXADDR(4 downto 0) => muxaddr_out(4 downto 0),
      OT => ot_out,
      RESET => reset_in,
      VAUXN(15 downto 0) => VAUXN(15 downto 0),
      VAUXP(15) => ibuf_an_vauxp15_n_0,
      VAUXP(14) => ibuf_an_vauxp14_n_0,
      VAUXP(13) => ibuf_an_vauxp13_n_0,
      VAUXP(12) => ibuf_an_vauxp12_n_0,
      VAUXP(11) => ibuf_an_vauxp11_n_0,
      VAUXP(10) => ibuf_an_vauxp10_n_0,
      VAUXP(9) => ibuf_an_vauxp9_n_0,
      VAUXP(8) => ibuf_an_vauxp8_n_0,
      VAUXP(7) => ibuf_an_vauxp7_n_0,
      VAUXP(6) => ibuf_an_vauxp6_n_0,
      VAUXP(5) => ibuf_an_vauxp5_n_0,
      VAUXP(4) => ibuf_an_vauxp4_n_0,
      VAUXP(3) => ibuf_an_vauxp3_n_0,
      VAUXP(2) => ibuf_an_vauxp2_n_0,
      VAUXP(1) => ibuf_an_vauxp1_n_0,
      VAUXP(0) => O,
      VN => vn,
      VP => vp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    daddr_in : in STD_LOGIC_VECTOR ( 7 downto 0 );
    den_in : in STD_LOGIC;
    di_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dwe_in : in STD_LOGIC;
    do_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    drdy_out : out STD_LOGIC;
    dclk_in : in STD_LOGIC;
    reset_in : in STD_LOGIC;
    jtagbusy_out : out STD_LOGIC;
    jtaglocked_out : out STD_LOGIC;
    jtagmodified_out : out STD_LOGIC;
    vauxp0 : in STD_LOGIC;
    vauxn0 : in STD_LOGIC;
    vauxp1 : in STD_LOGIC;
    vauxn1 : in STD_LOGIC;
    vauxp2 : in STD_LOGIC;
    vauxn2 : in STD_LOGIC;
    vauxp3 : in STD_LOGIC;
    vauxn3 : in STD_LOGIC;
    vauxp4 : in STD_LOGIC;
    vauxn4 : in STD_LOGIC;
    vauxp5 : in STD_LOGIC;
    vauxn5 : in STD_LOGIC;
    vauxp6 : in STD_LOGIC;
    vauxn6 : in STD_LOGIC;
    vauxp7 : in STD_LOGIC;
    vauxn7 : in STD_LOGIC;
    vauxp8 : in STD_LOGIC;
    vauxn8 : in STD_LOGIC;
    vauxp9 : in STD_LOGIC;
    vauxn9 : in STD_LOGIC;
    vauxp10 : in STD_LOGIC;
    vauxn10 : in STD_LOGIC;
    vauxp11 : in STD_LOGIC;
    vauxn11 : in STD_LOGIC;
    vauxp12 : in STD_LOGIC;
    vauxn12 : in STD_LOGIC;
    vauxp13 : in STD_LOGIC;
    vauxn13 : in STD_LOGIC;
    vauxp14 : in STD_LOGIC;
    vauxn14 : in STD_LOGIC;
    vauxp15 : in STD_LOGIC;
    vauxn15 : in STD_LOGIC;
    vp : in STD_LOGIC;
    vn : in STD_LOGIC;
    busy_out : out STD_LOGIC;
    channel_out : out STD_LOGIC_VECTOR ( 5 downto 0 );
    eoc_out : out STD_LOGIC;
    eos_out : out STD_LOGIC;
    ot_out : out STD_LOGIC;
    user_supply3_alarm_out : out STD_LOGIC;
    user_supply2_alarm_out : out STD_LOGIC;
    user_supply1_alarm_out : out STD_LOGIC;
    user_supply0_alarm_out : out STD_LOGIC;
    vccaux_alarm_out : out STD_LOGIC;
    vccint_alarm_out : out STD_LOGIC;
    user_temp_alarm_out : out STD_LOGIC;
    vbram_alarm_out : out STD_LOGIC;
    muxaddr_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    alarm_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_system_management_sysmon
     port map (
      alarm_out => alarm_out,
      busy_out => busy_out,
      channel_out(5 downto 0) => channel_out(5 downto 0),
      daddr_in(7 downto 0) => daddr_in(7 downto 0),
      dclk_in => dclk_in,
      den_in => den_in,
      di_in(15 downto 0) => di_in(15 downto 0),
      do_out(15 downto 0) => do_out(15 downto 0),
      drdy_out => drdy_out,
      dwe_in => dwe_in,
      eoc_out => eoc_out,
      eos_out => eos_out,
      jtagbusy_out => jtagbusy_out,
      jtaglocked_out => jtaglocked_out,
      jtagmodified_out => jtagmodified_out,
      muxaddr_out(4 downto 0) => muxaddr_out(4 downto 0),
      ot_out => ot_out,
      reset_in => reset_in,
      user_supply0_alarm_out => user_supply0_alarm_out,
      user_supply1_alarm_out => user_supply1_alarm_out,
      user_supply2_alarm_out => user_supply2_alarm_out,
      user_supply3_alarm_out => user_supply3_alarm_out,
      user_temp_alarm_out => user_temp_alarm_out,
      vauxn0 => vauxn0,
      vauxn1 => vauxn1,
      vauxn10 => vauxn10,
      vauxn11 => vauxn11,
      vauxn12 => vauxn12,
      vauxn13 => vauxn13,
      vauxn14 => vauxn14,
      vauxn15 => vauxn15,
      vauxn2 => vauxn2,
      vauxn3 => vauxn3,
      vauxn4 => vauxn4,
      vauxn5 => vauxn5,
      vauxn6 => vauxn6,
      vauxn7 => vauxn7,
      vauxn8 => vauxn8,
      vauxn9 => vauxn9,
      vauxp0 => vauxp0,
      vauxp1 => vauxp1,
      vauxp10 => vauxp10,
      vauxp11 => vauxp11,
      vauxp12 => vauxp12,
      vauxp13 => vauxp13,
      vauxp14 => vauxp14,
      vauxp15 => vauxp15,
      vauxp2 => vauxp2,
      vauxp3 => vauxp3,
      vauxp4 => vauxp4,
      vauxp5 => vauxp5,
      vauxp6 => vauxp6,
      vauxp7 => vauxp7,
      vauxp8 => vauxp8,
      vauxp9 => vauxp9,
      vbram_alarm_out => vbram_alarm_out,
      vccaux_alarm_out => vccaux_alarm_out,
      vccint_alarm_out => vccint_alarm_out,
      vn => vn,
      vp => vp
    );
end STRUCTURE;
