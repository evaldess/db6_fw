-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Mon Jun  1 19:36:57 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sr_ram_adc_iddr_sim_netlist.vhdl
-- Design      : sr_ram_adc_iddr
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
E1oeUhMXtdrK75+O+uJoRSbZQiqhBSHvGzDd2qrsS008Gx9fpro2HuQOLwHZwiCv9lkcTlc7A4Rs
mNLkZ4POCbzgc4qRvg9czLRFS71Ds5vbd2+nXK9QTCLkQ2wIUgP+QhCd5Xn14xoIjlR/H0u0CSoQ
Y+b6B2tYNcK+vd56nIxLCRSlMQ9FyVlpl8wglEH14TKYyNTD7FJIJlQxhf0xQkz4QJ9ZWsVEMx31
tVIDsxZXeEnmiWMB2JcQ84jQ4VwDfhp7C/t8DfAwJvvNhF5x//DjxBTZkFbTSDqyByGIMXNjDXLq
8BEIONVt5lwvGuo1yBKXxWLZTlzHQ1NEOiiu3w==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OXZuGCvAQZh5J7mvFUII05XA53mrwHpo7vUjOqq9+RM7XewlZwlc5VLfmf0mAJHXHD0kP2soxCkM
uBv0uVEKYhO8NvEsq35/7fZI+n1cxhOFBzxrwI+uZ7ygeHpwHUwcGKsJmO7YQMHjJb4ajQDp+pQn
iocKJ/eFJ0QoHwnD1d2V1S0HArSLhk39prL3ZHsrukekynsEWmoNf2JNtHW8FpnJuDqyRoyR6s/X
JukC0FZyaCAg0XTmKKdV8li6MBEMlgKd+afHxD5EVo9CnPSPG0nGLPnSAPXiaSI27NtXcL0Iw5LW
yP1+mQ85yMih+xpBM3su7K5KHJ6/qoYvnfbRhA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17344)
`protect data_block
5f8WwBTB4Y0T6SbtEZ1996niPzjcYQqb1CjpkkqAeo92xvMNjDmFA7uEH7dS9C1CAivnTarWgCWw
pN8F82O8h/YeNShkoMcfhwh0wckaLBK4+lGl8Y1k7o+s7PGOYdMEXXX/uIP4B+LJwV+K2bcWjDSw
/KEG2MRO2GRgrGY+haHxuK9bOPwey5cIP/wmb8AP/cSqHWDLN+45zVVCN6eYykA9I4wsdOy+rhme
rsciOHCVum2TMJ6BCcwUOk4QOtg/o5bihThOM+pvXQfzMKtV01n3ASunLlnQQy0Lban+xUyqhRxd
OGm5lg+yFcys2BV/Tt5WX1EsS7x+LykVJeEgw5BdyK5M3zfUQNh2eKIgmckFKGubEABaGTELJLMR
GEQLeb3duuENhAz31e0X+zsOUvS6ZFecu2nOXIhDk4vsggORjVAPED5ZgmvuXOM0YhRe3IsO6iSr
R8TsUbtB81CZ41jFVf69hgwHlswD9vczjTRlgsvEn0LzaepPhq1OpMw4SFde2Q9BtCRNkWkaAVSQ
GJsrv5EEbZ+uN8s4rB5B6oMjCzJoeQiW5x0w0lJYfNsNNtMuz3fCznY3YSm+rYDyo8nTR6vUaYTh
w1fK9vFFLYhl6WYUNNCfqtPY2M7olHaQ34XWMITKumsI8AtzzOX3gfa/UK8hQgprIF+XDsw4vFIx
0W1IRB6POK4SGGW+4MSKhyD0ZeWizysjmUsC8aG5IHXhNUkZ7isUYeb4J4rGCymZzfCaTHInPZkg
n7qFM2jBPVpuZUzIN/dg9GoIIY5wfncf9a63gU+NpSZUxU6S1BFfWrKKETn/Ka6fuO5PoXxdb0C+
Ktm9Wse6qNUnDyrcKL3HppysNwvl+Q0GXky5LZz2pdHj8yo3sHuXkEMK9jihQiiqb/Lgu/tARzBS
7tdajByXbWls6f4oU5SXMVpUykastvDm6OPb1TYr7acwUpxJ+uef6MQFwzyb20l5/uX/inlXwa+F
6P5H0gCgnE6F+6T+h3ldlTSS45EHobcIqtu6F+uN9SOwS6peC19zda21mydw2fceCOcwhFi5tB2m
dhCqpW1fsKea8v/36yB8ojIX/TxVdRQjYt1GXnsGTjou0nauMdAsOB9vymXmjYGlK205ouWxxQQ0
UUYega7p+523FmUzwXX2NdLoC3FroL0k5fEj4wSbfh3O4khfZguq0za5i+l/KY6zCTe8WAjv/3YK
jNIVoTaBXj4B/lxeZB0J2K1xl2KWy6da16KwBr52UFYy+KfN5LK1GJlRyhhftM1hiWl7/r8k0zsN
QZmRaOb/S9Y1yHzbKDlXZaxKClmmHpa6N8yPtS+BGPXd55R5j4pdV2CFq9qAQuWyNdFeeOnNJae3
oQkbS9X8oG85h6m50yHYZzRBwlWZymEWb53Nd18wSxVZx9K+YqJEFiEhf+L+MniHOxCsugKK7Jea
WtmcUprRTwtkYcY0W76k0pvEf5r/FH7yeIPuYyj81zqyXtJqbOZC5xSu3eD3046ITVBXOIb6/PLj
iSXpqkKE6Ar3sVMKZrUsZT7mfmCuAEyNc9/OkqW5ZGuH3ZOkqSn8RnAeTC5HxRBr5Ie8H8Lanusz
Os5uOP+Q9MOAxtlHeNlnKjU51s76nkKZMEFloMs6hJKngF5V75Bha4O50uEOpj83J31q7UTZ6+uI
DbDrGGaBYFDF2/VLCbiJJyrj4SD2jRMpAcNiIyYGBS91D+0CIfqa9tQLkw6mFWg59D1B9KaRBbjo
pzq6G6VSP6EGp8Plg3P48yVEPtt0skLzvk0sLvC83pLeNtbB7s96vT45Tx5ZogsB9voslpdxDQNd
1/EFy8JRAgEQtj5rzpBPhmgqsdu+XpRQaaD8bwLOoe/wPprWn+AKoQ5cOA3AgPmepIVU+PIl99t6
rWtskRFidZR6691STR9fRUtNpkwFyrzIuD0zfb+mTSUc2JyN7z2J5mGwguNfFojqLng5LQ/kr7hE
90tJt1dTmVInUSk0bSY34fh0+IVsrVhvi8aQt7NJFIfrUii/D66toAcdI1C/Wu25WIaQ7ygVk5HT
bJn38WC8sYVoll8sCzLrxZBrHhhfwo64D2KVrkce+PHQZRYE2Z7vtSvcLFYQ67O2y5xEJW+MUNHw
OCdQa1REs3P6V8x/IVTXhH86+4lUGVaBqEtMhjHQ1TBkWxGKRwsP9zBbiwKGmEGSqvPG1/jWvSmf
U6TPWjJ0TcFfY4A4kkYt5b2Bt1RBoVf57l9ef76j1blzY2m5iJ5pge9X56PlBbWfByjEuJs866yo
8c7suwsc3+i/wStZnojOhRjSVL+YQ0mNuVZ6p77u3GKtSXP13uAPre2yq0yphkKlRROjwT4B73/5
ilX1AHvda6pM+Ycz0L94EBTEaqXfeYobQR0i+X+5qHRNYkg/j0zbL9NtdDAbQNv4/XXAU9OyXU55
d35TrFPsEGWGFKquyVyDZKqnWy3SrejEUt23xlJr9ZumUFvDOE1s0yw3f523k8qU5V1mEYRyBuF4
yiER1ZPPCKHmnrboZ6ldKA4IKrtVDdcgpPkh+fPitHMbBAqZ4BsUvrjTIMIpGIHN171wlGQ4ASrI
l8jde4yWEj6yp/BgCj54fKXEwjTgdJl7uqGfeOi1Bu+3I7YqF4cxKg/D6z9TQWP78yeTxB7ESXXZ
osYV0Ftx4Y+7j89N5d1fkHtJdFmNlSVUxYRpFllJBTxv+whg3whjPO+h1K85/D4chXFHCGol9i7n
fXZH7qZVF2Wx/yAkU1TktGLFy5JHBsVprwFWTNO/D8SWf99rWr21wn23ucLHK+AeTpsPG6Kl4aI4
edRWAabzRKxIPryk54pO/nrjKlm6j6PJnXqk6HbDqluTRXYqkPDcjSMAfvMpCESo+I6lK95xAdhu
JNE8oK5TcbAn0UC0H5ntSFYV18Da3oSQhq6LYq/cXlR2oP7CmkSlP6hwvMzYVZoM2XeCPKQSVcLL
RuN7Oih+7I5CazzN6glF4ExnGd/dIONfkDT2dnH3wEQBbsPvOoOAGzF2FqULCZtu5mW57DaIZnqU
96ZQwnIW3K3S0neYd6CfpJPi9CH8g8xKHOZTKmjNyS1yZBP0FZBl5HbezMvh3DZIVizQuMtvj+rL
tSbKDaUyHV9rNYCIMBiugyrI/hfUQBOl5h3j2hFZmQXE7rUo0JDJntzkQe8aqXzRlxgoIHyD7NR5
OM3ewwRRrnNaqu+k3Yf/cyiKGRbT4GFOuM7ddtbkRZfTOcjBsqiTKuRH9RTSqTzR+2nCv3g94/Am
XM6BkizRJacX+4kQ+7VrrUZrukAAIs3RQT9/pUU5HVmYOpVUmnF2A30w8ZedFEhHZy81CK5bqUA9
4MSVmiF4jyQ06IzjIoHkl4WTG9QcYhnecU0XsSLt03Q9GM4IvSZOlK22mWuTjaPDpkHfxLVoYnk/
bPV3Hp3AtD5Jh/fpkbNmi4sRC9OoFsQKnZ1UT5FeVaRHrwIw2++AOOKG3rrXMclx418CFZrthyqi
ihE1H/KkIYs2V42EnN8O8vPF3aOcaO6G5mXQF0QbX9WZww4FCn7dR69LKdSxcFylmi17Rqg70rfr
dOCzTJr4ivO6ycTv/eU/uyFb8GTT5xoS9KCo6PTiiEXc7dS8vM6TqGPVWyPsbSLAmO0M91QVqZ3J
sZKsH2thjF6y56DWgSW09PSrASddP0zlffRzlUw3QBEE3P0oIcGQVu7frMfcJSGfhwS2D6NUyBys
+4tz3tKFpx1rqAsNj2D8Igv0nwZEkqIWNSOME4xSRekJNkXeg0EgsTKwCTaZqPj/NjKgsbDyL31P
yW8WBvMe2Yy2YeHKym/rp2TDey6O/zUTDJO/q1Sx9BDoXC/8OlVqU1CYqD2uQI/tN3SSIHDTfj3v
aXGeTVcAd8Ei13u4i5UEOCbdoauiDOU09/iPCcxCjMU0BcWP3V1sVxmXKbdtgYvS1z+2z5l7H0Be
yNIMuKYuYk2f4b4J4YnZeZH2BXvnwf+vbtEkboaXgNhtpNbx94081lz1jm0QlROt8ZGFMvFMS3gg
QgOEBnaEUHrgFX2Rs1OxMV77syY/8lD2jrOyxz6KkjzhXxEDz9HRMx1dscaaMawqfr/BXyTdXWqf
5djEwzOOfkvDV2xPA1r3OwSh+bt7GZM5eZxlM+taywr1wpN2sFNpJmkqCgltVsRBAglAdM/G6If7
tj2Ta8P9NeeLFRewC/oqAc8n6Zjpo345h7iYYHUVcpxJdITIgh6tF7Q/Z/cI85g/GFSNAgru6aiI
cDe5fwNDNFcgnic1qY87wLvioJgBYVxqs+dP4w5STB8roHczIdk0vCfw25Tm//rBNYLCgge9MU2c
Ntt5qCdYpzdd5AqmAYRbi4p21j6RPcl4N1aTieyX7Z6J35faABHoscxh78mae2byqTXqT/trKHtR
vBp/1g2Zg366LtDJ/+fEZywxxRJpnjPJKZYBzalYZLLBRoApRVbTYx5EvQbtNkCaK1H7eaflA53N
DSxoBde4nVa7g1yyuoQ/QhRtDoIkzAg4yC8Nim25ed4XRaMXPlYt99aHfBbj8w2+Ai1Hr+So0ioN
O8pe25idVYoIQESqRwHipwsBGzHlu157YuuhdMmhMJNtSPphpMW/T5O0H+Cccusl1DiBRJxECF9E
XbJBD4KfnpKoSMJe4aWzNJfkjxlDYbP0ufKwT7LHSolRTTt6Ul3ptQ6vveoXCheq/B/b7oKnGurH
mDGqNua5A6DpbNsDseKOvJ5jaaEnuUDuHfpjpVpF5gxVHfv/EW5WbkdwOvEwB2M5ePCfoVVTq1Dj
c4VawVLVGSw0rG8PggivrsBzELWLu7G5rxAa1XUUp00nVnHhYydugFoR+VKeOxBNnWNh3YwA9+xj
VbgyLYpMFEkY7AeIwNyxzojfk1PaEMnvWqggj41bhhR3FA4b9DyL4+EVJAIoAc2rJD+Lu3CXlU8+
A6at4jNDwQmUXJGYTegpgUT0CAmAD2FgxRzLQ/5DYaxOyj4tqI1/B4cytWFfckxtH/V1W0Ym6Wgg
IMo1PUOr6rtmlxZ3HNtLJoiyEPiEfXVZ829KgX1Spak9mQUGT6BzbrKOcAQk0RecYgSP+gC7Fouh
M5oCcX2wNUrQfa78Gmu4W5tVvLSRcIIy8kFDk0nPmClyN1K3ALZ4hBenI5bngNia/bcaNJkEiqZr
vPP6/9uVpUXPvMfobitEeOGMZhJDjeBjbKiQKyHBKXZS0Ut+JSQxKLgZzzHq4E7X9mNrmIjKCwRO
TNnFpaTOs/Pssv0/AJjMjLQJMb+9j2OFcU5+ePm1y1HcLT16YJ/3B4Juk9Sg2vNUHlUh754ppIuw
7WYjPg0RY4h+GihaXI6yDyEJik34yIjGaxLmFc5S9yYcPkrGbZJYyFPwOjg/zm2vqodw2BSSCLQx
kiJzb1uucTaRtTUUaEnoILt61NteYUCtaziuUynVNBM5Jeug/YtwrhrYoTTHmh7oMESqPio01HVI
7a6F8v3zCxvnWhSUdTp/nh09zNivRyk8+f4m/T1maScj0H6JzJ1SgMjFhHbKYSJsFLVKY3MySAEq
++TksyE+Nqw700qDMFOS1lnTPyNpsA6pXnXUAeU1J4kp95gVwF3DSk0f/2OmVL/9ZzgefZyxwGBc
vuzoI8RU0F6fV6TOYkOringJQgUM0/Cnd5XmUOYQ8WCHFCVTveXP77IbaGlc7ePoMcOo2b3c17u5
ar67259G8ERpT+WNwHAlyXXv8U75t9GlNjM/QiGQIWd1oXhsDBffGwRkcRxhx4aauBNV7VUyxS0L
S5Tgr9e/g7msML/Re4flh9g00X4rZq4ezs6ZCuSLMIiIc7o/gHDVfopPHTaEooNTZzRMce6fgeXu
Y5e/zzvo/YLbIeRhH1K4bq9eF0MEnqpmWSt5iTp84T/jt0Ej0qsmvKsjUzUfyoBmypg6fWiC0gta
o/RRvnqOctoUiwIA1XjCxosHycJ3ewrGst6YLIhsrJKCLWVwbcSY83lm3ww6B9/XB73YZOoaTLrt
FtXVlvzrX08Qm6RGII497jtzx+nWZ0DGNpbCe5Yn9sGY7hbqEBC2E3XD7a89dDPs04DFdT3xeOzu
aPe6D1juVmiYX4xMd0o022UkwksDKVWKGM+nlf4MEIN9P9AYAiNpWVls+OoP0qkCV9oFOrXG/K5V
8y/HCkMTDtrSG2H+4SCvD9NyoyT7KhddjvjbmfRuRao228e7zjpIiqCmCunudWc1RufZJqGh29S6
BcMZSTF0SLOneyTgDnKCvAk01OA/4J2ElJzBCetVvrV/zFmjpXih5+QyeneLkghvNrR76fcKhcwt
yKFss3RbD2QEAx+n5gsNa0Kw2yuAXTwzxYMTsCzkYoV26gt3QCOdY2n97BczNrHMU/0DPjc3vDDJ
tpx2r4icNmrDMW5KgBsfR2tAq9OaemkWAqXPzXzsR1YoTmNoa2cX55g1pI5A6FII1nJZyD5mxKAg
6kUNYqEtZnZPITsdMpF4G0INU8nOjJEwD7Ak67gNYN1H38hk+fj8XrlU2qBgobJ8zbe2UlqcEAx2
VqRp7loZvcTG+QlUiO4aOFZEk9/GbK03mp8M/dlncocF5YwSIo1q/KMDkwlUendc9uIpByJkmk71
/p+qo1DGTbc3ZFRhDee4nTk/2AXzBBeGiSgrueqEyIdIpKPoMK94H3X3mmZMuCA4m4sMqfvXbBch
Ujx/FG9NR3c8hc0KnHFRzvjNb44GdlTiY32aemkOwba0OjH7EbDTSxPJ7/1Z3sKD0KWDRDrHpT6O
SZT11wKOAnd7MLc8OQFVUbKAclnbifXGvYl8SZsFIrYAD4Q3q8lO+x189bxZqbiUsDgTrWHYlMiA
0dkOhyFfZAisfYOcs9+rdMaMhrVwcoHy8pcFtY6f7ypzfNw8k3D0kN7esK9qUglEKOh1elwLfKCK
FiIC0T1VaNV/1+RE+XayHPmykeYYiqwnBhqn4idAePXd+gxp1Z8YHMrhzbrEIaowh5zTbniQyDFa
pkTEHgnYnOOjVZO9TVd27/3kEbkAe7PhKYYouVa4GYj/6btDmceqEA5Mlsgo7P+Dbo3+5cUHJt5y
kVkcC+beFHn0FOfrtVGgCBl2V39e030ZbAx3RRWXaE0T2x5aQwHnRcyu5eDa4wAvh/AEuKDvDpRz
rIynfrP+TKx+yPYIyl6RJGTwN7itzwVEzNtb/sF1GkoBbEPdpq9EQKY7YTc9J1aHjKMwPGed74zr
xYX8BPxrE6Oe0aTQNHsYxJ7y4vjX5AaeTk242gtzH9xHoPGXED4MO9t3z5P3tXJco/r1nuao81QL
BHMyh5poJkw0dyQ+0OaJ5FmUfm6jXDpUarA/kLS43G4j7pY3+1HqCMlP7g6FBWKpGK8RDdlYYwtB
XWcs4L79Exp3p4B/OS0nMtU8V9XAqI4Eg52+39xJJIsvmWxl9beABF1HpOewmJ8OefJObj8K8tGM
WQbKlYvgzHkpiHqs4PQqekXVEw4ymIjwvvh5peX9Ix646IPB+srfqyw8c2XJgil4MF6NNKt5NJQJ
HHez25mDkY3XyUNkNZI2jx4D377xhLKv4bjBMuzfnR06WpF4vrriDi207GJ7BEndxqp2p5czKkN5
Jk0j1Xu5sqh63s3UGlccpUNDfGxkIW7jO7ySkqpmJ2pfxYLbvUDhJGj0tqxo5b7B0Cdy0JvE647y
QHUVsa2RPHoa0ytRy4ePCR4SnSylwCRJMw1TopqL5oFZdlf9riUYYJ++hN5zpJb+e5HQuh6D5wiX
itWmMnWNitEOBavy7uWZ93PXqddEvJzsG96+Llns6PbRhGo6z/oNjRbsc4r2R6mTsyBkLi2t/H6V
wJGZIR2z7IDdawfPbYzoOv2Du4DFxPoDKv2NG1YERCGn7PrBHNRgZFUIWgq/TUT9jm6h/cFWind5
xBFmaNIvoAzvFidv1qfPkGu1sSUXu7zBcuNo1P43DHDstU66o0CB1rsusPrkHD+I1LO3NOS7PMsn
+bWrdgLCH1gA4eefQGCUuFaVuUaSnVu3Vg0cz0Z6aGDl1v85FPxmn81+XO5kL7DUo83AEaCeeP7c
+A1YA8LBlDLob+D/0xoUrvDiaQXNjAtPZmELv/VtMpxzPBvanpGzWXfv0SNXYg7JEz7rmjpK9hWg
ULGfdLh/owm4klsOextgR0NbO0aTra5vqLGrsWeGfnQwsrwFb+IbTP70tOHx8QDvrMWhO9AiJjVa
Q6y+l+yi9rQIGZxhkscr0YDdP1640gunX0GjTZfMw/UrTcRBbAQKOw4RPxP3L2Nj0KC+EKim5ejY
yssyFA2gCS/KjrgQ0bdroNmS8KP2vGHg+GhzcDx5iBSZXaya37KzCTN3UyJnv2pmDcXbMhxiux1a
4GF5DycbI0ZzkY2IeNY4QqptaMtwNQiSCD5w5jWeCg0QKa2AFFcSsPSN48rfD7WozbzcQrK3dQgo
430rarsPjaXaB7CmzcDpcU3c8yVrw6CQbbW2P4uIJADu7LKADnu5bOsZDPBcIVuWEAV7q+9I8d8X
RbMx5J3Z7CE9+odoP9PBacQKtb+7q83cAt8sw2dmypoQxU0Fnj4UfFrHxEdQbr1tQX0JoqiKbDU0
IlR7s2A5FVmNUrlv+VU3RJsYPwPmYjuLu4iqxq5iHCE/zWDdddKvcxBfDd62grrWstwe4khhHFMO
Q7U8MbvAcRgAnIL59HFvPAX0dhwtg1FRmwQGKRx4J2o33bmHFKejCN4nPnNaMg+jUBY2oHHSk8Yn
77GNKlKckyMWaHunUMqGyRwjvLy4Qvsk219vKx9h1ukLQagS//hZDh6ZVpgzrEqTg9B6wVgHyfFP
CHe7kbul+1ZSJc/sLN0UERZ/Z2emnpcW0wdKRi8EOHeXCVirexSCQXeONwFzqekIZV1NE7ZAQTjn
55bFZ2KPQ+V7U2pQmJHKS6A4+s6seyN0Fyv3g4tn+kTEkFLJ7GtIUjJJfqeitVpUoqrCokyZrlTu
ZCvDmgYjnFoz2dUsNzniQAoVVOuE3rb//WNv5rn7YNILerJHBUXZ0J8D8qtygTbhIAm83KKYsaR4
nxzaCarnuIqkw4KpiX8UPY7WN41MH1rrsSTAmHYKweOQeT65GZB9CeZw18KJxXj+ZGq7TbUsTnBV
Ok+vsWYoNEnZuZuYADZB7E3PGbb2kUPuyMKVmMvz7GKXDj52G7remYGvozHq0bweB8r49Gh9kDPk
9P36++EZ44wufhVcABLd+tW3qOOFkcHG9aGqzVOaWc79gC2FTii0UErgulrphU6b79IrnshuYYLX
FyaawitTHErdJQwv0BXokxiNb8aYiINSCE9mFC6AX5vzon90YUdGhjkoBOFyXZ3lOXMEOvJoRmdX
WfcSEy5nCYIoNEWAWw9f0Uk2/tDclIbPQ+01Ov2EnG6ebggRoQ8qX8y5y8jUSTkIhnlBkyB699ch
v/0mVScZ1K+mr2y9LjMRH588V36i7UEvt7qZFy6Le2B/SjKra9ZOwTAwlONidvjfnWdcwd6G16+g
h2RX/HWhh/xBILLyv6xd6zngwJNF6gDTNtHel5a35zGGPqomAE8j07FP4jEZp94SItaODYEnGsOS
pQb/cbGjjQAWXe9OBFg33aLO7BTOpCE4AZjRvGhY9LFgCpSnSPIF6zcbF4J4LfZ/PWeaDwRyslay
HUpzjT7txVUDsNTdlewt5fg2XcprBguGgy7MCQEX+4F9woARG5BMbyho9CET/CIMN5D8fcvDL/12
fxcCD8J6TbddJ4JYU2vKWtJ8zgV86BMxpttpAOjHiVX2zsFLS54fhxxMJBB1/5r54SFL+Dy9K/l0
9Mavkk9ikN6jkqmVa5oiVHF4p47jRcmtB5Tp9m7luHarA3ndXch2V0wBO+DcNYheb2sboxV1HmNz
XnqtqmvHXKRSZxaT6NkX8bNhF7cHOh4auv78dvdgSmecVUb9P3r69Gm87HBOKR5V+29GutsGg3Rd
NQwXrzTOCZIFU/n1kXqfET0/dE6epNGiyzzvDgytbS+0i+YQsgaZX75jv8QgS5x+XEDLtC/dqWlW
nZ6083AKCW8+QF93XxBb8fEirpCcEj0Ghm8Wy+coRlb7iba4t0qtsY8kytjm3yaOY5a2fCGMHicn
DnK+ZyvfgANPDfpLusHxh3uomMb8QmIciNvRzvQuo8YzWl6OFx4p7TP8TRSojdSIr0bTozefJU40
XMHurNdquxDJsho99vIFJ7jgaMIzOPH23UZknt/K8WkyC+P0FhDxIk9OPHB/PdBnXX3a/e/PGHh/
sVlDL8tUpfAQ5YF0PQ5hrJ/2r5XnlzfQ4fTVU/oK6X56hk1HPJ1ueyAPc6juFTfGEGAZDmlaK9wy
X83jbkY8nQcn4Lys2kKVwzrzWnscno+Cp2ZhHt22xvg47R5Yy2MNesUXdvhcUplnnTgVwdFC9+pz
8WM05y+H04KYnYT6SlDgJo18izMKHhcqW/Om5cUAovQbHNPXS/IO1vQtBIDJ1DB8iIrKhZWqUGap
CPLydyxtivwvrwA8+aV03/Q+mFMpaZCiNJvM9m3VUOUM/3gOQmo4Gx22xZYCfPtv0kdgHLNQZHMz
XWAzwMweRimN3duQiIQTPvbvA1ldBBWEToAJfwt6puG5XcoiAj5x3m9phQ73hrPdJf/zB+Gz6Kmh
RtNtyiMWJFQZxXSV5+B6HG/02CL8o9O5GJMqLRy++D2jDZqPWiNlyN66JTg+TxHkUruJePWa8kOa
NNSWwLD+Ga3Lc84zUyGsDAvyb9BLQjvNEpbBYmJcCD7Z0FHFiV0AvkKxkP6XVr9e06+ZLtUCkkna
Hew+agPzArmygYvbLRISWUZ/pJ9j18FwM0IQZAxkXQuo0IVVfaWJSbFj26Dbq0L/SHXJ0wFCBSg5
9nInBReo4tniwVTWaT0BrL2lUH17z4wYzec9NIpsluDxGAb2P+Mm1Xu1arZa2wPscpciO84weAei
Y3/KNkgI4oPJes8ms2kPZHkhM52Aq1EHd0LWkReTdJGHPB8gadJZzAY1HRTX+nger3sILVVK1pCP
ZJV55EyBnJQagqw4//rBZldcMtmoGF1v9X5RFRB6g67vmKt1OfTbUMty/KvUcwfLlNYTsy92EjuJ
aTuA/SN47mgTcPKgrPBfeidz/7LApDBiLIxdevKYHwJtrQt1DNa+DNwEXkV8vs6Fn+6kPkOQL8gr
pC4bqa/0opP7ZCrc9W2l6uLKx8aP81W+RCVAABak8raq+utKmc3lDL48OBW7Wewe3avVnA2pN1oL
FZ/GAuXSeN13ZQvlJoC66+1UcoKEqPgLJzELJ90AS8FlCMVo02t6vFWT0KRaMEBsgO3hAH5M+87i
oQoaIqoiAVuV8OzGUd54Ma5pOwC1qZeaYmefr68AYy3nuBx9vopS1CLZ/FJkSgjSKsmnsrxZBvjv
ZOzVM7NU4ssO6PAgg4JAW8Tx6TZ4p+zg+BDgzhnuZowcZj8Bfqhc5he6iQRcJ/2esp5QbNo2BzPJ
L6RNvZ8rJHtqAw+HXFatCMx7ntP5S9M+p61sZJJp8Bqh7YHs5TBDQwVYV93UL9lDjHHU4j754N+q
hadxmIY3+ZaFnMdFDo6ZMF9O7xpdzM3z9JeYIdwpaD159VJ57JkW8oFYMWhLgLGMa1vgAidvddL+
22QLQSmFqBn/FcJmbMXyoqhAQdmMG8vr+FnaY11p48g/ZRLIDKPLryncyiXolofK7lG4oCl9HdcV
9DTxk0bhVrVRGlKZcRQyHDSeiDLlYRznD0VfM6FL8XR29JPUVebhTC3cKVnc66XNjyMnT8qd307R
45JzCBtI90kjAuQjNwlrjViXkry7JSwLTj2nwP/flvIwnItQvYPbBlEz7XrftYXl8DiyxUj/oLVe
6juvUQXGgzTiHd5vFqwTZUEsRWiKGrRM6JetSUPeF6GBto/vBRvoNR01BwF7B3X3LbFjTI3GbD1o
J3VlRsQZwZpuxitCn/UB627Fdn2iROUjP2ZJSMrZK6+iqbG9QSeCDaFFaIi0DIb+vD3RwhbVbYyc
zUAzkZPPU9iCFdKagHG5hjRLuuus+HYPcvUYnh0YeA46H1SG617H9GZcKDruSPco3MMHwlr8UkA6
NiLdSPat08CciTg7EquSB5vCLa/u4B9WUsbfLWjb6u9agEs/HliPCfZ3R0jcEZ0z9SSe3PgN8MB5
PDGlQAl9q0LnrNb/hIPjYe5mPGQLNJulK8B27encyoQAstofwp6RnDDxgJCOetRfIyrw1qYYZDNY
EVTPSvMY+hYD7kXBGlfE3QT1cd3fOlDeP0eTaIAQeWT4GESQMb8rWa2h7CxgOr2IjUhTdruL8nyF
2ybbFCBKXXgFJQTRd38vI7hPGhyYGCK7G1TS2MUjfZcM7D/jgD+eH+aehdRrFDoH4za8jEiMZU4/
ewEBZ+3bsDVxmKEj6w1ilxsntM3hQ82vtcPEO3SjEZOh1rnID6KuKVFJElL9b8Z+/rl7RngrY+1b
obQYi2Z36e7k0AKfwu4Cd8v1BWWWgaxwxhoSz2nkcPLVQ3mQ6P+XPM90mddd8pOy9k/bcUbnlhws
aLRx0xDpZKWRDYg+E90smZn39l0tnjrDToJcvW+AMyN3wGZonaEVY9tXgDR11AA12iCvPgGVmaPB
i+O6U8PYA0XjyN18v6XdJKl/EO1LXLpTnjSIYtTnk8L5VpPXpoTEONHfI5VNv0FQCCywEqRRQQe6
6PgRgFqigWZ07u9y1nAShVAgqxZ+UFQj5bM9fcJoi00d6CySP+nTPR98YUox1BmJf8utEAsaEIpX
jxN6rGfgtyYRTTE6MteB2CbEUfy6STsawrKnS8omZhbvC8Ku54xx/hN1tfaiNnhVdUfeOI4JwJ8d
polWRDEtWLoEoOZWex4SjP1iXJ0Ka6mlzDe15GWn+THyXNY/f0VezUT1X28v5/yJHBem9tn4XuZH
8pZ2pbkzEemASxctr9BRjkfjpTKS9aggbIuIm6QsXJ7wl5R+IHGlC/5+RvEx+iOs+z+noBqxqpw5
P9i73teOrTDgtkMcrStDyFBp+Is+OLhDaOcspzJTKr+qurh8GcKmdJ1BF/xQUQmXCxA/k2NJLM/J
G2tVrLMg6vT1fxTqGyuJ01btYDWZDPy45oRc/IvXpC+keg3ATbOdizCbKsaWPHnKyG6n7aqBqu/5
MiPIYIQGZCvxaQs3MKxGH2/dE06wg43sRv2mp+tpZbtEtu9usZvk8r56/gCSpd1ZBk9Ig7Uwg2TQ
MVzVyINEbTu/dQRRTktlJhHGARq90pGMmto9FxKyxzcCCeb8zPVl5xoOtHjTjtENUK+wq24A21nL
zkqbTwC1IgAcV+BzwW3Y6CvuTOxeGfcG30nOhPrmosOW/SrXmGfUO1p2nIsM5lMubIpuXpnfJWXa
ly4YevilWyYZk7FEF+HIn9nD59eazm8gbqI64Q31iEYG+4BYvDpwl9J8kJsHeBTBWNe+FtVgmwYf
laUyTI4hwqeCZ+YeiWsnxhAxM47Syp1n04q/SzHjgNb0jcjienwmVolvB613dOpaH6c3xSljHvov
E3DNgp/47YJe/uTYaC8FcBpg+xlhpdVAi9fEGWNWLHtyXLoxdrTtptRWDxp9DTkJSebCCkjhXN/Q
Ucy8YCjBzEqVqsQ2FyU73EbOgHwgR+gcwCDV0v7U7ZuzZDp+cfnQfbAkAAc9Trql3H8tOinNIrED
2S9OgN6uyMTNVxakADRYy8hOFVOQR6AAAHDaGDuQgLFT0Jx2U6YKZn+gq59G7Xh8GAyIxV43Gjdg
9CAONtg/Cszl2O++rV8lEdrkAeJf5SUCnZVb6vUylQP2+hZhcOgfSVw2DHDuHGLZ5b6mDRMw/dc1
Ykub+ExEOtGjmG/pfpZ+Q0DFLY0VaTY2cpg0bcl7JSrvLX30T2muvWGDMVTY1AbiT+KIPCzN41hP
z1XMfTNvmbaznsdkvmY+sQZlSnbFB2rgTUxjPmR1b+yWhKskovWycJrh/1CMllsDkfJOILFw/hZO
zfspLPoZJc4epuZGkgh/YLEuoC9lTiHUGzv/UYdDSmPt50wNLFDOlLti/RB6znWfAIa87gGfGB+x
JOXgklNNKYAldMq3MLaBedcvx+CUklTPP7K3iBEqyuhVKIxtPtVLbYb8XmaUf3nv01uGS/aLiprW
OwjfUMM9/YF066Tic2FjQsmKtOBWTf2X+5qSZniCRClSepxX3vtYIiKnd9V8Cazns9E96k5eBaGQ
ZRUra7hPa3twis+S7BiAQH94jYIsQT6Z3AnXHN6OhJ1EkRxbeRcSxn9YMhmvtnGZLqMDu/u+j1zU
6aXEfpGZ5oIwM0W/6RZhMvq3A8KZKy5w2M4v4tzbb7zgZDYU645LTvYT6ijsjlzN8m/qHOg5hjAI
EPkMRaP75b4QVW+ZieV4Py4S3PVZK6g8jMtZSy9qG3+mTfM/VQ34V4F4M/MI2iP2xt5F1EFVUZSj
cpJXo6cY3WaNotfAF2QTo2C9IImCbS3V4U9UNof3sPOvPuxyUrJLi73hyx0GZ5e5dGjhYqYkg1tK
+wuD5HwAcHlPdEssiocQPP4sIc7q5Mn6+aJgkNDx/yqZ08LjXZ2QlO1VQ+MXZimHWD+5EEW7FZ/d
WS0fL1Fvd4ey8MYWixTg4xaPSQdRFHNvv/QJn1aNQWLTBaiwKig5dMducnBHAtrpATxUSxYozPlF
fRVuSBUa4M3CR75hOfj/kku4nQSGfP2qPBTNptZ1+GZovS/YGcBdp7It+vUNDDS2gOy9nIvM+gDq
jxm4V2+j8R7gIUui5aNxTEVoWG1fAzRkAkoMIcab4KJEouVt/DZy3YV1nbodt9fa6nZXFdldh24s
5oKUfOLHsEuHDmIYGnHKsWnZDKIIzC3sL01kmnxA9SRmau4iJf6XB9difMOsuF7SDmHf5rpCtAJ2
qNPwi2ChzQ6fuGmoZbnDdwaGOw7r2gmThaTAwQl9+1VhDZQSpHncikuyH77AKssCDxp+FClNDF9h
3yyYxwlwklIjp4mjglXQP8TmU0vNGqJXbrkCIO4jB2k51Cm9bPp7Bxy17xngjvkg7VPqUcH2NRwU
WW6ivukm7GfPODRg6cB+n2n76M//gNkpKqOk7dRMM7SLEksfUiBkN+FsYiaJGCa8AZawez/k8xhR
t5AbaDTv2D7QDwN6TfE8auzE0DXFu4ePGyvNx/oIc1BaHwTgnhGIT3uS7zVsCj2zrqKh3vZJH0HP
KQTMS/27ilUPzSQkSXU8SjIeeJeq+P11LN+NaZfASdQ3iuA7hECslPhe9FeOPIAZobHOd+6ige0n
LlNZyIquhGQdAlWtcdOoZJ0n3V67pYWZKuuOr6k4KgX/dxS50WXmE212PEPjxNdqfkZq+UR7QDfA
Wza48x0MmymzhIhtG5wdESFSilBmRIvOAuFphz8EiFF5eg5fy5wYtpMcExqIuILDFOaYIKjG9VP+
jSqtC+QQbzqZ3opJBhkLWh7HzBLKX9tD58BS2FprLM3L0NZUSBdKoKVKsDC7qGyLsAB+94jeCpWZ
iiBY4bcOLZAEsxtSPrFOLkkRSNtimMv6toNe+xVAdBqJpTIGiZL8DxmMuatVR3uD0NF6pkcfUWjf
dcLRQzNrJLL8tTsYY/JtBXL//EqjCQqn9IwKuerWQ3YALBXI1Jg2j29/jhDqG+d6Bq74OoZmzgan
/wj9RqikJ6s23sEHXxSAxw51z+GrC/BLu2Q5fL/axkYAZBtjTpLsxxcnauewvs0b+kV2RbKN8hus
AFvbaftw2P+c/UZSsqsMxeGXHX7+IBF/u9ldVfi6KQaVTs53cAH5NGN2eLKNaowt2johQ5pV/FiI
dIGOxWC0j5RoPc9P+j2tC704rweIaF+Xz4sKScR4LGJxrwoUO8zFLUXI8aMF30d8SaU2NkZT7zWc
mBUvlry1YSwmb1AeYDic0GeJABJzSEwEp77mB+4esAFYNFkN45TQR6gOh2+GKdO1Ulk7SqqhJoZm
l7mY4EyM7B5DMG/wDf4GmtNdW9R7cHwe1lHlMZJS5+91uAhTkNMJLkvs0Midpy8iumuavJEOPupT
eEGhv4uHBb/Vb0vsoZKfFfU4kHrbw8VuHMvCns6pXLC/67YqXvB2HjFK9BstpPrFhNtXWIV+JRgA
05bxhHmsKymW72BM51R4OV4sWma5LscV4vZ91YEVMJgmjstg6/zSoWkx4UAVp0piUh5PUSZWoPgs
lXfmWUbAeCYmJu/DUmwnX+Dt4Fnim1/S3gm9HjDcA6seucEywSrGFzcuL8xSkbM7ab8A707qnzCX
j9xDi+HKuccMQizVVkuGxx6OsSW67PtrPSwZd+72L0BEB7CxV85axC1i4ouHiox3d59jaAWSVH5c
v6EnDGDVB6Hdbv9RrjMhfigG1Zg/sMVpKGgX3+Bjt9p0b0UucKrESemgHLquObIsi7baH1IbA7Bk
loH5s/G6b8+u+2cRPSM/KulsZJ6SOx4SufypKaN7r+NWCTn07BKYpDstFsXRn0K5YZFBuqVcEXTR
Oaa0yLWRe3VbE/GI2lXVvCbwnLnp+fXuwRq0pB61khUWUNX7qG8DLSxg8GFezu0Q4YodKI9IX5Ro
CILIIWjMnnizRRw98X+vEU8FksMILbdthFQvGdtd0nT289ABP5xarws6sMLFLTP7mKLhjguYT2XJ
hGP4GluVzNDu5AYMmHL1ogvwcnMNds9WUlsLQsWZqwwxDEMecexBeTuUYMwOUpAhmt9fX26CvoYN
3O4+TtfdjsovUojyT763idp1823F4FSuiW0OJ0IqQ3D0d1t4yfQ8CEjsUwCFVttIx05xxjG1rOVV
E9RZoP91M+NF2rceDSGEjV7xeHfTrSnymyODrsi4kTKaLWiKhQA4CmYFQxtR4W23r4KOUuaUIn+g
1JUmSGVssdq3aJJAvCXMawnOw7qm5xiyL0Pww36c7LGgB3FZwZPW1XAsLbB8JNlP1SE0cUBE4dEx
BEZg5s/KVCjqROH9uUofOeczvsy33vEHwD+jH0FeGXCgof66dXavjUg3zVzOXqRm9hUsOKCesFS0
lZbpqRdC/7B9akLqO/OyxLAWepTh59VeqHPENaN5xCzZgdGMMaB8Ltw7lCl2yP0Y9yesyzBzpvm3
Yvn6Iq8XwxcgJRH1od6rZg2Z9sYbxcbU7uGW7n9CfPpFivKGME9Vr13tJdlMEzsIaPAWsTL0TnNk
MafPA6oxEVMpvjFSx+xFxuRQwmym+pmqnm1lXI5W1sz8J15czYbeklzYYCLptM6aAmIisIlwx6FX
atG88SOGCWh+u70wDinvpFRGo8uGCnaTpecSnOxaQxJKgZh/LPG2TpGVAb9vK69jWHdLG7u5H6Ap
xIHjFf1ehX1v9BqJLEveWsddpe0PBTAjbBaroQbvrqBqXJ2az2zeBkuIe4/XZdAbHMNlbcE5J7CW
lwcK0Vor4Xi/zjKon1hADKmfQ0qXqHkOGwvZONzW4std3Nd+wgYVGwZHu5ucaLzkzbJKCmSVcHY7
qHonaQpMgjXoNQaHDQLZYzj6PMOxlJvPAJbYlnI3KOfChot1dv3lpy0EqnOwtj0rLu0V6+WgjbW2
PaPdd5lLGfFGTVpr0zjm3FrtNFgcshRgOAtBLkFz2SnT7Mteit6n8PuyPMBoYv1zN0Rge56hfP0u
kxfQnw0ua2l5WkoHgPZY+JycgkAu06D3m7p61ekn50Dec4qvTqLNnJ/qGvnWOHubkdC64h0P3mj/
pQg7GdfY4yGKXa1vB4RmymOXgBHepIyNlh4GVmFLEOVkGx5SM0ZRNfzXecvWEp7SE1M66RLizsLS
qml/QlOVFt2htquXWmX2kiSe6ZSE4tBWvi25yFo3iRh0tg7sar5WmICu3RN0YAzB0QvWqbfzyITa
qinhinSuomE6DxPoxbPuHkrj+MOwlpAhnqFacA/9pBM8BpyaDqYioIrznQa7jj1zr5URxpfYsgpA
OSqJdp/+x/hCoacIRJeoq378yzwcQDmHSHXkcKDCgMLySpgGIUnnmp2QIRIo8BdMgh5Yz112VOBi
JDUPBa5qjI7KSc7yXx9Cjs+7m4ZxwIfXXAlXeP2Y63WYOm2SmTntWxpNYGzLxGNtIMi1zbmJLvwJ
sTjeBxJfMfItHkUL3w/fKmSVllA0LTZ5WH4tsk1lZa0FymD37pTF0KCJ2pi10+YsdpAv8B0vQuBp
fhOsn6kexd8Z4yyhizfeNVMxZScnDrUrqljd8upvXXUlWQBX6qybqqSAS2F4gUBgA86IrcdzZj7g
9tCC6ALmElMQMqe3tpOjFCgLeAnEKRqD/uegBwu+7sB5D27Vc6QpWNhLFbosL3vL9lEYAvuwD5PZ
rU00lrPrm1CfGkD6PgJBcbblDxW7zQVBIm5FmP8gyOwtbECQO47rGH6g9cZ75kmVCeSVufTENy2H
9xCqxqpyfxM1CRK1uJKDjB0m9GH9s+u2KpcNb4fhut0ClBG7hqtpwFiIMAJlf2LjAfdcamywr/pW
80ynx4DCBWamzsRuahGvrXFz12j1HnycEhCSe0P/lOCkk0UAZXjGwXzZmRCsEE9ZM2hD0+gGwbVJ
VIVb1nqW6dL2F5yrACdXJFOM73gr+/kpPgvbJCBvOpREP0IPt2eOrIwQih5M4UyrF0M2hXVDf6fC
Mgo2M/Moh/QKcjTlTsZbwDu+NAAG1xjTDJTTubIsmGEfCd3tcP+EnDPozuRrr1dRK41rF+PblZy7
Jnd8cCbngQ8/OPcBrhIvdSIq9IRb085jd/Jzfy/h8/vmer6055ybluSX00OmyWO519Qj0vij8Fu1
+eovcX5ZOzC4gd+uYiBJf5WqzrTGw3BH+sgVTEANNCW0WLhLvnpNWO6ZnB4DOBp1JFUZdP10czlL
KS7LtAAkUyUZ5A1+xj3wKZPgYM2Ud6s513YbD9eAnn2RvnXyaf5qWP91U2EVwAuxvO8vs6nRj44M
gCPkjINy41fX8y5aepGXBrRLszEKDNBzK948Q0hlRUXwkNbh7+qv5w9zB21O/4GDrm+p/9LQ8x8H
5lby2BxZwkU8KhJ/z8bl/S1We/9pOqVOp6pI5BLh5uvLAMnsXdT9oZUKF1xyhSzEkHVCsr4sBlXJ
BwcQKpIvu40yU0J0J+8pgsHnrGtUxghj6A/KJ5CckToZPii6P+Nb43Mss9vo3UjT7sVnNEHnUBbC
n9+nQwZNnCDNjxIFo3Ft4IdkLFpI4z5l4Dmd5rIj5hFUjaFvexk/PrX8261J+XhEGgdcNYf21pqm
q4ZUqD/xO6nZvjjrZ3znm/a6PReu8GfxTEjyqDYaQtyiT1Dt40fLDpVdsQTT7y2JlKGuCFLjVq3h
oekEoJuJu0fAW9aN9+a327KYERzqUO24kdYDDQMPiXQ1vMDfmGOjUbOvjq6jJEUU29zQTkVi2MO6
PJZekbkqsclE0T49s9ZdklUXp6qPUiM2hZxuqZzDsb3RGnUnkIncb1RSq+0PFqhVmI7fHCB4cUKE
FNrwXuw5x8GyBb5laxzcJdRAE7Sw1W8qUiyq2GWpOvMIRqIgMSBU8csZJVTU3/qnlYkgsweYf4jn
LOQqgr479T9uVTwiEU87uBdeATJtzwUlpWWcNPv5qh08UD/Sd8Glh673j+43b7o0DgU+bHIe1WMZ
iObVOSpToH58+VadkXM0uS9Evu25krQ9AHwI17L+fvfbwap0onl0RtCGerxLrefav3QXi9c9Q9JV
+ZvLYRhr/jHBWUh3p0urYeQABJT4ia75jboUJ2ynLiObuNx+PfdvQUntA5JwuSSn1vZtVCG1Fg87
k3i+t8iNtSUL4Q29u3n2v/fdvldBPbihtDgPepObOIZOPeEuQ3GMXjlHEwmLSDCRpiJBMbzQ1RA9
mLdP/C5uzA9nJxgCBGkiuu2yaRQZv1n9aWEW25nUfFFV16sg0nrvIkyf++e2sHAvUpMHQszmuG4x
cPsNT0wz0k4G2nBHQtQEJIxU6foMagkoeLRqlVsCF0ys8zAedS976sBELxfebSdlJG2+88l4gqht
efR9NncqtqYEYdTNqb0PZYfRJQXrOjVzqcILFs2Kpp106DauM+x0KrpCrWBWeCHUR+gW/a7KClyr
qJkDG0hh/MFzbMK/zryiQ2SuNE92ZBP5Nk1Vbfvd1L9hBpxAxDHnz88WvBgIlKXu2HeUkbsjdZnT
HRzvMerxaTpWjcRTJCXEfbJWQQq82ozDPBhGfYJ48nbixBFUnCv4CluXxhGEt8D4WN7orZ1wwDRq
EljTrWucmkRefWiDMJliyoaZNmDrsMaAmr/fyz3ZeZMdj8jIq0C/K9w02SL6P4UlLfI30Aj+27HT
NtCzUkiwlDq2njTqhnz6Msq9n9KpE+y+iPGMOzXjZX3s6dD9oxNLRu5KODN05P/fcJ9xEMYEfbUy
dLLysgvNkmP+GK15OCq9gWD5ItzJIZAaV70TqpJnmL0pzyzceQoC6J0naY/hrInJ1RxcTmU38ocM
8YQSU7MahSnyOSDdQ2jsizJzRRe5pQDY2uxhFlJlEvL5rKrvijMXWYbOr9Zpxxxo9/Iyc/C/ELVR
M2tXphYKWA/LEeO9SypnJSv8BaVCbILp8j3UBK2VBvkuzMnCOpKosNev1eLzygCNfJcwkpcSIOGS
sPEFL+FS/RFgwx6GnqcOmYomC6kcO9zNC3N3C0noDSKh5emPj0EyMSi8pePMLBvyWl8I290TZ8k4
VJ4Ex15RixCWUixmzPAiaSa/5hdWa+TS2qRJ5R+QhHj4GBdNUXiUCtsPMYxLDSWuYYedYNg4uC4t
JvOyM4z5vT6GOkxHsZC1PnOK2K2Xhf+zPdy3QMPP6DbH4ayrJG3zMvCa5+LFW6IHlj/7r3ci/c4k
v6fhKlF1PDL5g9+dom9vz70mA4cr029GeqmDjOBwgH2R6HUGOj80DbXyRgoG0MNI+vUtlr1i/oKJ
RM7HTRCbxyY13ZZsQ4/xpbXEP4oLMg6rzzA69nuzXuSStSrR273GXFkk69YQ2i9L7vaYG1TRvIg2
WzNfWj3NhmP/Te5V/2wxIQlprsv86mD5Pvz2gEYFBRTO1VUvrGMV7iFmjickSrrcclEZlfJiDp/k
mi5s47QvQM3xOjc4G5TNz9gajaid46ggaVjCrNf08lgdrNKO7ibRPss/oqgeEaySJallmO00mH2I
/xdg4vm2Hz24brumWp6vg0IY6oDG8tiOlBtQVZyNFzlg8AxihW0VSMbJV1YpETmmWfjxwYKGHpLt
mWFy1TYCYgqR3ufggaogh5WWzaR5WaJyaEx612g1ZYEMO9lrisyzzmMa//9/GZnf6upqTAvG4qri
Jm95cqFox8vHLh86/hRtn4iRTDMnDXJMe8Ipby0wPTKGSbLtnSSG4HOLp/ojwu7IckOiJXK5vB5q
EYd0HXwvaPSoN8MDMTmhy975wuj1Ls9mC8yuhqdI53T3KlBIC4DnNq1BRIRQrFBAz225RatiGVfM
IzchNTcYTiDOHwSGKXsId86M2rpEoBG9bV7KvykaYclnusbSpLs3ET/7abuyGez4qA/F34MBe41c
Ey+qnYRQcxN3Ly+N2FLisZnIUxa9iUcpWJX8l52Z5Jp8mpnnajfMIMPy5DohaEnFWjwHIKnFZFkm
aydkXy7OG62kCvn1pTaR08/KVrVOsgNxsbWDkPo5m2D5nqVgmCHhS4cXxPHx01A9WjH12XVSBMfh
tsa2qFIjg66XLQHLTzFkMO5oxO0wn1rU8N+gvJQhVTx43NQYHcoa6mXiVTEp1tU8mPhs+scNbrwz
IsgVczmiJK7a4CBARkCTs7OwujZE0nQceTwgkpZRG1bVdNFajGWAZKt+NsbnHqCuoYSuWDuvzWLz
EESKxDMo7f35rcldaFPS4UmOYBzAXGAoEPNPmBFw1M16QhBCW945A77sD8udr9i675pSOVnFvW7Q
wesngRHdKq1XVFMAqlOSft5Xs+ixNJYvNy6fqFkAW15XJAAExXn6ecypJ0M6kbxkb+sPJrUGyTlU
NgRtfE7eWmCPhxVTTJJT9g2X9cN+GYFqkXHEjwQ+tDsZQYok5eiY787sSIdNPP3s+JClHlakEJMm
LjcUoLAreNU4VCU//Wtgzx+iIOiuZRpVdt0kbW74Qy/yrU8un69M/AVH9xydhJB9Rai0jRlb0iNr
yurJ41ndEC3tDUiuZLUsAfcl+XIJ0DUIIX0SS61bgtb/utIztwFDST07XVbKd5AjgomnYKQqZm1y
ASuPggAhBkO3zOp86I/OSs1wEoMuwmKgUu8hf/eUGVxFv9YLWChx/UYk3Ut92yzNvSavdqT9snLy
RQzhwCxE2OnQQJFsvxv//cIi/zWRVbCP2GDz0AKbI7XAhJ2JrLyr+8NmWZgRx0BMsornKL38AwEJ
xCGIZLZMSRiG4pV5rkhApe7Rhcn1Q7yrp8VcALbOv3u0RTSuLB4qYWIpQPjWDTY6e24OrjrfTx+B
09x76rlPssyvfpgHsJ1qxsOQYEMSWg3Aw079VzF+AYpt3TiYn9XFJI6O9PAk9SAi7CgD9C7ogvN9
iAMCESM3+okzs3LVQbafA9nQIKoVOp6qFiFwM1gsqloDwqG5IuSomwEPZBw5UpWGZ054Ccq9xDD2
J9f1EqP5HiZqoUm1F+SGNkI1PC7S+tghafeQS+ZHoywIewUwuesruQtOfzT9+AQmKySzKfLCsCYu
qU3pMDgA3zV7ZCA/kiirulmvj5W+G3bDv4WSyiDNhcfRf7pewk0dBEcDitysE96SoeNKXn3sI6YX
2hVosi4ngWwyrnuupzjAnv0OR5j/TAAxgIZJp5d0PMqZe8ngzCrWKb1DDvTbySceXi4Q7CpIZN7U
xwkBLrr3dMpccZT/JMAdplozZSSlV4FPxNmAmwP2S2uC9HFg+uHxQhu4yXAuXVUa8iO5qkbQWUkV
LBu3ccwrNx3yR1Ec+0Q6lNxpgV+eNwBV9WBNd43ENuid0tmzHYtObxLpRfVfUvQSueFo+Sbjw6An
h0dw0rv91AotR9qm8L4iFhCRPqf1ntj4EJKW295NT/xT343z7Uuu4VAR4CVQxQdHONpjqt39mRLu
InFxWTcAiX8ENSzLQ5THclExoGrG2N6dsfh7VCW2kasCnTbA4ietO7He2GPbvQwt/Hhf2CYqOwwI
aooCo8BqsMD0xCusUnhxJA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 7;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 2;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "kintexu";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "00";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 2;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00";
  attribute c_depth of i_synth : label is 7;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "kintexu";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sr_ram_adc_iddr,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 2;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 7;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "kintexu";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of SSET : signal is "xilinx.com:signal:data:1.0 sset_intf DATA";
  attribute x_interface_parameter of SSET : signal is "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
