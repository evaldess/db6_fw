// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Jun  1 19:36:57 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sr_ram_adc_iddr_sim_netlist.v
// Design      : sr_ram_adc_iddr
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_adc_iddr,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "7" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
oXWtLD5iZgbQrYuMNe2KUdqtAJeVy4ThwfEWo2WzPvA3Hi39OsMDoYUQ08qJUYP4c7N+jqvZ159k
++VSflYBsC/FweUuqMnUkXdA/eY70d/afnriIQ9jaZJjl9wwAllOoUpdMOiEkUNhffeMXGn//CpA
APeY3S1nQXlKyYhbTaHASrY+MRM1q9tgCeIY69r1q0j6+dl4nXFH1WtPZbBVv07Bhv/GxOHdf6D5
YyOv8I1LOm+HWV5a2cX2lUz/TpRrCM+2DAlhG03isTPJnQeKfEz5bMiAGmrkEKX7ZiEgsLK7Y0A7
Hrih21OWB/pf7bor8sdSS2Jf9CQfO+RY2XMjnA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
l6BN8OOPTxn8lS5GNHkki0ElZ8+771nTKCabKGugMViu9HTapThF2hvvOfDe4ze7iJQ7OKJhT+T0
7vvp5/tV69ZgwfC4uvfPmI1Ixuo+8x+wHjDHl2zDxXhnQeyQLHrLOOFsZ51SLCCwXx1mptsWb9iR
ppR/FG4gDwB8OTDn5dW9hLGS9K8DOJI2lv7VutBJKBSfvRstHEOBJwzk1Bp41+5GOXhSlk59G6Vh
8ZdWGgL8b7/3OJPS8dtcnmR/DOSLjt7Jz7i0qgi3sWOw7EWe2we4rC5a1WF4sRqoUSoonuJwiAEN
qZhZzP0/AWZ0RLwdRmDGSJTTKsdFO9NOlTZPVA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6896)
`pragma protect data_block
bhxna5Pbg+rZQbV/tpMo9SV/3Vf0yqUbajFdi18NQOTiu3Qryri6WQPi1SS8A1/WUJzPs/2r4Bzh
xZWPrQJ0Ku0Y6WIATo5MesVNvlimYSLjInf6hWFHxw74kWuuIhH6LTzoypQyYQna5MO0QnILMLPL
XE67NV79qmYT39LWDijNpuIKklpiZocxfDxCm0o5vTNRAWQIsCQeHkg+1qTPA1onpdTfD/q64xJn
JplJdZTlQXj7WlLy3WadBKKHHO+0Eka4Ujogfumq2+xTs0uhkEqzo2w0sAPu+BzT/reWucrEUTIx
Zu0Acqshng4eGfbdwtkSZwMSAhrFB2A82FXAqYiJvhEeickIqEiQhR/UQwVr7dLXW/KKqXB7pnWE
auk/1SW0XVBjKgHWnMCW0E2iYMa6sJyP+QGeJdhAO1vSESkpSTXCVVrvq2vMYLLqExzYdsl8NNDq
KoU73PUXya0szoum9xEqNG07WslbbEqyOymK08eSA6NkFyliZAOZ42OLb5H9RyYWtDOp2joSO1+2
1aiu1ULxbFfV4uZ3pUZEJu85ECE8n39eeAbB+wgjjqitEQVhhdhDL22wwg+dxhRc8GJNnWbdLKRo
8PUTKCXq4b9hNHdCUnRJ9VDueCIqpXJ3m4weMT6tSSbP9enh309Raue0mveD3FH19tz3zBDPBAvo
ju9UqoOnPgEYh+4u8KBIgizmNGDVEe8gbCKH1J1ifA5ob5PRhTRN1dP9m9d52YX56+21+Kizho13
2gXj95/tDd9/jw0S3KypJs7Cxen8FJzWIrVgoJ9IIpPV1kKQQysaDCZFhm+wsxPMLt2L9a1ac6ck
y1AJ1fVh5l5VEvXkj3tF9IIIRmuDHHqOjMY5FwU++0N8RQ0XhIEI2487ENc9RBxZDOhzbdaLK3G1
7o1vz0ItHKB31rCUwhkLOAj8KQqzH3D2szZS8KDuXC55+tM7PqPWFS+3QIdRsztmtx4CbWnxxm7n
SMivnGVBvCA7epBTSSFwmW2QzSflXSh6n2BTukljmtCtWbnEpSda7jFrFArDZlBUpworsqhfkbYN
Hv+t/nrL3HnIqZ1t3kuxb8JY41FTXOTfaYOrhDOXPX2yf8ac0lHoLvdLn412Ak/GzNAAf3fAMEJC
zqxwVIbyYWVn+LwMx2xubYcEpCkb5RJSXModFw40s9nVTivd8zwAQxdrbMaYmQnCMdMSHLOwi4IN
dYOA1KMvcPsTAdS2xNDV+36h/tGEuZeW6yHx+X05kcqtCz3idgneP8PBLISCkf4jJc9OtafgMyBq
TuOe3tMan/PFdY4tOtPQIK1Xj16DHEQQn3Xf6pds8Z8paExJTpnotMBjkWu34QWotMzRxmj1yFPI
CMv9P2nCUpQDK5jhWczJ42KyHKKCmTIWHSNpJOO4qjxwVWULjEeqXdMkYEdW0EcZObq8fqt6OpMB
OeJLuH1PEg86DPVErTv/0zJAtly0+95tJ2FBgGx/6GUBKmfAYWWycd4Y+/SoFW8hGTwnsvhxS+DT
73YEaXx9DPoO3BgfreKacI6ZsdX3RDK3gasJFszfTqIuw0OUm/QuXWV9kdBK0XW+b8RIxIE4qbfH
5vepaKa6JWjBMwkVZy44XJhUS2QdSHeww2EQWwzaPsNEanvr7cRQfCdoJyjxFkn8Wko29kdx+s69
CSaD7DZoxtr1dzp02tAC7fW9j00bZ2TJeTXu99XHkzHsP9xrki6u1FWeRf71Yp3wVYn07pe/14iU
6miL2N4Pkx98QsiSd1yZKCfjp6VYrYqIyqhnjtJ93Y9vNFo9drRQnCHmE61mePn/Sw4IxyTLMNAg
ZGnBHhCstVOyisMpVjOB8G7cg1irXFpLAcFklGA2SnDct+erI6merQJ3wE69AlEKkOjx5jfKQtOX
hmN4/E7tX0Skmx0FTZ+0E90U0QjOWw/rmHjgt1TOjzyE+oXoyrvK6ofbpII90B1Ew+QVy0dmZEQL
lcc2wMeKUiACBZPqV+muRG3UwPYLq5AT0Z7cHejpZBFf6KSBHiMPi8ds6WraDnkWOdbmawh4M6RR
rhEwOhQj50XTf7TpDmgD84dwHz8Qkm5jZ9IeOWJhjFIjJCF+NHrVLuIL7eNNn+7wYFDp461buon8
3hrkFyL7LhNNgAd8ebrhRWLNQe2V4VOG8E5HpAypzpO/SISVWGmNhuAvtc5E8LCDzBOExo9j5uOy
LkDB6BcsNb9HFkMQ2rAGXRkoW795n7cjwvyq/wFNme1p5CxdHeNWbre6j/MYYLYX2Wvbv2JUsdbq
kYB5CoulokUaURTIiKHclkNFHkb5xw3Qn4ZeSUDIRoNoSu5Rg3zEso2lMWoFzUTTA2y7ol9jhOo3
3EOYYv3RJ9FUcx1zDK5jpi/nuFJKKqMF217yq3D7wEGTUAGR0l0irzuou/+aE9eV7Az96SiftBsp
tknn9hXsl3x7dJjSmLm5sIhuHK7mDdR5iNYdkeU1rskV7HGfGLiwAHuYu3aJJGwlV/3EZC+BswS4
LXGYVJe63he3BuQ7ofEUeryX4UHSvMamuq4lsSG+7KXlRSmXpO77nX0ztVifvj7Dxpc3/A+tOOVd
xvWfhxtvYChwdDW2qO1TD73Xy83LXCcWVUAjGLKWLUiFCtKCECP6FwHY6E0+KRttTtiBZlteYf8p
mUJ+D27lh15Pziro4+7Zm5Cld3j0FGGHN5pRi3557vLEl3rVpHP32dXKTJNe3BxuvAimtFgM4xgt
Es2HEBwOw6oqxM9bXZ3R4KM06P8bwnzEJjlkrj1moURIc14z1XKYnjYxFOFWz81yfN21XCptAp0C
epJYtHXO72sE7adBp5eh8cM/vD2bPLoJAehpSFzRccJc0m7dFuJ806lUJqj2IVYn+jZunerPWDuI
9cpAxE0JO9p23GbFE/qhALS3sfUnM/BvUIbD4Y0ubGiO7AZV2FsM2QRR3f2L7XuXomTr8dIUoPYH
d8V1oiejQ7P1SZ13ouf9WdDlD5v/vtEAcxpgL4Yz5kp2TZblFuI89TcLlbUQzO88NGNpRoE+sUnB
yHZU03EvLayMI49tM3R1i7zbCK3iaY9f33MkGsVPb/ui9rBDS1CB6OrtbiiILFpFFfN7O8A2sldH
a74fhvTFr3yot/ojizIId0hmldmWYUjq3C2HoItCqlUKndDVFxclChKCyJ0jNU1USfqIQ8HR0ADO
TRQNhNxwFeyDYoM4Ken7tT5o3FL/8tISY4FegFlZTgwdWXsY8Pa89RF/IIFjTZpWJJKH9ywkbQrZ
utypgdJsv8xiuJy32ma16gRSfM38oP4eaG9LBZgupvc0QI79Ic2VqRJBGFjdV7JYzu+AlLht3kpT
XLNxP3xmokcQVqBP1fICLQ9zqzQre70yhoppG9XeXYU3WUnmffkm2FAGKmQI+ZMJBknvlJjybcuL
wW4frHOGyfWjy2CKedNDtWWtopUOs49tjQLxy6irNriY6MygPhw17OhsrXtJ941/CcniKOM4VtMp
K1zbZ/z574RICZEk56saCHPWO66AGaas+SLtqgezR/zmt7aw4d7+TRsXKTDyJn6f6/k5igI5DxWz
jgP+ze24ba15Sr89hLgGF9jORhtFdtJMNjU6cDUa69aEGGgGtyZQgNhlIwr6iLrHAtimw6ti1Xsn
qK2qAYDQDdqA+/n1C6KjMZrZcucV9PIGQ5h28C4QBuisKNcR+Hv0asSNwX336pAypTQfsr74wMZJ
A70nCE5P8zI9x809B5RmrHoUGoFpDnIQH44J/MmQ8B8hhmz2NNuTfwyum+NN2xYS5nbMSE2KHjzF
Z2j0zRtmR1gkFWlaCHP1xir0oUKpuGbTE1nh1uEl4AOMgoYt6t5VJhRLIlWaDN79y1d7YhuvFkYe
ibrxHj/ThYLLODFWqxpd9I68WtSwxM5OxKvzZ3L23yKSKfHMACupPi3AvErocjf6HLMncfMF4SRo
XvK58jSlSSLxR77L/HtKgywDsNse43zvc211BiO5oGMNgRTZM/+EazMFttABnH0chMuXQITVFqev
Ppd1wjxms51NhbzA5YX4fEh2QYPU/J6iy50J9KOIbQ3WKJjBlBt1WglxyK/DMEDANMfHOwun7P/X
qk0uD3OruZMPphNl9I0eHn0I6uAGAWeAcCLw0mN9xiLhvEV+FOcykdSkJLktfVAajHWNJavovi9t
G2b2sHoikQjf8E61gPs7AYcRFm1YU4Tt77bXycegvxqUdHQGO5jH8wQqszGPTFVJ4LaV9clEXlhK
AOypa5UUfQ/AEohUvlY6hLumJeYGxwjOHXbtsCwJbfEoaC6PAZgQF8vJWLBgA9Qx0o1gfI30e/tc
i/ti3LyQKmvl+zFNhh+kFC5ehzW386A9pJnsZdhqm3bu0GpSTlWDVxtjyA14yo5Pap/xo6CflP3R
r/LKQyo4F8Y7MPMK2SogmKQNNqr2mwGCTJC5yPOCxD+eCneGfqapFM1SaeNu8daYQ4jQER/apjJs
dH0a81/sHHt4OWCzvQt5vzxIZvc10rwPHwTHAQbNgYpZwytLvj6Pe1hQg77glWSB4GWinZCLV/9i
wmXvwOBsjQTQozitBDK2LXoOoC1PMk+6e/EK9G8v4ilsDwgiRuETJzZw0hnk8dkEcsGYasUtnDC+
7WpdQ15wwM6EMCnJUPLre+uHGiLA/BxvtaPSOtKjp61PciXzib5lUE3GWIVVnTtS8/ZRDQ8jAOG4
9vCqPhCMKQlHGNwSVJmeQSbCnhrIijkDcJdQWop2XBRE7re5MYQkQzSbr6saDB3TGPV73JyiBgF1
0KGUkRuiRUSRPRZYlpsg7jsa7FE7e98Ozw5cqSo47bt2wma+zdyFgyStB2qRZAky2w+j/IH6xLao
K0R2gx+wYCQxHcJtbDo/sgHl6FcRT1Bt3JwZCWFSyAaMqm7CmrFD/EXa75Lw622WluOIVqYlBJIf
nJQy0NojRRRxasHHbvRcACuduuYz87ZaRK2AGcWMEUTPeWuxrjOzQAZi4eGFsMcglKa7IqPhCBiX
olECSPmkAVFj0XqcZedm0w8JL3QjsHTmJpHA+PyIO/wogwv4dgilq6SCC7Ar8MZjUvoIqRb9jw/h
jMGVfkLbZv9CMBHSF/vBibZ/2rtNFJq91QgJeJXn46ytJF5VdjDOsWtw0mpYwnkqzuO6IqYhzx60
TUm72Arkf9sbloHS8hEv9dVH1IRdW7ZvlH0fN5x6jcAZxqvvBQAjnPo0SqJ/3HCvr84ax2257Q/7
lqnmzFFu2wEIbe7vGK1I2esPMasavVgn9lqdx2PnfvX4HKMlfqRMjtf/3l8AgN/ai82B567y9pmC
lXLMo0m3pSox7lhtHs9iHzvhXW5MpDJKgT+wYgRbRaByEZgF73HdwJRdWB8NxW5PDSHEp5igBD8k
wfMIdrCkKcakuYOaPmtxMd8YUSQvMRyxfKmLHqKpkB1keYVHiZ7VTMh9Gsl+x7OiVWQxHfs2bD7g
kODwMOT+4BTT+n8/NVeYyaHYx8ImCQep8B9e3CdM/JJQIJOtBJcaxD7o6Az3xAFHZOAWohGeCZsF
TjdEoj5HZ+0YVZRDmhuis0uCo6mewPRB/qYx8o79YGCmE/+xhJB35rjxavQYyyzghQgpSBR9gYRo
YGaWx7HU+4wbAY56z0q44NsrbTojTg7+MwbjyBewJXYVI4iuHednDMGSeflH8uLa1boKBLTWtAKh
eak3idokS15y3c4PyGYJ4T3cxNkr49C8etUzEqKs6v5jToVcOYcWX7+N4cbEqyWyEAgn8zg0nYNg
WRK7O62+62Bqx1FNXm9HlZ+Xf4YWY9FbfOlxfxBBIsssUR1Ub1YDBwThKqwY9itJUSY4CFYEhFJh
GpgFXOFXXqHYw4FkAvGon402FGQL7E0onnLakEx23LGHe3e2SzVgjFuHyHsRE07bccey9DuD6nRW
7OpGL1w9bEwnWR0dq9LDrGGDeHe3is+Lk5gfbN3Z5Xt4rM4uYvMk7x6gtUZVV05yFiM7RcRIPdMR
+4aBgxVZjc0i+01v2itzbcWVtH0IqmSc8Kl/WZ+zW9k9mImgr/nRAD6WdG5vFy8+NQjXcvvxSTk2
I3hUekZMF4BMfVd8cBDwvxbOpSGuV7EeN9Ta4TOCwfp0d1AVN8D6AsEbZuhbK2WXGxbO3AMknlDE
V0hyxAHeFq/kN4jaQyHqOYY3fT3cvMerW6xnVhkcBk3XyV4uBOCwX7EnF5SaLhtQ6nNduJzO3gB5
QCGcMz3B2GYoh2jP2pUx/EBGsruHm2ACQ4+2uHUb3NttfkYx+ugKxD0pO++tVMNz2EidC5+0XHh7
HovclYuxh98tNyCeBsnHg73EncE6xlVZGYS88IxCJiTYNUumnRMWcH6tIVVBAihaVp8MUMIoUPzX
gH7ZIiieiVyuu83eAQrWoDjNvUDHjP4jwJNgRhkzupWbSmXDlevkkzznotpGUrmVveNeIJ4/0Hmr
YdbPHzXmbnZx8aytChNwxg/uYaUgusu4gS/nv/DTq3WzB0fL8xQAeN4L4baHx29ZAPbnjFGrQ120
lzPyaACMtlWF7lzOZ6OBn+ukjoObsm994TGHix7gsdSjXmISl8rV5rETVA/sweGJC0m+gyMl3zcW
b4ZVHJvccc/6VlKwth2KhOSGrraj29N7UhPWW6fqlz6VeuDmqKA1LDKyxMAcpjX38nCT5iJJ5Smz
2/A3YfX2J+HyxnHXLmJABJJuFy0B4eiVXuh8z92qyktu/CNmQzQyan4osCbrBKIbLF5lGG26V2FR
W+4qLW7aF7swdgB/zebxHEOkwqFnh847lYtX1b9OX/tLpG3ogZ2c2SYP2n/5kQtoa3kxXbdIXFNS
JaasRaVdKJLNSvAhWj0IkFXcomc2pbkIY7ffpCRTPVfTNTWSceajsnLo6yvbiIQs5xGLm/eg5o5P
Cw330DSYAqQ+ASkytypIX0NEncNVjJ0EtOxeIckzxs4vKnRLBdH55NwzPQYNA5vfluH72ZEFVu2K
9e0Fv0okQfiIk9FnqiAuoy18kZmm7yYKf8J2oIApgKFd3J7meOMDz5W0c9Wkobr9mKtXK5u0XInK
QIVasBsEAOM1nG1d35peg3ocFH4cwUOYyPXsTM9GPOmZhqN/xeB9IHm5V+EKRVjheTRwXkYcL2Hj
cJOkdQQJr3P1Hh7WglIhcwqYXGs6aWDe3YGSoByTwRdqXXzhJbw+5ffHDQlRgpTAJVYeWo5IRJ2O
A0vrVyT8D/awf/1BTCKJirkM1kAUYWzYB/U+D5BGiGwVlpvM3RgdOxx5YWGSQvDdkqR5rADSOtXb
CiYO6WSkoowbldIfQxTUlP65R09Hw7TEtV5YMbXbhi+8b8M0vTe8LlXNc/3/dZC0LIxDELZ2/fis
OO6RfvRZL5pZcNloj+hXECIhDCDz5eEQ5H3w0KhLCf5IMEERMU3S/ZHD2nJNOB488vA16N+d+lGy
FJVUqUR4s2LDpCBIGnGLhragW1uKbtWYQ58osIgu7+vUNcne05hq3lyEqYJnOjtyS8fNgVzHLRpM
r42M77sXo1w8/vYDE5kzCSiSY1n4ZZYZxx1TnIQkpQWgdyOJt8HNBomn8yflnqaXmu62QbnpL+gP
67a8X0ofyxbyY/X7vJfyIGIw1XFtwMuP2EJj3dyqJ6vFAImSr71dU/sKa6YwX8K2zsaQy/ia//j/
dYsjGuQ2vR5V4qRFYeGs5WFxuNRSzOdEtkWmmyy8KAjQzxn0fGYhXOA82YMarl81Geec7To7xnhy
bW6ugJikHtYDnmrSHDfOjwkhf7r02cbi6sjcGboPsXbQVru9d5cLRepcYaGylhFFGj0SLOUYYQZw
gpgJ514YtMDe/L/pCkBh3WVGltl6gS0th36o601qlXiA0LJWcjbduy4OxZFfpuoSSKCFEzCzEd+O
lzYf2+v7BeUeCE54nm08xuodza/FZCUWt5PmktrUE5KeP5kneHleHUUeUL9dKZkUJQFNH3exh9K7
A+droY3P21J8cMjHvjvC92XdExgSxy/x032Ilhy9ExWes8On4S6oUSwMcIZ19GgccgA6ZWaFxkxb
THz4wm3aeZ0x5WCI8X+Qv7UpgsKKcx2E2VrBduVtmttXvCblwRVwRKjw/hxzoMmynn+aVRIjqQZh
UX2iP/9AKzGyg8QtipG2CnhKpdFp+KqK795URPmj/X1UUTbRDlxJ5Mj10tc9Sb61gP4WQct+LEXC
iWxy/fcj0IIwCGHXQClzZ3NCFePuvVPPM9X8DxjLGU5v2sKnpQfoL9jEmp/KvqjPSaXSlliMPfX1
HFmXApT0BpWxh6c3HndlbDZJuN1fnJr1zzLcsWwy79S4+w/V1oCBi+aauVNQ6gjqh+p/SXboigPU
iJSF/P6/FX/ml5MooQegSOm/WcEbeGgJOnG2IJznONVLuQ/K2mGgkJYy7KfgPDQU77SgfpkZ3AOv
60HXdfU+DfF5CXlTPfygHKPxlQ0aWyu2IMSZoCOXq0dQR+1oK1bbYUdD16nK8dKY+8oTB12mluSk
LsfJMg88/z5iNIvJywlAHV3PHPneJSKqaL/D6zlH9my/xpypLMYQOlbWnSPnc4EP5NHkJh2CgbQi
2hpDI65P+nXUUY+S8FRUsodPOqn9nNAgtspm5O7M0ZSS+4Uie8mu4KefzKPx0eLbuVwcZcxz7cBA
ZIc4sKQEKxx/9pezDRORmp4ZBCQkHisZdYuq9l4bC11QuX0sb39adFJyCilZgFzBpUpGt9C8Lf0P
Mwh0rhN5Y8nApE9dE9nn5CZ7oyetNF94NAoSCp4xyEWZ+MuvFeenZE2C88bzhr90afJ8Z4RPAtME
GYryylAU/f6QPoHOIc9MS8B322KWgGzNBdg9R2QeGUrVy8vcyKEt7dHerQXFhkfcoHhGnRdlLnL0
rD0ZLm+H71FZwydPSEu7jq7WSSt2QehnZkPJD7jEbP+Ijp2ywNnqT1HYDsp3eWbV9vG4ud0goOYn
f/JsMe0pOREIyOSPr2GcfusdGoEY775ui8EtyVaHDzKj1hFt39ECNnpKj7JCwMUJ7aOS2j7ka1+y
ErkJKz08iAQR5vug+Kl4NC7RPRL7MOdshjT9cDRkexL5RPVut4SUZmZd8ZiaX4IDj+8yerjqIuAu
gdVxGogWbm3WSCojZApa5KLF4ZVgZJgEZ4py3LJodbjdVHT1j4Gi9gappQ148qOk2baO1uDBxjQ=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
