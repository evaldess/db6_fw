-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Thu Apr 23 13:53:18 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_gbt40_mb_stub.vhdl
-- Design      : mmcm_gbt40_mb
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    p_clkin2_in : in STD_LOGIC;
    p_clk_in_sel_in : in STD_LOGIC;
    p_clk40_out : out STD_LOGIC;
    p_clk40_90_out : out STD_LOGIC;
    p_clk40_180_out : out STD_LOGIC;
    p_clk40_270_out : out STD_LOGIC;
    p_clk80_out : out STD_LOGIC;
    p_clk280_out : out STD_LOGIC;
    p_clk560_out : out STD_LOGIC;
    daddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    dclk : in STD_LOGIC;
    den : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 15 downto 0 );
    drdy : out STD_LOGIC;
    dwe : in STD_LOGIC;
    psclk : in STD_LOGIC;
    psen : in STD_LOGIC;
    psincdec : in STD_LOGIC;
    psdone : out STD_LOGIC;
    reset : in STD_LOGIC;
    input_clk_stopped : out STD_LOGIC;
    clkfb_stopped : out STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    cddcdone : out STD_LOGIC;
    cddcreq : in STD_LOGIC;
    p_clkin1_in : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "p_clkin2_in,p_clk_in_sel_in,p_clk40_out,p_clk40_90_out,p_clk40_180_out,p_clk40_270_out,p_clk80_out,p_clk280_out,p_clk560_out,daddr[6:0],dclk,den,din[15:0],dout[15:0],drdy,dwe,psclk,psen,psincdec,psdone,reset,input_clk_stopped,clkfb_stopped,p_locked_out,cddcdone,cddcreq,p_clkin1_in";
begin
end;
