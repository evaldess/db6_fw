// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed Apr 22 19:37:42 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_gbt40_mb_stub.v
// Design      : mmcm_gbt40_mb
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(p_clkin2_in, p_clk_in_sel_in, p_clk40_out, 
  reset, p_locked_out, p_clkin1_in)
/* synthesis syn_black_box black_box_pad_pin="p_clkin2_in,p_clk_in_sel_in,p_clk40_out,reset,p_locked_out,p_clkin1_in" */;
  input p_clkin2_in;
  input p_clk_in_sel_in;
  output p_clk40_out;
  input reset;
  output p_locked_out;
  input p_clkin1_in;
endmodule
