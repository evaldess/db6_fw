// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed Apr 22 17:58:40 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_gbt40_db_stub.v
// Design      : mmcm_gbt40_db
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_in2_p, clk_in2_n, p_clk_sel_in, 
  p_clk40_out, p_clk80_out, p_clk120_out, p_clk160_out, p_clk280_out, p_daddr_in, p_dclk_in, 
  p_den_in, p_din_in, p_dout_out, p_drdy_out, p_dwe_in, p_psclk_in, p_psen_in, p_psincdec_in, 
  p_psdone_out, p_reset_out, p_input_clk_stopped_out, p_clkfb_stopped_out, p_locked_out, 
  p_cddcdone_out, p_cddcreq_in, clk_in1_p, clk_in1_n)
/* synthesis syn_black_box black_box_pad_pin="clk_in2_p,clk_in2_n,p_clk_sel_in,p_clk40_out,p_clk80_out,p_clk120_out,p_clk160_out,p_clk280_out,p_daddr_in[6:0],p_dclk_in,p_den_in,p_din_in[15:0],p_dout_out[15:0],p_drdy_out,p_dwe_in,p_psclk_in,p_psen_in,p_psincdec_in,p_psdone_out,p_reset_out,p_input_clk_stopped_out,p_clkfb_stopped_out,p_locked_out,p_cddcdone_out,p_cddcreq_in,clk_in1_p,clk_in1_n" */;
  input clk_in2_p;
  input clk_in2_n;
  input p_clk_sel_in;
  output p_clk40_out;
  output p_clk80_out;
  output p_clk120_out;
  output p_clk160_out;
  output p_clk280_out;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_psclk_in;
  input p_psen_in;
  input p_psincdec_in;
  output p_psdone_out;
  input p_reset_out;
  output p_input_clk_stopped_out;
  output p_clkfb_stopped_out;
  output p_locked_out;
  output p_cddcdone_out;
  input p_cddcreq_in;
  input clk_in1_p;
  input clk_in1_n;
endmodule
