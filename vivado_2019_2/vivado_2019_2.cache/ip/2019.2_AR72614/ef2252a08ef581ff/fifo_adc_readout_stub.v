// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed May 27 20:05:02 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ fifo_adc_readout_stub.v
// Design      : fifo_adc_readout
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_2_5,Vivado 2019.2_AR72614" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(srst, wr_clk, rd_clk, din, wr_en, rd_en, 
  injectdbiterr, injectsbiterr, dout, full, wr_ack, overflow, empty, valid, underflow, sbiterr, 
  dbiterr, wr_rst_busy, rd_rst_busy)
/* synthesis syn_black_box black_box_pad_pin="srst,wr_clk,rd_clk,din[13:0],wr_en,rd_en,injectdbiterr,injectsbiterr,dout[13:0],full,wr_ack,overflow,empty,valid,underflow,sbiterr,dbiterr,wr_rst_busy,rd_rst_busy" */;
  input srst;
  input wr_clk;
  input rd_clk;
  input [13:0]din;
  input wr_en;
  input rd_en;
  input injectdbiterr;
  input injectsbiterr;
  output [13:0]dout;
  output full;
  output wr_ack;
  output overflow;
  output empty;
  output valid;
  output underflow;
  output sbiterr;
  output dbiterr;
  output wr_rst_busy;
  output rd_rst_busy;
endmodule
