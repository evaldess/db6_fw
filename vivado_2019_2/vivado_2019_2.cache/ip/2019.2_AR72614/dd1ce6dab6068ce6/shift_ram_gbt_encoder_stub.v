// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Jun  1 19:33:21 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ shift_ram_gbt_encoder_stub.v
// Design      : shift_ram_gbt_encoder
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(D, CLK, CE, SCLR, SSET, Q)
/* synthesis syn_black_box black_box_pad_pin="D[115:0],CLK,CE,SCLR,SSET,Q[115:0]" */;
  input [115:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  output [115:0]Q;
endmodule
