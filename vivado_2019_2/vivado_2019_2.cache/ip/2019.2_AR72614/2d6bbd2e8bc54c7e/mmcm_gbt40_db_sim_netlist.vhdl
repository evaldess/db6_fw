-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Fri Apr 24 17:58:51 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ mmcm_gbt40_db_sim_netlist.vhdl
-- Design      : mmcm_gbt40_db
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mmcm_gbt40_db_clk_wiz is
  port (
    clk_in2_p : in STD_LOGIC;
    clk_in2_n : in STD_LOGIC;
    p_clk_sel_in : in STD_LOGIC;
    p_clk40_out : out STD_LOGIC;
    p_clk80_out : out STD_LOGIC;
    p_clk160_out : out STD_LOGIC;
    p_clk280_out : out STD_LOGIC;
    p_clk40_90_out : out STD_LOGIC;
    p_clk40_180_out : out STD_LOGIC;
    p_clk40_270_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_psclk_in : in STD_LOGIC;
    p_psen_in : in STD_LOGIC;
    p_psincdec_in : in STD_LOGIC;
    p_psdone_out : out STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_input_clk_stopped_out : out STD_LOGIC;
    p_clkfb_stopped_out : out STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_cddcdone_out : out STD_LOGIC;
    p_cddcreq_in : in STD_LOGIC;
    clk_in1_p : in STD_LOGIC;
    clk_in1_n : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mmcm_gbt40_db_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mmcm_gbt40_db_clk_wiz is
  signal clk_in1_mmcm_gbt40_db : STD_LOGIC;
  signal clk_in2_mmcm_gbt40_db : STD_LOGIC;
  signal clkfbout_buf_mmcm_gbt40_db : STD_LOGIC;
  signal clkfbout_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk160_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk160_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal p_clk280_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk280_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal p_clk40_180_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk40_180_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal p_clk40_270_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk40_270_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal p_clk40_90_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk40_90_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal p_clk40_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk40_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal p_clk80_out_mmcm_gbt40_db : STD_LOGIC;
  signal p_clk80_out_mmcm_gbt40_db_en_clk : STD_LOGIC;
  signal \^p_locked_out\ : STD_LOGIC;
  signal seq_reg1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of seq_reg1 : signal is "true";
  attribute async_reg : string;
  attribute async_reg of seq_reg1 : signal is "true";
  signal seq_reg2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg2 : signal is "true";
  attribute async_reg of seq_reg2 : signal is "true";
  signal seq_reg3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg3 : signal is "true";
  attribute async_reg of seq_reg3 : signal is "true";
  signal seq_reg4 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg4 : signal is "true";
  attribute async_reg of seq_reg4 : signal is "true";
  signal seq_reg5 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg5 : signal is "true";
  attribute async_reg of seq_reg5 : signal is "true";
  signal seq_reg6 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg6 : signal is "true";
  attribute async_reg of seq_reg6 : signal is "true";
  signal seq_reg7 : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute RTL_KEEP of seq_reg7 : signal is "true";
  attribute async_reg of seq_reg7 : signal is "true";
  signal NLW_mmcme3_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcme3_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcme3_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcme3_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcme3_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of clkf_buf : label is "BUFG";
  attribute BOX_TYPE of clkin1_ibufds : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufds : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufds : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufds : label is "AUTO";
  attribute BOX_TYPE of clkin2_ibufds : label is "PRIMITIVE";
  attribute CAPACITANCE of clkin2_ibufds : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of clkin2_ibufds : label is "0";
  attribute IFD_DELAY_VALUE of clkin2_ibufds : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout1_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout4_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout4_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout5_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout5_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout6_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout6_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout7_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout7_buf_en : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcme3_adv_inst : label is "PRIMITIVE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \seq_reg1_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \seq_reg1_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg1_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg1_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg2_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg2_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg3_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg3_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg4_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg4_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg5_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg5_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg6_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg6_reg[7]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[0]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[1]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[2]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[3]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[3]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[4]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[4]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[5]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[5]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[6]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[6]\ : label is "yes";
  attribute ASYNC_REG_boolean of \seq_reg7_reg[7]\ : label is std.standard.true;
  attribute KEEP of \seq_reg7_reg[7]\ : label is "yes";
begin
  p_locked_out <= \^p_locked_out\;
clkf_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "ASYNC",
      SIM_DEVICE => "ULTRASCALE"
    )
        port map (
      CE => '1',
      I => clkfbout_mmcm_gbt40_db,
      O => clkfbout_buf_mmcm_gbt40_db
    );
clkin1_ibufds: unisim.vcomponents.IBUFDS
    generic map(
      DIFF_TERM => false,
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1_p,
      IB => clk_in1_n,
      O => clk_in1_mmcm_gbt40_db
    );
clkin2_ibufds: unisim.vcomponents.IBUFDS
    generic map(
      DIFF_TERM => false,
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in2_p,
      IB => clk_in2_n,
      O => clk_in2_mmcm_gbt40_db
    );
clkout1_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg1(7),
      I => p_clk40_out_mmcm_gbt40_db,
      O => p_clk40_out
    );
clkout1_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk40_out_mmcm_gbt40_db,
      O => p_clk40_out_mmcm_gbt40_db_en_clk
    );
clkout2_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg2(7),
      I => p_clk80_out_mmcm_gbt40_db,
      O => p_clk80_out
    );
clkout2_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk80_out_mmcm_gbt40_db,
      O => p_clk80_out_mmcm_gbt40_db_en_clk
    );
clkout3_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg3(7),
      I => p_clk160_out_mmcm_gbt40_db,
      O => p_clk160_out
    );
clkout3_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk160_out_mmcm_gbt40_db,
      O => p_clk160_out_mmcm_gbt40_db_en_clk
    );
clkout4_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg4(7),
      I => p_clk280_out_mmcm_gbt40_db,
      O => p_clk280_out
    );
clkout4_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk280_out_mmcm_gbt40_db,
      O => p_clk280_out_mmcm_gbt40_db_en_clk
    );
clkout5_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg5(7),
      I => p_clk40_90_out_mmcm_gbt40_db,
      O => p_clk40_90_out
    );
clkout5_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk40_90_out_mmcm_gbt40_db,
      O => p_clk40_90_out_mmcm_gbt40_db_en_clk
    );
clkout6_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg6(7),
      I => p_clk40_180_out_mmcm_gbt40_db,
      O => p_clk40_180_out
    );
clkout6_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk40_180_out_mmcm_gbt40_db,
      O => p_clk40_180_out_mmcm_gbt40_db_en_clk
    );
clkout7_buf: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => seq_reg7(7),
      I => p_clk40_270_out_mmcm_gbt40_db,
      O => p_clk40_270_out
    );
clkout7_buf_en: unisim.vcomponents.BUFGCE
    generic map(
      CE_TYPE => "SYNC",
      IS_CE_INVERTED => '0',
      IS_I_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE",
      STARTUP_SYNC => "FALSE"
    )
        port map (
      CE => '1',
      I => p_clk40_270_out_mmcm_gbt40_db,
      O => p_clk40_270_out_mmcm_gbt40_db_en_clk
    );
mmcme3_adv_inst: unisim.vcomponents.MMCME3_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 28.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => "FALSE",
      CLKIN1_PERIOD => 24.950000,
      CLKIN2_PERIOD => 24.950000,
      CLKOUT0_DIVIDE_F => 28.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => "FALSE",
      CLKOUT1_DIVIDE => 14,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => "FALSE",
      CLKOUT2_DIVIDE => 7,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => "FALSE",
      CLKOUT3_DIVIDE => 4,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => "FALSE",
      CLKOUT4_CASCADE => "FALSE",
      CLKOUT4_DIVIDE => 28,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 90.000000,
      CLKOUT4_USE_FINE_PS => "FALSE",
      CLKOUT5_DIVIDE => 28,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 180.000000,
      CLKOUT5_USE_FINE_PS => "FALSE",
      CLKOUT6_DIVIDE => 28,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 270.000000,
      CLKOUT6_USE_FINE_PS => "FALSE",
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKFBIN_INVERTED => '0',
      IS_CLKIN1_INVERTED => '0',
      IS_CLKIN2_INVERTED => '0',
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => "FALSE"
    )
        port map (
      CDDCDONE => p_cddcdone_out,
      CDDCREQ => p_cddcreq_in,
      CLKFBIN => clkfbout_buf_mmcm_gbt40_db,
      CLKFBOUT => clkfbout_mmcm_gbt40_db,
      CLKFBOUTB => NLW_mmcme3_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => p_clkfb_stopped_out,
      CLKIN1 => clk_in1_mmcm_gbt40_db,
      CLKIN2 => clk_in2_mmcm_gbt40_db,
      CLKINSEL => p_clk_sel_in,
      CLKINSTOPPED => p_input_clk_stopped_out,
      CLKOUT0 => p_clk40_out_mmcm_gbt40_db,
      CLKOUT0B => NLW_mmcme3_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => p_clk80_out_mmcm_gbt40_db,
      CLKOUT1B => NLW_mmcme3_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => p_clk160_out_mmcm_gbt40_db,
      CLKOUT2B => NLW_mmcme3_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => p_clk280_out_mmcm_gbt40_db,
      CLKOUT3B => NLW_mmcme3_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => p_clk40_90_out_mmcm_gbt40_db,
      CLKOUT5 => p_clk40_180_out_mmcm_gbt40_db,
      CLKOUT6 => p_clk40_270_out_mmcm_gbt40_db,
      DADDR(6 downto 0) => p_daddr_in(6 downto 0),
      DCLK => p_dclk_in,
      DEN => p_den_in,
      DI(15 downto 0) => p_din_in(15 downto 0),
      DO(15 downto 0) => p_dout_out(15 downto 0),
      DRDY => p_drdy_out,
      DWE => p_dwe_in,
      LOCKED => \^p_locked_out\,
      PSCLK => p_psclk_in,
      PSDONE => p_psdone_out,
      PSEN => p_psen_in,
      PSINCDEC => p_psincdec_in,
      PWRDWN => '0',
      RST => p_reset_in
    );
\seq_reg1_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg1(0)
    );
\seq_reg1_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(0),
      Q => seq_reg1(1)
    );
\seq_reg1_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(1),
      Q => seq_reg1(2)
    );
\seq_reg1_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(2),
      Q => seq_reg1(3)
    );
\seq_reg1_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(3),
      Q => seq_reg1(4)
    );
\seq_reg1_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(4),
      Q => seq_reg1(5)
    );
\seq_reg1_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(5),
      Q => seq_reg1(6)
    );
\seq_reg1_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg1(6),
      Q => seq_reg1(7)
    );
\seq_reg2_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg2(0)
    );
\seq_reg2_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(0),
      Q => seq_reg2(1)
    );
\seq_reg2_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(1),
      Q => seq_reg2(2)
    );
\seq_reg2_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(2),
      Q => seq_reg2(3)
    );
\seq_reg2_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(3),
      Q => seq_reg2(4)
    );
\seq_reg2_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(4),
      Q => seq_reg2(5)
    );
\seq_reg2_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(5),
      Q => seq_reg2(6)
    );
\seq_reg2_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk80_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg2(6),
      Q => seq_reg2(7)
    );
\seq_reg3_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg3(0)
    );
\seq_reg3_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(0),
      Q => seq_reg3(1)
    );
\seq_reg3_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(1),
      Q => seq_reg3(2)
    );
\seq_reg3_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(2),
      Q => seq_reg3(3)
    );
\seq_reg3_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(3),
      Q => seq_reg3(4)
    );
\seq_reg3_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(4),
      Q => seq_reg3(5)
    );
\seq_reg3_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(5),
      Q => seq_reg3(6)
    );
\seq_reg3_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk160_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg3(6),
      Q => seq_reg3(7)
    );
\seq_reg4_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg4(0)
    );
\seq_reg4_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(0),
      Q => seq_reg4(1)
    );
\seq_reg4_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(1),
      Q => seq_reg4(2)
    );
\seq_reg4_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(2),
      Q => seq_reg4(3)
    );
\seq_reg4_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(3),
      Q => seq_reg4(4)
    );
\seq_reg4_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(4),
      Q => seq_reg4(5)
    );
\seq_reg4_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(5),
      Q => seq_reg4(6)
    );
\seq_reg4_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk280_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg4(6),
      Q => seq_reg4(7)
    );
\seq_reg5_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg5(0)
    );
\seq_reg5_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(0),
      Q => seq_reg5(1)
    );
\seq_reg5_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(1),
      Q => seq_reg5(2)
    );
\seq_reg5_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(2),
      Q => seq_reg5(3)
    );
\seq_reg5_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(3),
      Q => seq_reg5(4)
    );
\seq_reg5_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(4),
      Q => seq_reg5(5)
    );
\seq_reg5_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(5),
      Q => seq_reg5(6)
    );
\seq_reg5_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_90_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg5(6),
      Q => seq_reg5(7)
    );
\seq_reg6_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg6(0)
    );
\seq_reg6_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(0),
      Q => seq_reg6(1)
    );
\seq_reg6_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(1),
      Q => seq_reg6(2)
    );
\seq_reg6_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(2),
      Q => seq_reg6(3)
    );
\seq_reg6_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(3),
      Q => seq_reg6(4)
    );
\seq_reg6_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(4),
      Q => seq_reg6(5)
    );
\seq_reg6_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(5),
      Q => seq_reg6(6)
    );
\seq_reg6_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_180_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg6(6),
      Q => seq_reg6(7)
    );
\seq_reg7_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => \^p_locked_out\,
      Q => seq_reg7(0)
    );
\seq_reg7_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(0),
      Q => seq_reg7(1)
    );
\seq_reg7_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(1),
      Q => seq_reg7(2)
    );
\seq_reg7_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(2),
      Q => seq_reg7(3)
    );
\seq_reg7_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(3),
      Q => seq_reg7(4)
    );
\seq_reg7_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(4),
      Q => seq_reg7(5)
    );
\seq_reg7_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(5),
      Q => seq_reg7(6)
    );
\seq_reg7_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => p_clk40_270_out_mmcm_gbt40_db_en_clk,
      CE => '1',
      CLR => p_reset_in,
      D => seq_reg7(6),
      Q => seq_reg7(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_in2_p : in STD_LOGIC;
    clk_in2_n : in STD_LOGIC;
    p_clk_sel_in : in STD_LOGIC;
    p_clk40_out : out STD_LOGIC;
    p_clk80_out : out STD_LOGIC;
    p_clk160_out : out STD_LOGIC;
    p_clk280_out : out STD_LOGIC;
    p_clk40_90_out : out STD_LOGIC;
    p_clk40_180_out : out STD_LOGIC;
    p_clk40_270_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_psclk_in : in STD_LOGIC;
    p_psen_in : in STD_LOGIC;
    p_psincdec_in : in STD_LOGIC;
    p_psdone_out : out STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_input_clk_stopped_out : out STD_LOGIC;
    p_clkfb_stopped_out : out STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_cddcdone_out : out STD_LOGIC;
    p_cddcreq_in : in STD_LOGIC;
    clk_in1_p : in STD_LOGIC;
    clk_in1_n : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mmcm_gbt40_db_clk_wiz
     port map (
      clk_in1_n => clk_in1_n,
      clk_in1_p => clk_in1_p,
      clk_in2_n => clk_in2_n,
      clk_in2_p => clk_in2_p,
      p_cddcdone_out => p_cddcdone_out,
      p_cddcreq_in => p_cddcreq_in,
      p_clk160_out => p_clk160_out,
      p_clk280_out => p_clk280_out,
      p_clk40_180_out => p_clk40_180_out,
      p_clk40_270_out => p_clk40_270_out,
      p_clk40_90_out => p_clk40_90_out,
      p_clk40_out => p_clk40_out,
      p_clk80_out => p_clk80_out,
      p_clk_sel_in => p_clk_sel_in,
      p_clkfb_stopped_out => p_clkfb_stopped_out,
      p_daddr_in(6 downto 0) => p_daddr_in(6 downto 0),
      p_dclk_in => p_dclk_in,
      p_den_in => p_den_in,
      p_din_in(15 downto 0) => p_din_in(15 downto 0),
      p_dout_out(15 downto 0) => p_dout_out(15 downto 0),
      p_drdy_out => p_drdy_out,
      p_dwe_in => p_dwe_in,
      p_input_clk_stopped_out => p_input_clk_stopped_out,
      p_locked_out => p_locked_out,
      p_psclk_in => p_psclk_in,
      p_psdone_out => p_psdone_out,
      p_psen_in => p_psen_in,
      p_psincdec_in => p_psincdec_in,
      p_reset_in => p_reset_in
    );
end STRUCTURE;
