-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Tue Jun  9 17:29:43 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/mmcm_gbt40_mb_synth_1/mmcm_gbt40_mb_stub.vhdl
-- Design      : mmcm_gbt40_mb
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mmcm_gbt40_mb is
  Port ( 
    p_clk40_out : out STD_LOGIC;
    p_clk40_90_out : out STD_LOGIC;
    p_clk40_180_out : out STD_LOGIC;
    p_clk40_270_out : out STD_LOGIC;
    p_clk80_out : out STD_LOGIC;
    p_clk280_out : out STD_LOGIC;
    p_clk560_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_psclk_in : in STD_LOGIC;
    p_psen_in : in STD_LOGIC;
    p_psincdec_in : in STD_LOGIC;
    p_psdone_out : out STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_input_clk_stopped_out : out STD_LOGIC;
    p_clkfb_stopped_out : out STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_cddcdone_out : out STD_LOGIC;
    p_cddcreq_in : in STD_LOGIC;
    p_clk_in : in STD_LOGIC
  );

end mmcm_gbt40_mb;

architecture stub of mmcm_gbt40_mb is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "p_clk40_out,p_clk40_90_out,p_clk40_180_out,p_clk40_270_out,p_clk80_out,p_clk280_out,p_clk560_out,p_daddr_in[6:0],p_dclk_in,p_den_in,p_din_in[15:0],p_dout_out[15:0],p_drdy_out,p_dwe_in,p_psclk_in,p_psen_in,p_psincdec_in,p_psdone_out,p_reset_in,p_input_clk_stopped_out,p_clkfb_stopped_out,p_locked_out,p_cddcdone_out,p_cddcreq_in,p_clk_in";
begin
end;
