# 
# Report generation script generated by Vivado
# 

proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {Synth 8-256} -limit 10000
set_msg_config -id {Synth 8-638} -limit 10000

start_step init_design
set ACTIVE_STEP init_design
set rc [catch {
  create_msg_db init_design.pb
  set src_rc [catch { 
    puts "source /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/constraints/pre-implementation.tcl"
    source /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/constraints/pre-implementation.tcl
  } _RESULT] 
  if {$src_rc} { 
    send_msg_id runtcl-1 error "$_RESULT"
    send_msg_id runtcl-2 error "sourcing script /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/constraints/pre-implementation.tcl failed"
    return -code error
  }
  set_param chipscope.maxJobs 6
  create_project -in_memory -part xcku035-fbva676-1-c
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.cache/wt [current_project]
  set_property parent.project_path /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.xpr [current_project]
  set_property ip_output_repo /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.cache/ip [current_project]
  set_property ip_cache_permissions {read write} [current_project]
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  add_files -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/synth_1/db6_top.dcp
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/system_management/system_management.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/sem/sem.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/sr_ram_gbt_encoder/sr_ram_gbt_encoder.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/gbt_rx/xlx_ku_rx_dpram.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/gbt_tx/xlx_ku_tx_dpram.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/fifo_adc_readout/fifo_adc_readout.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/sr_ram_adc_iddr/sr_ram_adc_iddr.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/mmcm_gbt40_cfgbus/mmcm_gbt40_cfgbus.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/pll_osc_clk/pll_osc_clk.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/mmcm_gbt40_mb/mmcm_gbt40_mb.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/blk_mem_tdpram_1kx18_8b512/blk_mem_tdpram_1kx18_8b512.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/ip/xlx_ku_mgt_ip/xlx_ku_mgt_ip.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/sources_1/ip/xlx_ku_mgt_commbus/xlx_ku_mgt_commbus.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/sources_1/ip/pll_commbus/pll_commbus.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/sources_1/ip/sr_ram_commbus/sr_ram_commbus.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/sources_1/ip/blk_mem_tdprom_1lx18_8b512/blk_mem_tdprom_1lx18_8b512.xci
  read_ip -quiet /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/sources_1/ip/blk_mem_tdpram_1kx18_8b256/blk_mem_tdpram_1kx18_8b256.xci
  read_xdc /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/constrs_1/imports/constraints/db6_timing.xdc
  read_xdc /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/constrs_1/imports/constraints/db6.xdc
  read_xdc /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/constrs_1/imports/constraints/p_blocks.xdc
  read_xdc /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/constrs_1/imports/constraints/xadc_system_managment.xdc
  read_xdc /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/src/db6_loc.xdc
  link_design -top db6_top -part xcku035-fbva676-1-c
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
  unset ACTIVE_STEP 
}

start_step opt_design
set ACTIVE_STEP opt_design
set rc [catch {
  create_msg_db opt_design.pb
  opt_design 
  write_checkpoint -force db6_top_opt.dcp
  create_report "impl_1_opt_report_drc_0" "report_drc -file db6_top_drc_opted.rpt -pb db6_top_drc_opted.pb -rpx db6_top_drc_opted.rpx"
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
  unset ACTIVE_STEP 
}

start_step place_design
set ACTIVE_STEP place_design
set rc [catch {
  create_msg_db place_design.pb
  if { [llength [get_debug_cores -quiet] ] > 0 }  { 
    implement_debug_core 
  } 
  place_design 
  write_checkpoint -force db6_top_placed.dcp
  create_report "impl_1_place_report_io_0" "report_io -file db6_top_io_placed.rpt"
  create_report "impl_1_place_report_utilization_0" "report_utilization -file db6_top_utilization_placed.rpt -pb db6_top_utilization_placed.pb"
  create_report "impl_1_place_report_control_sets_0" "report_control_sets -verbose -file db6_top_control_sets_placed.rpt"
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
  unset ACTIVE_STEP 
}

start_step phys_opt_design
set ACTIVE_STEP phys_opt_design
set rc [catch {
  create_msg_db phys_opt_design.pb
  phys_opt_design 
  write_checkpoint -force db6_top_physopt.dcp
  close_msg_db -file phys_opt_design.pb
} RESULT]
if {$rc} {
  step_failed phys_opt_design
  return -code error $RESULT
} else {
  end_step phys_opt_design
  unset ACTIVE_STEP 
}

start_step route_design
set ACTIVE_STEP route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force db6_top_routed.dcp
  create_report "impl_1_route_report_drc_0" "report_drc -file db6_top_drc_routed.rpt -pb db6_top_drc_routed.pb -rpx db6_top_drc_routed.rpx"
  create_report "impl_1_route_report_methodology_0" "report_methodology -file db6_top_methodology_drc_routed.rpt -pb db6_top_methodology_drc_routed.pb -rpx db6_top_methodology_drc_routed.rpx"
  create_report "impl_1_route_report_power_0" "report_power -file db6_top_power_routed.rpt -pb db6_top_power_summary_routed.pb -rpx db6_top_power_routed.rpx"
  create_report "impl_1_route_report_route_status_0" "report_route_status -file db6_top_route_status.rpt -pb db6_top_route_status.pb"
  create_report "impl_1_route_report_timing_summary_0" "report_timing_summary -max_paths 10 -file db6_top_timing_summary_routed.rpt -pb db6_top_timing_summary_routed.pb -rpx db6_top_timing_summary_routed.rpx -warn_on_violation "
  create_report "impl_1_route_report_incremental_reuse_0" "report_incremental_reuse -file db6_top_incremental_reuse_routed.rpt"
  create_report "impl_1_route_report_clock_utilization_0" "report_clock_utilization -file db6_top_clock_utilization_routed.rpt"
  create_report "impl_1_route_report_bus_skew_0" "report_bus_skew -warn_on_violation -file db6_top_bus_skew_routed.rpt -pb db6_top_bus_skew_routed.pb -rpx db6_top_bus_skew_routed.rpx"
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  write_checkpoint -force db6_top_routed_error.dcp
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
  unset ACTIVE_STEP 
}

start_step write_bitstream
set ACTIVE_STEP write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  set src_rc [catch { 
    puts "source /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/constraints/pre-implementation.tcl"
    source /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/constraints/pre-implementation.tcl
  } _RESULT] 
  if {$src_rc} { 
    send_msg_id runtcl-1 error "$_RESULT"
    send_msg_id runtcl-2 error "sourcing script /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/constraints/pre-implementation.tcl failed"
    return -code error
  }
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  catch { write_mem_info -force db6_top.mmi }
  write_bitstream -force db6_top.bit 
  catch {write_debug_probes -quiet -force db6_top}
  catch {file copy -force db6_top.ltx debug_nets.ltx}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
  unset ACTIVE_STEP 
}

