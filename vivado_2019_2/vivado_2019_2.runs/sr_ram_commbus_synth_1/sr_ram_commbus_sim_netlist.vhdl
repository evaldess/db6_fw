-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Mon Jun 22 17:48:57 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/sr_ram_commbus_synth_1/sr_ram_commbus_sim_netlist.vhdl
-- Design      : sr_ram_commbus
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ndkVpimeg9xPfptxPLX3Amx+FYrZVEnHSi5gu429CinopLNnpwyFSDi1Gr/jHMoyOi1KbswsYzo3
ejKvQmn5gUOShjPlbMZ7ev7L+dG7odPNhYdyxdj4l2J0uSZTRGG0kcNhDEcmKOY92YrtIJ4xUt7V
LccMVCzOZp5TozKiiJEhGlkxczIQrXDng3P98OBRQYvlfaEgKFIEGpKvvp89uyA4lEyOxKPmdEOL
fwQCmt3nheTxfaiqRxsce4ZSGhzGgaR9IiOeA4oFO5Kr7lnhoo4H/fEuFjTZTkWCq6ZGrVbg/hWb
/PcF8LkHWufEblS4ln2gMwPwrCgsjIChkyMrRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
v+6s/Lonxr7LUJ4lcRFcMLlQep/i6oKmHSQt9x0QvO0wI0/FzZHDsUYWqFIcw84hSH9SdMuoLLdf
qB4RED5lv09eBa5mXruO/Xw7uBC6dPtxkSIVbJUfoMcPalol9n9CkpOzZg3A7P4bl2YI7dFF1hJw
K0a6XlNK4cG07ygCvRB3E7F12Iah1KBMa8xjh6vKLP978FZbdNoK3qt2HpRDT5JNyWkWMOVj9aYE
211/7U6d4mBsMkKyN4xXw7cBlpzhgLVPoSQBVnZpihi7d10beLiOVCaGZqj2tTbIHdXlWtWzW0lG
bCwMAtJO/P3DJelW0mO4t2Ymq8cEmu5iHPQKzA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18784)
`protect data_block
mDvN0tmHns+31eRnbq2dIP/sNoiT0DYJ/y6fv3WNj+tRanHB1WSStooVoJrXiVCyKe8oy1/lzWnp
/yr3XIxm7SpKqyFppyFnQsjnbC8M3foDk7+HdTFpScjLh3zWja+hIRQFPcP5D0gBrs7i08G5Ytzx
b0M0Pjy/cPBXAxcY7OP7VI3q1yj4ApLQp4bL7V+DteDptOSfdDSFelehFPu8FIcjrLYVo4D6r0Po
Bcatd6tcLEV9c3pP18Je3n9tqvIcX13dAYEUuFeJmfE61HBPuS44xUI5Tcwl0JMdl8ue27CpwLAo
bMSjnVRrlcHXwhn+7+Nmxjiq3qmw3MUXdD2ks5MNXt12+ouWL3NIrMj+IFrszqdectEtpUMdCt6u
6VievkmTTyXt3IzD9QydPCCs7Btlih0eDRhwAH4INDAVP4aluhP+OaT5Rs7sSWlEIMWxuWdj3s2H
2nMNLLqYmceo4XCveiWoAr0SaQFuDFBOR81x8ObPnzvpfXYrBZvCaCZPj8EMa5waDzudUH4Gr8TB
wXpqFCtT1wT6WMzVioRWRfxFWyIgNM/0qj1cEJLAEV7ehBsOLUrQfEd49ApNwtMoqA8NM6HC3ICP
r+VvCUm7UOyczBFU/25mAV1JsJjnPikUiKdTT41o8uydH+MjuKIvKCav7gV0nS4PuHNeUN58koXF
W9/zcGsplrjPXzXPpTHJMY1wTq1KBl8lGiCYMmSEsNdEfZKMgjvfeU1RQNU2VYDkeZ8VYkdkSzJe
dLPdDSbvButhZUjhGjO1R+Hyrgcx2tTjjWj39ATJJgAQrLec9ua2Py4J1u6+uY9QaLz9EkelF8dS
k7PgPTBL2+GKNBJLw7uzpBHJlPkMy7Pzu025pL6XxV5BrAfIyJar2TTUJgr0n3OHhitkR7AkBJFz
R4/ml2vlP5A3+aNSbNQMAGYBRRhLnHLTGMYCXZBQ2VYevpXs4N+CyzfnzBQ6dfirQbLeCJiL/A66
OXDvgoEKtQKTmH3ptKg908FPh7oRl+9Cd+dv7HYfQgFX6omuBqg0xN+maO/7cd+/NfAAtSRlEs34
5CWcYhuiOVMrnpfddRjPhersJxiKT5ZclueML17D67ijie5qqgCkzU+foc0bekV7SFoT65JDMSR6
QzS7YMGJ6atOR7IlzDtO19yIng323VdKiAhgifDTDymHe6Orsd99FiwWv9lp/Q29Z3vC/eb1W9VI
HBc48TijMaN2C4K1UVDK9cGy3X3tUH6teQgwwoO3DEyLjwJn/jSzkwyu5xwtbJF1/4PNYx+c9yFg
bgvKzVf9xwOr7ZIe81mid8gH12z7RO/U+RWy1v1uH4qHSuvTU/QiuFmuvdw8arDcbLVbtPeBUyDk
RGq2kC+hlD9wkVI89EpYtUly5RaWG1R686SbYuaNnZNji4hHKROjdQosvLw3hYWDccqvUu3wObhI
y9I1HRrSnYYaRIi2x52NXb+DPpKfqMRmQLReCzidcm1IeLY32gdryWWt6TuqhAmoXO3kOdYqyKGn
kzmPzTt1oIGmefeKSf+SLqDIwxu7K+XjhAVYef0uIfswKtKVVK0J2moQ9cMZZCleIUENa1LrlJ4I
SNGywPVb4DcwDeVn/infY8KV6wDQ7UHCIC22BJGZ8cktIBkucpsGz1IEJhEZdBaIuMqF3u3ztIm7
+USTt7IR2SYE8HkT8yLqhBDvUUIE+gjhH4RRI7eGtJPPE5skZKq4IRvdJYFy8W+8+CubWnN4hc+H
fRGpm+ZSAb/WOt2XGsdQQi+1+QTyPJlTx25dxoDyOXu790uEBzKg7xOdDClHyX7AHnpQ6yGs+Wo8
DI9FiojfslmN+Q/3MgMseiDzABdA/zgwlfIqrJ23MPETFraysxMTjA5hbBmPn75wV8mbOdIgP7qF
PI3rVplPCrBS0mwd/HlayKrBD/r/qmmEnY0LPv1x5GhfzWXLn0I57cHCMI3kYFk8VhhwTtj1gvmr
wF3uB0Mak29qy7itGfqwHaVaT6UNUKOFUVZk0Xj8G5Vqqdt0/Okrd3B9nCcXt5/w5e+maO7/Z95v
77YE90pYFgSfPahsUnVlU4XNM0O9Fo4rlmSe6M04cdP8kiUsS/e06Tl4RiB5H15GU/dMCPQORDMd
5Ucj4Sic7Dxo8HL2iWuhS+A3yzu+0LOky5O1/BZuqFpfRaUzkoPgmE8rGlBC2QO99SpDAYlRHNjo
oJ6MKnR9AOzt+cX2+WFe8S9by8MLNF2ZAywGdM4Ij6j/jxcCPlR8wkSqYqAc+ccbMUs3LyTndKFm
rqWf8pd1k1teoZcwB2GO5yWotKn8KF3mE461anQuD8Ui8e8q5fgVOE2UeKj7cyPLpuhBtC9/ct+k
7BjNRfNaA8OW6t1heCktaltbnkyf2nWoIarjVa0++pTagdK/OmA0P0ol2Y4+ETrn8rfURLkhH83y
MlR3raWMip33e6pCEsp24JSfXcojFc4rYCI3WvfsEjr65fB9IiPg9AwpG8tlyLUsCeUXStZ1DW1F
Fi8/rRkLTXXYZTHputbvSALKf/gflhsfUq0MwewVhLqx7i7+y+vHjLFCeTsnj5wE4Too4tBQ4kAs
gnFOcxXbUFT9v8PJEcUUyARYqqqDn7fm7JRKJCVtIyXrJM5pAuUMMO6TCQ2+TQG6aLtOPRsKh7s5
PkhMT3efcBj2HC3xyADVRACIbHKhTSvb+wW0P8OzlE9gvQxXFfKmKfwHe2Hxwok2h/0EGbZe4aoc
0GAO33NcDK4RjYx+pz+soEs+YwYD5GfFq0zLLY4Wx/N8AhBiysox0p6lgneU6HdT+cLYGAB16Cxt
8fxdZ/idHSlx5ngMGIpyDCSl0p9YbTIHD/yJiGCVIbtrk85fkfn55Esth6EYqJ6idfGsjf2osPtX
RkrzCDpWa8q9hV5gbZ0DRX2DdZDcuz73FB1lIzCaFdq6OxB2mlSgrDvMitHwnovotgLjVaQFfPll
hzsFTNNiJTfwQEAA9yCp9YCDy0iZYW6p7uefVEgEp31Ja0XjIkRcJoyO2skQUkFdFsbLlF91wzd1
eNMegGnNmcMZMEtech9nnaYFpmkz9QlKUNzPsWkxzH47epxLOOcMP4NAHJBlj5XAFxUP8Zjdd1MS
XI8CwUHigmfWGE5gZuY/ehvHgq3ISMv+k7Qj4+noT79xt8Z8678iW6NizZlwgnAuviuxARBqpq60
9PYE3bPgxLoygvftaIEwOyLPK1qwSwyrDLp0LGQW7asu+SXJJaFGycaWBJxYxeL6VzMg8VRNuwMZ
zVAZDc50m0VxDtoQhaLI1C4G/e+9FJNLaaQIPibksaXukT6JT9sNz2CJnHGh7dfMNBOLkNprCTp5
Bb0biV2wnfAqUjbTzEqWzSgRFsRGuG3y3VBBNSj6ivnz4QNRiq16Aj/ALKb6yo59uK0te1ok5lWy
HT+/1q/I2p+pOt0XkMtlH+cSyKo8GP15LTHrCACbrzonO0P120q1Fp59by3zNXzhwAwxty2+21TI
Pa6EKm0/4OWtXBwbWCL6FAw1CDIjUj+wRFZcfcGNQfdlg/ryGUkVi43wd8q7hmouc/x2tMkvFhPI
ZZYq9O+BWKwPXAV53H8y1f+W19WVroMsuJZ651+Ulwq14LI6d856yxTvfV7ZyqrKE7TCL+VsKcTo
RDmQRcqUHH6jUhHd8YP51ncokjH1S0PuB0h8OqD894GTOkSGpriNNxs6/Xf7bTVjgeCezG5zMu3+
Az/R1B7fysbJZ1Zf0T/pgWsU+pDeleRfgaGmPx8qu0NDQlhtVcg28656AlpR0bw6oh6aSgWLAfRz
wzHGD0JcbaY/ZC0z5A7WR1Wi4pWaaMtuWlqSUwwUnQIHkto+0AGZZl72YJAZ40c2dEK8P1oPkYI1
9Clk1ZAOC5BOnaXQy62vWFgVRZMAU/fgELGIZcr2vtuxMNuCNbP4ugGtxCeQtYa+7XHEOMzJTjl9
7yhg7MbsyMWbWns/c61bghnWgkJ6OmG6YRgkZWZbv6ra2Bfy4Mwl77iICj2t4YSCfhDb/nbPqQe3
L1kRWvHiYdM6IwOvjLlMOYRd5J0UTBTzZ8hk0rZpqJ53Zf03aG8I4o8fkz26JmwdGvpbwUKKqNX6
xXZl7f67SxDGqi0ikqj9PauiRPlGg2iGW9d971r/LvuoflFn36nJmsB1L+9R638grb2M5qm3DEWM
7g8YYt0CGyLLJ/q+MzXFN+NT75ycraihMA3mWPOdq+f1r9nrNtZmil5NVrzEhObRPSvBBYwd7fdP
u7haLRkvuW3KzUOEDckJ/Ij2hSGZnD1z7vTnoYEl3SgbxintJWtgzUOY2hImMLPLKqxL3YictHX0
DqJ/zGmaWP42W4IRkkHBE9BhzJfhIp2cI7QBkFEoGxlDhoosvdoUlh93E1CA2Em+vF0m3+/4lYnf
NLS8TreI+hztY8nIHO2OOtac9MnfLEsnYWNT9qNcKYV7pNLIhjRaPj98WYu42H9Kq+3nSoegAfiW
GNJhyPGBU7QneTffYtYcCeuszophGWsUFVsZTXsODrIpc1WRtmUkl3z20wN1Ayg43B2I3WZ9skc7
HkN9Dcpt4U17PkIhyQRmNhGRWNsxRYn2XJwo8MeqRKNoeSCpE07P+fMNBvxud+OnwO9ARQByNvZ4
3EFxSU3HgO8INTkvAYnoXXvDx7rCgQvT+fud8a2iJI2/KpsNc4VRF4bHuZv2dAU+XPk+Z4yeDTJU
/tstBiyxB94yC7B4quHxyKN0hRUXbkDDObZSLFU4AOrqBAd9ONg+RvDieF3c4QVAtjVB1acHw6yo
v9OygYSqmeGaDcaV4wXmP3gCJ5rrbkvpz2NakT9NxJU5k+4S0+KlukZc4EQ+eYgfhptu/oH7lsG1
AU+SJ7rKaI5pfo+N1PRv4YtRVhRC3MCpn9y79+VSNGGd1v/5Qm+xJaPs49WvDIugwRQ3CRjOZ+uW
xOY+2xK9tLVXLs9NJDFvjlHkPlRrFFYVmo2h+SEQVt+uZ/yqblX9FfKh6X7db+NujrMnJfN6QuT1
MAMWgtMY4sd5whTMG1p3xUHQMIcGM/Sh5eF7cy5mG8/H/VVE5YzrzChfxHsXNzgyHQAIMLTlXH4F
JkIkz7CMXsjN3aKVc78zVqAqbzESDO8QeemaAV5NwLk6ylDe2aXwyOAvVvvhxK9WF/nz+cweqPzK
c171/SFVmxolwalneYrEaNW9vL0SuD4Vv0hrYma/k8eIDSl2BQ3C6CgVniWxrI52jZswX373GgYr
eSe/PeR98I7xouUj5t+NnI6gnXQT3LEvLCGnduI125jVG6JhznqOiXxFTBmfZKDYlQMaf4M5H7A1
rtPhO5Jv58PGRUuKBfS0IVXYFwsT5lySgHeHcvyzHicrloiD9OF31uNOpNftDXEBUQZoUOZCyl88
zBFFb5E04rJBmg8fhCZAOnBimDqrg/zjMULxy2mHmipmwMuo5M0Lo1n5z0Rhm0hlZmIF6iar00lr
ux1eytRPh92zY8YkJhGJz7J6ztluv2fmw0hEQDpY99zN4AwXmsU+9U99SMB2C4Egoe1edv+vy38P
W3qO5XYK7W8myMod2u6hlFJQBjozSPyYUyfAUgamxxFT1niv+p92A1wmay+0iuckpzUa161mMQkh
gtwKUsVVh7d8+IGNdJcNpi8+78blbI8wtbrGJc9hyLE4XsfcG5N3d7Mw6muBqVz/rntSqYNP+Vmc
NA5N+AgoUoUw5WoVfeDtuJIiiNdwsbQvRfdidA4ju/ayYjoRo3CfE4ejlnMi5ijaIjpTxK2NdL7G
eJ14bReLxs27qzb1sgDYKNqV8rkyYRgaoSxnMPr9JDhS2SaRU2E3iHv9neg4oyEu1ImMlWBal1ff
X5lzk2PGqA/DRKzTdgeCwl7MS9cADyjfjE91m7h7DCYrzRXV1U74FYrNroQTTMqaMS87llIWVd+b
QOmF/PyXsZBXTWZjfd9DnGUX7B3iV/3zm6RBIYrR5Es7zT2mqlJZ1q1lsoFFhYhwfsrfVAmjenQb
dMziBMMAKRlIvHrHPqPKXEwJhYSt0hvzIbSnLfgeb823sOdgRKex9el/qVwGysdqLU290V9gejY0
WK6TGO5XGGjEB9r/D/sHiLuJwy9TCgtJVlh7/4mXeBCgjXkN1BnO42bLUQHpIYw98TNr2nzkKpX6
mcGjdiSStUNJG2gJiXGjd/Tw9jCGE7OrUi3N353aYJPwDiXwAVYhNA0Gcjf5G5jSBpI6URjs8K5n
bB4sS92vKqr+Fsz1q4eJY5DUhJoBvNYeGXVU94GDmX6QBJ/09A2WTAn9CTUVfSWRXdm50kU1YM+I
8Kiet6T55HUA0KCmgK7Zidd56w7jctTQsU/0DWPbvbeOuV4SVXeEaMnoF7et04WFkA+0Y5Ani+c3
PpV0mXW27lKGXJyabrChgPBSnWrfFlzg5Ka3DAlDxDST23CTpphTpaQ77NYsvF00Fk3tdtzsD4zp
5qX2iN8Gvg2Zpe1XH5pES707O8oyA1azWP0LMcq44bhsPM1h8aRzZTw2aNV6Yk7gcB+e0tnKlbEz
9XILQjagjLW6NNnC1fWqFzalhQbLDZvVMVIXebtLsQYggOIP7Ux/nMTHL9Cfw5b7r7OPjRbbsh8U
i//ayupM6a4iHl0nR+i7i813Xg7eaqlDqI+DREtugr9PfeGZjQHqDTxZDqDR/HIxPsD2jNR8Swnu
9yZoVW4PGGBnr+BsGpMNlVxPuZ+P0XY6erBtq61Khai2vNi28daXy72HyWNwSKNwSqbO5eJ54pFb
PFDOoTI/uZ+uARsDw71IGxYx4+vLgVsiIpl2V6YMFlYpLO89ZZVzZm309LWaIEPbo9MA74Tyqnc3
iWr8QLfIr7txJnsuVgQg1aRc8VuwlENjJ5ekgJG4mAGZhdyl3vObh+BJn75hrsEwAQ1AIitkUbkI
uOLqZgjZ2LBVRGdCkptTRamGA+PNDBIgH47ncuC3NAvsQ8+KNZuqWtGIFQj5OZbwsDjW9/1HiegZ
hsbzL3khzs4MNG8IqJIRk7ezE1NOmmzBhJijHAUxkyKCB5IKcmIoEX++P2CWioYvDqtrkX6CcsE3
IAtIajcZNBSuRKNRLnUgrhaEMRLiOPxN9G8Twmj3WOhVEGTa5LkrjZuvoQNbWZCaKKXHbL+Avw13
8oBFGfFkGa3u71twTQpF/aHnNBCVExn+P+iGa9CpPNMX420irjK6cBRS4gpegTz1If1EolN0phPu
9HppXmxe4tN+GUnSFh7rJAZcrb0wfomMDfrx/0FpAfEJhh+Ga13JCXBCe5cPL3h9Extj1QjCJX3z
ObYYK23o6wmDaknkiQdiRHFonycg6SPmGCtyz5BPCJnAPsyTokTHA05oVCrWFKlS9Yl36W9sT53M
buD7PFKiBmgatRnrc8HWyxg7zrDMpONthobp97MGEKY/7BE9PBmlp6qLuBduglu/kbUkgdyT8TXQ
P/IKlSoEaY10PTk7tl8ars5JIfgQXoMsJDEX9SQk37aH2vLRLTgFZe61yyjbZNSy1Om8vODSLcBS
yrMFJerx9F5N+1i+N6mMc0qTc2/e+/uEvqLELljDCM69A+828Qri4Grq6AuAE1f8xCg4lNhFIz8Z
k0z62KvhKQktIfhZ03BOTZYYV5VVl2zM54SHC++I5NXoHORZauofJcvuktCyypddGeXUUx43wm2T
CfFpQkzj5kfm1wsq/nVGmGTYa6MgFQXYt1GFlBYO7vcKjzDjw8rHzHTHNnlbDxivijDFI8HxneFj
ZTGmXtXDzelTN+Tieggsz5r7Xgg44sCRQXATJk5Zog887JW1iL8QNM05YxR6lJiGedhIZzjXSDnQ
bciIdDK9KF6YXE8va4lyjEn5JSG0zyHIS2MqOOryx7kxvaTgddJyRQTVfc3a9dFHQb8IJLQQ3Erh
InrxU4Ast/1cnPLfMJwfQeumme+HuN18Z8yc44dJzQxnQKHpBJ+S0BthAPwCYaASCbe4aZh2k8Ml
uRBcuV47s3g0v/vE8Wilux+g9dwMDSHNozQFMfRt0hftNdZxk1z4L3TkKD149W8jnyxs/rLRqADI
qfs8yA/vhoiCrLybnspqeYLJOzLyeeeLRPXf8wzJp3eFaXi3VM0EpZaZjbMk8xttU2reQHgwPxBh
0r16WF35347N41QA73+IaAl0FsVtuC4kKyhKGFKT/wLT0rxGcqCrCnT3/EQKpggbxnbMrQzcXR1u
lmpqyUslYahDjXtc/ECGEQf0xqQsZiZKsaSk+RWvxqLfJBkSxqjDTcs0jivBlyXCZmTM43sQD4Oa
J/VvxSo8SzPsM/IKlGuqY8XgpPZ8byswENKD7cFAEwCs29dw143lpg0OhV4qqRZtxc0bJsFawEAx
gD3rmhCxP/PSHwvE83WMVzU/aWHEIPidDzLnY45nizan3W38ipOclI55WwduIxkM/2MgpZXmpmrk
o4lmZKJj4++ZAYz/p8hnfR8is4P39pTfVrQ5s0oADvgVGuq048ARrqhX9TnwV0ZM/uCI292SN85u
Q+U8/0nOxLH9LYw7rjnxA1AgeOMRlOLhIxRXrE7OUirqRH5JpB40qx/5sm3uaKhH+Dp44JIJhqaL
4EBqOlUa1b772kf+EYiCkW+E5H6DX32T18f2ju14YKEJHMYXTiWLosRUt/bQ+FfaCSbeNMqY+o8N
fogHAS4QYRXEVa/n7jZnuYCO4rTkCjmysIpc443lxrgcBwsJKcJFSgwYWXmbkfkfCQpswrtSLzLB
gmgatosdACT2qcYnKhhbICJPp7D5yNcWWIFUz/ODoLNagYiFPBw4JDpjyXFpQAXTXezRec6aXXLQ
CBA0E5iWz5PTw/kXgziY8V25LvrdPyev0CZ8IIoEX12zFOE6W5183qIHEylWQnHb0bTCamw/Q1KS
UKS8OW3iRVge/3cUIeuQAftG1367GZv4B8NrC33J7CzB3Xz20ZJWtL67HYrqbtHn//3Qy6ThYKoI
NSU6b+vsREZOT35q/jygzsA5Wm2ni8YMy65zG8RV89koYhHnROG3fnTPiN4W6hTicky1fq9Z4Eea
EBm2unwf6Pe6wtKeBPGzvzjyAmE91x5U8BCE/oHlSfQHZ7UG4BmHLQtuLIBDfO9pxF17h+fkrURY
PimWm3liSUhdrG2nXiupnQEYAbKjjU1hJ9k7wNF9vaI8AXM+5WmheaJcVo08avlGYtVrVRqqxWFL
kIrWKBr4mc+MAQsJh6CV7hULn0INZBKxbZ9OrLZyENXZ5f5Veb1VIPt0GJGVeMmYiGWFepOatV6E
fScWoarzOJetKJ7QsDKfsXMz9X5v6jaDl07il2BDJbjEOqd1CKefDqffISv4ZlEnyVHxUqJGTxc4
J6m6JteiyzbOc/kVx5vU24OuN1eahzqeC4pA+KcxD6OylhW/bpHzqYRqQX/vXrP2DY9fL72Q/TTV
ojycJ3O9AzAadUOn2qat1/pfLBO0qZzcQg+yzVi1vaSzc7sZvrYc9y5wduANW1ymIm7NmhrPjXsD
fFG87BRHTX9opTuNUUN1Nv2UIqBjJPC+rOUA81yh+qCjbaGgCOO8lTbrATY5nKIi47r5R5RupREA
ez34j3E2mS4ohRDeZ5X+MxWZmuk5TXgRjlWHpequzl01SmI9gXokaDg4oqOuwrXUqZHYHJWBW+n3
ENe66FapvxCreCgbdP5Ll4UUiSVpfjZKOVW+jzZUvgHkFuMM0WMuw6YnxJOWZ02gbERxNwstMSWf
lUT/yH6DPMl1zwoyAG6i0MflDJvhemXo9M4DzxOle143XJDFjFMgzq1+/BBP6aI/s/hNhIrdF9me
t7xVyQVaERKQOlXJdSSHEHQ6W5SQlxGCv0ryShukjdM9gjPVQkmVeKwfj/Xvzb+m7UMrUIa0uoMY
2RBuXfEaK5eyQP1R5y9bejs0nQasXdI9t4Ec8vUz2kEo0I3Uf+MPtPRK6zsF5oerX7h+siE0Cbsd
lJhOXlhiOJjlHvOuyrqRVCbOlVHGtsBk8rI/S/k3P+uFtwP+QDeI1Z5hr8UGracMkv00re498YuC
S4jLFUNuGh18TnxsrrVXKTf5OwRQPVPwy74JVWMWwpoEsjW13b0N5FwdMlO5uTY5hQoo44khVpu/
O840kGUd1exm8QlXyzjyIUUqM/p8qbwCwHgLZdjgcFVqx+a2zBUEf/4a6bPdjfv+0rlsCAYzY+TV
Kzl6yMU8orlRGlx+m3hJlEiCFv21wyWGF/XnjJ6hWWPxyanc74yQQHhYj2FRxJpF7NS5jpOT+NrI
/36HUV42P1xHNyS3A/aTFYWhM9Y5onlGfO5QxTLn7Ns+gPlev4F6k0IhwtKVotUWoR/ZK/hXGhfy
sr524vZCbPlfinpDmo2Y/LGh+omNiy5dFZkWzGAp1vLYgQZWc9cXAHdJKSuJywILIHVnUGZEeyWW
tujZNvfGO79WEbBuI1LaQkDJePKK4rojujRE4GKM8YbVYElgN2jafe2aDZXYisKyWWwUusy+Puvs
C6NsueHlz+Hjog1kX6O7tBuajQB6hpgQJG/3niD+eqgOjizVMkHm1G/ilRhLOQ1pCyIy4BlydU3g
88jzwgDMjvM2Nvxow/gqtsc+E67A6q410QPj9reW0cbutD8cW3p/1nIt56MZOO6KYXaCGXt1Qswr
+O83j5N/V19ehGOMXS0AlLxAF2a2sHMI9asv/fguT85rA0OE9WT1ZYpsfnTNXxKw0ubcFHAibSpK
YSFZ7WmT2INbDeLczpLdIBBQGXX6zVOQHQOpGEKXRKBWpifcYAbDVm5CGmvesvaaQU5LweUpFmRI
GB5W70T6FNGzQcqlqoAbF4EzaXooVzVDA//FF067kGAQ3uFgNkSeY2r3VbiD90R4hVpITEfn9c3a
xDbP1tmmt0JbLehobBg7EAPTiXaYb275U/V7W7MptXW6ni9TCYI4/oIAieJIn5HCHN7qbFgitS3d
4kV9v7j9pZNwqMPnOT037LV0mMrQGdA4KnYQwkTlTb7chDZHeO7nq2Nx9dIXaFS9YLsfzH3upjJs
YlQYfNZj0M4D8jTFVNyaqyyWMKlI1pXvyG6gfoce/LzKJdjPl/LCf3cXJ5xGqW2eultnR0H8IoQW
/kBX+n8x/CZmB3aTxzCFiW/dNI2Bo8N6Zeew7bimqtkIvOkbe9fm2Tci7iOBS0weGBB5TvCdJgnL
OSyE3/KgkYBowu9imY8mjE5Lg2zMJj+HlCps7VzUVhtjBQxZknQDkmS4GlwwMlfILC/xg1oDU1dk
JBuhuaDNME+8jcWrfdM2Q+ko6quJD2surgZcRaLIQcXv5xU2aQNtM0U4I4t7Y/KcMkt/WceJR0ei
lU2vvuH1mToxGR6G3aAc91jaOTfmpem2L6NRX3ffFcw0y27imdSjrhANshG8TQw4H3oZ91cQUeRG
DfxyVHnwmDNaYQGxSi2INPJZ3CbiNIRSz8iX9QqiAM3VufZFVRbclYSN+fwr5fxs/9uUuFcHCwcs
lUzenwhfnfSfBl67OST2rF4VhXhbhp6takNR0pf3mfCR3VXVX5wZln/AH13GD7VJvbDB/G4PPe8W
KkzVx7m+Bz35Ix78IqUBXXvIpjurg677KkXihR72jmcIExB57VmQhh6YG6Bdf/pGqOPJmhBw/IAW
9HDnFbe4jxWyqfxzu0yQtg73SU0WAZPrCqYzDcFLgubEJXCdf/ZE3HBitC5itKjgNfut8ORnIcRV
MhMdKUTpHrEBXUx6ch9M3eqbE4WCRkroPTlWExULo0Y6VFBdt349FkZuzObDhMcKWY3N211ASzv5
V+GPKJQ4wVbLLaycDYdfBDIMqAAAbSKd26WBvv6wjHbhGHvWMlBwPK5jidwuVGOmMiorTR5gSz/T
8whwEDaKz/rFgVYPVvBKuZUYwFD0qgHdIJZKX58F1NIIWE97Y9rQtFrxTLcaNSmiFQsXaiVk6Vf/
SD9T+WstF7oxLeuYbzVVy854os52OR37tzBhZo3Yi3WE/o7+YfBcFkK/d/Ycx0n97TjUVDQePg/E
D+HrrwVolJi9XNhFDgrNnB1CImbZw3ue1atqBjycXfrjYlWnOSrWYSU8wBEg56KQTtvlyM/dXnDI
G2ZvnAqUc6JtDEW+QWo85B0iIs010nBKWrwzTK9VqUEPWDNbnHH867AZc8yf7Low/wFbwOYf2LEp
06FjCnc8DjLoW8TQj1sMDv1HhiXOITReKXoifiOIYtK7539PnzbJxv4UtIxi4CTc35PV5OM7lsPX
l10Rb0CP4fh0unocj1neQvUn5YF5oBw3+2zw6ytAG5P9vmXoqYUWeX7zIzO8BrxWubrKY7iPUC0N
9Y/CszVd9YqS2IXXzjx7j1XLrV3OB4ArHlt3yMT8CbTewJQQ4TmOm1fPKfySdLHXlLEbxwIfp+2W
W+0ZhcW7kUosV7CZq48Hp3uSXFOG3IM2Dc4BopD3kd7NCyXa/1XUPxILmdvGvtGzZbdgYMLPSLkt
AQQhgbx/7uCfD01TULPa56jd1PBZpCk5Wg9uPemMAcdSAP/DFZ6PlJDzNfqMj5hB5aNs+FRaDGsw
WtY4AeAxtpdY2hn0RyoYpZi2vDmUbj1FtZYNXJ4BCySM3/Z2L13UH9UEZ9w2y1bL+LYhTmxAICj6
GMjfTFyLIaSTEkBagwhxpPU2nR+nGpryv+tKXkf8ZdccV6L7I/7tof2Tm3JV6rWW6cnVxXGW1GWZ
I4qpFPcFFglhTq3B052VbmuDZSkYcqXVDJYZJDWLe59kgXKLlUjZccjtazCdFQ/UzGGODnn4FkTm
aibEcDFmom13tn9CZSj8IMbss+pLQ2zM3SXOZD588TiVr/pm/5WmcSNO4QwFOgbZ3j1qgxQEqDK/
m4kMcP1yduzUIvvJQuYKbfJSbKlp/mEbAuq9euCbJRuXovFVXCDmE//u4T1LI96QIeKDe7H8QCpt
PXM7XIAUKXK6409Wyd9ZWqXB89XYH2kvd7CRPy1i9PUA3X3tass7so+TwJ+gek0GjL+1wskZrv/0
7EhP8VXU7byoljp3KsYsuLVbUCpUqccGRjSLT2D0TKznlrCU5Z9H27US3IzJZmQtA292PivVuDdV
t+w2MgCpWIAWJPxwY5f2JBe7h59OXHB/Gelz7zqywSrqk0fbxwS+IPXuEaNoZGxqFJ0dT2cJ7v6g
RKNLFXHJnj0UM9Qe1+WOL6IArH7ZzBxz8IVfzuZG/BkP2BTUrAFVXND2o//fdTrJneK59JyeebBf
RufnT14Ah8HN2oYwv4UJBf9TvVgPNGynJxAJ38QTtfZaXNS+tjOEjMeymthLp06XjA8FHVPGbs81
+SOE0PPjUOfeWQTcaji22RG83yeZdYGn77X3xpQyLJtRWYQzBhgNqHnUUxTlR7Mcd337pZltNR1T
m9LASERxg6tqafCLeP0Qeb7e/nJg31u/sLxW5zsKrRbPuQFRG1QwKvC3CoiaQqdC+bjo+3rJM3lq
z1PquUD9xWPYYzsHX50+PgwP/ElOejnDEG5RLpUXlOzfoMqbA3Hs/EzxZvGlwbygg03VkJBTeXVr
aaCiIF8xWoEuvhUzKtCnMnV7McN00z8jcjUtoGkcWtnzEV8T4gMlwLiqvTZSKgJA55uxY2lzJ0wZ
eVdbrHN4r7qPOgsEShvgHRUNNnumvblSH0skxTQdL/UepwrwP1CiEsMep2rAXHRkVU/Eytd2K6dI
U70VcqEPF9FvADXuKHTCygj0zmNBvvEIY4OQ57SjehpjAydbpWdx5hnCylOf1yhZlxsRANQb9Quy
vFwz3Qb9dsAoX7SzDAv04JD8NdqzRD1CKSQDAk/q54oBIrtld9dF8AadQxnZ2ysW673v8kPHERsP
XmCFrxPBnqhlYwW/KyLwMEAUVUQoJrXFyq0DbHibo6XhccEYNcQbME9tztlwS5mvYZ4OtHUkUaMX
xKsXROGUzkNW96KGwnl+ULGF3GGznM2ykJHeEnz+kgNtuRGCrJrwa8xUeb986xjcmoRjk+8mgVFZ
65Xh1kkeFc2N5xJws9aD59oc6JbB7j5EYSZ/oUNqLkuoXJiQvKcd8aVaHj9mARdgTcQEMXiY8WhX
jWSJtVxYTuqGfPpamTSDEedgD5Pz1SL/reaXJP5xt+YZFBFIN0S9oy0Bmz1qxUOw8+OZuO62mMsl
ZBep0ZDu++5dBe/5REeKx+4q8mDefAA2gZzmf1zATSH6KswbXS0yfrIvFNCVdvT9UCYvKY++EsvJ
xcbuDT2MWyTUMi2WtWy1QgwuoCESKVSVFIX7l1kFsLhmWBZRlEmwRqow4aSZt0a8G5x1Bgpguz3C
o23pRU/YwAVXoUvWUwCrAdjQWl1JZurbNoNbc3AXrNFY9p9NUtu7ja93an+W77e0t4Rq86Q8Sm5g
3EYMj4t9HoDx642b2KdDn8HLqQw7LqkFxSrxLtpsj0BKhheNC30zpY73hoQKFhE7uhiexwkD2hyI
mrGx4Ofohd76ZssEH6pvJiNd8e6tMJ67UXZt5XjZ8x0fxkFaD6o/vvATuLL0+OGrP7USEOPqslpa
al5PU6mT7QSz2f6Gy8uDcpukEcDemNumdMrh+uJ2VZT277tRVa682imfIhZ5zP69ZuiKOwnGAKfU
4OhIz7y6Pn7q0lCQ9ZSvOsjcXM3tO8eUqaIwXfvZIPjXNNJGIFsSlkBU8o8ue5bj84HTP23mlGvE
mff8EGuAjcw/taOScgp0rSU6b323reHfe2ckTD4w3YVbnmBdgxXTTnfQ0MHii1m4l5QADh8bPSYB
TmtEeH6ZvV5yxDgh1EJh4Emu/wLljW6ZIJ/M9CFQemkFq9mLRY5lN4VvFFamwhU3F9bSmCO1yaZM
qq5NPws87VrLjbImVwCoyDDBxDuHclthnG8AE8hZTolAQOh3hg54wkLVZRlYntneaJv1l/nOzsq6
ozl7sZH70qqWiLg/ZMogxq3mR75smZpcPbzKqhcG3LOIXq0avpTLheIGiRMTNlu5aLcyP1IWPYuX
HQo4cusjUUk7no2ejPlv9mDvpY/4C8pgb05/zoLZSdxXKkFwPWZrKLI8s3YOseBo7aCjT2l9yvkF
GtnreuXrnC7IvX0vVknT0mTAdHo8V40ka37vw1DcjIwf+eTEBB6Kor16zKtxEhxAj02LpVwOoBlh
DiaArTBl5EfAiAFB3Jo+7AKsQjAlFKouJNsN0/e9Ayc7FFmeHC7f1fCe+1krXNdWREsRvsi95oPU
ypV+V3m5hDIeGQcPEeeQzLKqONWk+FPukZeL5mZ7DyYzIV1dnhkVU9u081NMMVkb8Y9ztIqcoyc/
36ibF12aQDWqR8i7y2LoJGBUfcUeBLH0blfsStrdZ8hE4B5FPEUxVKKjn7klL3SzZvmebdwER3cQ
OMZa5Q3ivCox0p61VRMe9U/D/yOpEj2MQFXLglXmXksFGwSSKnhkO2UfFhudC22O5TuupGksTB5K
OWVdYQyt+Q2J67KB0o5ARnTTMcOH/oYKAbADYrwmOikmXWrS4ydetDSuqX9i5tloP4Xm8b7QzTpG
RIPWAtpCJJDNpVsckOPpioRLZXzv/ys09qAwzaWQ9OPsc1nQERo5oZxzfZH+aB2O3+Dkvi+riLmu
+D/fZseIc4ew5iIp4rucQGv0LOkymvX3IH2ObReHqJyE7m8JnvBT2s5CFuQb4S4zlgIA/IvP/85z
gojBOPiFM8STSV/rprJE/7xd0wxUXp2XHzo+nOpVNZPgcNxNiubU/eiocuU6hH0wTyaWdooWauhE
W9V87quk+2dUZzlg79zBFhERNaMQW2q2XzBdX7d9VACe/EC0NpoPUOmZfFzhoem+66DBgVnHykMQ
OATaUhTfLAXCnyh24AwP20HTMDzHz5mnbd2OV2FQefAXwH7X5pRqigwClvIZqdAtDmJDcvfAAngd
I84/j0Olsjq0gNziFz8BPHR5U9di5iIwPMHKyh6S6ZqWQ5OVvbVN+9OP03lXA4zQo64iJLjxoEQn
vDGvwuLwstkAI6TG5KSMrfri2DA6Rn0nPEyrRqTwvR2Gex8EQ7FrK7wjnv/pbe3RgJFl9hRk3Ykk
0q+v5OSQTujQgesPz9m5qB/mfQjCyFy8yzfXZDskP5Aui1SVcktvMbTCPMSqtYisBUV2e5JA57XM
mbV/EgzqL6HrEpP88etMzBhjStgFoMQhP3vChmoRQMmbo19WsBoofFWdala/dsBWt+S26LyrZLKi
zL4e+XFYTA7sbNERMKzVsfImIHOdU1wgxF3wfMpkQ+G2hTNb9zGsFi4NOCQWTgTHOQclp7uvk+88
4sPVZH7+n4otzFVCz7sln81hvUcko4SPb8Xqbg6rHzmPg2vJ3wGMe4nHNd5aUGBLcWWzZ/NG8RWt
Y2dqVdQBTgMB+NQGCjDPRdsfvlI4ZHvdR5b9PbWR3e9QE7AYz7PGrhrwxDL4N0ojX8jMN+S5xHn4
PkQIeQ7cJWDjLbMU2gVaxDQ8evlFcfa4rnFMu/e/eu+RELqyWNZZ24sRVEI3wsSAqPL24eG0OTAE
k29DTElqpyVUzbVlV3nzddb7TA+gsshWSgOgQbiTummuW2xn60TLo+aUuWcr4YIfCMXY1cA+2eZD
HL0W4Wz8ypc2ZkHU+doVPS4j9UlUR1oCpTvYOhSrRgy5jXKx8RdqitPx4yLmzlnd7GI6N3JqiWoY
bZKY6ELH8nEa+no0+9gsop/uBZ5aTDXp7YH+JvMqRvuV/iCcNNaF2p19rCaT1ZDV4QJDW24Rh58+
HXNcyi/elQDTs6Jpfhfm9wMUEjQMt6Fk/YvDONe0VdzuRJ2gVsUrlZOs2qRS6ZQ3eSd7bChD1Uir
x6SY4XGKeBQmkjiYyGZTEn0hMjXQbW8+jPtCbFmexiYHmzTpFLFsNMhQti3ouIM+oDib0Q2xm0yH
aAYNxVYSNyeIao7F+vLZ7ojnfIdJgiJdNfUps3YPMfZizNG5HFurvOZLrkC8TQ6rum6i2kMfagQg
ksHScWB92b79rE98VGWVtmTd5YMMKFu2UMyLIuGYcdqlqZODkTIiOEUFM4s0LF4CtdUNKCNolvGx
cloBJbgW4XqzYFWopJfVz+jHhymXpoWulZGyR6fVcFF4H9oPwfXx1AC/Nn8S+Y2016QVU2QFUuXr
7FhWX4ZnUabDFY3t3BBG+pkOuvjZOSd1ZD8MGrO1fbi8tDex/DXjNeuH8ZBQtkVJTzeJjXfIdIrz
1KCT4L9Umvls9ih94RBIKu6RW8l6Vstx+gazTCCwPpzriBIDwcjhGXiVdItc1g8mOFcHmpYPEcVL
ziTd8R7P5MdGDSK2RYQBxiYrGOm9QUd/kEIFNyfp1QLe0CTqkWAtVbSIQJU7BQNWkx10Cci6Qd7k
eBzYqPvovrsAeEj0IX3maGezIGZptKvroR6Tv7dDq6hkd73hNiD6GlFu7GRrTXL733qmaK5sMOnw
Lj/Sp36IRNDPIeN/BENhlN2HZBX0Ha9DvnvZ+FNSIpyDp4T2YTrJwex53BkrVEGGM0dPuxs1FQtp
8DI5sHWuO9uH4C9wLDn9YC16Uq3v+1tepLUROPhSUccuuYbjKklKY+b2Xy25+E2F+tqJo4OTHVeg
Dikq2DqgTC/v4J9Mww55F/rTt94OEBvW9fYcMZrPnqDfgHJ2lnASJ1qdVmzUQtuowPY9dZ7XW2Ou
H/CVlM1k5h6TXvcT++HymWLVHZ1vGtQGDtJuHvm4FZ/i0wsWJuYbPyS+uphosVpB8UE0/Oc6iHSB
RvAafkfXOXdaMLEQ1X69sDJ/CxfpvCrnj4yhr5HQfNIX101anSVldscxFkLRd7HJ6pf/JtSEXZLL
sHN2PuYUCeyzH+lbNeR0QRrnI9Jp5o3FspBWhBIL4NU1558vVpic63TmLIMMYxXqkOoNFmvhEi5K
QzZzTs3tFVDnU+OK+kcd5BWDckqFRMJgVFNjP+tMTUUshR+WVR8lJTlf+iEV8A/NwW9znxD9gohP
N9uVchpPfIrV3PF90jeSdTC6jVaWkqGScTJnqExTxfZle2Xftcmhhk80n43nan0EzdPocy5dlzLp
qUIvR9cXtuxY3Xfx5y2O+zTDbf+KJ84tG6yJ9qKBZv3ASXX35nMRHCCSRFbBbUAlMLo1RhStAozp
S3EtY4DusI3xQ7T57BeJhXPoi+rwDLblvysfP2mjo6xCaUJs0XJjalZaoKIJzpWnlDu4pzihuDGV
p8euaaalaoRT9M5QpjmBI1dJIv/Vz/E8ZQ+OZ13e14tSVc2jrYUMzWUjfm9XJTv2CDCtSuu6pMQ/
rzKJLEKkz4KDX9HJOX+puzbij5XgCM2u4hKt/Se7BrrTglMQEHL8c+fDZhw3a6H/voPCBzezDco7
uEZYSQtUAaHNRX2sCRkUPiySwPAhRTvGe9gdqkU4rBHDsTYBPsaEr6FSXAiHSJTkc50K1equy4vu
+w03VuHCYAYmSOtmj1cBnTAdLu8/54bX/tYK9jU8vAVOA+DDa5PXxfADf5Ze4OCofmFbwKiY5oQl
ACybZnwqtLzczWLK0btXXFSYiPqBXnKe3FrRpdXAnEZoX1IU4zsF6nB7ZU8L3LmOn5SFs94hqjAj
W7jsBtH7x2kpP/1yuYx9bIBegPgDGt0ePvWFxeC8rkb73vHdHJKOLC23A77HssteNTEpnpGUM9RP
SQgqs0KSq5fpCxBAUzVOG4/aaTVRJx/Dww/i6NahS5Tgzxrbnz5b+mTk2m1CBFRIDj46Bwnhzoc8
VABThid3Qast8IlWphM845o6ztA+9CxkQ5CMfxL5uGKJYypoNf4Yl5Pbm/PuAaeddhcTeu7brQuR
V/YgfZeN6N0rx7ogBI/eKoNipQP9wG3OVQnWMtqfgbwwxWyBfD7KfmX5McqCXhfwJR6RGoVf3PzT
cjdTbMOEJxJl1PAwi9hAsO8HXA+bQaKh/WGUnzVq//N1p0L7gGtFtjY7HvQ2bxF7ijLvpkSvFf7f
i9Wh/BBFq1DLWCcYjHScJFDrz4zbcHi3LXUFWI1nGOEoi8LzdXrIlwhsdXKRf6roUHlJIPIrMy0a
DlnzkJfyG/EMNZdgl4BChDmGmNXPNL+UgJO6z1Q5YEJu6Qk6YAfoXxAj+qY91/QI9by40K5zSski
2975c7qMLqcvOp2CVfoRgOif8IibOw5mF488LZasW/s3wa62fSjPIZx3O8lxAjFERLqisu3yJ17n
Dq+PZ8yXt+/2SwQUJnMXD3q3btp/qq2KMABZoCuNZnW75FQuKzZRGJovg1VQ7iFbzFd/p+3HQZwh
sJVsnleAwSo5QaG7LVvAhCmQg7/J4AX/ksr3fRgxxxUlTaerUpyNyPWOLVeHsqj1Of3ye4c0CpnH
oU72AAhRoqH6d2N9rnDg3PgiGuyM+ZvKvTN07jHh5pG/i/4KI2Z1fbZ0H2oT2++rYX25t0WQOoHM
ljomgqJ058BDD6uPGMHCZ/1NOOJUSTTz4n/piEnvTHoOfV/AndVe6RiT0+WgmIrW/UGGQ8N4krW1
ssPNTM7kiaAzQ3WdEyYVWoUajeLHI8s2g2+rrpv5owfJNH0M80MK3KOh/M5qGiS2FmtfCpFf4c5u
S/UBnERnlZegjrRmmSUNaAV3qy8WFq7QTCLH1mxG6FMko3d4MhyrBR3JXYPxKswIywmXXykaedCP
u8emmFojvO0zN2eIIPT2Yy30TiTzyq/+4OeikBs5VojaC7avGBHTQZ2UpNwFs/aMagv/1xIEwG/L
1p7HEqT5MLyEvQqheWwktaPrC81OoQ1Rqm3VIwpk+S15yYbdz7pU6io+dAg8ZcSU94IqJHVUlOYa
yw6/elpiJ/yP9rUmOIppMtLrJTBR1Yfz09lETEgOV/Z1WJhxHEX3+a5BKgzk9tiz6aI0DNPNJYOF
/yy24boZkf0zNTbwiZfz7uVYEboyX8VHUgtV3aBz+cC1jKlaHocEf2jDzwKFI0Jkz0715SdlXBig
me5cAXbHOTbK4C0NNoRj+8di+PIgUNFlYC5ywHf6Dn7aTg8mjrJBoQGs2aWQK6S8/7XrJq158w8F
z36Jdw1cjKjXWAuUQekQavBienu431nnkuak6qxgG/BLbl5C5/79K6fFpAP8t4N5YkYTCeIbTtmp
WPyVmebr+yAmYNtqAtVSvBq84sg4wAL+e933GpSvAeGP68c+WlqK5PnNjmV+sIycrMP+8J7cO+jn
UQKlUkn892zVC72NWaMw0LK06x/8scW4K0Wq3Hc5BKsSJfQj88u/jeYk7+5gkWSQq0x/uASyDFPc
ZH56mQX3A19Qz+gh78xsaS6LuWsih+n9gGCUABtzuI/rl2TYoPDSAtM8oSRWvuAQ1oLxZ65V4GJe
eeLnalCjSQUpU4YA0TGBymKkP2VdpMjynLuri3418qfXf0zNl68OxbBGQtjPZhtx8NS1b6R0ga+q
8vrfOxvU0eb4f6Z+NxcUMhE5N7XJEf018dUYu/nnpKx1PoP7HnwA5Hy2xbT0hpHOGstUWMrk8h4C
Yk+GET7JM6s0cJfabAtsxfUyCYlKw10CopesIKCJJ/rrzL66klxYfuqaw+P+ZmnL9WCXclNc5EQJ
51wCknlR1mEEbxn9Q+4LPHhHCFRgpzQF21PBDwxC1BlTfFkjdHWBsDcv4t1s6ikpe0ODyFabkghC
BGaIt7nyhGXXyqGljDxHzya+UiG6Z8aiS1JeCqlDPxtwet6nSnbbTUr09sUvK3If6jrV45urVx8C
bCGCoQExNHJNVyElCbIMAIa8PvP/z1LnWJcTV4Q0CdNv5qq8D+bYvNrngXSfkT26AFZ3krq5OahZ
cvgqPt8YzGfyGc/27Q7lErnoQDrmi2JOzofunKu1wNxBZw4owyqVeSqu4ABBq/0/P6DFeYBRzfE1
2QPj7Fw9TBef18YkQyHbVxWR6MmNnwh59HeRRfhMSRf9tWA+mvkjqweS3nT0gfwGgI5VerK2C9he
fJJyqJGOuSOxEegttBtT2wUu/Rq2cGK6LOrU/o67GzOXSE/Ng0MfxMfDVlUlnpTRISQ4HZ6ESyft
C9jw7SjgNXJ+cK9UGZQsPHa9y6SkC8QovkjRnthK4JG+BMpThevR752Eahngy42XtKpx29UXOWyD
wFrFR/f9NB5mnXH0TKlN4hsmknuZ1gKlFLVJQ0T8xRYhfn06fDE+FR/MGG6cA5d79HdU3ByD9HjS
zoFbcJYvfslEeq21/+VCQ3s9JEg3UiP+zEASBKTZ0ugWqA5PTQj/rSF6t3T0YOsiZwm8Yq1+vlJN
oYq59nWEsuYngQ8uk50T617oPek3MDxzWudhaD+Or8V0nHKR2ta5PflCxRYAklcROKszAb+9jPzZ
dCJsexhBynEQx8C+VWeibO+UrQxFjJtJNUxpoPq2nLTO7/QFCfRc9NRdgiArQ/LvWasZ8sdh8QP2
SWlogjYyW9FZVe56q2X/L/L9ngmw3TuTVGAo2qIqkuDf3mHhJmEyMvxvyx5GysDnTPxXImg1Kf7K
CVe/+9PvFBG+rSWViRfxP6QTIINc9tBERFS/hDenAp5c3KyrcMAFjDFUC84wlpnQ9eJy/tXJC3Dx
pI3i9o7ol9vi8yn4SKMm+6OugULzf49vPOJsgSG4w7iqrr7GuTCLuK6eAMbvv0t0C6fjDK9ZYL6m
I5XfkCANtFkypHX31JC9CQCN9LnYwDi/TpXDfIucWj+SQlO0xW6YTBPZE4Zl+ykfK/x+SbaeRuzz
fV7VuYT/dgKgJwJtoBNJ76FweVoil3rgeRrF0YJpMeczkHcWJ0DS9w3SkAhfOlRKM07yzGIOxt+8
sqh9mxjt1WJSwsIeex+/7jrrztGHjmUzL9qZkzVFF9IPS3EOLG+p2uHLwraTT/CpFipCvgIah1z7
rTFdSOR1cKARWyU6ibHscLunz0/qN4db66uHQfIBz6nuhgqlIv4dgn5kbWKlCp9tGM5jfEnaa600
99fsNmz5g30QiTf3xuDCUYLWG5xyKucytev1zLiM/PYuvWgT06yNjFXuSJBV1k5bhTr/40z1piR1
Yc0E77C4KMX+lWianJ5HsL+soMOe51z27nrlDF0Iu1G9n2vqwBvajnoMcx+me6aWKonVq+gH1Qai
eRLdsKnNHoAUZMgG2lflN2NwrJHlvtTVHgiIA482nCwTrEkVuPG54Xrhl+WjEOdCfBKxxpvo0n4j
46nWWb10GOaSigvv2QKFHJqkspcEp1j+d2vO7gnRFLIx3IO5qpja18pAmjkvCwwiec9woxUE91ZL
m5Uj0DezZtlYmINOXHHIG0m14+6nZCNi89E/yailHo1PrlKigrkBa/S5PzlKzsgcWzjCeLcGN/eX
E4yFAUMrj3dDltNopTBcEnfs0WWoU0hmd80M1rrRku0FEjhmA0AS7Ba7kbHOdvfddoillfiUC+HT
lrgBc+i2NR4Q0PWU6sKF0mB7HmzE0lxenHmtLhPg+cM6gdSf/kXVqkz2E8nuygC5hP+HQuxeL7g4
pWY6LbrcFk35fHpaHCaaXSPHLBSt4nj85StJvRG72ANPXOAT7iiEijCaG7KOcOUsLxrSLvXjnTB2
Fcl76Us7aNcrrjmkH9YOO3YNuBbjc2xnwYqMIQg3by7AXu59YMf6KA8aGlDz++78s9hqApF7aP6i
zTrQszT7xCvAiqO3Ts1EuNf6EfdCfWLNedNe1P2qTPJ45iv/c9ImCDuxTcNMxtKBeI81E7WkdCBg
0QExVBuusBcCXbd53QUIjKbpJQoDr75d7Nh0l7n2wbXGjJC6Ab80bKlzndIzeyr07mwvtARd60TI
VfeaPFXUIGnA9PZutbiWKr+CrkaOKHfF06+aBIbAaPsx6U+xDPb/5sO9sxjh6vzoonF6+uFk96DV
z9kUxdNmdcN21BePS+mdQpi+bA0RyRP3vFdQDR35079KkKb6GF4ik+vFLhjURspUTTIPYyikDECV
B12ISVbZ5m04O5DuJv5WEDrw+3ooJ/cRfLhxWhDHF24w9mVTO96iCazIBzgeV3c9tlyYfrYc8rnH
L8cP24HyU2KFPkW712Bz/xzYUx35u7Ygzz0xcw+gKl/4XSYosONgxvrj9YouLVyL8UefGuuEF4yx
bxOaqXh2MpX5hY3ja4uGtu4GBU2JdvbFkd6rDAB7s/ZeKXMNES1X7FsOIg37/51KaFM/u4C665a6
tWK0HNPd/XUY7kYB6Dd/9LZpl+23trKQqH8vu7g3Kzcw5mny1Vabg5GYJQr/4UlYL0+Mv2QEJYs0
WQfEHhDkqWWKR7wr7pm/bxwJbJBAW0VNHENviqVpG82Tq7xi1iV+w2A+5a/l5ONyl7WqEJ5C5ihu
g1IExLlzRHwQNDBoKh71mssmMuED91qoOY7Pd6UZ8bSPo4eRrCMcXFxzP99gvtX8OWZasThbeKV2
0JN84An6dIzXTd7Ne4x9fhRCYt8gagbSIrv9YKjnJbCKb10mWh+9jfhToFFsVit+derRUxElKrxZ
+XioQSQ6Wa10h3n5O1VYCfjoKNDXJ8SgpoWd9XNazbW0MVQh7qgflsJt3dkXa0h0spYkOk3MuBoW
fxG50uw00SLn//6YCPIi0qHqY4KjK3hd29zstrrQW2dFtv98HVjutZ9E8Z3dI3FyB66rmuMcIvSx
9kQTcx/CUSmkbk0Vjc9HoiTI19i4v9kQgwS4iCNwAe+LWW6eaY1ZRFfoS4Nl5n+lMUClpGYrMVsf
lVpTQv6RdbVVXjdx9rl8v5y16qgFHta/II08uJmuOSa6atCmDUlc1OtVIlYG9HGGaaPVbDN2FHVD
Uzlchdt2RGW2noa183VfA6TYDez0/ASen6SjMn7M0PsMUp9cmZOgr9Zfsfbp8Hcn9oA1wXdlEZ8c
6uKUrn/uxkQUa46+8FCYBPIInyb++FGsPUz2F3Ibpn+IaZeOn3kiPjZJi+z1FisI1CeRJi8TVp7X
bheU9RgZfSTkGYnyWVB/nh/s1jeToq7OM2j4yS/WiwDxdEKmI1KPj/eFeqlTQm1vcHnjozKdOZfp
xTzZmaa8EzULj4eeCIjXEiiUuPo2m0/pkwMG9SpZCeD6W2hXNi6d7ehLcewpa3aHMgoe1GoYq3F4
7ULZHaVFqvvAuS44ETtLFq1yYArvXGTzr9z0NMLV0JY1vJSkNGus905TQk5c5IqSJ7oaNBvbwYDT
lMHNMXCXE7pcKI7LMVSYaZnhM26HrPsjJh8qHW+P8/PjrOVcGkgaXoPBeMW5zsN9gzF5NcE4jNlS
J7aLeRlfHexwQDwqdq+3qa91H1Gzoc60gflMMttR/M9qNPKX46/KnHPbM4/26uUYUCh19t+GjoH+
xen0W6XoD8UTeNx+4V9txwhMEkq+0eOXO/fhmeLC0jxW31FpsH6U2tm1M4CxOoB62/5lVBgMgxLc
+I6SddKjPg3uaUGDeqZO77UcibOloUesBOWUh6ChKRvuwxBIuZYEHkNQhYaJdgLQldbFvLbMXPgo
9xEWg8TnVjA6eqZSCWikXBMAF7RVJiBASCD3hQzs8C2IFx5i+qoHP5WWb2QExFOkmHHsMKzD8PwM
BcZ3UMTxuHylnIU50NvADfkmgywKMscbD1A3g8xrC5GcIK534ouYWKkzALzEem4Qd1Zvxh3n0DOB
NRndh1689u50qsmfbrvyJMs4bSWTSinchZxZcefoI6qyL5z3rfD2GUzN1LTXFDmCr1dTdY40QknI
llb2ZMWO0cRWPGKdBj6PmtQ+0Gxlay0v94NSYRKdrmi5BmG4T9/UI1O0fitRv3vDP6arqpaOWUiL
G4uxQVlY8Iy9eoDsadTTEMPDAM7w1hblwGhLQ3u563x8QtiroHNRpV1W8TRb1C5HJjA3y+hB7OAR
lPnMGBQK/P4xHoHMmkIzVBHbYz/dpbSiuxByBNNIjdXxo9qsnp+Ko2Le9xaL0TeJEid57HzaQlTJ
s3FBwU+eSsjT+Smssibg1np+NPLwWE55i28LFFkY8AJJdhhcWreI6P4zOx3gtdz2umGVtwGCUmip
JFu1eI7EJFdRQdgJ0yPHGs6/Ck3lwVIIU4P1lG52PcQM79MfPxPs4obj4sO0oUL1f0PAJTrrlFXD
rSxQ9OGp7iIDYlRvMtKfaIgmlJJNC15hq/zGd+C2Ag==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sr_ram_commbus_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 60;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 2;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "kintexu";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "yes";
end sr_ram_commbus_c_shift_ram_v12_0_14;

architecture STRUCTURE of sr_ram_commbus_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00";
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "00";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 2;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00";
  attribute c_depth of i_synth : label is 60;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "kintexu";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.sr_ram_commbus_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sr_ram_commbus is
  port (
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of sr_ram_commbus : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of sr_ram_commbus : entity is "sr_ram_commbus,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of sr_ram_commbus : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of sr_ram_commbus : entity is "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614";
end sr_ram_commbus;

architecture STRUCTURE of sr_ram_commbus is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 2;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 60;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "kintexu";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of SSET : signal is "xilinx.com:signal:data:1.0 sset_intf DATA";
  attribute x_interface_parameter of SSET : signal is "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.sr_ram_commbus_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
