// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Jun 22 17:48:57 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/sr_ram_commbus_synth_1/sr_ram_commbus_sim_netlist.v
// Design      : sr_ram_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_commbus,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module sr_ram_commbus
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  sr_ram_commbus_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "60" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module sr_ram_commbus_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  sr_ram_commbus_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
d0c8Hwt4EdsOPdusny0jbd3DLo2/jkVorpmNDzeIz3/OxPmxAmojQK+5GK+5IAGyz/kC5I0XIn3l
AYDbZxoHjYh+Jse3y46o/sdx5Ltf0ecv3pJ13XcJXLIILabw9uXvYmD1Ska9wNn0eDAluIx/wkOF
MppPur2QwNLRGDIMcoyv712Yc9o8l/m0IXytaSe4wfBxmaSTiR7CHLr4tWw/8aIwp3xfTg3ibPFW
YlDk6ZYUUsIlANmnXq6Kqyg4l6zP6Y92jSAKcuLvvTR6WKDpvcBPJRER8lN/XtjA56Xly8A61RBu
L8nZ5J0aKZR05jIB9TuhlodapG2uXnFwFtTCzw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uvUIc+1sDJaI+IyaNF4Vt8l7+cQiwo1/zSQB+H63gnc7+NL06+KvErYX64bGincWryRri2fryqY+
pFiEabz2QaHSqedPxOGL8eFsPEw41hK61RsTlfQWGd04teteXTft5xRPvXecELHJ9JBljRL1OmqF
Crm24TWVo0i7VDwXXkb9kNaiNk3ZyNe/mK/xw8HEZvba/gdYBL3GL6Zvo5I8s1Ey174u23qdlV3a
vwYMnxKTzG5nkBYNbA1mleTiqFD/9GUN4Ww9iCvfiaIO9UUbR36oUNKzr0w4dCo6zZG+dvh/EbOk
r605qowCWslJucQF0bSycFHE+/yHA+BnKIAedA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9152)
`pragma protect data_block
/P92uqJ+PXcprjGs9irNbpQmK0Lf2uppD999kQRuwfmPzKF5j2yAjOzZsL0i/2oXQ1sms2O/N7KH
LBL2Vsvx9pU+voNXmpidnd3bUVxkzt8QHipovr2IQ2NbbY7hZh+kPznKIb7jyoBLPobLnL4VPjjl
ZPNXrC4m0K/I/6UgIUvTAcKUsLoExBYzDM5wTdLSl/puOCTdAPAv1v7MXDjCSVYJHYugEeV/u7ui
e/xoKS6M4fRJKJQ6hTcZMjql7MgNP44st+jnbg7sX9bUuprywxxNH5htmIFs9D1fpIkNQ1k41mgU
w9i8qQir41I/tFINvhJGKGnPDoCavsZZ4mbaC8ciiGmV4mBX5rWpgnIlRsFeyrIjuXnF7ubRWqUI
6SvLYGK/8NbrWjpmLMRYnuCZdYRJkbP6VK0zAPXeTgvjta2brOPtqK8AUwmefYwfp4k+qsig5pnX
cUFKJ5Bxa+knKqCY+EwJoW6BQCoaOJqgFG+a6s50YUOFFs+aEpY8b3qs8yVwBQHV2VX/4TQnqF99
wru/8Zm4imm3G9/yRlODjrnjo4BJPD6fPlSb7hznUSRjMFoED8nLa9wp2I/KuvOsNSZGTDP7Tzpw
zz0BsvIP+7UDocYyVFYFs0q4Be4MhliHbHlGAv0qablF1wGYnDdA/S1MtO06GCBnhsgkyTC/CGmR
ZByox2hqdlRAqLlPsANWOyAc2lIy6A/4FHyHYa7qXu8Eu/V6rRJZsKWkH3hHnFlOTdDs/eNSwv50
4gOxvlPZoT2tPmPU7JKTg/KaBagqfW7cF+ya2JKcK/LVDjHBpFwojahNq20p9vqOVil5iBR4GmMx
eoQ2FvGXm/eXI5Vw0ZFFWkKRyaG86F7MZ7lQIgQg5jZ8a9YxDKFmGXK+9kzC2APQwzo6wmoMIh1t
AsVRJHmCt7Oqm8glkeI86gg3XF21w/BOo7mNjuadg9nJh2NaohNMeoIujY125cYQIk4EujCtjbYl
tAZ30FDnL2PqPDSJoU19s+Vaxn6WOO4j9ZlpYX09q6TeTjnuCBjCBQzBMfPFvtdtJ37C4UR0itjB
Xvb5Rvgm64UIBDRuevq9FJnB5P74/ny+oJ2sPNkxNCZYsuQt9nWQ+ytPuEW0AiVmyHlDvgYhADS+
YxPg16YXyd87SelPbcJBxPPnbflIv8FIMZwa8iSqIt9sJWAux8pkXxzA4b2IJYKD7uQGaicH7Sbj
gE2xcZNINMOZE6Hw5oMUdG3tYTzKSB9NCZr+ybShym3m6gpStU+9ophd7/TfqswJfulGhJOyMl7Y
Ovwn/a5YgnWKV/cWAwROn3YhMYSZm+vyRYvoSgWpmvkL4xm536LeIpifeawoOGaOzBJv1GzeY3Zn
zkl2YXpur4JkXPJWFj4XPqIFCtnAQDJUZA0Vf5KvfQO623thojKDdSa0oPDSIBSKiA9qEPhv/Kp/
LAemF6WIhpZhWPtWYlPb8LzmFnlvItofornLYTUM3fwulheFQOxHCnhAHCSPaolZK8totanYICIa
HbpNaVvIS29/mdP6FpBKZfZsmP5XH56YhPITT6BuEJMGd20ZoLfdkE8zlit58AVhaq8KRPzKIa/Y
ERR/ucFcN6YBs4QjR7W6HNE6pOBk5nBAxM3cS+lssjWkhRl4fD8fjcg70UqOo6jTkF4iOaeWDhiJ
A8/HZ3aF44y9anX0ftgABmZpNGGYAK7pMYog8zlgXwhJ+lmSDktFDuCoswu75p3Ub82JZ8/MVF1E
NLewjP07vCJvJyN8g4yVZLe449yuxGpfP7SgWBNffEz38/EJPwD486R1xBojSvwdmbH1P34e7uyt
M4E1wc2sZZRCMmx95vVVuZEUsUNFs/e9VrX2Zovrr224tD6fGJ/8x8okzIel+0QvVlwTc6diP7S8
tML6Jsa1dSNzZGTIMHgvRsu/br4wswiDK4ekRr3SzECC5cdLi7mCzVRJ1WALNdFKXO+gDkC/Tqzn
w3SoYelOy0AuWiIFD7WGxX6W88GC13qYTxSjDGqYqDAUnzCHr3gwlxMEHIgnWh97lngSFGjv6isu
Q+eJdn2b/B1q9kVBF+WzxZt9kXw6kc9kYsuIchJ+PIO3vpRKE0NxcP+AqVSDvWmDndsEwma1otTz
AGt+89Uk2Cn/b5DEr5Zw4phaYw7bdvseBMez/eSXwwGmkCRy38JCqLhv1tQPpRz5VYiXkjN1J9vi
Fb8LxrvRt+z0K4eRKPWOXRP2EMqFcjclkhFfXyyMODqOItIq7uAcrI6NhPTrSZOmt4hS0FXnNE5f
bmkBzHzLKsHHQ/3SKjVEzeasD27rb5iy4XvXqjJgg4zAro7WcWN1zg+5ZPUYa5xQ+u50dCqrBOgM
BKlNpllQ2bvc0HKZltLDV/6oAmm6LKCUi7j/ONVR8epUkfka5FgGYR+MuX8QYbDMY1MmmWsb59Lq
l92oA0lRoA0GE+nsuP6bK/iyIyIGcLHjiujxf1sMOdZaG92bElbqTj8IxheNvsmhXK0Vs8fDeg9H
PyvJTF/gl+E4vJkrG1KSFxc595G4ksFFnDCdgK2kk3zoaaRqhAJtq8SwYFsDgYLeRWy5FxJYVnol
mRqhYpHFEuVUaAlJfej3uRfcktK6rMzOduy7Z8Ui/QVermUaLqfdDlDEJuP2dufK+oUEq4XBLRzb
9bYyE0qJnf6Ms3iXcCHx9jwrsdEFzoxvsrRIelEPd54pzr2KLKdsoyzylym8sm8r3VheuU8okW2t
a9Czo8azdbnEGy1drJut55PQiRtTpFsDOifdOPtZKUgpI4NX/aUakyXkWIkyYNTdRwiMbvcYWnc5
U6KrLyKxI5IjGum8k02qSERY9Eu5bc3flBXOzBRjxZ5lP2wWkZZRtxkabcwblEOTfHTsjstwem/P
OzZmqQembNp57RSqH58yfsffyhLf6+08jdRPmX8gwndtHuKxLZURmsWNs2wVTY5wgS4+/u9YSXXi
AAUWxdcLvOByN0rd2Okr3J6ONbFfJ+tsrcVp8K7A1FKzdoePWG5Qf5WWioVpFxgP2FJ5kXqePs27
P3b1RXXOvDUjgVKjZqYXzVsziZ0vJ5R/DTJ0CFsAtYA8i8Mn5T4jEAY7gKEayFD0FS5HOcax12qA
+YowbXhUiq6MxVq8PNAdr4NFuZqr2Ba3tUS88vtBAJHRpF7i0AciApF/WWscN3Rx1aYR1npnR1g1
3kH+zJDHrFnXj3gEwsAREIJ0y30F7rofka2LlZslhtEQq2/Ukibcrbw9aBC0utRmaghDs5dWVazo
gmjrU6FzuySM+fhKruBN78gU2hnNoztUbesXZopUK5BkYcefKzH5KWZTmJLPvzAtdknZDbx4GUqZ
wdxX+1wSvzHo4GLbbIUBkXuzsNIo+4FPvKaSp8Xo8oXusK+0FnLRiwWA19m79P2j+PoSyBFCoOe8
DjvjUZt64QoS2JM1BtErxfhECtApxDFAsH58qEYZKFtzlqd5QghXGQI92fZm8I4oCdqsmgWIco9v
uvvR50Opuq+4ItCULy9yfW6LP+wK3NCMBlrIrymxPBXZdB3Irs5ONlHTFO4nyS63Lyv7MxzhC43D
yq2LwW5TCRJaqoMPeSd+mA13hTIO+IJGQ80pPfjEEHjkNaY8CExy7o1FAohC2oKZ3jva0N2hdSDc
nPD7hTN0P427dyglMGJhax/9SZGcpWqsqUmmrGSD8sEPzG4urdFQIoMMz2UQBZZN2z9g5FJy9/qu
AwjxUzls3prsiNHeWoPhwRQuDlu02b3x+Y474cruYMaPTZHPI8gvvV989Lz9PtwFCieMtxkA4XtI
CItjky1CECZ1y18kO7lQw6zwpqDdAoFAX9WfVUYtxAAvC22N/RcKOCN1cg/C3vT82HLogtWA5wEx
kdPOcflhC8y9hc2ZJcbpPwfkRJA+n+m2FG9so+SyZjDuYgA0duuTvg20ycAzuKnygBqzyLD87uXS
QaT6juhIvvd+uHKs1hN+GfZAvWmmtp8V6kK5qkaHH8Wjc70762xJeRKh2iO+9MwixlazRLVTYdwT
gqmyUSdBiEtJ7RzACe4Y3UvZqkxT1RnQMtegTH+8XrQ/eAEqy2A4v/2IBETEKn2sVH7BEF0cUlnc
A4xmXO73HPsdcajoM/DHSmy+7JLscd7A/JTvIczR1dhHKz64p/g28M+WtmqWE7FFdtqfYvSAIdZf
U9YhRT6hzCLBXCkYUbxYRLQMB+2YlLXDyIhYyAy6X7ON8OYJhsCzD7/mzoTEtE+Ol4YqKp1G6HLS
L+uBMo9LLflQ6nMbJfjONdhYrmO/hWysTJOZ9e7XPKw+ezdwrhs6yqrR8MfYAJly3zow9gaiGyej
GkACPqErtZFtvczdyE+4zKjEliiAjkA/PV9V6xTfqo2/RhZLQwzshxgpdgV7AGW0GHxnPG8lKjM7
Gsw5mbHdIuH14IlWf+Fqa0Cgr7RR8Yh+XHQ/RP1aYgOSRoVQOg4v5QTEvCV/63IXU2JlUem9sVTz
5mGhwIBBQtbd51fCKWHJI4XoL+zPfA5EmYqELtgpP9IbRGHPaGOQ2/Q4RU0HSTif2bzxSYP6cPoX
CAt9HN1ek7XR7TnikGryL8SRojXHWhJQcLUagRzTSaNND3GxeHXLe6FdLlWm11eCgxzT3NTNnwDK
VFfm+6zFo5mJ13bIhAVMsOdPDdDwfwGssMfLcIN8QEVbYOEn4RBG2UHdVvlapevKu4Re8Or+TLRq
BMM2r7vF+/9y2T9qia/YWrKGuJW11RMiUwGXoeXQEWko+mAk6q3G6oAMskTz+8LFTpM6oRwfprRW
XVZSwyYedys6ZREB9ZBhdMvJxjXF49W5yrJ3EFM4Bs4zxb95ASVCuTKhBWvJfcuOJ4gfmufjmM89
AWPX9vax/lOB5eu/uyv9cDPiFQX/DZ3YyFgZdAo4bwQLnFnOSim/K6UTgK04AGzS/3WYcTC6TvIZ
QfAAYTnuHdE0GGNdfkwQdyflXDGK0VkIUNhdSii9JyHHGTv8zXiBF40XX+xOznW7LU87cv4c6PoG
a1leJwBvrIPoOa6DxgVio0uhTlqtX5iPDVwEVuf9OXgmxlBX94/NlGy4ufvPl7lScKWtobl5KajQ
rCmkt5AQ+V4rc2RziOa6p9YOPWUP+EbXpWsP2QqgrdAYNLe+R8vBHjjpll6PAdy+PoYv3KFZBo2+
bdtpTHHeIG90exbclcRDXff3ZTSNEfqfJ4Os9uLiQWMm5tLoYVTPGZFvGVNMR3DB3+QRpxMyofB0
hmNfCysPqbDGRxh0jNxFtm1LMACeQVeDqciJV2zYAjVK5wM6FxqSVJB/AGWNx4jr6PlhHerD/+ZD
T/Wuyxihchb0AR1nCTwzI0odFH07RD1UEQihEQHP/GX4IZ9htmmWcandgvfwB0+ChRc4X8/At397
Y7KAAkiyCgv3uDR/o3MxhLh/qywls3mli53bYfMcbmhxJ+tSxZ+thTmIZBmcZeVn9TUo55U3ODEf
N99dbQFYgaEraHsLVvccodSPbHOiE9xlQDJ6+TiAD0aEgv3M0iyUcoJJg8Wvum3UgOHJfjs0pSvf
elFK6oR4XwU/ufZEDIkdI+DDfdP0L+RpW7Qw4wXPHu2qXdSlVdbYauHq0tlZzhxfap1Op7mo+tPo
YuVPOq/D3pZO9YIQMPCfu+iSf9/LD0Rac1Lu6Kph6Hujy3BJhD3qvi6BXyOx9JP3bthmyaOwVNQZ
bWVWUiZma/mTRZSpQRQstsgdJ9j39eziCMvPGRr68NtCBRuAkTF0PgXyow9NvWy+zVRtCnQoyIDX
8WUFB/opmYpb4fnesLk1+gmWEM6y0sqW+E7ZyepIYPOPbXsjVGzfneMYuuf8HcpDf8sJkZ343Lpg
xx7vRkesnOpsURpJAUbNSNu+L/+4JYgB6YdHAcfQo+kOs4eSOwYWHPVFgfqkbEamPaarg9B5Az9w
5yrweZEQ3r1/q+gFIm3D2VKy5OOg0s37ujuY/0mMGTLJ8EgE3C3EY1Mu0hwVXp3EB0GRV8gs/CP8
F4Ky+fcAfdo/v9NrB1giurgTI86+Q0WGbhCdth/7bASWtiuLMFoJBCz62h4EFgbJmUJYaaibLTj7
F0tDXbcbIM4WVw3Rz0gajvJSMbKstKO45X5cxCIou1/YJbXEsltjvuRbOKtZxYyn8ElXB2evTvGZ
uCvrtFiE+hKgPKggBBBZON/Z0c3sI0cvEV/mb86IjbuL7AiS5qsjAT4m1HxJs3okBB+kYzvUqX5z
2VFoP5P4lee/rGjzvZ6iQ+L9eI5UdiBDeXCRfnkrEh3D7s+SW31QRClnBMvIAAT+Q1733B+rFpL9
mfb4DTzhpWEt9E6Ind1yPYx5J7n9IMlTB5d9QiZdkrAXXzhe78S5NFBGcutOI5gNRmHK7HBspk3M
/gpWPmuyIjiXHlRxzAA74Prov4jNKOyE/DJlt4zOxsekOs96K7eHjipnwf1ASKf5X+Dc5qoPzWw4
c3YWpBPT+TgK1W1kgL0PGK0wzImhobWz43aH7+5DzGKV/sgTs3heu/limf3/ZRlVaoKZddTO6N4g
CIKf1WUSsHYJcVf1zJFeeP2R0iViYW7CTjsR5p8Wb5S8NGqtQuQSmv2H5Nds5OSxt2vqcxWjDvyb
RukcG3mTdWPk4iPWSDX/K6fsNkdb6TJUpSjAcfvbEtlItbxyqCJWDFbjgOVeEdCxhH8ru04YqNDl
7P2K0JpQg1dUd74y03B2VaKbIqwb51X2FQh+wmqXrTF18axU7H3wBltXR6P1uF7J3I4Cis1uTOuF
eU/t/tPLpoKU35QTXt661HoIegh9HxLt/q4YlVs39RA1UgpJEe4sVbIn/cRIp3pDklr+c2eIkoCg
8rK/Ekrt+5YZnRjE7omkT26J9OhaL63xgXQ6WKZnPwWk9de8VKuRDNLgyA7371IL7EMglN1y/+ce
knq8URaQzDopmm2iRpOLE2P8lL+vCQnsVY8E0zOeEvgyYNZIVcRVFeoWJQ+hBGC66mGTrR+9IJOS
mfAvT+HoqWocAoIhbkYu2Td1EoAoRevUsCJEMKAQFUXzfLDCB6GlgWeESOLEfBFvOxeKdC8ZT5ZK
+Kej8ZVYXV3SGIJP4bYJEP+xYhE2gWOrQfvHZPmH4SEbIDpQ1RR/NThcEWy5S1G6ES7gPSFH4rDW
4z7KZgbDxmtw5ltF+wqtRTfxE+1vze5z+2fWL2Oz9zizvuXEW5k/d3ZPVs29erZK6gwJXDFvpMcK
jhl/xJ459Ud5uKUyfuA+O7DwylfpgDnmlUxwXRohhslNZFHunPJVI5Lsd+N4XcXhPFvr/ZVVEkad
JlqG4XyWHCeVDqgSfFhB6PXJpcL1qdZ6nlzrfeF9+S04k0I+/kIjGqv+JVqZ0BSzhcrC4CMJk0Bv
LQ4U5HbVZijcSGGqzXJ8m8eRh1IrBw44JuusSnGr2707iNkk/kySHtfMrhAwHHLx2RDFNC/zJ9Zx
kw1hy/k/TUfNsLyDz62e7St8dJb5jpKKhl75EFtswYwH5uosc4bVqlqxHbinOp98dd0CMTOM8J7N
ie1YLEWGLKE8mGQYPQBUxrMLd87JO/8E1bT4lLVeIOieWM8WxbjAVYTny2wpNpu/g6hoyJvLHHxb
hwCQZAiWrKJU8P+Xlc9w6MMr5/Guapce0KxWh9jN311k+BzhxRa+WlDYrqUbiGL6OCJ2RqOAk4AI
8t4ezndstMos1LLKXwugm78jonAJ2O3hDe0O4knInvpFH8+px5BuBpCbQ7JPeN2hUBTFgOGZLKCO
Pl3zC7o0Lmt/cXTigAHn7Ao0MUtn/USuDp6m0cOq/iho4uObnVmz/DFrzPALJSx1r9ZD8ND/5LC3
hbF1DL4Q2wMldjIxoCi2so7hZbOMCs+zUSZnsugLJDczDCED64QXJAMKYXDhT/xE1Jqat5DH+e2p
xlNfhbeLkOzdPtOH8kFEsZCSFlCIfa7BFGPwf/vSwi70EbTR+uqpcZHOHcnsyAy/qpQgxAbbSERn
T/Nym6P2tRH1yU3JYBFCkbfQl9HXyhVYPwGESi4btTaCf7aFZqFLnIG9oRnxAZXOqjuXXJppftEK
Fl6SwU4hQ7AB89k4LYWFu8HtzqJS9knXLTmvcB3lLswufjdusO9gl9y+uQHcbrhBX3z69nbpvAMx
+JT7yRqmmDtYQXBc2IM3pbo7Twyx+BFprLqEC0oEtROXT3NFKeF5m9FPgTh2acm/SrxCsUeem7xC
il6699EwCTyXW9+0FCb24OHr3ARZsDQVLXNdGP5mx/zTWHBpqypobAePDgWOLiF7ypCHtXf2U/cm
/3lu+jIBjnytGdCozeIySbYkdJzB0kl+AJC0xhTgiJOw8CGcCV3fVRWguknau9NCZZ4gClbcPOqH
ngQTeFbmPDkPJpufar4SdKxVx3SXXyjI21u0aQqw9R4fSDZuR9JWSP0YbX3tp7HURhYsdmtJrwWM
bJ/QVZpKLv3ORgV+Z2UdpoVY69PBprJooyMRcQvSo/7bAQrwrBepDC04zQYjdh4wcX7FPUQW+GUW
Yjl4fB7WPl9i7DgZWwo8GuKfd8xETyYLw2d4wos9s8UspXi73d5gzhF+MFZhVKSXajv3/P0ha820
4kDpd9929eYcmfThItvB7NvrdKajoGyPcdX9pT5w4B6OHzJI1hw7wUARlZ1dmZxCy9fGYyfzhTDk
gRNTiGz1AdA8PAhN91JsqEUJALzhZhlBwNEm0nfaANYj9YxoGHji/KJseOnviDEWI8V4AZauP9bp
+yi14XP+oKuG2HeRwIAFnraVGYcdeI3E5K/CSZhl0LvCa4f65rUqyS7bBBlNz75X+jeRPZwZTH/X
7B2oE3zN0TSijGLwew1u9+6QoAQ3J5afh0p67quVenFApre6wEVnmwW1AYROkRhrkmotCPInGqCD
X850hqayPtwsAt4nhyc5Ybq1Gq/hRRSwq/9PEAmzs2FMgtOTXeYzcoDfUGNhMQOzwp+gXFCqjQmA
IO0+VuThb30STA+DNV6OJxVlPq0PJoBgEP1JHgCJIE3cuAEhVCa301z1j9FBi8XCyVfNZ1SEQDFa
iRg0zJEMQSlibkEhq0F9AOZy6vKQDrWQKStLh+9kl1rISQBittbqFinAM/SZds/Of9+RyJ4iNfXr
DdkAz0HsLSS8cNzUntRIzvMzj5FAiQohhd5ZwYGMlfMSTl+qRuYt/JcnruVTe7t823WUYw555OW8
OqL/pIKwhFMyBzGaMIca/RGlEeXb5QYYx+Ouhiw9Kbw74UD/XvWk00h2yld75vjaSAiha9io6mrC
NczIymOwzboG56MSXQovreWlwV1pK4F+jDpMj21v4L7Ot1Vn4hY7OaWzwYA/lr5y874+sRpogkiY
u8MbBrfMC7T/9R3hOTYW55VuzRCzsEGFRZQnphRFjZ4zdX9L3bzgY+wlIocJxAw0a+WkFWJUnxFa
a6Nug6V8SsJXKJhMYsm1aVZty6pgnU7kFmiHnXxr8/PoX9ysrZAxzchN/WY24ycvD5rkXS0tPQdv
nmji6cy/wnsTVEHWtXaVD2QITCRfMF85sJCe2kDrvNUzqlEPdIe7a9kW96U94gShR7aoHpuJBrXY
ZEqj7ZuN3QhoHxrWwcuiCz2O4RJutXSDWGhrxwMJrj9VhQxTms+8zR+BZOdt382vEBXHrLbVrAzD
tENg3ngkQou8JvYccRlzRFZZfgbFXEE9DZ5nEKUOrE7JJaV9xPpCI7SwKCmbcu/QZdUtnwhuK1vg
cMCJ1+vOrtJ6B/pC1n31pZzLXLg6HIFzdteLs7CaZEo/QzRZalLfpSHhXezA3KRSK8CzhMj01SaR
sQSYQv/aMEXuVwhd7RL91aT8OXa5y42FMsOA6s+rj9t/UgFJ72obRovUNhByyO0xJX1C4WS71450
ZeBU19Y3dXAbnqrd52lW/7Zsp9A36TU14/k8k2yIOfzCmpRM2yxO5SBqtuCiZnGvEDDh3CC7Y4M6
0Ca0A6vdtzKppak0YO11+Oqcqhehnv8y93Sl4RP1EGoW62teFlCU3WUAmRgocPvEF37RxUgJFZs0
Lf/9CFzwQcciYjWwBmWLjirMV/CmrXAAWHz4sbjj3GPYV0boqhy2zMvGc8CIs23nx93CSmWBYBeg
CXbfh+aD7+rGfcFQit8sYRFYx5IufDek3S9I+jfGYFxAn5oIhcJ/jcl4913nEBZDf5SJJlu052Ez
tv5913I9RBZfWSi7Y3Of4A4KhquaQRAa+B2N8Og+mDdPUbKZtnL6o51Coxi5kKGis5lXx3TjecAa
G+U8QH1a1teyzAuB/rkM1clY4ztiGTgLPS76UqSi5piJ+gFEhTZDwwl52v/rPkRo6ZdykGSRUVVw
ba8g1V2tMCJvJR0cG6IdGAtt7Win9sdkrJH7wyhbehJHyVBcKXhxC/eTKw4WhpH9lObb4Zl7YAKd
16Pvv1rXHr51UOfYHN8zBUxypc0Oc3py1xkQ6KF5If9D6A2Ef5uOPlV9JjbhzDolG7WArhXrRMk+
VdRKbr1yHbGeynerAba/JtgpmZ4T6j9HSe4Y5Qfr1+p9wan4/fTeIZ6nNC2Ac1vekQVuV7ev0WZD
y73ecPvvb3AJPt+iJaDnBaPLdPQFoCeFLlXisyhdUXBr1FyVngPA7CMf/yuvYs34JTi/9haLTLgb
VUfndA1xlkwKCiIHow3aqlBi/BeA8qOD1XSP9twt3RfOTCyy6qjcC1hr4M1sCPd6ztNHzwOB1MHT
niYaYtAYq6WcwFUjG9fWNNf3PL2EPB5VqxwRjGdhGAO0U1F4kdphDlaXEAy1RZ4jo5wAud5eJl+s
OrQD5/MnEgJuMMHAlcVHgeQVyP29inJ31L2z7KCH/iB/WYGmjqLVcutLlK/tBi4HgINO3dBHPkAM
tDwJLo9xio2GLqGM9nUqgG3ngUXZQns9I8bl+1WUoRKkng0/3UsCOFGAFCBZFg5LI3XXhCYUZiNa
DfKOtjcEdbS7pk5GiaAji2tQUhX+YjSV7qSesZA3hlpGVbtruit//63IeUbL68J4/XhEGQV5OpE1
EQm9OMdYV9bJm+ihr9ikz0Kfyls9V4+M8K9C4IgrrAMHmp9HjsAoB1XTJFvxCrzXbmgEpiWKGF9Q
e7UAh8/3N7kkiGuNyhsTI/gCFansnxh0GtXqPL3twTz+yJ9tB51IYlHfvfxWaEf6o3KKkx1Nqi80
hCMQdWfkOh5yaCmkOyvqhLLtEeWOtnCpPwVf8owN3/ufbNDS1FueYRGFzDbHEpXK/zT3rWeOiV+V
Hw6Qh6KNFtzNDApxVvd9CWA+mAyJzhaFvu8IYXiYK1oZSKqSSL/Uq7/W/b8J1bEYcITZVQ4tFeF7
Q32f1tsJAXJsjDcWA6MAyeRrHDYdlk2Alm1dFJRBubJgOFntoW0/WmwlOszDCPKCBw6IQdnPihRe
DIBq+bnCuPsWxW8KaukG8R4IJazt+riJRP2l1XFIi22dZP0ng6bGZ9opCsPoRigUO0FTsP4aGdke
RsVLD/h+S9U7L28pyzo2By9rV3rnsD0S+hiUGgtNomUXixGQQqImzdqXju7s0pSTQ6wz0WCQyrhq
Ri9RsUUAYEUOaYj+XSt9MPNT0RT31p6ttawjA2exSSrCUAbthVCorCRcHUzelIoupMJ0V0FrvbLe
2HUfShPWMeeh4L7h6KsVqS3iLKMmUX3ePDqk4ZTVcI1BLuyPBw6ALEH5lCOzG1QZxvKq0CE+GScm
+P6JQ05JsmdDHXNslYkYDAuJivDojAkco85TTqnR+uDrxWXTu58g4qhoA2R2hVLj7e1ZCsLvmSAs
uOsS6CIzCXza6LIKfF171/t2eIF0k/jbnihnuXo5oftReanYPqB87sIDX+6YRusx8t5eajOvIuYu
HgWLRn2krS6/fH6aHYTLVnkTykMGNgdu7H0cdB/WknGIgNHdp9rF6UUYbqzt6HuafCaVdSzyx9xI
ohvtXCoh9b6WcaC97tHvk4h/sMBu5y6ZZgKWRLJVz2eXRvdh2n4U7y8MB18chtUFTnLays5Crp20
TB+nzymdcxCY8a5L6zSdYPwTV2fwfB2xG6CIe/tLTav1JdSdmg+n6ofF+uXPuaBoI6Cdd0jig056
7ItXoQUhq1dGavIWhKEbgeO4NNuLTd1pPMGy1gjNDgWIhzZY4vyCw2EF9fohh1SwTWhKTRbzsw1N
XyJTeHo49FQ1zhHac2QnlOIde9AAn/S7ke+CdMb3wCU=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
