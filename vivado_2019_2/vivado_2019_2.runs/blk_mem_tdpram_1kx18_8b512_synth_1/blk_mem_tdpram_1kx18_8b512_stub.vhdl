-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Tue Jun  9 17:32:38 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/blk_mem_tdpram_1kx18_8b512_synth_1/blk_mem_tdpram_1kx18_8b512_stub.vhdl
-- Design      : blk_mem_tdpram_1kx18_8b512
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity blk_mem_tdpram_1kx18_8b512 is
  Port ( 
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 8 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 7 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 7 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 8 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sleep : in STD_LOGIC
  );

end blk_mem_tdpram_1kx18_8b512;

architecture stub of blk_mem_tdpram_1kx18_8b512 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clka,rsta,ena,wea[0:0],addra[8:0],dina[7:0],douta[7:0],clkb,rstb,enb,web[0:0],addrb[8:0],dinb[7:0],doutb[7:0],sleep";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "blk_mem_gen_v8_4_4,Vivado 2019.2_AR72614";
begin
end;
