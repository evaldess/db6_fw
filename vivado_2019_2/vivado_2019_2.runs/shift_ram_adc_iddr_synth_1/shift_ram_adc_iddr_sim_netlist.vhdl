-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed May 27 20:03:06 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/shift_ram_adc_iddr_synth_1/shift_ram_adc_iddr_sim_netlist.vhdl
-- Design      : shift_ram_adc_iddr
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
cOJAdiE/tjD8vKDfMClQSkkmNIpp6YpRgJlBajl5Dow2wFT3rYwy4SfsMgh/b75dePSkTr45gf58
oWWH9YyiCgwV+/1UpxoVLjvh4/F5MDRopQ29GJ5+tADZsQE0PmIq8QCqpcyXR7TmxHQ2T61yfqSf
YCO3o9xw7v2cFE0tgeJD809OToBAffflyY8iBVnAmKK15NNjGSf18JKUZyLu9V9Wl3VTaWADug/G
LxtkmGlBENKX9xH8jhVz7P2tEMULtucFI6Zl1pOrhfl9ZnxddAGS+YJ7Tq0bW/Kpm2H7NsCJo34q
WH7xddcR4tzGxLLmD6ty7wzZnMbVKxpe48AarQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3iroHRoIdrqCBuO1jy3vcGPjUuTkdH6aM0S6isMpWB4H7aLykibi3ptahfChsptLlyQpO3Patmu9
nLv27yW4HEsCk2D8anGpDapcM+qWKaXqKezgcr5OIcbV3HRsaoLroKPN6j6HmQh4BQyLNV2NDO5q
Is/p2dxpcOtwstLoxPPKRN54QffF2pL0/ZwJDge2PobUeCA1xH1xBYK/hp60egrequvCmMvFewBX
RkX0fdn/mHtWOYGMhVGzSzWUX8sLcI8CwQUShx1aHVaDmDqtqlkdTLvHJJo0hGqLJETmskc3EM6u
chYUl75Xd3GMdkpqT5xxuuv5VBJM0rlEAYuL4Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16560)
`protect data_block
sRzwaFByG8iEc9urjAGojaEz4r4UWllQhTYtWxX+MjcDqFdJsmGf7E9LKnnIhUJw/xf1FZUcQrxD
mNWEvM+5a41MxF0IIl8RyDjNyikWF5Vw36QbBw6lWwGuteAVRpG66kkZefLqa7mfkIy9CXK9RgLm
HNfiAPbf+QEclwycRQhkc8SzJ6JXMDnwn7lTEb94j8gvDXOCKjW5hM6O/+FIGr2hcXrgi8x3J2a1
UFRzYsUNtB4KdbJjYRIIm47lSosSPddw4dkF1F7kO0OreRNOLUhiQBJxzNVirmB3HkdUaYKcnuG1
8d6w02XahyKYDd9fTTSt+dzUja+0k22wgN+TEmyp/Qwd8SQprh+THazA2WcOs/qXOpRsKI7JZPvi
yktrJTX/UxRHQZchVeYdJfHbFTsu+VbwLedALE13qApiLh7r0MbsJWLfzL9cKPCBiMp9PniX+UlJ
Ik5Hhn2zONIzBlCKLOn9RoiGEcAJfSlu9flPi1WpqiY7NvOXS20g6BnrOBnPLbIDMFTnlCGva2GH
nkpwYaRtQy5z5xMO+MrHz5kHvZ3fFSicXj0V33ROmUeLwdvxZzGXkBb0wbYof1RL5dkoPMe5f9pq
5fv0w2sQ6Wrde0KeQ/krpvgnnuVrL+PSojbaPmSni94HLuHila4XDMvd64/XKbevsjdmEamPPeUs
8x6n7EgZsETZpeHd5jGYtVzImNSv31Y2k0ATbe1ajkNhw59+Juatg1/JKDF5gcA+Nntg0pn6KmJB
5Mvbwy7aYTLny+p5egvM+zYnZ/4ruO6R9VUb8av8eLZvBBSIwFtdhhys/roNls9mV8YvRkhNGX99
aytXjw7bYWrfyK2u9PLNf7GFmOSj11ADC7trHCVhsmeqmO+RIvdjOZ+iUIM0vQcRgqcJq2Jvb263
r25rppTAPyC9GsJFTM63UOuUAjLdEKZyfoPoY470YthMD3B9pXebuk9JSzdbpnFm+FnCSrdhTC96
Wxgdo2dT1dIx6MOa15O+baV6mcCZqrzHzSrYLQcsEH5WZ3hwDXpKpIdPLebiRVMMs4ggN1vV2EO6
o/nQh1MH0Ko6DiZPmPrtynFk9fr3eANmItxHK4OZ6ee9y/Avs1SuWt/J9CmGsSF+bKbPe7VcFdsA
hi6cOkSSISsjuuIwNBOSTj0e6AjFGObBKpnnSu7USYV1F4qZOXZFPAlr1Gz7MzNEFq6tK5RWBxXT
AtaQNnIRTm4HqFZXmrUrdPZg4Hz8tQUi3nugOOLxHY8LJksOL/t6S1onzOz51suqHAYyHEgt6OwB
oqfpBdrH8GVUBZrWLPg/udlwbEnkMo81eHdA1hJnYZjPvG3CwGh+bnRsJ+t250G+7VouSFCSjrZr
IyXTa9+s3DipMU5RdwXc7zMvi5OYTnAz+UvfWfuutZMzpVDHEPa6JFQC0XwOOMVpfXAHxEmJfI/u
X/2VwmquIiPWuQ93pEpBZ4NoqnQM4CQk3OhDh+QcRVeoIWHSnCxJ7uCs5nO3Ik8Knn3dsM4Dl2Kk
VhxjJltimdwoIymO3escBNRnZVm2DndDeA98FEo1db966IbnMpoWmdvGcTMPhDA5JucHFr6t97C1
rkLUGxrWVCJauiWy5sWzkR/O5ECmo125QKD7BixSwM0D1Jte22UUZze/YdXpiAmm3EPRCYSS/ZN/
GXzfVsmEbJsfqzTyP4wMPZ8HRx2FDm7Aww2Dea12Ysn5Ss0Ab7MSgzIJZIx7/oRgQhQeKGpLFooZ
FeYiJFosndI/Uf1sKYhyyCGld8Df/keOmgs3Etu5+dfltSB3uEQKORzNUlst22C+LOpMWbsaByiM
it6DJKVm5JdrnQSJmYmeLC0ivGBuNIBVQXTA7eG8AY91vkvyMCaxFTjEx82n/RAP8LwSTj7bEfZ/
cqywBGfAG322feU602B/aMQT6UKd2815IdJM9wI/rt8pTXqVJ4ea2MTQa0/zho+ZW1W6F3P5nGUy
kw22ZNW1sDmGPu20+409MYCBfZE8qeNRCQTBlnUjnd0mxExZdj5+blD6DKTToR1GrAbmXyhzkEqR
5Rr+11KE2l5CsjR3l023gGrWcgV7IoFn1Ecn9KMy8x5M6LCuJEHMTc3LTGyoatIaXsMehnO+IS1v
5CjzLKgg658d78cUfGnwLsu5BL9x481VIrBO0kmkLQ2nsZ8lZljkjIXNpQwnaDgmKaFEboULfnrV
VVsRSfT2IBFBQ+jTbcUljzyEbfd/tMbP52uanGYxRt2knmrSYPVecHOMIz62LtjYtwGO50tas9KA
Fmq4tECPqYdKNWUJ2ECXfAU5akXGYQKEUb6PCtVIF0v8Hr8hpIqiDwldXbNKaHPOsdcaISO5RCh8
+dsK8SQWx6/Zs2KZ99Yg6zj2ZmK/NB61gSd4uWne4FVXVmEFBA3QYGY14KLc8LfCWtEQeq90Cwka
nROQ5GWbjWkTAzWGjsNYn6QMUcsGUO1Xja+VSwekwPgJXHvNHA9h6vM91Yvm/qu1/6yyK8nwLd18
gqAMa4MKCSXDK+//gdcWF3dw1XxE2dcwCq9EPoUoac4dwXEUCrewST5bB9tmYRZjv3bQ+vKePxjw
G2yu8AuXcMLrO8yhJGpAFXewRCz0B4qh3s1ifqPFV2+G6MC1r7F2fn3DK2nag+y/y+ml+dhCsJn2
rK178oXiKdSbJEuH3M5ikRJIOgSNQ675+Ta2vTunNUK2bLmTmjjNHahbEavE5asyhaf8vYSLTmnW
qX9eyaDLvELu0V/HZhUhe+cTNd8v/lsUO+I5lYCNjjfVDVMAGPFU2rI8MPzQe1qUdzx5FA7D92yx
3uf9jYajYIEutzaIbudq0F5+i/6M+oXBSidmAZ4SSKxWZ36duxCR/lRO7/32a0JsTlFMPM+fFZtf
F8FUuZ6MeDBNvVPba9VWRLRWyz35T0ypqk8tqRHQRWe5J9NvQtcw9VS28UaQ+LZDYwakjEisctKw
ifsyZt92XJi802tdvob1UTZ20u06z9JKVCsC2eJkjtdA/Y30QpMLQl9wkpG/Vf9zvi3w46HFD6Mp
XkII+IgApxvlXxXIPIiyAGnvPGV84ipgo0XrEUpDi+a7e1znhotTCU1aqvMpsbtyCJMKYVEnjW0L
WKpZzj4qdbxClJ+A+TL25tY+l41NtAXI1e8cDPpJAA/JrF+EcYD8PsnJB7uXNSUq7IhH2wPQe5F1
RQqYrc6l6Xyu21Fpb2/jDNmGbjsPnIcALCVHi9Mh7O3qTYHPHNJSQRxg78voysK0mSk7y5XOMSjv
Ogkklekq0ONGLkzep+wuzEkaUQ/MGkEqOF1ECk51/7+i2zfp7I93zNkXYRB3F9KdhbZv82qMyj7O
iwRhf+RPsT8I63Ey7W9SiWblxD2u8vop+THrBD2mmpdiqB2CqewZKIybu7ZUa/N7QQYpKd4vyXbg
tkShH8wNXS86Xva0Nq9J+AlHX6T2yqS/5ASvGrRKc3+J0A2nxu8D4xKVHIYQqNmA3yMmezo7QpTt
hmUClA9GKP6tSSkVEUlppCaGZJWeKbbjNBGWaWxJHGle04FwrcA2QzTCG7uzFTLXckndzB7aj56y
l15o3ykVFrc75ibmMseIGaNH+zrT8i/o744u5krxaWGzpe5BM8G1gVJYxlpJ/n9IbUNrdL5PdQJu
+bJRPPdBwZZWx2HxBL2qHUk1Rs/YFAV4LzLZzVXpluGh4ZyPt/5u4eW9k1Uq/hnWTGOFid1woK2s
cAXO8IODzbbzr+d9PX4pDfa2qGBk4e6b0qjwjXqNegWk67+kdgJWwRaegF1Sdi04lJ/34is8oq/k
xQmQx5BlODgRERvhUl4Ec95al9apr18yfXw0K/xNo0fp4+hYll5Bg/XUnEAvsn+ThqVeRF+lQLkE
1844hfDqu6UTC6WkH6rahOPC3r/JmpTgY5eJzBkdgCE+PkzuDeSvLm9zaMoBwzc5uE4vbwtL6odw
8mDC2C/aW4FgsGS6dwXbU6BSJxgI7tPvLSPnvFHXKeLJ+qe6FF/8WwJeafJqyVjW6wc7oW6Op91F
FNepPnTEBBK+ugOQ7PjpzfKqpXFaAH7uQVvGoXerUnRctRZU6j0xzNgsJ+UI0tjhTEzJEpA6W/ZB
aeC6XcyIX8L6haJ9SWkCqoQBsgXt6VSOnZfnGnaVn0IlcdsYTw0ZGVPrVIhGixcxfKiakVWPgPCD
IRgbebx3EdXCVzHx5Ty8ZkUJYQR2eqXhhg+9FFZNbZ5VrAw5MpQzkhr8hKqjLWwDt+kNJs+NzTua
Lq9GJAx0Pzt+s5H35jsbn6z1GzTPiywdVH28/i6R5wJm2wmK46v1Va2bZllD0FKxEsJwR4fBlzzV
0UCG/17tkh823Pe+IGNu/t4AoRZ4rAIixlc39Qv6owzsL+THNcLUfiX+0YaeEFkslwkAbfggHkfA
7atgl/QM4AVN/SOaHiwNpGntosoMSXJBwXD8RNURic36tk220v+di18wztFAYcwLc5PsrlAiqPvG
XBCMVxGyWlMy08WTSG/khx6V5MDtjNCldVfLYsAVW2sGykTCtofMLalQxR1L8ueYGvUOowr4XhYs
fkY5W7XL/CYLxItfGmc09VXNi/0UN0nTuklnaGTfDrBj8eRItApt/PFEHv77dDd4GsI9Th9B6AJJ
Sjz3srzTr0PGLWeytUF+IhI0yW4MdEC7OeG70xrQRvX61q3uUyXmKX6nQxiqz13xdJMnsuyNynMT
JkwbGm3TAo7zcm/ECusW35uUu0CHlYNaDJoVO0BQEEggQzSBjj+NXR3OcmpqWva8THfqEoiXLpAR
ZR4mBvjIZtjdG6/kZ792YUs0nAQaDGKB5f3WnERxiVyHav7z3PUGlAwrOyI4uqyKp32sfVPeu8uN
mGLwZErUfnGok+Plh7ATgDr0Pyx37KlZD8DcGICAzHn9rqVv9ry22p9QOZQAlTk0Giy0FLlmaR3p
Mw1vS67IYf6hA18r61YWrF83kJaPOsed572Q14KE35evlTHAezlZT2MJhNRtOVkYvkjAv97KHQzd
2UYGjDQbVsRlg3dL/+9y1aFmqN3hIaXJXVKer61VZ95Vzzc0L+nKskEeh+RFcSSHXYajhiJP5osx
0YnE777AnASkCQtEejz1nUHHz6nSCLZWAmwu1k9b6KgA1d1KLPiVN5gkKUOcmRWUT3bIAPRBTSGF
9cJ7OXwBOLy8jcObQNQc9B85aefUD+0s86j3EG0dSmtEGY+gY7rjfwlXRMsPMpO3UtPaPLCju72f
jRh4+qG/TLbuw4fS2u4M9Mo/mqJJ1X/pimK/l25lmlOTbXivsLHRx6XxC5sUDe5D8yJTN2QsBJTZ
D+RmyKXSYbcwZjCSHIT6dWb+9pEFbwB1Sfy8rV7h2oeoNg8EvgK112qo+mwkxVx+3GLv4zCdFKND
0tr1gtHH/IzfcwzqiQ9y12AsNLaaC5109qwTyarYxvcKRjcIrUnraIlFdGHbuYs6+PVqgUioDxMM
5WEBAttUmCsHJ1u6tGHeK8Ve8nm1ERA/g/jfNY0rFli8sOtWZ77lcUYldYck2ZY6+5qDatCtwg81
o1M6B3wQPqj7n0LeLNlHslhZU84vpyZrNwMCX46JOssYgmSXMGN0cCX4fQYSs8O73zJXoooPr/gZ
2W4CxN+vkEyN/v8yVlKSrfmnQBrePqXAY66haaXbHb55pncJJNnjyKvfOqovPWyW15IgKs8VWCGs
mGFzS8EElxdFfSn9FEMaRXAQQp/La6XfSEq1JxcqilHHntKCiJTys2S5ZA/SgKsHiBwgeculsS9E
xco3Ay1X0H68n6tkgTqpFLV5scnj4bN+gx5GbZncjl72guU6k9vNs4PZJg5QsX0DvEXm1z/ASYH4
+cRA/ts8K95WO3W6Yuyf6RXpYtQaCEoD94iBg2AzH3LV/Kai5j3AvemlY70xvkY2jETCPdPbc8Yg
Az1/cNp5L8KWT6G5qUn3OqOOza3YDJe707n3g3m/t2KlLyznsHDrY+DD/CAGDXnD5vTyU26epZ8R
LIyqPOlJmQ83ORDBtqdLghnWsGFzLn/rbN3Adp4K901RsiFDpeiK43Gyb2PdGhZfGnDG3JbTF7gh
qwGeUrzpqgehSqkt/Og9K0SM3/Z6ToXnpQrlwTGPpu6YynGl1YiYE/lGvmVqcLpt40j31kyNHwVr
ULe1aWmXLzmMDNxl9QHjk0P1QlDQGbcMPMx5CPBrQggLnwHr1ZnOL8BkxQOE+tvkYfz9ic9VaTb4
LpFGUfrybwKcEDNk0iBMAafUUv7WbAw+lI9SjejtdKh3XbtgK6z5QenfHsTofBe+g/Q+r3dE5Ebf
+c888/AvDRBGjrOMv+7lap8NRJ3ijFgI5FsvnhGzaG8v/7a9oFpK9b3lEeDJcTzX7oksUa4APYjs
BxL3iN1TPvn9U3xeN90poazvZzGzuqc3p/YR0oqB1bHLLBF7cYMty1T2t9oFOWpP/1E9j2CSfZAO
VlV9hORe5Dlp+rT037uCvEYLurOnjKMkVOdTnqiuyIqB2HmNjQWSNkdKVn/Gbtk0tVDduWbhpFdy
o6p6IrWYP/ONZ3g6zgdrEnc2HgpN2z/z6vEoA2vf6eZD3ESMzhRPbZ0TKBxtUPf3lxcaLUUGY+w6
DiUxH6K5NlRcanrBAGPJgJYCka08pwhOVYpYjcEHjfmq2GtdBy3B5dYe1YwNB6CrOK1sioifZRoe
4KdsZNHDzZHXWPlwDBz1lul0ldUIfam3hJwehET97oQsG0mwPMtt+kqqf3KOizGNtT01tp5A3sxH
xiOv8OLdGH+0tPHd/3t453GAnXL1A9ZiAxKan1Pht+g6JZjhIzG7uHug/W/wI7rJ5SBfPQwEP/s+
QcdqDfLNjHMMK/pAjstkOEqnaEiqh2uJb/pS+RbYfSNc9orJPtH2CnYzidNTvWogUIAExCWQ2n6V
3pq5C9w1U9XzUPCGD6BOMXG1M63YOrTrPIwHnmpKbUYCLUulUNjoj9xJTbiQltiuO1qrCpWob4tZ
9teYr+oUYdlldrvbhDldusDVZ2OqreSzagRyOMM/9KpiF1MFA5rLihxdkELXygwsRXt1KlgHnPHk
iTneavJplGkddr4zl6An5tfoNMtmh625rmvab4AUk1bMKZWbwEYlzsK9qJvdWD7jOIJdietjB4Wn
IN/uA2HREeXqxtqlLYQt6t2ReGiUPB8M50A9gzLfINncK2aMIhI5MWevsWugjyVB4p/8cuImqK99
ndWELyc4b+7jk6xwxR2teQ0noDnYhOu1mcghPnxMdzKRuS3SQb3oJV5jIHfgS3Ca4wGsKD5L0K0C
DB/UR7THca9PAeo8s/oueCT/84MKpbc/nNLGaVskQWm6cdoDAqa+MvyEUHLHdAcFazKqcUd9/Mmq
Rr4RwtBqypzwHO2etFE1jV+oJ3mrC6tg8zRGzDoBvBOqnbpXPm1JmQIArZDLRRIk78Fp9xmTs2bR
xpyGipus/Qr3m+fW9rOzVqeY2YrHa9nh576oeS0UKKEvFDEI+eKq9hzJc8v6C1GlcDn58v3x8e8H
O/e1wHTvsTPmbkG+UDsEvA4kY3QcbcB0fO9EBAvxeFRnlZzKWeO+r/IK0zYnlk7BvtisRp3EMP7+
85ABGAS0ndUU3SB9DuiKI3SbDcFkVPKCd6Hj7tM7tPxetV0tVKn9v46qdT4R4tbI31SyJbHXEWNK
m6yW4iKEg7K1Ex/vGYgBGWb5Qo36A+mxdsfw9i9tt9j2waIHmw0qosuPEGE9Gl4Lt/dPm55yhjJd
fski+FrLxcPqbUYHwKp9lqQBPtbM5rbzT3Lfw+WUbwE86Np4HWKikYBL2RKgfywbDWMFwQjl4L9C
qrzLH67vrjkNQKXG7KMmdN6SebkwSWoHlLReJLa7Yy8DoKv+8nqoLNseQ0OY1NiEEhTP6Bzq61Ma
F+uYuRi4QpAoPHyMBGyaJuYHjhM/sBzU6aMKC51CwT55n0WAe6kVvZ9pnV3xlKNBLjYPhaj+TRxa
6dQgXJsUjW+y/7/KWtGZmB9mGu+UI9pEeMxCW5tWAC59K54i5jXkb8bVAt82gbxJ0jjIiOELKLzC
AyLPXRNgQgd8WZuvSHv6sY9Y37V9xlKsiw3nbyL+Fhj2EydHHbYEQ+vwB3ltkkOxm3YvU1akphBN
JSui2i72oPokfbXtvKJlswdGleUjcYmYazfsUog0Ei3fghccEfGvA0TUGsTw9uhgFDuwS6IGKUPY
PQKBQHwUK+GcUoRJ7YWX2Vb2s/lrbXIvEaYIABE9v8cd4xY9ZuTnFOIu/zy2FY5Xn6bZrP9eme0d
k3w4wI1Gw2HEYDcKpu6TjMkvN1GP5FFDlnmDZC74FzhKaphl553u2Xv31wprvlIRzsPDkejFIBGl
UZAA4LLfniMyqtLITezU9ORHr1nK2iVZdDRhnNAg1YMdN9r538DHECWaWn+AXWFh6ECcsvycESA1
8YThiLtxVbV6UjhEhJT+QJhUrW5R17NCuDc7WBWASiQeB58Nat+/+u3rNabpKhr6RAkyUCwQLTrd
4N6zVkAqAUijt7KGoT/3LwBFotu7pPvFZ4U/9VmE2z99hZCebBRcIDh27MZwRKhFxJrpvFofBQz0
rc+0mY6seFxK8+/wMIwfCz4F5gxppZ647IahgUaFBeHqZnY/t5Om/kao6Y+kJ2tBNyD4w5GiujGc
x1UQSIPfhErdeyJGSLuqs6WTl6S5vkLayYr3T1Mv4o0Ylhz450BnyQUMYjJbzl0u78XOyhtAY38K
mrwGJiAzSevegOh3/9W6fvWTGCfCf5iNwpiEKr+OJOM7swTDZS3806kcZNaI0kM6D7GO9DsdhCgy
l748Fq9Ru8f5j2xfYMBuUQ1YV+jJvtkrxTDfuu2rjbS5Z6aaJXmiP1nOpNi+emuH480C2H5KceP4
bYXnGwrwHrmRt4CliEpzsXvpWXGjmBb5CkCL9rtc0Cphc6TJ8oLIOiLMjV9C2ZjgqYFdx+PSJEDL
h0twR+ziCR9isbT5VMLw6wjDZjHNI5lDAjWKy/IISH2D2JvR1+ae58wB68mHpWVPn3+rKnJ8mcuk
2sOJ5B+43Rnd4wvLeO3QJvHvTV03M3ZkhZwZxsMOI9wm6bQZTF9bKrjlq2iQmgHFhIgp35o9vj4i
EsaM2S9gaQnOjkSknxGfdyQfsjoOe4nRacD46qwDoaaQ9n0nj/uCENYTGolPR9LPfR23gkpJdfvK
z+H1ivLnJpKH9J1O1UKlfZ62+5w6OPbqufvSoOpjUWFS5rffiG9ykLlHNzfT2vNkIfPtgnbpC4AW
YewRclPfzTytA5tCmPAPYzVcrJMxrhgT9xQy4E3EYZPfPC2/dmqAreGGvpDOclbjP5HxlzgPUQEk
jL3m1SWt2EXB/lgd4LkvauIewWtsOU467ccP10NMOaVQahf2sMDO44TRxyMlPzonjsEiGZXJ9i9i
tNvdxW9SGiYivxMZd1Z6e+z3ZDS4Zk5GZTa/Ku/ZwSadquNf4Hdshj7e4iPN5q8GO8+2jVdIdEym
akFpBl3+N0LjNkIFICBcCNdQNAX1rt7BSFMrmMDiAcOso9q1V9IpZNckp40jUMZ8JBBurND7c/+m
IvnGiGDoQTg5PFRZTkczEND/AOYVYLsnYDBC96nqix8Dqviv3UimiL9h+iFYnpcnGWmU7M0prnXf
9giy88VdTQiQEiGwUi/6LAVNjyh4mjDYc39eZTbDj6LN2dmnQi/S/YvwRSJPwjlZ+slgEoR0bn43
gYKmOjVYp0511GWQXDPG7yM4o4EFX0fuVsb42OukWEfwY5w+K/QKUAxvYYte2t5KP91REG5pqhSB
F/CMsHtIvJyl4Jei15FN0r/rlAGAIXfgCt8E5Sw65iCKqzMXLFvoyOjKVPWHI8XvkcDEcgWCVVdf
iLPVFSx84evbgXDhYmMN8fiuTg6uTD8QDlytKbu5BnM+HaXf4vRYl6KRobccpJt3Mi6i1spg1dpa
pQ3Y6CLtS4y157pZUbHqb5O+NAMriHHCsV86bIPYth2sSAGhKNd9g7CBLzL38xL0L2fLardYvg94
qhBZrRb8REKIuCaI9g+31o6I6dc3oMARgtL7DKFFh+Ha+TNs5z3pwGDgJK3BcIxjjmZu4UgR08vU
KIoFQ1yD5Y1AjWj7jeZD80qKBUcNS9+ZCirx6b+L/SfhnQGSVjG/yXwiKRoMLKiX4PKiMchbu4et
90PbVx9IEG2W9dgYxwTv51ltcc+da7z1AcoGlNumQOzp/y2XdcPplf8ru+cNkOMcyfmk9wXfpULE
Hb7q5Brd+3JhCPasuLp6aVGRpl/e72sQe6chbD73WTJVCBxYlfVKHcQsFA1JOTns6zrR+DbA9ap1
ktH8qsELl0BiW9/SH+PcByTHdoPHeyVqldTGH7kga3x4Vv1WQGBuN1TEkruiJsb57vn0yTAJww2K
XF3wmn8n6v/zzraSvpfkUDm1QqveITWd+pxE2DSW+RPYYqrwD16F0cMmP9c28oXAv9f3Y+Nlqtxa
5++3VwkTqyFkXh/qiVOrgHf0wR5Pv6AxErG9kz4+WmCbkcjOoU33HsES8dNaNrPQzeYaEwVAAZhk
kZcz52Cdt3FfJWe2/3dmAYw5JqmRKy4SER2w72lwJh2GWpIfflweDktJG0qcJIhdGJ0Ha/uutYKr
wUt/aOVGAG0UzMn/6VgQFkKBj3TIbRQLiwMy/PrQtnV6LcYe45XVxN/0gZU59eKJzkBAvln7u8M7
MOQh1nS7Iif9MUgU6Yxvcu6DpgcGi34gIh4ePoQV9xgZHQmIc+F5uKJ92J7vqZ+nwFwruUAKDujw
PMEi2E1tbf7pQ5DoRhkw01KQsMzKk7dEqQ+e8l+Ncu5AdgDEM5KDmw0qaL463O0wmJw+CTto1eND
8E9Vy2RE7Pxj1CnbLjMCsmdxaVINgFnr63NIEcN8+HTT6Kc+FvTcKwdKdIH0l4aobrujczGgHT66
Ux8ZktEcEQXh9sWfUcGfnPd5shTrHhp3iLQfCVOkXxHQ0ao0nzqC6aN5R05qq4fG3fFJrqy9uDnZ
zwXrpNO39794MXoI7+EVsY6Abe6V9Hf7XGNRDLbMh4BdgHrRm17Cej7ArMX+oFYgO0/50TI66P9R
BfBh+ur4HArF+Jvm5x138i1bMw9Fuou2eV+XEeihwRan0huBP+pzUiWo4/Sj6ErmBrwyCiAjKEj3
6DdNZ5FgtmgP7JgP4cus5kCzGTb//eyrd1XgGrBAf3sTOXTZBlwdpDSyje0TyZLX0KH3/Quk3rsh
WwewPoSFmrL08Bs4P936EcRJ/6M7Zg51rE/kLiwRE37/YKkE2hQny1jGWgAkHOXP5rUA/QsQiV6s
YJVvvcCqToFhxNHqWRoWr9D2kuzNKNNJi6sX5jW6v0TTCHoaoU4urBhNYShix4HO86vK6d8ZZpMP
WLuSgeJeJ4RfHpAo4r0xq7yZQKHgcVS+djHCbN2Wg2W+W+/UbLHhqv/q4WuJ/2N2FQXT/JaCAA1Y
ra7p4XFzeyoUJeaundps7ikzUUfzgqKMHn8OsC1Wsf6fm0YqQAEJEjSrymKK6wHFPs6i7qGB2zhs
NHDfhRGtkHYYNDo+Tmbo05IWwirDnQIEnnb5CuJXnHlfDErXW+sJyWjsYKIVsr9qi+ndsy4/3xSz
pYYz8zU7w+60O0YgB85qyv3RBKuplRolGQbHWwwHNBp89NOEHqG9tT7PIYtkyrNYFguw8Ie5Rexh
1nD9tuV8+bPbZwr0pzpBJTaYV2L5lMhtyUsoVxaMVWsRIf4/9nG5X0rB8n9zqpTEtKLu/G+bnYeA
1bdc47mqT/tuRAPSm/prZS7qF5Y0I+zavHF/nBfmjViEINAauWYFHCMkGESFiUJI7mNmxwZM5FxV
AK+3Xu8t6ky83LHfmxUDWusR0m8OFEHZ2+7+OL1z8uIOirFjJApsx9Y5Zf0CxgA/NhzXR9WxEu92
8bFCBnPiC03fDmIlGmGS2rRBXwJDnbZIUPbhb/xjfksodD6qFLskwwFZaIZ3VI1ElwWWp/wDnJWb
nJmiUIGM+2xzRgpildXc1idYGe481eekS8q+G/4QKQVow2Xrd657AfTw4V9IPLXWlPmV1kCwlSRB
mXnFStgwWEH7UtGlJTcluNoDCOHlmF1G6pR2w7lAwmn/zcQEnrAnH0nMZo2ID62F+qs2MWQrMxqc
Ft9jMf2K56DVU+ffrOJHKcnArOJ9ewHTOYgqT7cJI8y9RlioN9BZYrhw7S52nzIUPCddl8qWsNOn
cPVKfhKXbd7IWxaNyfyLskXxoJRIRP7qNG+tpJsLdRuFhIwFNJ8gf29TLLRJSn6drgAJWwpYaeK+
g+hMXUBbs/1HDAZAqCa0ka9C+l0yZDNZN5wTFgbVySN3qIPOxcgy0ZRkQz9Pjtnq8Nv7BgVbzYzR
w0CvTgOL+YsF7B6XJJvu8t4U/YxfOxpd5DqZ/uFhGbq4VmqSlcCwShILlVOE8Pc6SdX3G19wSir8
Mv1ZlAPz3/Ve/qO4dIvVg0K8fmKMdrz7+e+UZQ9m1vvDsAwe2hXE2jtC/RLO04x0exMJfBdZWmEg
1csAdVgf2VQfjMs0DqTAMlCf5hted1uffmPOBtUxE8kR/55GCeea2QVCSde74K3d9IzzuiBOGxof
OC/vJaTDi2TdHecLDPA/QnDRyNiWChOghAk6clUhrXb0bBwRtv6wdSUnJkNPNWqqnMDkhSX6350z
vnqu9jf7snLB7cgQz9hPYTsV0GHQcs4WmNc3Q2FtvUh8EGhJTx8Ck2XM21AT+wBiC107PQ2DpTxC
nRCV6viuzHoPUtMRNq295ahNXKjfNL5jvK6NAUVOIFqOo58x2wZk3bhmX/liCAs0ZwlxngrS1esQ
sdTtNroR9Sw/JFkzeJ0UVULjZE2k4bG3o7krjwgVSNeUhQKrfsDhJ8/6AuZVPS2JbPNYvMQ0BuLg
Zcos7VarrvJMhd5LMW0Op7npClIV95fIqdqqxSyXLQVWqKMvlpartwT6/MiKD+l61g3QjSZp+yme
+cLG4pdDvCgjiFIfV52Jjb27bkkk8tz9+L1nKR8cLdAmFp24lxDoTXst3giHJdik4QE4BwS+U3BU
6+1FrrL5xHLD7NdetThPSz5MFlrulyeXu3Lx//tGWPWrvMU7FZLvLO7lz36MUl7yVShkwChFXpSi
nhgfcKlbMfubdZUVOSu7PALzQSr2ztPUS26iyPCCfeVBQ1IALTrBLhHP9HShqhXARqYvIN7PVUIW
R13NnhCl9dt6SI+o9QWpjnoDshzNWAJw489sAW2o5FeS4/CFMG5jDMBO5BA6HY6/Q+YF2fwL7nd2
RMMYTkI2+O55LvTp54QqLTSHCiHXtp6eSPiuRQ+S1tuPoxkeyu8pY312qqkhuqTz+LZ9QsR8n3cU
ZBXnw6UxUGtquh5oDQMTqiW3G34/H4ToQ+9rvU7dzT3sdgOIZgoGc3/HBE/Pn6HEeeIGp2Cd9KWE
jjZicTnOWCQGzIAPXS7KbZmewsDRPytuCcLHr5m102FBJ0EkGOwt9A8rtoXFPWTYbV0dCw6QKfiB
+rDYMw2dRu5hf/gJT2Qr0ifMrVUQfJWJpuzCS7S2NwhmgNxZ+qdtecg+5Nl7iwZNNreanPyRHFgT
SoVki81tOCbWZ9oeYWMlRwvZV6CTUnScn5Cy6iVZvDibaQza4CZIMXHfOu6JiDdtaHlyIIw1w2mn
7NA8si2OiX9dSW484fq4iRjAkKM3FAuZYEjlWEygUScPnz2j3X3+Ycf3zf9w9uxjGC/FtFBN12SA
+ECAohjxzY7FH+bgLu2MUiBZuTXBrBTcmgWWHz1zqVuivn6SmnpYj0RBWRu4Qlza8Q5u3EbB974n
lnj1ZDz+zTuwJcXxgWe4D8ozunlWaKHNbDEOxJ5dxFYz+NuJdsZAd8jKdrOnlRC4oFtNgdiAFdB2
nEAwDS5H5xwspK0Uckon/eL+kd1sfoQ/C+rCAi3RJ4jzJptmBiewp3ESh1iG6j1wKU0DTNflsdDl
pu1kEaj9jnLe22HgpQ3Hdro6T85bTa4/IvF2JracCPWkBmKqyIpCjc1k+a5JmV9m/ASXAGvB3T27
m7Z3WcPzUriNKDLQMg5BYIMLyQPjdxLYgPI0D3bfkleDtTxgWzk+pC7M5YzZOZnE1vi34N+WjOyh
OUdGW3qEDNjJxDXbpOzedh+6lDa0cAsI2WnU2Uvz9R2bB6CcLUEbGpJOvRAfE8DgcaZyHeLKv6YG
SlhR4jCZO1zMnejiWwcwqPe1tfU94Nqwht78gXbLkXbueTumWM1Lc9fH0hmXiKhqeJ5UF6XOr/oC
GR1LBNP64BHwjHGrUh8YtnypcjdI/n1wjhlk27xMkKy0ZF3tx/407lsUKuTZSzCBLn/CpIzpr/k9
XB0WG6l3nHwG2TkZzAsKogS9mO2hait2r6p6it8iS7DrrXoqw518vbTUapv09Jj86wu0kbXiVH1i
S14EmYJ17j0BlPPxhDDC5MiqFRxnJjmBITGMolhnFrZ2kMz3PtB85EvFlxREL7kCrSs8+F+TG1cJ
rP2oYdo3xnXyftOnRNNp6SNzCyrQBzmImKMhPWxP2NNQ+qVCOYcpi48ZbhStTjIo7syE3tgVfEbR
mySxQeiA0MThwdik5QFyaeEdCd9Urw2tiIOoJtgZFyuRGsHshNPOZQe20ZHjkK2b3NFTF68CHJqm
3y2R9tyLlw2hxTFV48ue9feH2KZ/uwAoBPjKfCw1WI75BvQiWMFRPdJ47aIv8xspCVeq0F4qj4is
2oWeyb+eXc5Y5WkpITiLiuGpD3UltDQi4DTCRdw12sQg/cIcEcHZKg97c3L1iKxQrC9RZd1+AuqM
oCjx1QdXfWpkWhPqnd/+Vm4Br0teWQpUOLV/dwTJYC9S95/rgsouLcNN6B5e/uzPNbbjRDlS0eO6
WkFRVF+ec99micnXTRgxSvp3WEbSYHSAazH2U04RL6PJT7hSKloF4ZNT5YMCRviPvJjX8NysNpfP
yJZ7zmzdCvH6AV9duSSNtUBQPbGhd3c34mOYmbztMC0JLQQfulszntCEubfO0aDXtO+IxckcVbPa
/aeRP5nyVKWuhs79sH2dQ6nygC72N7ASKBj7h8Qr6hXv4dh9fvNiyO3kfLn5DF02UfGfQDOLytze
bLJeP61HwxhzxI4K06MTdvbG8VHBjsl3mR7HT+/xa040kvJFF2H68F0YMPtCJkV55ykWA3Iwf4Hu
kGLFNJAhO12urEMYOMBG6xam+SMl/ooj3+fwzQ46J9nBOodEPyZCtThbmHYCJJDpZdBy4/6tN++M
INjZxiRVDUTF4YQteHS9oFhoaL6meqyRhrQazJ1ZgFsxqPy54ecfkm6MAdnjiZb7MgqC4wjc+EM4
ANSN2ah7dCAQSUJC0SXuMGuVccLtrWfpudi3Sn5QlDXfQIR3i9euIp0pRv2FNdaqlOiVsNtqjI8v
PH3iqiVcNVN4IgSP1BCmUGMn1aviNiOPXGfS392iyo09DOv0uG5WB70/Ebq1M9++EssSbEuvq2VB
SkRVT6xXnKgO+B9JM5TCIY+YTgnu/K3bzy9uVGo1vM/quIbyI2FpE72IMtCJHKlkQQ6MUOQsv2xy
wRXsdqUE8yvN5XLjE9rYKJ8tbTXsEt/kWJo9xfxw20QHwkerce4RVOYL/Hr8VlVdS3wRm8Duene4
90gLo+/tfP2zXZQVDdSxXlKE0XMUIq4tsTrv+kzdjKo6rKWjCu9xtfmRnqosWpRIsgAIf/V+pDz9
41duAdHkjQyPdn2ZWE1o33WNKNMYv6cUTgLcjGIZwG5LXvn47B7qLym/ecegEp+MJtqAdy2EFRHK
2XXTFArE3LUyA7uWPkiQb6GQE1YXodPgr+aUYdJ5PymvybLTlQwbuhHR58b8skcpQmuc4YKUdCHt
vLqFbaPZ4QNdfR7h01K0bc+ju4p2xfzBr2WwTQ7H09dCwQDVcRl32fYF8CVkYZaaa4g4N8xFuC9z
RYHUQGERjUK4OOKoe5S2nR/gAxYOTRXqXMMv562upWvHiQmg6NghWU4d+ftjsTRfZnHU2lW5lWPU
LMhyuvDoYslk456idhB4Bok/IE40hG2OQmw0XXExFwgBeuy6OdbECk1oOGnkY2IKejo6VfJdigmR
y/4ecQJvEwcM0Hyl/7N6CzNJOPdNG22sd01h5958Y36+YOEK6Dx2TXITvrWV0dZ+PUtFw0Ou+gTi
RMQFesWFs3Ld1N7b9YNiwDMs9RwkWdtoq/tvw71Zbvw1nVZumSQ2dvmLL0Ym543jTNqV80wJP33o
nwhRhTddHUxs/k0HsW3dekBZhZZGUqEn4nQz7rPvK3b8mkvrnJQqcptSASby1NLmkJLcB1Taxt77
pazrA0djsZy4IG/Zmd0NM3fcrTYKV6IzAF2LBRj6CZejea9vO36OG2vE0svF/AUZBCCkpRoapQTZ
7PsuAthlBVaWFcK5RFwIMqni5v0MyI8a2FIDRlAKa5Lh94Qk30E3IgWg6I3Xt87e2k0JYdh33kqF
L0PwGwvGbCgVK+j0BKB3OvHNFGaptTimotpBllXg2AOElzpCKBJn8Hv4/SVW+6YJNY/KJa2KifAv
LImjp5jbNenzzIpGanQ1nTp3SeITesjsCyp0c2FEnlX+v9yLev/Zqigsd5X5lcauf2Z8lkq+Xr5W
Ka8ssQP48tHs4/IpLqJjJm8zQUlEU/r4ixhzCUgETNVKuNSCR0nHH1StkNcYZJu5AbxGMRxJ2BjB
RNyaENXRZb4pyWrgSbYz9/onmnD/zVTi+/Lh7fJs+CZWElvmTaieRAV52UxY1ryUWPTW6bUsBt5F
Mw+VvfLnBBBJEUgioxzCVlibTYfGN0SFYeJCzIL3jD45ZJh3/Nhq25YtZzVFYtxXjVTI7/Xx5Wd9
jJGC8fFa5HATadxJuTuHImKWNCyx4M0bzQAdyzdhdw4l8RaRc0CpiA+7UQCPeHpSfbdXH9oeqh9d
ifZBrRziHXJUY64iiXzLPxPjLPVARJYBC/HDRkYewfd+XVdTh5/kN3AU025kV69rz14XXpuEiUbg
1fg97WrBSdrQOG1IagkYGhCrZbCFemROmXFnLA/oKmBEvufWfnVhT/4jfxBplGLp8rDuOiONQivy
t0SXRPA4y0usqpTOUByo9riVxR3vrd4A22Ss4tyD4AMRLkZH/fZNzHiHkEy9BHg2JnvHzT29pSHp
ghKmk9p/JnZGV7w22TYdze2VXL63hfVLmWppWWNOtMGAOdqvb+YK+4eOJVQia1KsexEkcFWOXrvr
snQ/69SKNEj8/QmYmKUkjvofaq8/eQsCma2fv1Pq9ugEtZaik+q/T1IwSHnnaq/qcgYRa7qT3zww
sYoyd18nHOTK5C3IW50DqWQFGIinhvJa1kvFoe/dH/jm2Q1+lxiHjze2+plO1rbQNYBPKtG5ziVD
njWH6yKUoTvE5oMLik58xrZxZDl3/UO/fBwDCC0hMbtaPX0/wtAwpIXyjPuR8MozksTqtFcVEBoB
5wO2+d3SMMOxyJYssgkFwsvD1xHOb5AdL7gfp3eIf61nnQwEEvDk96to8QK2B4baRxQvnts74nz3
2sqW4v0B6SRBJefVczOCRUKUEO3Mlj0dYy4EGOm/mQ7t4WIn6KY/CQQUVoPUPWTSb+sPOyLOiAVV
7eweCxfFLGPwpCCFcSxr8jUYW/Fh669Fee8IgcYJ/q9U/0hlvRS5XYRQtZhNz4/VgOzbXZlwtikp
1MSmGn01ku6qzwqWzpjohBsIBKyzyL8hHxFiS7jJn3g4bmP2pZ5RCYNDVr3qYrWRpeLnk8h3YGzs
S3dJUyTgf0YveqF13oNqS4ejXr8FeiB04gdxRx0YU7GCSIU4UXib7hus7OgFVKK7sFKuYK0BknXr
6lh11c5zMuojpzOQIzVwHO+dl2GbJkPrLqysR3SfUfMD2je+PfUKAoSkRQaq2ImFFfVD9mNcAn+/
9/jL/h9BqrkoiaX8Tlq+xzE72p7MrfkuC6P8tNVSTowxIx8Lroihpp3gmy7xIM3XQb6AlLTLy20d
IZmMXdg/HiVd+oflZxeRsWcgL5uNd6V9CaWPLo9mIKFny4ID/lTlB4WVA82nlVKCk/+jI0515Hy5
/v0phfSPdZsvIwigFakQdKFPsE1KzBYhzc6NIVwRe+tL8YB9NEQY2woVnJs3K0d8GgodQfLGMHM2
y/UYzesou4UpgyLLJlVM94wS82K//60c42yjm/9ZVeVZdjZ//8qnlJIiWGYvLW3s3QP2jwUIJYFl
RLQz19pjZWwYMUp0EOex8l+QivUPCSTm7H9VJsfVGgxrsjGv7eN9Lhjvl5qqw7rvqIzdOEqMXUWW
g+ZKLZ+xmoTCJUQQWNLTNppbdNG620jF6VDYERWP5L2qR4Po0BTFVfEpwmYkecGjd4iJ5JfMNltV
Zaj5YvxaIHC97zdjE8pBMEcPxiusOa6hBZViLFJIUNaM+DYWsGWBWigI7KOWBi4e7qhZlrSKLQYf
aZWTMvEJ3UwaOKPxn7IvRZwcIOkP8zSC0vlNsGpOP+Atvc8OUJdqocVnq/4PmrhYYFVOq0aW+9e/
5pMwAlf2oq+MJSfBOBwNO7hytNGQBi7FpvRxDWn3k2oenHlQ9Jdzbg8cqNKgYzpwR2tWnYzUexjJ
bdWwovHiTEFJZWjSIO3km97XNLOP8pJcuxZNkNsS8WyniPOtG3SU/7nk/VkLxlx9rZrpX/0SvDWM
feukpEty6Sqy0cBE69AOv9zQkq/vL2PeBcm9+PN2g7G4Rx1zoyIu0qdnTkti1/QGKcvmMweHZuMe
8lS3yaqKkM3AtOoh0BQMS7TItIuMD5TbOg97PNVai5j8iQ0MSVpPeGJvx8JUyJ2YXoYegUJd+wt5
Cxr5ciOyyrRGrYKL0DqtmhSOZa++ig3Cq0FNOZ1e+MW4ZBGByfIGeWz3ohnlD7j1dYb0UtCdS8rB
ZeYXnnmhIRnRXCgp8bqDAgULZlBb2w6uKtHIX5xLGHsXmqJpSVUtX3PpbX3RqUtOGjkWm3qCtVed
MrS8pr7LCdZrCrfCRkhw4FmViP6RPoWpnsc2BBsIj5bbwASRfWDeIY9Dbgo+alTNShxZpy+g2oYi
h3b11/h1q1uU8Sfu4HK87P0CGiXozLX31BK/44q+F8QnncRo5B1dKgMmZA02wrrp/wr5OS84po3E
jZJxIU/cV+u04X4oQYv1IEmC4cjOmJ+ucWhq2cNIKTpqRkcHhPRPVCtwLg0leLVDzYEkEhDuGBG/
ovG/tEAlU8gjwpWjgC81sEXRtT+lqsU5yh3xCP6cUEc/6Fu8DMfBXOBYRV9k4BTioSXv38xkV0eW
3RCqAPzoUpqKdTf2BnAYg2JQLsE8WIPwot/Lu3gDEcpL93VXnSZerhMGVnjfZo7Sdutj4WQSR/6g
7kUwTykJ7426IL8oEHHWfKYVNl+cGhHg1Zbdi7sjqu++wqdTNOoSSIso2hfRur13ogy7VJ1g6BWh
97vwjrktwBwVMUXFir9VORcp+ajC0ZAxQGPmnc2YBvRr1NkPUgpHRRQ6oNQsdrdyCP3iFyN4Wxrr
nnqTsuiIIE0Hj1IFqJsTCcVasigvlnCfGWqAq86z+WT14lh/yvPiA618hPI2rpoOuudPYu7AMxEB
JJ9h+k8BXI8YGSGFdB797toK7FeSseeI6YmW6zpdR44tTHgQkdGQhkBDfTEFCwfeuXcBLWX82WX4
zl3jp5X7AY1RvjyyzzdKebdG7LSCX1qfsJKh4Kxf5iAbIMqRW9ibT13SaEzv8SRF3T/ZF8O8dOVn
apsIK7/uFwmvrwvXvPxUC6UZGTInoKzbeXc7Kak6PzRIAun7q2IsekBzxW8SuSNE73P2hmM3ByiD
JeDCf9KOD0psH9MoW2U+nsEtADT9BaekIK/mXyMDkI8XLIhh027/Y8woR8bmf9CUrmZZLSLCNWlw
NcMKEjiJe1JcVQjFrjMw4eRQRqoanZoBZgTey4iT57rIStW2LJulQTEbtXyCuFDyHNp4HfGelcEc
G4L/mX3egcUPnJSQMfSOpVDVFN9Fvl7Q9y+HsB21jKXJhBl25SIp+X9nlxvrw0SuP+z4m6O+JyeX
2eQ9/3cnLQj2JPjLA+MzAdfSc3u+POo+PBJ98FzoXI9J/HYpXrcRGt8r/Td++3HSLUFw/de29qiu
8Q6MiM//RTkFh7jinYNt/78n7YlqzxHSTXnSd9BKUT0iBU1zNoUG/HnRkjJI+v/1MaAZa2LTQbQo
psRpmb7r9Fj+VKwD4EuoTW7R1Z0SmviCE71JCK8nOaDP4cOCkVq7qfCJ6GXpRxPF5LSKNc+7cGOX
QTZ0NgaQw6F0/PMCjap4Pjv0aEVESkBkPZn2iKYXYgWxJf4lDTVbdko7qRaIeZp4/CIS+dzfanJu
8gnpNBhiUTJqwe7LeCM9asy3H7XhDgBRGMrg8j6wFgv5WlHmHZu1qn7D2FdPj4oJUE4I8WIBhzAt
8LJ4LN3yKMUrIjzpXTzxx1jHJbAZ/GyllMVDn619pZ0DIcuLS/+WftidYAvMrSmCQfFjAO/2AOqO
WqnLET7ncxnqWEaMKr444egesEbkN9fa+nr6tkokiGk9D3uy9klOko0WxyyGSYe5L9h+gp6zrB5b
XmlsAU0N84b0MIsI/PnJsZWcuXTmmy7uiSuZoElZEl6nmRpFGVa6RIvZRB744yvJfDQFTWaxJixQ
Fdmr1K1NEXZ63cK+i4Ndh3j2FhInL9HVSadHj6nSfQXvM+Lwjz7DNXWGIIj11lM/UB/PLW3ck1Oj
XJ77HvIFL21MntHtDJYzcoCA7IpyPVwK21lKJBWuieyovaajUnkRmg2yill6QG+ZssS4ER1Vxlrh
j3+zYuUnQ4oGvFitC36heP18ncfxJRJzfXwwF2/q8fbo3oUDLzfby6xiMGpkzV5D1BxmhXlcpWr1
lXAQ4XrKcxz9xXij25b4xgXhUmWyjm4fQcq6RahGleymd4dkZrcLTEf56r41/jIEXIlDaUfDK0sY
NyBknNN6AIuMN+G4y8LE8xVSldQFxj36FZuuFABbXAynyE6SvXok/wpttDfU5ILrhMQ3hEO2OwRN
YBqp9+ATRhg7idHF3KPMfHoev42xpN0sbo1O09Um7HEppnjpaLT6PqoY+lalLFIZwW2HAS+/dPtv
3pKTwDl5zG4mu57ZE/mQCtEF8LSkIwe/Ks4zM+HBhphwXnB7ZApuH6KQ2h/+ISIpj/7jYRt7UagN
8QHirbQKdKwZZqeoVkk3XSv64P2T7EmhrqkMP510ORrFE3NQIozGwM3I63YDtDf7lr7Zavj5GTxi
hv5PGes68xbLciEblzo5kdQJsP+aJVHazDlGNSAJJiTHSJdgRvbG6kRBlTsfIR2uM2EOwX8M8+Jv
MN9Sjarj18sKc7f1nYlsHvYsnGgafqFR8EoNOZRjW3Dtu7rCQockEl0FULKP2JPGysXZTyR/rqnb
94rnq4TMUSnsBHsrHV6bx7/cXpIq59X/damwgY8MuvIr7fyvE8Q0qOg7bz7DO3yomn0NxVDLTPCX
KZVpqXMpcIOMVrqPWcbsAqUllbl6UL0tb4QC0nF4i5BnXhdiLymUjfYa6EDjnoPLvulkS/v66Cui
g7a28hUkwuu8r9zgr+DhvNIl68yFceZTrvjxwGGTPl+RPnRtoFHyFN8F8bwyGFOQcVHcT538NIqc
4ddr33YSx0y1fSlY2FtC0xRs4GDkOh9zG7/2CjN31S9C2wPZAly+ogfuPSyI3QRC4uYEm29m6JSQ
QHJ+1hOAOCtcMt5gNQr9PTcQ5+b3W7QcYRfbHABFUiDCAyqSspB8uvWoPoso+2nDRN73A0QwzvRn
7ev5JffsNtSMVonPJ01Mv4/CTWlNULATIcnXrcaysZLfl9jVahLhzD43TP2kIdS3JSj45GosyXjB
wkXLUP+0ETl24F2SWjF7lsePhg6lbrSc7u/0mvxFnI6rxmkl+ipGpx5RTYnC0N5xi+XOOuUbU6v3
r87y3Wgk1QkfR29M/XLcL2BYvV8I3mfKoV35FjUS/1TXySVAYUIx2lSMsY6eQc9gY+/ekQKKrjFr
1Z26WyHH3iEszgo30rd3OdoF0r7FqskBO3Igo0Kn
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity shift_ram_adc_iddr_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 7;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 2;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "kintexu";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of shift_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "yes";
end shift_ram_adc_iddr_c_shift_ram_v12_0_14;

architecture STRUCTURE of shift_ram_adc_iddr_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00";
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "00";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 2;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00";
  attribute c_depth of i_synth : label is 7;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "kintexu";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.shift_ram_adc_iddr_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity shift_ram_adc_iddr is
  port (
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of shift_ram_adc_iddr : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of shift_ram_adc_iddr : entity is "shift_ram_adc_iddr,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of shift_ram_adc_iddr : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of shift_ram_adc_iddr : entity is "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614";
end shift_ram_adc_iddr;

architecture STRUCTURE of shift_ram_adc_iddr is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 2;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 7;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "kintexu";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of SSET : signal is "xilinx.com:signal:data:1.0 sset_intf DATA";
  attribute x_interface_parameter of SSET : signal is "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.shift_ram_adc_iddr_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
