// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed May 27 20:03:06 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/shift_ram_adc_iddr_synth_1/shift_ram_adc_iddr_sim_netlist.v
// Design      : shift_ram_adc_iddr
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "shift_ram_adc_iddr,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module shift_ram_adc_iddr
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  shift_ram_adc_iddr_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "7" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module shift_ram_adc_iddr_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  shift_ram_adc_iddr_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z+kogG8pzUGMqXdAQYYW63MXibnFnLBKc3DsQGBrOPmAfDb9zhA4NxTmvV1tEyZly4G6LxGZBSbB
5bxBY3MRJtwIH2b9+29ge+EY4ohspQN5qxyw9I7NvHJzjf5ZSRRknkdWVsUhuQ4C4L43yVddE7+R
FAYFbd4tmv+QpHY6iNKfQabi0OvTnrhXzkYxhiwSy/4Jo2rLsaC+kqbtacE+fZ+IbRPxYJfA8pqA
E5fp9NInOhreT1VB0ynxqMdqgvduR6c2ghvn3QokIbJhnEHV1UabqjGbOurrTUmh4mA1RR/tvCMR
iP9JidwU0FKbSR4t8rvLzEwZJbnqwu04uuS6KA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
F/KyYoCX/cVGmEnR8jyYY72opsZTPmrIyRuhUmaCuu2CGNfpFp+ixrHpqCASslILiB76H7AFkpTN
hKGFzJ2DPT2El7KBIWEvA7C3mFnc8hmf8o6bIOGyCj14Mg49g9R4tTw0FdK8uzJMUqWuN8Ve47tL
yRgzZbV4VIAoI+6U7z5zRV5JS+EbcPLGrz0kC4aKAs16yRIJS1AfKqo8QT13AqzuJlDSmYp7tInW
i71/QZQIovAOJkTo53zTkmMJ+ZU8nTvloonptHK2kKtOXOHO/TiMJsUGoM8ZDQF1Xfr8XXywiMn7
vJGY/RSXwe+1h8EaGGFkSh/YjXWg49NmGPQWWA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7264)
`pragma protect data_block
6Cyi+68GeW0hrzqWV2CX2wwmXgeNdsNLcxDvbtJ147jqq1syO6lM5GyUmFEum3v30y6UuiAgR5yR
LbXKw9A+nMRzGSRhoPCLHwfUJfzFF4JfawWSa7E5uR188ISrL919U2iZofI+jas2hJa67O1TX0wM
HcJswislX38EaHUpA93pzTfKFpt5aV825A12HbA4xiHLz+hm7lFeueHifdtlMAIiuM+TDnG56a9C
xDK7oFbpuWaBIFC/oziOJ8k9OefpCDDy4NXu5zVENw/rAytHn8dKqWs/aXPYr5zyoCFr0Xvldwyy
hZs7IFOWDS6wbxprC06USVb/kAWKqfRgVzbep22ijL/AT7ssW0TdZhoqBlwDcz46SC06d3h9/OLM
yoeSGnkQx8YFajGZ/JCU8+mmKfTtPnmxFn5cUBBTzozvBRiM8N1RicPbV29iqYHOXjOv3AuRK4YT
bzSqk4o7Lda0BCawn8zQip5128Nncp5iTbVM0EAGGg2NTg4BftHReu7VDf7se3V+dDW2Vz0tVOM1
Z5jE/Szxd0vefsGkxEE3VqPcsebtq+BsyLvkNGvSwN3/pW78kU0nw/19yw0AKatvC1woIClnSBX/
V21z1d17BShYOUS+6dFgJ1gBR5X8ZOOL85o12mi+fftJ9tAHLpJjLcHrfULQbjWU3beMDQLBbm49
FKwo/0owcV/K0+18fyK6Wlr1951g1DdrAaUwRKzOquRAyMod3znKuNYkQdvIy7gsjgOsEcpLWLV/
IFguC1h3An2/xEad208fnzqF16Dq28dcgsquiqAF7Xk7U8lV4HAU/Tpxe0svcaYJdrb9WSjx6SAD
k74n3xtVURlOo2MrPVkyQCXqlhkSZWV8F/its8PWA0qF0oIPc36ewmkiBw7zP9NbBmTW8vpJ4Sff
Wc9IXte1JfK6dC3LV3cHj+VG9aH5tkigB5T/rFhHX7HV7iQ7KbyuRXVuLdoR08Evf+RKLCnVAgfm
o9V8yMXDvMJzasJFiBGYA8BschsyIduLov4CyBRR/yLomFJk4JFPszL52QKfGBxvwvh0nA34ND1S
fCeXiyyr8JbjJaeNeWE10ijwvko0r8s4TBWYpyr/w51KxYkyC3jX+FbVHgNGbs25Duy0G4Rkmwqs
stN2utd4wDxnloM8roiw9TIkH1GbUYYeBlo9+O7Zb/07oyPpQoEWUAG5cOQF+5g0FMtZYSSO+tUJ
43w4V3H8t4dZp4d2lk5kamyLflzoI5PNCsPlUae3u3kfiabiasf3FHeK4V1wy+blY+jG+mUZvJoL
lf32LyqXdEmC+aiDfO8qXu+vsELr94cjAzbllHBFvtOrtD0Mq1tfaQyt0umzd/OQ/QtEI+95dh9P
m08pCL7+Uc++PvlNA3q00pmA5baxvyDXee5NmWjCVbJqA6nHd+skoC+bZWwXgn6KXPt0lCR7jO3n
iwX9xhFa4U1TFnMRa0aUiXRXo+7PYtd0V2guLkGJAFG/owZ54nrFf3Gsz3uWguTkDY2g+J1dz9Qn
gBp0xkWxVqT3LctAZE1ESqjL22GIFKOxvv1V6os18L5U/t432mPdM9uVqeH8qrs/ObbFHI0iQ4KD
arYVzboPZPrbUwMKxQUSqQ6iccsoyYuDHwkBM4TPEe2mA5epgqE5LP0G8d60tHoXyvHIgZMyfUVC
VQ3GLjHvwUPGfePavgGQ/LeICcjh3Q/1WuyQppR4Pgj0LXz1iW+mBOu/ebfMmi1JdPEKLXG3rYEF
aEEkWsT4pdcN0Bg9nmeYIHJ+m0Z8pHJiiKPq7ZXQDoJ18sc0DNAUI+VwHrJQmyEJQ2ys7WupzTYp
kLSANkcTUQClm2qmxhc+6Ugu7sNr8L3PgOSc9BVpdgyygdaD5vxPIRdEtvS4yer8CrWWbLpViG6q
FkXbbn11fBpeBraTEfp67UMI9GxE3gVW6ium4I5Q231QQ1UMa3cOICMmeJ2IZnc+vyvbVr956Ith
HwkL7BCRanohgqbXM3xrmV83pIAm2pot+8nM8yk7IdopEHiteUCyvAhSste7sEn3R8MiAJlQYdx9
KMLXqGTha+cmDRJXdm799r1Wccpts8qkAR8wPQ/IAeMEtuqIqmewZYLH9yu/+/CLXnDLVbW4dyTx
/VXKayd+zgLgiZJYnyWmV6VLA4s4CulUvE9YP4FC6aSFR8YJ70jzoheWLQ62g8QBQUgH2oB2aMLG
8mz0xEzhIH8TCioTk13IBabliHdoO2SX6MdbUTT5ZY0vrjZdXscpgbmCbkriCweNEqos76sNGohx
KkmVk/hTNUuuMNwRktI/R/rEDh9I+zUcKTZxt+tiKlvqKvedbgTFvM81Ax4OWQ/xrs9LxM99ZHht
F0WoifJQc8OAsAOsigFuTM3CCiQ57jkYdeE9UkWdyS3WFWNVRr646ivN2hORjR7Q2WwO/UfEWKwx
YPu+vS+nRv6SAHbbCflDXi7Xb7DpZ0O7910zL+D5PGrCySwJjeW/VA6g4RSUS/Hlu5+I4VC0a3FD
uT8+Ym7Yq5iPOG18eyz5+UrGdYcHNinNuRxuZhKjP3nxS5UsG2rvJPmX8gXPyftp2GmehbzgODRp
m/v256jb7THFEN3ZattHTtgdjHYBnw11dVg6/BrhIRgP22hG1VF0469JvevfCb7DMHA1DQWdwpUo
rDOqtDu1z3RrvNzl4DZphZz+6AiwSrxUKLxfwb7zb4yVjSxsDUsaa47nj3E72g0SprnAZMhoDGaZ
JutcLyK9j+pRx0kz2jy+4IqKLTfW3uHVc6dlMRPvFX/Q9QrK2amfnUez7iqmi3FhOVtgLhOSRzfL
qbF8V6Xuqxp6rube1YVkZtO/VugpJMjp67y/vmnAja9Xowg+9CPrz1HNfzl6oAZlltIazaa9Elzi
Hb7l4kR51T1H45tr0Kzk5BuIaJFau13znaKANOgXhXBxOxXanosYqF94gOCscwSSra6TwUtw3wX+
YF8Z49e2tTLFJ4386GCJmBkHlS+DGey831lzxJZTS10flZAwKaUHBsOudZZE+BbO6yCX45+ddsdp
5T96ava0sqhc+5oNdMTcjPCDTDAej6mw8n3xh+/JuqnVYGQWheQo+KWDi+WH7UUbqRhqER4yEAVt
CS52ZTFFODzkQ3ZnMHPwaMUvFZMlzniyYdHS7z0KsVvP6jG3RyjAGrghmywMaaitg5An5C9bIhNY
rkJovkrWSfaHQHHTMa2DhfJ0b445pr6fA8mPqRHXgsu9qKpriQaC972O18oPHdzBGwbvgj/gLHMG
AR8MRobPSCBzyEggR3w70fbVTLCk2jjEp3AQD/SBP6o2qCm41USoFVacgjIsej/Om3WdSCyPf3Xa
vOrRQyLBEhiIQOjmKKZxw2c86EgZnFIiYUBTcsRCPfr82OwHWTjarEdjA4JR6ISYS1BTv+8TjtM0
wMKQADArejAs3pMzDSjMUgw7fHDAWCeLRmowEmyQIsyqCU7drUlc88WEheR9S4dlyqpiJG2DDxSQ
KBv9IKqxp+9SAOSkf4asOZ9EpfF4WeOdjNOeE5vc3DR3XgfnNACfl0tRXeqAV3pqauAR2D8fE3Ie
EKl6Xod41aE/24TOVk9VCl+fN/D36PGvBzFUhg+LKkdzPsVCJljHWSB9MMCHps0kJF8PAmRUi2SY
Abnwt/KYJf8ZTl8TzV3JHNq0qCV+Pp7UO9XbTEjrUZAmrxA9T5e2Cc4d6wBztHFi2bwEuSMELRQA
zP38CP8xAoeR4cAZUp2U5mCKHbjLkcnqkED4uxvqqxs7uq62LTomJPLK5lYiCzrqKp/o1TIiqLIe
5fiv/J2D7Azb79GNZ5BX2owLR/PMAJmFyPaUaGEjXo6p1kXaoCCT+5nHUYh/nAdGpOtBvy7gyE5x
bsCGAMHqG9bcLgcofseGrVR8toyOQ+ydcKoVBDhZxCx8bNlkMlEbVLCMTe6+77iyfgrK84O56bWn
stybhEQOFP+nilXXxl/J81PQbhH0SlQaZ3he4JkkXDXW+0eZ/D8MTy/C70lzWMz3pRuRA202zuO8
vNQvB5ptPbiR6q3dtUFm3kAx/LLRIE1CF18OZsqy6kWTOra8Q70VE+F3AH0C5S/d1h7CifxjTj63
ew1DPoht7gJrWAHPQy9b1Y/DMgv7MzFESHTKR09fXM5Q0woFyZIRsoUmHiVpEFz2d867KD0lAW+S
l8sZqv+HpxANCPv3OBbmyc8T/SneWGuuBl+fLHZwyYq4+ahhgM8evr4RM1lPQiISMrnAzdmS41sb
xQYg9VL2jNoNah2aR89gsGt2LYKN/dDAnEyppi6ZFus7i6XqwPf0ELSw19xrTYJqgqfGF4lFtn/T
vTEoH67Jaj3NSCHbXa6ZTrzZExQJBWffxcek6+pgIOyW1mTg/ZPR2Rkz3mCi84z5PyinAxZGWYsw
02Bvi+89tFLbH1RWp0xauIwSHS7UFcVXt1B351cdp0bju7htL+gtx21CjLV0a1iozgP4QdyEw54G
8O4br8C2b7nzwpQASaY47OMT9St9iG3RrOus5l9bV/uhgmrgUCaQncOQnl62PZ5dELg3d6zOE0Wf
9rWELINdBA4lf6ueIZYxYsf9eBJ778IjK2hCTd+ko7JLc6QauJA5+FRoc9JoO/im1oTZ3nXJpGQ2
lAiriVF2KMbzI6ei7OSW4v3nfxZ+jvFMLHgOhM8kp0qeIWmv9bgBFRWD3iv63pFocdEWp6TNwPXP
VwRbWTvfB6bBrCAblCm4FNMoF3ovpmZIMe+qEnPJHZhxW1aIwGQof+XJk+/9vLYrWr9SbUFIHPiw
gbEAFnZFGHn9D0WOrUsTmrso6z7GpYgjG5aO+4YFJshS5fSEyzm92bqFOZCnpxZr0n5fPsxWECdp
Tk+vEnxd3cV5x7NoaQl7TC7DUkY7aWiwYPL4DRIfd+SCTYt/LnfAIOwoD1Z3tAKVBUwh2fOgv6+p
oFh7HUg7eXxSD77bzF5db6rVItQI1VoWemVLMcuONfeOjfMiivf7/ArJZzaMjq3NmZ93U92O/aUV
Q+an5bmAj+DekWnOxfcPkqC3lZWsgV3V2qGVkgAHeD+mdJ4P9PODKCv2XPlJ55M7h6YxlHOUGqnK
lgW/p/UIwGX5IOPLhxqcmTQOLzpQFj7HN/ekyQnMdE0U1OFQcusiPZoVDyoKmemzcIbYCuIx1Ib7
pazACZHnVd0zrTbSAVJsyACwVU5Y4rc4L5Nsx6FIhNbh34oCZbVpnnu0ktKJkkmgcbTJ7o0RCwW7
8nbedok9+80xDfcIgx6cAKGb1twhQiLBIbwYuWYh2EPaHRWa9xqxYFjFIiV2oL1rQ2siyC7QOWKI
SobDVflUbo02RioUO8+H+Q+vUjPnmdTj9L4jaqsZ7ApdfAWupqhW0JnxVOFncyTDBf3qghJKeJ4F
A/Y4vmen3aVGMxOFlCJ0vylbn1Mrytb+RfO+PpiswU5evkn3djUQFALb2PV7VBg0id+s/BkqgyuS
zgKmzlmPWE1DpsNUqrqlpKRBNCzMQso/jaQwZLa4TWEGUjZz7n580x4iNJkDhqaIhv8YLATeU24t
47a3M2XfuAfYPTkT8SP9KgdcMUfs+AazE33o/Ms3QRCyToWyDcWM3QSDzJoovk9OKX+M49lGW8xA
gWZpnVnbHDmTXVdmQKni639IDPB4dB+1robzXe8rgUsjq9B3o4o2nQpooOLGVpFyLokTGYH39xHz
Fkls+h7sQY7oHotbMSJ+NDGpk9lcjSAND53dYxMlxhX9STHnRqm1L8tlg7QbWtQHIbsj5ew/beP6
obAMSwTpAGyd4aprChtDco8c92rG0eMYvI4w+WZpBQ3TPsCGExYtMJYdQ+fE04esx7PI0ch2BbeS
I+iQMjTDrDfZlFVh/QVvT9G2BfqZnb5D7HrFK49EhFUtMM8sHfle5OKNny9XxEQwVoWE2vzMko7b
btTLln5WGy7ZhTtzvqmtXldCfySaBK9N+qlRag5JzOFcKUoOhnwNKq52um+RDUDQxUKfR089OkoP
BuUl7nfSz3Y7PaifL02U3Ij671W7yLZD05uoW16etpsm8ADjs7OO2xlJwEyZ+Pq7cNkXgfSlHBF4
STOljB4oBIu9tlHbmm/aR1kTgQW8xjWzaF65O54jrRmnosAnA0/royTyND39D2iTbdIESAZcauh4
NouuXuWm+bo9mqnnAonkFOQZ9LcYMlwe5niZPm2qajzsNtbVgMXlXIUMC4dhXaFeAt7laEH1eJaW
nOmEYfsNDkC6T7KnbY2enDNOs3fUkG2UYGX9JHVXAzxDA84HJXbQ71yj1Apqt/A8x2r+BzouzpXf
4w5GB7NREHMpGwOVs2TF8PLvBz+lHhaeggdIR3Km3wh9cOo2j96qaT1NpAZ/T2BWz7ntJFiAXb3D
y4Jd3PMyKQyE+3DYNfWaosQwWvIkcCnHxFGZYPMRcYO6KzfMPUYJD0NoUbJD1mOePAVOa0wB69jK
4Eg6xwfO5nvuPxQ6ijFGdQ4ixkmqp5UsT2k8bcKphPtY5sM7pnJ3FCO4xjEcGAJJGrdUSYeRNICn
ctMySuBscZLHap6k3YNI2wcJIZ9hfD4z06Z3d9z2ROXuKj/+6H2LBAB0WFU0a25lMWyrqWq3fOFJ
6h/cJg/fNBtCEcwqmqgR1VZtwiGXY1exIhs2LCWlc6IOCIrzxh3IkReMu7eegwidlwNZQ9CkL4YY
KPm3IZPvjsKKB5VWApwH5zJAmUdtH19qhUuThgAqACgZoSgZSRAeaEmXLxtho6aTNd41J2xSm1fU
vOPm8eVS9fPRT6BjzzaznkbXByVD1voS4FefjT4GsnhSVzUvi3/GKgWjvY493r8wpUdCcKK+NQXg
T5oD7wGoaH9+Pn0QA8AF8Tvm3m46iUvNikdNaAk4BFbchC7tBA6sCfo5RrRX3okDJsWzyRcvHEQ0
nW4daPrE7UeWJeRou4XSlMCDtbSgdiV94An/izc1tu/Yp0yj5fvGVTXG2GIZcKjuGi/T+jv1ulW+
Rm1Bp70NRiJ3NBdDcRwFDh8jvajEPM07QbzwbiDbBmAmMAt9UwMREk4WMzDNnux8owZSRWBqxKfw
U+AH45cBv6Pd7icuCeE6e2b3WptVCJpEJJonZzwSnTaNVOEs0lDqoTjSqu1QQke1r9u4FaWWM0wA
tXxzOL6SKBnJxYgLQdE6UtLmM1kbn09qAsQXoZuYzekSLzhGzC+agdnXEZtWXnFW6IN35yBSnbqL
gC6f93vlGo4D1F+bc3/DyPSr3eaLeD+t+TKDuJZge+EE/5reEF4g8qIsFgOUFefha0pLa8R+dFL7
xkJI+oRWCYEN9QuxEGjkowf2P11RiLW50Xgz38zVBA9IiWkKogBgHl/cqsbYlqhqR2kSW+m9Cuow
uxndlpK7x/0bV9gVkDZ0KnTgFoc2ZmGUBgFmwKO/aeN88jnKJVxz750luwd4TCbAkVjdH/xfB6VK
Ga7+G7pYTF2QoLgTkzQ9T6kDGWednZ5Oyg7SQ5WPoYyG+7TnUIRwI5zT/IhnbFc1RRQ3yUwkbiai
yjUJEAcTnn/8GxQL3eOa7Ta4tpmSnmld2iedHKL/cR/0Xi+aXsJkikJewcltKeTkYvqy7/RVGSwg
n3FiPL6MeOdEhQhaQkYXNEE/fhadTdEdd5ADJTQNPQvKyEvXagVSb0ebofXvl2iR8r4EaI9nie1A
8XF4+UCIeMTvCOz1Bq4RYCUWa5otmkOPV17T2UYzLYoOELEBEdo+atQGdGauXpHbDgr2D0bXFJu8
AB0e1A3UYBGB8krSMGydMvH7soIjKdAA/C5OecF2vldIT6mX5bONjPGlnJIpf9xZbTIaptRoGfx6
MZ0ix+EMV4wKkdoXIyQ0cUMXdmkWGBTvMzCYcAVUrR6sAM6KXavCWJkzADATKI8mwKtKkEqETT1X
cx4ZK4Idn8fFXOcrpsoN7vOU0F+uus7IcGTM529S75SxvKm4fYUMxA0WUilR3Zj2qqeAS+3lwPpJ
+gsPLpn0QPtDzuxlrHlYpLAyBOA7qMkTNQi69dP7WN+V2Xv8B/mT1K+ISJpPi7slmhuXodCq8H9Y
WzEFEeOErVCM4ZQP3wNd7u3lWg6uWR/ggCMrPDTYGJB7P9mTarYvzv94TNUfVZEswvZJ9ivUFrGX
M9sbgAGvQJQqgaOgazkzIHtYq0qvomJwPzf3SS3ONz7YnXXHaObFBjgkfj8SHSDtqI9NKRMCZqMI
zCd0ib6CgCn3JJcVCnmcYkzo10vu2EoWXSd1eMcrhU7hquEPlFTdfQP+QHIxEKWSl8WYXlGP42fT
VYTqIE/p0vsWcDYI9VFqEkxMVMIdRI5rW172jN2IeoHGTYyDrjsLXWrFVBBdDABcR1DGCJ7t2+PH
pAAiSmCrkpJvv1ACtpN+XDWhlMDcrOP10juFPfOYnPCiS5v8JkXtcdd3LKQrCyC5uC3YeQ2/4FEK
FL9FyJrtDUfkfmB3ZsTymhaFcl4gyJbLgCXHH5hTbIRx+qTW8oRHHOwdnoVirma9/5veZbrYvwV5
L0DgnHO/vGWY+gRqEmzNAMon8JTJDpd4Ci2K4EIexiKcWiA7WJ+XDgHlqnnwDQ/tS6EtxPeP0Vaz
F1rDkgur2hU6PXWmwgY8PCZhnwW8ddgLLTPWSlNI293ZX6+pldRZDZFq21PPBql11pKKgqFwQed5
GIJ9+xBiQqCwe9hVOTCzeGL3YE5S0xSbYQ101M5r4c44JzH8pVQPcjFGUVjzitkeEaqXhulcMsuE
ZQns+lmbruF9kLVUK+sCriYqR104fKqhbOlpqJfl0sjT9pYJvH+3ypJcOLGGqjT9cZGpv+7dna3v
pzJa+83W2HFXny4wqkNCwXuYAHITEgFgnI4/QtkzA5IgH4DKENcqSWNQF5H/Mu5Emk/FKAFqiwiq
Uc/7o+Wtj5WjqAu4AWrkOFUXN7yC/fcRE3haJyq/1AP1gKAFwtQfnLd7rw6G1Nt6+GWUQ3WXB4bx
HBjpy/FDIQT4RvEk3bA7Gq1RM+BcW1yR019PKFjzeqaJ+0KBFbVScTPNI/nlxWNrG8hZ4+zOw/Wc
62pFH8FcltAi7iS1VTu6hPuU2bmUBrJEvhwnwyyhU03+NU4rHfg1OykqnYgQWp7NChSB5xUwmMfu
JAmWa3PhsBauATXhBnI0WgSHnB9wS1Q2ALcTUujhDLyV28r9LBp4BEen/RIMpi72a0ecbe2fIFb5
b5W19MZWC3uElxmbLzBipuW9KkbarTkf1jpM0n3ad3FzA8azGqPtyk71bcHg39spDwhZIOTywQz2
0ehTOJmeWZEZEdV46ORFU4YhGfAylmVkhni4oIA6DsUT7tJwS0VPXl2aucguwyMXJSY2OxkIF/fX
JtJLVgJ0qpzrC5fnv1jlX49XeQ/QqgYBc8Bf0bPjVxRi7nWioWSP/iJ8WccxgsccIYd6Gxvj4ZVC
Gdiowq1x8IXMdn+JGDz4qGvFlnKOsOsKXXoe6G1SwDSXcNXKI2loPq0v1ZOx7mvL2fRtWcgV4bxM
ZwXSH3JPr0uD4S++O8djUYistevUiLg3rJl4lT1VvyQTpjwowBSoOQSD9ny0ETGufwbxiTbd9rYM
haKi2p7Vc7qdxVhv3a7MBBFBNwUcHs65yg9+Rt95ff4zudqdBX0JHb1vln4Qzt2sX+nY/1Ay6V6K
B++xLeXbxNoIMiqixTTOnurV2tGJgyrZFg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
