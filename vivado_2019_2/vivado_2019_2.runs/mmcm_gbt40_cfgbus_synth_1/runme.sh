#!/bin/sh

# 
# Vivado(TM)
# runme.sh: a Vivado-generated Runs Script for UNIX
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# 

if [ -z "$PATH" ]; then
  PATH=/home/pirovaldes/apps/xilinx/Vitis/2019.2/bin:/home/pirovaldes/apps/xilinx/Vivado/2019.2/ids_lite/ISE/bin/lin64:/home/pirovaldes/apps/xilinx/Vivado/2019.2/bin
else
  PATH=/home/pirovaldes/apps/xilinx/Vitis/2019.2/bin:/home/pirovaldes/apps/xilinx/Vivado/2019.2/ids_lite/ISE/bin/lin64:/home/pirovaldes/apps/xilinx/Vivado/2019.2/bin:$PATH
fi
export PATH

if [ -z "$LD_LIBRARY_PATH" ]; then
  LD_LIBRARY_PATH=
else
  LD_LIBRARY_PATH=:$LD_LIBRARY_PATH
fi
export LD_LIBRARY_PATH

HD_PWD='/home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/mmcm_gbt40_cfgbus_synth_1'
cd "$HD_PWD"

HD_LOG=runme.log
/bin/touch $HD_LOG

ISEStep="./ISEWrap.sh"
EAStep()
{
     $ISEStep $HD_LOG "$@" >> $HD_LOG 2>&1
     if [ $? -ne 0 ]
     then
         exit
     fi
}

EAStep vivado -log mmcm_gbt40_cfgbus.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source mmcm_gbt40_cfgbus.tcl
