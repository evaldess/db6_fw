
// file: mmcm_gbt40_db.v
// 
// (c) Copyright 2008 - 2013 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
//----------------------------------------------------------------------------
// User entered comments
//----------------------------------------------------------------------------
// None
//
//----------------------------------------------------------------------------
//  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
//   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
//----------------------------------------------------------------------------
// p_clk40_out__40.08000______0.000______50.0______210.344____175.930
// p_clk80_out__80.16000______0.000______50.0______173.475____175.930
// p_clk160_out__160.32000______0.000______50.0______150.192____175.930
// p_clk280_out__280.56000______0.000______50.0______138.028____175.930
// p_clk40_90_out__40.08000_____90.000______50.0______210.344____175.930
// p_clk40_180_out__40.08000____180.000______50.0______210.344____175.930
// p_clk40_270_out__40.08000____270.000______50.0______210.344____175.930
//
//----------------------------------------------------------------------------
// Input Clock   Freq (MHz)    Input Jitter (UI)
//----------------------------------------------------------------------------
// __primary__________40.080____________0.010
// _secondary________40.080____________0.010

`timescale 1ps/1ps

(* CORE_GENERATION_INFO = "mmcm_gbt40_db,clk_wiz_v6_0_4_0_0,{component_name=mmcm_gbt40_db,use_phase_alignment=true,use_min_o_jitter=false,use_max_i_jitter=false,use_dyn_phase_shift=true,use_inclk_switchover=true,use_dyn_reconfig=true,enable_axi=0,feedback_source=FDBK_AUTO,PRIMITIVE=MMCM,num_out_clk=7,clkin1_period=24.950,clkin2_period=24.950,use_power_down=false,use_reset=true,use_locked=true,use_inclk_stopped=true,feedback_type=SINGLE,CLOCK_MGR_TYPE=NA,manual_override=false}" *)

module mmcm_gbt40_db 
 (
  input         clk_in2_p,
  input         clk_in2_n,
  input         p_clk_sel_in,
  // Clock out ports
  output        p_clk40_out,
  output        p_clk80_out,
  output        p_clk160_out,
  output        p_clk280_out,
  output        p_clk40_90_out,
  output        p_clk40_180_out,
  output        p_clk40_270_out,
  // Dynamic reconfiguration ports
  input   [6:0] p_daddr_in,
  input         p_dclk_in,
  input         p_den_in,
  input  [15:0] p_din_in,
  output [15:0] p_dout_out,
  output        p_drdy_out,
  input         p_dwe_in,
  // Dynamic phase shift ports
  input         p_psclk_in,
  input         p_psen_in,
  input         p_psincdec_in,
  output        p_psdone_out,
  // Status and control signals
  input         p_reset_in,
  output        p_input_clk_stopped_out,
  output        p_clkfb_stopped_out,
  output        p_locked_out,
   output       p_cddcdone_out,
   input        p_cddcreq_in,
 // Clock in ports
  input         clk_in1_p,
  input         clk_in1_n
 );

  mmcm_gbt40_db_clk_wiz inst
  (
  .clk_in2_p(clk_in2_p),
  .clk_in2_n(clk_in2_n),
  .p_clk_sel_in(p_clk_sel_in),
  // Clock out ports  
  .p_clk40_out(p_clk40_out),
  .p_clk80_out(p_clk80_out),
  .p_clk160_out(p_clk160_out),
  .p_clk280_out(p_clk280_out),
  .p_clk40_90_out(p_clk40_90_out),
  .p_clk40_180_out(p_clk40_180_out),
  .p_clk40_270_out(p_clk40_270_out),
  // Dynamic reconfiguration ports            
  .p_daddr_in(p_daddr_in),
  .p_dclk_in(p_dclk_in),
  .p_den_in(p_den_in),
  .p_din_in(p_din_in),
  .p_dout_out(p_dout_out),
  .p_drdy_out(p_drdy_out),
  .p_dwe_in(p_dwe_in),
  // Dynamic phase shift ports                
  .p_psclk_in(p_psclk_in),
  .p_psen_in(p_psen_in),
  .p_psincdec_in(p_psincdec_in),
  .p_psdone_out(p_psdone_out),
  // Status and control signals               
  .p_reset_in(p_reset_in), 
  .p_input_clk_stopped_out(p_input_clk_stopped_out),
  .p_clkfb_stopped_out(p_clkfb_stopped_out),
  .p_locked_out(p_locked_out),
  .p_cddcdone_out(p_cddcdone_out),
  .p_cddcreq_in(p_cddcreq_in),
 // Clock in ports
  .clk_in1_p(clk_in1_p),
  .clk_in1_n(clk_in1_n)
  );

endmodule
