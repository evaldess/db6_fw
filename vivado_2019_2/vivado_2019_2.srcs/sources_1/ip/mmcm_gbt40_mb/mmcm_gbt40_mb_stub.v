// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Fri May 15 01:06:09 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.srcs/sources_1/ip/mmcm_gbt40_mb/mmcm_gbt40_mb_stub.v
// Design      : mmcm_gbt40_mb
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module mmcm_gbt40_mb(p_clk40_out, p_clk40_90_out, p_clk40_180_out, 
  p_clk40_270_out, p_clk80_out, p_clk280_out, p_clk560_out, p_daddr_in, p_dclk_in, p_den_in, 
  p_din_in, p_dout_out, p_drdy_out, p_dwe_in, p_psclk_in, p_psen_in, p_psincdec_in, p_psdone_out, 
  reset, p_input_clk_stopped_out, p_clkfb_stopped_out, p_locked_out, p_cddcdone_out, 
  p_cddcreq_in, p_clk_in)
/* synthesis syn_black_box black_box_pad_pin="p_clk40_out,p_clk40_90_out,p_clk40_180_out,p_clk40_270_out,p_clk80_out,p_clk280_out,p_clk560_out,p_daddr_in[6:0],p_dclk_in,p_den_in,p_din_in[15:0],p_dout_out[15:0],p_drdy_out,p_dwe_in,p_psclk_in,p_psen_in,p_psincdec_in,p_psdone_out,reset,p_input_clk_stopped_out,p_clkfb_stopped_out,p_locked_out,p_cddcdone_out,p_cddcreq_in,p_clk_in" */;
  output p_clk40_out;
  output p_clk40_90_out;
  output p_clk40_180_out;
  output p_clk40_270_out;
  output p_clk80_out;
  output p_clk280_out;
  output p_clk560_out;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_psclk_in;
  input p_psen_in;
  input p_psincdec_in;
  output p_psdone_out;
  input reset;
  output p_input_clk_stopped_out;
  output p_clkfb_stopped_out;
  output p_locked_out;
  output p_cddcdone_out;
  input p_cddcreq_in;
  input p_clk_in;
endmodule
