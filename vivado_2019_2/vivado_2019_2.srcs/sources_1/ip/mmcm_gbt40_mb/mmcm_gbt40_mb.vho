
-- 
-- (c) Copyright 2008 - 2013 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
------------------------------------------------------------------------------
-- User entered comments
------------------------------------------------------------------------------
-- None
--
------------------------------------------------------------------------------
--  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
--   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
------------------------------------------------------------------------------
-- p_clk40_out__40.08000______0.000______50.0______210.344____175.930
-- p_clk40_90_out__40.08000_____90.000______50.0______210.344____175.930
-- p_clk40_180_out__40.08000____180.000______50.0______210.344____175.930
-- p_clk40_270_out__40.08000____270.000______50.0______210.344____175.930
-- p_clk80_out__80.16000______0.000______50.0______173.475____175.930
-- p_clk280_out__280.56000______0.000______50.0______138.028____175.930
-- p_clk560_out__561.12000______0.000______50.0______124.657____175.930
--
------------------------------------------------------------------------------
-- Input Clock   Freq (MHz)    Input Jitter (UI)
------------------------------------------------------------------------------
-- __primary___________40.08____________0.010


-- The following code must appear in the VHDL architecture header:
------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
component mmcm_gbt40_mb
port
 (-- Clock in ports
  -- Clock out ports
  p_clk40_out          : out    std_logic;
  p_clk40_90_out          : out    std_logic;
  p_clk40_180_out          : out    std_logic;
  p_clk40_270_out          : out    std_logic;
  p_clk80_out          : out    std_logic;
  p_clk280_out          : out    std_logic;
  p_clk560_out          : out    std_logic;
  -- Dynamic reconfiguration ports
  p_daddr_in             : in     std_logic_vector(6 downto 0);
  p_dclk_in              : in     std_logic;
  p_den_in               : in     std_logic;
  p_din_in                : in     std_logic_vector(15 downto 0);
  p_dout_out                : out    std_logic_vector(15 downto 0);
  p_dwe_in               : in     std_logic;
  p_drdy_out              : out    std_logic;
  -- Dynamic phase shift ports
  p_psclk_in             : in     std_logic;
  p_psen_in              : in     std_logic;
  p_psincdec_in          : in     std_logic;
  p_psdone_out            : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  p_input_clk_stopped_out : out    std_logic;
  p_clkfb_stopped_out : out    std_logic;
  p_locked_out            : out    std_logic;
  p_cddcdone_out          : out    std_logic; 
  p_cddcreq_in           : in     std_logic;
  p_clk_in           : in     std_logic
 );
end component;

-- COMP_TAG_END ------ End COMPONENT Declaration ------------
-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.
------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG
your_instance_name : mmcm_gbt40_mb
   port map ( 
  -- Clock out ports  
   p_clk40_out => p_clk40_out,
   p_clk40_90_out => p_clk40_90_out,
   p_clk40_180_out => p_clk40_180_out,
   p_clk40_270_out => p_clk40_270_out,
   p_clk80_out => p_clk80_out,
   p_clk280_out => p_clk280_out,
   p_clk560_out => p_clk560_out,
  -- Dynamic reconfiguration ports             
   p_daddr_in => p_daddr_in,
   p_dclk_in => p_dclk_in,
   p_den_in => p_den_in,
   p_din_in => p_din_in,
   p_dout_out => p_dout_out,
   p_drdy_out => p_drdy_out,
   p_dwe_in => p_dwe_in,
  -- Dynamic phase shift ports                 
   p_psclk_in => p_psclk_in,
   p_psen_in => p_psen_in,
   p_psincdec_in => p_psincdec_in,
   p_psdone_out => p_psdone_out,
  -- Status and control signals                
   reset => reset,
   p_input_clk_stopped_out => p_input_clk_stopped_out,
   p_clkfb_stopped_out => p_clkfb_stopped_out,
   p_locked_out => p_locked_out,
   p_cddcdone_out => p_cddcdone_out,
   p_cddcreq_in  => p_cddcreq_in,
   -- Clock in ports
   p_clk_in => p_clk_in
 );
-- INST_TAG_END ------ End INSTANTIATION Template ------------
