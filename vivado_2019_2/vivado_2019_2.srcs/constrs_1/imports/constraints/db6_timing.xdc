#################################################################################--
###                                                                            ##--
### db6 constraints                                                            ##--
### Version: 1.0                                                               ##--
### Creation date: 2019-11-05                                                  ##--
### Created by: : Eduardo Valdes                                               ##--
###                                                                            ##--
### Modification date:                                                         ##--
### Modified by:                         		                               ##--
###                                                                            ##--
#################################################################################--

# locate mmcms
##set_property LOC MMCME3_ADV_X1Y1 [get_cells i_db6_clock_interface/i_mmcm_osc_clk/inst/mmcme3_adv_inst]
#set_property BEL MMCME3_ADV [get_cells i_db6_clock_interface/i_mmcm_osc_clk/inst/mmcme3_adv_inst]
#set_property LOC MMCME3_ADV_X1Y1 [get_cells i_db6_clock_interface/i_mmcm_osc_clk/inst/mmcme3_adv_inst]

set_property BEL PLLE3_ADV [get_cells i_db6_clock_interface/i_pll_osc_clk/inst/plle3_adv_inst]
set_property LOC PLLE3_ADV_X1Y3 [get_cells i_db6_clock_interface/i_pll_osc_clk/inst/plle3_adv_inst]

#set_property LOC MMCME3_ADV_X0Y4 [get_cells i_db6_clock_interface/i_mmcm_gbt40_cfgbus/inst/mmcme3_adv_inst]
set_property BEL MMCME3_ADV [get_cells i_db6_clock_interface/i_mmcm_gbt40_cfgbus/inst/mmcme3_adv_inst]
set_property LOC MMCME3_ADV_X0Y4 [get_cells i_db6_clock_interface/i_mmcm_gbt40_cfgbus/inst/mmcme3_adv_inst]

#set_property LOC MMCME3_ADV_X0Y3 [get_cells i_db6_clock_interface/i_mmcm_gbt40_mb_q0/inst/mmcme3_adv_inst]
set_property BEL MMCME3_ADV [get_cells i_db6_clock_interface/i_mmcm_gbt40_mb_q0/inst/mmcme3_adv_inst]
set_property LOC MMCME3_ADV_X0Y2 [get_cells i_db6_clock_interface/i_mmcm_gbt40_mb_q0/inst/mmcme3_adv_inst]

#set_property LOC MMCME3_ADV_X1Y4 [get_cells i_db6_clock_interface/i_mmcm_gbt40_mb_q1/inst/mmcme3_adv_inst]
set_property BEL MMCME3_ADV [get_cells i_db6_clock_interface/i_mmcm_gbt40_mb_q1/inst/mmcme3_adv_inst]
set_property LOC MMCME3_ADV_X0Y0 [get_cells i_db6_clock_interface/i_mmcm_gbt40_mb_q1/inst/mmcme3_adv_inst]

#logic clocks
#set_property PACKAGE_PIN H22 [get_ports {p_gbt_cfgbus_clk40_local_in[p]}]
#set_property PACKAGE_PIN H24 [get_ports {p_gbt_cfgbus_clk40_remote_in[p]}]
set_property PACKAGE_PIN H22 [get_ports {p_gbt_cfgbus_clk40_local_in[p]}]
set_property PACKAGE_PIN H24 [get_ports {p_gbt_cfgbus_clk40_remote_in[p]}]

#osc clocks
set_property PACKAGE_PIN W20 [get_ports {p_osc_clk_in[p]}]
set_property IOSTANDARD LVDS_25 [get_ports {p_osc_clk_in[p]}]
set_property IOSTANDARD LVDS_25 [get_ports {p_osc_clk_in[n]}]
create_clock -period 10.000 -name {p_osc_clk_in[p]} -waveform {0.000 5.000} [get_ports {p_osc_clk_in[p]}]

#clks to mb quads
set_property IOSTANDARD LVDS [get_ports {p_gbt_mb_q0_clk40_out[p]}]
set_property IOSTANDARD LVDS [get_ports {p_gbt_mb_q1_clk40_out[p]}]
set_property PACKAGE_PIN B21 [get_ports {p_gbt_mb_q0_clk40_out[p]}]
set_property PACKAGE_PIN U26 [get_ports {p_gbt_mb_q1_clk40_out[p]}]

set_property PACKAGE_PIN D18 [get_ports {p_gbt_mb_q0_clk40_local_in[p]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_gbt_mb_q0_clk40_local_in[p]}]
set_property PACKAGE_PIN D20 [get_ports {p_gbt_mb_q0_clk40_remote_in[p]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_gbt_mb_q0_clk40_remote_in[p]}]
set_property PACKAGE_PIN T22 [get_ports {p_gbt_mb_q1_clk40_local_in[p]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_gbt_mb_q1_clk40_local_in[p]}]
set_property PACKAGE_PIN V22 [get_ports {p_gbt_mb_q1_clk40_remote_in[p]}]
set_property IOSTANDARD SUB_LVDS [get_ports {p_gbt_mb_q1_clk40_remote_in[p]}]

#set_property PACKAGE_PIN G12 [get_ports {p_gbt_tp_q0_clk40_remote_in[p]}]
#set_property PACKAGE_PIN E13 [get_ports {p_gbt_tp_q0_clk40_local_in[p]}]
#set_property PACKAGE_PIN E10 [get_ports {p_gbt_tp_q1_clk40_remote_in[p]}]
#set_property PACKAGE_PIN E11 [get_ports {p_gbt_tp_q1_clk40_local_in[p]}]
set_property PACKAGE_PIN G12 [get_ports {p_gbt_tp_q0_clk40_remote_in[p]}]
set_property PACKAGE_PIN E13 [get_ports {p_gbt_tp_q0_clk40_local_in[p]}]
set_property PACKAGE_PIN E10 [get_ports {p_gbt_tp_q1_clk40_remote_in[p]}]
set_property PACKAGE_PIN E11 [get_ports {p_gbt_tp_q1_clk40_local_in[p]}]

#bitclks
set_property PACKAGE_PIN E20 [get_ports {p_adc_bitclk_in[1][p]}]
set_property PACKAGE_PIN D19 [get_ports {p_adc_bitclk_in[0][p]}]
set_property PACKAGE_PIN K23 [get_ports {p_adc_bitclk_in[3][p]}]
set_property PACKAGE_PIN U24 [get_ports {p_adc_bitclk_in[5][p]}]
set_property PACKAGE_PIN J24 [get_ports {p_adc_bitclk_in[2][p]}]
set_property PACKAGE_PIN T23 [get_ports {p_adc_bitclk_in[4][p]}]

#create clocks
create_clock -period 3.564 -name {p_adc_bitclk_in[0][p]} [get_ports {p_adc_bitclk_in[0][p]}]
create_clock -period 3.564 -name {p_adc_bitclk_in[1][p]} [get_ports {p_adc_bitclk_in[1][p]}]
create_clock -period 3.564 -name {p_adc_bitclk_in[2][p]} [get_ports {p_adc_bitclk_in[2][p]}]
create_clock -period 3.564 -name {p_adc_bitclk_in[3][p]} [get_ports {p_adc_bitclk_in[3][p]}]
create_clock -period 3.564 -name {p_adc_bitclk_in[4][p]} [get_ports {p_adc_bitclk_in[4][p]}]
create_clock -period 3.564 -name {p_adc_bitclk_in[5][p]} [get_ports {p_adc_bitclk_in[5][p]}]

create_clock -period 24.950 -name {p_gbt_cfgbus_clk40_local_in[p]} [get_ports {p_gbt_cfgbus_clk40_local_in[p]}]
create_clock -period 24.950 -name {p_gbt_cfgbus_clk40_remote_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_cfgbus_clk40_remote_in[p]}]
create_clock -period 6.238 -name {p_gth_refclk_gbtx_local_in[0][p]} [get_ports {p_gth_refclk_gbtx_local_in[0][p]}]
create_clock -period 6.238 -name {p_gth_refclk_gbtx_remote_in[0][p]} [get_ports {p_gth_refclk_gbtx_remote_in[0][p]}]
create_clock -period 6.238 -name {p_gth_refclk_gbtx_local_in[1][p]} [get_ports {p_gth_refclk_gbtx_local_in[1][p]}]
create_clock -period 6.238 -name {p_gth_refclk_gbtx_remote_in[1][p]} [get_ports {p_gth_refclk_gbtx_remote_in[1][p]}]
create_generated_clock -name {p_gbt_mb_q0_clk40_out[p]} -source [get_pins i_db6_clock_interface/i_oddre1_gbt40_q0/CLKDIV] -divide_by 1 -invert [get_ports {p_gbt_mb_q0_clk40_out[p]}]
create_generated_clock -name {p_gbt_mb_q1_clk40_out[p]} -source [get_pins i_db6_clock_interface/i_oddre1_gbt40_q1/CLKDIV] -divide_by 1 -invert [get_ports {p_gbt_mb_q1_clk40_out[p]}]
create_clock -period 24.950 -name {p_gbt_tp_q0_clk40_local_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_tp_q0_clk40_local_in[p]}]
create_clock -period 24.950 -name {p_gbt_tp_q1_clk40_local_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_tp_q1_clk40_local_in[p]}]

create_clock -period 24.950 -name {p_gbt_tp_q0_clk40_remote_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_tp_q0_clk40_remote_in[p]}]
create_clock -period 24.950 -name {p_gbt_tp_q1_clk40_remote_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_tp_q1_clk40_remote_in[p]}]

create_clock -period 24.950 -name {p_gbt_mb_q0_clk40_local_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_mb_q0_clk40_local_in[p]}]
create_clock -period 24.950 -name {p_gbt_mb_q0_clk40_remote_in[p]} -waveform {0.000 12.475} [get_ports {p_gbt_mb_q0_clk40_remote_in[p]}]
create_clock -period 24.950 -name {p_gbt_mb_q1_clk40_local_in[p]} [get_ports {p_gbt_mb_q1_clk40_local_in[p]}]
create_clock -period 24.950 -name {p_gbt_mb_q1_clk40_remote_in[p]} [get_ports {p_gbt_mb_q1_clk40_remote_in[p]}]

#group rules
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks {p_gbt_tp_q0_clk40_local_in[p]}] -group [get_clocks -include_generated_clocks {p_gbt_tp_q0_clk40_remote_in[p]}]
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks {p_gbt_tp_q1_clk40_local_in[p]}] -group [get_clocks -include_generated_clocks {p_gbt_tp_q1_clk40_remote_in[p]}]

set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_adc_bitclk_in[0][p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_adc_bitclk_in[1][p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_adc_bitclk_in[2][p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_adc_bitclk_in[3][p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_adc_bitclk_in[4][p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_adc_bitclk_in[5][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks {rxoutclk_out[1]}]


set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_adc_bitclk_in[0][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_adc_bitclk_in[0][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_adc_bitclk_in[1][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_adc_bitclk_in[1][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_adc_bitclk_in[2][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_adc_bitclk_in[2][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_adc_bitclk_in[3][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_adc_bitclk_in[3][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_adc_bitclk_in[4][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_adc_bitclk_in[4][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_adc_bitclk_in[5][p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_adc_bitclk_in[5][p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_gbt_tp_q0_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_gbt_tp_q0_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_gbt_tp_q1_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_gbt_tp_q1_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks {rxoutclk_out[1]}]

set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_gbt_tp_q0_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_gbt_tp_q0_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_gbt_tp_q0_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_gbt_tp_q0_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_gbt_tp_q1_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_gbt_tp_q1_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_gbt_tp_q1_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_gbt_tp_q1_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]

set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks {txoutclk_out[1]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {txoutclk_out[1]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {txoutclk_out[1]}]

set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[0][p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[1][p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[2][p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[3][p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[4][p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[5][p]}] -group [get_clocks p_clk_40_out_mmcm_osc_clk]

set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks p_clk_80_out_mmcm_osc_clk] -group [get_clocks -include_generated_clocks p_clk80_out_mmcm_gbt40_cfgbus]
set_clock_groups -asynchronous -group [get_clocks p_clk_40_out_mmcm_osc_clk] -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus] -group [get_clocks p_clk_40_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks p_clk_80_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks p_clk_80_out_mmcm_osc_clk]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk_80_out_mmcm_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk_80_out_mmcm_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]

set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks p_clk80_out_pll_osc_clk] -group [get_clocks -include_generated_clocks p_clk80_out_mmcm_gbt40_cfgbus]
create_generated_clock -name p_clk40_out_pll_osc_clk_Gen -source [get_pins i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I0] -divide_by 1 -add -master_clock [get_clocks -filter {IS_GENERATED} -of_objects [get_pins i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I0]] [get_pins i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/O]
create_generated_clock -name {p_gbt_cfgbus_clk40_local_in[p]_Gen} -source [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I1}] -divide_by 1 -add -master_clock p_gbt_cfgbus_clk40_local_in[p] [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/O}]
create_generated_clock -name {p_gbt_cfgbus_clk40_remote_in[p]_Gen} -source [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I1}] -divide_by 1 -add -master_clock p_gbt_cfgbus_clk40_remote_in[p] [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/O}]
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks {p_clk40_out_pll_osc_clk_Gen}] -group [get_clocks -include_generated_clocks {p_gbt_cfgbus_clk40_local_in[p]_Gen p_gbt_cfgbus_clk40_remote_in[p]_Gen}]

set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[0][p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[1][p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[2][p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[3][p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[4][p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[5][p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks p_clk40_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk] -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks p_clk80_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks p_clk80_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_pll_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_pll_osc_clk] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk] -group [get_clocks {txoutclk_out[1]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {txoutclk_out[1]}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {txoutclk_out[1]}]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[0][p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[1][p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[2][p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[3][p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[4][p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_adc_bitclk_in[5][p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks p_clk80_out_mmcm_gbt40_cfgbus] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks {txoutclk_out[0]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks {txoutclk_out[1]}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks p_clk80_out_pll_osc_clk]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks {txoutclk_out[2]}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]_1}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[1]_1}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[2]_1}] -group [get_clocks p_clk40_out_pll_osc_clk_Gen]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks {txoutclk_out[0]_1}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks {txoutclk_out[1]_1}]
set_clock_groups -asynchronous -group [get_clocks p_clk40_out_pll_osc_clk_Gen] -group [get_clocks {txoutclk_out[2]_1}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]_1}] -group [get_clocks {rxoutclk_out[1]_1}]
set_clock_groups -asynchronous -group [get_clocks {rxoutclk_out[0]_1}] -group [get_clocks {rxoutclk_out[2]_1}]




#create_generated_clock -name p_clk_40_out_mmcm_osc_clk_Gen -source [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I0}] -divide_by 1 -add -master_clock [get_clocks -of [get_pins i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I0] -filter {IS_GENERATED}] [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/O}]
#create_generated_clock -name {p_gbt_cfgbus_clk40_local_in[p]_Gen} -source [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I1}] -divide_by 1 -add -master_clock p_gbt_cfgbus_clk40_local_in[p] [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/O}]
#create_generated_clock -name {p_gbt_cfgbus_clk40_remote_in[p]_Gen} -source [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/I1}] -divide_by 1 -add -master_clock p_gbt_cfgbus_clk40_remote_in[p] [get_pins {i_db6_clock_interface/i_BUFGMUX_CTRL_db_clk40/O}]
#set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks {p_clk_40_out_mmcm_osc_clk_Gen}] -group [get_clocks -include_generated_clocks {p_gbt_cfgbus_clk40_local_in[p]_Gen p_gbt_cfgbus_clk40_remote_in[p]_Gen}]

#commbus
set_property PACKAGE_PIN Y17 [get_ports {p_commbus_ddr_clk_in[p]}]
set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_clk_in[p]}]
set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_clk_in[n]}]
set_property PACKAGE_PIN V18 [get_ports {p_commbus_ddr_clk_out[p]}]
set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_clk_out[p]}]
set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_clk_out[n]}]

#create_clock -period 2.079 -name {p_commbus_ddr_clk_in[0][p]} -waveform {0.000 1.040} [get_ports {p_commbus_ddr_clk_in[0][p]}]
create_clock -period 4.158 -name {p_commbus_ddr_clk_in[p]} -waveform {0.000 2.080} [get_ports {p_commbus_ddr_clk_in[p]}]
create_generated_clock -name {p_commbus_ddr_clk_out[p]} -source [get_pins i_db6_commbus_interface/i_db6_commbus_ddr_interface/i_oddre1_commbus_clk_out/CLKDIV] -divide_by 1 -invert [get_ports {p_commbus_ddr_clk_out[p]}]
set_clock_groups -asynchronous -group [get_clocks {p_clk40_out_pll_osc_clk_Gen}] -group [get_clocks {p_bitclk_out_pll_commbus_2}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_local_in[p]}] -group [get_clocks {p_bitclk_out_pll_commbus_2}]
set_clock_groups -asynchronous -group [get_clocks {p_gbt_cfgbus_clk40_remote_in[p]}] -group [get_clocks {p_bitclk_out_pll_commbus_2}]





