-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed May 27 14:13:13 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/fifo_gbt_encoder_synth_1/fifo_gbt_encoder_stub.vhdl
-- Design      : fifo_gbt_encoder
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fifo_gbt_encoder is
  Port ( 
    clk : in STD_LOGIC;
    srst : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 115 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    injectsbiterr : in STD_LOGIC;
    sleep : in STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 115 downto 0 );
    full : out STD_LOGIC;
    wr_ack : out STD_LOGIC;
    overflow : out STD_LOGIC;
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    underflow : out STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    wr_rst_busy : out STD_LOGIC;
    rd_rst_busy : out STD_LOGIC
  );

end fifo_gbt_encoder;

architecture stub of fifo_gbt_encoder is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,srst,din[115:0],wr_en,rd_en,injectdbiterr,injectsbiterr,sleep,dout[115:0],full,wr_ack,overflow,empty,valid,underflow,sbiterr,dbiterr,wr_rst_busy,rd_rst_busy";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "fifo_generator_v13_2_5,Vivado 2019.2_AR72614";
begin
end;
