
// file: pll_commbus.v
// 
// (c) Copyright 2008 - 2013 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
//----------------------------------------------------------------------------
// User entered comments
//----------------------------------------------------------------------------
// None
//
//----------------------------------------------------------------------------
//  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
//   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
//----------------------------------------------------------------------------
// p_commbusclk_out__80.16000______0.000______50.0______169.969____125.476
// p_bitclk_out__240.48000______0.000______50.0______136.664____125.476
//
//----------------------------------------------------------------------------
// Input Clock   Freq (MHz)    Input Jitter (UI)
//----------------------------------------------------------------------------
// __primary___________80.16____________0.010

`timescale 1ps/1ps

module pll_commbus_clk_wiz 

 (// Clock in ports
  // Clock out ports
  output        p_commbusclk_out,
  output        p_bitclk_out,
  // Dynamic reconfiguration ports
  input   [6:0] p_daddr_in,
  input         p_dclk_in,
  input         p_den_in,
  input  [15:0] p_din_in,
  output [15:0] p_dout_out,
  output        p_drdy_out,
  input         p_dwe_in,
  // Status and control signals
  input         p_reset_in,
  output        p_locked_out,
  input         p_clk_in
 );
  // Input buffering
  //------------------------------------
wire p_clk_in_pll_commbus;
wire clk_in2_pll_commbus;
  IBUF clkin1_ibuf
   (.O (p_clk_in_pll_commbus),
    .I (p_clk_in));




  // Clocking PRIMITIVE
  //------------------------------------

  // Instantiation of the MMCM PRIMITIVE
  //    * Unused inputs are tied off
  //    * Unused outputs are labeled unused

  wire        p_commbusclk_out_pll_commbus;
  wire        p_bitclk_out_pll_commbus;
  wire        clk_out3_pll_commbus;
  wire        clk_out4_pll_commbus;
  wire        clk_out5_pll_commbus;
  wire        clk_out6_pll_commbus;
  wire        clk_out7_pll_commbus;

  wire        psdone_unused;
  wire        p_locked_out_int;
  wire        clkfbout_pll_commbus;
  wire        clkfbout_buf_pll_commbus;
  wire        clkfboutb_unused;
    wire clkout0b_unused;
   wire clkout1b_unused;
  wire        clkfbstopped_unused;
  wire        clkinstopped_unused;
  wire        reset_high;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg1 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg2 = 0;

 
    PLLE3_ADV
  #(
    .COMPENSATION         ("AUTO"),
    .STARTUP_WAIT         ("FALSE"),
    .DIVCLK_DIVIDE        (1),
    .CLKFBOUT_MULT        (9),
    .CLKFBOUT_PHASE       (0.000),
    .CLKOUT0_DIVIDE       (9),
    .CLKOUT0_PHASE        (0.000),
    .CLKOUT0_DUTY_CYCLE   (0.500),
    .CLKOUT1_DIVIDE       (3),
    .CLKOUT1_PHASE        (0.000),
    .CLKOUT1_DUTY_CYCLE   (0.500),
    .CLKIN_PERIOD         (12.475))
  plle3_adv_inst
    // Output clocks
   (
    .CLKFBOUT            (clkfbout_pll_commbus),
    .CLKOUT0             (p_commbusclk_out_pll_commbus),
    .CLKOUT0B            (clkout0b_unused),
    .CLKOUT1             (p_bitclk_out_pll_commbus),
    .CLKOUT1B            (clkout1b_unused),
     // Input clock control
    .CLKFBIN             (clkfbout_pll_commbus),
    .CLKIN               (p_clk_in_pll_commbus),
    // Ports for dynamic reconfiguration
    .DADDR               (p_daddr_in),
    .DCLK                (p_dclk_in),
    .DEN                 (p_den_in),
    .DI                  (p_din_in),
    .DO                  (p_dout_out),
    .DRDY                (p_drdy_out),
    .DWE                 (p_dwe_in),
    .CLKOUTPHYEN         (1'b0),
    .CLKOUTPHY           (),
    // Other control and status signals
    .LOCKED              (p_locked_out_int),
    .PWRDWN              (1'b0),
    .RST                 (reset_high));
  assign reset_high = p_reset_in; 

  assign p_locked_out = p_locked_out_int;
// Clock Monitor clock assigning
//--------------------------------------
 // Output buffering
  //-----------------------------------

  BUFG clkf_buf
   (.O (clkfbout_buf_pll_commbus),
    .I (clkfbout_pll_commbus));






  BUFGCE clkout1_buf
   (.O   (p_commbusclk_out),
    .CE  (seq_reg1[7]),
    .I   (p_commbusclk_out_pll_commbus));

  BUFGCE clkout1_buf_en
   (.O   (p_commbusclk_out_pll_commbus_en_clk),
    .CE  (1'b1),
    .I   (p_commbusclk_out_pll_commbus));
  always @(posedge p_commbusclk_out_pll_commbus_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	    seq_reg1 <= 8'h00;
    end
    else begin
        seq_reg1 <= {seq_reg1[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout2_buf
   (.O   (p_bitclk_out),
    .CE  (seq_reg2[7]),
    .I   (p_bitclk_out_pll_commbus));
 
  BUFGCE clkout2_buf_en
   (.O   (p_bitclk_out_pll_commbus_en_clk),
    .CE  (1'b1),
    .I   (p_bitclk_out_pll_commbus));
 
  always @(posedge p_bitclk_out_pll_commbus_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg2 <= 8'h00;
    end
    else begin
        seq_reg2 <= {seq_reg2[6:0],p_locked_out_int};
  
    end
  end





endmodule
