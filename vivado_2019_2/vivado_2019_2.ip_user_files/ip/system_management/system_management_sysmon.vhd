-- file: system_management.vhd
-- (c) Copyright 2013 - 2013 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
Library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity system_management_sysmon is
   port
   (
    daddr_in        : in  STD_LOGIC_VECTOR (7 downto 0);     -- Address bus for the dynamic reconfiguration port
    den_in          : in  STD_LOGIC;                         -- Enable Signal for the dynamic reconfiguration port
    di_in           : in  STD_LOGIC_VECTOR (15 downto 0);    -- Input data bus for the dynamic reconfiguration port
    dwe_in          : in  STD_LOGIC;                         -- Write Enable for the dynamic reconfiguration port
    do_out          : out  STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
    drdy_out        : out  STD_LOGIC;                        -- Data ready signal for the dynamic reconfiguration port
    dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
    reset_in        : in  STD_LOGIC;                         -- Reset signal for the System Monitor control logic
    jtagbusy_out    : out  STD_LOGIC;                        -- JTAG DRP transaction is in progress signal
    jtaglocked_out  : out  STD_LOGIC;                        -- DRP port lock request has been made by JTAG
    jtagmodified_out : out  STD_LOGIC;                        -- Indicates JTAG Write to the DRP has occurred
    vauxp0          : in  STD_LOGIC;                         -- Auxiliary Channel 0
    vauxn0          : in  STD_LOGIC;
    vauxp1          : in  STD_LOGIC;                         -- Auxiliary Channel 1
    vauxn1          : in  STD_LOGIC;
    vauxp2          : in  STD_LOGIC;                         -- Auxiliary Channel 2
    vauxn2          : in  STD_LOGIC;
    vauxp3          : in  STD_LOGIC;                         -- Auxiliary Channel 3
    vauxn3          : in  STD_LOGIC;
    vauxp4          : in  STD_LOGIC;                         -- Auxiliary Channel 4
    vauxn4          : in  STD_LOGIC;
    vauxp5          : in  STD_LOGIC;                         -- Auxiliary Channel 5
    vauxn5          : in  STD_LOGIC;
    vauxp6          : in  STD_LOGIC;                         -- Auxiliary Channel 6
    vauxn6          : in  STD_LOGIC;
    vauxp7          : in  STD_LOGIC;                         -- Auxiliary Channel 7
    vauxn7          : in  STD_LOGIC;
    vauxp8          : in  STD_LOGIC;                         -- Auxiliary Channel 8
    vauxn8          : in  STD_LOGIC;
    vauxp9          : in  STD_LOGIC;                         -- Auxiliary Channel 9
    vauxn9          : in  STD_LOGIC;
    vauxp10         : in  STD_LOGIC;                         -- Auxiliary Channel 10
    vauxn10         : in  STD_LOGIC;
    vauxp11         : in  STD_LOGIC;                         -- Auxiliary Channel 11
    vauxn11         : in  STD_LOGIC;
    vauxp12         : in  STD_LOGIC;                         -- Auxiliary Channel 12
    vauxn12         : in  STD_LOGIC;
    vauxp13         : in  STD_LOGIC;                         -- Auxiliary Channel 13
    vauxn13         : in  STD_LOGIC;
    vauxp14         : in  STD_LOGIC;                         -- Auxiliary Channel 14
    vauxn14         : in  STD_LOGIC;
    vauxp15         : in  STD_LOGIC;                         -- Auxiliary Channel 15
    vauxn15         : in  STD_LOGIC;
    vp              : in  STD_LOGIC;
    vn              : in  STD_LOGIC;
    busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
    channel_out     : out  STD_LOGIC_VECTOR (5 downto 0);    -- Channel Selection Outputs
    eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
    eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
    ot_out          : out  STD_LOGIC;                        -- Over-Temperature alarm output
    user_supply2_alarm_out : out  STD_LOGIC;
    user_supply1_alarm_out : out  STD_LOGIC;
    user_supply0_alarm_out : out  STD_LOGIC;
    vccaux_alarm_out : out  STD_LOGIC;                        -- VCCAUX-sensor alarm output
    vccint_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
    user_temp_alarm_out : out  STD_LOGIC;                        -- Temperature-sensor alarm output
    vbram_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
    muxaddr_out     : out STD_LOGIC_VECTOR(4 downto 0); 
    alarm_out       : out STD_LOGIC
);
end system_management_sysmon;

architecture xilinx of system_management_sysmon is


  signal aux_channel_p : std_logic_vector (15 downto 0);
  signal aux_channel_n : std_logic_vector (15 downto 0);
  signal alm_int : std_logic_vector (15 downto 0);
signal i2c_sda_master : std_logic;
signal i2c_sclk_master : std_logic;
signal i2c_sda_slave0 : std_logic;
signal i2c_sclk_slave0 : std_logic;
signal i2c_sda_slave1 : std_logic;
signal i2c_sclk_slave1 : std_logic;
signal i2c_sda_slave2 : std_logic;
signal i2c_sclk_slave2 : std_logic;

begin

          user_supply2_alarm_out <= alm_int(10);
          user_supply1_alarm_out <= alm_int(9);
          user_supply0_alarm_out <= alm_int(8);
          alarm_out <= alm_int(7);
          vbram_alarm_out <= alm_int(3);
          vccaux_alarm_out <= alm_int(2);
          vccint_alarm_out <= alm_int(1);
          user_temp_alarm_out <= alm_int(0);

    ibuf_an_vauxp0: IBUF_ANALOG 
       port map (I => vauxp0,
                 O => aux_channel_p(0));  
    ibuf_an_vauxn0: IBUF_ANALOG 
       port map (I => vauxn0,
                 O => aux_channel_n(0));  

    ibuf_an_vauxp1: IBUF_ANALOG 
       port map (I => vauxp1,
                 O => aux_channel_p(1));  
    ibuf_an_vauxn1: IBUF_ANALOG 
       port map (I => vauxn1,
                 O => aux_channel_n(1));  

    ibuf_an_vauxp2: IBUF_ANALOG 
       port map (I => vauxp2,
                 O => aux_channel_p(2));  
    ibuf_an_vauxn2: IBUF_ANALOG 
       port map (I => vauxn2,
                 O => aux_channel_n(2));  

    ibuf_an_vauxp3: IBUF_ANALOG 
       port map (I => vauxp3,
                 O => aux_channel_p(3));  
    ibuf_an_vauxn3: IBUF_ANALOG 
       port map (I => vauxn3,
                 O => aux_channel_n(3));  

    ibuf_an_vauxp4: IBUF_ANALOG 
       port map (I => vauxp4,
                 O => aux_channel_p(4));  
    ibuf_an_vauxn4: IBUF_ANALOG 
       port map (I => vauxn4,
                 O => aux_channel_n(4));  

    ibuf_an_vauxp5: IBUF_ANALOG 
       port map (I => vauxp5,
                 O => aux_channel_p(5));  
    ibuf_an_vauxn5: IBUF_ANALOG 
       port map (I => vauxn5,
                 O => aux_channel_n(5));  

    ibuf_an_vauxp6: IBUF_ANALOG 
       port map (I => vauxp6,
                 O => aux_channel_p(6));  
    ibuf_an_vauxn6: IBUF_ANALOG 
       port map (I => vauxn6,
                 O => aux_channel_n(6));  

    ibuf_an_vauxp7: IBUF_ANALOG 
       port map (I => vauxp7,
                 O => aux_channel_p(7));  
    ibuf_an_vauxn7: IBUF_ANALOG 
       port map (I => vauxn7,
                 O => aux_channel_n(7));  

    ibuf_an_vauxp8: IBUF_ANALOG 
       port map (I => vauxp8,
                 O => aux_channel_p(8));  
    ibuf_an_vauxn8: IBUF_ANALOG 
       port map (I => vauxn8,
                 O => aux_channel_n(8));  

    ibuf_an_vauxp9: IBUF_ANALOG 
       port map (I => vauxp9,
                 O => aux_channel_p(9));  
    ibuf_an_vauxn9: IBUF_ANALOG 
       port map (I => vauxn9,
                 O => aux_channel_n(9));  

    ibuf_an_vauxp10: IBUF_ANALOG 
       port map (I => vauxp10,
                 O => aux_channel_p(10));  
    ibuf_an_vauxn10: IBUF_ANALOG 
       port map (I => vauxn10,
                 O => aux_channel_n(10));  

    ibuf_an_vauxp11: IBUF_ANALOG 
       port map (I => vauxp11,
                 O => aux_channel_p(11));  
    ibuf_an_vauxn11: IBUF_ANALOG 
       port map (I => vauxn11,
                 O => aux_channel_n(11));  

    ibuf_an_vauxp12: IBUF_ANALOG 
       port map (I => vauxp12,
                 O => aux_channel_p(12));  
    ibuf_an_vauxn12: IBUF_ANALOG 
       port map (I => vauxn12,
                 O => aux_channel_n(12));  

    ibuf_an_vauxp13: IBUF_ANALOG 
       port map (I => vauxp13,
                 O => aux_channel_p(13));  
    ibuf_an_vauxn13: IBUF_ANALOG 
       port map (I => vauxn13,
                 O => aux_channel_n(13));  

    ibuf_an_vauxp14: IBUF_ANALOG 
       port map (I => vauxp14,
                 O => aux_channel_p(14));  
    ibuf_an_vauxn14: IBUF_ANALOG 
       port map (I => vauxn14,
                 O => aux_channel_n(14));  

    ibuf_an_vauxp15: IBUF_ANALOG 
       port map (I => vauxp15,
                 O => aux_channel_p(15));  
    ibuf_an_vauxn15: IBUF_ANALOG 
       port map (I => vauxn15,
                 O => aux_channel_n(15));  

-- inout logic for i2c_sda and i2c_sclk ports


 inst_sysmon: SYSMONE1
     generic map(
        INIT_40 => X"3000", -- config reg 0
        INIT_41 => X"2ED0", -- config reg 1
        INIT_42 => X"1400", -- config reg 2
        INIT_43 => X"2008", -- config reg 3
        INIT_45 => X"DEDC", -- Analog Bus Register
        INIT_46 => X"0007", -- Sequencer Channel selection (Vuser0-3)
        INIT_47 => X"0007", -- Sequencer Average selection (Vuser0-3)
        INIT_48 => X"7F01", -- Sequencer channel selection
        INIT_49 => X"FFFF", -- Sequencer channel selection
        INIT_4A => X"4F00", -- Sequencer Average selection
        INIT_4B => X"FFFF", -- Sequencer Average selection
        INIT_4C => X"0000", -- Sequencer Bipolar selection
        INIT_4D => X"0000", -- Sequencer Bipolar selection
        INIT_4E => X"0000", -- Sequencer Acq time selection
        INIT_4F => X"0000", -- Sequencer Acq time selection
        INIT_50 => X"B723", -- Temp alarm trigger
        INIT_51 => X"4E81", -- Vccint upper alarm limit
        INIT_52 => X"A147", -- Vccaux upper alarm limit
        INIT_53 => X"CB93", -- Temp alarm OT upper
        INIT_54 => X"AA5F", -- Temp alarm reset
        INIT_55 => X"4963", -- Vccint lower alarm limit
        INIT_56 => X"9555", -- Vccaux lower alarm limit
        INIT_57 => X"AF7B", -- Temp alarm OT reset
        INIT_58 => X"4E81", -- Vccbram upper alarm limit
        INIT_5C => X"4963", -- Vbccram lower alarm limit
        INIT_59 => X"4963", -- vccpsintlp upper alarm limit
        INIT_5D => X"451E", -- vccpsintlp lower alarm limit
        INIT_5A => X"4963", -- vccpsintfp upper alarm limit
        INIT_5E => X"451E", -- vccpsintfp lower alarm limit
        INIT_5B => X"9A74", -- vccpsaux upper alarm limit
        INIT_5F => X"91EB", -- vccpsaux lower alarm limit
        INIT_60 => X"9A74", -- Vuser0 upper alarm limit
        INIT_61 => X"4DA7", -- Vuser1 upper alarm limit
        INIT_62 => X"9A74", -- Vuser2 upper alarm limit
        INIT_63 => X"4D39", -- Vuser3 upper alarm limit
        INIT_68 => X"98BF", -- Vuser0 lower alarm limit
        INIT_69 => X"4BF2", -- Vuser1 lower alarm limit
        INIT_6A => X"98BF", -- Vuser2 lower alarm limit
        INIT_6B => X"4C5E", -- Vuser3 lower alarm limit
        SYSMON_VUSER0_BANK    => 44,
        SYSMON_VUSER0_MONITOR => "VCCO",
        SYSMON_VUSER1_BANK    => 45,
        SYSMON_VUSER1_MONITOR => "VCCINT",
        SYSMON_VUSER2_BANK    => 46,
        SYSMON_VUSER2_MONITOR => "VCCAUX",
        SIM_MONITOR_FILE => "design.dat"
        )

port map (
        CONVST              => '0',
        CONVSTCLK           => '0',
        DADDR               => daddr_in,            --: in (7 downto 0)
        DCLK                => dclk_in,
        DEN                 => den_in,
        DI(15 downto 0)     => di_in(15 downto 0),
        DWE                 => dwe_in,
        RESET               => reset_in,
        VAUXN(15 downto 0)  => aux_channel_n(15 downto 0),
        VAUXP(15 downto 0)  => aux_channel_p(15 downto 0),
        ALM                 => alm_int,
        BUSY                => busy_out,
        CHANNEL             => channel_out,
        DO(15 downto 0)     => do_out(15 downto 0),
        DRDY                => drdy_out,
        EOC                 => eoc_out,
        EOS                 => eos_out,
        JTAGBUSY            => jtagbusy_out,
        JTAGLOCKED          => jtaglocked_out,
        JTAGMODIFIED        => jtagmodified_out,
        OT                  => ot_out,
        I2C_SCLK            => '0',
        I2C_SDA             => '0',
        I2C_SCLK_TS         => open,
        I2C_SDA_TS          => open,
        MUXADDR             => muxaddr_out,
        VN                  => vn,
        VP                  => vp
         );




end xilinx;

