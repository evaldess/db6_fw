-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed May 27 20:03:05 2020
-- Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top sr_ram_adc_iddr -prefix
--               sr_ram_adc_iddr_ shift_ram_adc_iddr_sim_netlist.vhdl
-- Design      : shift_ram_adc_iddr
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OTm5CjpG/km3D3WLidx+2HAHwJ14h1YFhnsfRLbjVbOA9cL+3eHHCiC8srSbKNfiKqg44+FAfWiM
xIpeI28LwmtYE4TtfjQsjJNSMTeSB5YZCDHUTSh+wq4y/7huZirDgvcOH0puEE3mUCATa4b/952f
QGg2tTf+pc/VM26HQtFQqcQTKSDWwkTB/JHNwl5hcybAdcji9ScGRlaZZ6fsKGWzNYnxEGyxYYFQ
wvcj7WJxdIqjba15mWutNWXs+KlHhUOcEH3JcwvB48CX2FIQeXa/TG1ecr1a9atcfNXI273hVzw6
CpyiTW/nBYrIw5p7+CVuDwBhzFEmPktGMDCmQw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OsspvzRazVFf2c7qINWPzJA9XBR7qCFylwO+36uqOZigPrGQO+xbF9nljah50n5dWi0dxSu7uAhm
ths43nABSjHrzuKck35s5/22cH5dQWMFGkspMOtJwY82Jm5ZM1SSBPsQ7YqFxdy5AVwvMofsxy3W
m731lgDKMffUBCPLBTkSML0HjbYB3+qyHXgu09MujVQdcyWTKtumoiSdmirnXK/uCKTQX3sdb308
0sxTd0c/6ZEZ0pIFIIW6e/dTdAqiqFdGT0YyNS8KUg7rXossbBC+PYlGQpG/4ilLlYnWDb3O9I2Y
E3x2gxQgswTKjLWbRUnFFGQAXNY2TVNMpRl93w==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17728)
`protect data_block
/OijV+/fT3a+DWkycKDraPJNR9Gku3TUCJwD8IhSzc+0DEzTRImr7EZmPujfo6nwmcp1hBq8CPny
S3exqVKLtga98eUDdjpShlBOwkSSLp91ABdSUTs3Korl1MfUdowaFt03jsIyhG29pWXDZh0aHQUD
OAxjo8iLaje8de7I3XTC/UECW4Q4JsISX1W/tjnsIuJm6EvRoa065K0ZHghAd80iWlued03GiDAG
GmsT8EJPegt33gY6jB2E/pfXEojBJWVRvI3cl1XTn2JWbA83bRWU/vq5nAb2hhFcTrMxL9GpjFPW
/rBSdtVpa8H6+4HdfchacD5mf9J+f25BmDPdoWI/owyAdrAbfo62GxvBMJmZdXdOKndElHpOrGSW
1xSBGuysIMMm1pAZq1/FhppaxMuYWiPl+CvzajvcXA6+J/TMV/D6Sm8hVgdXEh2aQkL1VON8le7H
36LwaCaksomk/kSPlSZb4dX9XBmMB7lZ77gFKIOM2DfndGMlCQ+tc3FWICarRi4L+1J1d1bzAqAm
v1HcctiQRTGs1RtOc82D/+jbZUgdnAVlO02WoMPGyGmPCgvAKxbsiH2GKu8zfSQP+YAhamncUUrx
vyjYGKwG03qyBUoSYkkBztXuXduSxRz4CIK08daf2T2WCxmGrGZ2HM0USTxcF24woK/SBUXCSvOB
S9fMGx6ObD/jHldAPZJv0kvIPKDs1TCmXjxixZPj3HjdRf/Lj6mvMdQNg61ZQLTf6MqGjnpA+gAc
LwFT1K2knHaAR3qixhcNVUXJTQEKm1jpin7T/lR95DXD3nM+b/5BCQyg9/e99KhM1MxkqyPLdmes
JtpX+uSk72oi64AIPOHt0VWkHEkzRrbn7Sp9Wzk1hZ9JeU2U+dnBQ+PdTzc+pNX0Y/8mV9GNncQ9
/+DVtcLDr+sXYklsgE0wVEhBZAmgJJXNQufmNVddZfkHVSYiloeJXz1dWjN1dU3VYn7h0MTrJ0Ft
6YjpLw4VUAiM4ZVcHhdByQ4h0yG7JAxtH7gC1svhOTzdN4zJup41/kjTWUTmMWStU1+Wsk5zNnYa
KT0IQJD5cQtWjEYWBon/WKQfx08JR6WM5Qry80ukzMnLhCmVqX4fPX2n0e3LAUagXdKo+LhcQ0wv
mrIG6dNtyNO6IQHTvOZVIpkuyZ0HmO5SaDlLCQCcemx9JpZHhg+iimuY1MEAxyekjK71RK+EheHP
CQh5oA+07c0JBUNqiAgt5+ZGx4JbezVYWds6hL0Myd4oTpdGiw9K0KYXHyM3xejp+Y7r7jr05yN3
7j3sAvzo/1r9jCCkTiNgGHnxj4mMfuQQUvSL49ZzJ2tIBBl0Zy+vjXaxxYUNIrMt8CQgJ8NPZ4WC
S00AyI/HTopZrP7pASH0ejZbXNKjiLY6tAuwwnAER9NFcOfHo3h/3N0h7l9JChHvnjJP0GYw11zG
5rrOXuBfPFelso3Bu9UhOeXWjQLaML82MrQqhU05YDS40zbtloC0HxwCuy9NqYltBJwE0ZnNFbQ0
ytZ0KPgKJCRpBNGdiP0maILrvlSr335WC+jFM0JJYfO+DliHCFOINbEvgNVf52aaXRoEz/Oedhg8
iKN2mlyVxnB6Q6foEsg5SrznW6DUoabGweRtjqMkmetLos7Vjcq56IriDtPMoGdLmrqrV3oHtHWL
Fz69O76S4iO+SvMB0DBxcXygLJ+AMDxCRW737nG8/ZgYAWuybYWdcO92aoFEWkdZOIQfRKYZwCdr
FWCDnRutxX7f3qPvDn61tTMexFtulTTImrHP4DUkbazDTJTiKo48YtyBDeIj0K1B+4kU/yjYVP1g
tRq1aHihhoo/PO5Ilv3B01vHMcEGOWkee+PzbN98GrNVReDyCmPcXBIwz864+ReeBb3FVsew++bs
1R6shy7Kt/D/tel+75LtZKf781I/5v6xRqvzVWQ4qxPfY+awP3GnIHg3T1Syo1XhlYnbAc7yQAbb
fvWQx/DSTeeGlS6zmefFadoRQ2yAIc63YXhuorFM0wIMZ2RJW4Ey1HDXZklbpsScOKgFwZl4RKZh
aaqWCCWlM8O0XYh1SVOoagxwJLoncJopMBhZCVDgKZmKhh8s6AmM/Qc8BJVBzMrM/TfKgRd2Xszi
q+g3Z9MRQWmobdt41nwrWFfmy44+R8Z01yYB/3Cyz7ksBceFw0j1f5K3CeJ7TzDa2IGx+RnRrZvg
7afTfqPWuEubXFK7J5KbulAZ7cZ1tzJG3jBRjvZ9Y9QytOQ4950e60QOevrMCzh8Rf8F1nxM6/qJ
jjHVDJcAAFM0XGl74yT+teLtMxdLSZrFJK//jZ6FRWyI+va4AdZhtZB515XgN7kDizZUklaNAT9v
I4/2wSxmGPviX/f8mv2LWe1hAbIWdCS+lxcXTNOLJ56k7MtxyvBoNReWt+tofcfZA2F6mGmpXPB9
eUJt38iP3wBE6KTaGp5hgJK3jRXgN02D7GB7E+t5ziEt5LdizDcs/aR4vUc01FUKbTxRszAHUMw/
k9l7JDyR5J2jiF+cE9oYApYWwMqBvTuSnGJt0Eq7kbuyhBJ1eeuUQ7WJgYnTHom92aMIaULM2qBu
IyUk/R+4q+sKtQJQIo0ZjJ3c/kr4g9DlaMgZUeXtLB9EWjeXOK4Nj9G5BRSzuWApfMVkz8Wc5YxZ
p63glq7gNpmHydxZRFglBCB057j8G/jerfKbQU8kjXMjt4dulUax88B/ucPQuc0DREaemRTESlAN
tBbqiNQp6V7CUvYtBIWVkJ/f04oZAYwVa8l4t9sgaT4xd2jcnPFjQf+hZL+Cox4POBt9IYbH+NK0
pFY+R7D1bQJ+M6Z8U31J/okGY8o5LijOl5/SNubjN8HGQ3HAFa4ItKQTqnNA+rZ6FKKv0E7kpZmL
U/gBRjaAqohxEDiKOJ6HDGA/vKFKtfqonVQOsG2xktXjODrscY094czz9JSFS2wY5xUvwpEyn4Ea
jxDu3oLMZZRaWkivfoXMHQb1tegP0dV+VaAfncJV03/NkL6QnRFwPrSd4NCtktwwOVID2c2EFMfw
MqC0Xeuu5ESS32o2QVtm1+FPjIE0N9tsbtfj6GAKGC4pWyL2ys895Y6PPyWeTSIkXPEcSSdrjbQ6
16j+740ixfXOtSMhrzx1LX8pDNSxHsJYi8xRsJbD6+AR6Y8wD264uDt2CFPsEFlNqwz+QTsnGzH0
b6zhYHwqokyi8xCoHFuQ5fML5BeNvmpEFoOba3sf0PyiME7j3FBg93thfZ/dEphMPt0AnvnXCd2K
BLGAZNKpd9+rtduefsaBQclrHtLKI/+JcTklevdzJaoPn5O+WZsNrNwYu1wVaaMZIRsRgkta5HKG
ezKEWnLm8ZLeKqMXRzYGSYGmvW6lxDrR1ALhvU4jtxUD1B0WyYupK0GzbHJjhnIZt4Jl3zQR3/sq
HmbIxgA0XIkOv74OsvqOu2kIfxxeYqEbCkxIsAUCNfV0OEXNdCoLE1kYmVu/oKFldnf+ihLkpulR
cw5vWQmG1YIp+b6bP+poACbSHQwzuKvWMCZPH87v03H1ASl2NcK+Ac6zo0F52jD6RbiBJkNDTEig
bguz0QftPgtIA/RfSEd3+P1RZldB08BNXLEhd0WexIFnamsHGn/yQbkiJFhLkUlX0M7A0YYKjmvM
8fzECLR/okR9qrehuMTJDVcEIZqwLAo3rHWWImyk50FQVpyz1YQ05BCwe147OIGA5ZuG8mINlX9d
zYHVCIe2LKowWPupk+NfwE0YdmTNk8p/CQ/5mcbXGK6OJI4Xyz40+fwKnp0kdwI3ajRhMzXY44oB
eQLrFmi8FRlC5BgzbL31rAtF98a8Cc8/zlUQL90+wM/Yf1+KyRjan+ZQZdWcStUe7bKfXNRWbj25
y4h58qUMufcgyz6hUSZKeZdxxxpP+AK/Ek/RGnsAiUpDztrCWVQnifHzmCSoP9F1qrPy2VqfpRPz
EMM84EEyefiyvXNzwaxLw7NoYG953qXZsXrXgAo+0u/ah6ZxgfT+bltl87HpZlrEfR+KO8NmCJ1k
8eaDQGho3mUaNiCzEqSWiT5tHLac0B3mTrgbCuyUFga5y4sEIZdV8ClM8wMm61255KTJ0sgvI4kg
mV9L+rj/kuu6zUc6AXBnwAKbsZ4QlD3IqixwVNUYuDajL4G3aThk6Sf/vBqw4EiDAdSUbJpu0k6y
8auqbmtcONoenNRILe2ZZqkhU7MKuwUIlq38+mp9iskitaTK06WSBcJ6Fuy1FgFJ5mNb3YuwJsqP
WqngpjAYYxOSgPUzhF6KCy8Rk7khyAum/OZOAweVUMO0zYpUuvb9+YbXLqXrXuNSyd+EOCoTyUnw
Lg3ASGx319McdHdwEns9Z7OuNESIwDgInZ7pT3iCJqwitd656rH3e6huQhsgZtHVVt4ZZyvY8eyC
I82rvMG7Y7ZmN5VAlrqs5T0OWGxGkt+LdVwib+UrZC9hT3vWrp7TNqycBiobR4ZMEqMvBFuoonT6
C7THcNEJT3YK9KMTPqylA6AhBjF/zjYfOwySMUjThYSCL0wd3/ZHfU9LiJCedYZlct1VWJ+5n5Tp
TD2ThBNu5jsjgglPjBl+aiWH30GBPZS14WnMRU8FmoUbSrN5vo9PsBokYMq0kvd0fpgiCUk1WnyM
Bpfw1emUpG0k/MFxumr6AAR9Ylc1RjBdj3+vkyzmi7B6MQc3MmcC/mEc02Fd/sOYwdiyzAh0PmIj
RIasXzhzbKfVInHza+rOHE2f8O7tgiuCq/5ch5J02MNv8ZvBET4HO9umI6/WCvj8YlayVA/TgmRV
WIUdLN10NmZF+Pq9WUNwlXNVur1BrZwCEEpeOs0zLanKpWKodYcJY9eOBOPB81r2xZdD+lus205J
P1RC/PjcOLGTAogaBxg9aZtALZlzXeI6lEWNc0Wvazy6G8H+RnChVJGsigizwwbkn48bMN1YR775
P07MOYEjgV0RpNPkmMuPyRoF/6HOnat9b4zQyknz+WD9we5Fx5Bl6tzUS/dlWgj5A4iEg7ri+HlA
7QM2UMJQPkPkw9hbP8T4+ldXy6j7uvJoZUk3HIlUVndFqfJOp7MqIznX+DMgDV+h9b8i92Fh/Uga
qJF2fzSo2QFvw+qNBrvTAb5U+6cuDMQIs5uVNQWs49xoaTsM1WJhxrKC+TUaiXsoFZxk8WYj6Xpp
Oo5UDXM0EKItEucnDjiktaqmvS9KU3c9UIRGrt7iNqTMPsfFQsE16kiojsQYN2+YFRt/UiWVk/3V
89pgh/M794tIUDIThvNVJDDBeJVgSiEyabqYapoFg0m6CPQAA5sAuYRAsIIzjQocXiJwOqORoXGg
UVFPfecyHU2wZXEIUNWXHLthDkHPqwOa+6366sj4whATnN9T239I6RAjbL0A9hB2gUR5vzZRY74B
VICB317C6XL671QvKmpAG0+pFiN2p3O+KgbwNKj/HOOyViKjC3Ljt4CtHvyfObOqmcfQ1uwZoWiY
f1q7+7mdebHN3fikqS+Kc3nxnbz3tCWeuP1PFWyzQ+ytIfPajOs3F+Z1Rr6V5X8FnmYD3Zx4Wp6W
TSnAu2um8cNgvLqBgYRUqbZSjGKb+Z51qWQVglBjK+D+EVeVm0nCAK6mobQv+P9zhyhrG73axvSM
kQUNGRnb4IqlwITC4kJ5/edgr0xm6hLvqhCaESZGHNSkSNz34wZWo/oJ6px8nhmqksUhiuIpGgX1
Rllvt4Ip1AhrIZYHVq1ALkxOmohN04Yc9mF9Y/Oga1f6jf0vJdLJs+LLAO/gKLmgXzv0PrtIBNau
uMwGLZyj9xVeKjEysSIAZMZ5jPtNFb68RMWLrUeUhnIP5ixDUjwVFsM3l3hy5/05MWLXBm4Vh4nN
i0B422wNyyLEXbjYD5TeF15LTqvbW8JREu8lZmoLc4V7/Zywz0lSSzdFdMmQrJGkbqn3EQa77q1Y
dA8rArd3Xnd5TavSETG8f+iHVBAZ4KufvKHC6WOzg2bU7JvN4bSq9VEnsOhFXan62IBdO1ibtEtu
1WIhi9E+jkvGCFvUIEyuESb4i0fgUzv6FnF3MY0Q2cdOQ1hlWbYzhA6it7cZ/V9VCP2MxB8sSJ5y
bdL05tnfjjpgvNw/IGBXV6PqUEDf9rwgh9gEZLdAnXaK1JfTAHTZo0LmCjjloRWrNd9+pUExtrxB
RfRVj833v5Di6xrX70om0viksZMF0rXyiC3/Bn67+3zmgsadM5Pubf7Lj+IjyH48eMZZH9ZcRl+i
E+rte0popW8sbqf0yK3MRK3UorACuFdJAm6nYLeHmtwPF7m01EgnRp3qDoJ1AJqj0F2C2w4XXfju
N0Igtyi0LSFrhfvdo2nNbKIHW5VS8RQgAPixJqALMTnVx2DGSFCS5LHJcpi0GN5frS5AyES716jN
jbmSR2IxulQTlf4l1fntPtrfCKqTY08leIn1WGQygyf1b8S8WYWUUCNE2yWXteFjXNIzzXFYXtdb
THwcghGM8FYGWFzPZJoaBRA144MdnDUtJB0dNIuUAbD1O6cssDoS0UiI5vcxtd3yycQWXu9GP/TE
wHMQOg1s6IA4E0Pwu8Xd9Mjm4Bz+Mw4NHIzDQNheDtIW6digARI3g14ZSU06u38ql0IO/+JnRFrU
cR6YMQOyDxFMfoQcwCJJdtKu05ArTUIRGWT8SNbApf41ew/uiM/2MBeqD87+FsaWk58CdGhbwmSR
WFGZ8jp9h+OiO2Vnbi+KbTkMSbVHyeV57syIsIn5gQO9hdUDTcvhONybmcszDGG8lfiQHReBO6Xd
Q9rRyTaZiPpKdufTDWCwkIne6sNhqhAizg1w+1/8k+PUZ+Ymw2NxAx7h0NOL9BTlbT1+UJLEg1II
a6hLd3xjVj4i7/lXuUa4NCQwnZhqPNKRdfCpCk/AbzrMlzmDmi9FDcFZvH+mm1w0YbWoBzE8g4Ms
47HfhWD3SjVeANgoEpcSIN7k3bZ6xOEbwiWGuVTfLbKqYxveynf+RuXdTDSD4F+FLf4nwPjdLvxN
9joWJb4WSr8E4BjarRVD58rE08usZ/vDlGaGSekRmS3L4xdOfWYR34s9K5ptaNnrRMW16TGvVsqk
7lLz0JHm/xWE3AxBkM2J6c5NMElZ4V85ZHNKgfwPb1NPU7cBD163Oe9SI5JBpVgm2M14Qnuue9hP
yVYQMe69Gt0Pos024HNn/Y2nrO7SsBBK2LLFdNVHI1nAzyVrHMo3CalJzCTMkxAaaki672c0w+1P
dVjdSny13mdZQ6LrNIBYfWHGxpsnsjFcayt4Xu6UCQHLtz/msLtA6iTZyHUFxGICakOuWOIUIMGq
LLTjFcD0V2LsEqz5KYj3xiqRSzLCFn+YqYO+rI38YbmBO4vkLjafQn4pkrlA9K4+qCm/L2B/3yky
KfNcIu+GtsaHkeUdFgkgYILVt6Q/lt0K6U0iZ7NSwMMV7pv4u3/ZT1FIn2C1LyqqVdpy0WE2Tbv0
l3/a/J+18aInbaDQHMx3T/YQ2Vdd/fdUjO6QyRqUH++f9uhd93ZwVn+FBbLCCp/rpN+PDQX5CPad
8agvecoSb44j2U48cWLuYMMYSfYUoq9UFaT3t5wiwsCKXKJIp6nwKWn1g6kXgrMVkvn2vm8za2sW
XpYVoGnMMgkaxD7ZfDtf4zvpdKNJGh399HvlZeDEuvvfm4pb5vbj6TzjS4XY0q6GZwjrGF9CcOmR
5jwyBljmQkwb3J4K+VG+oRytwFPg9JvtZVq7BadRaJUQBITHLBhIVZYMULJMXvCH5LSFHSOQC4Ma
RJ2Oy0Wu8l/7RmNYc7PWdlX85pe9gdnxA6/EvLroPEZxVnBPattCh6/FjdlM7v5eCcSRM0CBtusO
nFyY0zw3BAofc+s4F9qu6bIgAnwOvK2MV5KVgDvGYP1i/gISf+HQajqmDYhIoZ2SrSmVgRXEmdOF
IRTXtYOwjNHotC2E19SmaRDw19/VWbxd9JHsHelnJKQYP3L4Zi0VMsbMW1ZpbyQiqCcEFhE0z2or
jvCtNl1UW6tBZe+U50opvDX9M60cOFDRW0Chtwp91KzylBA/nyjfWafyhmb1NtNsSOu6NflkKvpO
MtiWDNRRQrQi3SbhZbNaR/Bd/60ZoPOx2rHAt8f6YiOgsnjcacz0DAFdXHn2119Kypi4+awwU1zC
kT8FrEVN0DNUxN10Gty0HRW2hLehzE0YBi3tU9FM6nvF8zq7P1Vx9IBJ/zfN+zyFSiIQEM1r0Hod
9emEFyeAmiLw80JxZNVS3MpbCyWTi21KRJyi/wbwKL8prdeMnqBKBgEbJDKi4RCpS385jiX1nRSB
kkQMZtSum4QlvRbZK391pO7Y220wXe8C4tG6zFb54Ja5e+ur/kYVzpIln16O5L8o8stmAHnzJhw6
wKJtDuEUqAWp/nxLnlMwJA88D2sE8FTDHOGBVSJTRXoSsAQsPqfgBwyBJ38m5AW4+TvQCyAUcwXV
kmLLbg2TNMQE8xf3BNSmjAmYD8URGR2Xz0lcH0qEOxbCmn64LunhL0hIFOAo1B40NPPFhdRvwq30
FxpM31s3j1rYsDtTbVldGkNQOoYZ6eJGGCFcebBYnzXBpglRm8zuY/cSTJcD+RfKJVqylIR8hHNE
j9CgXCQAft129iJ3M3E80iKss3GTXd3vWr6DIe76RnbXYzXq+WOvrHgEYRxD1XdKmg74Q3sljsTg
849bai3qqkMcv68ZnOKBGpUz2Oi2FLviXMj5qhnEkE/Fbb09gx5LJjWoL1LpswSXerfiBW+kJGp4
KfOVaao1Q1DE3cDWhdWShHU+ACNeWMHWptzJ24cjtfSsrK2gyofRy50ecN6cXrrlDrI2z/60peCl
bNIWoi1pFa/QdZNRTitgOJIwH+1l88hhu/JBzH/6+cgLkW/T2iTqQ2Mt3Tz77jFL59f7Qsl4UzwW
y72DQHJD2/zPIXWCt715drY913V4xwKihrZ/KuV/jXvts6gS0bu8bXZmyNaSnqIzLSZ2DtqYnBnJ
7isSC/QDWy26xzAW1U+P8R9IkJmnKxPsV+DfWL3avgxwAD6GRHkBvXUBvUvG24h4te55tbft8JR3
/BIRncjFfjaBLWFIXJIc3KiOiV45GQxKFH0dKoTLWccgsZQKmpUVI/ZL3+HUxTbKPCVs0Qi4j7S7
EqFEy2yQYCRAnB3LteX550l/4WPJ2LrXHRjJt8wdNGUxRhksbNrDHkcXnvlOHa4CE/ovpDABXyoz
ph5js4Qv+J2V1jI+KIiqs/8AQnIN8xVFcKm+rrzvfAc0dzQ5bkUFih1iUzBXsZKV3HC306GkvyF+
tmBC1pyr0kMF6baiY1ZNDgXOORvYiX3WTGX6UVNbRvohVcP+62ugHJ4Dmnv4gqeDILdjFqcyhwIv
PayLDoQ4ybiMXb93DUb82QJH7q3nkJTTitSLZ3MbrX7+UMIykwxO7VwkJ1m2ECbSiGsC/EOenMM7
clckSF/ygxhT3FDCHVslY3GKXyAnl3srvU4VvgV5vHCPyi1HyF3/J9x+qTmVhs1LpMXIDuboQVVP
8EemAGzKsXq2USsGuHvZ0nhWd3ZIPRxlu66n+JFyZmJnnAsvF5ZQJO8CCURZtvd6PdpS4d3bwYbI
fehU9k2Inij6RWcG4vU1lcn+1lWZI9ekCF6dolMtPBUyHZa2MNtfC5PyvYXbvcKiJJyIuf1dJkvE
HT6j1kVF2LN7/rLsZAq1q2/raOskNTKJ7XrnbIAffgBZVILb/l1jV8BTMDw4WIK4OeQx2Ot77TTy
Oqufjh6boGm9NanBIy04FupkRF3Lr44HuED5tjsWemSIDVcAOAnK9luEkOxvRfxNMSCncoXhh4Ie
NQQ6MbycIG5jZRr/HxDxvaUjwICRbZqwIzEI/gD/Nly42775465dr1A8DEQdevHFXlD35xSGHkk5
45ooQnnK+tMRJjF7idQkc1r1GZIPJhX9Y6vnSbTL7mAUa96AVwohIeNxfOa9khf+7LEXEfRNaQUf
bckYB2F4OSEqEQumYrOaG1Wq7dQCVZtIE4DkvDKedHxF5HxlBU175mhRjDCReiTwWqATtJyUYS0H
YguPP/DQ0HlljpWCtI5BIAuCzMFbNDWd76bP48nh8w+32qcLECaHCIjHuscX3w4s0aDUtwcGZ4u8
SAX8MDk5dV0/yNaFql2RrSKpXTXCIWrtjykAgSS1dgRLqfYBs9l9XbyOkl3agYdZLGqWbqWFfYeU
wGtcoC3xxU6/Q8vMw5u+uLUdyfrVyaWCmWmMPX+FuQeFRMbcCngA9yFrNyHM+gf2VF4W1ETNBeF8
fb4oXzskZoA8dHgpK/z8VSr6jE9kMGZi4q10qgfBUq2m0s1VfX0u5qNc9ZJb6jwWx5ZUNSGO9bxR
StyyVJIQUz9FbwpyndP22N0/p4F2+wGXQWedasHPpZrgGZKlRLdjf9lu0fYQFXQJ1mswiu0ePKGx
zhTlwdewUQ/x3xZ/kS9mzpZSVwwhdmL40kCma9lmKHJs+0D8Vs3EWg5D4fBfM+x0XmV3xlXv93kG
fJ4W786vSGkKhnQIVyQZ3WhuGHJbl7YnwciZ566qUvGyBIzCs455xKo7t+YmVJ4rGV+lbOgS9Mow
5QwpSti4fsMrnW6LxTk5KlKg12trp7d1raTdtoFNJp5TYM3kJlpmaKhBDS1PD6cMeZhAlrhxbKjU
YR69dZA3BuClG7kqB+5Mc4IRNM5yp0xQ1M+UAUtLq8BwyOVh5XmpmNwVKECszEdY/mjKfBBvevCN
iNyIWygSWwCPYxLBGi+klbVAa9ogXelqplVrLqBcDUkBLfgJ7679LUC5bxE19rHBEodggyZhdSDJ
cuBLXZMghNZkKVOGJm6FDrmSE4bzvBkOg8ttc6Q8P0kzdCWgRxk2a83IPNggjeGvBCjcVlh7weF7
rAZsZoBzmq4YYU4BOXkdQJIvYD5EEdvHIyObFo9ZoLG2O6VPUjvlq/ZT+t7QUAl2oxLnDZvcsjdt
NhkIRMGArrdl28qsQhbK5fUEjKgVTBYIvm8/3KTYMcsKGzem5p7S3R7fRrnL5JyhuPZ60Kd30BBX
6lHVD+KuCfRq9CQx5/5037LW8bQ535Ty2qw9yu2lWpJzLtkYHb+FEXIrTpjwkkWCCNzJ4KZoEsiI
xYX+HHzl8OUBreydTPGRbW1B4u5GTzKFnDhs49egJeNwAezynj9opa7ZL8qYb1m1+FNXrPvqHBKA
KJ3EAQSRUU5SaLt+mRaMikrNeYuEnhDN2z82ub63PhyNkPUKs4yF5JlYOL6gr0f6Ql0fyYxM4WzD
HSZlC2wjiFkiAmEJ3Z5hvc9hvkjwfsdGrXbXhgHwuGQ9jf6stnoxUdJ5zcwrZUB02Pz9dpVICVlR
T9882ym7JRko+Sz6L0D/IvEvBwB/jsiIEjrUfGabNKJCZB4D/14wbz+PtHDteBXowDGXHr5j8mO1
vHCElLfb2Ow0oskp21tE/khWsN6xOdt6qqi3PguIvZy6hXTibXQ/ettDI5gadnEI/RU7bXBZPimQ
YFVF22jwqcMYIX3iNdn7A1qeiGgVOaQvczFBIw1fTt7W3vfF2QGkroSsWCN1pDnFFcEPwevS/d8h
cwLyKCOHrnqtHEGK7WL9pWE+IqeItDYG2UhK8TrYZGX6ruKDS8pjCGuhLg3yWBYlIIxcn8fdXctX
Z2B8IdFmdbsg1tumbyK2qbn/eQtDA43DEW//McYCsxNOEnbVL+m1oizUr6pUuFeqCFD2cTHZkhtS
Mvu+OAKP+sJ1rFbi1so8PVk7zusrCGG2EqgFBp+ADsj4rrGO9Ofcdh6WY5AlnPum9dD/DpvacEZ/
KKNRnJmTkFOHdkyImJvDLzpkDimS8cpimtB7G6nxQvocM7DuTtTQAqfLfApnVhF+4vkFHtiVs0LQ
bwZqs3sxxd1/UAEq0ksyKRxD+5do2IEU8MG72BjzGV1rq+H1kmsGGwtImdMj0DEoL+E4ffyi631b
0+ZzqoDk6SGEPRxGVcmufy+Kv/WpmBBsaAkuL6oQWQkIsTBq3Vq9jephNgUKR6D7bkNc/i0YFkba
qD4i51OH50rN3NBHMfVpmSYUoUjNxw75uUn/c9JAGg03ONxvqMdV2Ufxdjr2PFl3WsolCsGZEE3I
P2JAo39Hqr438PdN3NRbcIL5baI+E5emNR2CzekrFSeqhVlTYWlNKF0bzaDLFy2z/FycSsngpMqo
EugY8PDfwf0gtPsCXTGTpXtYhaOE6878FtyxrShUp1b5HXJKT+IAJHKjvYK7FDq3wzeRfS6FeXFM
N9XO+nDJtIo5vnceLnZmm2CNSw+1JRE1sv9xzFaz+BhJr4YU31EcpQ5dnejihfpWgKQ2l4QazbwD
edU5qKCUebHpaKqwuQ6lKQn8F4cLDM2haU2a3nJKzOaYg2ZAQU3eeHvhvFVo0vjv7e1HlDlmn2X8
OrFtAYAvzCCdvItFPCUnmlBT6OhdkT+JEdbaVj0/m7odOHxXhvSf5S/n4iL7cRvZeEUTxa3EK4ve
9qy52FWxGtoI9m20NbfTRA3k9uLB3IbaLkULJo2d8Z4k4rtTvJoPVFCXr8HZxofL3bs/fF9HeGGM
ui7KcQF/AYEVYxDWO58M0GIcfYjr5Pk4SnkHCS8hOsnzD82shC5aYKpPLmJ3oE8xSGt8lYF7uxLv
aY3KjXOh5NX099K9+4AfeKEHQap07EC9qG3YfYP0/gBvaGlb8NtePaUC53tGvc4cfQUnW5fHYKcD
SfFaupftAUHWsFnCHd4dCIdjfjVFc5q51GAiqzPx9HMolHq6LHSzmtWAIi85YbZesToFbR8fzvdn
MYoxATBTAh52fUoqV/hrMSVFvbOV3HmR6WrQNxMkeQ9Fgvy/jLYT7hLhV+O5vYrxds2tYOirRq9t
IvDghfHTdgmfqMyOuLI5vvfDLdzoAy1tQoM4h8Kl/a1u8V9zTBatElJUikLi8InPDTpiM3DDvVI3
3zAv4b3ksIRXdvZztoC2nI9y0xKCdlYPUeC5L++xkcAGzk8DZt2CaSNst4JrADNTw2u/UV1UOEzt
WN+eeThWOSFn1buPaO434prrL//Dhxo0zV0eeXOPL7/Aue+YFCv3J5SfnKONVcxRXlyC7FILNLFm
5brtAQ3cJdN08ZipxRu8NL0Mq2dEZOI987zWszi876GJ7+Cyw4etw02HVbcXrgY1vou1NwOVW88m
O/6SYvEkEjJjVzqU6vvuqe9C8FP6PxUllMpceEx8kSvLVjalph86bEsaQ5PWvEsu9H8p5EWJMV/c
t8embiZ2VwZAdPUqODS7+fuHpLF4H8+3C3v0U/GchEX2h0XA2aQyInA1by3y8X7zESTsU+1ChC1T
1uovQCCKBLbq8XgzwbwzOFUtyGOwLHUm8fnQz4pHhc/MeXz2DFcEl7Gx7NGdylJw5bz8ardJ0Feb
+BQNI0aeOnJ89d3Kju9D2EXJBTrZ2v6C3Jk2ZCzlis7EAf5UQC4hOjTTVOaUlH5W3lvsqe7vUUuL
xIaGcYYq2v1LUv6RsfSUqn/nGtqBCfj1QGsD/TS+fe0sZvQpN+DRRBD9DjBRdxKAi/Y+gmWfjOo4
CndoXtIqIfVoZXzINgjaCfsNlbXe9VUJKMsN54oFjo5H1gv+rPb07bjuZQjodrQQb5uCVF7WRDf6
Fx3UxXs+gyymMT87dGkoWnC43KGoDwv+tvrHJ1SFHJ2wtzqod6lVa9b0a49BXteVP71CPSZZG6qR
3dKw4vK3O78vGea4BWpGmO4e8jUWB88MdYXpMsbxHmXt2TUENY9gLYeg/el/o+PIgz58eJxxbDOM
TAR3vcc+axRh2eAyyptYcnOclHMnyxshJrQRZrUfkxqodex0vZWkonGglzpAWE4Pmz/yK/3Jm40I
VDKDDErrYLrVIDuFfWR5na734L5IWICAW49z9Nz+eR0Bl+28nO8LrwCFvSAhlj3Chn22gRkrYAV1
JRZOeqtonvMSzGJP4e1HM6Uyog8YGes0HZhMGTCl6KJwG0wtrPr9SkkKAcjBIzCZp8oUBoc0CLaC
/BGmj+9AsslYj+w+vnnGxOdJ9uESV/jyHAhl0/Kt7V86W8u2sYLoQYZZhTdQdKj+weN5f6NO+UKQ
YcXU3PTeTYNJUBYR6CQo1ODICqoX2YIP+6SyECxZAlDnjwo3yb4grhvzdzWUGxM6x22s54awoVq3
nVuPlPAtBWA5+6s4EdX9Hj7PG5ZfH7YrJVdv6ScLbHLQU0hQQt9unjLtTwmIYt4bBxFWAG2vcKGb
x3GLfmeYI2cmfvBwrIIX295URsdjR0/IKzTF4ptrtf2MZlGvA5pUdblB55N/igTQBKbT7g+WTsVG
UsMOT8C90OhwrxCVMGUrSxn9VCvMsThmZC1l04dfAJqTC5gklwkBN/TtdQvh3zutp41eeeIfV4Cz
GZEo9DgTVY2/65ghKUBfff0y9wIyp8+Bvs4pSkppsUI8BzWkm6v3zc4gweQoA2BwM2746z5gAIjI
jKxH3rrGglys9v9A0iYmBfAerEnBuu+iW6GUKUgPa7MY3bMiH2lPsC7Flvi8YSgfEUMh5S3ddQus
NZHzKo+MWcVBOTxHPqFUwz3mOGNbDZ0iassknvfipvqWshRpFI3HkGASpfxZ3SdKlC8BBj92//Lm
s1Heg7D9coyDIj3k1DsXU6PtagjzQZ4xh6uV25z8Lk+t5q7Gln9e76L23WSMTdBquGmUuJR1JYTC
bz7/dmJY3z+NdHKxW4pGSyqJLOVLii5IzdDUWbLgIFBtPYlk4DQEcZH9ibseovF6GvDtd7DvGptt
yV+6bP8BL4piy5w0quOXoBVkLXIP/OQHw+MJz0oOV6tJTrn7yAHwuIcxbfFE8CpOcgRfPftgA5JW
PnrjKktWt3dMpeUGh3NEQa+zdZIgWeODbAzC+RAL2mup/xDbHM8ndrm2ngmwX0mRg+nO93T6f5dO
F+XwAdoVUsLjDUku2dOFtrq8Gl6z33rvXCDOAcMuhMHjnz44jTdL9uIB7Zu2ywVSXVSXl+pbIXvd
CohVYtAr9KBMc3anuvqlMTx8NLhwEpJxPpO2wN1gEP79Izsw1D98U3KkhVzXUxT5jcQg9p7EM3/0
hqiX4kfamEoQmWc+bXn5AUFTOhgChlMZ10cTZ17CoOnJ/RifKhRPN2A1JWcKSHyL7UUXbMiAYaiN
oYjeyBGAzz6fAQeBAlui7IkDU876QkDdQ1V+YmHjiFF0booh6EiD7jfGaTX2pzRe75NbJq40ZYU5
SUtEBeF2ZqJ6l1Fgf3AsgT1JX+YZTgoIcA/FGI6nVOLCRVf9nLYHpPik2v1qR8U8YtJ2CnTXNIla
aKHTXnsjLQVfxNszGsasuvZzf3QdNjbe6eDQ3dCNvlVOahYYBUvc4SUvZuWi6oED2byv6Le2byNo
Ip+3MRvx/uunY2GihhQOPFJfnmygiigkrsN8GR8O+jYyWLgjtskFRLte2O6cUl0WzFfrBFxIV9bp
U465wjp1sbAYu5jkNsSY/mtIZETtCFUPMx2VFnzCezdUXofqi210WccqVoPiXpmOwvfqxzEIVF07
lCrFCAZ5sI84kg/JzE3K0vYxAmoeAZVnLOn/cfPUhdNnXGJB/wI2UuBMF9aUBWzMORMKcLHzL94w
BtxX2x0QLpC20aNdOUUO0er43XXAukL3+bO4FBBayZbYUvcSwK3x7AFWlU+7mSmq6aNFTBEj7BTD
WlUyTiByoOBrrbGbTDXy7C6RdhFD+TzlyflJckzqdQekEs2elJjKrBFkHbvgv999uxx02yI8QMBE
xdGI2vU2oTahciuftvyH3UMsUYcxb2k5HJE1d8di/1AOQRwfX+WTX/taWWLBkdO42vdRfpx1O+Z4
7bqFnZwp2mTHu4U8NrFiZUUh5XgKETL14BXhzIL/OMhchemIPXoniwvXIYhDYm8iRigkuOQ2QSgU
0tjb0nTnACE/rYrmcR62HaaUDxLSpqpd55nYZZQg+5P03mebUqaZVZqpfoJs1Y+77eHjdzLpBWZA
ATT7bJTFuWUiCQquu0xAGjjheW4s+oU0HNUPQfS73ypZQYXJaZ60b3vZ7xkopMZRyBZz4iPlONHO
UaOJbRZ8+/XihfSfBIQFrYbeWwnnrazvBwKcczO2CIycff1R5w1Qj/hos2tpOgJbHCnUypSUkuMV
O5pexvleZLZagwwVoaN4KkMpoLkDTlwbzyoznhrpKCV342Wk7a5j9wMapctLSU5G46P5wDzMx6KB
EpXK69Zj575Z/PEIR9tDTN/XSGiCHsAhTO9/3A2MEaq8TEElqbSWHBYrHv22CeP3b3hHSc5OaJra
ynMtdocKeWp1X4QDcOyLYamYY/EYFrWOXDv8pPDcdCkGtnXrc7fzi73rSnSm2FFjq5LqENqvwz1y
vzn3VBoMXIX7gzUtMgNIhSqNvrB8dN2SxF33Evy8JyWAEJwUZu52df/cOdIED0wCmsxT/Sj0twq7
0in/fZKeYVHvXDcy1CzDWwNzKoDE1MQAqnLBQu08wWKYfsywePuDpS+r7u6jB/MSqePAtNlF8CAt
cCWpjsQGIGImSyXRTNhRLjJKx2tIJGkrSPT9tFJxdVsKSY6ExqAfVo7kBL97cEwWoEPAUFTXGfkS
/jWRV7GSYyb2DgKPMlKA87rCASbIA6jqyxZ9X2Z7qAOTLdvbTSx++C1E9ttECyhJgRRN9evFaJQp
PWwvutA4LN0AffChg7oiOGExjauHBdYZKVDp4mJBCnQoptzuoNIzVcjnptDa+5w8nzOGfYlyQedW
jKyaUSoKiGmRxxtl5wuqCLb/wrzX4rDBBfuw7xPvGKbV3jEGYe+1vIxndCjVw8G87HrcMSxI6Mne
VhE8TYOVEuiPvtZdUpO2e3Q1zfcw4cLPyMoBLF62o1MkxthEf8Wm1kCqah2IGSNAKq2SDady5BlT
MaV9SVri7q3i8Sl/820bTDTVx4Q/3cBvTnGfsYwwjNPrej23J+N9Dt6bGTTSDpWuQtNBM8YvA8in
MTKL/V1EutrfVvX+QJriAuQdENrJoTP194fGzt4aVCcMdaf8OBlQM2buuH23+FJ+HFVjFg54kH5u
FTbBBlz7uu08ldQszD228bD1xvxpslFNoT+pwiKZa3SlVldi3GftVapfv8nxWsBz8DA3z6Fs+MzB
xLQTdIJTLY2n9Hf5HAu8A25OacbqeHpDf6gbKTZxQvNyQi4Su0Ad0LQIBoIXUytobaGDu0VbWbR7
N1Bgo/eNwuxQvBMWrrnfgZKwisjHa0nIGsbFPIei8BoC+CCJ+GQgZMSPrt79iD3jD8ASzwXpDClD
Y0A9yaotuEzWYnprLQXz17zg0FS6N+YWbWktJPQjpKt6BNEAxFfCA45kJkrSlr/KwayAssezAQHo
B37Aobr1XJCo16WgyX6dJEbjIucXdAr40TEiXz1C/AYMUNuqnXHXQXk2E8dGakjVWviIruoNTs6Z
sqIQrWsiU/8aWhUGjsroL/GhJE5nHNWrbobp2vSfGa1aCxZELyt3fFCGUOn1mbpvqmJAjukk40fx
zKde2aw396l6lVWfG1JKOzFrK1BxhBqpRgsIz2WxGCQVJmznyTEKzWOuCApAH0uaMxpDx225kVGg
6N72KBBpzgGMA0PU0iSyTg5+y6iy7CAj/71NyT0tyjvXhzaJ6BHBD8gudAay3cxW68bi7cQm6nwg
Oo+p9Sfgg2dlU0Xjy8ZXYDAqy1a0hoeJTBFVXO7jhecGhAUmiyVdKs98yS/xWh+/IMki7L5LM9MH
PoUJMfrCgdYgBfyRUhDja8CiEO/mBGRkU6MAkVgMxIJqm8aSsiTlmzNZZx0Qf2BvkPUVeZ3/SepH
ugXWta+b53JT/M7M4Q+zhww//oP28PiiUTvrp0aeokk3aDD+WvVubgknP4LplNUqUbj/+rA6AmuV
fGXzSyymCuRDQBUT6dlWx7jv+GB7C1FOgEjU9qcPIIZodnYvfA9JG+U1ii7f+P/POync7LLhoYmD
oRM2hm+tQFHiCeh3jZ54McQwd/KFTkRjKOI4yZoh9iwaPMns12wzEjt0BlxWRT6MwnvJ2lYBhJfm
Vib0wtROmCtrfikTYLAJkwNuGO8wfMNPJlf937f90FuRPTZHn/2LO6tG1ABL7a51U3jjQXcb9Fv7
MFSqkK3uGX+vvbOOZmDE3XiT2m2ShXd+V0RBSERlKQQ4uSD0bZ8FjL1KKsj4JZq6GgyCPYf6F3j4
O96TUPkN8nsaXH1WNbTQMo3uGZ3DYqwjple7a5iXZa9hFUmhayhN1h3IPEnPLNYF1ecRmofEPrYz
790bp9sP90iiOW7qfgkRtTDYMw1byizL2+BRrzm07mjA43St1QSaQ8axq9pjAfkObMuDeHCwMfWS
UWeed9L36rAHHSqaCvE8p7vXMv7Yy2jUcj9PuXbUTXpn9S+9IiuIf+kvhsojK81fQ+o5HgIxLkDr
uxiIIjm3MpvZTTqeHRoAod9II7p+H8dWnlqCA1jbyiyCRyd9CA7Y08yFXo+E82jim3xkZs5siMIt
G6+BN5zXpauvFp5mYrDCVho9LLaG1kleU7DXmenTL+DCwbezudYVo7FU/7v6V+kChdPkbaeas/zF
jNA3QQEpsj6ZDfnGh4GhxH5aYXD8Jidc8+nEspTUvpJElfGKH3qzxGQNdDQRtBZhD0SojusDHkVk
QphUx3TtvBZ+OIZ02ZGy5IBZBth2wPytAPwuEeYpBQiDf/fc/dTLU4yVpzFXmCIGP1O9BWyo3tPh
nQ79WyJal2pJrSBLj7S6PQ014WBCLcIjQFYRK2Q5IHNULiBOy3c4Y5odNcsirdpEnBuVnJEDG7O8
lHW7/X79qK5kuxXbLBU+NFnTiHsZFJCbXp6p0QA+6cZHkRGApF+S5r8BVb/GtjaDX1ym664yK9KE
77s/tH1krtuu2TpyNFJWy3uqpsBFvSRR0lXadlVC/gfm38YKNv/ICPZQR0n3ko58SLWJFiuynmv1
mjtuZY0bahlKWRG9+aG24DjTBQ515aUYyncSs44kGcV2mGP3JCcVqvdpFJjRYFKuBQAOjGkXOOkb
Mbr77bNMYf1SojGrZeP2AJZ7oZsz1s3DWXBcWHSQdepCIqfex9QXN7lLbcIzt03J3R6n3wTJ6x9e
XKjxLe/KV26dmaJ9i8uq/AwGDxPv6D+o0RXtrRfiF52GMszgOBmR7Acdf9K6dE8eXPuwZImsaf7s
TSw46pbQOsNTbKNxXAf7n6Jiqv/Ld+SnejoOKu0oho9hinT3Wt6jwwWOxHpIebGrBRcGUKJbKIjs
OxpQxdTrzoW4i6+aAncDqhK+pyn4Vpy8hdHPtIbehCk08WPDW+aIVEATpgl0/bJd88E8EzbQFcJG
3HN2GeNx+QiYy5oEoJLXAKdRqx6dBo4BXx6rgtxWJ1X8HF1yq65tQTZgH3gUhHEuo/9Dm/dN/TYu
aqz22OjkJhyy9cvwq1SL34tpGClj/8JrBvJGBLeUkJlO05F2uaRcOA0tiCO3oGhiSU1AIKHHcsWU
5Pgy5+mLHe4tZm0pK//923X+Scm494DpX6FJNaUYvvTQAi/8xIrATgJ03Zw+OueUAFkn8Od3rV9P
x2E/XixcjkeCje59OMKoaPU6vL6uD7E/W08WcXi/i+q1J2dbMobNsxTpgWyr2rGhNyJlTGWKnd9V
MaKMMdDp871kz/bTCMgxcC6K5Du1CbhoPwLmPKeD8/D3PS3p1ZGjcChMpCOSZyCu8jhWZrcduqkI
eb7Rl4QIGmLXD77egOG7HtZ98njg/wG4lir9cOGkJsYcPjXUCUdXKi5a6Z+QvyPTyDCScEk2Eu71
db5lZF1XWU/2hxlOCJRFzrlfEhBZTygMv0GF3Xylpo/pEdmLt6FjpMiaSg8u4Z7aVzuVi244yk7a
CN2fAH/I9HYOWKwmlPRVanBQSL2Q8pPXXmScO/JWgKK+2ggv00M2vnaAugxyQC5lV/L/qiP13KzX
GUrqMqIK3XroFlFR+wUEkEPXWPncOoL7IRMOZOoOHOPr9/CCFA9UIQlr4mT8uZMLXgVksKrBFx8o
gaUzo67PTTU1N+hRBFM5VqN3z0mwi9BBdNDZm6qlC+lfPrFv6rEkQcza+yDFsB+jrleIlRs/qugz
aKqYLOV3fSbiX01OaTZSdtrIq4VPXQLqN1zzLLZf74Oxfd8KK6y/Ujei9l7ZkZFHKFq28cVmyFpA
cvuDpM6+h5dpml5iQfpERiv1cCaLO/kbVWIqm6P8mLOrPKVcnPPgVaj0IDVWyQntTjaQOZ8M+PCE
7wOyvbrKeSeb8tDb0ryGXijv3ixEwlUOuCSCA3vJcz6ZSzRin+dUFy1Fht04V9BD6BM9S58a3pKP
pD6wWWKaHvXRqEWBFmX/JI7kjqN8nXy4ue5D19CYUexfpJl47eCXxoV89KSez9ZUSwttimvNqlol
kxavmpYUAckA/OEqHD+LstWHepFiutuiW9T567iOjLY6rGtIaliTU+kRwWIFr32WEU9N2lKcRoPA
MZOnNx8XTlJ8SlOkei+APZenrxgDdvY0ZfSEwsGScPkWOohyoJAnzH41j/EZZPpR/SsL6vEKkh4M
RaecfapBxUoS/KbmJ3hB+QTfGs1M2Jv9XpcmP2AQJ1/+MsTU4d9vftF8iZrgYpHR6ZA4k3KlyFO6
h/ZgZGMsNDCaJydgE1MsbqC41VZUetF9H4I9l1AZYgpKKZdnBEQYH0+C4PqAHjyFz7nKLfO2gJMD
NEY3lb28xRSjW3zOexlGOAT3j0+fdRnwEYyffvcEHIcHltKa/41qMzcBKCSJpm4lDOJR3LC63I10
aLPd7rAxS6reyB6rSy79yEufLQbYux//74iLV2dfVUMm5qfDFYwoXfOPeklqtnnz+c2yzPnpVBC0
4hn86N2VavALL8kDcERbclfEE04XEJzwvTPVxX+mTEKT6o9HOLZh7yZqn8CErw5VF5/oBkgVV8JD
eK3pkHYe+vsxOZTcFIAGxm9JkjpKwi5+d/6gHbn+CAl9HpHyKarbG21O97S6idyhPWj5qaT5IMVF
2Vra1F37RwFv1t53j3t3G5GW9JcSm1QEjpvZitHui67B4XgvbKVmPJw3CtHL0vgRCFtIw58jcH4K
eLaqBGYBZ/J9eVfjBESMBsXayxEI71pXPDlspyx25pA8NSy3r+krV9PuIbzHhKF6Ppt/GSWwF8pe
tmhv/89dLkwv52aMwJKgYkFbOhDkUd8Pf9z381HWwx7LYiNKh0rET0FFbioDNESWYpqar8xbX3EU
UA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sr_ram_adc_iddr_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 7;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is 2;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "kintexu";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of sr_ram_adc_iddr_c_shift_ram_v12_0_14 : entity is "yes";
end sr_ram_adc_iddr_c_shift_ram_v12_0_14;

architecture STRUCTURE of sr_ram_adc_iddr_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00";
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "00";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 2;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00";
  attribute c_depth of i_synth : label is 7;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "kintexu";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.sr_ram_adc_iddr_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sr_ram_adc_iddr is
  port (
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of sr_ram_adc_iddr : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of sr_ram_adc_iddr : entity is "shift_ram_adc_iddr,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of sr_ram_adc_iddr : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of sr_ram_adc_iddr : entity is "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614";
end sr_ram_adc_iddr;

architecture STRUCTURE of sr_ram_adc_iddr is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 2;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 7;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "kintexu";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of SSET : signal is "xilinx.com:signal:data:1.0 sset_intf DATA";
  attribute x_interface_parameter of SSET : signal is "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.sr_ram_adc_iddr_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
