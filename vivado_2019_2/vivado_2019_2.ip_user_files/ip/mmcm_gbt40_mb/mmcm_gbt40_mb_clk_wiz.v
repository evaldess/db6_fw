
// file: mmcm_gbt40_mb.v
// 
// (c) Copyright 2008 - 2013 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
//----------------------------------------------------------------------------
// User entered comments
//----------------------------------------------------------------------------
// None
//
//----------------------------------------------------------------------------
//  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
//   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
//----------------------------------------------------------------------------
// p_clk40_out__40.08000______0.000______50.0______210.344____175.930
// p_clk40_90_out__40.08000_____90.000______50.0______210.344____175.930
// p_clk40_180_out__40.08000____180.000______50.0______210.344____175.930
// p_clk40_270_out__40.08000____270.000______50.0______210.344____175.930
// p_clk80_out__80.16000______0.000______50.0______173.475____175.930
// p_clk280_out__280.56000______0.000______50.0______138.028____175.930
// p_clk560_out__561.12000______0.000______50.0______124.657____175.930
//
//----------------------------------------------------------------------------
// Input Clock   Freq (MHz)    Input Jitter (UI)
//----------------------------------------------------------------------------
// __primary___________40.08____________0.010

`timescale 1ps/1ps

module mmcm_gbt40_mb_clk_wiz 

 (// Clock in ports
  // Clock out ports
  output        p_clk40_out,
  output        p_clk40_90_out,
  output        p_clk40_180_out,
  output        p_clk40_270_out,
  output        p_clk80_out,
  output        p_clk280_out,
  output        p_clk560_out,
  // Dynamic reconfiguration ports
  input   [6:0] p_daddr_in,
  input         p_dclk_in,
  input         p_den_in,
  input  [15:0] p_din_in,
  output [15:0] p_dout_out,
  output        p_drdy_out,
  input         p_dwe_in,
  // Dynamic phase shift ports
  input         p_psclk_in,
  input         p_psen_in,
  input         p_psincdec_in,
  output        p_psdone_out,
  // Status and control signals
  input         p_reset_in,
  output        p_input_clk_stopped_out,
  output        p_clkfb_stopped_out,
  output        p_locked_out,
  output        p_cddcdone_out,
  input         p_cddcreq_in,
  input         p_clk_in
 );
  // Input buffering
  //------------------------------------
wire p_clk_in_mmcm_gbt40_mb;
wire clk_in2_mmcm_gbt40_mb;
  IBUF clkin1_ibuf
   (.O (p_clk_in_mmcm_gbt40_mb),
    .I (p_clk_in));




  // Clocking PRIMITIVE
  //------------------------------------

  // Instantiation of the MMCM PRIMITIVE
  //    * Unused inputs are tied off
  //    * Unused outputs are labeled unused

  wire        p_clk40_out_mmcm_gbt40_mb;
  wire        p_clk40_90_out_mmcm_gbt40_mb;
  wire        p_clk40_180_out_mmcm_gbt40_mb;
  wire        p_clk40_270_out_mmcm_gbt40_mb;
  wire        p_clk80_out_mmcm_gbt40_mb;
  wire        p_clk280_out_mmcm_gbt40_mb;
  wire        p_clk560_out_mmcm_gbt40_mb;

  wire        p_locked_out_int;
  wire        clkfbout_mmcm_gbt40_mb;
  wire        clkfbout_buf_mmcm_gbt40_mb;
  wire        clkfboutb_unused;
    wire clkout0b_unused;
   wire clkout1b_unused;
   wire clkout2b_unused;
   wire clkout3b_unused;
  wire        reset_high;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg1 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg2 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg3 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg4 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg5 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg6 = 0;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg  [7 :0] seq_reg7 = 0;


  
    MMCME3_ADV

  #(.BANDWIDTH            ("OPTIMIZED"),
    .CLKOUT4_CASCADE      ("FALSE"),
    .COMPENSATION         ("AUTO"),
    .STARTUP_WAIT         ("FALSE"),
    .DIVCLK_DIVIDE        (1),
    .CLKFBOUT_MULT_F      (28.000),
    .CLKFBOUT_PHASE       (0.000),
    .CLKFBOUT_USE_FINE_PS ("FALSE"),
    .CLKOUT0_DIVIDE_F     (28.000),
    .CLKOUT0_PHASE        (0.000),
    .CLKOUT0_DUTY_CYCLE   (0.500),
    .CLKOUT0_USE_FINE_PS  ("FALSE"),
    .CLKOUT1_DIVIDE       (28),
    .CLKOUT1_PHASE        (90.000),
    .CLKOUT1_DUTY_CYCLE   (0.500),
    .CLKOUT1_USE_FINE_PS  ("FALSE"),
    .CLKOUT2_DIVIDE       (28),
    .CLKOUT2_PHASE        (180.000),
    .CLKOUT2_DUTY_CYCLE   (0.500),
    .CLKOUT2_USE_FINE_PS  ("FALSE"),
    .CLKOUT3_DIVIDE       (28),
    .CLKOUT3_PHASE        (270.000),
    .CLKOUT3_DUTY_CYCLE   (0.500),
    .CLKOUT3_USE_FINE_PS  ("FALSE"),
    .CLKOUT4_DIVIDE       (14),
    .CLKOUT4_PHASE        (0.000),
    .CLKOUT4_DUTY_CYCLE   (0.500),
    .CLKOUT4_USE_FINE_PS  ("FALSE"),
    .CLKOUT5_DIVIDE       (4),
    .CLKOUT5_PHASE        (0.000),
    .CLKOUT5_DUTY_CYCLE   (0.500),
    .CLKOUT5_USE_FINE_PS  ("FALSE"),
    .CLKOUT6_DIVIDE       (2),
    .CLKOUT6_PHASE        (0.000),
    .CLKOUT6_DUTY_CYCLE   (0.500),
    .CLKOUT6_USE_FINE_PS  ("FALSE"),
    .CLKIN1_PERIOD        (24.950))
  mmcme3_adv_inst
    // Output clocks
   (
    .CLKFBOUT            (clkfbout_mmcm_gbt40_mb),
    .CLKFBOUTB           (clkfboutb_unused),
    .CLKOUT0             (p_clk40_out_mmcm_gbt40_mb),
    .CLKOUT0B            (clkout0b_unused),
    .CLKOUT1             (p_clk40_90_out_mmcm_gbt40_mb),
    .CLKOUT1B            (clkout1b_unused),
    .CLKOUT2             (p_clk40_180_out_mmcm_gbt40_mb),
    .CLKOUT2B            (clkout2b_unused),
    .CLKOUT3             (p_clk40_270_out_mmcm_gbt40_mb),
    .CLKOUT3B            (clkout3b_unused),
    .CLKOUT4             (p_clk80_out_mmcm_gbt40_mb),
    .CLKOUT5             (p_clk280_out_mmcm_gbt40_mb),
    .CLKOUT6             (p_clk560_out_mmcm_gbt40_mb),
     // Input clock control
    .CLKFBIN             (clkfbout_buf_mmcm_gbt40_mb),
    .CLKIN1              (p_clk_in_mmcm_gbt40_mb),
    .CLKIN2              (1'b0),
     // Tied to always select the primary input clock
    .CLKINSEL            (1'b1),
    // Ports for dynamic reconfiguration
    .DADDR               (p_daddr_in),
    .DCLK                (p_dclk_in),
    .DEN                 (p_den_in),
    .DI                  (p_din_in),
    .DO                  (p_dout_out),
    .DRDY                (p_drdy_out),
    .DWE                 (p_dwe_in),
    .CDDCDONE            (p_cddcdone_out),
    .CDDCREQ             (p_cddcreq_in),
    // Ports for dynamic phase shift
    .PSCLK               (p_psclk_in),
    .PSEN                (p_psen_in),
    .PSINCDEC            (p_psincdec_in),
    .PSDONE              (p_psdone_out),
    // Other control and status signals
    .LOCKED              (p_locked_out_int),
    .CLKINSTOPPED        (p_input_clk_stopped_out),
    .CLKFBSTOPPED        (p_clkfb_stopped_out),
    .PWRDWN              (1'b0),
    .RST                 (reset_high));
  assign reset_high = p_reset_in; 

  assign p_locked_out = p_locked_out_int;
// Clock Monitor clock assigning
//--------------------------------------
 // Output buffering
  //-----------------------------------

  BUFG clkf_buf
   (.O (clkfbout_buf_mmcm_gbt40_mb),
    .I (clkfbout_mmcm_gbt40_mb));







  BUFGCE clkout1_buf
   (.O   (p_clk40_out),
    .CE  (seq_reg1[7]),
    .I   (p_clk40_out_mmcm_gbt40_mb));

  BUFGCE clkout1_buf_en
   (.O   (p_clk40_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk40_out_mmcm_gbt40_mb));
  always @(posedge p_clk40_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	    seq_reg1 <= 8'h00;
    end
    else begin
        seq_reg1 <= {seq_reg1[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout2_buf
   (.O   (p_clk40_90_out),
    .CE  (seq_reg2[7]),
    .I   (p_clk40_90_out_mmcm_gbt40_mb));
 
  BUFGCE clkout2_buf_en
   (.O   (p_clk40_90_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk40_90_out_mmcm_gbt40_mb));
 
  always @(posedge p_clk40_90_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg2 <= 8'h00;
    end
    else begin
        seq_reg2 <= {seq_reg2[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout3_buf
   (.O   (p_clk40_180_out),
    .CE  (seq_reg3[7]),
    .I   (p_clk40_180_out_mmcm_gbt40_mb));
 
  BUFGCE clkout3_buf_en
   (.O   (p_clk40_180_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk40_180_out_mmcm_gbt40_mb));
 
  always @(posedge p_clk40_180_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg3 <= 8'h00;
    end
    else begin
        seq_reg3 <= {seq_reg3[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout4_buf
   (.O   (p_clk40_270_out),
    .CE  (seq_reg4[7]),
    .I   (p_clk40_270_out_mmcm_gbt40_mb));

  BUFGCE clkout4_buf_en
   (.O   (p_clk40_270_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk40_270_out_mmcm_gbt40_mb));
	
  always @(posedge p_clk40_270_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg4 <= 8'h00;
    end
    else begin
        seq_reg4 <= {seq_reg4[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout5_buf
   (.O   (p_clk80_out),
    .CE  (seq_reg5[7]),
    .I   (p_clk80_out_mmcm_gbt40_mb));
 
  BUFGCE clkout5_buf_en
   (.O   (p_clk80_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk80_out_mmcm_gbt40_mb));
 
  always @(posedge p_clk80_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg5 <= 8'h00;
    end
    else begin
        seq_reg5 <= {seq_reg5[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout6_buf
   (.O   (p_clk280_out),
    .CE  (seq_reg6[7]),
    .I   (p_clk280_out_mmcm_gbt40_mb));
 
  BUFGCE clkout6_buf_en
   (.O   (p_clk280_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk280_out_mmcm_gbt40_mb));
 
  always @(posedge p_clk280_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg6 <= 8'h00;
    end
    else begin
        seq_reg6 <= {seq_reg6[6:0],p_locked_out_int};
  
    end
  end


  BUFGCE clkout7_buf
   (.O   (p_clk560_out),
    .CE  (seq_reg7[7]),
    .I   (p_clk560_out_mmcm_gbt40_mb));
 
  BUFGCE clkout7_buf_en
   (.O   (p_clk560_out_mmcm_gbt40_mb_en_clk),
    .CE  (1'b1),
    .I   (p_clk560_out_mmcm_gbt40_mb));
 
  always @(posedge p_clk560_out_mmcm_gbt40_mb_en_clk or posedge reset_high) begin
    if(reset_high == 1'b1) begin
	  seq_reg7 <= 8'h00;
    end
    else begin
        seq_reg7 <= {seq_reg7[6:0],p_locked_out_int};
  
    end
  end





endmodule
