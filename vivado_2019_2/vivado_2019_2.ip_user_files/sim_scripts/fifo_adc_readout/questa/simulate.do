onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifo_adc_readout_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_adc_readout.udo}

run -all

quit -force
