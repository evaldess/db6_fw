onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib mmcm_gbt40_db_opt

do {wave.do}

view wave
view structure
view signals

do {mmcm_gbt40_db.udo}

run -all

quit -force
