onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib blk_mem_tdpram_1kx18_8b512_opt

do {wave.do}

view wave
view structure
view signals

do {blk_mem_tdpram_1kx18_8b512.udo}

run -all

quit -force
