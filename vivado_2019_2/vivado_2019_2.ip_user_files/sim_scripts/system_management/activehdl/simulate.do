onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+system_management -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.system_management xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {system_management.udo}

run -all

endsim

quit -force
