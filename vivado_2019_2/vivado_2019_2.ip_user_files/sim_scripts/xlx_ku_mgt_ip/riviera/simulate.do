onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+xlx_ku_mgt_ip -L xpm -L gtwizard_ultrascale_v1_7_7 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.xlx_ku_mgt_ip xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {xlx_ku_mgt_ip.udo}

run -all

endsim

quit -force
